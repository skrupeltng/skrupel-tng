#/bin/sh

psql postgresql://skrupeladmin@localhost:1234/skrupel_integration_test -f Scripts/clear_planets.sql

pg_dump --host localhost --port 1234 --username "skrupeladmin" --encoding UTF8 --file "docker/postgres-it/complete_dump.sql" "skrupel_integration_test";

sed -i "s/SELECT pg_catalog.set_config('search_path', '', false);//g" docker/postgres-it/complete_dump.sql
