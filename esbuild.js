#!/usr/bin/env node

const esbuild = require('esbuild');
const sassPlugin = require('esbuild-sass-plugin');

esbuild.build({
	entryPoints: ['src/main/ts/modules/common_index.ts'],
	bundle: true,
	sourcemap: true,
	outfile: 'build/resources/main/static/dist/landingpage.bundled.js'
}).catch(() => process.exit(1));

esbuild.build({
	entryPoints: ['src/main/ts/modules/dashboard/index.ts'],
	bundle: true,
	sourcemap: true,
	outfile: 'build/resources/main/static/dist/dashboard.bundled.js',
}).catch(() => process.exit(1));

esbuild.build({
	entryPoints: ['src/main/ts/modules/ingame/index.ts'],
	bundle: true,
	sourcemap: true,
	outfile: 'build/resources/main/static/dist/ingame.bundled.js',
}).catch(() => process.exit(1));

esbuild.build({
	entryPoints: ['src/main/ts/browser_warning.ts'],
	bundle: false,
	outfile: 'build/resources/main/static/dist/browser_warning.js',
}).catch(() => process.exit(1));

esbuild.build({
	entryPoints: ['src/main/scss/modules/dashboard/dashboard.scss'],
	bundle: true,
	outfile: 'build/resources/main/static/dist/dashboard.bundled.css',
	external: ['*.woff2', '*.ttf', '*.png', '*.jpg', '*.gif'],
	plugins: [sassPlugin.sassPlugin()]
}).catch(() => process.exit(1));

esbuild.build({
	entryPoints: ['src/main/scss/modules/ingame/ingame.scss'],
	bundle: true,
	outfile: 'build/resources/main/static/dist/ingame.bundled.css',
	external: ['*.woff2', '*.ttf', '*.png', '*.jpg', '*.gif'],
	plugins: [sassPlugin.sassPlugin()]
}).catch(() => process.exit(1));

esbuild.build({
	entryPoints: ['src/main/scss/dark_mode.scss'],
	bundle: true,
	outfile: 'build/resources/main/static/dist/dark_mode.css',
	external: ['*.woff2', '*.ttf', '*.png', '*.jpg', '*.gif'],
	plugins: [sassPlugin.sassPlugin()]
}).catch(() => process.exit(1));

esbuild.build({
	entryPoints: ['src/main/scss/light_mode.scss'],
	bundle: true,
	outfile: 'build/resources/main/static/dist/light_mode.css',
	external: ['*.woff2', '*.ttf', '*.png', '*.jpg', '*.gif'],
	plugins: [sassPlugin.sassPlugin()]
}).catch(() => process.exit(1));

esbuild.build({
	entryPoints: ['src/main/scss/space_mode.scss'],
	bundle: true,
	outfile: 'build/resources/main/static/dist/space_mode.css',
	external: ['*.woff2', '*.ttf', '*.png', '*.jpg', '*.gif'],
	plugins: [sassPlugin.sassPlugin()]
}).catch(() => process.exit(1));
