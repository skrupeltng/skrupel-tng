const {defineConfig} = require('cypress')

module.exports = defineConfig({
	viewportWidth: 1920,
	viewportHeight: 1080,
	defaultCommandTimeout: 20000,
	scrollBehavior: false,
	video: false,
	reporter: 'junit',
	reporterOptions: {
		mochaFile: 'test-results/my-test-output-[hash].xml',
	},
	e2e: {
		supportFile: 'src/test/ts/cypress/support/e2e.ts',
		specPattern: 'src/test/ts/cypress/e2e/**/*.cy.ts',
		baseUrl: 'http://localhost:8080'
	}
})
