import com.github.gradle.node.npm.task.NpmTask

plugins {
	id("org.springframework.boot") version "3.4.2"
	id("com.github.node-gradle.node") version "7.1.0"
	id("org.jetbrains.kotlin.jvm") version "2.1.10"
	java
	id("org.sonarqube") version "6.0.1.5171"
	jacoco
	id("org.ajoberstar.grgit") version "5.3.0"
	id("com.avast.gradle.docker-compose") version "0.17.12"
	id("org.jetbrains.gradle.docker") version "1.6.0-RC.5"
}

apply(plugin = "io.spring.dependency-management")
apply(plugin = "docker-compose")

repositories {
	mavenCentral()
}

sonar {
	properties {
		property("sonar.projectKey", "skrupeltng_skrupel-tng")
		property("sonar.organization", "skrupeltng")
	}
}

tasks.jacocoTestReport {
	dependsOn(tasks.test)

	reports {
		csv.required.set(false)
		html.required.set(false)
		xml.required.set(false)
	}
}

tasks.test {
	extensions.configure(JacocoTaskExtension::class) {
		val coverageReportFile = System.getProperty("coverageReportFile")
		setDestinationFile(layout.buildDirectory.file("reports/jacoco/${coverageReportFile}.exec").get().asFile)
	}
}

tasks.bootJar {
	archiveBaseName.set("skrupel-tng")
	mainClass.set("org.skrupeltng.Main")
	archiveVersion.set("1.0-RELEASE")
	launchScript()
}

java {
	toolchain {
		languageVersion.set(JavaLanguageVersion.of(JavaVersion.VERSION_20.majorVersion))
	}
}

// Hibernate 6.6.x that gets shipped with the Spring Boot BOM of 3.4.2 has a breaking bug so we need to fall back to a slightly older one.
// Once https://hibernate.atlassian.net/browse/HHH-18936 is fixed this can be removed again when the next Spring Boot BOM is out.
extra["hibernate.version"] = "6.5.3.Final"

val thymeleafLayoutDialectVersion = "3.3.0"
val commonsMathVersion = "3.6.1"
val commonsCsvVersion = "1.13.0"
val mapstructVersion = "1.6.3"
val passayVersion = "1.6.6"
val owaspSanitizerVersion = "20240325.1"
val greenMailVersion = "2.1.2"
val jetbrainsAnnotationsVersion = "26.0.2"

val mapstructProcessor = "org.mapstruct:mapstruct-processor:$mapstructVersion"

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-mail")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("org.springframework.boot:spring-boot-starter-validation")
	implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
	implementation("org.thymeleaf.extras:thymeleaf-extras-springsecurity6")
	implementation("org.springframework.boot:spring-boot-starter-cache")
	implementation("org.postgresql:postgresql")
	implementation("org.liquibase:liquibase-core")
	implementation("org.passay:passay:$passayVersion")
	implementation("org.jetbrains:annotations:$jetbrainsAnnotationsVersion")

	implementation("nz.net.ultraq.thymeleaf:thymeleaf-layout-dialect:$thymeleafLayoutDialectVersion")
	implementation("org.apache.commons:commons-lang3")
	implementation("org.apache.commons:commons-math3:$commonsMathVersion")
	implementation("org.apache.commons:commons-csv:$commonsCsvVersion")
	implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml")
	implementation("org.mapstruct:mapstruct:$mapstructVersion")
	implementation("org.mapstruct:mapstruct-processor:$mapstructVersion")
	implementation("com.googlecode.owasp-java-html-sanitizer:owasp-java-html-sanitizer:$owaspSanitizerVersion")

	runtimeOnly("org.springframework.boot:spring-boot-properties-migrator")

	annotationProcessor(mapstructProcessor)

	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.springframework.security:spring-security-test")
	testImplementation("org.junit.jupiter:junit-jupiter-api")
	testImplementation("org.junit.platform:junit-platform-commons")
	testImplementation("org.mockito:mockito-core")
	testImplementation("org.mockito:mockito-junit-jupiter")
	testImplementation("org.testcontainers:testcontainers")
	testImplementation("org.testcontainers:junit-jupiter")
	testImplementation("org.testcontainers:postgresql")
	testImplementation("com.icegreen:greenmail-junit5:$greenMailVersion")

	testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")

	testAnnotationProcessor(mapstructProcessor)
}

java.sourceSets["main"].java.srcDir(layout.buildDirectory.dir("generated/sources/annotationProcessor/java/main"))

node {
	download.set(true)
	version.set("20.14.0")
	npmVersion.set("10.7.0")
	distBaseUrl.set("https://nodejs.org/dist")
	workDir.set(file("${project.projectDir}/.gradle/nodejs"))
	npmWorkDir.set(file("${project.projectDir}/.gradle/npm"))
}

val gitBranch = grgit.branch.current().name.replace("_", "-")

docker {
	registries {
		create("registry.gitlab.com") {
			imageNamePrefix = "registry.gitlab.com/skrupeltng/"
		}
	}
	images {
		create("postgres-it") {
			files {
				from("docker/postgres-it")
			}
			imageName = "skrupel-tng/postgres-it"
			imageVersion = gitBranch
		}
	}
}

tasks {
	bootBuildImage {
		imageName.set("registry.gitlab.com/skrupeltng/skrupel-tng/skrupel-tng:${gitBranch}")
		environment.put("BP_JVM_VERSION", "21")
	}

	val npmCi by registering(NpmTask::class) {
		args.set(listOf("ci"))
		inputs.file(projectDir.resolve("package.json"))
		outputs.dir(projectDir.resolve("node_modules"))
	}

	val buildTypeScript by registering(NpmTask::class) {
		dependsOn(npmCi)

		args.set(listOf("run", "build"))
		inputs.dir(projectDir.resolve("src/main/ts"))
		outputs.dir(layout.buildDirectory.dir("resources/main/static/dist"))
	}

	val bundle by registering(NpmTask::class) {
		dependsOn(buildTypeScript)

		args.set(listOf("run", "bundle"))
		inputs.dir(projectDir.resolve("src/main/ts"))
		inputs.dir(projectDir.resolve("src/main/scss"))
		outputs.dir(layout.buildDirectory.dir("resources/main/static/dist"))
	}

	val copyFontAwesomeWebFonts by registering(Copy::class) {
		dependsOn(npmCi)

		from("${project.projectDir}/node_modules/@fortawesome/fontawesome-free/webfonts/")
		into("build/resources/main/static/webfonts")
		outputs.dir(layout.buildDirectory.dir("build/resources/main/static/webfonts"))
	}

	val copyTitilliumWebFonts by registering(Copy::class) {
		dependsOn(npmCi)

		from("${project.projectDir}/node_modules/@fontsource/titillium-web/files/titillium-web-latin-400-normal.woff2")
		into("build/resources/main/static/webfonts/")
		outputs.dir(layout.buildDirectory.dir("build/resources/main/static/webfonts"))
	}

	processResources {
		dependsOn(bundle, copyFontAwesomeWebFonts, copyTitilliumWebFonts)
	}

	val uiTestsChrome by registering(NpmTask::class) {
		args.set(listOf("run", "cypress-ui-test-chrome"))
	}

	val uiTestsFirefox by registering(NpmTask::class) {
		args.set(listOf("run", "cypress-ui-test-firefox"))
	}

	dockerCompose {
		environment.put("GIT_BRANCH", gitBranch)
		isRequiredBy(uiTestsChrome)
		isRequiredBy(uiTestsFirefox)

		nested("postgres").startedServices.set(mutableListOf("postgres"))
	}

	test {
		useJUnitPlatform()

		var postgresImageVersion = System.getProperty("postgresImageVersion")

		if (postgresImageVersion == null) {
			postgresImageVersion = gitBranch
		}

		systemProperty("postgresImageVersion", postgresImageVersion)

		testLogging {
			outputs.upToDateWhen { false }
			showStandardStreams = true
		}
	}
}
