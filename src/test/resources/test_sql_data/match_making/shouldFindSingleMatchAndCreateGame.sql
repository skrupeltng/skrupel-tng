INSERT INTO match_making_entry (created, login_id, active, faction_id)
VALUES (now(), 1, true, '_RANDOM_FACTION'),
       (now(), 6, true, '_RANDOM_FACTION'),
       (now(), 7, true, '_RANDOM_FACTION');

INSERT INTO match_making_criteria_item (match_making_entry_id, type, value)
VALUES (1, 'COOP', 'true'),
       (1, 'PLAYER_COUNT', '2'),
       (1, 'PLAYER_COUNT', '4'),
       (1, 'BOT_COUNT', '2'),
       (1, 'BOT_COUNT', '4'),
       (1, 'GAME_MODE', 'CONQUEST'),
       (1, 'GAME_MODE', 'INVASION'),
       (1, 'GALAXY_SIZE', '1000'),

       (2, 'COOP', 'true'),
       (2, 'PLAYER_COUNT', '2'),
       (2, 'PLAYER_COUNT', '4'),
       (2, 'BOT_COUNT', '2'),
       (2, 'GAME_MODE', 'CONQUEST'),
       (2, 'GAME_MODE', 'NONE'),
       (2, 'GALAXY_SIZE', '1000'),
       (2, 'GALAXY_SIZE', '2000'),

       (3, 'COOP', 'true'),
       (3, 'PLAYER_COUNT', '2'),
       (3, 'BOT_COUNT', '6'),
       (3, 'GAME_MODE', 'CONQUEST'),
       (3, 'GAME_MODE', 'NONE'),
       (3, 'GALAXY_SIZE', '1000'),
       (3, 'GALAXY_SIZE', '2000');
