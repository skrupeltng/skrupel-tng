export const DEFAULT_PASSWORD = '$adminPassw0rd';

class LoginHelper {
    login(url: string, username = 'admin') {
        cy.wait(100);
        cy.visit(url);

        const usernameElem = cy.get('#username');

        if (usernameElem) {
            usernameElem.type(username);
            cy.get('#password').type(DEFAULT_PASSWORD);
            cy.get('#skr-login-button').click();
        }
    }

    logoutFromDashboard() {
        cy.get('#skr-dashboard-account-menu-button').click();
        cy.wait(500);

        cy.get('#skr-dashboard-logout-link').should('be.visible').click();
        cy.get('#skr-landingpage-top-register-button').should('be.visible');
    }

    logoutFromIngame() {
        cy.get('#skr-ingame-user-menu-button').click();
        cy.wait(500);

        cy.get('#skr-ingame-logout-button').should('be.visible').click();
        cy.get('#skr-landingpage-top-register-button').should('be.visible');
    }

    register(username: string) {
        cy.visit('/register');

        cy.get('#username').should('be.visible').type(username);

        cy.get('#password').type(DEFAULT_PASSWORD);
        cy.get('#passwordRepeat').type(DEFAULT_PASSWORD);
        cy.get('#dataPrivacyStatementReadAndAccepted1').click();
        cy.get('#skr-register-submit-button').click();

        cy.get('#skr-registration-finished-heading').should('be.visible');
    }
}

export const loginHelper = new LoginHelper();
