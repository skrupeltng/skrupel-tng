class GameDetails {
    selectFaction(selectElementIndex: number, selectionOptionValue: string) {
        cy.get('#skr-game-players').find('.skr-game-faction-select').eq(selectElementIndex).select(selectionOptionValue);
        cy.wait(500);
        cy.get('#skr-game-players').find('.skr-game-faction-select').eq(selectElementIndex).find(':selected').should('have.value', selectionOptionValue);
    }

    invitePlayer(playerName: string, teamName?: string) {
        cy.wait(500);

        const container = teamName ? `.player-table-team-${teamName}` : '';

        cy.get(`${container} .skr-game-player-search-input`).should('be.visible');
        cy.get(`${container} .skr-game-player-search-input`).click();
        cy.get(`${container} .skr-game-player-search-input`).type(playerName);
        cy.get(`${container} #skr-game-add-player-button`).should('not.be.disabled').click();
        cy.get(`${container} .skr-game-remove-player-button`).should('be.visible');
    }

    startGame() {
        cy.get('#skr-start-game-button').should('be.visible').should('not.be.disabled').click();
        cy.url().should('contain', 'ingame/game?id=');
    }
}

export const gameDetails = new GameDetails();
