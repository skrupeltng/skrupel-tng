import {ingameHelper} from '../../../support/ingame-helper';
import {loginHelper} from '../../../support/login-helpers';

describe('Starbase SpaceFolds Page Tests', () => {
    it('Should not display space folds page because starbase tech levels too low', () => {
        loginHelper.login('/ingame/game?id=2');

        ingameHelper.openStarbase(3);

        cy.get('#skr-ingame-selection-panel-tab-space-folds').should('be.visible').click();

        cy.get('#skr-ingame-space-folds-content').should('not.exist');
        cy.get('#skr-starbase-spacefolds-unavailable-message').should('be.visible');
    });

    it('Should display space folds page', () => {
        loginHelper.login('/ingame/game?id=66');

        ingameHelper.openStarbase(94);

        cy.get('#skr-ingame-selection-panel-tab-space-folds').should('be.visible').click();

        cy.get('#skr-starbase-spacefolds-unavailable-message').should('not.exist');
        cy.get('#skr-ingame-space-folds-content').should('be.visible');

        cy.get('#skr-ingame-spacefolds-target-image').should('exist').invoke('attr', 'src').should('not.exist');
        cy.get('#skr-ingame-spacefolds-target-select').should('be.visible').select('ship_174');

        cy.get('#skr-ingame-spacefolds-target-image').should('exist').invoke('attr', 'src').should('equal', '/factions/nightmare/ship_images/12.jpg');
    });
});
