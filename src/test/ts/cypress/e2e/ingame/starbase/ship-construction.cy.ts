import {ingameHelper} from '../../../support/ingame-helper';
import {loginHelper} from '../../../support/login-helpers';

describe('Starbase Ship Construction Tests', () => {
    it('Should allow for reselect of components for ship', () => {
        loginHelper.login('/ingame/game?id=85');

        ingameHelper.openStarbase(117);

        cy.get('.skr-ingame-starbase-select-button[data1="orion_18"]').should('be.visible').click();
        cy.get('.skr-ingame-starbase-select-button[data1="orion_18"]').should('not.be.visible');

        cy.get('#skr-ingame-shipconstruction-selected-propulsion').should('be.visible').should('have.text', 'Psion flow net (9)');
        cy.get('#skr-ingame-shipconstruction-selected-energy').should('be.visible').should('have.text', 'Tryxoker (8)');
        cy.get('#total-costs-money .skr-icon-label-value').should('be.visible').should('have.text', '958');
        cy.get('#total-costs-mineral1 .skr-icon-label-value').should('be.visible').should('have.text', '140');
        cy.get('#total-costs-mineral2 .skr-icon-label-value').should('be.visible').should('have.text', '71');
        cy.get('#total-costs-mineral3 .skr-icon-label-value').should('be.visible').should('have.text', '281');

        cy.get('#skr-propulsion-button-left').should('be.visible').click();
        cy.get('.skr-ingame-starbase-select-button[data1="solarisplasmotan"]').should('be.visible').click();

        cy.get('#skr-ingame-shipconstruction-selected-propulsion').should('be.visible').should('have.text', 'Solarisplasmotan (7)');
        cy.get('#skr-ingame-shipconstruction-selected-energy').should('be.visible').should('have.text', 'Tryxoker (8)');
        cy.get('#total-costs-money .skr-icon-label-value').should('be.visible').should('have.text', '864');
        cy.get('#total-costs-mineral1 .skr-icon-label-value').should('be.visible').should('have.text', '120');
        cy.get('#total-costs-mineral2 .skr-icon-label-value').should('be.visible').should('have.text', '75');
        cy.get('#total-costs-mineral3 .skr-icon-label-value').should('be.visible').should('have.text', '255');
    });
});
