import {loginHelper} from '../../../support/login-helpers';
import {ingameHelper} from "../../../support/ingame-helper";

describe('Fleet Leader Tests', () => {
    function openAndCloseFleetOverviewModal() {
        cy.get(`#skr-ingame-fleets-button`).should('be.visible').click();
        cy.get('#skr-close-fleet-overview-button').should('be.visible').click();
        cy.get('#skr-close-fleet-overview-button').should('not.be.visible');
    }

    it('Should clear fleet leader of fleet when removing last ship from fleet', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=83#ship=235');
        openAndCloseFleetOverviewModal();

        cy.get('.skr-ship-headline-fleet-button-group .dropdown-toggle-split').should('be.visible').click();

        cy.get('#skr-ship-fleet-selection-item-7').should('be.visible').click();

        cy.get('.skr-ship-open-fleet-button span').should('have.text', 'Fleet: Fleet2');

        ingameHelper.openFleet(6);

        cy.get('#skr-ingame-fleet-empty').should('be.visible').should('have.text', 'This fleet has no ships!');
        cy.get('#skr-fleet-leader-image').should('not.exist');

        cy.get(`#skr-ingame-fleets-button`).should('be.visible').click();
        cy.get(`#skr-ingame-fleet-overview-table-content #6 .ship-count`).should('be.visible').should('have.text', '0');
        cy.get(`#skr-ingame-fleet-overview-table-content #7 .ship-count`).should('be.visible').should('have.text', '2');
    });

    it('Should move ship to other fleet', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=83#ship=239');
        openAndCloseFleetOverviewModal();

        cy.get('.skr-ship-headline-fleet-button-group .dropdown-toggle-split').should('be.visible').click();

        cy.get('#skr-ship-fleet-selection-item-11').should('be.visible').click();

        cy.get('.skr-ship-open-fleet-button span').should('have.text', 'Fleet: Fleet6');

        ingameHelper.openFleet(10);

        cy.get('#skr-ingame-fleet-empty').should('not.exist');
        cy.get('#skr-ingame-selection-panel-tab-ships').should('be.visible').click();
        cy.get('#leader-radio-238').should('be.visible').should('have.attr', 'checked', 'checked');

        ingameHelper.openFleet(11);

        cy.get('#skr-ingame-fleet-empty').should('not.exist');
        cy.get('#skr-ingame-selection-panel-tab-ships').should('be.visible').click();
        cy.get('#leader-radio-239').should('be.visible').should('have.attr', 'checked', 'checked');

        cy.get(`#skr-ingame-fleets-button`).should('be.visible').click();
        cy.get(`#skr-ingame-fleet-overview-table-content #10 .ship-count`).should('be.visible').should('have.text', '1');
        cy.get(`#skr-ingame-fleet-overview-table-content #11 .ship-count`).should('be.visible').should('have.text', '1');
    });

    it('Should move leader to other fleet and update leader on original fleet', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=83#ship=236');
        openAndCloseFleetOverviewModal();

        cy.get('.skr-ship-headline-fleet-button-group .dropdown-toggle-split').should('be.visible').click();

        cy.get('#skr-ship-fleet-selection-item-9').should('be.visible').click();

        cy.get('.skr-ship-open-fleet-button span').should('have.text', 'Fleet: Fleet4');

        ingameHelper.openFleet(9);

        cy.get('#skr-ingame-fleet-empty').should('not.exist');
        cy.get('#skr-ingame-selection-panel-tab-ships').should('be.visible').click();
        cy.get('#leader-radio-236').should('be.visible').should('have.attr', 'checked', 'checked');

        ingameHelper.openFleet(8);

        cy.get('#skr-ingame-fleet-empty').should('not.exist');
        cy.get('#skr-ingame-selection-panel-tab-ships').should('be.visible').click();
        cy.get('#leader-radio-237').should('be.visible').should('have.attr', 'checked', 'checked');

        cy.get(`#skr-ingame-fleets-button`).should('be.visible').click();
        cy.get(`#skr-ingame-fleet-overview-table-content #8 .ship-count`).should('be.visible').should('have.text', '1');
        cy.get(`#skr-ingame-fleet-overview-table-content #9 .ship-count`).should('be.visible').should('have.text', '1');
    });
});
