import {loginHelper} from '../../../support/login-helpers';

describe('Fleet Fuel Tests', () => {
    /**
     * This and the next function are needed due to a potential race condition that occurs:
     * When a test performs a fuel-operation on a fuel group (i.e. equalize or change the percentage) the ship-overview modal is also updated after.
     * But this is nested inside the response callback of two ajax calls, so these tend to be executed after the test was already finished and run
     * while the next test is running, leading to CSRF-Token mismatches since each test performs a new login, creating a new CSRF-Token each time.
     * So we need to wait for these ajax calls to finish before finishing a test method.
     */
    function interceptShipOverview() {
        cy.intercept('/ingame/ship-overview').as('shipOverview');
    }

    function waitForShipOverview() {
        cy.wait('@shipOverview');

        cy.wait(100);
    }

    it('Should open ship selection', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=90#fleet=14;fuel');

        cy.get('#skr-ingame-fleet-fuel').should('be.visible').find('.skr-fleet-fuel-group').should('have.length', 3);

        cy.get('#skr-fleet-fuel-group-95-86 .form-range').should('not.exist');
        cy.get('#skr-fleet-fuel-group-95-86').find('.fuel-ship-item').should('be.visible').should('have.length', 2);

        cy.get('#skr-fleet-fuel-group-95-86 .collapser').should('be.visible').click();
        cy.get('#skr-fleet-fuel-group-95-86').find('.fuel-ship-item').should('not.be.visible');

        cy.wait(500);

        cy.get('#skr-fleet-fuel-group-95-86 .collapser').should('be.visible').click();
        cy.get('#skr-fleet-fuel-group-95-86').find('.fuel-ship-item').should('be.visible');

        cy.get('#skr-fleet-fuel-group-95-86 .fleet-group-head-label a').should('be.visible').should('have.text', 'X: 95 Y: 86').click();
        cy.get('#skr-ingame-ship-selection').should('be.visible');
    });

    it('Should open ship', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=90#fleet=14;fuel');

        cy.get('#skr-ingame-fleet-fuel').should('be.visible').find('.skr-fleet-fuel-group').should('have.length', 3);

        cy.get('#skr-fleet-fuel-group-123-50 .form-range')
            .should('be.visible')
            .should('have.value', '71')
            .should('have.attr', 'max', '76');
        cy.get('#skr-fleet-fuel-group-123-50 .btn-primary').should('be.visible').should('have.text', 'Apply');
        cy.get('#skr-fleet-fuel-group-123-50').find('.fuel-ship-item').should('have.length', 2);

        cy.get('#fuel-ship-item-262 img').should('be.visible').click();

        cy.get('#skr-ship-stats-details-wrapper').should('be.visible');
    });

    it('Should equalize across ships', () => {
        loginHelper.login('/login');

        interceptShipOverview();

        cy.visit('/ingame/game?id=90#fleet=14;fuel');

        cy.get('#skr-fleet-fuel-group-95-86 .fleet-group-head .skr-icon-label-value').should('be.visible').should('have.text', '53%');
        cy.get('#fuel-ship-item-264 .skr-icon-label-value').should('be.visible').should('have.text', '285 / 300 (95%)');
        cy.get('#fuel-ship-item-263 .skr-icon-label-value').should('be.visible').should('have.text', '955 / 2000 (47%)');

        cy.get('#skr-fleet-fuel-group-95-86 .btn-primary').should('be.visible').click();

        cy.get('#skr-fleet-fuel-group-95-86 .fleet-group-head .skr-icon-label-value').should('be.visible').should('have.text', '52%');
        cy.get('#fuel-ship-item-264 .skr-icon-label-value').should('be.visible').should('have.text', '159 / 300 (53%)');
        cy.get('#fuel-ship-item-263 .skr-icon-label-value').should('be.visible').should('have.text', '1060 / 2000 (53%)');
        cy.get('#skr-fleet-fuel-group-95-86 .btn-primary').should('not.exist')

        waitForShipOverview();
    });

    it('Should fill up to maximum and remember collapse state', () => {
        loginHelper.login('/login');

        interceptShipOverview();

        cy.visit('/ingame/game?id=90#fleet=14;fuel');

        cy.get('#skr-range-slider-value-display-123-50').should('be.visible').should('have.text', '71%');
        cy.get('#fuel-ship-item-262 .skr-icon-label-value').should('be.visible').should('have.text', '334 / 350 (95%)');
        cy.get('#fuel-ship-item-261 .skr-icon-label-value').should('be.visible').should('have.text', '100 / 260 (38%)');

        cy.get('#skr-fleet-fuel-group-123-50 .form-range').should('be.visible')
            .invoke('val', 76)
            .trigger('input');
        cy.wait(500);

        cy.get('#skr-fleet-fuel-group-123-50 .collapser').should('be.visible').click();
        cy.get('#skr-fleet-fuel-group-123-50').find('.fuel-ship-item').should('not.be.visible');

        cy.get('#skr-range-slider-value-display-123-50').should('be.visible').should('have.text', '76%');
        cy.get('#skr-fleet-fuel-group-123-50 .btn-primary').should('be.visible').click();

        cy.get('#skr-range-slider-value-display-123-50').should('be.visible').should('have.text', '76%');

        cy.wait(500);
        cy.get('#skr-fleet-fuel-group-123-50').find('.fuel-ship-item').should('not.be.visible');

        cy.get('#skr-fleet-fuel-group-123-50 .collapser').should('be.visible').click();
        cy.get('#fuel-ship-item-262 .skr-icon-label-value').should('be.visible').should('have.text', '267 / 350 (76%)');
        cy.get('#fuel-ship-item-261 .skr-icon-label-value').should('be.visible').should('have.text', '197 / 260 (75%)');

        waitForShipOverview();
    });

    it('Should fill to max on planet without fuel', () => {
        loginHelper.login('/login');

        interceptShipOverview();

        cy.visit('/ingame/game?id=91#fleet=15;fuel');

        cy.get('#skr-fleet-fuel-group-129-127 .form-range').should('be.visible')
            .invoke('val', 57)
            .trigger('input');
        cy.wait(500);

        cy.get('#skr-range-slider-value-display-129-127').should('be.visible').should('have.text', '57%');
        cy.get('#skr-fleet-fuel-group-129-127 .btn-primary').should('be.visible').click();

        cy.get('#fuel-ship-item-268 .skr-icon-label-value').should('be.visible').should('have.text', '80 / 140 (57%)');
        cy.get('#fuel-ship-item-272 .skr-icon-label-value').should('be.visible').should('have.text', '80 / 140 (57%)');
        cy.get('#fuel-ship-item-271 .skr-icon-label-value').should('be.visible').should('have.text', '80 / 140 (57%)');
        cy.get('#fuel-ship-item-270 .skr-icon-label-value').should('be.visible').should('have.text', '80 / 140 (57%)');
        cy.get('#fuel-ship-item-269 .skr-icon-label-value').should('be.visible').should('have.text', '80 / 140 (57%)');
        cy.get('#fuel-ship-item-267 .skr-icon-label-value').should('be.visible').should('have.text', '80 / 140 (57%)');

        waitForShipOverview();
    });

    it('Should fill to max on planet with fuel', () => {
        loginHelper.login('/login');

        interceptShipOverview();

        cy.visit('/ingame/game?id=92#fleet=16;fuel');

        cy.get('#skr-fleet-fuel-group-200-145 .form-range').should('be.visible')
            .invoke('val', 10)
            .trigger('input');
        cy.wait(500);

        cy.get('#skr-range-slider-value-display-200-145').should('be.visible').should('have.text', '10%');
        cy.get('#skr-fleet-fuel-group-200-145 .btn-primary').should('be.visible').click();

        cy.get('#fuel-ship-item-273 .skr-icon-label-value').should('be.visible').should('have.text', '51 / 510 (10%)');
        cy.get('#fuel-ship-item-275 .skr-icon-label-value').should('be.visible').should('have.text', '51 / 510 (10%)');
        cy.get('#fuel-ship-item-274 .skr-icon-label-value').should('be.visible').should('have.text', '51 / 510 (10%)');

        waitForShipOverview();

        cy.intercept('/ingame/ship-overview').as('shipOverview2');

        cy.get('#skr-fleet-fuel-group-200-145 .form-range').should('be.visible')
            .invoke('val', 22)
            .trigger('input');
        cy.wait(500);

        cy.get('#skr-range-slider-value-display-200-145').should('be.visible').should('have.text', '22%');
        cy.get('#skr-fleet-fuel-group-200-145 .btn-primary').should('be.visible').click();

        cy.get('#fuel-ship-item-273 .skr-icon-label-value').should('be.visible').should('have.text', '113 / 510 (22%)');
        cy.get('#fuel-ship-item-275 .skr-icon-label-value').should('be.visible').should('have.text', '113 / 510 (22%)');
        cy.get('#fuel-ship-item-274 .skr-icon-label-value').should('be.visible').should('have.text', '113 / 510 (22%)');

        cy.wait('@shipOverview2');
        cy.wait(100);
    });

    it('Should fill to capacity on planet with a lot of fuel', () => {
        loginHelper.login('/login');

        interceptShipOverview();

        cy.visit('/ingame/game?id=93#fleet=17;fuel');

        cy.get('#skr-range-slider-value-display-200-153').should('be.visible').should('have.text', '0%');

        cy.get('#skr-fleet-fuel-group-200-153 .form-range').should('be.visible')
            .invoke('val', 100)
            .trigger('input');
        cy.wait(500);

        cy.get('#skr-range-slider-value-display-200-153').should('be.visible').should('have.text', '100%');
        cy.get('#skr-fleet-fuel-group-200-153 .btn-primary').should('be.visible').click();

        cy.get('#skr-range-slider-value-display-200-153').should('be.visible').should('have.text', '100%');
        cy.get('#fuel-ship-item-277 .skr-icon-label-value').should('be.visible').should('have.text', '310 / 310 (100%)');
        cy.get('#fuel-ship-item-276 .skr-icon-label-value').should('be.visible').should('have.text', '310 / 310 (100%)');

        waitForShipOverview();
    });

    it('Should equalize ship fuels with different ship types', () => {
        loginHelper.login('/login');

        interceptShipOverview();

        cy.visit('/ingame/game?id=93#fleet=18;fuel');

        cy.get('#skr-fleet-fuel-group-119-163 .fleet-group-head .skr-icon-label-value').should('be.visible').should('have.text', '80%');

        cy.get('#skr-fleet-fuel-group-119-163 .btn-primary').should('be.visible').click();
        cy.wait(500);

        cy.get('#skr-fleet-fuel-group-119-163 .btn-primary').should('not.exist')
        cy.get('#skr-fleet-fuel-group-119-163 .fleet-group-head .skr-icon-label-value').should('be.visible').should('have.text', '80%');

        cy.get('#fuel-ship-item-278 .skr-icon-label-value').should('be.visible').should('have.text', '960 / 1200 (80%)');
        cy.get('#fuel-ship-item-280 .skr-icon-label-value').should('be.visible').should('have.text', '176 / 220 (80%)');
        cy.get('#fuel-ship-item-279 .skr-icon-label-value').should('be.visible').should('have.text', '640 / 800 (80%)');

        waitForShipOverview();
    });

    it('Should fill tanks to close to capacity', () => {
        loginHelper.login('/login');

        interceptShipOverview();

        cy.visit('/ingame/game?id=93#fleet=19;fuel');

        cy.get('#skr-range-slider-value-display-150-100').should('be.visible').should('have.text', '93%');

        cy.get('#skr-fleet-fuel-group-150-100 .form-range').should('be.visible')
            .invoke('val', 99)
            .trigger('input');
        cy.wait(500);

        cy.get('#skr-fleet-fuel-group-150-100 .btn-primary').should('be.visible').click();
        cy.wait(500);

        cy.get('#skr-range-slider-value-display-150-100').should('be.visible').should('have.text', '99%');

        cy.get('#fuel-ship-item-281 .skr-icon-label-value').should('be.visible').should('have.text', '149 / 150 (99%)');
        cy.get('#fuel-ship-item-282 .skr-icon-label-value').should('be.visible').should('have.text', '148 / 150 (98%)');

        waitForShipOverview();
    });
});
