import {loginHelper} from '../../../support/login-helpers';

describe('Fleet Creation Tests', () => {
    function createFleet(name: string) {
        cy.get('#skr-ingame-create-fleet-modal #skr-ingame-new-fleet-name-input').should('be.visible').type(name);
        cy.get('#skr-ingame-create-fleet-modal-button').should('be.visible').click();
    }

    it('Should create fleet via overview modal', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=82');

        cy.get('#skr-ingame-fleets-button').should('be.visible').click();
        cy.get('#skr-fleet-overview-create-fleet-button').should('be.visible').click();

        createFleet('k');

        cy.get('#skr-ingame-fleet-overview-table-content').find('.skr-table-row').should('have.length', 1).click();

        cy.get('#skr-ingame-selection-panel-tab-options').should('be.visible').click();
        cy.get('#skr-game-options-content .btn-danger').should('be.visible').click();

        cy.get('#skr-ingame-delete-fleet-modal').should('be.visible').find('#skr-fleet-confirm-delete-button').click();
        cy.get('#skr-game-selection').children().should('have.length', 0);

        cy.get('#skr-ingame-fleets-button').should('be.visible').click();
        cy.get('#skr-ingame-create-fleet-modal').find('.skr-table-row').should('have.length', 0);
    });

    it('Should create fleet via ship construction', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=82#starbase=114');

        cy.get('.skr-ingame-starbase-select-button[data1=nightmare_3]').should('be.visible').click();
        cy.get('.skr-ingame-starbase-select-button[data1=nightmare_3]').should('not.be.visible');
        cy.get('#skr-starbase-create-fleet-button').should('be.visible').click();

        createFleet('l');

        cy.get('#skr-starbase-add-directly-to-fleet').should('be.visible');
        cy.get('#skr-starbase-add-directly-to-fleet option:selected').should('have.text', 'l');

        cy.wait(500);
        cy.get('#skr-starbase-shipconstruction-build-button').should('be.visible').click();
        cy.get('#skr-starbase-shipconstruction-success-message').should('be.visible').should('have.text', 'Ship construction job submitted successfully.');

        cy.get('#skr-ingame-finish-turn-button').should('be.visible').click();

        cy.get('#skr-ingame-overview-modal').should('be.visible').find('#skr-close-overview-button').click();

        cy.get('#skr-ingame-ships-button').should('be.visible').click();
        cy.get('#skr-ingame-ship-overview-table-content').should('be.visible').find('.skr-table-row').should('have.length', 1).click();
        cy.get('.skr-ship-open-fleet-button span').should('be.visible').should('have.text', 'Fleet: l');
    });
});
