import {loginHelper} from '../../../support/login-helpers';
import {ingameHelper} from "../../../support/ingame-helper";

describe('Ship Fleet Leader Tests', () => {
    it('Should allow fleet leader assignment in ship page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=86#ship=245');

        cy.get('.skr-ship-headline-fleet-button-group .dropdown-toggle-split').should('be.visible').click();
        cy.get('.ship-stats-fleet-selection-menu').should('be.visible')
            .find('.assign-as-fleet-leader-button').should('not.exist');

        ingameHelper.openShip(246);

        cy.wait(500);

        cy.get('.skr-ship-headline-fleet-button-group .dropdown-toggle-split').should('be.visible').click();

        cy.get('.ship-stats-fleet-selection-menu').should('be.visible')
            .find('.assign-as-fleet-leader-button')
            .should('be.visible')
            .should('have.text', 'Assign as fleet leader').click();

        cy.get('#skr-ship-task-string-label').should('be.visible').should('have.text', 'None');

        cy.get('.skr-ship-headline-fleet-button-group .dropdown-toggle-split').should('be.visible').click();
        cy.get('.ship-stats-fleet-selection-menu').should('be.visible')
            .find('.assign-as-fleet-leader-button').should('not.exist');
    });
});
