import {ingameHelper} from '../../../support/ingame-helper';
import {loginHelper} from '../../../support/login-helpers';

describe('Ship Routes Page Tests', () => {
    function verifyOrder(expectedOrder) {
        const ids = [];
        cy.get('.skr-route-delete-entry-button')
            .each(elem => ids.push(elem.attr('id')))
            .then(() => expect(ids).to.deep.equal(expectedOrder));
    }

    it('Should add two route entries to ship', () => {
        loginHelper.login('/ingame/game?id=79');

        ingameHelper.openShip(229);

        cy.get('#skr-ingame-selection-panel-tab-route').should('be.visible').click();
        cy.get('#skr-ingame-route-content').should('be.visible');
        cy.get('#skr-ingame-route-empty-message').should('be.visible');
        cy.get('#skr-ingame-route-requires-two-message').should('not.exist');

        cy.get('#skr-galaxy-map-planet-1960').find('.owned').should('be.visible').rightclick();
        cy.get('#route-entry-planet-select-0').should('have.value', '1960');
        cy.get('#skr-ingame-route-add-take').should('be.visible').click();

        cy.wait(500);

        cy.get('#skr-ingame-route-content').find('.skr-ingame-route-entry').should('have.length', 1);
        cy.get('#skr-ingame-route-empty-message').should('not.exist');
        cy.get('#skr-ingame-route-requires-two-message').should('be.visible');

        cy.get('#skr-galaxy-map-planet-1964').find('.owned').should('be.visible').rightclick();
        cy.get('#route-entry-planet-select-0').should('have.value', '1964');
        cy.get('#skr-ingame-route-add-leave').should('be.visible').click();

        cy.wait(500);

        cy.get('#skr-ingame-route-content').find('.skr-ingame-route-entry').should('have.length', 2);
        cy.get('#skr-ingame-route-empty-message').should('not.exist');
        cy.get('#skr-ingame-route-requires-two-message').should('not.exist');

        cy.get('#skr-ingame-route-content #skr-ship-courseselect-speed-select').should('be.visible').should('have.value', '7');
        cy.get('#skr-ingame-route-min-fuel-input').should('be.visible').should('have.value', '30');
    });

    it('Should remove route entries', () => {
        loginHelper.login('/ingame/game?id=79');

        ingameHelper.openShip(228);

        cy.get('#skr-ingame-selection-panel-tab-route').should('be.visible').click();
        cy.get('#skr-ingame-route-content').should('be.visible').find('.skr-ingame-route-button-column').should('have.length', 3);

        cy.get('#skr-route-remove-entry-23').should('be.visible').click();
        cy.wait(500);
        cy.get('#skr-ingame-route-content').should('be.visible').find('.skr-ingame-route-button-column').should('have.length', 2);
        cy.get('#skr-ingame-route-requires-two-message').should('not.exist');

        cy.get('#skr-route-remove-entry-22').should('be.visible').click();
        cy.wait(500);
        cy.get('#skr-ingame-route-content').should('be.visible').find('.skr-ingame-route-button-column').should('have.length', 1);
        cy.get('#skr-ingame-route-requires-two-message').should('be.visible');
    });

    it('Should move route entries', () => {
        loginHelper.login('/ingame/game?id=79');

        ingameHelper.openShip(230);

        cy.get('#skr-ingame-selection-panel-tab-route').should('be.visible').click();

        cy.get('#skr-route-move-entry-up-25').should('be.visible').click();
        cy.wait(500);
        verifyOrder(['skr-route-remove-entry-25', 'skr-route-remove-entry-24', 'skr-route-remove-entry-26']);

        cy.get('#skr-route-move-entry-up-25').should('be.visible').click();
        cy.wait(500);
        verifyOrder(['skr-route-remove-entry-25', 'skr-route-remove-entry-24', 'skr-route-remove-entry-26']);

        cy.get('#skr-route-move-entry-down-24').should('be.visible').click();
        cy.wait(500);
        verifyOrder(['skr-route-remove-entry-25', 'skr-route-remove-entry-26', 'skr-route-remove-entry-24']);

        cy.get('#skr-route-move-entry-down-24').should('be.visible').click();
        cy.wait(500);
        verifyOrder(['skr-route-remove-entry-25', 'skr-route-remove-entry-26', 'skr-route-remove-entry-24']);
    });

    it('Should change route entry resources', () => {
        loginHelper.login('/ingame/game?id=79');

        ingameHelper.openShip(230);

        cy.get('#skr-ingame-selection-panel-tab-route').should('be.visible').click();
        cy.get('#skr-ingame-route-action-select-MONEY-24').should('be.visible').select('IGNORE');

        cy.wait(500);

        cy.get('#skr-ingame-selection-panel-tab-transporter').should('be.visible').click();
        cy.get('#skr-game-ship-transporter').should('be.visible');

        cy.get('#skr-ingame-selection-panel-tab-route').should('be.visible').click();
        cy.get('#skr-ingame-route-action-select-MONEY-24').should('be.visible').should('have.value', 'IGNORE');
    });

    it('Should change route entry planet', () => {
        loginHelper.login('/ingame/game?id=79');

        ingameHelper.openShip(230);

        cy.get('#skr-ingame-selection-panel-tab-route').should('be.visible').click();
        cy.get('#route-entry-planet-select-24').should('be.visible').select('1964');

        cy.wait(500);

        cy.get('#skr-ingame-selection-panel-tab-transporter').should('be.visible').click();
        cy.get('#skr-game-ship-transporter').should('be.visible');

        cy.get('#skr-ingame-selection-panel-tab-route').should('be.visible').click();
        cy.get('#route-entry-planet-select-24').should('be.visible').should('have.value', '1964');
    });

    it('Should toggle wait for full storage', () => {
        loginHelper.login('/ingame/game?id=79');

        ingameHelper.openShip(230);

        cy.get('#skr-ingame-selection-panel-tab-route').should('be.visible').click();
        cy.get('#skr-ingame-route-wait-for-full-storage-checkbox-24').should('be.visible').click();

        cy.wait(500);

        cy.get('#skr-ingame-selection-panel-tab-transporter').should('be.visible').click();
        cy.get('#skr-game-ship-transporter').should('be.visible');

        cy.get('#skr-ingame-selection-panel-tab-route').should('be.visible').click();
        cy.get('#skr-ingame-route-wait-for-full-storage-checkbox-24').should('be.visible').should('have.attr', 'checked');
    });

    it('Should change route travel speed', () => {
        loginHelper.login('/ingame/game?id=79');

        ingameHelper.openShip(230);

        cy.get('#skr-ingame-selection-panel-tab-route').should('be.visible').click();
        cy.get('#skr-ingame-route-content #skr-ship-courseselect-speed-select').should('be.visible').should('have.value', '7').select('6');

        cy.wait(500);

        cy.get('#skr-ingame-selection-panel-tab-orders').should('be.visible').click();
        cy.get('#skr-ingame-navigation-content #skr-ship-courseselect-speed-select').should('be.visible').should('have.value', '6');

        cy.get('#skr-ingame-selection-panel-tab-route').should('be.visible').click();
        cy.get('#skr-ingame-route-content #skr-ship-courseselect-speed-select').should('be.visible').should('have.value', '6');
    });

    it('Should disable and re-enable route', () => {
        loginHelper.login('/ingame/game?id=79');

        ingameHelper.openShip(230);

        cy.get('#skr-ingame-selection-panel-tab-route').should('be.visible').click();
        cy.get('#skr-ingame-route-disabled-select').should('be.visible').should('have.value', 'false').select('true');
        cy.wait(500);

        cy.get('#skr-ingame-selection-panel-tab-orders').should('be.visible').click();
        cy.get('#skr-ship-task-string-label').should('be.visible').should('have.text', 'None');

        cy.get('#skr-ship-courseselect-resume-route').should('be.visible').click();
        cy.wait(500);
        cy.get('#skr-ship-task-string-label').should('be.visible').should('have.text', 'Routing');
        cy.get('#skr-ship-courseselect-resume-route').should('not.be.visible')

        cy.get('#skr-ingame-selection-panel-tab-route').should('be.visible').click();
        cy.get('#skr-ingame-route-disabled-select').should('be.visible').should('have.value', 'false');
    });

    it('Should change min fuel', () => {
        loginHelper.login('/ingame/game?id=79');

        ingameHelper.openShip(230);

        cy.get('#skr-ingame-selection-panel-tab-route').should('be.visible').click();
        cy.get('#skr-ingame-route-min-fuel-input').should('be.visible').clear().type('42{enter}');
        cy.wait(500);

        cy.get('#skr-ingame-selection-panel-tab-orders').should('be.visible').click();
        cy.wait(500);

        cy.get('#skr-ingame-selection-panel-tab-route').should('be.visible').click();

        cy.get('#skr-ingame-route-min-fuel-input').should('be.visible').should('have.value', '42').clear().type('30{enter}');
    });

    it('Should change primary resource', () => {
        loginHelper.login('/ingame/game?id=79');

        ingameHelper.openShip(230);

        cy.get('#skr-ingame-selection-panel-tab-route').should('be.visible').click();
        cy.get('#skr-ingame-route-primary-resource-select').should('be.visible').select('MINERAL1');
        cy.wait(500);

        cy.get('#skr-ingame-selection-panel-tab-orders').should('be.visible').click();
        cy.wait(500);

        cy.get('#skr-ingame-selection-panel-tab-route').should('be.visible').click();

        cy.get('#skr-ingame-route-primary-resource-select').should('be.visible').should('have.value', 'MINERAL1');
    });
});
