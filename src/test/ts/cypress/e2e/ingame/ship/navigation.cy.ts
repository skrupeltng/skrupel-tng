import {loginHelper} from '../../../support/login-helpers';
import {ingameHelper} from "../../../support/ingame-helper";

describe('Ship Navigation Tests', () => {
    const init = (shipId: number) => {
        loginHelper.login('/login');
        cy.visit(`/ingame/game?id=76#ship=${shipId}`);
        cy.get('#skr-ship-courseselect-speed-select').should('be.visible');
        cy.get('.owned.colonized.skr-game-planet-mouse').should('be.visible');
        cy.get('#skr-game-selection').should('have.css', 'width').and('eq', '550px');

        cy.wait(500);
    };

    const verifyCourse = (shipId: number, taskText: string, x: number, y: number, dist: number, turns: number, targetName: string, fuelConsumption: number) => {
        let expectedTurnText = `${turns} turn`;

        if (turns !== 1) {
            expectedTurnText += 's';
        }

        cy.get('#skr-ship-task-string-label').should('have.text', taskText);
        cy.get('#skr-ship-courseselect-coordinates').should('have.text', `${x} / ${y}`);
        cy.get('#skr-ship-courseselect-distance').should('have.text', `${dist} light year(s)`);
        cy.get('#skr-ship-courseselect-duration').should('have.text', expectedTurnText);
        cy.get('#skr-ship-courseselect-name').should('have.text', targetName);
        cy.get('#skr-ship-courseselect-speed-select').should('have.value', '9');
        cy.get('#skr-ship-courseselect-fuelconsumption').should('have.text', fuelConsumption);

        cy.get('#skr-ship-courseselect-delete-course').should('be.visible');

        const id = `#skr-game-ship-${shipId}`;
        cy.get(id).find('.skr-ingame-travel-line').should('exist');
        cy.get(id).find('.ship-destination-marker').should('exist');
    };

    it('Should set course to empty space', () => {
        const shipId = 197;
        init(shipId);

        cy.get('#skr-game-galaxy').rightclick();

        verifyCourse(shipId, 'Astro-physics lab, Travels to 125, 125', 125, 125, 94, 2, 'Empty space', 18);
    });

    it('Should set course to single scanned ship in empty space', () => {
        const shipId = 199;
        init(shipId);

        cy.get('#skr-ingame-shipicon-214').find('.ship-in-space').rightclick();

        verifyCourse(shipId, 'None, Intercepts alien ship', 164, 87, 88, 2, 'Doays_9', 104);
    });

    it('Should set course to single scanned ship on planet', () => {
        const shipId = 202;
        init(shipId);

        cy.get('#skr-galaxy-map-planet-1940').find('.skr-game-ship-mouse-planet').rightclick({force: true});

        verifyCourse(shipId, 'None, Intercepts alien ship', 178, 118, 61, 1, 'Doays_4', 12);
    });

    it('Should set course to scanned ship on planet where multiple ships are', () => {
        const shipId = 204;
        init(shipId);

        cy.get('#skr-galaxy-map-planet-1937').find('.ships-around-planet').rightclick({force: true});
        cy.get('#skr-ingame-ship-mouseover-ship-id-217').should('be.visible').rightclick();

        verifyCourse(shipId, 'None, Intercepts alien ship', 172, 50, 126, 2, 'Doays_10', 28);
    });

    it('Should set course to scanned ship in empty space where multiple ships are', () => {
        const shipId = 206;
        init(shipId);

        cy.get('.skr-game-ship-cluster-mouse[coordid=128_91]').find('.ship-in-space').rightclick();
        cy.get('#skr-ingame-ship-mouseover-ship-id-213').should('be.visible').rightclick();

        verifyCourse(shipId, 'None, Intercepts alien ship', 128, 91, 88, 2, 'Doays_8', 24);
    });

    it('Should set course to unknown ship in empty space', () => {
        const shipId = 207;
        init(shipId);

        cy.get('#skr-ingame-shipicon-208').find('.ship-in-space').rightclick();

        verifyCourse(shipId, 'None, Intercepts alien ship', 100, 103, 91, 2, 'Alien ship', 20);
    });

    it('Should set course to unknown ship on foreign planet', () => {
        const shipId = 209;
        init(shipId);

        cy.get('#skr-galaxy-map-planet-1938').find('.skr-game-ship-mouse-planet').rightclick({force: true});

        verifyCourse(shipId, 'None, Intercepts alien ship', 97, 194, 61, 1, 'Alien ship', 5);
    });

    it('Should set course to unknown foreign planet with starbase', () => {
        const shipId = 210;
        init(shipId);

        cy.get('#skr-galaxy-map-planet-1939').find('.colonized').rightclick({force: true});

        verifyCourse(shipId, 'None, Travels to Vailiv', 90, 69, 124, 2, 'Vailiv', 132);
    });

    it('Should set course to own ship in empty space', () => {
        const shipId = 212;
        init(shipId);

        cy.get('#skr-ingame-shipicon-197').find('.owned-ship').rightclick();

        verifyCourse(shipId, 'None, Escorts Xirclarod_1', 219, 125, 81, 1, 'Xirclarod_1', 2);
    });

    it('Should set course to unknown uninhabited planet and disable route', () => {
        const shipId = 215;
        init(shipId);

        cy.get('#skr-galaxy-map-planet-1938').find('.skr-game-planet-mouse').rightclick();

        verifyCourse(shipId, 'None, Travels to Chacarro', 97, 194, 61, 1, 'Chacarro', 1);

        cy.get('#skr-ship-courseselect-resume-route').should('be.visible');
    });

    it('Should set course to inhabited unscanned planet and refresh fleet', () => {
        const shipId = 218;
        init(shipId);

        cy.get('#skr-galaxy-map-planet-1941').find('.colonized').rightclick();

        verifyCourse(shipId, 'None, Travels to Gnov 4XF', 79, 123, 92, 1, 'Gnov 4XF', 2);

        const fleetShipId = 216;
        ingameHelper.openShip(fleetShipId)
        verifyCourse(fleetShipId, 'None, Follows Fleet Leader', 155, 175, 0, 0, 'Sporiin Scout_1', 0);

        cy.get('#skr-ship-courseselect-follow-leader').should('not.be.visible');
    });
});
