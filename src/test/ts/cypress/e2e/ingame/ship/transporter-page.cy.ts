import {ingameHelper} from '../../../support/ingame-helper';
import {loginHelper} from '../../../support/login-helpers';

describe('Ship Transporter Page Tests', () => {
    it('Should open planet when clicking on planet image in ship transporter view', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71');

        ingameHelper.openShip(183);

        cy.get('#skr-ingame-selection-panel-tab-transporter').should('be.visible').click();

        cy.get('#skr-ship-transporter-target-image').should('be.visible').should('have.class', 'cursor-pointer').click();

        cy.url().should('contain', 'ingame/game?id=71#planet=1896');
        cy.get('#skr-planet-selection').should('be.visible');
    });

    it('Should not open planet when clicking on planet image in ship transporter view because not own planet', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=56');

        ingameHelper.openShip(146);

        cy.get('#skr-ingame-selection-panel-tab-transporter').should('be.visible').click();

        cy.get('#skr-ship-transporter-target-image').should('be.visible').should('not.have.class', 'cursor-pointer').click();

        cy.url().should('contain', 'ingame/game?id=56#ship=146');
        cy.get('#skr-planet-selection').should('not.exist');
    });

    it('Should open ship when clicking on ship image in ship transporter view', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=84');

        ingameHelper.openShip(241);

        cy.get('#skr-ingame-selection-panel-tab-transporter').should('be.visible').click();

        cy.get('#skr-ship-transporter-target-select').should('be.visible').select('247');

        cy.get('#skr-ship-transporter-target-image')
            .should('be.visible')
            .should('have.class', 'cursor-pointer')
            .should('have.attr', 'src', '/factions/orion/ship_images/3.jpg')
            .click();

        cy.url().should('contain', 'ingame/game?id=84#ship=247');
        cy.get('#skr-ship-stats-details-wrapper').should('be.visible');
    });

    it('Should target switch to ship and back to planet', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=84#ship=247;transporter');

        cy.get('#skr-ship-transporter-target-select').should('be.visible').should('have.value', '2007').select('241');
        cy.get('#skr-game-ship-transporter-fuel-target').should('be.visible');

        cy.get('#skr-ship-transporter-target-select').should('be.visible').should('have.value', '241').select('2007');
        cy.get('#skr-game-ship-transporter-fuel-target').should('not.exist');
    });
});
