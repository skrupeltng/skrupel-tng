import {loginHelper} from '../../support/login-helpers';

describe('Ingame Story Mode Tests', () => {
    it('Should mark story mode step as viewed', () => {
        loginHelper.login('/login', 'userToBeInvited');

        cy.get('#skr-dashboard-story-mode-button').should('be.visible').click();
        cy.get('#skr-start-mission-button-silverstarag-1').should('be.visible').click();

        cy.url().should('contain', 'ingame/game?id=');

        cy.get('#skr-ingame-tutorial-welcome-modal-continue-button').should('be.visible').click();

        cy.get('#skr-ingame-story-mode-dismiss-step-button').should('be.visible').click();
        cy.get('#skr-story-mode-message-popup').should('not.be.visible');

        cy.get('#skr-planet-factories-build-factories').should('be.visible').click();
        cy.get('#skr-ingame-finish-turn-button').should('be.visible').click();

        cy.get('#skr-ingame-story-mode-dismiss-step-button').should('be.visible').click();
        cy.get('#skr-ingame-tutorial-welcome-modal-continue-button').should('not.exist');

        cy.wait(2500);

        cy.reload();

        cy.get('#skr-ingame-tutorial-welcome-modal-continue-button').should('not.exist');
        cy.get('#skr-story-mode-message-popup').should('not.exist');
    });
});
