import {loginHelper} from '../../../../support/login-helpers';

describe('Orbital System Removal Tests', () => {
    it('Should not remove orbital system because not enough resources', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=81#planet=1976;orbital-systems');

        cy.get('#skr-orbitalsystem-1 div').should('be.visible').should('have.text', 'Metropolis').click();

        cy.get('#skr-orbital-system-content-7453 #skr-orbital-system-tear-down-button').should('be.visible').should('have.attr', 'disabled');
    });

    it('Should remove orbital system', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=81#planet=1976;orbital-systems');

        cy.get('#skr-orbitalsystem-0 div').should('be.visible').should('have.text', 'Bank').click();

        cy.get('#skr-orbital-system-content-7452 #skr-orbital-system-tear-down-button').should('be.visible').click();

        cy.get('#skr-orbitalsystem-0 div').should('be.visible').should('have.text', 'Empty slot');
    });

    it('Should not remove orbital system because is orbital extension', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=81#planet=1976;orbital-systems');

        cy.get('#skr-orbitalsystem-3 div').should('be.visible').should('have.text', 'Orbital extension').click();

        cy.get('#skr-orbital-system-content-7455 #skr-orbital-system-tear-down-button').should('not.exist');
    });
});
