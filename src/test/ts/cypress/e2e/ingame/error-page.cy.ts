import {loginHelper} from '../../support/login-helpers';

describe('Ingame Error Page Tests', () => {
    it('Should show error page when ingame page does not exist', () => {
        loginHelper.login('/ingame/game?id=5464565');

        cy.get('#skr-error-page-message').should('be.visible');
    });
});
