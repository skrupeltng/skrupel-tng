import {loginHelper} from '../../support/login-helpers';

describe('Dashboard Error Page Tests', () => {
    it('Should show error page when dashboard page does not exist', () => {
        loginHelper.login('/game?id=5464565');

        cy.get('#skr-error-page-message').should('be.visible');
    });
});
