import {createNewGame} from '../../support/create-new-game';
import {existingGames} from '../../support/existing-games';
import {gameDetails} from '../../support/game-details';
import {loginHelper} from '../../support/login-helpers';

describe('Game Creation Tests', () => {
    function createFixedTeamsGame() {
        loginHelper.login('/login');
        existingGames.openCreateNewGame();

        createNewGame.createNewSimpleGame('fixed teams game');
        cy.get('#skr-game-tab-options').should('be.visible').click();

        createNewGame.changeWinCondition('DEATH_FOE');
        createNewGame.toggleFixedTeams();
        createNewGame.changePlayerCount(4);

        cy.get('#skr-game-mode-text').should('be.visible').should('have.text', 'Death foe with fixed teams');

        cy.get('#skr-save-game-button').should('be.visible').click();

        cy.wait(100);

        cy.get('#skr-game-tab-players').should('be.visible').click();
    }

    function addTeam(name: string) {
        cy.get('#skr-add-team-button').should('be.visible').click();
        cy.get('#add-team-name-input').should('be.visible').type(name);
        cy.get('#add-team-button').should('be.visible').click();

        cy.get(`.player-table-team-${name}`).should('be.visible');
    }

    it('Should Create And Delete Game', () => {
        loginHelper.login('/login');

        existingGames.openCreateNewGame();
        createNewGame.createNewSimpleGame('to be deleted');

        gameDetails.selectFaction(0, 'kuatoh');
        gameDetails.startGame();

        cy.url().then((url: string) => {
            const gameId = parseInt(url.split('=')[1]);
            cy.visit(`/game?id=${gameId}`);

            cy.get('#skr-game-tab-options').should('be.visible').click();
            cy.get('#skr-dashboard-show-game-delete-modal-button').should('be.visible').click();
            cy.get('#skr-game-confirm-delete-button').should('be.visible').click();

            cy.url().should('contain', '/my-games');
        });
    });

    it('Should add player to own team', () => {
        createFixedTeamsGame();
        gameDetails.invitePlayer('AI (easy)', 'Standard');
    });

    it('Should add players to new team', () => {
        createFixedTeamsGame();

        const newTeamName = 'new-team-01';
        addTeam(newTeamName);

        cy.get('.remove-team-button').should('be.visible');

        gameDetails.invitePlayer('AI (easy)', newTeamName);
        gameDetails.invitePlayer('AI (medium)', newTeamName);

        cy.get('.remove-team-button').should('not.exist');
    });

    it('Should move creator to new team', () => {
        createFixedTeamsGame();

        const newTeamName = 'new-team-02';
        addTeam(newTeamName);

        cy.get(`.player-table-team-${newTeamName} .join-this-team-button`).should('be.visible').click();
        cy.get(`.player-table-team-${newTeamName} .skr-player-name-label`).should('be.visible').should('have.text', 'admin');

        cy.get('.remove-team-button').should('be.visible').click();

        cy.get('.remove-team-button').should('not.exist');
    });

    it('Should rename team', () => {
        createFixedTeamsGame();

        cy.get('.rename-team-button').should('be.visible').click();
        cy.get('#rename-team-name-input').should('be.visible').clear().type('something else');
        cy.get('#rename-team-button').should('be.visible').click();

        cy.get(`.player-table-team-something-else .card-title`).should('be.visible').should('have.text', 'Team something else');
    });
});
