import {DEFAULT_PASSWORD} from '../../support/login-helpers';

describe('Init Test', () => {
    if (Cypress.env('RUN_INIT_TEST')) {
        it('Should init the application', () => {
            cy.visit('/');

            cy.url().should('contain', '/init-setup');

            cy.get('#skr-init-setup-email-warning').should('be.visible');

            cy.get('#username').should('be.visible').type('admin');
            cy.get('#contactEmail').should('be.visible').type('test@test.de');
            cy.get('#password').should('be.visible').type(DEFAULT_PASSWORD);
            cy.get('#domainUrl').should('be.visible').type('http://localhost:8080');
            cy.get('#dataPrivacyStatementEnglish').should('be.visible').type('data privacy en');
            cy.get('#dataPrivacyStatementGerman').should('be.visible').type('data privacy de');
            cy.get('#skr-init-setup-form-submit-button').should('be.visible').click();

            cy.url().should('contain', '/open-games');

            cy.get('#skr-dashboard-admin-menu-button').should('be.visible').click();
            cy.get('#skr-dashboard-admin-guest-account-code').should('be.visible').click();

            cy.url().should('contain', '/admin/guest-account-code');
            cy.get('#skr-admin-no-guest-code-generated-label').should('be.visible');
            cy.get('#skr-admin-guest-account-link-label').should('not.exist');
            cy.get('#skr-admin-generate-new-guest-account-code-button').should('be.visible').click();

            cy.get('#skr-admin-no-guest-code-generated-label').should('not.exist');
            cy.get('#skr-admin-guest-account-link-label').should('be.visible');
        });
    }
});
