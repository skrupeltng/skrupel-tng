import {loginHelper} from '../../support/login-helpers';

describe('Achievements Page Tests', () => {
    it('Should open the Achievements Page', () => {
        loginHelper.login('/login');

        cy.get('#skr-dashboard-account-menu-button').click();
        cy.get('#skr-dashboard-account-dropdown-menu').should('be.visible');
        cy.get('#skr-dashboard-achievements-link').click();

        cy.url().should('contain', '/achievements');

        cy.get('#skr-achievement-item-PLANETS_COLONIZED_1').should('have.class', 'earned');
    });
});
