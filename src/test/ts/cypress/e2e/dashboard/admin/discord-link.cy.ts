import {loginHelper} from '../../../support/login-helpers';

describe('Discord Link Admin Page Tests', () => {
    it('Should set and clear the Discord Link', () => {
        loginHelper.login('/login');

        cy.get('#skr-dashboard-admin-menu-button').should('be.visible').click();
        cy.get('#skr-dashboard-admin-discord-link').should('be.visible').click();

        cy.url().should('contain', '/admin/discord-link');

        cy.get('#discordLink').clear();
        cy.get('#discordLink').type('https://discord.com');
        cy.get('#skr-admin-save-discord-link-button').should('be.visible').click();

        cy.get('#skr-dashboard-discord-link').should('be.visible');
        cy.get('#discordLink').should('have.value', 'https://discord.com');

        cy.get('#discordLink').clear();
        cy.get('#skr-admin-save-discord-link-button').should('be.visible').click();

        cy.get('#skr-dashboard-discord-link').should('not.exist');
        cy.get('#discordLink').should('have.value', '');
    });
});
