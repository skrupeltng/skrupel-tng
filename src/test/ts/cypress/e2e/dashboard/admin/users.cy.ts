import {loginHelper} from '../../../support/login-helpers';

describe('Users Page Tests', () => {
    it('Should use Master Login and return to admin user again', () => {
        loginHelper.login('/login');

        cy.get('#skr-dashboard-admin-menu-button').should('be.visible').click();
        cy.get('#skr-dashboard-admin-user-overview-link').should('be.visible').click();

        cy.url().should('contain', '/admin/users');

        cy.get('#skr-admin-users-master-login-button-testuser01').should('be.visible').click();

        cy.url().should('contain', '/my-games');
        cy.get('#skr-dashboard-return-to-admin-user-button').should('be.visible').click();

        cy.get('#skr-dashboard-return-to-admin-user-button').should('not.exist');
        cy.get('#skr-dashboard-admin-menu-button').should('be.visible');
    });

    it('Should not mark overview and news as viewed when using Master Login', () => {
        loginHelper.login('/login');

        cy.get('#skr-dashboard-admin-menu-button').should('be.visible').click();
        cy.get('#skr-dashboard-admin-user-overview-link').should('be.visible').click();

        cy.url().should('contain', '/admin/users');

        cy.get('#skr-admin-users-master-login-button-workingTestUser').should('be.visible').click();

        cy.url().should('contain', '/my-games');
        cy.visit('/ingame/game?id=66');

        cy.get('#skr-ingame-overview-modal').should('be.visible');
        cy.get('#skr-ingame-overview-news-tab-badge').should('be.visible').should('have.text', '1').click();

        cy.reload();

        cy.get('#skr-ingame-overview-modal').should('be.visible');
        cy.get('#skr-ingame-overview-news-tab-badge').should('be.visible').should('have.text', '1');
    });

    it('Should delete user via users management view', () => {
        loginHelper.login('/admin/users');

        cy.get('#skr-admin-users-master-delete-button-userToBeDeletedByAdmin').should('be.visible').click();

        cy.get('#skr-admin-delete-user-modal-delete-button').should('be.visible').click();

        cy.wait(500);

        cy.get('#skr-admin-users-master-delete-button-userToBeDeletedByAdmin').should('not.exist')
        cy.url().should('contain', '/admin/users');
    });

    it('Should find all users', () => {
        loginHelper.login('/admin/users');

        cy.get('.skr-table').find('.skr-admin-users-username').should('have.length.greaterThan', 3);
    });

    it('Should filter users by match making', () => {
        loginHelper.login('/match-making/', 'workingTestUser');

        cy.get('#skr-match-making-active').should('be.visible').check();
        cy.get('#skr-match-making-save-button').click();
        cy.get('#skr-match-making-saved-info').should('be.visible');

        loginHelper.logoutFromDashboard();
        cy.clearCookies();

        loginHelper.login('/admin/users?matchMakingEnabled=true');

        cy.get('.skr-table').find('.skr-admin-users-username').should('have.length', 1);

        cy.visit('/admin/users?matchMakingEnabled=false');

        cy.get('.skr-table').find('.skr-admin-users-username').should('have.length.greaterThan', 3);

        // Cleanup: deactivate match making for workingTestUser again
        loginHelper.logoutFromDashboard();
        cy.clearCookies();
        loginHelper.login('/match-making/', 'workingTestUser');
        cy.get('#skr-match-making-active').should('be.visible').uncheck();
        cy.get('#skr-match-making-save-button').click();
    });
});
