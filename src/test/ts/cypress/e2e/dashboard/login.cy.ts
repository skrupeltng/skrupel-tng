import {loginHelper} from '../../support/login-helpers';

describe('Login Tests', () => {
    it('Should open the My-Games page after login', () => {
        loginHelper.login('/login');

        cy.url().should('contain', '/my-games');

        cy.get('#aiPlayers').should('be.visible');
    });
});
