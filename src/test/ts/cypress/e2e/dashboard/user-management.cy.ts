import {createNewGame} from '../../support/create-new-game';
import {existingGames} from '../../support/existing-games';
import {gameDetails} from '../../support/game-details';
import {loginHelper} from '../../support/login-helpers';

describe('User Management Tests', () => {
    it('Should Register And Delete User', () => {
        loginHelper.register('userToBeDeletedBySelf');

        cy.get('#skr-dashboard-account-menu-button').click();
        cy.get('#skr-dashboard-account-details-link').should('be.visible').click();

        cy.get('#skr-dashboard-delete-user-button').should('be.visible').click();
        cy.get('#delete-user-button').click();

        cy.get('#skr-landingpage-create-account-button').should('be.visible');
    });

    it('Should Invite User To Game', () => {
        loginHelper.login('/login');
        existingGames.openCreateNewGame();
        createNewGame.createNewSimpleGame('testWithHuman');
        gameDetails.selectFaction(0, 'kuatoh');
        gameDetails.invitePlayer('userToBeInvited');
        loginHelper.logoutFromDashboard();

        cy.clearCookies();

        loginHelper.login('/login', 'userToBeInvited');
        cy.url().should('contain', 'my-games');
        cy.get('.skr-select-faction-button').click();
        cy.url().should('contain', 'game?id=');
        gameDetails.selectFaction(0, 'orion');
    });

    it('Should Create Test Account And Create Game', () => {
        cy.visit('/guest-account/1746baf0-6429-4fde-a574-06fb2beb218f');
        cy.title().should('eq', 'Guest account - Skrupel TNG');
        cy.get('#skr-guest-account-creation-success-page').should('be.visible');

        existingGames.openCreateNewGame();
        createNewGame.createNewSimpleGame('guest account game');

        gameDetails.selectFaction(0, 'vantu');
        cy.get('#skr-game-guest-ai-info-text').should('be.visible');
        gameDetails.invitePlayer('AI (medium)');
        gameDetails.selectFaction(1, 'silverstarag');

        gameDetails.startGame();
    });
});
