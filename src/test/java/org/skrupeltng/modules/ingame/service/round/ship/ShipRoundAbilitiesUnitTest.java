package org.skrupeltng.modules.ingame.service.round.ship;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ImageConstants;
import org.skrupeltng.modules.dashboard.service.AchievementService;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFoldRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHole;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipRemoval;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipTransportService;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.service.StarbaseService;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.data.util.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ShipRoundAbilitiesUnitTest {

	@Spy
	@InjectMocks
	final ShipRoundAbilities subject = new ShipRoundAbilities();

	@Mock(lenient = true)
	ConfigProperties configProperties;

	@Mock
	NewsService newsService;

	@Mock
	OrbitalSystemRepository orbitalSystemRepository;

	@Mock(lenient = true)
	PlanetRepository planetRepository;

	@Mock
	PlayerPlanetScanLogRepository playerPlanetScanLogRepository;

	@Mock(lenient = true)
	PlayerRelationRepository playerRelationRepository;

	@Mock(lenient = true)
	ShipRepository shipRepository;

	@Mock(lenient = true)
	ShipService shipService;

	@Mock
	SpaceFoldRepository spaceFoldRepository;

	@Mock
	StarbaseService starbaseService;

	@Mock(lenient = true)
	WormHoleRepository wormHoleRepository;

	@Mock
	StatsUpdater statsUpdater;

	@Mock
	ShipRouteEntryRepository shipRouteEntryRepository;

	@Mock
	AchievementService achievementService;

	@Mock
	GameRepository gameRepository;

	@Mock
	PlayerRepository playerRepository;

	@Mock(lenient = true)
	ShipTransportService shipTransportService;

	@Mock
	ShipRemoval shipRemoval;

	@BeforeEach
	void setup() {
		MasterDataService.RANDOM.setSeed(1L);

		when(configProperties.getMinAnomalyDistance()).thenReturn(30);

		when(shipTransportService.transportWithoutPermissionCheck(any(), any(), any(Planet.class))).thenAnswer((Answer<Pair<Ship, Planet>>) invocation -> Pair.of(invocation.getArgument(1), invocation.getArgument(2)));
		when(shipRepository.save(any())).thenAnswer((Answer<Ship>) invocation -> invocation.getArgument(0));
		when(wormHoleRepository.save(any())).thenAnswer((Answer<WormHole>) invocation -> invocation.getArgument(0));
	}

	Ship createShipWithAbility(ShipAbilityType type, Map<String, String> values) {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		PropulsionSystemTemplate propulsionSystemTemplate = new PropulsionSystemTemplate();
		propulsionSystemTemplate.setTechLevel(5);
		ship.setPropulsionSystemTemplate(propulsionSystemTemplate);
		ShipAbility ability = new ShipAbility(type);
		ability.setValues(values);
		ship.getShipTemplate().setShipAbilities(new ArrayList<>(List.of(ability)));
		when(shipRepository.getShipsWithActiveAbility(1L, type)).thenReturn(new ArrayList<>(List.of(ship)));
		return ship;
	}

	Ship createSubParticleClusterShip() {
		Map<String, String> values = new HashMap<>(4);
		values.put(ShipAbilityConstants.SUB_PARTICLE_CLUSTER_SUPPLIES, "9");
		values.put(ShipAbilityConstants.SUB_PARTICLE_CLUSTER_MINERAL1, "1");
		values.put(ShipAbilityConstants.SUB_PARTICLE_CLUSTER_MINERAL2, "1");
		values.put(ShipAbilityConstants.SUB_PARTICLE_CLUSTER_MINERAL3, "1");
		Ship ship = createShipWithAbility(ShipAbilityType.SUB_PARTICLE_CLUSTER, values);
		ship.getShipTemplate().setStorageSpace(2700);
		ship.setMineral1(0);
		ship.setMineral2(0);
		ship.setMineral3(0);
		return ship;
	}

	Ship createQuarkReorganizerShip() {
		Map<String, String> values = new HashMap<>(4);
		values.put(ShipAbilityConstants.QUARK_REORGANIZER_SUPPLIES, "113");
		values.put(ShipAbilityConstants.QUARK_REORGANIZER_MINERAL1, "113");
		values.put(ShipAbilityConstants.QUARK_REORGANIZER_MINERAL2, "113");
		values.put(ShipAbilityConstants.QUARK_REORGANIZER_MINERAL3, "0");
		Ship ship = createShipWithAbility(ShipAbilityType.QUARK_REORGANIZER, values);
		ship.setMineral1(0);
		ship.setMineral2(0);
		ship.setMineral3(0);
		ship.getShipTemplate().setStorageSpace(1000);
		ship.getShipTemplate().setFuelCapacity(500);
		return ship;
	}

	Ship createJumpEngineShip() {
		Map<String, String> values = new HashMap<>(3);
		values.put(ShipAbilityConstants.JUMP_ENGINE_COSTS, "20");
		values.put(ShipAbilityConstants.JUMP_ENGINE_MIN, "500");
		values.put(ShipAbilityConstants.JUMP_ENGINE_MAX, "1000");
		return createShipWithAbility(ShipAbilityType.JUMP_ENGINE, values);
	}

	Ship createGravityWaveGeneratorShip() {
		Map<String, String> values = new HashMap<>(1);
		values.put(ShipAbilityConstants.GRAVITY_WAVE_GENERATOR_COSTS, "100");
		return createShipWithAbility(ShipAbilityType.GRAVITY_WAVE_GENERATOR, values);
	}

	Ship createJumpPortalShip() {
		Map<String, String> values = new HashMap<>(4);
		values.put(ShipAbilityConstants.JUMP_PORTAL_FUEL, "10");
		values.put(ShipAbilityConstants.JUMP_PORTAL_MINERAL1, "20");
		values.put(ShipAbilityConstants.JUMP_PORTAL_MINERAL2, "30");
		values.put(ShipAbilityConstants.JUMP_PORTAL_MINERAL3, "40");
		return createShipWithAbility(ShipAbilityType.JUMP_PORTAL, values);
	}

	Ship createDestabilizerShip() {
		Map<String, String> values = new HashMap<>(1);
		values.put(ShipAbilityConstants.DESTABILIZER_EFFICIENCY, "0");
		return createShipWithAbility(ShipAbilityType.DESTABILIZER, values);
	}

	@Test
	void shouldProcessSensors() {
		subject.processSensors(1L);

		verify(shipRepository).resetShipScanRadius(1L);
		verify(shipRepository).updateScanRadius(1L, 116, ShipAbilityType.ASTRO_PHYSICS_LAB);
		verify(shipRepository).updateScanRadius(1L, 85, ShipAbilityType.EXTENDED_SENSORS);
	}

	@Test
	void shouldNotAutoTransportWithSubParticleCluster() {
		Ship ship = createSubParticleClusterShip();

		subject.processSubParticleCluster(1L);

		verify(shipTransportService, never()).transportWithoutPermissionCheck(any(), any(), any(Planet.class));

		Planet planet = new Planet(1L);
		ship.setPlanet(planet);

		subject.processSubParticleCluster(1L);

		verify(shipTransportService, never()).transportWithoutPermissionCheck(any(), any(), any(Planet.class));
	}

	@Test
	void shouldAutoTransportWithSubParticleCluster() {
		Ship ship = createSubParticleClusterShip();
		ship.setTaskValue("true");

		Planet planet = new Planet(1L);
		ship.setPlanet(planet);

		subject.processSubParticleCluster(1L);

		ArgumentCaptor<ShipTransportRequest> requestArg = ArgumentCaptor.forClass(ShipTransportRequest.class);
		verify(shipTransportService).transportWithoutPermissionCheck(requestArg.capture(), eq(ship), eq(planet));

		ShipTransportRequest request = requestArg.getValue();
		assertEquals(0, request.getColonists());
		assertEquals(0, request.getFuel());
		assertEquals(0, request.getHeavyGroundUnits());
		assertEquals(0, request.getLightGroundUnits());
		assertEquals(0, request.getSupplies());
		assertEquals(0, request.getMineral1());
		assertEquals(0, request.getMineral2());
		assertEquals(0, request.getMineral3());
	}

	@Test
	void shouldCheckSuppliesWithSubParticleCluster() {
		Ship ship = createSubParticleClusterShip();
		subject.processSubParticleCluster(1L);
		verify(shipRepository, never()).save(ship);

		ship.setSupplies(99);
		subject.processSubParticleCluster(1L);
		verify(shipRepository).save(ship);

		assertEquals(0, ship.getSupplies());
		assertEquals(11, ship.getMineral1());
		assertEquals(11, ship.getMineral2());
		assertEquals(11, ship.getMineral3());
	}

	@Test
	void shouldCheckMaximumSuppliesWithSubParticleCluster() {
		Ship ship = createSubParticleClusterShip();

		ship.setSupplies(2600);
		subject.processSubParticleCluster(1L);
		verify(shipRepository).save(ship);

		assertEquals(17, ship.getSupplies());
		assertEquals(287, ship.getMineral1());
		assertEquals(287, ship.getMineral2());
		assertEquals(287, ship.getMineral3());
	}

	@Test
	void shouldCheckStorageCapacityWithSubParticleCluster() {
		Ship ship = createSubParticleClusterShip();
		ship.setMineral1(800);
		ship.setMineral2(800);
		ship.setMineral3(800);

		ship.setSupplies(270);
		subject.processSubParticleCluster(1L);
		verify(shipRepository).save(ship);

		assertEquals(0, ship.getSupplies());
		assertEquals(830, ship.getMineral1());
		assertEquals(830, ship.getMineral2());
		assertEquals(830, ship.getMineral3());
	}

	@Test
	void shouldConsumeFullSuppliesWithSubParticleCluster() {
		Ship ship = createSubParticleClusterShip();
		ship.setMineral1(0);
		ship.setMineral2(0);
		ship.setMineral3(0);
		ship.setSupplies(2700);

		subject.processSubParticleCluster(1L);
		verify(shipRepository).save(ship);

		assertEquals(117, ship.getSupplies());
		assertEquals(287, ship.getMineral1());
		assertEquals(287, ship.getMineral2());
		assertEquals(287, ship.getMineral3());
	}

	@Test
	void shouldCheckPlanetPresenceWithTerraFormer() {
		createShipWithAbility(ShipAbilityType.TERRA_FORMER_COLD, null);

		subject.processTerraformer(1L, ShipAbilityType.TERRA_FORMER_COLD);

		verify(planetRepository, never()).save(any());
	}

	@Test
	void shouldProcessColdTerraFormer() {
		Ship ship = createShipWithAbility(ShipAbilityType.TERRA_FORMER_COLD, null);
		Planet planet = new Planet();
		planet.setTemperature(40);
		ship.setPlanet(planet);

		subject.processTerraformer(1L, ShipAbilityType.TERRA_FORMER_COLD);

		verify(planetRepository).save(planet);
		assertEquals(39, planet.getTemperature());
	}

	@Test
	void shouldProcessWarmTerraFormer() {
		Ship ship = createShipWithAbility(ShipAbilityType.TERRA_FORMER_WARM, null);
		Planet planet = new Planet();
		planet.setTemperature(40);
		ship.setPlanet(planet);

		subject.processTerraformer(1L, ShipAbilityType.TERRA_FORMER_WARM);

		verify(planetRepository).save(planet);
		assertEquals(41, planet.getTemperature());
	}

	@Test
	void shouldNotAutoTransportWithQuarkReorganizer() {
		Ship ship = createQuarkReorganizerShip();

		subject.processQuarkReorganizer(1L);

		verify(shipTransportService, never()).transportWithoutPermissionCheck(any(), any(), any(Planet.class));

		Planet planet = new Planet(1L);
		ship.setPlanet(planet);

		subject.processQuarkReorganizer(1L);

		verify(shipTransportService, never()).transportWithoutPermissionCheck(any(), any(), any(Planet.class));
	}

	@Test
	void shouldAutoTransportWithQuarkReorganizer() {
		Ship ship = createQuarkReorganizerShip();
		ship.setTaskValue("true");

		Planet planet = new Planet(1L);
		ship.setPlanet(planet);

		subject.processQuarkReorganizer(1L);

		ArgumentCaptor<ShipTransportRequest> requestArg = ArgumentCaptor.forClass(ShipTransportRequest.class);
		verify(shipTransportService).transportWithoutPermissionCheck(requestArg.capture(), eq(ship), eq(planet));

		ShipTransportRequest request = requestArg.getValue();
		assertEquals(0, request.getColonists());
		assertEquals(0, request.getFuel());
		assertEquals(0, request.getHeavyGroundUnits());
		assertEquals(0, request.getLightGroundUnits());
		assertEquals(0, request.getSupplies());
		assertEquals(0, request.getMineral1());
		assertEquals(0, request.getMineral2());
		assertEquals(0, request.getMineral3());
	}

	@Test
	void shouldCheckSuppliesWithQuarkReorganizer() {
		Ship ship = createQuarkReorganizerShip();
		subject.processQuarkReorganizer(1L);
		verify(shipRepository, never()).save(ship);

		ship.setSupplies(113);
		ship.setMineral1(113);
		ship.setMineral2(113);
		subject.processQuarkReorganizer(1L);
		verify(shipRepository).save(ship);

		assertEquals(0, ship.getSupplies());
		assertEquals(0, ship.getMineral1());
		assertEquals(0, ship.getMineral2());
		assertEquals(113, ship.getFuel());
	}

	@Test
	void shouldCheckMaximumSuppliesWithQuarkReorganizer() {
		Ship ship = createQuarkReorganizerShip();

		ship.setSupplies(200);
		ship.setMineral1(200);
		ship.setMineral2(200);
		subject.processQuarkReorganizer(1L);
		verify(shipRepository).save(ship);

		assertEquals(87, ship.getSupplies());
		assertEquals(87, ship.getMineral1());
		assertEquals(87, ship.getMineral2());
		assertEquals(113, ship.getFuel());
	}

	@Test
	void shouldCheckFullTankWithQuarkReorganizer() {
		Ship ship = createQuarkReorganizerShip();

		ship.setSupplies(200);
		ship.setMineral1(200);
		ship.setMineral2(200);
		ship.setFuel(400);
		subject.processQuarkReorganizer(1L);
		verify(shipRepository).save(ship);

		assertEquals(100, ship.getSupplies());
		assertEquals(100, ship.getMineral1());
		assertEquals(100, ship.getMineral2());
		assertEquals(500, ship.getFuel());
	}

	@Test
	void shouldCheckFuelCostsWithJumpEngine() {
		Ship ship = createJumpEngineShip();

		subject.processJumpEngine(1L);
		verify(shipRepository, never()).save(any());
		verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_jump_engine_not_enough_fuel, ship.createFullImagePath(), ship.getId(),
			NewsEntryClickTargetType.ship, ship.getName());
	}

	@Test
	void shouldCorrectlyUseJumpEngine() {
		Ship ship = createJumpEngineShip();
		ship.setFuel(100);
		ship.setDestinationX(800);
		ship.setDestinationY(800);

		when(shipRepository.getShipsWithActiveAbility(1L, ShipAbilityType.GRAVITY_WAVE_GENERATOR)).thenReturn(List.of());
		when(planetRepository.findByGameIdAndXAndY(anyLong(), anyInt(), anyInt())).thenReturn(Optional.empty());

		subject.processJumpEngine(1L);

		assertSuccessfullJumpEngine(ship);
	}

	@Test
	void shouldCheckGravityWaveShipOwnerWithJumpEngine() {
		Ship ship = createJumpEngineShip();
		ship.setFuel(100);
		ship.setDestinationX(800);
		ship.setDestinationY(800);

		Ship gravityWaveShip = createShipWithAbility(ShipAbilityType.GRAVITY_WAVE_GENERATOR, null);
		gravityWaveShip.setPlayer(ship.getPlayer());
		when(shipRepository.getShipsWithActiveAbility(1L, ShipAbilityType.GRAVITY_WAVE_GENERATOR)).thenReturn(List.of(gravityWaveShip));
		when(planetRepository.findByGameIdAndXAndY(anyLong(), anyInt(), anyInt())).thenReturn(Optional.empty());

		subject.processJumpEngine(1L);

		assertSuccessfullJumpEngine(ship);
	}

	void assertSuccessfullJumpEngine(Ship ship) {
		assertEquals(649, ship.getX());
		assertEquals(649, ship.getY());

		verify(shipRepository).save(ship);
		verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_jump_engine_success, ship.createFullImagePath(), ship.getId(),
			NewsEntryClickTargetType.ship, ship.getName(), 919);
	}

	@Test
	void shouldCheckGravityWaveShipRelationWithJumpEngine() {
		Ship ship = createJumpEngineShip();
		ship.setFuel(100);
		ship.setDestinationX(800);
		ship.setDestinationY(800);

		Ship gravityWaveShip = createShipWithAbility(ShipAbilityType.GRAVITY_WAVE_GENERATOR, null);
		gravityWaveShip.getPlayer().setId(2L);
		when(shipRepository.getShipsWithActiveAbility(1L, ShipAbilityType.GRAVITY_WAVE_GENERATOR)).thenReturn(List.of(gravityWaveShip));
		when(planetRepository.findByGameIdAndXAndY(anyLong(), anyInt(), anyInt())).thenReturn(Optional.empty());
		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.ALLIANCE);
		when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(List.of(relation));

		subject.processJumpEngine(1L);

		assertSuccessfullJumpEngine(ship);
	}

	@Test
	void shouldCheckMapBoundariesWithJumpEngine() {
		Ship ship = createJumpEngineShip();
		ship.setFuel(100);
		ship.setDestinationX(-800);
		ship.setDestinationY(-800);

		when(shipRepository.getShipsWithActiveAbility(1L, ShipAbilityType.GRAVITY_WAVE_GENERATOR)).thenReturn(List.of());
		when(planetRepository.findByGameIdAndXAndY(anyLong(), anyInt(), anyInt())).thenReturn(Optional.empty());
		when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());

		subject.processJumpEngine(1L);

		verify(shipRepository, never()).save(any());
		verify(shipRemoval).deleteShip(ship, null);
	}

	@Test
	void shouldCheckGravityWaveCosts() {
		Ship ship = createGravityWaveGeneratorShip();

		subject.processGravityWaveGenerator(1L);

		verify(shipRepository, never()).save(any());
		verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_gravity_wave_generator_not_enough_mineral3, ship.createFullImagePath(),
			ship.getId(), NewsEntryClickTargetType.ship, ship.getName());
	}

	void assertNoGravityWaveGeneratorInteractions() {
		verify(shipRepository).save(any());
		verify(newsService, never()).add(any(), anyString(), anyString(), anyLong(), any());
	}

	void assertGravityWaveGeneratorActivatedSuccessfully(Ship ship) {
		verify(shipRepository).save(ship);
		assertEquals(20, ship.getMineral3());
	}

	@Test
	void shouldIgnoreEmptyShipsWithGravityWave() {
		Ship ship = createGravityWaveGeneratorShip();
		ship.setMineral3(120);

		subject.processGravityWaveGenerator(1L);

		assertGravityWaveGeneratorActivatedSuccessfully(ship);
		assertNoGravityWaveGeneratorInteractions();
	}

	@Test
	void shouldIgnoreSlowShipsWithGravityWave() {
		Ship ship = createGravityWaveGeneratorShip();
		ship.setMineral3(120);
		ship.setTravelSpeed(7);

		when(shipRepository.findByIds(anyList())).thenReturn(List.of(ship));
		when(playerRelationRepository.findByPlayerIds(1L, 1L)).thenReturn(Collections.emptyList());

		subject.processGravityWaveGenerator(1L);

		assertGravityWaveGeneratorActivatedSuccessfully(ship);
		assertNoGravityWaveGeneratorInteractions();
	}

	@Test
	void shouldSlowDownSelfWithGravityWave() {
		Ship ship = createGravityWaveGeneratorShip();
		ship.setMineral3(120);
		ship.setTravelSpeed(8);

		when(shipRepository.findByIds(anyList())).thenReturn(List.of(ship));
		when(playerRelationRepository.findByPlayerIds(1L, 1L)).thenReturn(Collections.emptyList());

		subject.processGravityWaveGenerator(1L);

		verify(shipRepository, times(2)).save(ship);
		assertEquals(20, ship.getMineral3());
		assertEquals(7, ship.getTravelSpeed());
		verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_gravity_wave_generator_slowed, ship.createFullImagePath(), ship.getId(),
			NewsEntryClickTargetType.ship, ship.getName());
		verify(newsService).add(any(), anyString(), anyString(), anyLong(), any(), any());
	}

	@Test
	void shouldSlowDownOwnShipsWithGravityWave() {
		Ship ship = createGravityWaveGeneratorShip();
		ship.setMineral3(120);

		Ship ownShip = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		ownShip.setTravelSpeed(8);
		when(shipRepository.findByIds(anyList())).thenReturn(List.of(ownShip));
		when(playerRelationRepository.findByPlayerIds(1L, 1L)).thenReturn(Collections.emptyList());

		subject.processGravityWaveGenerator(1L);

		assertGravityWaveGeneratorActivatedSuccessfully(ship);
		verify(shipRepository).save(ownShip);
		assertEquals(7, ownShip.getTravelSpeed());
		verify(newsService).add(ownShip.getPlayer(), NewsEntryConstants.news_entry_gravity_wave_generator_slowed, ownShip.createFullImagePath(),
			ownShip.getId(), NewsEntryClickTargetType.ship, ownShip.getName());
		verify(newsService).add(any(), anyString(), anyString(), anyLong(), any(), any());
	}

	@Test
	void shouldSlowDownAlliedShipsWithGravityWave() {
		Ship ship = createGravityWaveGeneratorShip();
		ship.setMineral3(120);

		Ship alliedShip = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		alliedShip.setTravelSpeed(8);
		when(shipRepository.findByIds(anyList())).thenReturn(List.of(alliedShip));
		when(playerRelationRepository.findByPlayerIds(1L, 1L)).thenReturn(Collections.emptyList());

		subject.processGravityWaveGenerator(1L);

		assertGravityWaveGeneratorActivatedSuccessfully(ship);
		verify(shipRepository).save(alliedShip);
		assertEquals(7, alliedShip.getTravelSpeed());
		verify(newsService).add(alliedShip.getPlayer(), NewsEntryConstants.news_entry_gravity_wave_generator_slowed, alliedShip.createFullImagePath(),
			alliedShip.getId(), NewsEntryClickTargetType.ship, alliedShip.getName());
		verify(newsService).add(any(), anyString(), anyString(), anyLong(), any(), any());
	}

	@Test
	void shouldInterceptShipsWithGravityWave() {
		Ship ship = createGravityWaveGeneratorShip();
		ship.setMineral3(120);

		Ship otherShip = createJumpEngineShip();
		otherShip.setActiveAbility(otherShip.getAbility(ShipAbilityType.JUMP_ENGINE).orElseThrow());
		otherShip.setPlayer(new Player(2L));
		when(shipRepository.findByIds(anyList())).thenReturn(List.of(otherShip));
		when(playerRelationRepository.findByPlayerIds(2L, 1L)).thenReturn(Collections.emptyList());

		subject.processGravityWaveGenerator(1L);

		assertGravityWaveGeneratorActivatedSuccessfully(ship);
		verify(shipRepository).save(otherShip);
		verify(newsService).add(otherShip.getPlayer(), NewsEntryConstants.news_entry_jump_engine_stopped, otherShip.createFullImagePath(),
			otherShip.getId(), NewsEntryClickTargetType.ship, otherShip.getName());
		verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_gravity_wave_generator_stopped_jumps, ship.createFullImagePath(),
			ship.getId(), NewsEntryClickTargetType.ship, ship.getName());
		verify(newsService, times(2)).add(any(), anyString(), anyString(), anyLong(), any(),
			any());
	}

	@Test
	void shouldCheckIfShipCloaked() {
		Ship ship = ShipTestFactory.createShip(100, 1000, 0, 0, 100, 1L);
		ship.setMineral2(1);

		subject.updateCloak(ship);

		assertEquals(false, ship.isCloaked());
		assertEquals(1, ship.getMineral2());

		ship.setMineral2(20);

		subject.updateCloak(ship);

		assertEquals(true, ship.isCloaked());
		assertEquals(10, ship.getMineral2());
	}

	@Test
	void shouldCheckPropulsionSystem() {
		MasterDataService.RANDOM.setSeed(3L);

		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		ship.setCloaked(true);

		PropulsionSystemTemplate propulsion = new PropulsionSystemTemplate();
		propulsion.setTechLevel(7);
		ship.setPropulsionSystemTemplate(propulsion);

		subject.finishCloakingChecks(ship);

		assertEquals(true, ship.isCloaked());
		verify(subject).checkCloakingPlasmaDischarge(eq(ship), anyFloat());
		verify(shipRepository).clearDestinationShip(List.of(3L));
	}

	@Test
	void shouldTriggerPlasmaDischarge() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		ship.setCloaked(true);

		PropulsionSystemTemplate propulsion = new PropulsionSystemTemplate();
		propulsion.setTechLevel(7);
		ship.setPropulsionSystemTemplate(propulsion);

		subject.checkCloakingPlasmaDischarge(ship, 0.1f);

		assertEquals(false, ship.isCloaked());
		verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_cloaking_failed_plasma_discharge, ship.createFullImagePath(),
			ship.getId(), NewsEntryClickTargetType.ship, ship.getName());
	}

	@Test
	void shouldProcessPerfectCloaking() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		when(shipRepository.getShipsWithActiveAbility(1L, ShipAbilityType.CLOAKING_PERFECT)).thenReturn(List.of(ship));

		subject.processPerfectCloaking(1L);

		verify(subject).updateCloak(ship);
		verify(subject).finishCloakingChecks(ship);
		verify(shipRepository).save(ship);
		verify(shipRepository).cloakAntigravPropulsion(1L);
	}

	@Test
	void shouldProcessReliableCloaking() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		ship.setPropulsionSystemTemplate(new PropulsionSystemTemplate());
		when(shipRepository.getShipsWithActiveAbility(1L, ShipAbilityType.CLOAKING_RELIABLE)).thenReturn(List.of(ship));

		subject.processReliableCloaking(1L);

		verify(subject).updateCloak(ship);
		verify(subject).finishCloakingChecks(ship);
		verify(shipRepository).save(ship);
	}

	@Test
	void shouldProcessUnreliableCloaking() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		ship.setPropulsionSystemTemplate(new PropulsionSystemTemplate());
		when(shipRepository.getShipsWithActiveAbility(1L, ShipAbilityType.CLOAKING_UNRELIABLE)).thenReturn(List.of(ship));
		when(configProperties.getVisibilityRadius()).thenReturn(83);

		subject.processUnreliableCloaking(1L);

		verify(subject).updateCloak(ship);
		verify(subject).finishCloakingChecks(ship);
		verify(shipRepository).save(ship);
		verify(shipRepository).getShipIdsInRadius(1L, 0, 0, 83);
		verify(planetRepository).getPlanetIdsInRadius(1L, 0, 0, 83, false);
	}

	@Test
	void shouldCompleteUnfinishedJumpPortal() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		WormHole wormHole = new WormHole();
		subject.finishJumpPortal(ship, wormHole);

		verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_jump_portals_completed, ship.createFullImagePath(), ship.getId(),
			NewsEntryClickTargetType.ship, ship.getName());

		ArgumentCaptor<WormHole> wormHoleArgs = ArgumentCaptor.forClass(WormHole.class);
		verify(wormHoleRepository, times(2)).save(wormHoleArgs.capture());

		List<WormHole> wormHoles = wormHoleArgs.getAllValues();
		WormHole secondPortal = wormHoles.get(0);
		WormHole firstPortal = wormHoles.get(1);

		assertEquals(WormHoleType.JUMP_PORTAL, secondPortal.getType());

		assertEquals(firstPortal, secondPortal.getConnection());
		assertEquals(secondPortal, firstPortal.getConnection());
	}

	@Test
	void shouldStartNewJumpPortal() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		subject.startJumpPortal(ship);

		verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_jump_portal_build, ship.createFullImagePath(), ship.getId(),
			NewsEntryClickTargetType.ship, ship.getName());

		ArgumentCaptor<WormHole> wormHoleArgs = ArgumentCaptor.forClass(WormHole.class);
		verify(wormHoleRepository).save(wormHoleArgs.capture());

		WormHole newWormHole = wormHoleArgs.getValue();
		assertNull(newWormHole.getConnection());
		assertEquals(WormHoleType.JUMP_PORTAL, newWormHole.getType());
	}

	@Test
	void shouldCheckTravelSpeedOfJumpPortalShip() {
		Ship ship = createJumpPortalShip();
		ship.setTravelSpeed(5);

		subject.processJumpPortalConstruction(1L);

		verify(newsService, never()).add(any(), anyString(), anyString(), anyLong(), any());
		verify(subject, never()).finishJumpPortal(any(), any());
		verify(subject, never()).startJumpPortal(any());
	}

	@Test
	void shouldCheckNearPlanetsOfJumpPortalShip() {
		Ship ship = createJumpPortalShip();

		when(planetRepository.getPlanetIdsInRadius(1L, 0, 0, 30, false)).thenReturn(List.of(1L));

		subject.processJumpPortalConstruction(1L);

		verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_jump_portal_planets_too_near, ship.createFullImagePath(), ship.getId(),
			NewsEntryClickTargetType.ship, ship.getName(), 30);
		verify(subject, never()).finishJumpPortal(any(), any());
		verify(subject, never()).startJumpPortal(any());
	}

	@Test
	void shouldCheckNearWormHolesOfJumpPortalShip() {
		Ship ship = createJumpPortalShip();

		when(wormHoleRepository.getWormHoleIdsInRadius(1L, 0, 0, 30)).thenReturn(List.of(1L));

		subject.processJumpPortalConstruction(1L);

		verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_jump_portal_anomalies_too_near, ship.createFullImagePath(),
			ship.getId(), NewsEntryClickTargetType.ship, ship.getName(), 30);
		verify(subject, never()).finishJumpPortal(any(), any());
		verify(subject, never()).startJumpPortal(any());
	}

	@Test
	void shouldCheckResourcesOfJumpPortalShip() {
		Ship ship = createJumpPortalShip();

		subject.processJumpPortalConstruction(1L);

		verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_jump_portal_not_enough_resources, ship.createFullImagePath(),
			ship.getId(), NewsEntryClickTargetType.ship, ship.getName());
		verify(subject, never()).finishJumpPortal(any(), any());
		verify(subject, never()).startJumpPortal(any());
	}

	@Test
	void shouldBuildJumpPortal() {
		Ship ship = createJumpPortalShip();
		ship.setFuel(100);
		ship.setMineral1(100);
		ship.setMineral2(100);
		ship.setMineral3(100);

		Optional<WormHole> wormHoleOpt = Optional.empty();
		when(wormHoleRepository.findUnfinishedJumpPortal(3L)).thenReturn(wormHoleOpt);

		subject.processJumpPortalConstruction(1L);

		verify(subject, never()).finishJumpPortal(any(), any());
		verify(subject).startJumpPortal(ship);

		assertEquals(90, ship.getFuel());
		assertEquals(80, ship.getMineral1());
		assertEquals(70, ship.getMineral2());
		assertEquals(60, ship.getMineral3());
	}

	@Test
	void shouldCheckForEmptyShipListWithPerfectEvade() {
		subject.processPerfectEvade(1L);

		verify(shipRepository, never()).saveAll(any());
		verify(shipRepository, never()).clearDestinationShip(anyList());
		verify(newsService, never()).add(any(), anyString(), anyString(), anyLong(), any());
	}

	@Test
	void shouldPerformPerfectEvade() {
		Ship ship = createShipWithAbility(ShipAbilityType.EVADE_PERFECT, Collections.emptyMap());

		when(planetRepository.findByGameIdAndXAndY(1L, 0, 0)).thenReturn(Optional.empty());

		subject.processPerfectEvade(1L);

		verify(shipRepository).saveAll(List.of(ship));
		verify(shipRepository).clearDestinationShip(List.of(3L));
		verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_evade_perfect, ship.createFullImagePath(), ship.getId(),
			NewsEntryClickTargetType.ship, ship.getName());
	}

	@Test
	void shouldCheckForEmptyShipListWithUnreliableEvade() {
		subject.processUnreliableEvade(1L);

		verify(shipRepository, never()).save(any());
		verify(shipRemoval, never()).deleteShip(any(), any());
		verify(shipRepository, never()).clearDestinationShip(anyList());
		verify(newsService, never()).add(any(), anyString(), anyString(), anyLong(), any());
	}

	@Test
	void shouldGetDestroyedByEvadeDamage() {
		Map<String, String> values = new HashMap<>(1);
		values.put(ShipAbilityConstants.EVADE_DAMAGE_MAX, "5000");
		Ship ship = createShipWithAbility(ShipAbilityType.EVADE_UNRELIABLE, values);

		when(planetRepository.findByGameIdAndXAndY(1L, 0, 0)).thenReturn(Optional.empty());

		subject.processUnreliableEvade(1L);

		verify(shipRemoval).deleteShip(ship, null);
		verify(newsService).add(any(), anyString(), anyString(), isNull(), isNull(), any());
		verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_evade_destroyed, ship.createFullImagePath(), null, null,
			ship.getName());

		verify(shipRepository, never()).save(any());
		verify(shipRepository, never()).clearDestinationShip(anyList());
	}

	@Test
	void shouldPerformUnreliableEvade() {
		Map<String, String> values = new HashMap<>(1);
		values.put(ShipAbilityConstants.EVADE_DAMAGE_MAX, "5");
		Ship ship = createShipWithAbility(ShipAbilityType.EVADE_UNRELIABLE, values);

		when(planetRepository.findByGameIdAndXAndY(1L, 0, 0)).thenReturn(Optional.empty());

		subject.processUnreliableEvade(1L);

		verify(shipRepository).save(ship);
		verify(shipRepository).clearDestinationShip(List.of(3L));
		verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_evade_damaged, ship.createFullImagePath(), ship.getId(),
			NewsEntryClickTargetType.ship, ship.getName(), 5);

		verify(shipRemoval, never()).deleteShip(any(), any());
	}

	@Test
	void shouldCheckPlanetPresentWithDestabilizer() {
		createDestabilizerShip();

		subject.processDestabilizer(1L);

		verify(subject, never()).destroyPlanet(anyLong(), any(), any());
	}

	@Test
	void shouldCheckPlayerWithDestabilizer() {
		Ship ship = createDestabilizerShip();
		Planet planet = new Planet(1L);
		planet.setPlayer(ship.getPlayer());
		ship.setPlanet(planet);

		subject.processDestabilizer(1L);

		verify(subject, never()).destroyPlanet(anyLong(), any(), any());
	}

	@Test
	void shouldCheckAlliancesWithDestabilizer() {
		Ship ship = createDestabilizerShip();
		Planet planet = new Planet(1L);
		Player alliedPlayer = new Player(2L);
		planet.setPlayer(alliedPlayer);
		ship.setPlanet(planet);

		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.ALLIANCE);
		when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(List.of(relation));

		subject.processDestabilizer(1L);

		verify(subject, never()).destroyPlanet(anyLong(), any(), any());
	}

	@Test
	void shouldCheckPlanetaryShieldWithDestabilizer() {
		Ship ship = createDestabilizerShip();
		Planet planet = new Planet(1L);
		Player alliedPlayer = new Player(2L);
		planet.setPlayer(alliedPlayer);
		OrbitalSystem planetaryShield = new OrbitalSystem();
		planetaryShield.setType(OrbitalSystemType.PLANETARY_SHIELDS);
		planet.setOrbitalSystems(List.of(planetaryShield));
		ship.setPlanet(planet);

		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.TRADE_AGREEMENT);
		when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(List.of(relation));

		subject.processDestabilizer(1L);

		verify(subject, never()).destroyPlanet(anyLong(), any(), any());
	}

	@Test
	void shouldCheckEfficiencyWithDestabilizer() {
		Ship ship = createDestabilizerShip();
		Planet planet = new Planet(1L);
		Player otherPlayer = new Player(2L);
		planet.setPlayer(otherPlayer);
		ship.setPlanet(planet);

		when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());

		subject.processDestabilizer(1L);

		verify(subject, never()).destroyPlanet(anyLong(), any(), any());
	}

	@Test
	void shouldUseDestabilizer() {
		Ship ship = createDestabilizerShip();
		ship.getAbility(ShipAbilityType.DESTABILIZER).orElseThrow().getValues().put(ShipAbilityConstants.DESTABILIZER_EFFICIENCY, "100");
		Planet planet = new Planet(1L);
		Player otherPlayer = new Player(2L);
		planet.setPlayer(otherPlayer);
		ship.setPlanet(planet);

		when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());
		Player player = ship.getPlayer();
		player.getGame().setPlayers(Collections.emptyList());

		subject.processDestabilizer(1L);

		verify(subject).destroyPlanet(1L, player, planet);
	}

	@Test
	void shouldCheckPlayerEqualWithDestabilizer() {
		Ship ship = createDestabilizerShip();
		Planet planet = new Planet(1L);
		Player otherPlayer = new Player(2L);
		planet.setPlayer(otherPlayer);
		ship.setPlanet(planet);

		when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());
		Player player = ship.getPlayer();
		player.getGame().setPlayers(List.of(player, otherPlayer));

		subject.destroyPlanet(1L, player, planet);

		verify(newsService).add(player, NewsEntryConstants.news_entry_planet_destroyed, ImageConstants.NEWS_EXPLODE, null, null, planet.getName(),
			"A1");
		verify(newsService).add(otherPlayer, NewsEntryConstants.news_entry_our_planet_destroyed, ImageConstants.NEWS_EXPLODE, null, null,
			planet.getName(), "A1");
		verify(planetRepository).delete(planet);
		verify(starbaseService, never()).delete(any());
	}

	@Test
	void shouldCheckStarbasePresentWithDestabilizer() {
		Ship ship = createDestabilizerShip();
		Planet planet = new Planet(1L);
		Player otherPlayer = new Player(2L);
		planet.setPlayer(otherPlayer);
		ship.setPlanet(planet);
		Starbase starbase = new Starbase();
		planet.setStarbase(starbase);

		when(playerRelationRepository.findByPlayerIds(1L, 2L)).thenReturn(Collections.emptyList());
		Player player = ship.getPlayer();
		player.getGame().setPlayers(Collections.emptyList());

		subject.destroyPlanet(1L, player, planet);

		verify(planetRepository).delete(planet);
		verify(starbaseService).delete(starbase);
	}
}
