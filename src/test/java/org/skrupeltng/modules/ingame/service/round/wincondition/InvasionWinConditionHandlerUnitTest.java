package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.InvasionDifficulty;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class InvasionWinConditionHandlerUnitTest {

	final InvasionWinConditionHandler subject = new InvasionWinConditionHandler();

	@Test
	void shouldReturnNoWinnersBecauseNotAllWavesFinished() {
		Player p1 = new Player();
		Player p2 = new Player();
		Player pai = new Player();
		pai.setBackgroundAIAgent(true);

		Game game = new Game();
		game.setInvasionDifficulty(InvasionDifficulty.EASY);
		game.setPlayers(List.of(p1, p2, pai));

		WinnersCalculationResult result = subject.calculate(game);

		assertThat(result).isNotNull();
		assertThat(result.winners()).isEmpty();
		assertThat(result.unlockedAchievements()).isEmpty();
	}

	@Test
	void shouldReturnNoWinnersNotAllEnemyShipsDestroyed() {
		Player p1 = new Player();
		Player p2 = new Player();
		Player pai = new Player();
		pai.setBackgroundAIAgent(true);
		pai.setShips(Set.of(new Ship()));

		Game game = new Game();
		game.setInvasionWave(7);
		game.setInvasionDifficulty(InvasionDifficulty.EASY);
		game.setPlayers(List.of(p1, p2, pai));

		WinnersCalculationResult result = subject.calculate(game);

		assertThat(result).isNotNull();
		assertThat(result.winners()).isEmpty();
		assertThat(result.unlockedAchievements()).isEmpty();
	}

	@Test
	void shouldRemainingPlayersAsWinners() {
		Player p1 = new Player(1L);
		Player p2 = new Player(2L);
		p2.setHasLost(true);
		Player pai = new Player();
		pai.setAiLevel(AILevel.AI_EASY);
		pai.setBackgroundAIAgent(true);
		pai.setShips(Collections.emptySet());

		Game game = new Game();
		game.setInvasionWave(7);
		game.setInvasionDifficulty(InvasionDifficulty.EASY);
		game.setPlayers(List.of(p1, p2, pai));

		WinnersCalculationResult result = subject.calculate(game);

		assertThat(result).isNotNull();
		assertThat(result.winners()).containsExactly(p1);
		assertThat(result.unlockedAchievements()).containsExactly(AchievementType.INVASION_EASY_WON);
	}

	@Test
	void shouldReturnMatchingAchievementTypes() {
		assertThat(subject.getUnlockedAchievementTypes(InvasionDifficulty.NORMAL))
			.containsExactly(AchievementType.INVASION_EASY_WON, AchievementType.INVASION_NORMAL_WON);

		assertThat(subject.getUnlockedAchievementTypes(InvasionDifficulty.HARD))
			.containsExactly(AchievementType.INVASION_EASY_WON, AchievementType.INVASION_NORMAL_WON, AchievementType.INVASION_HARD_WON);
	}
}
