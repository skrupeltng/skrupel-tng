package org.skrupeltng.modules.ingame.service.round.combat;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTactics;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.service.MasterDataService;

class ShipDamageUnitTest {

	@BeforeEach
	void setup() {
		MasterDataService.RANDOM.setSeed(1L);
	}

	@Test
	void shouldNotApplyDamageToWeapons() {
		Ship ship = ShipTestFactory.createShip(100, 100, 10, 10, 100, 1L);
		ship.getShipTemplate().setHangarCapacity(10);

		ShipDamage damage = new ShipDamage(ship, ShipTactics.STANDARD, 0);

		assertEquals(10, damage.energyWeaponCount);
		assertEquals(10, damage.projectileWeaponCount);
		assertEquals(10, damage.hangarCount);
	}

	@Test
	void shouldApplyDamageToWeapons() {
		MasterDataService.RANDOM.setSeed(1L);
		Ship ship = ShipTestFactory.createShip(100, 100, 10, 10, 100, 1L);
		ship.getShipTemplate().setHangarCapacity(10);
		ship.setDamage(90);

		ShipDamage damage = new ShipDamage(ship, ShipTactics.STANDARD, 0);
		assertEquals(2, damage.energyWeaponCount);

		damage = new ShipDamage(ship, ShipTactics.STANDARD, 0);
		assertEquals(2, damage.hangarCount);

		damage = new ShipDamage(ship, ShipTactics.STANDARD, 0);
		damage = new ShipDamage(ship, ShipTactics.STANDARD, 0);
		damage = new ShipDamage(ship, ShipTactics.STANDARD, 0);
		assertEquals(2, damage.projectileWeaponCount);
	}

	@Test
	void shouldSetNoEnergyWeaponDamage() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 10, 100, 1L);
		ship.setEnergyWeaponTemplate(null);

		ShipDamage damage = new ShipDamage(ship, ShipTactics.STANDARD, 0);

		assertEquals(0, damage.energyWeaponDamage);
		assertEquals(0, damage.energyWeaponDamageCrew);
	}

	@Test
	void shouldTakeEnergyTechLevelFromTemplate() {
		Ship ship = ShipTestFactory.createShip(100, 100, 10, 10, 100, 1L);

		ShipDamage damage = new ShipDamage(ship, ShipTactics.STANDARD, 0);

		assertEquals(ShipDamage.ENERGY_WEAPON_DAMAGE_DATA[4], damage.energyWeaponDamage);
		assertEquals(ShipDamage.ENERGY_WEAPON_DAMAGE_CREW_DATA[4], damage.energyWeaponDamageCrew);
	}

	@Test
	void shouldSetMinimumEnergyWeaponDamageBecauseOfImprovedShipSystemsModule() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 10, 100, 1L);
		ship.setShipModule(ShipModule.IMPROVED_ENERGY_SYSTEMS);
		ship.setEnergyWeaponTemplate(null);

		ShipDamage damage = new ShipDamage(ship, ShipTactics.STANDARD, 0);

		assertEquals(ShipDamage.ENERGY_WEAPON_DAMAGE_DATA[1], damage.energyWeaponDamage);
		assertEquals(ShipDamage.ENERGY_WEAPON_DAMAGE_CREW_DATA[1], damage.energyWeaponDamageCrew);
	}

	@Test
	void shouldChangeEnergyDamageWhenCapturing() {
		Ship ship = ShipTestFactory.createShip(100, 100, 10, 10, 100, 1L);
		ship.setTaskType(ShipTaskType.CAPTURE_SHIP);

		ShipDamage damage = new ShipDamage(ship, ShipTactics.STANDARD, 0);

		assertEquals(Math.round(ShipDamage.ENERGY_WEAPON_DAMAGE_DATA[4] / 2f), damage.energyWeaponDamage);
		assertEquals(Math.round(ShipDamage.ENERGY_WEAPON_DAMAGE_CREW_DATA[4] * ShipDamage.CAPTURE_DAMAGE_CREW_FACTOR), damage.energyWeaponDamageCrew);
	}

	@Test
	void shouldSetNoProjectileWeaponDamage() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 10, 100, 1L);
		ship.setProjectileWeaponTemplate(null);

		ShipDamage damage = new ShipDamage(ship, ShipTactics.STANDARD, 0);

		assertEquals(0, damage.projectileWeaponDamage);
		assertEquals(0, damage.projectileWeaponDamageCrew);
	}

	@Test
	void shouldTakeProjectileTechLevelFromTemplate() {
		Ship ship = ShipTestFactory.createShip(100, 100, 10, 10, 100, 1L);

		ShipDamage damage = new ShipDamage(ship, ShipTactics.STANDARD, 0);

		assertEquals(ShipDamage.PROJECTILE_WEAPON_DAMAGE_DATA[2], damage.projectileWeaponDamage);
		assertEquals(ShipDamage.PROJECTILE_WEAPON_DAMAGE_CREW_DATA[2], damage.projectileWeaponDamageCrew);
	}

	@Test
	void shouldChangeProjectileDamageWhenCapturing() {
		Ship ship = ShipTestFactory.createShip(100, 100, 10, 10, 100, 1L);
		ship.setTaskType(ShipTaskType.CAPTURE_SHIP);

		ShipDamage damage = new ShipDamage(ship, ShipTactics.STANDARD, 0);

		assertEquals(Math.round(ShipDamage.PROJECTILE_WEAPON_DAMAGE_DATA[2] * ShipDamage.CAPTURE_DAMAGE_FACTOR), damage.projectileWeaponDamage);
		assertEquals(Math.round(ShipDamage.PROJECTILE_WEAPON_DAMAGE_CREW_DATA[2] * ShipDamage.CAPTURE_DAMAGE_CREW_FACTOR), damage.projectileWeaponDamageCrew);
	}

	@Test
	void shouldNotCheckEnemyTacticsBecauseNotPresent() {
		Ship ship = ShipTestFactory.createShip(100, 100, 10, 10, 100, 1L);
		ShipDamage damage = new ShipDamage(ship, null, 0);
		assertStandardDamages(damage);
	}

	@Test
	void shouldNotCheckEnemyTacticsBecauseNotEnabled() {
		Ship ship = ShipTestFactory.createShip(100, 100, 10, 10, 100, 1L);
		ShipDamage damage = new ShipDamage(ship, ShipTactics.OFFENSIVE, 0);
		assertStandardDamages(damage);
	}

	private void assertStandardDamages(ShipDamage damage) {
		assertEquals(ShipDamage.ENERGY_WEAPON_DAMAGE_DATA[4], damage.energyWeaponDamage);
		assertEquals(ShipDamage.ENERGY_WEAPON_DAMAGE_CREW_DATA[4], damage.energyWeaponDamageCrew);
		assertEquals(ShipDamage.PROJECTILE_WEAPON_DAMAGE_DATA[2], damage.projectileWeaponDamage);
		assertEquals(ShipDamage.PROJECTILE_WEAPON_DAMAGE_CREW_DATA[2], damage.projectileWeaponDamageCrew);
	}

	@Test
	void shouldNotAddPenaltyOrBonus() {
		Ship ship = ShipTestFactory.createShip(100, 100, 10, 10, 100, 1L);
		ship.setTactics(ShipTactics.STANDARD);

		ShipDamage damage = new ShipDamage(ship, ShipTactics.STANDARD, 0);
		assertStandardDamages(damage);

		ship.setTactics(ShipTactics.DEFENSIVE);
		damage = new ShipDamage(ship, ShipTactics.DEFENSIVE, 0);
		assertStandardDamages(damage);

		ship.setTactics(ShipTactics.OFFENSIVE);
		damage = new ShipDamage(ship, ShipTactics.OFFENSIVE, 0);
		assertStandardDamages(damage);
	}

	@Test
	void shouldAddPenaltyBecauseOtherIsDefensive() {
		Ship ship = ShipTestFactory.createShip(100, 100, 10, 10, 100, 1L);
		ship.setTactics(ShipTactics.STANDARD);
		ship.getPlayer().getGame().setEnableTactialCombat(true);

		ShipDamage damage = new ShipDamage(ship, ShipTactics.DEFENSIVE, 0);

		assertEquals((int)(ShipDamage.ENERGY_WEAPON_DAMAGE_DATA[4] * ShipDamage.TACTICS_PENALTY), damage.energyWeaponDamage);
		assertEquals((int)(ShipDamage.ENERGY_WEAPON_DAMAGE_CREW_DATA[4] * ShipDamage.TACTICS_PENALTY), damage.energyWeaponDamageCrew);
		assertEquals((int)(ShipDamage.PROJECTILE_WEAPON_DAMAGE_DATA[2] * ShipDamage.TACTICS_PENALTY), damage.projectileWeaponDamage);
		assertEquals((int)(ShipDamage.PROJECTILE_WEAPON_DAMAGE_CREW_DATA[2] * ShipDamage.TACTICS_PENALTY), damage.projectileWeaponDamageCrew);
	}

	@Test
	void shouldAddBonusBecauseOtherIsOffensive() {
		Ship ship = ShipTestFactory.createShip(100, 100, 10, 10, 100, 1L);
		ship.setTactics(ShipTactics.STANDARD);
		ship.getPlayer().getGame().setEnableTactialCombat(true);

		ShipDamage damage = new ShipDamage(ship, ShipTactics.OFFENSIVE, 0);

		assertEquals((int)(ShipDamage.ENERGY_WEAPON_DAMAGE_DATA[4] * ShipDamage.TACTICS_BONUS), damage.energyWeaponDamage);
		assertEquals((int)(ShipDamage.ENERGY_WEAPON_DAMAGE_CREW_DATA[4] * ShipDamage.TACTICS_BONUS), damage.energyWeaponDamageCrew);
		assertEquals((int)(ShipDamage.PROJECTILE_WEAPON_DAMAGE_DATA[2] * ShipDamage.TACTICS_BONUS), damage.projectileWeaponDamage);
		assertEquals((int)(ShipDamage.PROJECTILE_WEAPON_DAMAGE_CREW_DATA[2] * ShipDamage.TACTICS_BONUS), damage.projectileWeaponDamageCrew);
	}

	@Test
	void shouldNotApplyNonPositiveAdvantage() {
		Ship ship = ShipTestFactory.createShip(100, 100, 7, 0, 100, 1L);

		ShipDamage damage = new ShipDamage(ship, ShipTactics.OFFENSIVE, -1);

		assertEquals(ShipDamage.ENERGY_WEAPON_DAMAGE_DATA[4], damage.energyWeaponDamage);
		assertEquals(7, damage.energyWeaponCount);
		assertEquals(3, damage.hangarCount);
		assertEquals(100, damage.mass);

		damage = new ShipDamage(ship, ShipTactics.OFFENSIVE, 0);

		assertEquals(ShipDamage.ENERGY_WEAPON_DAMAGE_DATA[4], damage.energyWeaponDamage);
		assertEquals(7, damage.energyWeaponCount);
		assertEquals(3, damage.hangarCount);
		assertEquals(100, damage.mass);
	}

	@Test
	void shouldSetEnergyWeaponDamageWhenAdvantagePresentAndNoEnergyWeapons() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);

		ShipDamage damage = new ShipDamage(ship, ShipTactics.OFFENSIVE, 2);

		assertEquals(ShipDamage.ENERGY_WEAPON_DAMAGE_DATA[1], damage.energyWeaponDamage);
		assertEquals(2, damage.energyWeaponCount);
		assertEquals(3, damage.hangarCount);
		assertEquals(100, damage.mass);
	}

	@Test
	void shouldIncreaseHangarsWhenEnergyWeaponAdvantageFull() {
		Ship ship = ShipTestFactory.createShip(100, 100, 8, 0, 100, 1L);

		ShipDamage damage = new ShipDamage(ship, ShipTactics.OFFENSIVE, 4);

		assertEquals(ShipDamage.ENERGY_WEAPON_DAMAGE_DATA[4], damage.energyWeaponDamage);
		assertEquals(10, damage.energyWeaponCount);
		assertEquals(5, damage.hangarCount);
		assertEquals(100, damage.mass);
	}

	@Test
	void shouldIncreaseMassWhenHangarAdvantageFull() {
		Ship ship = ShipTestFactory.createShip(100, 100, 10, 0, 100, 1L);

		ShipDamage damage = new ShipDamage(ship, ShipTactics.OFFENSIVE, 12);

		assertEquals(ShipDamage.ENERGY_WEAPON_DAMAGE_DATA[4], damage.energyWeaponDamage);
		assertEquals(10, damage.energyWeaponCount);
		assertEquals(10, damage.hangarCount);
		assertEquals(600, damage.mass);
	}

	@Test
	void shouldCapMassWhenHangarAdvantageFull() {
		Ship ship = ShipTestFactory.createShip(100, 100, 10, 0, 100, 1L);

		ShipDamage damage = new ShipDamage(ship, ShipTactics.OFFENSIVE, 50);

		assertEquals(ShipDamage.ENERGY_WEAPON_DAMAGE_DATA[4], damage.energyWeaponDamage);
		assertEquals(10, damage.energyWeaponCount);
		assertEquals(10, damage.hangarCount);
		assertEquals(1000, damage.mass);
	}

	@Test
	void shouldUseAllWeaponsAgainstShip() {
		Ship ship = ShipTestFactory.createShip(100, 100, 4, 5, 100, 1L);
		Ship enemyShip = ShipTestFactory.createShip(100, 100, 4, 5, 100, 1L);
		ShipDamage damage = new ShipDamage(ship, ShipTactics.OFFENSIVE, 0);

		damage.damageEnemyShip(enemyShip, 3, false, enemyShip.getShipTemplate().getMass());
		Mockito.verify(enemyShip, Mockito.times(3)).receiveDamage(Mockito.anyInt(), Mockito.anyFloat(), Mockito.anyBoolean(), Mockito.anyInt());
		Mockito.verify(enemyShip).receiveDamage(15, 4, false, 100);
		Mockito.verify(enemyShip).receiveDamage(4, 1.5f, false, 100);
		Mockito.verify(enemyShip).receiveDamage(8, 2, false, 100);
	}

	@Test
	void shouldCheckEnergyWeaponCountAgainstRound() {
		Ship ship = ShipTestFactory.createShip(100, 100, 7, 0, 100, 1L);
		ShipDamage damage = new ShipDamage(ship, ShipTactics.OFFENSIVE, 0);

		int result = damage.getEnergyWeaponDamage(5);
		assertEquals(damage.energyWeaponDamage, result);

		result = damage.getEnergyWeaponDamage(7);
		assertEquals(damage.energyWeaponDamage, result);

		result = damage.getEnergyWeaponDamage(8);
		assertEquals(0, result);

		result = damage.getEnergyWeaponDamageCrew(5);
		assertEquals(damage.energyWeaponDamageCrew, result);

		result = damage.getEnergyWeaponDamageCrew(7);
		assertEquals(damage.energyWeaponDamageCrew, result);

		result = damage.getEnergyWeaponDamageCrew(8);
		assertEquals(0, result);
	}

	@Test
	void shouldReturnNoProjectileWeaponDamageBecauseNoProjectiles() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 7, 100, 1L);
		ship.setProjectiles(0);
		ShipDamage damage = new ShipDamage(ship, ShipTactics.OFFENSIVE, 0);

		int result = damage.getProjectileDamage(5, 50);
		assertEquals(0, result);
	}

	@Test
	void shouldReturnNoProjectileWeaponDamageBecauseNotEnoughWeapons() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 7, 100, 1L);
		ship.setProjectiles(10);
		ShipDamage damage = new ShipDamage(ship, ShipTactics.OFFENSIVE, 0);

		int result = damage.getProjectileDamage(8, 50);
		assertEquals(0, result);
	}

	@Test
	void shouldReturnProjectileDamage() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 7, 100, 1L);
		ship.setProjectiles(10);
		ShipDamage damage = new ShipDamage(ship, ShipTactics.OFFENSIVE, 0);

		int result = damage.getProjectileDamage(5, 50);
		assertEquals(50, result);

		result = damage.getProjectileDamage(5, 50);
		result = damage.getProjectileDamage(5, 50);
		assertEquals(50, result);

		assertEquals(7, ship.getProjectiles());
	}

	@Test
	void shouldUseCorrectProjectileDamage() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 7, 100, 1L);
		ship.setProjectiles(10);
		ShipDamage damage = Mockito.spy(new ShipDamage(ship, ShipTactics.OFFENSIVE, 0));

		damage.getProjectileWeaponDamage(5);
		Mockito.verify(damage).getProjectileDamage(5, damage.projectileWeaponDamage);

		damage.getProjectileWeaponDamageCrew(5);
		Mockito.verify(damage).getProjectileDamage(5, damage.projectileWeaponDamageCrew);
	}

	@Test
	void shouldCheckHangarCountAgainstRound() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		ShipDamage damage = new ShipDamage(ship, ShipTactics.OFFENSIVE, 0);

		int result = damage.getHangarDamage(3);
		assertEquals(ShipDamage.HANGAR_DAMAGE, result);

		result = damage.getHangarDamage(7);
		assertEquals(0, result);
	}

	@Test
	void shouldUseAllWeaponsAgainstPlanet() {
		Ship ship = ShipTestFactory.createShip(100, 100, 4, 5, 100, 1L);
		ShipDamage damage = Mockito.spy(new ShipDamage(ship, ShipTactics.OFFENSIVE, 0));

		int result = damage.calculateDamageToPlanet(3);
		assertEquals(27, result);
		Mockito.verify(damage).getEnergyWeaponDamage(3);
		Mockito.verify(damage).getProjectileWeaponDamage(3);
		Mockito.verify(damage).getHangarDamage(3);
	}
}