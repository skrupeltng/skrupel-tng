package org.skrupeltng.modules.ingame.service.round.combat.ground;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

class GroundCombatRoundCalculatorIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	GroundCombatRoundCalculator subject;

	@Autowired
	PlanetRepository planetRepository;

	@Test
	void shouldCalculateForPlanetWithoutColonistsButGroundUnits() {
		subject.processGroundCombat(gameId);

		Planet planet = planetRepository.getReferenceById(2086L);

		assertThat(planet.getPlayer()).isNotNull();
		assertThat(planet.getPlayer().getId()).isEqualTo(162L);
		assertThat(planet.getNewPlayer()).isNull();
	}
}
