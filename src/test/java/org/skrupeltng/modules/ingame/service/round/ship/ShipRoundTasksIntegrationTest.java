package org.skrupeltng.modules.ingame.service.round.ship;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntryRepository;
import org.skrupeltng.modules.ingame.modules.overview.database.ShockWaveDestructionLogEntry;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TestTransaction;

class ShipRoundTasksIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	ShipRoundTasks shipRoundAbilities;

	@Autowired
	ShipRepository shipRepository;

	@Autowired
	PlanetRepository planetRepository;

	@Autowired
	NewsEntryRepository newsEntryRepository;

	@Autowired
	GameRepository gameRepository;

	@Autowired
	PlayerRepository playerRepository;

	@Test
	void shouldResetShipTasksBecauseNoValueForTractorBeam() {
		shipRoundAbilities.precheckTractorBeam(gameId);

		Ship ship = shipRepository.getReferenceById(47L);
		assertNull(ship.getTaskValue());
		assertEquals(ShipTaskType.NONE, ship.getTaskType());
		assertEquals(170, ship.getDestinationX());
		assertEquals(115, ship.getDestinationY());
		assertEquals(7, ship.getTravelSpeed());
	}

	@Test
	void shouldResetShipTasksBecauseTargetNotOnSamePosition() {
		shipRoundAbilities.precheckTractorBeam(gameId);

		Ship ship = shipRepository.getReferenceById(49L);
		assertNull(ship.getTaskValue());
		assertEquals(ShipTaskType.NONE, ship.getTaskType());
		assertEquals(60, ship.getDestinationX());
		assertEquals(178, ship.getDestinationY());
		assertEquals(7, ship.getTravelSpeed());
	}

	@Test
	void shouldManageTractoredShipAndLimitSpeed() {
		shipRoundAbilities.precheckTractorBeam(gameId);

		Ship ship = shipRepository.getReferenceById(50L);
		assertEquals(7, ship.getTravelSpeed());
		assertEquals("51", ship.getTaskValue());

		ship = shipRepository.getReferenceById(51L);
		assertEquals(0, ship.getTravelSpeed());
		assertEquals(-1, ship.getDestinationX());
		assertEquals(-1, ship.getDestinationY());

		ship = shipRepository.getReferenceById(52L);
		assertEquals(7, ship.getTravelSpeed());
		assertEquals("53", ship.getTaskValue());

		ship = shipRepository.getReferenceById(53L);
		assertEquals(0, ship.getTravelSpeed());
		assertEquals(-1, ship.getDestinationX());
		assertEquals(-1, ship.getDestinationY());
		assertNull(ship.getDestinationShip());
	}

	@Test
	void shouldDestroyAndDamageShipsByAutoDestructionShockWave() {
		MasterDataService.RANDOM.setSeed(1);

		shipRoundAbilities.processAutoDestruction(gameId);
		shipRoundAbilities.deleteAutoDestructionShips(gameId);
		TestTransaction.end();
		TestTransaction.start();

		Optional<Ship> autoDestructionOpt = shipRepository.findById(101L);
		Optional<Ship> capitalShipOpt = shipRepository.findById(99L);
		Optional<Ship> bigFreighterOpt = shipRepository.findById(103L);
		Optional<Ship> smallFighterOpt = shipRepository.findById(102L);
		Optional<Ship> smallFreighterOpt = shipRepository.findById(100L);

		assertTrue(autoDestructionOpt.isEmpty(), "Ship 101 was expected to be destroyed but was not!");
		assertTrue(capitalShipOpt.isPresent(), "Ship 99 was expected to not be destroyed but was not!");
		assertTrue(smallFreighterOpt.isPresent(), "Ship 100 was expected to not be destroyed but was!");
		assertTrue(smallFighterOpt.isEmpty(), "Ship 102 was expected to be destroyed but was not!");
		assertTrue(bigFreighterOpt.isPresent(), "Ship 103 was expected to not be destroyed but was not!");

		assertEquals(4, capitalShipOpt.get().getDamage());
		assertEquals(64, bigFreighterOpt.get().getDamage());
		assertEquals(0, smallFreighterOpt.get().getDamage());

		List<NewsEntry> entries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertEquals(2, entries.size());

		NewsEntry entry = entries.get(1);
		assertEquals(NewsEntryConstants.news_entry_ship_auto_destructed, entry.getTemplate());
		assertEquals(List.of("Vortex Sphere"), entry.retrieveArgumentValues());

		entry = entries.get(0);
		assertEquals(NewsEntryConstants.news_entry_multiple_damaged_and_one_destroyed_by_auto_destruction, entry.getTemplate());
		assertEquals(List.of("2"), entry.retrieveArgumentValues());

		List<ShockWaveDestructionLogEntry> logEntries = entry.retrieveSortedShockWaveDestructionLogEnties();
		assertEquals(3, logEntries.size());

		ShockWaveDestructionLogEntry logEntry = logEntries.get(0);
		assertEquals("Capital Ship", logEntry.getShipName());
		assertEquals(4, logEntry.getDamage());
		assertFalse(logEntry.isDestroyed());

		logEntry = logEntries.get(1);
		assertEquals("Tetrabase", logEntry.getShipName());
		assertEquals(432, logEntry.getDamage());
		assertTrue(logEntry.isDestroyed());

		logEntry = logEntries.get(2);
		assertEquals("Big Freighter", logEntry.getShipName());
		assertEquals(64, logEntry.getDamage());
		assertFalse(logEntry.isDestroyed());

		assertThat(playerRepository.getReferenceById(83L).getLastRoundDestroyedShips()).isEqualTo(2);
		TestTransaction.end();
	}

	@Test
	void shouldKillMostColonistsInPlanetaryBombardment() {
		MasterDataService.RANDOM.setSeed(1);
		shipRoundAbilities.processPlanetBombardment(gameId);

		Planet planet = planetRepository.getReferenceById(1245L);
		assertTrue(planet.getColonists() < 1000, "Expected colonist count below 1000!");
	}

	@Test
	void shouldNotKillMostColonistsInPlanetaryBombardmentBecauseBunker() {
		shipRoundAbilities.processPlanetBombardment(gameId);

		Planet planet = planetRepository.getReferenceById(1254L);
		assertTrue(planet.getColonists() >= 1000, "Expected colonist count to be at least 1000!");
	}

	@Test
	void shouldKillColonistsToBunkerLimitInPlanetaryBombardment() {
		shipRoundAbilities.processPlanetBombardment(gameId);

		Planet planet = planetRepository.getReferenceById(1256L);
		assertTrue(planet.getColonists() >= 1000 && planet.getColonists() < 2000,
			"Expected colonist count to be at least 1000 and below 2000 but was " + planet.getColonists());
	}
}
