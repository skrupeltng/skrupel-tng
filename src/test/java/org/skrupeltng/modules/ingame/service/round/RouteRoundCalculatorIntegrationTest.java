package org.skrupeltng.modules.ingame.service.round;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.springframework.beans.factory.annotation.Autowired;

class RouteRoundCalculatorIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	private RouteRoundCalculator subject;

	@Autowired
	private ShipRepository shipRepository;

	@Test
	void shouldNotRefuelAndNotSetNextRouteEntry() {
		subject.processRouteTravel(gameId);

		Ship ship = shipRepository.getReferenceById(40L);

		assertEquals(94, ship.getFuel());
		assertEquals(990L, ship.getCurrentRouteEntry().getPlanet().getId());
	}

	@Test
	void shouldRefuelAndSetNextRouteEntry() {
		subject.processRouteTravel(gameId);

		Ship ship = shipRepository.getReferenceById(41L);

		assertEquals(40, ship.getFuel());
		assertEquals(1006L, ship.getCurrentRouteEntry().getPlanet().getId());
	}

	@Test
	void shouldNotRefuelAndNotLoadResourcesBecauseNoRoutePlanet() {
		subject.processPlanetExchange(gameId);

		Ship ship = shipRepository.getReferenceById(42L);

		assertEquals(22, ship.getFuel());
		assertEquals(61, ship.getMineral1());
		assertEquals(42, ship.getMineral2());
		assertEquals(55, ship.getMineral3());
	}

	@Test
	void shouldRefuelAndUnloadResources() {
		subject.processPlanetExchange(gameId);

		Ship ship = shipRepository.getReferenceById(43L);

		assertEquals(20, ship.getFuel());
		assertEquals(0, ship.getMoney());
		assertEquals(0, ship.getMineral1());
		assertEquals(0, ship.getMineral2());
		assertEquals(0, ship.getMineral3());
	}

	@Test
	void shouldAlwaysHonorPrimaryResource() {
		subject.processPlanetExchange(gameId);

		Ship ship = shipRepository.getReferenceById(76L);

		assertEquals(216, ship.getMoney());
		assertEquals(170, ship.getMineral1());
		assertEquals(30, ship.getMineral2());
		assertEquals(0, ship.getMineral3());
		assertEquals(30, ship.getFuel());
	}
}
