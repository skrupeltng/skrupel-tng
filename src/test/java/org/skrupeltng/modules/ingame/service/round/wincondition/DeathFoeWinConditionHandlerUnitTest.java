package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerDeathFoe;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class DeathFoeWinConditionHandlerUnitTest {

	final DeathFoeWinConditionHandler subject = new DeathFoeWinConditionHandler();

	@Test
	void shouldDetectNoWinnerBecauseNoDeathFoesKilled() {
		Player player1 = new Player(1L);
		Player player2 = new Player(2L);
		player1.setDeathFoes(Set.of(new PlayerDeathFoe(player2)));
		player2.setDeathFoes(Set.of(new PlayerDeathFoe(player1)));

		Game game = new Game();
		game.setPlayers(List.of(player1, player2));

		WinnersCalculationResult result = subject.calculate(game);

		assertThat(result).isNotNull();
		assertThat(result.winners()).isEmpty();
		assertThat(result.unlockedAchievements()).isEmpty();
	}

	@Test
	void shouldDetectSingleDeathFoeWinner() {
		Player player1 = new Player(1L);
		player1.setHasLost(true);
		Player player2 = new Player(2L);
		player1.setDeathFoes(Set.of(new PlayerDeathFoe(player2)));
		player2.setDeathFoes(Set.of(new PlayerDeathFoe(player1)));

		Game game = new Game();
		game.setPlayers(List.of(player1, player2));

		WinnersCalculationResult result = subject.calculate(game);

		assertThat(result).isNotNull();
		assertThat(result.winners()).containsExactly(player2);
		assertThat(result.unlockedAchievements()).isEmpty();
	}
}
