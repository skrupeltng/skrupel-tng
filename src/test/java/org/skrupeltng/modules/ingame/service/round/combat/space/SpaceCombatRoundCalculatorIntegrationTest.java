package org.skrupeltng.modules.ingame.service.round.combat.space;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntryRepository;
import org.skrupeltng.modules.ingame.modules.overview.database.spacecombat.SpaceCombatLogEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.spacecombat.SpaceCombatLogEntryItem;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.service.round.RoundCalculationService;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SpaceCombatRoundCalculatorIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	SpaceCombatRoundCalculator subject;

	@Autowired
	RoundCalculationService roundCalculationService;

	@Autowired
	ShipRepository shipRepository;

	@Autowired
	NewsEntryRepository newsEntryRepository;

	@Autowired
	GameRepository gameRepository;

	@Autowired
	PlayerRepository playerRepository;

	@Test
	void shouldMutuallyDestroyWithoutWeapons() {
		subject.processCombat(gameId);

		assertTrue(shipRepository.findById(55L).isEmpty(), "Ship 55 is expected to be destroyed but was not!");
		assertTrue(shipRepository.findById(57L).isEmpty(), "Ship 57 is expected to be destroyed but was not!");

		List<NewsEntry> newsEntries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertEquals(1, newsEntries.size());

		NewsEntry newsEntry = newsEntries.get(0);
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_mutual_destroyed, newsEntry.getTemplate());

		List<SpaceCombatLogEntry> logEntries = newsEntry.retrieveSortedSpaceCombatLogEntries();
		assertEquals(1, logEntries.size());
		SpaceCombatLogEntry logEntry = logEntries.get(0);
		List<SpaceCombatLogEntryItem> items = logEntry.getItems();
		assertEquals(2, items.size());

		SpaceCombatLogEntryItem ownItem = items.stream().filter(i -> !i.isEnemy()).findFirst().orElseThrow();
		assertEquals("Freighter02", ownItem.getShipName());
		assertEquals("orion_2", ownItem.getShipTemplate().getId());
		assertNull(ownItem.getShipId());

		SpaceCombatLogEntryItem enemyItem = items.stream().filter(SpaceCombatLogEntryItem::isEnemy).findFirst().orElseThrow();
		assertEquals("Freighter01", enemyItem.getShipName());
		assertEquals("orion_2", enemyItem.getShipTemplate().getId());
		assertNull(enemyItem.getShipId());

		assertThat(playerRepository.getReferenceById(65L).getLastRoundDestroyedShips()).isEqualTo(1);
		assertThat(playerRepository.getReferenceById(66L).getLastRoundDestroyedShips()).isEqualTo(1);
	}

	@Test
	void shouldDestroyWeakerShip() {
		subject.processCombat(gameId);

		assertTrue(shipRepository.findById(62L).isEmpty(), "Ship 62 is expected to be destroyed but was not!");

		Optional<Ship> ship2Opt = shipRepository.findById(60L);
		assertTrue(ship2Opt.isPresent(), "Ship 60 is not expected to be destroyed but was!");
		assertEquals(0, ship2Opt.get().getDamage());
		assertEquals(ship2Opt.get().getShipTemplate().getCrew(), ship2Opt.get().getCrew());

		List<NewsEntry> newsEntries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertEquals(1, newsEntries.size());

		NewsEntry newsEntry = newsEntries.get(0);
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_victory_single, newsEntry.getTemplate());

		List<SpaceCombatLogEntry> logEntries = newsEntry.retrieveSortedSpaceCombatLogEntries();
		assertEquals(1, logEntries.size());
		SpaceCombatLogEntry logEntry = logEntries.get(0);
		List<SpaceCombatLogEntryItem> items = logEntry.getItems();
		assertEquals(2, items.size());

		SpaceCombatLogEntryItem ownItem = items.stream().filter(i -> !i.isEnemy()).findFirst().orElseThrow();
		assertEquals("Destroyer01", ownItem.getShipName());
		assertEquals("oltin_7", ownItem.getShipTemplate().getId());
		assertEquals(60L, ownItem.getShipId());

		SpaceCombatLogEntryItem enemyItem = items.stream().filter(SpaceCombatLogEntryItem::isEnemy).findFirst().orElseThrow();
		assertEquals("Probe01", enemyItem.getShipName());
		assertEquals("oltin_2", enemyItem.getShipTemplate().getId());
		assertNull(enemyItem.getShipId());

		assertThat(playerRepository.getReferenceById(68L).getLastRoundDestroyedShips()).isEqualTo(1);
		assertThat(playerRepository.getReferenceById(67L).getLastRoundDestroyedShips()).isZero();
	}

	@Test
	void shouldCaptureShip() {
		subject.processCombat(gameId);

		Optional<Ship> ship1Opt = shipRepository.findById(64L);
		assertTrue(ship1Opt.isPresent(), "Ship 64 is not expected to be destroyed but was!");
		assertEquals(56, ship1Opt.get().getDamage());
		assertEquals(0, ship1Opt.get().getCrew());

		Optional<Ship> ship2Opt = shipRepository.findById(65L);
		assertTrue(ship2Opt.isPresent(), "Ship 65 is not expected to be destroyed but was!");
		assertEquals(0, ship2Opt.get().getDamage());
		assertEquals(ship2Opt.get().getShipTemplate().getCrew(), ship2Opt.get().getCrew());

		List<NewsEntry> newsEntries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertEquals(1, newsEntries.size());

		NewsEntry newsEntry = newsEntries.get(0);
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_victory_single, newsEntry.getTemplate());

		List<SpaceCombatLogEntry> logEntries = newsEntry.retrieveSortedSpaceCombatLogEntries();
		assertEquals(1, logEntries.size());
		SpaceCombatLogEntry logEntry = logEntries.get(0);
		List<SpaceCombatLogEntryItem> items = logEntry.getItems();
		assertEquals(2, items.size());

		SpaceCombatLogEntryItem ownItem = items.stream().filter(i -> !i.isEnemy()).findFirst().orElseThrow();
		assertEquals("Big01", ownItem.getShipName());
		assertEquals("kuatoh_2", ownItem.getShipTemplate().getId());
		assertEquals(65L, ownItem.getShipId());

		SpaceCombatLogEntryItem enemyItem = items.stream().filter(SpaceCombatLogEntryItem::isEnemy).findFirst().orElseThrow();
		assertEquals("Small01", enemyItem.getShipName());
		assertEquals("kuatoh_1", enemyItem.getShipTemplate().getId());
		assertEquals(64L, enemyItem.getShipId());

		assertThat(playerRepository.getReferenceById(69L).getLastRoundDestroyedShips()).isZero();
		assertThat(playerRepository.getReferenceById(70L).getLastRoundDestroyedShips()).isZero();
	}

	@Test
	void shouldEvadeEachOtherBecauseBothDefensive() {
		subject.processCombat(gameId);

		Optional<Ship> ship1Opt = shipRepository.findById(68L);
		assertTrue(ship1Opt.isPresent(), "Ship 68 is not expected to be destroyed but was!");
		assertEquals(0, ship1Opt.get().getDamage());
		assertEquals(ship1Opt.get().getShipTemplate().getCrew(), ship1Opt.get().getCrew());

		Optional<Ship> ship2Opt = shipRepository.findById(70L);
		assertTrue(ship2Opt.isPresent(), "Ship 70 is not expected to be destroyed but was!");
		assertEquals(0, ship2Opt.get().getDamage());
		assertEquals(ship2Opt.get().getShipTemplate().getCrew(), ship2Opt.get().getCrew());

		assertThat(playerRepository.getReferenceById(71L).getLastRoundDestroyedShips()).isZero();
		assertThat(playerRepository.getReferenceById(72L).getLastRoundDestroyedShips()).isZero();
	}

	@Test
	void shouldEvadeEachOtherAfterFirstCombatRound() {
		subject.processCombat(gameId);

		Optional<Ship> ship1Opt = shipRepository.findById(73L);
		assertTrue(ship1Opt.isPresent(), "Ship 73 is not expected to be destroyed but was!");
		assertEquals(0, ship1Opt.get().getDamage());
		assertEquals(ship1Opt.get().getShipTemplate().getCrew(), ship1Opt.get().getCrew());

		Optional<Ship> ship2Opt = shipRepository.findById(75L);
		assertTrue(ship2Opt.isPresent(), "Ship 75 is not expected to be destroyed but was!");
		assertEquals(0, ship2Opt.get().getDamage());
		assertEquals(ship2Opt.get().getShipTemplate().getCrew(), ship2Opt.get().getCrew());

		assertThat(playerRepository.getReferenceById(73L).getLastRoundDestroyedShips()).isZero();
		assertThat(playerRepository.getReferenceById(74L).getLastRoundDestroyedShips()).isZero();
	}

	@Test
	void shouldWinBecauseOfSupport() {
		subject.processCombat(gameId);

		Optional<Ship> ship1Opt = shipRepository.findById(80L);
		assertTrue(ship1Opt.isPresent(), "Ship 80 is not expected to be destroyed but was!");
		assertEquals(66, ship1Opt.get().getDamage());
		assertEquals(295, ship1Opt.get().getCrew());

		assertTrue(shipRepository.findById(82L).isPresent(), "Ship 82 is not expected to be destroyed but was!");
		assertTrue(shipRepository.findById(83L).isPresent(), "Ship 83 is not expected to be destroyed but was!");
		assertTrue(shipRepository.findById(84L).isPresent(), "Ship 84 is not expected to be destroyed but was!");
		assertTrue(shipRepository.findById(85L).isPresent(), "Ship 85 is not expected to be destroyed but was!");
		assertTrue(shipRepository.findById(87L).isPresent(), "Ship 87 is not expected to be destroyed but was!");

		Optional<Ship> ship2Opt = shipRepository.findById(86L);
		assertTrue(ship2Opt.isEmpty(), "Ship 86 is expected to be destroyed but was not!");

		List<NewsEntry> newsEntries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertEquals(1, newsEntries.size());

		NewsEntry newsEntry = newsEntries.get(0);
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_victory_single, newsEntry.getTemplate());

		List<SpaceCombatLogEntry> logEntries = newsEntry.retrieveSortedSpaceCombatLogEntries();
		assertEquals(1, logEntries.size());
		SpaceCombatLogEntry logEntry = logEntries.get(0);
		List<SpaceCombatLogEntryItem> items = logEntry.getItems();
		assertEquals(2, items.size());

		SpaceCombatLogEntryItem ownItem = items.stream().filter(i -> !i.isEnemy()).findFirst().orElseThrow();
		assertEquals("CruiserWeak01", ownItem.getShipName());
		assertEquals("nightmare_6", ownItem.getShipTemplate().getId());
		assertEquals(80L, ownItem.getShipId());

		SpaceCombatLogEntryItem enemyItem = items.stream().filter(SpaceCombatLogEntryItem::isEnemy).findFirst().orElseThrow();
		assertEquals("CruiserStrong", enemyItem.getShipName());
		assertEquals("nightmare_6", enemyItem.getShipTemplate().getId());
		assertNull(enemyItem.getShipId());

		assertThat(playerRepository.getReferenceById(78L).getLastRoundDestroyedShips()).isEqualTo(1);
		assertThat(playerRepository.getReferenceById(77L).getLastRoundDestroyedShips()).isZero();
	}

	@Test
	void shouldRecordCombatLogCorrectly() {
		subject.processCombat(gameId);

		Optional<Ship> findById = shipRepository.findById(89L);
		assertTrue(findById.isEmpty(), "Ship 89 is expected to be destroyed but was not!");
		assertTrue(shipRepository.findById(90L).isEmpty(), "Ship 90 is expected to be destroyed but was not!");
		assertTrue(shipRepository.findById(92L).isEmpty(), "Ship 92 is expected to be destroyed but was not!");
		assertTrue(shipRepository.findById(93L).isPresent(), "Ship 93 is not expected to be destroyed but was!");

		List<NewsEntry> newsEntries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertEquals(1, newsEntries.size());

		NewsEntry newsEntry = newsEntries.get(0);
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_victory, newsEntry.getTemplate());
		assertEquals(List.of("2", "221", "93", "2", "1"), newsEntry.retrieveArgumentValues());
	}

	@Test
	void shouldEvadeShips() {
		subject.processCombat(gameId);

		Optional<Ship> shipOpt = shipRepository.findById(138L);
		assertTrue(shipOpt.isPresent(), "Ship 138 is expected to not be destroyed!");
		Ship probe = shipOpt.get();
		assertTrue(probe.getX() != 97 || probe.getY() != 251, "Expected Ship 'Probe' to have evaded but it did not move!");

		assertTrue(shipRepository.findById(136L).isPresent(), "Ship 136 is not expected to be destroyed but was!");
		assertTrue(shipRepository.findById(139L).isPresent(), "Ship 139 is not expected to be destroyed but was!");

		List<NewsEntry> newsEntries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertEquals(2, newsEntries.size());

		NewsEntry newsEntry = newsEntries.get(1);
		assertEquals(NewsEntryConstants.news_entry_evaded_successfull, newsEntry.getTemplate());

		List<String> args = newsEntry.retrieveArgumentValues();
		assertEquals(3, args.size());
		assertEquals("Probe", args.get(0));
		assertEquals("Omega Freighter", args.get(1));
	}

	@Test
	void shouldDestroyAndCaptureShips() {
		subject.processCombat(gameId);

		Optional<Ship> shipOpt = shipRepository.findById(141L);
		assertTrue(shipOpt.isPresent(), "Ship 141 is expected to not be destroyed!");
		Ship probe = shipOpt.get();
		assertEquals(106L, probe.getPlayer().getId());
		assertEquals(0, probe.getDamage());

		shipOpt = shipRepository.findById(144L);
		assertTrue(shipOpt.isPresent(), "Ship 144 is expected to not be destroyed!");
		Ship colossos = shipOpt.get();
		assertEquals(106L, colossos.getPlayer().getId());
		assertEquals(0, colossos.getCrew());
		assertEquals(0, colossos.getDamage());

		assertTrue(shipRepository.findById(142L).isEmpty(), "Ship 142 is expected to be destroyed!");

		List<NewsEntry> newsEntries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertEquals(1, newsEntries.size());

		NewsEntry entry = newsEntries.get(0);
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_total_victory, entry.getTemplate());
		assertEquals(List.of("1", "229", "271", "2"), entry.retrieveArgumentValues());

		List<SpaceCombatLogEntry> logEntries = entry.retrieveSortedSpaceCombatLogEntries();
		assertEquals(2, logEntries.size());
		SpaceCombatLogEntry logEntry = logEntries.get(1);

		SpaceCombatLogEntryItem ownItem = logEntry.getItems().stream().filter(i -> i.getShipName().equals("Probe")).findAny().get();
		assertEquals(false, ownItem.isCaptured());
		assertEquals(false, ownItem.isDestroyed());
		assertEquals(false, ownItem.isEnemy());
		assertEquals(141L, ownItem.getShipId());

		SpaceCombatLogEntryItem enemyItem = logEntry.getItems().stream().filter(i -> i.getShipName().equals("Colossus")).findAny().get();
		assertEquals(true, enemyItem.isCaptured());
		assertEquals(false, enemyItem.isDestroyed());
		assertEquals(true, enemyItem.isEnemy());
		assertEquals(144L, enemyItem.getShipId());

		logEntry = logEntries.get(0);

		ownItem = logEntry.getItems().stream().filter(i -> i.getShipName().equals("Probe")).findAny().get();
		assertEquals(false, ownItem.isCaptured());
		assertEquals(false, ownItem.isDestroyed());
		assertEquals(false, ownItem.isEnemy());
		assertEquals(141L, ownItem.getShipId());

		enemyItem = logEntry.getItems().stream().filter(i -> i.getShipName().equals("Alpha Freighter")).findAny().get();
		assertEquals(false, enemyItem.isCaptured());
		assertEquals(true, enemyItem.isDestroyed());
		assertEquals(true, enemyItem.isEnemy());
		assertEquals(null, enemyItem.getShipId());
	}

	@Test
	void shouldLogCombatWithEvasion() {
		MasterDataService.RANDOM.setSeed(1L);
		roundCalculationService.calculateRound(gameId);

		assertTrue(shipRepository.findById(152L).isPresent(), "Ship 152 is expected to not be destroyed but was!");
		assertTrue(shipRepository.findById(153L).isEmpty(), "Ship 153 is expected to be destroyed but was not!");
		assertTrue(shipRepository.findById(154L).isPresent(), "Ship 154 is expected to not be destroyed but was!");

		List<NewsEntry> newsEntries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertEquals(2, newsEntries.size());

		NewsEntry entry = newsEntries.get(1);
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_victory_single, entry.getTemplate());
		assertEquals(List.of("Science Class", "123", "241"), entry.retrieveArgumentValues());

		newsEntries = newsEntryRepository.findByGameIdAndLoginId(gameId, 4L);
		assertEquals(3, newsEntries.size());

		entry = newsEntries.get(2);
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_defeat_single, entry.getTemplate());
		assertEquals(List.of("Freighter", "123", "241"), entry.retrieveArgumentValues());
	}

	@Test
	void shouldMutuallyDestroyEachOther() {
		MasterDataService.RANDOM.setSeed(4L);
		roundCalculationService.calculateRound(gameId);

		assertTrue(shipRepository.findById(158L).isEmpty(), "Ship 158 is expected to be destroyed but was not!");
		assertTrue(shipRepository.findById(156L).isEmpty(), "Ship 156 is expected to be destroyed but was not!");

		List<NewsEntry> newsEntries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertEquals(1, newsEntries.size());

		NewsEntry entry = newsEntries.get(0);
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_mutual_destroyed, entry.getTemplate());
		assertEquals(List.of("Hunter", "123", "197"), entry.retrieveArgumentValues());

		newsEntries = newsEntryRepository.findByGameIdAndLoginId(gameId, 2L);
		assertTrue(newsEntries.size() > 0);

		Optional<NewsEntry> opt = newsEntries.stream().filter(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_space_combat_summary_mutual_destroyed))
			.findAny();
		assertTrue(opt.isPresent());
		assertEquals(List.of("Bebrone", "123", "197"), opt.get().retrieveArgumentValues());
	}

	@Test
	void shouldDestroyShipThatConstructedJumpPortal() {
		roundCalculationService.calculateRound(gameId);

		assertTrue(shipRepository.findById(178L).isEmpty(), "Ship 178 is expected to be destroyed but was not!");
		assertTrue(shipRepository.findById(181L).isEmpty(), "Ship 181 is expected to be destroyed but was not!");
	}
}
