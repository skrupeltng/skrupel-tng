package org.skrupeltng.modules.ingame.service.round;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpecies;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpeciesEffect;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetTypeRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.skrupeltng.modules.ingame.modules.planet.service.PlanetService;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbasePropulsionStockRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplateRepository;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class PlanetRoundCalculatorUnitTest {

	@Spy
	@InjectMocks
	private final PlanetRoundCalculator subject = new PlanetRoundCalculator();

	@Mock
	private PlanetRepository planetRepository;

	@Mock
	private StarbaseRepository starbaseRepository;

	@Mock
	private GameRepository gameRepository;

	@Mock
	private PlayerPlanetScanLogRepository playerPlanetScanLogRepository;

	@Mock
	private OrbitalSystemRepository orbitalSystemRepository;

	@Mock
	private NewsService newsService;

	@Mock
	private PlanetService planetService;

	@Mock
	private PropulsionSystemTemplateRepository propulsionSystemTemplateRepository;

	@Mock
	private StarbasePropulsionStockRepository starbasePropulsionStockRepository;

	@Mock
	private ShipRepository shipRepository;

	@Mock
	private PlanetTypeRepository planetTypeRepository;

	@Mock
	private VisibleObjects visibleObjects;

	@Mock
	private ConfigProperties configProperties;

	private Planet createPlanet() {
		Planet planet = new Planet();
		planet.setMoney(10000);
		planet.setSupplies(100);
		planet.setColonists(1000000);
		planet.setOrbitalSystems(new ArrayList<>());
		Player player = new Player();
		Faction faction = new Faction();
		faction.setMineProductionRate(1f);
		player.setFaction(faction);
		planet.setPlayer(player);
		return planet;
	}

	@Test
	void shouldNotMineAnyResourcesBecauseNoMines() {
		Planet planet = createPlanet();

		subject.processMines(planet);

		assertEquals(0, planet.getFuel());
		assertEquals(0, planet.getMineral1());
		assertEquals(0, planet.getMineral2());
		assertEquals(0, planet.getMineral3());
	}

	@Test
	void shouldNotMineAnyResourcesBecauseNoResourcesLeft() {
		Planet planet = createPlanet();
		planet.setMines(10);

		subject.processMines(planet);

		assertEquals(0, planet.getFuel());
		assertEquals(0, planet.getMineral1());
		assertEquals(0, planet.getMineral2());
		assertEquals(0, planet.getMineral3());
	}

	@Test
	void shouldMineWithoutNativeSpeciesOrOrbitalSystems() {
		Planet planet = createPlanet();
		planet.setUntappedFuel(1000);
		planet.setUntappedMineral1(1000);
		planet.setUntappedMineral2(1000);
		planet.setUntappedMineral3(1000);
		planet.setNecessaryMinesForOneFuel(1);
		planet.setNecessaryMinesForOneMineral1(2);
		planet.setNecessaryMinesForOneMineral2(4);
		planet.setNecessaryMinesForOneMineral3(6);
		planet.setMines(20);

		subject.processMines(planet);

		assertEquals(5, planet.getFuel());
		assertEquals(2, planet.getMineral1());
		assertEquals(1, planet.getMineral2());
		assertEquals(0, planet.getMineral3());
	}

	@Test
	void shouldMineWithNativeSpecies1() {
		Planet planet = createPlanet();
		planet.setUntappedFuel(1000);
		planet.setUntappedMineral1(1000);
		planet.setUntappedMineral2(1000);
		planet.setUntappedMineral3(1000);
		planet.setNecessaryMinesForOneFuel(1);
		planet.setNecessaryMinesForOneMineral1(2);
		planet.setNecessaryMinesForOneMineral2(4);
		planet.setNecessaryMinesForOneMineral3(6);
		planet.setMines(20);
		planet.setNativeSpeciesCount(1000);
		planet.setNativeSpecies(new NativeSpecies(NativeSpeciesEffect.CHANGE_EFFECTIVENESS_MINES, 50f));

		subject.processMines(planet);

		assertEquals(7, planet.getFuel());
		assertEquals(3, planet.getMineral1());
		assertEquals(1, planet.getMineral2());
		assertEquals(1, planet.getMineral3());
	}

	@Test
	void shouldMineWithNativeSpecies2() {
		Planet planet = createPlanet();
		planet.setUntappedFuel(1000);
		planet.setUntappedMineral1(1000);
		planet.setUntappedMineral2(1000);
		planet.setUntappedMineral3(1000);
		planet.setNecessaryMinesForOneFuel(1);
		planet.setNecessaryMinesForOneMineral1(2);
		planet.setNecessaryMinesForOneMineral2(4);
		planet.setNecessaryMinesForOneMineral3(6);
		planet.setMines(20);
		planet.setNativeSpeciesCount(1000);
		planet.setNativeSpecies(new NativeSpecies(NativeSpeciesEffect.CHANGE_EFFECTIVENESS_MINES_AND_FACTORIES, 75f));

		subject.processMines(planet);

		assertEquals(8, planet.getFuel());
		assertEquals(4, planet.getMineral1());
		assertEquals(2, planet.getMineral2());
		assertEquals(1, planet.getMineral3());
	}

	@Test
	void shouldMineWithOrbitalSystem() {
		Planet planet = createPlanet();
		planet.setUntappedFuel(1000);
		planet.setUntappedMineral1(1000);
		planet.setUntappedMineral2(1000);
		planet.setUntappedMineral3(1000);
		planet.setNecessaryMinesForOneFuel(1);
		planet.setNecessaryMinesForOneMineral1(2);
		planet.setNecessaryMinesForOneMineral2(4);
		planet.setNecessaryMinesForOneMineral3(6);
		planet.setMines(100);
		planet.getOrbitalSystems().add(new OrbitalSystem(OrbitalSystemType.EXO_REFINERY));

		subject.processMines(planet);

		assertEquals(27, planet.getFuel());
		assertEquals(13, planet.getMineral1());
		assertEquals(6, planet.getMineral2());
		assertEquals(4, planet.getMineral3());
	}

	@Test
	void shouldCheckBoundsWhenIncreasingColonists() {
		Planet planet = createPlanet();

		subject.increaseColonistsWithBoundaryCheck(planet, 20);
		assertThat(planet.getColonists()).isEqualTo(1000020);

		subject.increaseColonistsWithBoundaryCheck(planet, Integer.MAX_VALUE);
		assertThat(planet.getColonists()).isEqualTo(Integer.MAX_VALUE);
	}
}
