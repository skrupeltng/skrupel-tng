package org.skrupeltng.modules.ingame.service.round;

import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ShipToDestinationComparatorUnitTest {

	@Test
	void shouldSortCorrectly() {
		Ship ship1 = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship1.setId(1L);

		Ship ship2 = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship2.setId(2L);
		ship2.setDestinationShip(ship1);

		Ship ship3 = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship3.setId(3L);
		ship3.setDestinationShip(ship2);

		Ship ship4 = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship4.setId(4L);
		ship4.setDestinationShip(ship2);

		Ship ship5 = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship5.setId(5L);
		ship5.setDestinationShip(ship2);

		List<Ship> list = new ArrayList<>(List.of(ship5, ship3, ship4, ship1, ship2));

		Collections.sort(list, new ShipToDestinationComparator());

		assertEquals(List.of(ship1, ship2, ship5, ship3, ship4), list);
	}

	@Test
	void shouldSortWithoutOverflow() {
		Ship ship1 = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship1.setId(1L);

		Ship ship2 = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship2.setId(2L);
		ship2.setDestinationShip(ship1);
		ship1.setDestinationShip(ship2);

		Ship ship3 = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship3.setId(3L);
		ship3.setDestinationShip(ship2);

		Ship ship4 = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship4.setId(4L);
		ship4.setDestinationShip(ship2);

		Ship ship5 = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship5.setId(5L);

		List<Ship> list = new ArrayList<>(List.of(ship3, ship1, ship2, ship5, ship4));

		Collections.sort(list, new ShipToDestinationComparator());

		assertEquals(List.of(ship5, ship3, ship1, ship2, ship4), list);
	}
}
