package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.dashboard.database.Team;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ConquestWinConditionHandlerUnitTest {

	final ConquestWinConditionHandler subject = new ConquestWinConditionHandler();

	@Test
	void shouldReturnNoWinnersBecauseNoPlayerReachedTargetPoints() {
		Player p1 = new Player(1L);
		Player p2 = new Player(2L);

		Game game = new Game();
		game.setConquestRounds(10);
		game.setPlayers(List.of(p1, p2));

		WinnersCalculationResult result = subject.calculate(game);

		assertThat(result).isNotNull();
		assertThat(result.winners()).isEmpty();
		assertThat(result.unlockedAchievements()).isEmpty();
	}

	@Test
	void shouldReturnNoWinnersBecauseNoTeamReachedTargetPoints() {
		Player p1 = new Player(1L);
		Player p2 = new Player(2L);

		Team t1 = new Team();
		t1.setPlayers(List.of(p1, p2));

		Player p3 = new Player(3L);
		Player p4 = new Player(4L);

		Team t2 = new Team();
		t2.setPlayers(List.of(p3, p4));

		Game game = new Game();
		game.setConquestRounds(10);
		game.setUseFixedTeams(true);
		game.setPlayers(List.of(p1, p2, p3, p4));
		game.setTeams(List.of(t1, t2));

		WinnersCalculationResult result = subject.calculate(game);

		assertThat(result).isNotNull();
		assertThat(result.winners()).isEmpty();
		assertThat(result.unlockedAchievements()).isEmpty();
	}

	@Test
	void shouldReturnSingleWinner() {
		Player p1 = new Player(1L);
		p1.setVictoryPoints(10);
		Player p2 = new Player(2L);

		Game game = new Game();
		game.setConquestRounds(10);
		game.setPlayers(List.of(p1, p2));

		WinnersCalculationResult result = subject.calculate(game);

		assertThat(result).isNotNull();
		assertThat(result.winners()).containsExactly(p1);
		assertThat(result.unlockedAchievements()).isEmpty();
	}

	@Test
	void shouldReturnMultipleWinner() {
		Player p1 = new Player(1L);
		p1.setVictoryPoints(12);
		Player p2 = new Player(2L);
		p2.setVictoryPoints(11);
		Player p3 = new Player(3L);

		Game game = new Game();
		game.setConquestRounds(10);
		game.setPlayers(List.of(p1, p2, p3));

		WinnersCalculationResult result = subject.calculate(game);

		assertThat(result).isNotNull();
		assertThat(result.winners()).containsExactlyInAnyOrder(p1, p2);
		assertThat(result.unlockedAchievements()).isEmpty();
	}

	@Test
	void shouldReturnSingleWinnerTeam() {
		Player p1 = new Player(1L);
		p1.setVictoryPoints(5);
		Player p2 = new Player(2L);
		p2.setVictoryPoints(5);

		Team t1 = new Team();
		t1.setPlayers(List.of(p1, p2));

		Player p3 = new Player(3L);
		Player p4 = new Player(4L);

		Team t2 = new Team();
		t2.setPlayers(List.of(p3, p4));

		Game game = new Game();
		game.setConquestRounds(10);
		game.setUseFixedTeams(true);
		game.setPlayers(List.of(p1, p2, p3, p4));
		game.setTeams(List.of(t1, t2));

		WinnersCalculationResult result = subject.calculate(game);

		assertThat(result).isNotNull();
		assertThat(result.winners()).containsExactlyInAnyOrder(p1, p2);
		assertThat(result.unlockedAchievements()).isEmpty();
	}

	@Test
	void shouldReturnMultipleWinnerTeams() {
		Player p1 = new Player(1L);
		p1.setVictoryPoints(5);
		Player p2 = new Player(2L);
		p2.setVictoryPoints(7);

		Team t1 = new Team();
		t1.setPlayers(List.of(p1, p2));

		Player p3 = new Player(3L);
		p3.setVictoryPoints(6);
		Player p4 = new Player(4L);
		p4.setVictoryPoints(5);

		Team t2 = new Team();
		t2.setPlayers(List.of(p3, p4));

		Player p5 = new Player(5L);
		Player p6 = new Player(6L);

		Team t3 = new Team();
		t3.setPlayers(List.of(p5, p6));

		Game game = new Game();
		game.setConquestRounds(10);
		game.setUseFixedTeams(true);
		game.setPlayers(List.of(p1, p2, p3, p4, p5, p6));
		game.setTeams(List.of(t1, t2, t3));

		WinnersCalculationResult result = subject.calculate(game);

		assertThat(result).isNotNull();
		assertThat(result.winners()).containsExactlyInAnyOrder(p1, p2, p3, p4);
		assertThat(result.unlockedAchievements()).isEmpty();
	}
}
