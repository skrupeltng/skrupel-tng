package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.MineralRaceStorageType;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.masterdata.database.Resource;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class MineralRaceWinConditionHandlerUnitTest {

	MineralRaceWinConditionHandler subject = new MineralRaceWinConditionHandler();

	@Test
	void shouldCalculateWinners() {
		Game game = new Game();
		game.setMineralRaceMineralName(Resource.FUEL);
		game.setMineralRaceStorageType(MineralRaceStorageType.HOME_PLANET);
		game.setMineralRaceQuantity(10);

		Player p1 = new Player(1L);
		Planet homePlanet = new Planet();
		homePlanet.setFuel(20);
		p1.setHomePlanet(homePlanet);

		Player p2 = new Player(2L);
		p2.setHomePlanet(new Planet());

		game.setPlayers(List.of(p1, p2));

		WinnersCalculationResult result = subject.calculate(game);

		assertThat(result).isNotNull();
		assertThat(result.winners()).containsExactly(p1);
		assertThat(result.unlockedAchievements()).isEmpty();
	}
}
