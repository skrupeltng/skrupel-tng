package org.skrupeltng.modules.ingame.service.round.combat.orbit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.database.OrbitalCombatLogEntryRepository;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipRemoval;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
class OrbitalCombatRoundCalculatorUnitTest {

	OrbitalCombatRoundCalculator subject;

	@Mock
	OrbitalCombat orbitalCombat;

	@Mock
	ShipRepository shipRepository;

	@Mock
	ShipRemoval shipRemoval;

	@Mock
	PoliticsService politicsService;

	@Mock
	NewsService newsService;

	@Mock
	OrbitalCombatLogEntryRepository orbitalCombatLogEntryRepository;

	@Mock
	PlayerRepository playerRepository;

	@BeforeEach
	void setup() {
		subject = new OrbitalCombatRoundCalculator(shipRepository, shipRemoval, orbitalCombat, politicsService, newsService, playerRepository, orbitalCombatLogEntryRepository);
	}

	@Test
	void shouldCheckOrbitalShield() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		ship.getShipTemplate().getShipAbilities().add(new ShipAbility(ShipAbilityType.ORBITAL_SHIELD));

		Set<Ship> destroyedShips = new HashSet<>();
		Map<Long, Integer> totalPlanetaryDefenses = new HashMap<>();
		Map<String, OrbitalCombatPlanetResult> logEntriesMap = new HashMap<>();
		subject.processShip(destroyedShips, ship, logEntriesMap, totalPlanetaryDefenses);

		Mockito.verify(orbitalCombat, Mockito.never()).checkStructurScanner(Mockito.any());
		Mockito.verify(orbitalCombat, Mockito.never()).processCombat(Mockito.any(), Mockito.anyInt());
		Mockito.verify(orbitalCombat, Mockito.never()).handlePlanetDamage(Mockito.anyLong(), Mockito.anyMap(), Mockito.anyInt());
		Mockito.verify(orbitalCombat, Mockito.never()).handleShipDamage(Mockito.anySet(), Mockito.any());
	}

	@Test
	void shouldCheckAlliedPlayer() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		Planet planet = new Planet(1L);
		planet.setPlayer(new Player(2L));
		ship.setPlanet(planet);

		Mockito.when(politicsService.isAlly(1L, 2L)).thenReturn(true);

		Set<Ship> destroyedShips = new HashSet<>();
		Map<Long, Integer> totalPlanetaryDefenses = new HashMap<>();
		totalPlanetaryDefenses.put(1L, 100);
		Map<String, OrbitalCombatPlanetResult> logEntriesMap = new HashMap<>();
		subject.processShip(destroyedShips, ship, logEntriesMap, totalPlanetaryDefenses);

		Mockito.verify(orbitalCombat, Mockito.never()).checkStructurScanner(Mockito.any());
		Mockito.verify(orbitalCombat, Mockito.never()).processCombat(Mockito.any(), Mockito.anyInt());
		Mockito.verify(orbitalCombat, Mockito.never()).handlePlanetDamage(Mockito.anyLong(), Mockito.anyMap(), Mockito.anyInt());
		Mockito.verify(orbitalCombat, Mockito.never()).handleShipDamage(Mockito.anySet(), Mockito.any());
	}

	@Test
	void shouldCallOrbitalCombat() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		Planet planet = new Planet(1L);
		planet.setPlayer(new Player(2L));
		ship.setPlanet(planet);

		Set<Ship> destroyedShips = new HashSet<>();
		Map<Long, Integer> totalPlanetaryDefenses = new HashMap<>();
		totalPlanetaryDefenses.put(1L, 100);
		Map<String, OrbitalCombatPlanetResult> logEntriesMap = new HashMap<>();
		subject.processShip(destroyedShips, ship, logEntriesMap, totalPlanetaryDefenses);

		Mockito.verify(orbitalCombat).checkStructurScanner(ship);
		Mockito.verify(orbitalCombat).processCombat(ship, 100);
		Mockito.verify(orbitalCombat).handlePlanetDamage(1L, totalPlanetaryDefenses, 0);
		Mockito.verify(orbitalCombat).handleShipDamage(destroyedShips, ship);
	}
}
