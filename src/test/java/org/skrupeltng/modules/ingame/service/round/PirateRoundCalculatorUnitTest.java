package org.skrupeltng.modules.ingame.service.round;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.skrupeltng.modules.ImageConstants;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.service.MasterDataService;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PirateRoundCalculatorUnitTest {

	private PirateRoundCalculator subject;
	private ShipRepository shipRepository;
	private GameRepository gameRepository;
	private NewsService newsService;

	@BeforeEach
	void setup() {
		MasterDataService.RANDOM.setSeed(1L);

		subject = Mockito.spy(new PirateRoundCalculator());

		shipRepository = Mockito.mock(ShipRepository.class);
		subject.setShipRepository(shipRepository);

		gameRepository = Mockito.mock(GameRepository.class);
		subject.setGameRepository(gameRepository);

		newsService = Mockito.mock(NewsService.class);
		subject.setNewsService(newsService);
	}

	private Game createGame(int propCenter, int propOutskirts) {
		Game game = new Game(1L);
		game.setGalaxySize(1000);
		game.setPirateAttackProbabilityCenter(propCenter);
		game.setPirateAttackProbabilityOutskirts(propOutskirts);
		Mockito.when(gameRepository.getReferenceById(Mockito.anyLong())).thenReturn(game);
		return game;
	}

	private Ship createShip(int x, int y) {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		ship.setX(x);
		ship.setY(y);
		ship.setMineral1(100);
		ship.setMineral2(200);
		ship.setMineral3(300);
		ship.setMoney(400);
		ship.setSupplies(500);
		return ship;
	}

	@Test
	void shouldCalculateProbabilityWithShipExperience() {
		Game game = createGame(20, 20);
		Ship ship = createShip(20, 50);
		ship.setExperience(3);
		float result = subject.computeProbability(game, ship);
		assertEquals(0.05f, result, 0.000001f);
	}

	@Test
	void shouldCalculateProbabilityWithEqual() {
		Game game = createGame(1, 1);
		Ship ship = createShip(20, 50);
		float result = subject.computeProbability(game, ship);
		assertEquals(0.01f, result, 0.000001f);
	}

	@Test
	void shouldCalculateProbabilityWithCenter() {
		Game game = createGame(2, 1);
		Ship ship = createShip(500, 500);
		float result = subject.computeProbability(game, ship);
		assertEquals(0.02f, result, 0.000001f);
	}

	@Test
	void shouldCalculateProbabilityWithOutskirts() {
		Game game = createGame(1, 2);
		Ship ship = createShip(500, 0);
		float result = subject.computeProbability(game, ship);
		assertEquals(0.02f, result, 0.000001f);
	}

	@Test
	void shouldCalculateProbabilityInbetweenWithHigherCenter() {
		Game game = createGame(3, 1);
		Ship ship = createShip(250, 250);
		float result = subject.computeProbability(game, ship);
		assertEquals(0.02f, result, 0.000001f);
	}

	@Test
	void shouldCalculateProbabilityInbetweenWithHigherOutskirts() {
		Game game = createGame(3, 5);
		Ship ship = createShip(750, 750);
		float result = subject.computeProbability(game, ship);
		assertEquals(0.04f, result, 0.000001f);
	}

	@Test
	void shouldCalculateLootPercentages() {
		List<Float> result = subject.getLootPercentages(50f);
		assertEquals(5, result.size());

		assertEquals(0.06f, result.get(0), 0.0f);
		assertEquals(0.11f, result.get(1), 0.0f);
		assertEquals(0.01f, result.get(2), 0.0f);
		assertEquals(0.5f, result.get(3), 0.0f);
		assertEquals(0.5f, result.get(4), 0.0f);
	}

	@Test
	void shouldCheckLootedGoods() {
		Ship ship = createShip(0, 0);
		subject.loot(ship, 0.5f);

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());
	}

	@Test
	void shouldLoot() {
		Ship ship = createShip(0, 0);
		subject.loot(ship, 100f);

		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_pirate_attack, ImageConstants.NEWS_PIRATES, ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName(), 6, 22, 3, 400, 315);

		assertEquals(94, ship.getMineral1());
		assertEquals(178, ship.getMineral2());
		assertEquals(297, ship.getMineral3());
		assertEquals(0, ship.getMoney());
		assertEquals(185, ship.getSupplies());
	}

	@Test
	void shouldNotEncounterPirates() {
		Game game = createGame(0, 0);
		Ship ship = createShip(0, 0);

		subject.processShip(game, ship);

		Mockito.verify(shipRepository).getHighestSupportingTechLevel(ship.getId(), ship.getPlayer().getId(), 0, 0);
		Mockito.verify(subject, Mockito.never()).loot(Mockito.any(), Mockito.anyFloat());
		Mockito.verify(newsService, Mockito.never()).add(Mockito.any(), Mockito.anyString(), Mockito.anyString(), Mockito.anyLong(), Mockito.any());
	}

	@Test
	void shouldAvertPiratesBecauseOfSupport() {
		MasterDataService.RANDOM.setSeed(5L);

		Game game = createGame(80, 80);
		game.setPirateMinLoot(50);
		Ship ship = createShip(0, 0);

		Mockito.when(shipRepository.getHighestSupportingTechLevel(3L, 1L, 0, 0)).thenReturn(5);

		subject.processShip(game, ship);

		Mockito.verify(shipRepository).getHighestSupportingTechLevel(ship.getId(), ship.getPlayer().getId(), 0, 0);
		Mockito.verify(subject, Mockito.never()).loot(Mockito.any(), Mockito.anyFloat());
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_pirate_attack_averted, ImageConstants.NEWS_PIRATES, ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName());
	}

	@Test
	void shouldAvertPiratesBecauseOfHighExperience() {
		Game game = createGame(100, 100);
		game.setPirateMinLoot(25);
		Ship ship = createShip(0, 0);
		ship.setExperience(5);

		subject.processShip(game, ship);

		Mockito.verify(shipRepository).getHighestSupportingTechLevel(ship.getId(), ship.getPlayer().getId(), 0, 0);
		Mockito.verify(subject, Mockito.never()).loot(Mockito.any(), Mockito.anyFloat());
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_pirate_attack_averted, ImageConstants.NEWS_PIRATES, ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName());
	}

	@Test
	void shouldAvertPiratesBecauseOfActiveDefenseSystem() {
		Game game = createGame(100, 100);
		game.setPirateMinLoot(1);
		Ship ship = createShip(0, 0);
		ship.setShipModule(ShipModule.ACTIVE_DEFENSE_SYSTEM);

		subject.processPirateLooting(game, ship);

		Mockito.verify(subject, Mockito.never()).loot(Mockito.any(), Mockito.anyFloat());
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_pirate_attack_averted, ImageConstants.NEWS_PIRATES, ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName());
	}

	@Test
	void shouldLootShip() {
		Game game = createGame(100, 100);
		game.setPirateMinLoot(20);
		game.setPirateMaxLoot(40);
		Ship ship = createShip(0, 0);

		subject.processShip(game, ship);

		Mockito.verify(shipRepository).getHighestSupportingTechLevel(ship.getId(), ship.getPlayer().getId(), 0, 0);
		Mockito.verify(subject).loot(ship, 30f);
		Mockito.verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_pirate_attack, ImageConstants.NEWS_PIRATES, ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName(), 31, 2, 3, 12, 10);
	}

	@Test
	void shouldCheckIfGameHasPirateAttacks() {
		Game game = createGame(0, 0);
		Mockito.when(gameRepository.getReferenceById(1L)).thenReturn(game);

		subject.processPirateAttacks(1L);

		Mockito.verify(shipRepository, Mockito.never()).getShipsVulnerableToPirates(Mockito.anyLong());
		Mockito.verify(subject, Mockito.never()).processShip(Mockito.any(), Mockito.any());
	}

	@Test
	void shouldProcessShips() {
		Game game = createGame(0, 1);
		Mockito.when(gameRepository.getReferenceById(1L)).thenReturn(game);

		Ship ship = createShip(0, 0);
		Mockito.when(shipRepository.getShipsVulnerableToPirates(1L)).thenReturn(List.of(ship));

		subject.processPirateAttacks(1L);

		Mockito.verify(shipRepository).getShipsVulnerableToPirates(1L);
		Mockito.verify(subject).processShip(game, ship);
	}
}
