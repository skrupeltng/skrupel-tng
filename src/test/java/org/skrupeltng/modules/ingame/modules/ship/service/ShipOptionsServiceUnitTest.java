package org.skrupeltng.modules.ingame.modules.ship.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.skrupeltng.modules.dashboard.service.AchievementService;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetService;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipOptionsChangeRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.service.VisibleObjects;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ShipOptionsServiceUnitTest {

	ShipOptionsService subject;

	@Mock(lenient = true)
	ShipRepository shipRepository;

	@Mock
	PlayerRepository playerRepository;

	@Mock
	PlayerRelationRepository playerRelationRepository;

	@Mock
	VisibleObjects visibleObjects;

	@Mock
	ShipRouteEntryRepository shipRouteEntryRepository;

	@Mock
	AchievementService achievementService;

	@Mock
	FleetService fleetService;

	@BeforeEach
	void setup() {
		subject = spy(new ShipOptionsService(shipRepository, playerRepository, playerRelationRepository, visibleObjects, shipRouteEntryRepository, achievementService, fleetService));

		when(shipRepository.save(Mockito.any())).thenAnswer((Answer<Ship>) invocation -> invocation.getArgument(0, Ship.class));
	}

	@Test
	void shouldThrowBecauseNameIsBlank() {
		ShipOptionsChangeRequest request = new ShipOptionsChangeRequest();
		assertThatThrownBy(() -> subject.changeOptions(1L, request)).isInstanceOf(IllegalArgumentException.class).hasMessage("Ship name cannot be empty!");
	}

	@Test
	void shouldThrowBecauseNameIsBlankAfterSanitation() {
		ShipOptionsChangeRequest request = new ShipOptionsChangeRequest();
		request.setName("<div>");
		assertThatThrownBy(() -> subject.changeOptions(1L, request)).isInstanceOf(IllegalArgumentException.class).hasMessage("Ship name cannot be empty!");
	}

	@Test
	void shouldOnlyChangeName() {
		Ship ship = new Ship(1L);
		when(shipRepository.getReferenceById(1L)).thenReturn(ship);

		ShipOptionsChangeRequest request = new ShipOptionsChangeRequest();
		request.setName("New Ship Name");

		subject.changeOptions(1L, request);

		assertThat(ship.getName()).isEqualTo("New Ship Name");
		verify(shipRepository).save(ship);
		verify(subject, never()).giveShipToAlly(any(), anyLong());
	}

	@Test
	void shouldChangeNameAndGiveToAlly() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 4L);

		Player player = new Player(2L);
		player.setGame(new Game(1L));
		when(playerRepository.getReferenceById(2L)).thenReturn(player);

		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.ALLIANCE);
		when(playerRelationRepository.findByPlayerIds(4L, 2L)).thenReturn(List.of(relation));

		when(shipRepository.getReferenceById(1L)).thenReturn(ship);

		ShipOptionsChangeRequest request = new ShipOptionsChangeRequest();
		request.setName("New Ship Name");
		request.setNewOwnerId(2L);

		subject.changeOptions(1L, request);

		assertThat(ship.getName()).isEqualTo("New Ship Name");
		verify(shipRepository, times(2)).save(ship);
		verify(subject).giveShipToAlly(ship, 2L);
	}

	@Test
	void shouldCheckPlayerInGame() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 4L);

		Player player = new Player(2L);
		player.setGame(new Game(5L));
		when(playerRepository.getReferenceById(2L)).thenReturn(player);

		try {
			subject.giveShipToAlly(ship, 2L);
			fail("IllegalArgumentException expected!");
		} catch (IllegalArgumentException e) {
			assertEquals("Player 2 is not part of game 1!", e.getMessage());
		}

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	void shouldCheckPlayerHasRelation() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 4L);

		Player player = new Player(2L);
		player.setGame(new Game(1L));
		when(playerRepository.getReferenceById(2L)).thenReturn(player);

		try {
			subject.giveShipToAlly(ship, 2L);
			fail("IllegalArgumentException expected!");
		} catch (IllegalArgumentException e) {
			assertEquals("Player 2 is not in an Alliance with player 4!", e.getMessage());
		}

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	void shouldCheckPlayerIsInAlliance() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 4L);

		Player player = new Player(2L);
		player.setGame(new Game(1L));
		when(playerRepository.getReferenceById(2L)).thenReturn(player);

		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.NON_AGGRESSION_TREATY);
		when(playerRelationRepository.findByPlayerIds(4L, 2L)).thenReturn(List.of(relation));

		try {
			subject.giveShipToAlly(ship, 2L);
			fail("IllegalArgumentException expected!");
		} catch (IllegalArgumentException e) {
			assertEquals("Player 2 is not in an Alliance with player 4!", e.getMessage());
		}

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	void shouldGiveShipToPlayer() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 4L);

		Player player = new Player(2L);
		player.setGame(new Game(1L));
		when(playerRepository.getReferenceById(2L)).thenReturn(player);

		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.ALLIANCE);
		when(playerRelationRepository.findByPlayerIds(4L, 2L)).thenReturn(List.of(relation));

		subject.giveShipToAlly(ship, 2L);

		assertEquals(player, ship.getPlayer());

		Mockito.verify(shipRepository).save(ship);
	}
}
