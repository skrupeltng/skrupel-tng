package org.skrupeltng.modules.ingame.modules.politics.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationAction;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequest;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequestRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.spy;

@ExtendWith(MockitoExtension.class)
class PlayerRelationChangeUnitTest {

	private PlayerRelationChange subject;

	@Mock
	private PlayerRelationRepository playerRelationRepository;

	@Mock
	private PlayerRelationRequestRepository playerRelationRequestRepository;

	@Mock
	private PlayerRepository playerRepository;

	private Game game;

	@BeforeEach
	void setup() {
		game = new Game();
		subject = spy(new PlayerRelationChange(playerRelationRepository, playerRelationRequestRepository, playerRepository));
	}

	@Test
	void shouldCheckExistingRelationType() {
		checkExistingRelationType(PlayerRelationType.NON_AGGRESSION_TREATY, PlayerRelationAction.CANCEL_ALLIANCE, "Cannot cancel non-existing alliance!");
		checkExistingRelationType(PlayerRelationType.ALLIANCE, PlayerRelationAction.CANCEL_NON_AGGRESSION_TREATY,
			"Cannot cancel non-existing non-aggression treaty!");
		checkExistingRelationType(PlayerRelationType.NON_AGGRESSION_TREATY, PlayerRelationAction.CANCEL_TRADE_AGREEMENT,
			"Cannot cancel non-existing trade agreement!");
		checkExistingRelationType(PlayerRelationType.WAR, PlayerRelationAction.DELCARE_WAR, "War already has been declared!");
		checkExistingRelationType(PlayerRelationType.WAR, PlayerRelationAction.OFFER_ALLIANCE, "There already exists a relation!");
		checkExistingRelationType(PlayerRelationType.WAR, PlayerRelationAction.OFFER_NON_AGGRESSION_TREATY, "There already exists a relation!");
		checkExistingRelationType(PlayerRelationType.NON_AGGRESSION_TREATY, PlayerRelationAction.OFFER_PEACE, "Can only request peace in war!");
		checkExistingRelationType(PlayerRelationType.WAR, PlayerRelationAction.OFFER_TRADE_AGREEMENT, "There already exists a relation!");

		game.setUseFixedTeams(true);
		checkExistingRelationType(null, PlayerRelationAction.OFFER_ALLIANCE, "Cannot offer alliance in Team Death Foe!");
		checkExistingRelationType(null, PlayerRelationAction.CANCEL_ALLIANCE, "Cannot cancel alliance in Team Death Foe!");
	}

	private void checkExistingRelationType(PlayerRelationType relationType, PlayerRelationAction action, String expectedMessage) {
		PlayerRelation relation = new PlayerRelation();
		relation.setType(relationType);

		try {
			subject.checkExistingRelation(1L, 2L, action, relation, game);
			fail("IllegalArgumentException expected");
		} catch (IllegalArgumentException e) {
			assertEquals(expectedMessage, e.getMessage());
		}

		Mockito.verify(playerRelationRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	void shouldCancelCorrectly() {
		cancel(PlayerRelationType.ALLIANCE, PlayerRelationAction.CANCEL_ALLIANCE, 12);
		cancel(PlayerRelationType.NON_AGGRESSION_TREATY, PlayerRelationAction.CANCEL_NON_AGGRESSION_TREATY, 6);
		cancel(PlayerRelationType.TRADE_AGREEMENT, PlayerRelationAction.CANCEL_TRADE_AGREEMENT, 3);
		cancel(PlayerRelationType.TRADE_AGREEMENT, PlayerRelationAction.DELCARE_WAR, -1);
	}

	private void cancel(PlayerRelationType type, PlayerRelationAction action, int expectedRoundsLeft) {
		PlayerRelation relation = new PlayerRelation();
		relation.setType(type);

		subject.checkExistingRelation(1L, 2L, action, relation, game);

		assertEquals(expectedRoundsLeft, relation.getRoundsLeft());

		Mockito.verify(playerRelationRepository).save(relation);
	}

	@Test
	void shouldAddRelationRequest() {
		addRelationRequest(PlayerRelationAction.OFFER_ALLIANCE, PlayerRelationType.ALLIANCE);
		addRelationRequest(PlayerRelationAction.OFFER_NON_AGGRESSION_TREATY, PlayerRelationType.NON_AGGRESSION_TREATY);
		addRelationRequest(PlayerRelationAction.OFFER_TRADE_AGREEMENT, PlayerRelationType.TRADE_AGREEMENT);
	}

	private void addRelationRequest(PlayerRelationAction action, PlayerRelationType expectedType) {
		PlayerRelation relation = new PlayerRelation();

		subject.checkExistingRelation(1L, 2L, action, relation, game);

		Mockito.verify(subject).addRelationRequest(1L, 2L, expectedType);
	}

	@Test
	void shouldOfferPeace() {
		PlayerRelation relation = new PlayerRelation();
		relation.setType(PlayerRelationType.WAR);

		subject.checkExistingRelation(1L, 2L, PlayerRelationAction.OFFER_PEACE, relation, game);

		Mockito.verify(subject).addRelationRequest(1L, 2L, null);
	}

	@Test
	void shouldCreateNewRelation() {
		subject.createNewRelation(1L, PlayerRelationAction.DELCARE_WAR, 2L, game);

		Mockito.verify(subject, Mockito.never()).addRelationRequest(Mockito.anyLong(), Mockito.anyLong(), Mockito.any());
		Mockito.verify(playerRelationRepository).save(Mockito.any());

		createNewRelation(PlayerRelationAction.OFFER_ALLIANCE, PlayerRelationType.ALLIANCE);
		createNewRelation(PlayerRelationAction.OFFER_NON_AGGRESSION_TREATY, PlayerRelationType.NON_AGGRESSION_TREATY);
		createNewRelation(PlayerRelationAction.OFFER_TRADE_AGREEMENT, PlayerRelationType.TRADE_AGREEMENT);
	}

	private void createNewRelation(PlayerRelationAction action, PlayerRelationType type) {
		subject.createNewRelation(1L, action, 2L, game);
		Mockito.verify(subject).addRelationRequest(1L, 2L, type);
	}

	@Test
	void shouldNotCreateNewRelation() {
		dontCreateNewRelation(PlayerRelationAction.CANCEL_ALLIANCE, "Cannot cancel non-existing alliance!");
		dontCreateNewRelation(PlayerRelationAction.CANCEL_NON_AGGRESSION_TREATY, "Cannot cancel non-existing non-aggression treaty!");
		dontCreateNewRelation(PlayerRelationAction.OFFER_PEACE, "Can only offer peace in war!");
		dontCreateNewRelation(PlayerRelationAction.CANCEL_TRADE_AGREEMENT, "Cannot cancel non-existing trade agreement!");

		game.setUseFixedTeams(true);
		dontCreateNewRelation(PlayerRelationAction.OFFER_ALLIANCE, "Cannot offer alliance in Team Death Foe!");
	}

	private void dontCreateNewRelation(PlayerRelationAction action, String expectedMessage) {
		try {
			subject.createNewRelation(1L, action, 2L, game);
			fail("IllegalArgumentException expected!");
		} catch (IllegalArgumentException e) {
			assertEquals(expectedMessage, e.getMessage());
		}
	}

	@Test
	void shouldAddRelationRequestCorrectly() {
		Mockito.when(playerRepository.getReferenceById(1L)).thenReturn(new Player(1L));
		Mockito.when(playerRepository.getReferenceById(2L)).thenReturn(new Player(2L));

		subject.addRelationRequest(1L, 2L, PlayerRelationType.ALLIANCE);

		ArgumentCaptor<PlayerRelationRequest> arg = ArgumentCaptor.forClass(PlayerRelationRequest.class);
		Mockito.verify(playerRelationRequestRepository).save(arg.capture());

		PlayerRelationRequest request = arg.getValue();
		assertEquals(1L, request.getRequestingPlayer().getId());
		assertEquals(2L, request.getOtherPlayer().getId());
		assertEquals(PlayerRelationType.ALLIANCE, request.getType());
	}
}
