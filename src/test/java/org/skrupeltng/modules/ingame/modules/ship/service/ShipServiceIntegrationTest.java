package org.skrupeltng.modules.ingame.modules.ship.service;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

class ShipServiceIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	ShipService subject;

	@Test
	void shouldReturnAllShipsInExtendedTransporterRange() {
		List<Ship> results = subject.getOwnShipsOnPosition(88L, 156L, 178, 155, 25);

		assertThat(results).isNotNull();
		assertThat(results.stream().map(Ship::getId).collect(Collectors.toSet())).isEqualTo(Set.of(256L, 257L, 258L));
	}

	@Test
	void shouldReturnNoOtherShipsBecauseNoneAreInRange() {
		List<Ship> results = subject.getOwnShipsOnPosition(88L, 156L, 199, 81, 25);

		assertThat(results).isNotNull();
		assertThat(results.stream().map(Ship::getId).collect(Collectors.toSet())).isEqualTo(Set.of(259L));
	}
}
