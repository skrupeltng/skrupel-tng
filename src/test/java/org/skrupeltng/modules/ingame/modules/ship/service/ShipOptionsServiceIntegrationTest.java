package org.skrupeltng.modules.ingame.modules.ship.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.dashboard.service.DashboardService;
import org.skrupeltng.modules.dashboard.service.GameRemoval;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetService;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplateRepository;
import org.skrupeltng.modules.masterdata.database.ShipTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class ShipOptionsServiceIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	ShipOptionsService subject;

	@Autowired
	DashboardService dashboardService;

	@Autowired
	ShipTemplateRepository shipTemplateRepository;

	@Autowired
	PropulsionSystemTemplateRepository propulsionSystemTemplateRepository;

	@Autowired
	PlayerRelationRepository playerRelationRepository;

	@Autowired
	PlayerRepository playerRepository;

	@Autowired
	ShipRepository shipRepository;

	@Autowired
	FleetRepository fleetRepository;

	@Autowired
	FleetService fleetService;

	@Autowired
	GameRemoval gameRemoval;

	Game game;

	@Test
	void shouldGiveShipToPlayer() throws Exception {
		authenticateAsAdmin();

		game = dashboardService.createdNewGame(NewGameRequest.createDefaultRequest(), 1L);
		game.setPlanets(Set.of());
		Player player1 = game.getPlayers().get(0);
		Player player2 = dashboardService.addPlayer(game.getId(), 2L, null, 1L);

		playerRelationRepository.save(new PlayerRelation(player1, player2, PlayerRelationType.ALLIANCE));

		Fleet fleet = new Fleet();
		fleet.setPlayer(playerRepository.getReferenceById(1L));
		fleet = fleetRepository.save(fleet);

		Ship ship1 = new Ship();
		ship1.setPlayer(player1);
		ship1.setName("Test Ship");
		ship1.setShipTemplate(shipTemplateRepository.getReferenceById("orion_1"));
		ship1.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getReferenceById("transwarp"));
		ship1 = shipRepository.save(ship1);
		fleetService.addShipToFleetWithoutPermissionChecks(ship1, fleet);

		Ship ship2 = new Ship();
		ship2.setPlayer(player1);
		ship2.setName("Test Ship");
		ship2.setShipTemplate(shipTemplateRepository.getReferenceById("orion_1"));
		ship2.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getReferenceById("transwarp"));
		ship2 = shipRepository.save(ship2);
		fleetService.addShipToFleetWithoutPermissionChecks(ship2, fleet);

		player1.setShips(Set.of(ship1, ship2));

		subject.giveShipToAlly(ship1, player2.getId());

		ship1 = shipRepository.getReferenceById(ship1.getId());
		assertThat(ship1.getFleet()).isNull();

		fleet = fleetRepository.getReferenceById(fleet.getId());
		assertThat(fleet.getLeader()).isEqualTo(ship2);
	}

	@AfterEach
	void cleanup() {
		gameRemoval.delete(game);
	}
}
