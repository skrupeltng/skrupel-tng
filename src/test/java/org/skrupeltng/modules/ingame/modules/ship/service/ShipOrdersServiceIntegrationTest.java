package org.skrupeltng.modules.ingame.modules.ship.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTaskChangeRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

class ShipOrdersServiceIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	ShipOrdersService subject;

	@Autowired
	ShipRepository shipRepository;

	@BeforeEach
	void init() {
		authenticateAsAdmin();
	}

	@Test
	void shouldActivateSimpleShipAbility() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		request.setActiveAbilityType(ShipAbilityType.CLOAKING_RELIABLE.name());
		request.setTaskType(ShipTaskType.ACTIVE_ABILITY.name());

		subject.changeTask(225L, request);

		Ship ship = shipRepository.getReferenceById(225L);
		ShipAbility cloakingAbility = ship.getShipTemplate().getAbility(ShipAbilityType.CLOAKING_RELIABLE).orElseThrow();
		assertThat(ship.getActiveAbility()).isEqualTo(cloakingAbility);
		assertThat(ship.getTaskType()).isEqualTo(ShipTaskType.ACTIVE_ABILITY);
		assertThat(ship.getTaskValue()).isNull();
	}

	@Test
	void shouldActivateShipAbilityWithTaskValue() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		request.setActiveAbilityType(ShipAbilityType.QUARK_REORGANIZER.name());
		request.setTaskType(ShipTaskType.ACTIVE_ABILITY.name());
		request.setTaskValue("true");

		subject.changeTask(226L, request);

		Ship ship = shipRepository.getReferenceById(226L);
		ShipAbility quarksReorganizer = ship.getShipTemplate().getAbility(ShipAbilityType.QUARK_REORGANIZER).orElseThrow();
		assertThat(ship.getActiveAbility()).isEqualTo(quarksReorganizer);
		assertThat(ship.getTaskType()).isEqualTo(ShipTaskType.ACTIVE_ABILITY);
		assertThat(ship.getTaskValue()).isEqualTo("true");
	}

	@Test
	void shouldResetTravelBecauseEscortTargetReset() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		request.setTaskType(ShipTaskType.NONE.name());

		subject.changeTask(227L, request);

		Ship ship = shipRepository.getReferenceById(227L);

		assertThat(ship.getDestinationShip()).isNull();
		assertThat(ship.getDestinationX()).isEqualTo(-1);
		assertThat(ship.getDestinationY()).isEqualTo(-1);
		assertThat(ship.getTravelSpeed()).isZero();
		assertThat(ship.getTaskType()).isEqualTo(ShipTaskType.NONE);
		assertThat(ship.getTaskValue()).isNull();
	}

	@Test
	void shouldChangeEscortTarget() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		request.setTaskType(ShipTaskType.NONE.name());
		request.setEscortTargetId(226L);
		request.setEscortTargetSpeed(8);

		subject.changeTask(227L, request);

		Ship ship = shipRepository.getReferenceById(227L);

		assertThat(ship.getDestinationShip().getId()).isEqualTo(226L);
		assertThat(ship.getDestinationX()).isEqualTo(50);
		assertThat(ship.getDestinationY()).isEqualTo(200);
		assertThat(ship.getTravelSpeed()).isEqualTo(8);
		assertThat(ship.getTaskType()).isEqualTo(ShipTaskType.NONE);
		assertThat(ship.getTaskValue()).isNull();
	}
}
