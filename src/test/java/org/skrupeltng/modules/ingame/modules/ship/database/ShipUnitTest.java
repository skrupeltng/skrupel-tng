package org.skrupeltng.modules.ingame.modules.ship.database;

import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;

import static org.assertj.core.api.Assertions.assertThat;

class ShipUnitTest {

	@Test
	void shouldNotTowShipBecauseCoordinateX() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		ship.setPropulsionSystemTemplate(new PropulsionSystemTemplate());
		Ship s = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1);

		ship.setX(1);

		boolean result = ship.canTowShip(s);
		assertThat(result).isFalse();
	}

	@Test
	void shouldNotTowShipBecauseCoordinateY() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		ship.setPropulsionSystemTemplate(new PropulsionSystemTemplate());
		Ship s = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1);

		ship.setY(1);

		boolean result = ship.canTowShip(s);
		assertThat(result).isFalse();
	}

	@Test
	void shouldNotTowShipBecauseMassTooHigh() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		ship.getShipTemplate().setPropulsionSystemsCount(1);
		PropulsionSystemTemplate propulsionSystemTemplate = new PropulsionSystemTemplate();
		propulsionSystemTemplate.setTechLevel(1);
		ship.setPropulsionSystemTemplate(propulsionSystemTemplate);
		Ship s = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1);

		boolean result = ship.canTowShip(s);
		assertThat(result).isFalse();
	}

	@Test
	void shouldTowShip() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		ship.getShipTemplate().setPropulsionSystemsCount(2);
		PropulsionSystemTemplate propulsionSystemTemplate = new PropulsionSystemTemplate();
		propulsionSystemTemplate.setTechLevel(5);
		ship.setPropulsionSystemTemplate(propulsionSystemTemplate);
		Ship s = ShipTestFactory.createShip(100, 10, 1, 1, 1, 1);

		boolean result = ship.canTowShip(s);
		assertThat(result).isTrue();
	}

	@Test
	void shouldReturnTrueBecausePlanetIsOwned() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		Planet planet = new Planet();
		planet.setPlayer(ship.getPlayer());
		ship.setPlanet(planet);

		boolean result = ship.planetOwnedByPlayer();
		assertThat(result).isTrue();
	}

	@Test
	void shouldReturnFalseBecausePlanetIsOwnedByOtherPlayer() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		Planet planet = new Planet();
		planet.setPlayer(new Player(67L));
		ship.setPlanet(planet);

		boolean result = ship.planetOwnedByPlayer();
		assertThat(result).isFalse();
	}

	@Test
	void shouldReturnFalseBecausePlanetIsOwnedByNoOne() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		Planet planet = new Planet();
		ship.setPlanet(planet);

		boolean result = ship.planetOwnedByPlayer();
		assertThat(result).isFalse();
	}

	@Test
	void shouldReturnFalseBecauseShipNotOnAnyPlanet() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);

		boolean result = ship.planetOwnedByPlayer();
		assertThat(result).isFalse();
	}

	@Test
	void shouldKillShipInOneHit() {
		Ship ship = ShipTestFactory.createShip(0, 100, 1, 1, 50, 1L);

		ship.receiveDamage(34, 34, false, 100);

		assertThat(ship.getDamage()).isEqualTo(100);
		assertThat(ship.getCrew()).isEqualTo(50);
	}

	@Test
	void shouldNotKillShipInOneHit() {
		Ship ship = ShipTestFactory.createShip(0, 200, 1, 1, 100, 1L);

		ship.receiveDamage(34, 34, false, 200);

		assertThat(ship.getDamage()).isEqualTo(68);
		assertThat(ship.getCrew()).isEqualTo(32);
	}
}
