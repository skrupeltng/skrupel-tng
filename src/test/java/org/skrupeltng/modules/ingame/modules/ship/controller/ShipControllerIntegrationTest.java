package org.skrupeltng.modules.ingame.modules.ship.controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.dashboard.modules.storymode.controller.StartMissionRequest;
import org.skrupeltng.modules.dashboard.modules.storymode.service.StoryModeService;
import org.skrupeltng.modules.dashboard.service.GameRemoval;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplateRepository;
import org.skrupeltng.modules.masterdata.database.ShipTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.bean.override.mockito.MockitoSpyBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ShipControllerIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	MockMvc mockMvc;

	@Autowired
	PlayerRepository playerRepository;

	@Autowired
	ShipRepository shipRepository;

	@Autowired
	ShipTemplateRepository shipTemplateRepository;

	@Autowired
	PropulsionSystemTemplateRepository propulsionSystemTemplateRepository;

	@Autowired
	StoryModeService storyModeService;

	@Autowired
	GameRemoval gameRemoval;

	@MockitoSpyBean
	PoliticsService politicsService;

	Ship ship;
	long gameId;

	@Test
	@WithUserDetails("admin")
	void shouldFetchAllies() throws Exception {
		ship = new Ship();
		ship.setPlayer(playerRepository.getReferenceById(7L));
		ship.setName("Test Ship");
		ship.setShipTemplate(shipTemplateRepository.getReferenceById("orion_1"));
		ship.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getReferenceById("transwarp"));
		ship = shipRepository.save(ship);

		String htmlResponse = mockMvc.perform(get("/ingame/ship/options?shipId=" + ship.getId()))
			.andExpect(status().isOk())
			.andReturn().getResponse().getContentAsString();

		assertThat(htmlResponse).isNotNull().contains("skr-ingame-allied-players-select");
		verify(politicsService).getPlayersWithRelation(ship.getPlayer().getId(), PlayerRelationType.ALLIANCE);
	}

	@Test
	@WithUserDetails("admin")
	void shouldNotFetchAllies() throws Exception {
		StartMissionRequest request = new StartMissionRequest();
		request.setMissionNumber(2);
		request.setFactionId("silverstarag");

		gameId = storyModeService.startMission(request, 1L);

		List<Ship> ships = shipRepository.findShipsByGameIdAndLoginId(gameId, 1L);

		String htmlResponse = mockMvc.perform(get("/ingame/ship/options?shipId=" + ships.get(0).getId()))
			.andExpect(status().isOk())
			.andReturn().getResponse().getContentAsString();

		assertThat(htmlResponse).isNotNull().doesNotContain("skr-ingame-allied-players-select");
		verify(politicsService, never()).getPlayersWithRelation(anyLong(), any());
	}

	@AfterEach
	void cleanUp() {
		if (ship != null) {
			shipRepository.delete(ship);
			ship = null;
		}

		if (gameId > 0L) {
			gameRemoval.delete(gameId);
		}
	}
}
