package org.skrupeltng.modules.ingame.modules.planet.database;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.dashboard.modules.storymode.database.StoryModeCampaign;
import org.skrupeltng.modules.dashboard.modules.storymode.service.missions.silverstarag.SilverStarAGMission1;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.masterdata.database.Faction;

class PlanetUnitTest {

	@Test
	void shouldUseFastGrowthLimit() {
		Planet planet = new Planet();
		planet.setColonists(10000);

		int result = planet.retrieveColonistBuildingFactor(0, 1f, 200);

		assertEquals(100, result);
	}

	@Test
	void shouldNotUseFastGrowthLimit() {
		Planet planet = new Planet();
		planet.setColonists(20001);

		int result = planet.retrieveColonistBuildingFactor(0, 1f, 200);

		assertEquals(214, result);

		planet.setColonists(30000);

		result = planet.retrieveColonistBuildingFactor(0, 1f, 200);

		assertEquals(217, result);
	}

	@Test
	void shouldGrowPopulation() {
		Planet planet = new Planet();
		planet.setColonists(1000);
		planet.setTemperature(30);
		Player player = new Player();
		Faction faction = new Faction();
		faction.setPreferredTemperature(30);
		faction.setPreferredPlanetType("");
		player.setFaction(faction);
		planet.setPlayer(player);
		planet.setType("");
		planet.setOrbitalSystems(Collections.emptyList());

		float growthRate = planet.retrievePopulationGrowthRate();
		planet.setColonists(planet.getColonists() + Math.round(planet.getColonists() * growthRate));

		assertEquals(1021, planet.getColonists());
	}

	@Test
	void shouldHardlyGrowPopulation() {
		Planet planet = new Planet();
		planet.setColonists(1000);
		planet.setTemperature(60);
		Player player = new Player();
		Faction faction = new Faction();
		faction.setPreferredTemperature(30);
		faction.setPreferredPlanetType("");
		player.setFaction(faction);
		planet.setPlayer(player);
		planet.setType("");
		planet.setOrbitalSystems(Collections.emptyList());

		float growthRate = planet.retrievePopulationGrowthRate();
		planet.setColonists(planet.getColonists() + Math.round(planet.getColonists() * growthRate));

		assertEquals(1003, planet.getColonists());
	}

	@Test
	void shouldNotGrowPopulation() {
		Planet planet = new Planet();
		planet.setColonists(1000);
		planet.setTemperature(61);
		Player player = new Player();
		Faction faction = new Faction();
		faction.setPreferredTemperature(30);
		faction.setPreferredPlanetType("");
		player.setFaction(faction);
		planet.setPlayer(player);
		planet.setType("");
		planet.setOrbitalSystems(Collections.emptyList());

		float growthRate = planet.retrievePopulationGrowthRate();
		planet.setColonists(planet.getColonists() + Math.round(planet.getColonists() * growthRate));

		assertEquals(1000, planet.getColonists());
	}

	@Test
	void shouldReturnFalseBecauseNotTutorialMission() {
		Planet planet = new Planet();
		planet.setGame(new Game());

		boolean result = planet.highlightStarbaseButton();

		assertThat(result).isFalse();
	}

	@Test
	void shouldReturnFalseBecauseNotStarbaseUpgradeStep() {
		Planet planet = new Planet();
		Game game = new Game();
		StoryModeCampaign storyModeCampaign = new StoryModeCampaign();
		storyModeCampaign.setFaction(new Faction("silverstarag"));
		game.setStoryModeCampaign(storyModeCampaign);
		game.setStoryModeMission(1);
		planet.setGame(game);

		boolean result = planet.highlightStarbaseButton();

		assertThat(result).isFalse();
	}

	@Test
	void shouldReturnFalseBecauseStarbaseAlreadyUpgraded() {
		Planet planet = new Planet();
		Game game = new Game();
		StoryModeCampaign storyModeCampaign = new StoryModeCampaign();
		storyModeCampaign.setFaction(new Faction("silverstarag"));
		game.setStoryModeCampaign(storyModeCampaign);
		game.setStoryModeMission(1);
		game.setStoryModeMissionStep(SilverStarAGMission1.STEP_4_UPGRADE_STARBASE);
		planet.setGame(game);

		Starbase starbase = new Starbase();
		starbase.setHullLevel(3);
		starbase.setPropulsionLevel(7);
		planet.setStarbase(starbase);

		boolean result = planet.highlightStarbaseButton();

		assertThat(result).isFalse();
	}

	@Test
	void shouldReturnTrue() {
		Planet planet = new Planet();
		Game game = new Game();
		StoryModeCampaign storyModeCampaign = new StoryModeCampaign();
		storyModeCampaign.setFaction(new Faction("silverstarag"));
		game.setStoryModeCampaign(storyModeCampaign);
		game.setStoryModeMission(1);
		game.setStoryModeMissionStep(SilverStarAGMission1.STEP_4_UPGRADE_STARBASE);
		planet.setGame(game);

		Starbase starbase = new Starbase();
		planet.setStarbase(starbase);

		boolean result = planet.highlightStarbaseButton();

		assertThat(result).isTrue();
	}
}
