package org.skrupeltng.modules.ingame.modules.ship.service;

import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.ingame.modules.ship.GoodsContainer;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportItem;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;

class ShipTransporterUnitTest {

	@Test
	void shouldThrowBecauseUnknownTargetType() {
		ShipTransportRequest request = new ShipTransportRequest();
		Ship ship = new Ship();
		GoodsContainer target = mock(GoodsContainer.class);

		assertThatThrownBy(() -> new ShipTransporter(request, ship, target))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessageContaining("Invalid target: ");
	}

	@Test
	void shouldThrowBecauseNegativeValue() {
		ShipTransportItem item = new ShipTransportItem(0, 0, 0, 0, ShipTransportResource.fuel);
		int value = -1;

		assertThatThrownBy(() -> ShipTransporter.validateItem(item, value))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Negative value found for fuel");
	}

	@Test
	void shouldThrowBecauseValueLowerThanLeftMin() {
		ShipTransportItem item = new ShipTransportItem(8, 10, 8, 10, ShipTransportResource.fuel);
		int value = 2;

		assertThatThrownBy(() -> ShipTransporter.validateItem(item, value))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Value 2 lower than min value 6 for ship for fuel");
	}

	@Test
	void shouldThrowBecauseValueHigherThanLeftMax() {
		ShipTransportItem item = new ShipTransportItem(3, 10, 3, 10, ShipTransportResource.fuel);
		int value = 8;

		assertThatThrownBy(() -> ShipTransporter.validateItem(item, value))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Value 8 higher than max value 6 for ship for fuel");
	}
}
