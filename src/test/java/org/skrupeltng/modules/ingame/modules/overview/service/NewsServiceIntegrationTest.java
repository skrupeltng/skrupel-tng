package org.skrupeltng.modules.ingame.modules.overview.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntryArgument;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntryArgumentRepository;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;

class NewsServiceIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	private NewsService subject;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private NewsEntryRepository newsEntryRepository;

	@Autowired
	private NewsEntryArgumentRepository newsEntryArgumentRepository;

	@Test
	void shouldAddNewsEntryWithoutArguments() {
		Player player = playerRepository.getReferenceById(3L);
		subject.add(player, NewsEntryConstants.news_entry_mine_field_cleared_by_enemy, "testimage1.jpg", null, null);

		List<NewsEntry> result = subject.findNews(gameId, player.getLogin().getId());

		assertEquals(1, result.size());
		NewsEntry entry = result.get(0);
		assertEquals(player.getId(), entry.getPlayer().getId());
		assertEquals(NewsEntryConstants.news_entry_mine_field_cleared_by_enemy, entry.getTemplate());
		assertEquals("testimage1.jpg", entry.getImage());
		assertEquals(null, entry.getClickTargetId());
		assertEquals(null, entry.getClickTargetType());

		List<NewsEntryArgument> arguments = newsEntryArgumentRepository.findByNewsEntryId(entry.getId());
		assertEquals(0, arguments.size());
	}

	@Test
	void shouldAddNewsEntryWithSingleArgument() {
		Player player = playerRepository.getReferenceById(5L);
		subject.add(player, NewsEntryConstants.news_entry_artifact_colony_found, "testimage2.jpg", 1L, NewsEntryClickTargetType.planet, "testPlanet1");

		List<NewsEntry> result = subject.findNews(gameId, player.getLogin().getId());

		assertTrue(result.size() > 0, "Expected at least one news entry result but only got " + result.size());
		Optional<NewsEntry> elemOpt = result.stream().filter(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_artifact_colony_found)).findFirst();
		assertTrue(elemOpt.isPresent());
		NewsEntry entry = elemOpt.get();
		assertEquals(player.getId(), entry.getPlayer().getId());
		assertEquals(NewsEntryConstants.news_entry_artifact_colony_found, entry.getTemplate());
		assertEquals("testimage2.jpg", entry.getImage());
		assertEquals(1L, entry.getClickTargetId().longValue());
		assertEquals(NewsEntryClickTargetType.planet, entry.getClickTargetType());

		List<NewsEntryArgument> arguments = newsEntryArgumentRepository.findByNewsEntryId(entry.getId());
		assertEquals(1, arguments.size());
		assertEquals("testPlanet1", arguments.get(0).getValue());
	}

	@Test
	void shouldAddNewsEntryWithMultipleArgument() {
		Player player = playerRepository.getReferenceById(7L);
		subject.add(player, NewsEntryConstants.news_entry_big_meteor_hit_planet, "testimage3.jpg", null, null, "arg1", "arg2", "arg3");

		List<NewsEntry> result = subject.findNews(gameId, player.getLogin().getId());

		assertTrue(result.size() > 0, "Expected at least one news entry result but only got " + result.size());
		Optional<NewsEntry> elemOpt = result.stream().filter(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_big_meteor_hit_planet)).findFirst();
		assertTrue(elemOpt.isPresent());
		NewsEntry entry = elemOpt.get();
		assertEquals(player.getId(), entry.getPlayer().getId());
		assertEquals(NewsEntryConstants.news_entry_big_meteor_hit_planet, entry.getTemplate());
		assertEquals("testimage3.jpg", entry.getImage());
		assertEquals(null, entry.getClickTargetId());
		assertEquals(null, entry.getClickTargetType());

		List<NewsEntryArgument> arguments = newsEntryArgumentRepository.findByNewsEntryId(entry.getId());
		assertEquals(3, arguments.size());
		assertEquals("arg1", arguments.get(0).getValue());
		assertEquals("arg2", arguments.get(1).getValue());
		assertEquals("arg3", arguments.get(2).getValue());
	}

	@Test
	void shouldClearNewsEntries() {
		subject.cleanNewsEntries(gameId);

		List<NewsEntry> result = subject.findNews(gameId, 1L);
		assertEquals(0, result.size());
	}

	@Test
	void shouldCreateRoundSummary() {
		RoundSummary result = subject.getRoundSummary(gameId, 18L);

		assertEquals(0, result.getDestroyedShips());
		assertEquals(0, result.getLostShips());
		assertEquals(0, result.getNewColonies());
		assertEquals(0, result.getNewShips());
		assertEquals(0, result.getNewStarbases());
		assertEquals(20, result.getRound());
		assertEquals(WinCondition.INVASION.name(), result.getWinCondition());

		assertEquals(0, result.getDestroyedShipsPlayer());
		assertEquals(0, result.getLostShipsPlayer());
		assertEquals(0, result.getNewColoniesPlayer());
		assertEquals(0, result.getNewShipsPlayer());
		assertEquals(0, result.getNewStarbasesPlayer());
	}

	@Test
	void shouldNotToggleBecauseNotFound() {
		boolean result = subject.toggleDeleteNews(1111111111111L);
		assertEquals(false, result);
	}

	@Test
	void shouldToggleDelete() {
		boolean result = subject.toggleDeleteNews(55L);

		assertEquals(false, result);
		assertEquals(false, newsEntryRepository.getReferenceById(55L).isDelete());

		result = subject.toggleDeleteNews(55L);

		assertEquals(true, result);
		assertEquals(true, newsEntryRepository.getReferenceById(55L).isDelete());
	}
}
