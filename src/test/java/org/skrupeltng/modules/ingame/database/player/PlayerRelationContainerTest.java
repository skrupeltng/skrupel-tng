package org.skrupeltng.modules.ingame.database.player;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationAction;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationContainer;
import org.springframework.context.MessageSource;

class PlayerRelationContainerTest {

	private PlayerRelationContainer subject;
	private MessageSource messageSource;

	@BeforeEach
	void setup() {
		subject = new PlayerRelationContainer();

		messageSource = Mockito.mock(MessageSource.class);
		subject.offerMessageSource(messageSource);
	}

	@Test
	void shouldReturnNoActions() {
		subject.setRequestedRelationType(PlayerRelationType.ALLIANCE.name());

		List<PlayerRelationAction> result = subject.retrieveActions();
		assertEquals(0, result.size());

		subject.setRequestedRelationType(null);
		subject.setRoundsLeft(1);

		result = subject.retrieveActions();
		assertEquals(0, result.size());
	}

	@Test
	void shouldReturnAllPossibleActions() {
		List<PlayerRelationAction> result = subject.retrieveActions();
		assertEquals(PlayerRelationType.values().length, result.size());
	}

	@Test
	void shouldReturnOfferPeace() {
		subject.setRelationType(PlayerRelationType.WAR.name());
		List<PlayerRelationAction> result = subject.retrieveActions();
		assertThat(result).isEqualTo(List.of(PlayerRelationAction.OFFER_PEACE));
	}

	@Test
	void shouldReturnPossibleActionsForAlliance() {
		subject.setRelationType(PlayerRelationType.ALLIANCE.name());
		List<PlayerRelationAction> result = subject.retrieveActions();
		assertThat(result).isEqualTo(List.of(PlayerRelationAction.CANCEL_ALLIANCE));
	}

	@Test
	void shouldReturnPossibleActionsForNonAggressionTreaty() {
		subject.setRelationType(PlayerRelationType.NON_AGGRESSION_TREATY.name());
		List<PlayerRelationAction> result = subject.retrieveActions();
		assertThat(result).isEqualTo(List.of(PlayerRelationAction.OFFER_ALLIANCE, PlayerRelationAction.CANCEL_NON_AGGRESSION_TREATY));
	}

	@Test
	void shouldReturnPossibleActionsForTradeAgreement() {
		subject.setRelationType(PlayerRelationType.TRADE_AGREEMENT.name());
		List<PlayerRelationAction> result = subject.retrieveActions();
		assertThat(result).isEqualTo(
				List.of(PlayerRelationAction.OFFER_NON_AGGRESSION_TREATY, PlayerRelationAction.OFFER_ALLIANCE, PlayerRelationAction.CANCEL_TRADE_AGREEMENT));
	}

	@Test
	void shouldReturnPeaceRequested() {
		subject.setRequestId(1L);
		Mockito.when(messageSource.getMessage(Mockito.eq("peace_requested"), Mockito.eq(null), Mockito.any())).thenReturn("peace requested");

		String result = subject.retrieveDescription();
		assertEquals("peace requested", result);
	}

	@Test
	void shouldReturnAllianceRequested() {
		subject.setRequestId(1L);
		subject.setRequestedRelationType(PlayerRelationType.ALLIANCE.name());
		Mockito.when(messageSource.getMessage(Mockito.eq("player_relation_type_ALLIANCE"), Mockito.eq(null), Mockito.any())).thenReturn("alliance");
		Mockito.when(messageSource.getMessage(Mockito.eq("requested"), Mockito.eq(null), Mockito.any())).thenReturn("requested");

		String result = subject.retrieveDescription();
		assertEquals("alliance requested", result);
	}

	@Test
	void shouldReturnAlliance() {
		subject.setRelationType(PlayerRelationType.ALLIANCE.name());
		Mockito.when(messageSource.getMessage(Mockito.eq("player_relation_type_ALLIANCE"), Mockito.eq(null), Mockito.any())).thenReturn("alliance");

		String result = subject.retrieveDescription();
		assertEquals("alliance", result);
	}

	@Test
	void shouldReturnAllianceEndsInNineMonths() {
		subject.setRelationType(PlayerRelationType.ALLIANCE.name());
		subject.setRoundsLeft(9);
		Object[] args = new Object[] { 9 };
		Mockito.when(messageSource.getMessage(Mockito.eq("player_relation_type_ALLIANCE"), Mockito.eq(null), Mockito.any())).thenReturn("alliance");
		Mockito.when(messageSource.getMessage(Mockito.eq("player_relation_ends"), Mockito.eq(args), Mockito.eq(null), Mockito.any()))
				.thenReturn("ends in 9 months");

		String result = subject.retrieveDescription();
		assertEquals("alliance ends in 9 months", result);
	}

	@Test
	void shouldReturnAllianceEndsInOneMonths() {
		subject.setRelationType(PlayerRelationType.ALLIANCE.name());
		subject.setRoundsLeft(1);
		Mockito.when(messageSource.getMessage(Mockito.eq("player_relation_type_ALLIANCE"), Mockito.eq(null), Mockito.any())).thenReturn("alliance");
		Mockito.when(messageSource.getMessage(Mockito.eq("player_relation_ends_in_one_month"), Mockito.eq(null), Mockito.any()))
				.thenReturn("ends in one months");

		String result = subject.retrieveDescription();
		assertEquals("alliance ends in one months", result);
	}

	@Test
	void shouldReturnMinus() {
		String result = subject.retrieveDescription();
		assertEquals("-", result);
	}

	@Test
	void shouldNullCheckTypeWithIcon() {
		String result = subject.retrieveTypeIcon();
		assertNull(result);
	}

	@Test
	void shouldReturnCorrectIcon() {
		subject.setRelationType(PlayerRelationType.ALLIANCE.name());
		String result = subject.retrieveTypeIcon();
		assertEquals(PlayerRelationType.ALLIANCE.getIcon(), result);
	}

	@Test
	void shouldUsePlayerNameField() {
		String testUser = "testuser";
		subject.setPlayerName(testUser);
		Mockito.when(messageSource.getMessage(Mockito.eq(testUser), Mockito.eq(null), Mockito.eq(testUser), Mockito.any())).thenReturn("testuser_translated");

		String result = subject.retrievePlayerName();

		assertEquals("testuser_translated", result);
	}
}