package org.skrupeltng.modules.mail.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.ingame.database.player.Player;

import java.util.Optional;

class MailServiceUnitTest {

	private final MailService subject = new MailService(
		Optional.empty(),
		null,
		null,
		null,
		null
	);

	private Player player;
	private Login login;

	@BeforeEach
	void setup() {
		player = new Player(1L);
		login = new Login(2L);
		login.setEmail("test@test.com");
		login.setRoundNotificationsEnabled(true);
		player.setLogin(login);
	}

	@Test
	void shouldReturnFalseBecauseMailDisabledForLogin() {
		login.setRoundNotificationsEnabled(false);
		boolean result = subject.gameNotificationMailCanBeSend(null, player);
		assertFalse(result);
	}

	@Test
	void shouldReturnFalseBecausePlayerLost() {
		player.setHasLost(true);
		boolean result = subject.gameNotificationMailCanBeSend(null, player);
		assertFalse(result);
	}

	@Test
	void shouldReturnFalseBecauseLoginTriggeredItSelf() {
		boolean result = subject.gameNotificationMailCanBeSend(2L, player);
		assertFalse(result);
	}

	@Test
	void shouldReturnFalseBecauseMailNotPresent() {
		login.setEmail(null);
		boolean result = subject.gameNotificationMailCanBeSend(3L, player);
		assertFalse(result);
	}

	@Test
	void shouldReturnTrue() {
		boolean result = subject.gameNotificationMailCanBeSend(3L, player);
		assertTrue(result);
	}
}
