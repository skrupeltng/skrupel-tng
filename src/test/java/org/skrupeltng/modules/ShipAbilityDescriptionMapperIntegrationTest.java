package org.skrupeltng.modules;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipAbilityDescription;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;

class ShipAbilityDescriptionMapperIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	private ShipAbilityDescriptionMapper shipAbilityDescriptionMapper;

	@Autowired
	private ShipTemplateRepository shipTemplateRepository;

	@Test
	void shouldCreateShipAbilityDescription() {
		Optional<ShipAbility> abilityOpt = shipTemplateRepository.getReferenceById("orion_14").getAbility(ShipAbilityType.VIRAL_INVASION);

		ShipAbility ability = abilityOpt.get();
		ShipAbilityDescription result = shipAbilityDescriptionMapper.mapShipAbilityDescription(ability);
		assertNotNull(result);

		assertEquals(ability.getId(), result.getAbilityId());
		assertEquals("Viral invasion", result.getName());
		assertEquals(
				"Instead of using planetary bombardments this ship can dive into the atmosphere of a planet and release special biological substances that eliminates between 45% and 100% of the population.",
				result.getDescription());
	}
}
