package org.skrupeltng.modules.dashboard.service;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHole;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleType;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class WormholeCreatorIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	WormholeCreator subject;

	@Autowired
	GameRepository gameRepository;

	@Autowired
	WormHoleRepository wormHoleRepository;

	@Test
	void shouldAddUnstableWormholes() {
		Game game = gameRepository.getReferenceById(gameId);
		subject.createUnstableWormholes(game);

		List<WormHole> results = wormHoleRepository.findByGameId(gameId);

		assertThat(results).hasSize(3).allMatch(w -> w.getConnection() == null && w.getType() == WormHoleType.UNSTABLE_WORMHOLE);
	}

	@Test
	void shouldAddStableWormholes() {
		Game game = gameRepository.getReferenceById(gameId);
		subject.createStableWormholes(game);

		List<WormHole> results = wormHoleRepository.findByGameId(gameId);

		assertThat(results).hasSize(4).allMatch(w -> w.getConnection() != null && w.getType() == WormHoleType.STABLE_WORMHOLE);
	}

	@Test
	void shouldNotCreateWormholeBecausePlanetTooNear() {
		Game game = gameRepository.getReferenceById(gameId);

		Optional<WormHole> result = subject.checkWormHoleCreation(game, 106, 66);

		assertThat(result).isEmpty();
	}

	@Test
	void shouldNotCreateWormholeBecauseOtherWormholeTooNear() {
		Game game = gameRepository.getReferenceById(gameId);

		Optional<WormHole> result = subject.checkWormHoleCreation(game, 145, 29);

		assertThat(result).isEmpty();
	}
}
