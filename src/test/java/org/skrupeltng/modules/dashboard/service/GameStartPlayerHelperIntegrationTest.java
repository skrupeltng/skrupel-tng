package org.skrupeltng.modules.dashboard.service;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerDeathFoeRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class GameStartPlayerHelperIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	GameStartPlayerHelper subject;

	@Autowired
	GameRepository gameRepository;

	@Autowired
	PlayerRelationRepository playerRelationRepository;

	@Autowired
	PlayerDeathFoeRepository deathFoeRepository;

	@Test
	void shouldAssignPlayerRelations() {
		Game game = gameRepository.getReferenceById(gameId);

		subject.setupTeamRelations(game);

		Set<PlayerRelation> relations = playerRelationRepository.findByGameIdAndType(gameId, PlayerRelationType.ALLIANCE);
		assertThat(relations).hasSize(2);
	}

	@Test
	void shouldAssignTeamDeathFoes() {
		Game game = gameRepository.getReferenceById(gameId);

		subject.assignTeamDeathFoes(game);

		List<Player> deathFoes = deathFoeRepository.getDeathFoes(gameId, 1L);
		assertThat(deathFoes).map(Player::getId).containsExactlyInAnyOrder(178L, 179L);

		deathFoes = deathFoeRepository.getDeathFoes(gameId, 2L);
		assertThat(deathFoes).map(Player::getId).containsExactlyInAnyOrder(178L, 179L);

		deathFoes = deathFoeRepository.getDeathFoes(gameId, 3L);
		assertThat(deathFoes).map(Player::getId).containsExactlyInAnyOrder(176L, 177L);

		deathFoes = deathFoeRepository.getDeathFoes(gameId, 4L);
		assertThat(deathFoes).map(Player::getId).containsExactlyInAnyOrder(176L, 177L);
	}

	@Test
	void shouldAssignDeathFoes() {
		Game game = gameRepository.getReferenceById(gameId);

		subject.assignDeathFoes(game);

		List<Player> deathFoes = deathFoeRepository.getDeathFoes(gameId, 1L);
		assertThat(deathFoes).hasSize(1).map(Player::getId).containsAnyElementsOf(List.of(181L, 182L));

		deathFoes = deathFoeRepository.getDeathFoes(gameId, 2L);
		assertThat(deathFoes).hasSize(1).map(Player::getId).containsAnyElementsOf(List.of(180L, 182L));

		deathFoes = deathFoeRepository.getDeathFoes(gameId, 3L);
		assertThat(deathFoes).hasSize(1).map(Player::getId).containsAnyElementsOf(List.of(180L, 181L));
	}
}
