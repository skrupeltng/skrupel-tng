package org.skrupeltng.modules.dashboard.service;

import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.ingame.database.player.Player;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class PlayerColorAssignerUnitTest {

	@Test
	void shouldGeneratePlayerColors() {
		List<Player> players = new ArrayList<>();

		for (int i = 0; i < 12; i++) {
			players.add(new Player(i + 1));
		}

		PlayerColorAssigner subject = new PlayerColorAssigner(players);
		subject.assignPlayerColors();

		assertThat(players).allMatch(p -> p.getColor() != null);
	}
}
