package org.skrupeltng.modules.dashboard.service;

import jakarta.mail.internet.MimeMessage;
import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.dashboard.database.Team;
import org.skrupeltng.modules.dashboard.database.TeamRepository;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class DashboardServiceIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	DashboardService dashboardService;

	@Autowired
	LoginRepository loginRepository;

	@Autowired
	GameRepository gameRepository;

	@Autowired
	TeamRepository teamRepository;

	long getJoinedNotificationCount(long loginId) {
		String expectation = "A new player joined your game \"" + testName + "\"";
		return getNotificationCount(loginId, expectation);
	}

	long getGameFullNotificationCount(long loginId) {
		String expectation = "Your game \"" + testName + "\" is now full and can be started!";
		return getNotificationCount(loginId, expectation);
	}

	long getGameStartedNotificationCount(long loginId) {
		String expectation = "Game \"" + testName + "\" has just started!";
		return getNotificationCount(loginId, expectation);
	}

	@Test
	void shouldDoNothingBecauseIsCreator() {
		dashboardService.sendPlayerJoinedNotificatiosAndEmails(gameId, 1L, 1L);
		assertThat(mockedEmailServer.getMessages()).isEmpty();
		assertThat(getJoinedNotificationCount(1L)).isZero();

		dashboardService.sendGameFullNotificationAndEmail(gameId, 1L);
		assertThat(mockedEmailServer.getMessages()).isEmpty();
		assertThat(getGameFullNotificationCount(1L)).isZero();
	}

	@Test
	void shouldNotSendJoinedEmailBecauseNotificationDeactivated() throws Exception {
		dashboardService.sendPlayerJoinedNotificatiosAndEmails(gameId, 1L, 45L);
		assertThat(mockedEmailServer.getMessages()).isEmpty();
		assertThat(getJoinedNotificationCount(5L)).isEqualTo(1L);
	}

	@Test
	void shouldSendJoinedNotificationEmail() throws Exception {
		Login login = loginRepository.getReferenceById(1L);
		login.setJoinNotificationsEnabled(true);
		loginRepository.save(login);

		dashboardService.sendPlayerJoinedNotificatiosAndEmails(gameId, 1L, 47L);
		MimeMessage[] messages = mockedEmailServer.getMessages();
		assertThat(messages).hasSize(1);

		MimeMessage mimeMessage = messages[0];
		assertThat(mimeMessage.getSubject()).isEqualTo("A player joined your game - Skrupel TNG");

		assertThat(extractEmailContent(mimeMessage)).contains("has joined your game");

		assertThat(getJoinedNotificationCount(6L)).isEqualTo(1L);
	}

	@Test
	void shouldDoNothingBecauseGameNotFull() {
		dashboardService.sendGameFullNotificationAndEmail(gameId, 1L);
		assertThat(mockedEmailServer.getMessages()).isEmpty();
		assertThat(getGameFullNotificationCount(5L)).isZero();
	}

	@Test
	void shouldDoNothingBecauseNotAllPlayersHaveFaction() {
		dashboardService.sendGameFullNotificationAndEmail(gameId, 5L);
		assertThat(mockedEmailServer.getMessages()).isEmpty();
		assertThat(getGameFullNotificationCount(1L)).isZero();
	}

	@Test
	void shouldAddGameFullNotification() throws Exception {
		dashboardService.sendGameFullNotificationAndEmail(gameId, 5L);
		assertThat(mockedEmailServer.getMessages()).isEmpty();
		assertThat(getGameFullNotificationCount(1L)).isEqualTo(1L);
	}

	@Test
	void shouldAddGameFullNotificationAndEmail() throws Exception {
		dashboardService.sendGameFullNotificationAndEmail(gameId, 5L);

		MimeMessage[] messages = mockedEmailServer.getMessages();
		assertThat(messages).hasSize(1);

		MimeMessage mimeMessage = messages[0];
		assertThat(mimeMessage.getSubject()).isEqualTo("Your game shouldAddGameFullNotificationAndEmail can be started - Skrupel TNG");

		assertThat(extractEmailContent(mimeMessage)).contains("is full and can now be started!");

		assertThat(getGameFullNotificationCount(6L)).isEqualTo(1L);
	}

	@Test
	void shouldAddGameStartedNotificationsAndSendEmails() throws Exception {
		dashboardService.sendGameStartedNotificationsAndEmails(gameId, 5L);

		assertThat(getGameStartedNotificationCount(1L)).isEqualTo(1);
		assertThat(getGameStartedNotificationCount(5L)).isZero();
		assertThat(getGameStartedNotificationCount(6L)).isEqualTo(1);

		MimeMessage[] messages = mockedEmailServer.getMessages();
		assertThat(messages).hasSize(1);

		MimeMessage mimeMessage = messages[0];
		assertThat(extractEmailContent(mimeMessage)).contains("to make your first turn!");
		assertThat(extractEmailAddress(mimeMessage)).isEqualTo("testuser02@invalidhost.com");
	}

	@Test
	void shouldUpdateTeamsWhenChangingGame() {
		authenticateAsAdmin();

		Game game = gameRepository.getReferenceById(gameId);
		game.setUseFixedTeams(true);
		game = gameRepository.save(game);

		dashboardService.updateTeams(game, false);

		List<Team> results = teamRepository.findByGameId(gameId);

		assertThat(results).hasSize(1);

		game.setUseFixedTeams(false);
		game = gameRepository.save(game);

		dashboardService.updateTeams(game, true);

		results = teamRepository.findByGameId(gameId);

		assertThat(results).isEmpty();
	}
}
