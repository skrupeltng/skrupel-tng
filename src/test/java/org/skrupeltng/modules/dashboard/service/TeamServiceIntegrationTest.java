package org.skrupeltng.modules.dashboard.service;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.config.LoginDetails;
import org.skrupeltng.modules.dashboard.database.TeamRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class TeamServiceIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	TeamService subject;

	@Autowired
	TeamRepository teamRepository;

	@Autowired
	PlayerRepository playerRepository;

	@Test
	void shouldThrowBecauseTeamNotPartOfGame() {
		authenticateAsAdmin();

		assertThatThrownBy(() -> subject.renameTeam(gameId, 1L, "Standard2"))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Team is not part of game!");

		assertThat(teamRepository.getReferenceById(1L).getName()).isEqualTo("Standard");
	}

	@Test
	void shouldNotRenameTeamBecauseNameAlreadyPresent() {
		authenticateAsAdmin();

		boolean result = subject.renameTeam(gameId, 1L, "Bots");
		assertThat(result).isFalse();

		assertThat(teamRepository.getReferenceById(1L).getName()).isEqualTo("Standard");
	}

	@Test
	void shouldNotAddTeamBecauseNameAlreadyPresent() {
		authenticateAsAdmin();

		boolean result = subject.addTeam(gameId, "Bots");
		assertThat(result).isFalse();

		assertThat(teamRepository.findByGameId(gameId)).hasSize(2);
	}

	@Test
	void shouldNotSwitchTeamBecausePlayerNotPartOfGame() {
		authenticateAsAdmin();

		assertThatThrownBy(() -> subject.switchTeam(gameId, 2L, 1L))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Player is not part of game!");

		assertThat(playerRepository.getReferenceById(2L).getTeam()).isNull();
	}

	@Test
	void shouldNotSwitchTeamBecauseTeamNotPartOfGame() {
		authenticateAsAdmin();

		assertThatThrownBy(() -> subject.switchTeam(gameId, 177L, 3L))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Team is not part of game!");

		assertThat(playerRepository.getReferenceById(177L).getTeam().getId()).isEqualTo(1L);
	}

	@Test
	void shouldNotRemoveTeamBecauseTeamNotPartOfGame() {
		authenticateAsAdmin();

		assertThatThrownBy(() -> subject.removeTeam(gameId, 3L))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Team is not part of game!");

		assertThat(teamRepository.findByGameId(gameId)).hasSize(2);
	}

	@Test
	void shouldNotRemoveTeamBecauseTeamIsNotEmpty() {
		authenticateAsAdmin();

		assertThatThrownBy(() -> subject.removeTeam(gameId, 1L))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Team is not empty!");

		assertThat(teamRepository.findByGameId(gameId)).hasSize(2);
	}

	@Test
	void shouldThrowBecauseNeitherGameCreatorNorAdmin() {
		LoginDetails loginDetails = userDetailService.loadUserByUsername("workingTestUser");
		UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(loginDetails, null, loginDetails.getAuthorities());
		SecurityContext sc = SecurityContextHolder.getContext();
		sc.setAuthentication(authReq);

		assertThatThrownBy(() -> subject.getCheckedGame(gameId))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Only game creators and admins can modify teams!");
	}

	@Test
	void shouldThrowBecauseGameAlreadyStarted() {
		authenticateAsAdmin();

		assertThatThrownBy(() -> subject.getCheckedGame(gameId))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Cannot add teams to started games!");
	}

	@Test
	void shouldThrowBecauseNotUsingFixedTeams() {
		authenticateAsAdmin();

		assertThatThrownBy(() -> subject.getCheckedGame(gameId))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("This game does not use fixed teams!");
	}
}
