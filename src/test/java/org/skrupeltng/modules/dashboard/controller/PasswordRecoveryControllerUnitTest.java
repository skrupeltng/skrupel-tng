package org.skrupeltng.modules.dashboard.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.service.LoginService;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PasswordRecoveryControllerUnitTest {

	private PasswordRecoveryController subject;

	@Mock
	private LoginService loginService;
	@Mock
	private ResetPasswordValidator resetPasswordValidator;
	@Mock
	private HttpServletRequest httpServletRequest;
	@Mock
	private BindingResult bindingResult;
	@Mock
	private Model model;

	@BeforeEach
	void setUp() {
		subject = new PasswordRecoveryController(loginService, resetPasswordValidator, httpServletRequest);
	}

	@Test
	void shouldCheckIfTokenHasLogin() {
		String result = subject.showResetPasswordForm("abc", model);

		assertThat(result).isEqualTo("dashboard/password-recovery-not-found");
	}

	@Test
	void shouldCheckIfTokenHasLogin2() {
		String result = subject.resetPassword(new ResetPasswordRequest(), bindingResult, "abc", model);

		assertThat(result).isEqualTo("dashboard/password-recovery-not-found");
	}

	@Test
	void shouldCheckIfPasswordWasResetSuccessfully() {
		when(loginService.getLoginByPasswordRecoveryToken("abc")).thenReturn(Optional.of(new Login()));

		String result = subject.resetPassword(new ResetPasswordRequest(), bindingResult, "abc", model);

		assertThat(result).isEqualTo("dashboard/password-recovery-not-found");
	}
}
