package org.skrupeltng.modules.dashboard.modules.admin.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.InstallationDetailsHelper;
import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.database.DataPrivacyStatementRepository;
import org.skrupeltng.modules.dashboard.database.InstallationDetailsRepository;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.dashboard.service.GameRemoval;
import org.skrupeltng.modules.dashboard.service.LoginRemoval;
import org.skrupeltng.modules.ingame.database.GameRepository;

import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AdminServiceUnitTest {

	AdminService subject;

	@Mock
	LoginRepository loginRepository;

	@Mock
	GameRepository gameRepository;

	@Mock
	UserDetailServiceImpl userDetailService;

	@Mock
	DataPrivacyStatementRepository dataPrivacyStatementRepository;

	@Mock
	InstallationDetailsRepository installationDetailsRepository;

	@Mock
	InstallationDetailsHelper installationDetailsHelper;

	@Mock
	LoginRemoval loginRemoval;

	@Mock
	GameRemoval gameRemoval;

	@BeforeEach
	void setup() {
		subject = new AdminService(loginRepository, gameRepository, userDetailService, dataPrivacyStatementRepository, installationDetailsRepository, installationDetailsHelper, loginRemoval, gameRemoval);
	}

	@Test
	void shouldThrowBecauseAdminTriesToDeleteHimself() {
		when(userDetailService.getLoginId()).thenReturn(1L);

		Login login = new Login(1L);
		assertThatThrownBy(() -> subject.validateUserForDeletion(login)).isInstanceOf(IllegalArgumentException.class).hasMessage("The admin cannot be deleted!");
	}

	@Test
	void shouldThrowBecauseAiLoginIsTriedToBeDeleted() {
		Login login = new Login(1L);
		login.setUsername(AILevel.AI_EASY.name());

		assertThatThrownBy(() -> subject.validateUserForDeletion(login)).isInstanceOf(IllegalStateException.class).hasMessage("AI users cannot be deleted!");
	}

	@Test
	void shouldNotThrowBecauseValidLoginForDeletion() {
		Login login = new Login(1L);
		login.setUsername("SomeUserName");

		assertThatNoException().isThrownBy(() -> subject.validateUserForDeletion(login));
	}
}
