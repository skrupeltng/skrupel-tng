package org.skrupeltng.modules.dashboard.modules.storymode.service.missions.silverstarag;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.dashboard.modules.storymode.service.missions.StoryModeMissionHelper;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntry;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;

import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.skrupeltng.modules.dashboard.modules.storymode.service.missions.silverstarag.SilverStarAGMission1.STEP_11_SETUP_ROUTE;
import static org.skrupeltng.modules.dashboard.modules.storymode.service.missions.silverstarag.SilverStarAGMission1.STEP_12_INVASION_START;

@ExtendWith(MockitoExtension.class)
class SilverStarAGMission1UnitTest {

	private SilverStarAGMission1 subject;

	@Mock
	private PlanetRepository planetRepository;
	@Mock
	private PlayerRepository playerRepository;
	@Mock
	private ShipRepository shipRepository;
	@Mock
	private StarbaseRepository starbaseRepository;
	@Mock
	private WormHoleRepository wormHoleRepository;
	@Mock
	private StoryModeMissionHelper missionHelper;
	@Mock
	private ConfigProperties configProperties;

	private Game game;
	private Player player;
	private Player aiPlayer;

	@BeforeEach
	void setup() {
		subject = new SilverStarAGMission1(
			planetRepository,
			playerRepository,
			shipRepository,
			starbaseRepository,
			wormHoleRepository,
			missionHelper,
			configProperties
		);

		player = new Player(1L);
		aiPlayer = new Player(2L);

		game = new Game(1L);
		game.setPlayers(List.of(player, aiPlayer));

		when(missionHelper.getHumanPlayer(game.getPlayers())).thenReturn(player);
		when(missionHelper.getAIPlayer(game.getPlayers())).thenReturn(aiPlayer);
	}

	@Test
	void shouldSpawnDestroyerOnFarthestPlanetFromHomePlanet() {
		PlanetEntry planet1 = new PlanetEntry(1L, 100, 100);
		PlanetEntry planet2 = new PlanetEntry(2L, 1000, 1000);
		PlanetEntry planet3 = new PlanetEntry(3L, 10, 10);
		when(missionHelper.getVisiblePlanets(player)).thenReturn(List.of(planet1, planet2, planet3));

		player.setHomePlanet(new Planet(4L, 0, 0));

		game.setStoryModeMissionStep(STEP_11_SETUP_ROUTE);

		Ship ship = ShipTestFactory.createShip();
		ship.setRoute(List.of(new ShipRouteEntry(), new ShipRouteEntry()));
		player.setShips(Set.of(ship));

		subject.checkRoundEnd(game);

		verify(missionHelper).setNextStep(game, STEP_12_INVASION_START);
		verify(missionHelper).spawnShip(
			"nightmare_10",
			"transwarp",
			"particle_vortex",
			null,
			989,
			989,
			aiPlayer,
			"Destroyer",
			null
		);
	}
}
