package org.skrupeltng.modules.dashboard.modules.storymode.service.missions.silverstarag;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.ingame.database.Game;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class SilverStarAGMissionProviderUnitTest {

	private SilverStarAGMissionProvider subject;

	@Mock
	private SilverStarAGMission1 mission1;
	@Mock
	private SilverStarAGMission2 mission2;
	@Mock
	private SilverStarAGMission3 mission3;

	@BeforeEach
	void setup() {
		subject = new SilverStarAGMissionProvider(mission1, mission2, mission3);
	}

	@Test
	void shouldCreateMission1() {
		NewGameRequest result = subject.getNewGameRequest(1);
		assertThat(result.getName()).isEqualTo("Mission 1");
	}

	@Test
	void shouldCreateMission2() {
		NewGameRequest result = subject.getNewGameRequest(2);
		assertThat(result.getName()).isEqualTo("Mission 2");
	}

	@Test
	void shouldCreateMission3() {
		NewGameRequest result = subject.getNewGameRequest(3);
		assertThat(result.getName()).isEqualTo("Mission 3");
	}

	@Test
	void shouldCheckEndOfMission1() {
		Game game = new Game();
		game.setStoryModeMission(1);

		Optional<Integer> result = subject.checkRoundEnd(game);

		assertThat(result).isEmpty();
		verify(mission1).checkRoundEnd(game);
	}

	@Test
	void shouldCheckEndOfMission2() {
		Game game = new Game();
		game.setStoryModeMission(2);

		Optional<Integer> result = subject.checkRoundEnd(game);

		assertThat(result).isEmpty();
		verify(mission2).checkRoundEnd(game);
	}

	@Test
	void shouldCheckEndOfMission3() {
		Game game = new Game();
		game.setStoryModeMission(3);

		Optional<Integer> result = subject.checkRoundEnd(game);

		assertThat(result).isEmpty();
		verify(mission3).checkRoundEnd(game);
	}
}
