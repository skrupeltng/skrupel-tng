package org.skrupeltng.modules.ai.easy;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ai.AIConstants;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseShipConstructionJob;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseShipConstructionJobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

class AIEasyStarbasesIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	@Qualifier("AI_EASY")
	AIEasyStarbases starbases;

	@Autowired
	StarbaseRepository starbaseRepository;

	@Autowired
	StarbaseShipConstructionJobRepository shipConstructionJobRepository;

	@Test
	void shouldBuildSubparticleClusterShip() {
		Starbase starbase = starbaseRepository.getReferenceById(103L);
		starbases.buildShip(starbase, 138L);

		Optional<StarbaseShipConstructionJob> jobOpt = shipConstructionJobRepository.findByStarbaseId(103L);
		assertThat(jobOpt).isPresent();
		assertThat(jobOpt.get().getShipTemplate().getId()).isEqualTo("nightmare_18");
		assertThat(jobOpt.get().getShipName()).isEqualTo(AIConstants.AI_SUBPARTICLECLUSTER_NAME);
	}

	@Test
	void shouldBuildQuarksReorganizerShip() {
		Starbase starbase = starbaseRepository.getReferenceById(104L);
		starbases.buildShip(starbase, 139L);

		Optional<StarbaseShipConstructionJob> jobOpt = shipConstructionJobRepository.findByStarbaseId(104L);
		assertThat(jobOpt).isPresent();
		assertThat(jobOpt.get().getShipTemplate().getId()).isEqualTo("nightmare_15");
		assertThat(jobOpt.get().getShipName()).isEqualTo(AIConstants.AI_QUARKREORGANIZER_NAME);
	}

	@Test
	void shouldNotBuildAnyShip() {
		Starbase starbase = starbaseRepository.getReferenceById(105L);
		starbases.buildShip(starbase, 140L);

		assertThat(shipConstructionJobRepository.findByStarbaseId(105L)).isEmpty();
	}
}
