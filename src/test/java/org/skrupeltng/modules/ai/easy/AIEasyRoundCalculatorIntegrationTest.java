package org.skrupeltng.modules.ai.easy;

import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.ai.AbstractAIIntegrationTest;
import org.skrupeltng.modules.dashboard.service.GameFullException;

class AIEasyRoundCalculatorIntegrationTest extends AbstractAIIntegrationTest {

	@Test
	void shouldBehaveCorrectlyinFirstTwoRounds() throws GameFullException {
		checkFirstTwoAIRounds(AILevel.AI_EASY);
	}
}