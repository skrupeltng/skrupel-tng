package org.skrupeltng.modules.ai.easy;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequest;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import static org.assertj.core.api.Assertions.assertThat;

class AIEasyPoliticsIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	@Qualifier("AI_EASY")
	AIEasyPolitics subject;

	@Autowired
	PlayerRepository playerRepository;

	@Autowired
	PlayerRelationRepository playerRelationRepository;

	@Autowired
	PlayerRelationRequestRepository playerRelationRequestRepository;

	@Test
	void shouldAcceptAllRequests() {
		subject.processPolitics(166L);

		assertThat(playerRelationRepository.findByPlayerId(166L)).hasSize(1);
		assertThat(playerRelationRequestRepository.findIncomingRequests(166L)).isEmpty();

		PlayerRelationRequest newRequest = new PlayerRelationRequest();
		newRequest.setType(PlayerRelationType.ALLIANCE);
		newRequest.setRequestingPlayer(playerRepository.getReferenceById(165L));
		newRequest.setOtherPlayer(playerRepository.getReferenceById(166L));
		playerRelationRequestRepository.save(newRequest);
		subject.processPolitics(166L);

		assertThat(playerRelationRepository.findByPlayerId(166L)).hasSize(1);
		assertThat(playerRelationRequestRepository.findIncomingRequests(166L)).isEmpty();
	}
}
