package org.skrupeltng.modules.ai.easy;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ai.easy.ships.AIEasyShips;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

class AIEasyShipsIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	@Qualifier("AI_EASY")
	AIEasyShips ships;

	@Autowired
	ShipRepository shipRepository;

	@Test
	void shouldActivateShipAbilitiesForGenerationShips() {
		ships.processShips(140L);

		Ship ship = shipRepository.getReferenceById(195L);
		assertThat(ship.getActiveAbility()).isEqualTo(ship.getAbility(ShipAbilityType.SUB_PARTICLE_CLUSTER).get());
		assertThat(ship.getTaskValue()).isEqualTo("true");

		ship = shipRepository.getReferenceById(196L);
		assertThat(ship.getActiveAbility()).isEqualTo(ship.getAbility(ShipAbilityType.QUARK_REORGANIZER).get());
		assertThat(ship.getTaskValue()).isEqualTo("true");
	}
}
