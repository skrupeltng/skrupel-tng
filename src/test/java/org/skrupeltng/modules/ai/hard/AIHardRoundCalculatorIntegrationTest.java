package org.skrupeltng.modules.ai.hard;

import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.ai.AbstractAIIntegrationTest;
import org.skrupeltng.modules.dashboard.service.GameFullException;

class AIHardRoundCalculatorIntegrationTest extends AbstractAIIntegrationTest {

	@Test
	void shouldBehaveCorrectlyinFirstTwoRounds() throws GameFullException {
		checkFirstTwoAIRounds(AILevel.AI_HARD);
	}
}