package org.skrupeltng.modules.ai.medium;

import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class AIMediumPlanetsUnitTest {

	private final AIMediumPlanets subject = new AIMediumPlanets();

	@Test
	void shouldNotSetAutoSellSuppliesBecauseFullyUpgradedStarbasePresent() {
		Planet planet = new Planet();

		Planet planetWithStarbase = new Planet();
		Starbase starbase = new Starbase();
		planetWithStarbase.setStarbase(starbase);

		starbase.setHullLevel(10);
		starbase.setPropulsionLevel(10);
		starbase.setEnergyLevel(10);
		starbase.setProjectileLevel(10);

		subject.sellSupplies(planet, List.of(planet, planetWithStarbase));

		assertThat(planet.isAutoSellSupplies()).isFalse();
	}

	@Test
	void shouldNotSetAutoSellSuppliesBecauseFactoriesNotAtMaximum() {
		Planet planet = new Planet();
		planet.setColonists(20000000);
		planet.setMoney(1000000);
		planet.setSupplies(1000000);
		planet.setFactories(10);
		planet.setMines(Planet.MAX_MINES);

		subject.sellSupplies(planet, List.of(planet));

		assertThat(planet.isAutoSellSupplies()).isFalse();
	}

	@Test
	void shouldNotSetAutoSellSuppliesBecauseMinesNotAtMaximum() {
		Planet planet = new Planet();
		planet.setColonists(20000000);
		planet.setMoney(1000000);
		planet.setSupplies(1000000);
		planet.setFactories(Planet.MAX_FACTORIES);
		planet.setMines(10);

		subject.sellSupplies(planet, List.of(planet));

		assertThat(planet.isAutoSellSupplies()).isFalse();
	}

	@Test
	void shouldSetAutoSellSupplies() {
		Planet planet = new Planet();
		planet.setColonists(20000000);
		planet.setMoney(1000000);
		planet.setSupplies(1000000);
		planet.setFactories(Planet.MAX_FACTORIES);
		planet.setMines(Planet.MAX_MINES);

		subject.sellSupplies(planet, List.of(planet));

		assertThat(planet.isAutoSellSupplies()).isTrue();
	}

	@Test
	void shouldSellSuppliesBecauseMoreThanThreshold() {
		Planet planet = new Planet();
		planet.setColonists(1000);
		planet.setMoney(100);
		planet.setSupplies(200);
		planet.setStarbase(new Starbase());

		subject.sellSupplies(planet, List.of(planet));

		assertThat(planet.isAutoSellSupplies()).isFalse();
		assertThat(planet.getSupplies()).isEqualTo(50);
		assertThat(planet.getMoney()).isEqualTo(250);
	}
}
