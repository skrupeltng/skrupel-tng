package org.skrupeltng.modules;

import org.apache.commons.csv.CSVRecord;
import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpeciesType;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.service.ShipFactionAbilityItem;
import org.skrupeltng.modules.masterdata.service.ShipFactionItem;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class FactionFormatMigrationUnitTest {

	static final String TEST_DATA_DIR = "src/test/resources/faction_migration_test_data/testFaction";

	List<CSVRecord> loadShipTestRecords() throws IOException {
		return FactionFormatMigration.loadShipRecords(TEST_DATA_DIR);
	}

	String loadGermanTestDescription() throws IOException {
		return FactionFormatMigration.loadGermanDescription(TEST_DATA_DIR);
	}

	List<String> loadFactionTestData() throws IOException {
		return FactionFormatMigration.loadFactionData(TEST_DATA_DIR);
	}

	FactionFormatMigration createSubject() throws IOException {
		boolean migrateImages = false;
		List<CSVRecord> shipRecords = loadShipTestRecords();
		String germanDescription = loadGermanTestDescription();
		List<String> factionData = loadFactionTestData();
		boolean dryRun = true;

		return new FactionFormatMigration("testFaction", migrateImages, shipRecords, germanDescription, factionData, dryRun);
	}

	@Test
	void shouldMigrateFaction() throws IOException {
		FactionFormatMigration subject = createSubject();
		subject.migrateFaction();

		Faction faction = subject.faction;
		assertThat(faction).isNotNull();
		assertThat(faction.getId()).isEqualTo("testFaction");
		assertThat(faction.getHomePlanetName()).isEqualTo("TestFactionHomePlanet");

		assertThat(faction.getAssimilationRate()).isEqualTo(0f);
		assertThat(faction.getAssimilationType()).isNull();

		assertThat(faction.getFactoryProductionRate()).isEqualTo(0.68f);
		assertThat(faction.getMineProductionRate()).isEqualTo(1.42f);
		assertThat(faction.getGroundCombatAttackRate()).isEqualTo(0.72f);
		assertThat(faction.getGroundCombatDefenseRate()).isEqualTo(1.29f);
		assertThat(faction.getTaxRate()).isEqualTo(1.05f);

		assertThat(faction.getPreferredPlanetType()).isEqualTo("L");
		assertThat(faction.getPreferredTemperature()).isEqualTo(26);

		assertThat(faction.getForbiddenOrbitalSystems()).isEqualTo(OrbitalSystemType.RECREATIONAL_PARK.name());
		assertThat(faction.getUnlockedOrbitalSystems()).isEqualTo(OrbitalSystemType.HOBAN_BUD.name());

		List<ShipFactionItem> ships = subject.ships;
		assertThat(ships).hasSize(3);

		ShipFactionItem ship1 = ships.get(0);
		verifyShip(ship1);
	}

	private void verifyShip(ShipFactionItem ship1) {
		assertThat(ship1.getId()).isEqualTo(1);
		assertThat(ship1.getCostMineral1()).isEqualTo(6);
		assertThat(ship1.getCostMineral2()).isEqualTo(6);
		assertThat(ship1.getCostMineral3()).isEqualTo(8);
		assertThat(ship1.getCostMoney()).isEqualTo(55);
		assertThat(ship1.getCrew()).isEqualTo(12);
		assertThat(ship1.getMass()).isEqualTo(40);
		assertThat(ship1.getPropulsionSystemsCount()).isEqualTo(1);
		assertThat(ship1.getEnergyWeaponsCount()).isEqualTo(1);
		assertThat(ship1.getProjectileWeaponsCount()).isZero();
		assertThat(ship1.getHangarCapacity()).isZero();
		assertThat(ship1.getFuelCapacity()).isEqualTo(200);
		assertThat(ship1.getStorageSpace()).isEqualTo(150);
		assertThat(ship1.getName()).isEqualTo(List.of(Map.of("de", "Sporenkapsel")));
		assertThat(ship1.getTechLevel()).isEqualTo(3);

		Map<String, String> jumpEngineCosts = Map.of(ShipAbilityConstants.JUMP_ENGINE_COSTS, "32");
		Map<String, String> jumpEngineMaxRange = Map.of(ShipAbilityConstants.JUMP_ENGINE_MAX, "1000");
		Map<String, String> jumpEngineMinRange = Map.of(ShipAbilityConstants.JUMP_ENGINE_MIN, "500");

		ShipFactionAbilityItem jumpEngineAbility = new ShipFactionAbilityItem(ShipAbilityType.JUMP_ENGINE.name(), List.of(jumpEngineCosts, jumpEngineMaxRange, jumpEngineMinRange));
		ShipFactionAbilityItem structureScannerAbility = new ShipFactionAbilityItem(ShipAbilityType.STRUCTUR_SCANNER.name(), null);
		assertThat(ship1.getAbilities()).isEqualTo(List.of(jumpEngineAbility, structureScannerAbility));
	}

	@Test
	void shouldAssignAssimilationType() {
		List<String> factionDataLines = List.of("", "", "", "", "0.5:2");
		FactionFormatMigration subject = new FactionFormatMigration("withAssimilation", false, null, null, factionDataLines, true);

		subject.setAssimiliationAttributes();

		assertThat(subject.faction.getAssimilationRate()).isEqualTo(0.5f);
		assertThat(subject.faction.getAssimilationType()).isEqualTo(NativeSpeciesType.ENERGY);
	}
}
