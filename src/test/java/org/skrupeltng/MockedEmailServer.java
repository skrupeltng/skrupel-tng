package org.skrupeltng;

import jakarta.mail.internet.MimeMessage;

import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;

public class MockedEmailServer implements BeforeEachCallback, AfterEachCallback {

	private final int port;

	private GreenMail smtpServer;

	public MockedEmailServer(int port) {
		this.port = port;
	}

	@Override
	public void beforeEach(ExtensionContext context) {
		smtpServer = new GreenMail(new ServerSetup(port, null, "smtp"));
		smtpServer.setUser("some.invalid@invalidhost.com", "SomeInvalidPassword");
		smtpServer.start();
	}

	public MimeMessage[] getMessages() {
		return smtpServer.getReceivedMessages();
	}

	@Override
	public void afterEach(ExtensionContext context) {
		smtpServer.stop();
	}
}
