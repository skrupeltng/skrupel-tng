package org.skrupeltng;

import jakarta.mail.Message;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.TestInfo;
import org.skrupeltng.config.LoginDetails;
import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.dashboard.modules.notification.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.fail;

@Tag("INTEGRATION_TESTS")
public abstract class AbstractIntegrationTest extends AbstractDockerTest {

	private static final Map<String, Long> gameIds = new HashMap<>();

	@BeforeAll
	public static void initGameIds() {
		// NewsServiceIntegrationTest
		gameIds.put("NewsServiceIntegrationTest.shouldAddNewsEntryWithoutArguments", 2L);
		gameIds.put("NewsServiceIntegrationTest.shouldAddNewsEntryWithSingleArgument", 3L);
		gameIds.put("NewsServiceIntegrationTest.shouldAddNewsEntryWithMultipleArgument", 4L);
		gameIds.put("NewsServiceIntegrationTest.shouldClearNewsEntries", 1L);
		gameIds.put("NewsServiceIntegrationTest.shouldCreateRoundSummary", 8L);
		gameIds.put("NewsServiceIntegrationTest.shouldNotToggleBecauseNotFound", 1L);
		gameIds.put("NewsServiceIntegrationTest.shouldToggleDelete", 1L);

		// InvasionServiceIntegrationTest
		gameIds.put("InvasionServiceIntegrationTest.shouldNotSpawnWaveBecauseNotInvasion", 1L);
		gameIds.put("InvasionServiceIntegrationTest.shouldNotSpawnWaveBecauseNoConfigFound", 2L);
		gameIds.put("InvasionServiceIntegrationTest.shouldSpawnOneWaveBecauseOnlyOnePlayer", 3L);
		gameIds.put("InvasionServiceIntegrationTest.shouldSpawnThreeWavesBecauseThreePlayers", 4L);
		gameIds.put("InvasionServiceIntegrationTest.shouldSetupCooperativeGame", 5L);
		gameIds.put("InvasionServiceIntegrationTest.shouldSetupCompetitiveGame", 6L);
		gameIds.put("InvasionServiceIntegrationTest.shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets", 7L);
		gameIds.put("InvasionServiceIntegrationTest.shouldOnlyTargetPlanetsBecauseEasy", 8L);
		gameIds.put("InvasionServiceIntegrationTest.shouldTargetPlanetsBecauseShipsTooFarAway", 9L);
		gameIds.put("InvasionServiceIntegrationTest.shouldTargetShips", 10L);
		gameIds.put("InvasionServiceIntegrationTest.shouldCloakShips", 11L);
		gameIds.put("InvasionServiceIntegrationTest.shouldUseSignatureMask", 12L);
		gameIds.put("InvasionServiceIntegrationTest.shouldUseSubSpaceDistortion", 13L);
		gameIds.put("InvasionServiceIntegrationTest.shouldUseViralInvasion", 14L);

		// RouteRoundCalculatorTest
		gameIds.put("RouteRoundCalculatorIntegrationTest.shouldNotRefuelAndNotSetNextRouteEntry", 15L);
		gameIds.put("RouteRoundCalculatorIntegrationTest.shouldRefuelAndSetNextRouteEntry", 16L);
		gameIds.put("RouteRoundCalculatorIntegrationTest.shouldNotRefuelAndNotLoadResourcesBecauseNoRoutePlanet", 17L);
		gameIds.put("RouteRoundCalculatorIntegrationTest.shouldRefuelAndUnloadResources", 18L);
		gameIds.put("RouteRoundCalculatorIntegrationTest.shouldAlwaysHonorPrimaryResource", 38L);

		// PoliticsRoundCalculatorIntegrationTest
		gameIds.put("PoliticsRoundCalculatorIntegrationTest.shouldDeleteAndCountDown", 19L);
		gameIds.put("PoliticsRoundCalculatorIntegrationTest.shouldCreateTradeBonusData", 20L);

		// ShipRoundTasksIntegrationTest
		gameIds.put("ShipRoundTasksIntegrationTest.shouldResetShipTasksBecauseNoValueForTractorBeam", 21L);
		gameIds.put("ShipRoundTasksIntegrationTest.shouldResetShipTasksBecauseTargetNotOnSamePosition", 22L);
		gameIds.put("ShipRoundTasksIntegrationTest.shouldManageTractoredShipAndLimitSpeed", 23L);
		gameIds.put("ShipRoundTasksIntegrationTest.shouldDestroyAndDamageShipsByAutoDestructionShockWave", 43L);
		gameIds.put("ShipRoundTasksIntegrationTest.shouldKillMostColonistsInPlanetaryBombardment", 48L);
		gameIds.put("ShipRoundTasksIntegrationTest.shouldNotKillMostColonistsInPlanetaryBombardmentBecauseBunker", 49L);
		gameIds.put("ShipRoundTasksIntegrationTest.shouldKillColonistsToBunkerLimitInPlanetaryBombardment", 50L);

		// AITests
		gameIds.put("AIEasyRoundCalculatorIntegrationTest.shouldBehaveCorrectlyinFirstTwoRounds", 1L);
		gameIds.put("AIMediumRoundCalculatorIntegrationTest.shouldBehaveCorrectlyinFirstTwoRounds", 1L);
		gameIds.put("AIHardRoundCalculatorIntegrationTest.shouldBehaveCorrectlyinFirstTwoRounds", 1L);

		// LoginServiceIntegrationTest
		gameIds.put("LoginServiceIntegrationTest.shouldNotSendActivationMailBecauseActivationMailIsDeactivated", 1L);

		// DashboardServiceIntegrationTest
		gameIds.put("DashboardServiceIntegrationTest.shouldDoNothingBecauseIsCreator", 1L);
		gameIds.put("DashboardServiceIntegrationTest.shouldNotSendJoinedEmailBecauseNotificationDeactivated", 24L);
		gameIds.put("DashboardServiceIntegrationTest.shouldSendJoinedNotificationEmail", 25L);
		gameIds.put("DashboardServiceIntegrationTest.shouldDoNothingBecauseGameNotFull", 24L);
		gameIds.put("DashboardServiceIntegrationTest.shouldDoNothingBecauseNotAllPlayersHaveFaction", 26L);
		gameIds.put("DashboardServiceIntegrationTest.shouldAddGameFullNotification", 27L);
		gameIds.put("DashboardServiceIntegrationTest.shouldAddGameFullNotificationAndEmail", 28L);
		gameIds.put("DashboardServiceIntegrationTest.shouldAddGameStartedNotificationsAndSendEmails", 29L);
		gameIds.put("DashboardServiceIntegrationTest.shouldUpdateTeamsWhenChangingGame", 104L);

		// IngameServiceIntegrationTest
		gameIds.put("IngameServiceIntegrationTest.shouldNotCalculateRoundBecauseNotAllFinished", 30L);
		gameIds.put("IngameServiceIntegrationTest.shouldNotifyAndSendMailsToCorrectLogins", 32L);
		gameIds.put("IngameServiceIntegrationTest.shouldFinishTurn", 97L);
		gameIds.put("IngameServiceIntegrationTest.shouldThrowBecauseLoginNotPartOfGame", 97L);

		// SpaceCombatRoundCalculatorIntegrationTest
		gameIds.put("SpaceCombatRoundCalculatorIntegrationTest.shouldMutuallyDestroyWithoutWeapons", 33L);
		gameIds.put("SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyWeakerShip", 34L);
		gameIds.put("SpaceCombatRoundCalculatorIntegrationTest.shouldCaptureShip", 35L);
		gameIds.put("SpaceCombatRoundCalculatorIntegrationTest.shouldEvadeEachOtherBecauseBothDefensive", 36L);
		gameIds.put("SpaceCombatRoundCalculatorIntegrationTest.shouldEvadeEachOtherAfterFirstCombatRound", 37L);
		gameIds.put("SpaceCombatRoundCalculatorIntegrationTest.shouldWinBecauseOfSupport", 40L);
		gameIds.put("SpaceCombatRoundCalculatorIntegrationTest.shouldRecordCombatLogCorrectly", 41L);
		gameIds.put("SpaceCombatRoundCalculatorIntegrationTest.shouldEvadeShips", 54L);
		gameIds.put("SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyAndCaptureShips", 55L);
		gameIds.put("SpaceCombatRoundCalculatorIntegrationTest.shouldLogCombatWithEvasion", 60L);
		gameIds.put("SpaceCombatRoundCalculatorIntegrationTest.shouldMutuallyDestroyEachOther", 61L);
		gameIds.put("SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal", 67L);

		// PlanetRepositoryIntegrationTest
		gameIds.put("PlanetRepositoryIntegrationTest.shouldCountOrbitalSystemsCorrectly", 39L);

		// ShipRoundAbilitiesIntegrationTest
		gameIds.put("ShipRoundAbilitiesIntegrationTest.shouldDestroyAndDamageShipsBySubSpaceDistortion", 42L);
		gameIds.put("ShipRoundAbilitiesIntegrationTest.shouldDeleteShipsWithShockWave", 63L);
		gameIds.put("ShipRoundAbilitiesIntegrationTest.shouldViralInvadeEnemyColonyWithSingleShip", 44L);
		gameIds.put("ShipRoundAbilitiesIntegrationTest.shouldViralInvadeEnemyColonyWithMultipleShips", 45L);
		gameIds.put("ShipRoundAbilitiesIntegrationTest.shouldNotViralInvadeEnemyColonyWithSingleShipBecauseMedicalCenter", 46L);
		gameIds.put("ShipRoundAbilitiesIntegrationTest.shouldNotViralInvadeEnemyColonyWithMultipleShipsBecauseMedicalCenter", 47L);
		gameIds.put("ShipRoundAbilitiesIntegrationTest.shouldViralInvadeNativeSpeciesOfUninhabitedPlanet", 56L);
		gameIds.put("ShipRoundAbilitiesIntegrationTest.shouldViralInvadeNativeSpeciesOfOwnPlanet", 57L);
		gameIds.put("ShipRoundAbilitiesIntegrationTest.shouldNotViralInvadeColonistsOfOwnPlanet", 58L);
		gameIds.put("ShipRoundAbilitiesIntegrationTest.shouldOnlyProcessViralInvasionForShipsUsingIt", 59L);

		// PlayerEncounterServiceIntegrationTest
		gameIds.put("PlayerEncounterServiceIntegrationTest.shouldNotAddAnyEncountersBecauseNoShipsExist", 51L);
		gameIds.put("PlayerEncounterServiceIntegrationTest.shouldNotAddAnyEncountersBecauseNoShipsInRangeOfEachOther", 52L);
		gameIds.put("PlayerEncounterServiceIntegrationTest.shouldAddTwoEncounters", 53L);
		gameIds.put("PlayerEncounterServiceIntegrationTest.shouldAddTwoEncountersForShipEnteringPlanetOrbit", 64L);
		gameIds.put("PlayerEncounterServiceIntegrationTest.shouldAddOneEncounterForPlanetInShipScannerRange", 65L);

		// ShipAbilityDescriptionMapperIntegrationTest
		gameIds.put("ShipAbilityDescriptionMapperIntegrationTest.shouldCreateShipAbilityDescription", 1L);

		// PlayerRoundStatsServiceIntegrationTest
		gameIds.put("PlayerRoundStatsServiceIntegrationTest.shouldReturnStatsForFinishedInvasionGame", 62L);

		// VisibleObjectsIntegrationTest
		gameIds.put("VisibleObjectsIntegrationTest.shouldDisplayCloakedShips", 66L);

		// DeathDeathFoeGameRemovalIntegrationTest
		gameIds.put("DeathDeathFoeGameRemovalIntegrationTest.shouldDeleteTeamDeathFoeGame", 68L);

		// DeathDeathFoeGameRemovalIntegrationTest
		gameIds.put("GameStartHelperIntegrationTest.shouldCreateStartPositionsWithEqualDistance", 69L);

		// GameRepositoryIntegrationTest
		gameIds.put("GameRepositoryIntegrationTest.shouldReturnMyGames", 1L);
		gameIds.put("GameRepositoryIntegrationTest.shouldReturnOpenGames", 1L);

		// AIEasyStarbasesIntegrationTest
		gameIds.put("AIEasyStarbasesIntegrationTest.shouldBuildSubparticleClusterShip", 73L);
		gameIds.put("AIEasyStarbasesIntegrationTest.shouldBuildQuarksReorganizerShip", 74L);
		gameIds.put("AIEasyStarbasesIntegrationTest.shouldNotBuildAnyShip", 75L);

		// AIEasyShipsIntegrationTest
		gameIds.put("AIEasyShipsIntegrationTest.shouldActivateShipAbilitiesForGenerationShips", 75L);

		// ShipOrdersServiceIntegrationTest
		gameIds.put("ShipOrdersServiceIntegrationTest.shouldActivateSimpleShipAbility", 78L);
		gameIds.put("ShipOrdersServiceIntegrationTest.shouldActivateShipAbilityWithTaskValue", 78L);
		gameIds.put("ShipOrdersServiceIntegrationTest.shouldResetTravelBecauseEscortTargetReset", 78L);
		gameIds.put("ShipOrdersServiceIntegrationTest.shouldChangeEscortTarget", 78L);

		// ShipTransportServiceIntegrationTest0
		gameIds.put("ShipTransportServiceIntegrationTest.shouldReturnPlanetInExtendedTransporterRange", 88L);

		// ShipServiceIntegrationTest
		gameIds.put("ShipServiceIntegrationTest.shouldReturnAllShipsInExtendedTransporterRange", 88L);
		gameIds.put("ShipServiceIntegrationTest.shouldReturnNoOtherShipsBecauseNoneAreInRange", 88L);

		// GroundCombatRoundCalculatorIntegrationTest
		gameIds.put("GroundCombatRoundCalculatorIntegrationTest.shouldCalculateForPlanetWithoutColonistsButGroundUnits", 94L);

		// AIEasyPoliticsIntegrationTest
		gameIds.put("AIEasyPoliticsIntegrationTest.shouldAcceptAllRequests", 96L);

		// WormholeCreatorIntegrationTest
		gameIds.put("WormholeCreatorIntegrationTest.shouldAddUnstableWormholes", 98L);
		gameIds.put("WormholeCreatorIntegrationTest.shouldAddStableWormholes", 99L);
		gameIds.put("WormholeCreatorIntegrationTest.shouldNotCreateWormholeBecausePlanetTooNear", 67L);
		gameIds.put("WormholeCreatorIntegrationTest.shouldNotCreateWormholeBecauseOtherWormholeTooNear", 67L);

		// WinConditionCalculatorIntegrationTest
		gameIds.put("WinConditionCalculatorIntegrationTest.shouldFinishGameBecauseAllPlayersLost", 100L);
		gameIds.put("WinConditionCalculatorIntegrationTest.shouldFinishGameBecauseAllPlayersWon", 101L);

		// GameStartPlayerHelperIntegrationTest
		gameIds.put("GameStartPlayerHelperIntegrationTest.shouldAssignPlayerRelations", 102L);
		gameIds.put("GameStartPlayerHelperIntegrationTest.shouldAssignTeamDeathFoes", 102L);
		gameIds.put("GameStartPlayerHelperIntegrationTest.shouldAssignDeathFoes", 103L);

		// TeamServiceIntegrationTest
		gameIds.put("TeamServiceIntegrationTest.shouldThrowBecauseTeamNotPartOfGame", 105L);
		gameIds.put("TeamServiceIntegrationTest.shouldNotRenameTeamBecauseNameAlreadyPresent", 102L);
		gameIds.put("TeamServiceIntegrationTest.shouldNotAddTeamBecauseNameAlreadyPresent", 102L);
		gameIds.put("TeamServiceIntegrationTest.shouldNotSwitchTeamBecausePlayerNotPartOfGame", 102L);
		gameIds.put("TeamServiceIntegrationTest.shouldNotSwitchTeamBecauseTeamNotPartOfGame", 102L);
		gameIds.put("TeamServiceIntegrationTest.shouldNotRemoveTeamBecauseTeamNotPartOfGame", 102L);
		gameIds.put("TeamServiceIntegrationTest.shouldNotRemoveTeamBecauseTeamIsNotEmpty", 102L);
		gameIds.put("TeamServiceIntegrationTest.shouldThrowBecauseNeitherGameCreatorNorAdmin", 102L);
		gameIds.put("TeamServiceIntegrationTest.shouldThrowBecauseGameAlreadyStarted", 101L);
		gameIds.put("TeamServiceIntegrationTest.shouldThrowBecauseNotUsingFixedTeams", 103L);

		// ConquestServiceIntegrationTest
		gameIds.put("ConquestServiceIntegrationTest.shouldNotProcessBecauseNotConquest", 101L);
		gameIds.put("ConquestServiceIntegrationTest.shouldNotProcessBecauseNoPlayerHasConqueredPlanet", 106L);
		gameIds.put("ConquestServiceIntegrationTest.shouldNotProcessBecauseNoPlayerHasShipInCenter", 107L);
		gameIds.put("ConquestServiceIntegrationTest.shouldNotProcessBecauseMultiplePlayersRivalHaveShipsInCenter", 108L);
		gameIds.put("ConquestServiceIntegrationTest.shouldGainVictoryPointBecauseCenterPlanetOwned", 109L);
		gameIds.put("ConquestServiceIntegrationTest.shouldGainVictoryPointBecauseOnePlayerHasShipsInCenter", 110L);
		gameIds.put("ConquestServiceIntegrationTest.shouldGainVictoryPointBecausePlayersOfSameTeamHaveShipsInCenter", 111L);

		// MatchMakingServiceIntegrationTest
		gameIds.put("MatchMakingServiceIntegrationTest.shouldFindNoMatchesBecausePlayerCount", 1L);
		gameIds.put("MatchMakingServiceIntegrationTest.shouldFindSingleMatchAndCreateGame", 1L);
		gameIds.put("MatchMakingServiceIntegrationTest.shouldFindMultipleMatchesAndCreateGame", 1L);
		gameIds.put("MatchMakingServiceIntegrationTest.shouldSaveMatchMakingSettings", 1L);
		gameIds.put("MatchMakingServiceIntegrationTest.shouldFindDuelAndCreateGame", 1L);

		// MatchMakingServiceIntegrationTest
		gameIds.put("ShipOptionsServiceIntegrationTest.shouldGiveShipToPlayer", 1L);

		// ShipControllerIntegrationTest
		gameIds.put("ShipControllerIntegrationTest.shouldFetchAllies", 4L);
		gameIds.put("ShipControllerIntegrationTest.shouldNotFetchAllies", 1L);
	}

	@Autowired
	protected NotificationService notificationService;

	@Autowired
	protected UserDetailServiceImpl userDetailService;

	protected String testName;

	protected long gameId;

	@BeforeEach
	public void initIntegrationTest(TestInfo testInfo) {
		testName = testInfo.getDisplayName();
		testName = testName.substring(0, testName.length() - 2);

		String methodName = testName;
		String key = getClass().getSimpleName() + "." + methodName;
		Long gameId = gameIds.get(key);

		if (gameId == null) {
			fail("No gameId registered for test method '" + key + "'!");
		}

		this.gameId = gameId;
	}

	protected void authenticateAsAdmin() {
		LoginDetails loginDetails = userDetailService.loadUserByUsername("admin");
		UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(loginDetails, null, loginDetails.getAuthorities());
		SecurityContext sc = SecurityContextHolder.getContext();
		sc.setAuthentication(authReq);
	}

	protected long getNotificationCount(long loginId, String expectation) {
		return notificationService.getAllNotifications(loginId).stream().filter(n -> n.getMessage().contains(expectation)).count();
	}

	protected String extractEmailContent(MimeMessage mimeMessage) throws Exception {
		MimeMultipart content = (MimeMultipart) mimeMessage.getContent();
		return extractEmailContentFromMimeMultipart(content);
	}

	private String extractEmailContentFromMimeMultipart(MimeMultipart mimeMultipart) throws Exception {
		Object content = mimeMultipart.getBodyPart(0).getContent();

		if (content instanceof String text) {
			return text;
		}

		return extractEmailContentFromMimeMultipart((MimeMultipart) content);
	}

	protected String extractEmailAddress(MimeMessage mimeMessage) throws Exception {
		return ((InternetAddress) mimeMessage.getRecipients(Message.RecipientType.TO)[0]).getAddress();
	}
}
