package org.skrupeltng;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.wait.strategy.LogMessageWaitStrategy;
import org.testcontainers.utility.DockerImageName;

import java.time.Duration;

import static java.time.temporal.ChronoUnit.SECONDS;

@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace = Replace.NONE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {AbstractDockerTest.Initializer.class})
@AutoConfigureMockMvc
@Transactional(propagation = Propagation.REQUIRED)
public abstract class AbstractDockerTest {

	private static final Logger log = LoggerFactory.getLogger(AbstractDockerTest.class);

	@LocalServerPort
	protected int port;

	@RegisterExtension
	protected MockedEmailServer mockedEmailServer = new MockedEmailServer(2525);

	static final PostgreSQLContainer<?> postgreSQLContainer = createPostgresContainer();

	static {
		postgreSQLContainer.start();
	}

	@SuppressWarnings("resource")
	protected static PostgreSQLContainer<?> createPostgresContainer() {
		String imageVersion = System.getProperty("postgresImageVersion");

		if (StringUtils.isBlank(imageVersion)) {
			imageVersion = "develop";
		}

		DockerImageName imageName = DockerImageName.parse("registry.gitlab.com/skrupeltng/skrupel-tng/postgres-it:" + imageVersion)
				.asCompatibleSubstituteFor("postgres:14.1");

		return new PostgreSQLContainer<>(imageName)
				.withDatabaseName("skrupel_integration_test")
				.withUsername("skrupeladmin")
				.withPassword("skrupel")
				// the LogMessageWaitStrategy needs to be overwritten because the pre-built postgres image only prints out the message once instead of twice
				.waitingFor(new LogMessageWaitStrategy()
						.withRegEx(".*database system is ready to accept connections.*\\s")
						.withTimes(1)
						.withStartupTimeout(Duration.of(10, SECONDS)));
	}

	static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

		@Override
		public void initialize(@NotNull ConfigurableApplicationContext context) {
			init(context);
		}
	}

	protected static void init(ConfigurableApplicationContext context) {
		log.info(postgreSQLContainer.getJdbcUrl());

		String[] allConfigs = new String[]{
				"spring.liquibase.contexts=test",
				"spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
				"spring.datasource.username=" + postgreSQLContainer.getUsername(),
				"spring.datasource.password=" + postgreSQLContainer.getPassword(),
				"skr.auto_destruct_ship_module_enabled=true",
				"skr.skip_faction_update=true",
				"spring.mail.host=localhost",
				"spring.mail.port=2525",
				"spring.mail.username=some.invalid@invalidhost.com",
				"spring.mail.password=SomeInvalidPassword",
				"spring.mail.properties.mail.smtpauth=true",
				"spring.mail.properties.mail.smtpstarttls.enable=true"
		};

		TestPropertyValues.of(allConfigs).applyTo(context.getEnvironment());
	}
}
