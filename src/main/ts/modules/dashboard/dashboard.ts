document.addEventListener('DOMContentLoaded', () => {
    let id = 'skr-dashboard-my-games';

    if (window.location.pathname.indexOf('/') >= 0) {
        id = window.location.pathname.replace('/', 'skr-dashboard-');

        const parts = id.split('/');
        id = parts[0];
    }

    const elem = document.getElementById(id);

    if (elem) {
        elem.classList.add('active');
    }
});
