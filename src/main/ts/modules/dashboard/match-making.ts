import {Modal} from "bootstrap";

class MatchMaking {

    public init() {
        const urlParams = new URLSearchParams(window.location.search);
        const gameId = urlParams.get('gameId');

        if (gameId) {
            const button = document.getElementById('skr-game-created-confirm-button') as HTMLButtonElement;
            button.addEventListener('click', () => window.location.href = `/ingame/game?id=${gameId}`);

            const modal = new Modal(document.getElementById('skr-game-created-modal'));
            modal.show();
        } else if (urlParams.get('noMatch') === 'true') {
            document.getElementById('skr-match-making-saved-info').style.display = 'block';
        }
    }
}

const matchMaking = new MatchMaking();
(window as any).matchMaking = matchMaking;
