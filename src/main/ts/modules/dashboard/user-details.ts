class UserDetails {
    deleteUser() {
        fetch('/user', {
            method: 'DELETE'
        }).then(() => {
            (window as any).logout();
        });
    }

    changeLanguage(language) {
        fetch(`user/language?language=${language}`, {
            method: 'POST'
        }).then();
    }
}

export const userDetails = new UserDetails();
(window as any).userDetails = userDetails;
