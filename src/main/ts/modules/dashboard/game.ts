import {joinGame} from './open-games';
import {Modal, Tab} from "bootstrap";
import {ModifyTeamRequest} from "./modify-team-request.interface";

const gameId = parseInt(new URLSearchParams(window.location.search).get('id'));

class Game {
    private selectedLoginId: number;
    private teamIdForRenaming: number;

    searchPlayers(event: Event) {
        const target = event.target as HTMLInputElement;
        const query = target.value;

        fetch(`game/${gameId}/players?name=${query}`)
            .then((resp) => resp.text())
            .then((response) => {
                const list = document.getElementById(target.getAttribute('list'));
                list.innerHTML = response;
            });
    }

    selectPlayer(event: Event) {
        const inputElem = event.target as HTMLInputElement;
        const playerName = inputElem.value;

        if (playerName) {
            const resultItem = document.getElementById(`skr-player-result-item-${playerName}`);

            if (resultItem) {
                this.selectedLoginId = parseInt(resultItem.getAttribute('loginId'));
                document.getElementById('skr-game-add-player-button').removeAttribute('disabled');
                inputElem.blur();
                return;
            }
        }

        this.searchPlayers(event);
    }

    changePlayerFaction(faction, playerId, aiLevel) {
        if (faction) {
            if (aiLevel) {
                fetch(`game/faction?faction=${faction}&playerId=${playerId}`, {
                    method: 'PUT'
                }).then(() => {
                    window.location.reload();
                });
            } else {
                fetch(`game/faction?gameId=${gameId}&faction=${faction}`, {
                    method: 'POST'
                }).then(() => {
                    window.location.reload();
                });
            }
        }
    }

    startGame() {
        new Modal(document.getElementById('skr-game-creation-modal')).show();

        fetch(`start-game?gameId=${gameId}`, {
            method: 'POST'
        }).then(() => {
            window.location.href = `/ingame/game?id=${gameId}`;
        });
    }

    onClickAddTeamButton() {
        this.openTeamModal('add', '');
    }

    onClickRenameTeamButton(teamId: number, teamName: string) {
        this.teamIdForRenaming = teamId;
        this.openTeamModal('rename', teamName);
    }

    openTeamModal(modalType: string, presetInputValue: string) {
        document.getElementById(`${modalType}-duplicate-name-warning`).style.display = 'none';
        const modalElement = document.getElementById(`${modalType}-team-modal`);

        const button = document.getElementById(`${modalType}-team-button`) as HTMLButtonElement;
        button.setAttribute('disabled', 'true');

        const handler = () => {
            const nameElement = document.getElementById(`${modalType}-team-name-input`) as HTMLInputElement;
            nameElement.value = presetInputValue;
            setTimeout(() => {
                nameElement.focus();
            }, 100);
            modalElement.removeEventListener('show.bs.modal', handler);
        };
        modalElement.addEventListener('show.bs.modal', handler);

        new Modal(modalElement).show();
    }

    onChangeTeamNameInput(event: KeyboardEvent, modalType: string) {
        if (event.key === 'Enter') {
            this.changeTeam(modalType);
        } else {
            const button = document.getElementById(`${modalType}-team-button`);
            const disabled = !(event.target as HTMLInputElement).value?.length;
            button.setAttribute('disabled', disabled.toString());
        }
    }

    changeTeam(modalType: string) {
        const disabled = document.getElementById(`${modalType}-team-button`).getAttribute('disabled');

        if (disabled === 'true') {
            return;
        }

        const nameInput = document.getElementById(`${modalType}-team-name-input`) as HTMLInputElement;
        const name = nameInput.value;

        const request: ModifyTeamRequest = {
            gameId,
            name
        };

        if (modalType === 'rename') {
            request.teamId = this.teamIdForRenaming;
        }

        fetch('game/team/', {
            method: modalType === 'add' ? 'PUT' : 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(request)
        })
            .then(resp => resp.text())
            .then((result: string) => {
                if (result === 'true') {
                    window.location.reload();
                } else {
                    document.getElementById(`${modalType}-duplicate-name-warning`).style.display = 'block';
                }
            });
    }

    onClickJoinGame() {
        joinGame(gameId);
    }

    onClickAddPlayer(teamId: number) {
        if (!this.selectedLoginId) {
            return;
        }

        const url = `game/player?gameId=${gameId}&loginId=${this.selectedLoginId}`;
        this.addPlayer(url, teamId, false);
    }

    changePlayerTeam(playerId: number, teamId: number) {
        fetch(`game/team/switch?gameId=${gameId}&playerId=${playerId}&teamId=${teamId}`, {
            method: 'POST'
        })
            .then((res: Response) => res.text())
            .then((errorMessage: String) => {
                if (errorMessage) {
                    window.alert(errorMessage);
                } else {
                    window.location.reload();
                }
            });
    }

    removeTeam(teamId: number) {
        fetch(`game/team/?gameId=${gameId}&teamId=${teamId}`, {
            method: 'DELETE'
        })
            .then((res: Response) => res.text())
            .then((errorMessage: String) => {
                if (errorMessage) {
                    window.alert(errorMessage);
                } else {
                    window.location.reload();
                }
            });
    }

    joinTeam(teamId: number) {
        const url = `game/player?gameId=${gameId}`;
        this.addPlayer(url, teamId, true);
    }

    addPlayer(url: string, teamId: number, self: boolean) {
        if (teamId) {
            url += `&teamId=${teamId}`;
        }

        fetch(url, {
            method: self ? 'POST' : 'PUT'
        })
            .then((res: Response) => res.text())
            .then((errorMessage: String) => {
                if (errorMessage) {
                    window.alert(errorMessage);
                } else {
                    window.location.reload();
                }
            });
    }

    removePlayer(playerId: number) {
        fetch(`game/player?gameId=${gameId}&playerId=${playerId}`, {
            method: 'DELETE'
        }).then(() => {
            window.location.reload();
        });
    }

    leaveGame() {
        fetch(`game/player/self?gameId=${gameId}`, {
            method: 'DELETE'
        }).then(() => {
            document.getElementById('skr-dashboard-open-games-button').click();
        });
    }

    deleteGame() {
        new Modal(document.getElementById('skr-game-deletion-modal')).show();

        fetch('game?id=' + gameId, {
            method: 'DELETE'
        }).then(() => {
            document.getElementById('skr-dashboard-my-games-button').click();
        });
    }

    scrollToTop() {
        setTimeout(() => {
            window.scrollTo({
                top: 0,
                behavior: 'smooth'
            });
        }, 100);
    }

    selectPlayersTab() {
        this.selectTab('#players');
    }

    selectGameFinishedTab() {
        this.selectTab('#game-finished');
    }

    selectGameLostTab() {
        this.selectTab('#game-lost');
    }

    selectGameStatsTab() {
        this.selectTab('#game-stats');
    }

    selectOptionsTab() {
        this.selectTab('#options');
    }

    selectTab(tabId: string) {
        window.history.pushState(null, null, tabId);
    }
}

export const game = new Game();
(window as any).game = game;

document.addEventListener('DOMContentLoaded', () => {
    if (window.location.href.indexOf('/game?id=') < 0) {
        return;
    }

    const parts = window.location.href.split('#');

    if (parts.length === 2) {
        const tabId = parts[1];

        if (tabId.indexOf('=') < 0) {
            new Tab(document.getElementById(`skr-game-tab-${tabId}`)).show();
        }

        game.scrollToTop();
    } else if (window.location.href.indexOf('?id=') < 0) {
        new Tab(document.getElementById('skr-game-tab-option')).show();
        game.scrollToTop();
    }
});
