import {Modal} from "bootstrap";

class AdminGames {
    gameToBeDeleted: number;

    showConfirmDeleteGameDialog(gameId: number) {
        this.gameToBeDeleted = gameId;

        const modal = new Modal(document.getElementById('skr-admin-confirm-delete-game-modal'));
        modal.show();
    }

    deleteGame() {
        const modal = new Modal(document.getElementById('skr-game-deletion-modal'));
        modal.show();

        fetch(`/admin/game?gameId=${this.gameToBeDeleted}`, {
            method: 'DELETE'
        }).then(() => {
            modal.hide();
            window.location.reload();
        });
    }
}

const adminGames = new AdminGames();
(window as any).adminGames = adminGames;
