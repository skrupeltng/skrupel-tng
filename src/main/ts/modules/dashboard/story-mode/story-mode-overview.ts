class StoryModeOverview {
    startMission(factionId: string, missionNumber: string) {
        const request = {
            factionId,
            missionNumber
        };

        fetch('/story-mode/start-mission', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(request)
        })
            .then((response) => response.json())
            .then((gameId: number) => {
                window.location.href = '/ingame/game?id=' + gameId;
            });
    }
}

const storyModeOverview = new StoryModeOverview();
(window as any).storyModeOverview = storyModeOverview;
