import '../common_index';
import '../../notifications';
import 'chart.js/dist/chart.min';
import '../dashboard/feedback-modal';

import './debug';
import './story-mode/story-mode';
import './loading';

import './overview-table';
import './galaxy-map-contextmenu-handler';
import './galaxy-map';
import './control-groups';
import './logbook';

import './fleet/fleet';
import './fleet/fuel';
import './fleet/options';
import './fleet/merge';
import './fleet/task';
import './fleet/projectiles';
import './fleet/ships';
import './fleet/tactics';

import './planet/planet';
import './planet/planetary-defense';
import './planet/mines';
import './planet/factories';
import './planet/orbitalsystems';
import './planet/starbase-construction';

import './ship/ship';
import './ship/task';
import './ship/settings';
import './ship/tactics';
import './ship/transporter';
import './ship/projectiles';
import './ship/scanner';

import './starbase/starbase';
import './starbase/defense';
import './starbase/production';
import './starbase/shipconstruction';
import './starbase/space-folds';
import './starbase/upgrade';
