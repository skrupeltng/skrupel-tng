import {ingame} from '../ingame';
import {orbitalSystems} from '../planet/orbitalsystems';

export class MineralTransformer {
    getMoneyRate() {
        return parseInt((document.getElementById('skr-mineral-transformer-money-rate') as HTMLInputElement).value);
    }

    getSourceRate() {
        return parseInt((document.getElementById('skr-mineral-transformer-source-rate') as HTMLInputElement).value);
    }

    onChangeMoney(sourceMineral: string, targetMineral: string, event: Event) {
        if (this.checkInputBounds(event)) {
            const moneyValue = this.getMoneyValue(sourceMineral, targetMineral);
            const currentSourceValue = this.getSourceValue(sourceMineral, targetMineral);
            let newTargetValue = 0;

            if (moneyValue >= this.getMoneyRate()) {
                const maxTargetByMoney = moneyValue / this.getMoneyRate();
                const maxTargetBySource = currentSourceValue / this.getSourceRate();

                newTargetValue = Math.min(maxTargetByMoney, maxTargetBySource);
            }

            this.getTargetElement(sourceMineral, targetMineral).value = newTargetValue.toString();
        }
    }

    onChangeSource(sourceMineral, targetMineral, event) {
        if (this.checkInputBounds(event)) {
            const sourceValue = this.getSourceValue(sourceMineral, targetMineral);
            const currentMoneyValue = this.getMoneyValue(sourceMineral, targetMineral);
            let newTargetValue = 0;

            if (sourceValue >= this.getSourceRate()) {
                const maxTargetByMoney = currentMoneyValue / this.getMoneyRate();
                const maxTargetBySource = sourceValue / this.getSourceRate();

                newTargetValue = Math.min(maxTargetByMoney, maxTargetBySource);
            }

            this.getTargetElement(sourceMineral, targetMineral).value = newTargetValue.toString();
        }
    }

    onChangeTarget(sourceMineral, targetMineral, event) {
        if (this.checkInputBounds(event)) {
            const targetValue = this.getTargetValue(sourceMineral, targetMineral);

            this.getMoneyElement(sourceMineral, targetMineral).value = (targetValue * this.getMoneyRate()).toString();
            this.getSourceElement(sourceMineral, targetMineral).value = (targetValue * this.getSourceRate()).toString();
        }
    }

    checkInputBounds(event) {
        const currentValue = parseInt(event.target.value);

        if (currentValue > parseInt(event.target.getAttribute('max')) || currentValue < 0) {
            event.target.classList.add('error');
            return false;
        }

        event.target.classList.remove('error');
        return true;
    }

    getMoneyElement(sourceMineral, targetMineral): HTMLInputElement {
        return document.getElementById(`skr-mineral-transform-money-${sourceMineral}-${targetMineral}`) as HTMLInputElement;
    }

    getSourceElement(sourceMineral, targetMineral): HTMLInputElement {
        return document.getElementById(`skr-mineral-transform-source-${sourceMineral}-${targetMineral}`) as HTMLInputElement;
    }

    getTargetElement(sourceMineral, targetMineral): HTMLInputElement {
        return document.getElementById(`skr-mineral-transform-target-${sourceMineral}-${targetMineral}`) as HTMLInputElement;
    }

    getMoneyValue(sourceMineral: string, targetMineral: string): number {
        return parseInt(this.getMoneyElement(sourceMineral, targetMineral).value);
    }

    getSourceValue(sourceMineral: string, targetMineral: string): number {
        return parseInt(this.getSourceElement(sourceMineral, targetMineral).value);
    }

    getTargetValue(sourceMineral: string, targetMineral: string): number {
        return parseInt(this.getTargetElement(sourceMineral, targetMineral).value);
    }

    transform(sourceMineral: string, targetMineral: string) {
        if (this.getMoneyElement(sourceMineral, targetMineral).classList.contains('error')) {
            return;
        }
        if (this.getSourceElement(sourceMineral, targetMineral).classList.contains('error')) {
            return;
        }
        if (this.getTargetElement(sourceMineral, targetMineral).classList.contains('error')) {
            return;
        }

        const moneyValue = this.getMoneyValue(sourceMineral, targetMineral);
        const sourceValue = this.getSourceValue(sourceMineral, targetMineral);

        const orbitalSystemId = parseInt((document.getElementById('skr-ingame-orbitalsystem-id') as HTMLInputElement).value);

        const request = {
            orbitalSystemId,
            sourceMineral,
            targetMineral,
            moneyValue,
            sourceValue
        };

        fetch('orbital-system/transform-minerals', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(request)
        }).then(() => {
            ingame.refreshSelection(() => {
                ingame.updatePlanetMouseOver(ingame.getSelectedId());
                orbitalSystems.showDetails(orbitalSystemId);
            });
        });
    }
}
