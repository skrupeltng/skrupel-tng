import {distanceMeasurement} from './distance-measurement';
import {ingame} from './ingame';

export class GalaxyMapContextMenuHandler {

    private readonly selectionModeToggleButtonId: string;

    private activeTextId: string;
    private inactiveTextId: string;

    private activeText: string;
    private inactiveText: string;

    protected selectionModeActive = false;

    constructor(selectionModeToggleButtonId: string, activeTextId: string, inactiveTextId: string) {
        this.selectionModeToggleButtonId = selectionModeToggleButtonId;
        this.activeTextId = activeTextId;
        this.inactiveTextId = inactiveTextId;
    }

    onLeftClickGalaxyMap(event: MouseEvent) {
        distanceMeasurement.onRightClickGalaxyMap(event);
    }

    onRightClickGalaxyMap(event: MouseEvent) {
    }

    onLeftClickShipInSpace(event: MouseEvent, clickDataString: string, showShipDropdown: boolean) {
        this.handleLeftClickShipInSpace(event, JSON.parse(clickDataString), showShipDropdown);
    }

    handleLeftClickShipInSpace(event: MouseEvent, clickData: ClickData, showShipDropdown: boolean) {
        if (clickData.owned) {
            if (showShipDropdown) {
                ingame.showShipSelection(clickData.x, clickData.y);
            } else {
                ingame.select('ship', clickData.id);
            }
        } else if (clickData.canReceiveMessages) {
            ingame.messageToPlayer(clickData.recipientId, clickData.recipientName);
        }
    }

    onRightClickShipInSpace(event: MouseEvent, clickDataString: string, showShipDropdown: boolean) {
        this.handleRightClickShipInSpace(event, JSON.parse(clickDataString), showShipDropdown);
    }

    handleRightClickShipInSpace(event: MouseEvent, clickData: ClickData, showShipDropdown: boolean) {
    }

    onRightClickPlanet(event: MouseEvent, clickDataString: string) {
        this.handleRightClickPlanet(event, JSON.parse(clickDataString));
    }

    handleRightClickPlanet(event: MouseEvent, clickData: ClickData) {
    }

    onLeftClickPlanet(event, clickDataString) {
        this.handleLeftClickPlanet(event, JSON.parse(clickDataString));
    }

    handleLeftClickPlanet(event: MouseEvent, clickData: ClickData) {
        if (clickData.owned) {
            ingame.select('planet', clickData.id);
        } else if (clickData.canReceiveMessages) {
            ingame.messageToPlayer(clickData.recipientId, clickData.recipientName);
        }
    }

    onRightClickShipOnPlanet(event: MouseEvent, clickDataString: string, showShipDropdown?: boolean) {
        this.handleRightClickShipOnPlanet(event, JSON.parse(clickDataString));
    }

    handleRightClickShipOnPlanet(event: MouseEvent, clickData: ClickData) {
    }

    onLeftClickShipOnPlanet(event: MouseEvent, clickDataString: string, showShipDropdown: boolean) {
        this.handleLeftClickShipOnPlanet(event, JSON.parse(clickDataString), showShipDropdown);
    }

    handleLeftClickShipOnPlanet(event: MouseEvent, clickData: ClickData, showShipDropdown: boolean) {
        if (clickData.owned) {
            if (showShipDropdown) {
                ingame.showShipSelection(clickData.x, clickData.y);
            } else {
                ingame.select('ship', clickData.id);
            }
        } else if (clickData.canReceiveMessages) {
            ingame.messageToPlayer(clickData.recipientId, clickData.recipientName);
        }
    }

    onRightClickMouseOverShipItem(event: MouseEvent, clickDataString: string) {
        this.handleRightClickShipOnPlanet(event, JSON.parse(clickDataString));
    }

    onLeftClickMouseOverShipItem(event: MouseEvent, clickDataString: string, onlyScanned: boolean) {
        this.handleLeftClickMouseOverShipItem(event, JSON.parse(clickDataString), onlyScanned);
    }

    handleLeftClickMouseOverShipItem(event: MouseEvent, clickData: ClickData, onlyScanned: boolean) {
        if (!onlyScanned) {
            ingame.selectShipByMouseOverItem(clickData.id);
        }
    }

    onLeftClickStarbase(event: MouseEvent, starbaseId: number) {
        if (!this.selectionModeActive) {
            ingame.select('starbase', starbaseId);
        }
    }

    onChangeSpeedSelect(event: Event) {
    }

    initData() {
        if (!this.selectionModeToggleButtonId) {
            return;
        }

        const selectionModeToggleButton = document.getElementById(this.selectionModeToggleButtonId);

        if (!selectionModeToggleButton) {
            return;
        }

        selectionModeToggleButton.style.display = ingame.isTouchDevice() ? 'inline-block' : 'none';
    }

    toggleSelectionMode(skipSidebarToggle: boolean) {
        this.selectionModeActive = !this.selectionModeActive;
        this.updateToggleButton(skipSidebarToggle);
    }

    updateToggleButton(skipSidebarToggle: boolean) {
        const selectionModeToggleButton = document.getElementById(this.selectionModeToggleButtonId);

        if (!selectionModeToggleButton) {
            return;
        }

        if (!this.activeText) {
            this.activeText = (document.getElementById(this.activeTextId) as HTMLInputElement).value;
            this.inactiveText = (document.getElementById(this.inactiveTextId) as HTMLInputElement).value;
        }

        if (this.selectionModeActive) {
            if (!skipSidebarToggle) {
                ingame.toggleSidebar();
            }

            selectionModeToggleButton.classList.add('btn-danger');
            selectionModeToggleButton.classList.remove('btn-primary');
            selectionModeToggleButton.textContent = this.activeText;
        } else {
            selectionModeToggleButton.classList.remove('btn-danger');
            selectionModeToggleButton.classList.add('btn-primary');
            selectionModeToggleButton.textContent = this.inactiveText;
        }
    }
}

export interface ClickData {
    id: number;
    name: string;
    x: number;
    y: number;
    owned: boolean;
    visibleByScanners: boolean;
    canReceiveMessages: boolean;
    recipientId: number;
    recipientName: string;
}

export const emptyContextMenuHandler = new GalaxyMapContextMenuHandler('', '', '');
(window as any).contextMenuHandler = emptyContextMenuHandler;
