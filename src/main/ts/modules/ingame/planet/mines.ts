import {ingame} from '../ingame';

class PlanetMines {
    buildMines() {
        const quantity = (document.getElementById('skr-planet-mines-quantity-select') as HTMLInputElement).value;

        fetch(`planet/mines?planetId=${ingame.getSelectedId()}&quantity=${quantity}`, {
            method: 'POST'
        }).then(() => {
            ingame.refreshSelection();
            ingame.updatePlanetMouseOver(ingame.getSelectedId());
        });
    }

    toggleAutoBuildMines() {
        fetch(`planet/automated-mines?planetId=${ingame.getSelectedId()}`, {
            method: 'POST'
        }).then(() => {
            ingame.updatePlanetMouseOver(ingame.getSelectedId());
        });
    }
}

(window as any).planetMines = new PlanetMines();
