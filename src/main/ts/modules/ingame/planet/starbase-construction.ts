import {modals} from '../../../modals';
import {ingame} from '../ingame';

class StarbaseConstruction {
    openStarbaseDetailsModal(starbaseType: StarbaseType) {
        modals.showCopyOfModal(`skr-ingame-starbase-type-modal-${starbaseType}`);
    }

    toNameSelect(starbaseType: StarbaseType) {
        fetch(`planet/starbase-construction-name-select?type=${starbaseType}`)
            .then((response) => response.text())
            .then((resp) => {
                document.getElementById('skr-planet-starbasepconstruction-content').innerHTML = resp;
            });
    }

    construct() {
        const name = (document.getElementById('skr-planet-starbaseconstruction-name-input') as HTMLInputElement).value;

        const errorElem = document.getElementById('skr-planet-starbaseconstruction-name-empty');

        if (!name) {
            errorElem.style.display = 'block';
            return;
        }

        errorElem.style.display = 'none';

        const type = (document.getElementById('skr-planet-starbaseconstruction-type') as HTMLInputElement).value;

        fetch(`planet/starbase?planetId=${ingame.getSelectedId()}&type=${type}&name=${name}`, {
            method: 'POST'
        })
            .then((response) => response.text())
            .then((resp) => {
                document.getElementById('skr-planet-starbasepconstruction-content').innerHTML = resp;
                ingame.refreshSelection();
                ingame.updatePlanetMouseOver(ingame.getSelectedId());
            });
    }
}

enum StarbaseType {
    SHIP_YARD = 'SHIP_YARD',
    BATTLE_STATION = 'BATTLE_STATION',
    STAR_BASE = 'STAR_BASE',
    WAR_BASE = 'WAR_BASE'
}

(window as any).starbaseConstruction = new StarbaseConstruction();
