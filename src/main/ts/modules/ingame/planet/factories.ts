import {ingame} from '../ingame';

class PlanetFactories {
    buildFactories() {
        const quantity = parseInt((document.getElementById('skr-planet-factories-quantity-select') as HTMLInputElement).value);

        fetch(`planet/factories?planetId=${ingame.getSelectedId()}&quantity=${quantity}`, {
            method: 'POST'
        }).then(() => {
            ingame.refreshSelection();
            ingame.updatePlanetMouseOver(ingame.getSelectedId());
        });
    }

    toggleAutoBuildFactories() {
        fetch(`planet/automated-factories?planetId=${ingame.getSelectedId()}`, {
            method: 'POST'
        }).then(() => {
            ingame.updatePlanetMouseOver(ingame.getSelectedId());
        });
    }

    toggleAutoSellSupplies() {
        fetch(`planet/automated-sell-supplies?planetId=${ingame.getSelectedId()}`, {
            method: 'POST'
        }).then(() => {
            ingame.updatePlanetMouseOver(ingame.getSelectedId());
        });
    }

    onSellSuppliesKeyUp(event) {
        if (event.keyCode === 13) {
            this.sellSupplies();
        }
    }

    sellSupplies() {
        const availableSupplies = parseInt((document.getElementById('skr-ingame-supplies') as HTMLInputElement).value);
        const quantity = parseInt((document.getElementById('skr-planet-supplies-quantity-input') as HTMLInputElement).value);

        const errorElem = document.getElementById('skr-ingame-sell-supplies-error');

        if (!quantity || availableSupplies < quantity || quantity <= 0) {
            errorElem.style.display = 'block';
            return;
        }

        errorElem.style.display = 'none';

        fetch(`planet/sell-supplies?planetId=${ingame.getSelectedId()}&quantity=${quantity}`, {
            method: 'POST'
        }).then(() => {
            ingame.refreshSelection();
            ingame.updatePlanetMouseOver(ingame.getSelectedId());
        });
    }
}

(window as any).factories = new PlanetFactories();
