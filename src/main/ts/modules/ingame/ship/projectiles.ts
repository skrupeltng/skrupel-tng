import {ingame} from '../ingame';

class ShipProjectiles {
    toggleAutoBuildProjectiles() {
        fetch(`ship/automated-projectiles?shipId=${ingame.getSelectedId()}`, {
            method: 'POST'
        }).then(() => {
            ingame.updatePlanetMouseOver(ingame.getSelectedId());
        });
    }

    buildProjectiles() {
        const quantity = (document.getElementById('skr-ship-projectiles-quantity-select') as HTMLInputElement).value;

        fetch(`ship/projectiles?shipId=${ingame.getSelectedId()}&quantity=${quantity}`, {
            method: 'POST'
        }).then(() => {
            ingame.refreshSelection();
            ingame.updateShipMouseOver(ingame.getSelectedId());
        });
    }

    autoBuildProjectiles() {
        const planetIdElem = document.getElementById('skr-ship-projectiles-planet-id') as HTMLInputElement;
        const planetId = !!planetIdElem ? parseInt(planetIdElem.value) : undefined;

        fetch(`ship/projectiles-auto?shipId=${ingame.getSelectedId()}`, {
            method: 'POST'
        }).then(() => {
            ingame.refreshSelection();
            ingame.updateShipMouseOver(ingame.getSelectedId());
            ingame.updateShipStatsDetails(ingame.getSelectedId());

            if (!!planetId) {
                ingame.updatePlanetMouseOver(planetId);
            }
        });
    }
}

(window as any).shipProjectiles = new ShipProjectiles();
