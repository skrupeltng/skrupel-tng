import {IngamePage} from '../ingame-page';
import {shipTransporter} from './transporter';
import {shipNavigation} from './navigation';
import {route} from './route';
import {shipScanner} from './scanner';

export const ship = new IngamePage('ship');
ship.addPageForInit('orders', shipNavigation);
ship.addPageForInit('transporter', shipTransporter);
ship.addPageForInit('route', route);
ship.addPageForInit('scanner', shipScanner);
(window as any).ship = ship;
