import {ClickData, GalaxyMapContextMenuHandler} from '../galaxy-map-contextmenu-handler';
import {ingame} from '../ingame';

class ShipScanner extends GalaxyMapContextMenuHandler {
    constructor() {
        super('skr-ingame-scanner-toggle-selection-mode', 'skr-ship-scanner-selection-mode-active-text', 'skr-ship-scanner-selection-mode-inactive-text');
    }

    onMouseEnterShipDetails(x: number, y: number) {
        ingame.showPingAtPosition(x, y);
    }

    onMouseLeaveShipDetails() {
        ingame.resetPing();
    }

    handleRightClickShipInSpace(event: MouseEvent, clickData: ClickData, showShipDropdown: boolean) {
        event.preventDefault();
        event.stopPropagation();

        if (!clickData.owned && clickData.visibleByScanners) {
            if (showShipDropdown) {
                ingame.keepShipsMouseOver = true;
                return false;
            }

            this.showShipDetails(clickData.id);
            return false;
        }
    }

    handleLeftClickShipInSpace(event: MouseEvent, clickData: ClickData, showShipDropdown: boolean) {
        if (this.selectionModeActive) {
            if (showShipDropdown) {
                ingame.showGalaxyMapDetails('ships', clickData.x + '_' + clickData.y);
            } else {
                this.handleRightClickShipInSpace(event, clickData, showShipDropdown);
            }
        } else {
            super.handleLeftClickShipInSpace(event, clickData, showShipDropdown);
        }
    }

    handleRightClickShipOnPlanet(event: MouseEvent, clickData: ClickData, showShipDropdown?: boolean) {
        event.preventDefault();
        event.stopPropagation();

        if (!clickData.owned && clickData.visibleByScanners) {
            if (showShipDropdown) {
                ingame.keepShipsMouseOver = true;
                return false;
            }

            this.showShipDetails(clickData.id);
            return false;
        }
    }

    handleLeftClickShipOnPlanet(event: MouseEvent, clickData: ClickData, showShipDropdown: boolean) {
        if (this.selectionModeActive) {
            if (clickData.visibleByScanners && showShipDropdown) {
                ingame.showGalaxyMapDetails('ships', clickData.x + '_' + clickData.y);
            } else {
                this.handleRightClickShipOnPlanet(event, clickData, showShipDropdown);
            }
        } else {
            super.handleLeftClickShipOnPlanet(event, clickData, showShipDropdown);
        }
    }

    handleRightClickMouseOverShipItem(event: MouseEvent, clickData: ClickData) {
        event.preventDefault();
        event.stopPropagation();

        ingame.hideAllGalaxyMapMouseOvers();
        ingame.keepShipsMouseOver = false;

        this.showShipDetails(clickData.id);

        return false;
    }

    showShipDetails(shipId: number) {
        fetch(`ship/scanner/ship?shipId=${ingame.getSelectedId()}&scannedShipId=${shipId}`)
            .then((response: Response) => response.text())
            .then((resp: string) => {
                this.updateShipDetails(resp);
                ingame.resetPing();
            });
    }

    updateShipDetails(content: string) {
        document.getElementById('skr-game-ship-scanner-wrapper').innerHTML = content;
    }

    showShipDetailsByOrbitalSystem(shipId: number, orbitalSystemId: number) {
        fetch(`orbital-system/scanner/ship?orbitalSystemId=${orbitalSystemId}&scannedShipId=${shipId}`)
            .then((response: Response) => response.text())
            .then((resp: string) => {
                this.updateShipDetails(resp);
                ingame.resetPing();
            });
    }
}

export const shipScanner = new ShipScanner();
(window as any).shipScanner = shipScanner;
