import {ingame} from '../ingame';

class ShipTask {
    changeTask() {
        const selectedInput = document.querySelector('#skr-ingame-ship-task-form input[name=task]:checked');
        const taskType = selectedInput.getAttribute('tasktype');

        let activeAbilityType = null;
        const valueElem = document.getElementById(`${taskType}_value`) as HTMLInputElement;
        let taskValue = valueElem?.value;

        if (taskType === 'ACTIVE_ABILITY') {
            activeAbilityType = selectedInput.getAttribute('id');
            const valueElem = document.getElementById(`${activeAbilityType}_value`) as HTMLInputElement;

            if (valueElem?.type === 'checkbox') {
                taskValue = valueElem.checked.toString();
            } else {
                taskValue = valueElem?.value;
            }
        }

        const request = new ShipTaskRequest(ingame.getSelectedId(), activeAbilityType, taskType, taskValue);

        if ((document.getElementById('escort') as HTMLInputElement)?.checked) {
            request.escortTargetId = parseInt((document.getElementById('skr-ingame-escort-target-select') as HTMLInputElement).value);
            request.escortTargetSpeed = parseInt((document.getElementById('skr-ship-courseselect-speed-select-escort') as HTMLInputElement).value);
        }

        fetch('ship/task', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(request)
        }).then(() => {
            ingame.refreshSelection(() => ingame.updateShipTravelData());
        });
    }
}

class ShipTaskRequest {
    shipId: number;
    activeAbilityType: string;
    taskType: string;
    taskValue: string;
    escortTargetId: number;
    escortTargetSpeed: number;

    constructor(shipId: number, activeAbilityType: string, taskType: string, taskValue: string) {
        this.shipId = shipId;
        this.activeAbilityType = activeAbilityType;
        this.taskType = taskType;
        this.taskValue = taskValue;
    }
}

export const shipTask = new ShipTask();
(window as any).shipTask = shipTask;
