import {ingame} from "../ingame";

class Settings {
    onNameKeyUp(event: KeyboardEvent) {
        if (event.key === 'Enter') {
            this.save();
        }
    }

    save() {
        const name = (document.getElementById('skr-ingame-ship-name-input') as HTMLInputElement).value;

        if (!name || name.trim() === '') {
            const message = (document.getElementById('skr-ingame-ship-options-empty-message') as HTMLInputElement).value;
            window.alert(message);
            return;
        }

        const request = new ShipOptionsRequest(name);

        const alliedPlayersSelectElem = document.getElementById('skr-ingame-allied-players-select') as HTMLInputElement;

        if (alliedPlayersSelectElem) {
            const newOwnerIdValue = alliedPlayersSelectElem.value;

            if (newOwnerIdValue) {
                request.newOwnerId = parseInt(newOwnerIdValue);
            }
        }

        fetch(`ship/options?shipId=${ingame.getSelectedId()}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(request)
        })
            .then((response: Response) => response.text())
            .then((resp: string) => {
                if (request.newOwnerId) {
                    window.alert((document.getElementById('skr-ingame-ship-owner-changed-message') as HTMLInputElement).value);
                    window.location.href = '/ingame/game?id=' + new URLSearchParams(window.location.search).get('id');
                } else {
                    document.getElementById('skr-ingame-ship-options-content').innerHTML = resp;
                    ingame.updateShipMouseOver(ingame.getSelectedId());
                    ingame.updateShipStatsDetails(ingame.getSelectedId());
                }
            });
    }
}

class ShipOptionsRequest {
    name: string;
    newOwnerId: number;

    constructor(name: string) {
        this.name = name;
    }
}

const settings = new Settings();
(window as any).settings = settings;
