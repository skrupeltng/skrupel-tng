import {ingame} from '../ingame';

class StarbaseUpgrade {
    onUpgradeSelectChanged() {
        let total = 0;

        const hull = parseInt(document.querySelector('#skr-starbase-upgrade-hull-select option:checked')?.getAttribute('name'));
        const propulsion = parseInt(document.querySelector('#skr-starbase-upgrade-propulsion-select option:checked')?.getAttribute('name'));
        const energy = parseInt(document.querySelector('#skr-starbase-upgrade-energy-select option:checked')?.getAttribute('name'));
        const projectile = parseInt(document.querySelector('#skr-starbase-upgrade-projectile-select option:checked')?.getAttribute('name'));

        if (hull > 0) {
            total += hull;
        }
        if (propulsion > 0) {
            total += propulsion;
        }
        if (energy > 0) {
            total += energy;
        }
        if (projectile > 0) {
            total += projectile;
        }

        const costsWrapperElem = document.getElementById('skr-ingame-starbase-upgrade-costs-wrapper');

        if (total > 0) {
            const availableMoney = parseInt((document.getElementById('skr-ingame-starbase-upgrade-money') as HTMLInputElement).value);
            const upgradeButton = document.getElementById('skr-ingame-starbase-performupgrade-button');

            if (availableMoney < total) {
                costsWrapperElem.classList.add('text-danger');
                upgradeButton.classList.add('disabled');
                upgradeButton.setAttribute('disabled', 'true');
            } else {
                costsWrapperElem.classList.remove('text-danger');
                upgradeButton.classList.remove('disabled');
                upgradeButton.setAttribute('disabled', 'false');
            }

            const costsElem = document.getElementById('skr-ingame-starbase-upgrade-costs');
            costsElem.innerText = total + '';
            costsElem.dispatchEvent(new Event('change'));
            costsWrapperElem.style.display = 'block';
        } else {
            costsWrapperElem.style.display = 'none';
        }
    }

    performUpgrade() {
        const request = {
            hullLevel: parseInt((document.getElementById('skr-starbase-upgrade-hull-select') as HTMLInputElement)?.value),
            propulsionLevel: parseInt((document.getElementById('skr-starbase-upgrade-propulsion-select') as HTMLInputElement)?.value),
            energyLevel: parseInt((document.getElementById('skr-starbase-upgrade-energy-select') as HTMLInputElement)?.value),
            projectileLevel: parseInt((document.getElementById('skr-starbase-upgrade-projectile-select') as HTMLInputElement)?.value)
        };

        const planetId = parseInt((document.getElementById('skr-ingame-starbase-upgrade-planet-id') as HTMLInputElement).value);

        fetch(`starbase/upgrade?starbaseId=${ingame.getSelectedId()}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(request)
        })
            .then(resp => resp.text())
            .then((resp) => {
                document.getElementById('skr-ingame-starbase-upgrade-content').innerHTML = resp;
                ingame.updateStarbaseMouseOver(ingame.getSelectedId());
                ingame.updatePlanetMouseOver(planetId);
            });
    }
}

export const starbaseUpgrade = new StarbaseUpgrade();
(window as any).starbaseUpgrade = starbaseUpgrade;
