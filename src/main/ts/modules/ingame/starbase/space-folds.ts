import {ingame} from '../ingame';
import {RangeLine} from '../range-line';
import {ClickData} from "../galaxy-map-contextmenu-handler";

class SpaceFolds extends RangeLine {
    constructor() {
        super('skr-ingame-space-folds-toggle-selection-mode', 'skr-ship-space-folds-selection-mode-active-text', 'skr-ship-space-folds-selection-mode-inactive-text');
    }

    onLeftInputChanged(event: Event) {
        this.leftInputChanged(event);
    }

    onRightInputChanged(event: Event) {
        this.rightInputChanged(event);
    }

    onLeftMaxButtonClicked(event: MouseEvent) {
        this.leftMaxButtonClicked(event);
    }

    onRightMaxButtonClicked(event: MouseEvent) {
        this.rightMaxButtonClicked(event);
    }

    handleRightClickPlanet(event, clickData) {
        event.preventDefault();
        event.stopPropagation();

        this.updateSelectedItem('planet_' + clickData.id);
        return false;
    }

    handleLeftClickPlanet(event: MouseEvent, clickData: ClickData) {
        if (this.selectionModeActive) {
            this.handleRightClickPlanet(event, clickData);
        } else {
            super.handleLeftClickPlanet(event, clickData);
        }
    }

    handleRightClickShipInSpace(event: MouseEvent, clickData: ClickData, showShipDropdown: boolean) {
        event.preventDefault();
        event.stopPropagation();

        if (!clickData.owned) {
            return;
        }

        if (showShipDropdown) {
            ingame.keepShipsMouseOver = true;
            return false;
        }

        this.updateSelectedItem('ship_' + clickData.id);
        return false;
    }

    handleLeftClickShipInSpace(event: MouseEvent, clickData: ClickData, showShipDropdown: boolean) {
        if (this.selectionModeActive) {
            if (showShipDropdown) {
                ingame.showGalaxyMapDetails('ships', clickData.x + '_' + clickData.y);
            } else {
                this.handleRightClickShipInSpace(event, clickData, showShipDropdown);
            }
        } else {
            super.handleLeftClickShipInSpace(event, clickData, showShipDropdown);
        }
    }

    handleRightClickShipOnPlanet(event: MouseEvent, clickData: ClickData, showShipDropdown?: boolean) {
        event.preventDefault();
        event.stopPropagation();

        if (!clickData.owned) {
            return;
        }

        if (showShipDropdown) {
            ingame.keepShipsMouseOver = true;
            return false;
        }

        ingame.hideAllGalaxyMapMouseOvers();
        ingame.keepShipsMouseOver = false;

        this.updateSelectedItem('ship_' + clickData.id);
        return false;
    }

    handleLeftClickShipOnPlanet(event: MouseEvent, clickData: ClickData, showShipDropdown: boolean) {
        if (this.selectionModeActive) {
            if (showShipDropdown) {
                ingame.showGalaxyMapDetails('ships', clickData.x + '_' + clickData.y);
            } else {
                this.handleRightClickShipOnPlanet(event, clickData, showShipDropdown);
            }
        } else {
            super.handleLeftClickShipOnPlanet(event, clickData, showShipDropdown);
        }
    }

    handleRightClickMouseOverShipItem(event: MouseEvent, clickData: ClickData) {
        event.preventDefault();
        event.stopPropagation();

        if (!clickData.owned) {
            return;
        }

        ingame.hideAllGalaxyMapMouseOvers();
        ingame.keepShipsMouseOver = false;

        this.updateSelectedItem('ship_' + clickData.id);
        return false;
    }

    handleLeftClickMouseOverShipItem(event: MouseEvent, clickData: ClickData) {
        if (this.selectionModeActive) {
            this.handleRightClickMouseOverShipItem(event, clickData);
        }
    }

    updateInputFields(fieldName: string, max: number, value: number) {
        super.updateInputFields(fieldName, max, value);

        const costs = this.getCosts(this.createRequest());
        document.getElementById('skr-ingame-spacefolds-money-costs-label').innerText = costs + '';

        if (costs > this.getAvailableMoney()) {
            document.getElementById('skr-ingame-spacefolds-money-costs-wrapper').classList.add('text-danger');
            document.getElementById('skr-ingame-starbase-init-space-fold-button').setAttribute('disabled', 'true');
        } else {
            document.getElementById('skr-ingame-spacefolds-money-costs-wrapper').classList.remove('text-danger');
            document.getElementById('skr-ingame-starbase-init-space-fold-button').setAttribute('disabled', 'false');
        }
    }

    onSelectItem(event: Event) {
        const id = (event.target as HTMLInputElement).value;
        const elem = document.getElementById(id);

        if (elem) {
            const image = elem.getAttribute('image');
            document.getElementById('skr-ingame-spacefolds-target-image').setAttribute('src', image);
        }
    }

    updateSelectedItem(id: string) {
        const elem = (document.getElementById('skr-ingame-spacefolds-target-select') as HTMLSelectElement);
        elem.value = id;
        elem.dispatchEvent(new Event('change'));
    }

    initSpaceFold() {
        const request = this.createRequest();
        const availableMoney = this.getAvailableMoney();
        const costs = this.getCosts(request);

        if (costs > availableMoney - request.money) {
            let msg = (document.getElementById('skr-ingame-spacefolds-costs-message') as HTMLInputElement).value;
            msg = msg.replace('{0}', costs.toString());
            window.alert(msg);
        } else {
            const targetValue = (document.getElementById('skr-ingame-spacefolds-target-select') as HTMLInputElement).value;
            const parts = targetValue.split('_');

            if (parts.length !== 2) {
                const msg = (document.getElementById('skr-ingame-spacefolds-select-target-message') as HTMLInputElement).value;
                window.alert(msg);
            } else {
                const targetId = parseInt(parts[1]);

                if (parts[0] === 'ship') {
                    request.shipId = targetId;
                } else {
                    request.planetId = targetId;
                }

                const planetId = parseInt((document.getElementById('skr-ingame-spacefolds-planet-id') as HTMLInputElement).value);

                fetch(`starbase/space-folds?starbaseId=${ingame.getSelectedId()}`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(request)
                })
                    .then((response: Response) => response.text())
                    .then((resp: string) => {
                        ingame.updatePlanetMouseOver(planetId);

                        ingame.updateStarbaseStatsDetails(ingame.getSelectedId());
                        document.getElementById('skr-ingame-space-folds-content').innerHTML = resp;
                    });
            }
        }
    }

    getAvailableMoney(): number {
        return parseInt((document.getElementById('skr-ingame-spacefolds-available-money') as HTMLInputElement).value);
    }

    createRequest(): SpaceFoldRequest {
        return new SpaceFoldRequest(
            parseInt((document.getElementById('slider-money') as HTMLInputElement).value),
            parseInt((document.getElementById('slider-supplies') as HTMLInputElement).value),
            parseInt((document.getElementById('slider-fuel') as HTMLInputElement).value),
            parseInt((document.getElementById('slider-mineral1') as HTMLInputElement).value),
            parseInt((document.getElementById('slider-mineral2') as HTMLInputElement).value),
            parseInt((document.getElementById('slider-mineral3') as HTMLInputElement).value)
        );
    }

    getCosts(request: SpaceFoldRequest): number {
        return (request.supplies + request.fuel + request.mineral1 + request.mineral2 + request.mineral3) * 8 + 75;
    }
}

class SpaceFoldRequest {
    shipId: number;
    planetId: number;

    money: number;
    supplies: number;
    fuel: number;
    mineral1: number;
    mineral2: number;
    mineral3: number;

    constructor(money: number, supplies: number, fuel: number, mineral1: number, mineral2: number, mineral3: number) {
        this.money = money;
        this.supplies = supplies;
        this.fuel = fuel;
        this.mineral1 = mineral1;
        this.mineral2 = mineral2;
        this.mineral3 = mineral3;
    }
}

export const spaceFolds = new SpaceFolds();
(window as any).spaceFolds = spaceFolds;
