import {ingame} from '../ingame';

class FleetProjectiles {
    buildProjectiles() {
        const fleetId = ingame.getSelectedId();

        fetch(`fleet/build-projectiles?fleetId=${fleetId}`, {
            method: 'POST'
        })
            .then((response: Response) => response.json())
            .then((planetIds: number[]) => {
                ingame.refreshSelection();

                planetIds.forEach((planetId: number) => {
                    ingame.updatePlanetMouseOver(planetId);
                });
            });
    }
}

const fleetProjectiles = new FleetProjectiles();
(window as any).fleetProjectiles = fleetProjectiles;
