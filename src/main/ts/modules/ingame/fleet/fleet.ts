import {IngamePage} from '../ingame-page';
import {ingame, gameId} from '../ingame';
import {fleetsTable} from '../overview-table';
import {shipNavigation} from '../ship/navigation';
import {Modal} from "bootstrap";
import {ShipTravelData} from "../api-responses/ship-travel-data.interface";

export const fleet = new IngamePage('fleet');
fleet.addPageForInit('orders', shipNavigation);
(window as any).fleet = fleet;

class FleetDetails {
    private shipIdForFleetCreation: number;
    private forShipConstruction: boolean;

    updateStats(callback?: () => any) {
        fetch(`fleet/stats-details?fleetId=${ingame.getSelectedId()}`)
            .then((response: Response) => response.text())
            .then((resp: string) => {
                document.getElementById('skr-ingame-fleet-stats-details').innerHTML = resp;

                if (callback) {
                    callback();
                }
            });
    }

    updateTravelData() {
        const shipIds = (document.getElementById('skr-ingame-fleet-ship-ids') as HTMLInputElement).value;

        if (shipIds) {
            const shipIdsArray = shipIds.split(',');

            shipIdsArray.forEach((shipId: string) => {
                const shipIdNumber = parseInt(shipId);
                ingame.updateShipTravelData(shipIdNumber);
                ingame.updateShipMouseOver(shipIdNumber);
            });
        }
    }

    openCreateFleetModal(shipId?: number) {
        this.shipIdForFleetCreation = shipId;

        const modalElem = document.getElementById('skr-ingame-create-fleet-modal');

        const listener = () => {
            document.getElementById('skr-ingame-new-fleet-name-input').focus();
            modalElem.removeEventListener('shown.bs.modal', listener);
        };

        modalElem.addEventListener('shown.bs.modal', listener);

        new Modal(modalElem).show();
    }

    openCreateFleetModalForShipConstruction() {
        this.forShipConstruction = true;
        this.openCreateFleetModal();
    }

    saveNewFleet() {
        const name = (document.getElementById('skr-ingame-new-fleet-name-input') as HTMLInputElement).value;

        let url = `fleet?name=${name}&gameId=${gameId}`;

        if (this.shipIdForFleetCreation) {
            url += `&shipId=${this.shipIdForFleetCreation}`;
        }

        fetch(url, {
            method: 'PUT'
        })
            .then((response: Response) => response.json())
            .then((fleetId: string) => {
                fleetsTable.updateTableContent();

                if (this.shipIdForFleetCreation) {
                    ingame.updateShipMouseOver(this.shipIdForFleetCreation);
                    ingame.updateShipStatsDetails(this.shipIdForFleetCreation);
                    ingame.updateShipTravelData(this.shipIdForFleetCreation);

                    if (window.location.hash.indexOf('ship=') >= 0) {
                        ingame.refreshSelection();
                    } else if (window.location.hash.indexOf('ship-selection') >= 0) {
                        document.querySelectorAll('.skr-ship-fleet-dropdown-list-wrapper').forEach((item) => {
                            const itemShipId = item.getAttribute('shipId');

                            fetch(`ship/fleet-dropdown-list?shipId=${itemShipId}`)
                                .then((response) => response.text())
                                .then((resp) => {
                                    item.innerHTML = resp;
                                });
                        });
                    }
                } else if (this.forShipConstruction) {
                    const fleetOption = document.createElement('option');
                    fleetOption.value = fleetId;
                    fleetOption.innerHTML = name;

                    let select = document.getElementById('skr-starbase-add-directly-to-fleet') as HTMLSelectElement;

                    if (!select) {
                        select = document.createElement('select') as HTMLSelectElement;
                        select.id = 'skr-starbase-add-directly-to-fleet';
                        select.className = 'form-select form-select-sm me-2';
                        document.getElementById('skr-starbase-add-directly-to-fleet-wrapper').prepend(select);
                    }

                    select.appendChild(fleetOption);
                    select.value = fleetId;
                }

                (document.getElementById('skr-ingame-new-fleet-name-input') as HTMLInputElement).value = '';
                this.forShipConstruction = false;
                this.shipIdForFleetCreation = undefined;
            });
    }

    addShipToFleet(fleetId: number, shipId: number) {
        fetch(`fleet/ship?shipId=${shipId}&fleetId=${fleetId}`, {
            method: 'POST'
        }).then(() => {
            fleetsTable.updateTableContent();
            ingame.updateShipMouseOver(shipId);
            ingame.updateShipStatsDetails(shipId);
            ingame.updateShipTravelData(shipId);

            if (window.location.hash.indexOf('ship=') >= 0) {
                ingame.refreshSelection();
            }
        });
    }

    removeShipFromFleet(shipId: number) {
        fetch(`fleet/ship?shipId=${shipId}`, {
            method: 'DELETE'
        }).then(() => {
            fleetsTable.updateTableContent();
            ingame.updateShipMouseOver(shipId);
            ingame.updateShipStatsDetails(shipId);
            ingame.updateShipTravelData(shipId);

            if (window.location.hash.indexOf('ship=') >= 0) {
                ingame.refreshSelection();
            } else if (window.location.hash.indexOf('fleet=') >= 0) {
                ingame.refreshSelection();
            }
        });
    }

    assignNewFleetLeader(fleetId: number, shipId: number) {
        fetch(`fleet/leader?shipId=${shipId}&fleetId=${fleetId}`, {
            method: 'POST'
        })
            .then((response: Response) => response.json())
            .then((travelData: ShipTravelData[]) => {
                travelData.forEach((entry: ShipTravelData) => {
                    const shipId = entry.shipId;

                    ingame.drawShipTravelLineWithParams(
                        shipId,
                        entry.destinationX,
                        entry.destinationY,
                        entry.x,
                        entry.y,
                        entry.color,
                        '.skr-game-ship-travel-line-wrapper',
                        'dotted'
                    );
                    ingame.updateShipStatsDetails(shipId);
                    ingame.updateShipMouseOver(shipId);
                });
            });
    }
}

export const fleetDetails = new FleetDetails();
(window as any).fleetDetails = fleetDetails;
