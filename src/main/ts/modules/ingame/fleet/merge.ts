import {ingame} from '../ingame';
import {fleetsTable} from '../overview-table';
import {fleetDetails} from './fleet';
import {controlGroups} from '../control-groups';

class FleetMerge {
    mergeFleets() {
        const fleetId = ingame.getSelectedId();
        const otherFleetId = (document.getElementById('skr-ingame-fleet-merge-select') as HTMLInputElement).value;

        fetch(`fleet/merge?fleetId=${fleetId}&otherFleetId=${otherFleetId}`, {
            method: 'POST'
        })
            .then((response: Response) => response.text())
            .then((resp: string) => {
                document.getElementById('skr-game-options-content').innerHTML = resp;

                fleetDetails.updateStats(() => {
                    fleetDetails.updateTravelData();
                });

                fleetsTable.updateTableContent();
                controlGroups.updateControlGroupsPanel();
            });
    }
}

export const fleetMerge = new FleetMerge();
(window as any).fleetMerge = fleetMerge;
