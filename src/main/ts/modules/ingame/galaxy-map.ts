import {shipNavigation} from './ship/navigation';
import {route} from './ship/route';
import {spaceFolds} from './starbase/space-folds';
import {shipScanner} from './ship/scanner';
import {emptyContextMenuHandler} from './galaxy-map-contextmenu-handler';
import {shipTransporter} from './ship/transporter';
import {emptyRangeLine} from './range-line';

class GalaxyMap {
    pageChanged() {
        const hash = window.location.hash;

        let orders = false;
        (window as any).contextMenuHandler = emptyContextMenuHandler;
        (window as any).rangeLine = emptyRangeLine;

        if (hash.indexOf(';route') >= 0) {
            (window as any).contextMenuHandler = route;
        } else if (hash.indexOf(';space-folds') >= 0) {
            (window as any).contextMenuHandler = spaceFolds;
            (window as any).rangeLine = spaceFolds;
        } else if (hash.indexOf(';scanner') >= 0) {
            (window as any).contextMenuHandler = shipScanner;
        } else if (hash.indexOf('ship=') >= 0 || hash.indexOf('fleet=') >= 0) {
            if (hash.indexOf(';transporter') >= 0 || !document.getElementById('skr-ingame-selection-panel-tab-orders')) {
                (window as any).rangeLine = shipTransporter;
            }

            (window as any).contextMenuHandler = shipNavigation;
            orders = hash.indexOf(';orders') >= 0;
        }

        if (!orders) {
            shipNavigation.checkFuelTooLow();
        }
    }
}

export const galaxyMap = new GalaxyMap();
(window as any).galaxyMap = galaxyMap;
