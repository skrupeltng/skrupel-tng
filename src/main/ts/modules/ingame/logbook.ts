import {ingame} from "./ingame";

class Logbook {
    saveLogbook() {
        const logbook = (document.getElementById('skr-ingame-logbook-input') as HTMLInputElement).value;

        const prefix = ingame.getSelectedPage();
        const selectedId = ingame.getSelectedId();

        fetch(`${prefix}/logbook?${prefix}Id=${selectedId}&logbook=${logbook}`, {
            method: 'POST'
        })
            .then((response: Response) => response.text())
            .then((resp: string) => {
                document.getElementById('skr-game-logbook-content').innerHTML = resp;

                if (prefix === 'planet') {
                    ingame.updatePlanetMouseOver(selectedId);
                } else if (prefix === 'ship') {
                    ingame.updateShipMouseOver(selectedId);
                } else {
                    ingame.updateStarbaseMouseOver(selectedId);
                }
            });
    }
}

export const logbook = new Logbook();
(window as any).logbook = logbook;
