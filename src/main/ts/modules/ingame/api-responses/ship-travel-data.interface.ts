export interface ShipTravelData {

    shipId: number;
    x: number;
    y: number;
    color: string;
    destinationX: number;
    destinationY: number;
}