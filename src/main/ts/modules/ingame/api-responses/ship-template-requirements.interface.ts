export interface ShipTemplateRequirements {
    propulsionSystems: number;
    energyWeapons: number;
    projectileWeapons: number;
}
