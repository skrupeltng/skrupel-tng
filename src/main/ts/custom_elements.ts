const exceptions = ['btn-close', 'skr-ingame-help-show-modal-button', 'right-max-button', 'left-max-button', 'fas'];

class SkrButton extends HTMLElement {

    private readonly spaceMode: any;

    constructor() {
        super();

        this.spaceMode = document.querySelector('body.space_mode');

        if (this.spaceMode) {
            const shadowRoot = this.attachShadow({mode: 'open'});
            shadowRoot.innerHTML = `
            <style>
                .arwes-frame-underline__shapes {
                    display: inline-block;
                    position: absolute;
                    inset: 0px;
                    overflow: hidden;
                }

                .arwes-frame-underline__shape1 {
                    position: absolute;
                    overflow: hidden;
                    left: 0px;
                    right: 0px;
                    bottom: 20px;
                    height: 100%;
                }

                .borders .arwes-frame__bg {
                    background-color: #004747;
                    transition: opacity 150ms ease-out 0s;
                }

                .borders .arwes-frame-underline__shape2 {
                    position: absolute;
                    overflow: hidden;
                    left: 0px;
                    right: 20px;
                    bottom: 0px;
                    height: 20px;
                }

                .borders .arwes-frame-underline__shape3 {
                    position: absolute;
                    overflow: hidden;
                    right: 0px;
                    bottom: 0px;
                    width: 20px;
                    height: 20px;
                }

                .borders .arwes-frame-underline__line-1 {
                    position: absolute;
                    height: 2px;
                    background-color: rgb(0, 248, 248);
                    box-shadow: rgb(0, 248, 248) 0px 0px 2px;
                    transition: background-color 150ms ease-out 0s, box-shadow 150ms ease-out 0s;
                    left: 0px;
                    bottom: 0px;
                    width: calc(100% - 20px);
                }

                .borders .arwes-frame-underline__line-2 {
                    position: absolute;
                    height: 2px;
                    background-color: rgb(0, 248, 248);
                    box-shadow: rgb(0, 248, 248) 0px 0px 2px;
                    transition: background-color 150ms ease-out 0s, box-shadow 150ms ease-out 0s;
                    right: 0px;
                    bottom: 0px;
                    width: 20px;
                    transform: skewY(135deg);
                    transform-origin: left center;
                }

                .arwes-frame-underline__content {
                    padding: 3px;
                }

                .borders .arwes-frame-underline__content {
                    padding: 6px 20px 6px 8px;
                    display: inline-block;
                    opacity: .99;
                }

                .borders .arwes-frame-underline__shape4 {
                    position: absolute;
                    overflow: hidden;
                    left: 0px;
                    top: 0px;
                    width: 100%;
                    height: 100%;
                    transform: skewY(135deg);
                    transform-origin: left center;
                }

                .wrapper {
                    display: inline-block;
                    font-size: .9em;
                }

                .wrapper:not(.borders) .arwes-frame-underline__shapes {
                    display: none;
                }

                :host(:hover) .borders .arwes-frame-underline__line-1,
                :host(:hover) .borders .arwes-frame-underline__line-2 {
                    background-color: #b5fdf9;
                    box-shadow: #b5fdf9 0px 0px 5px;
                }

                :host(:hover) .borders .arwes-frame__bg {
                    background-color: #007f80;
                }
            </style>

            <div class="wrapper borders arwes-frame-underline__container">
                <div class="arwes-frame-underline__shapes">
                    <div class="arwes-frame-underline__shape1 arwes-frame__bg"></div>
                    <div class="arwes-frame-underline__shape2 arwes-frame__bg"></div>
                    <div class="arwes-frame-underline__shape3">
                        <div class="arwes-frame-underline__shape4 arwes-frame__bg"></div>
                    </div>
                </div>
                <div class="arwes-frame-underline__line arwes-frame-underline__line-1"></div>
                <div class="arwes-frame-underline__line arwes-frame-underline__line-2"></div>
                <div class="arwes-frame-underline__content">
                    <slot />
                </div>
            </div>`;
        }
    }

    connectedCallback() {
        // Mimic submit button behaviour
        this.addEventListener('click', () => {
            const type = this.getAttribute('type');

            if (type === 'submit') {
                const formElementId = this.getAttribute('form');

                let form;
                if (formElementId) {
                    form = document.getElementById(formElementId);
                } else {
                    form = document.querySelector('form');
                }

                if (form) {
                    form.submit();
                }
            }
        });

        if (this.spaceMode && this.removeBorder()) {
            this.shadowRoot.querySelector('.wrapper').classList.remove('borders');
        }
    }

    removeBorder() {
        const navbar = document.querySelector('.navbar');
        const routeContent = document.querySelector('#skr-ingame-route-content');

        if (navbar?.contains(this) || routeContent?.contains(this) || this.parentElement.classList.contains('skr-table-search-buttons')) {
            return true;
        }

        for (const exception of exceptions) {
            if (this.classList.contains(exception)) {
                return true;
            }
        }

        return false;
    }
}

customElements.define('skr-button', SkrButton);
