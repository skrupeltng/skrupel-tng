package org.skrupeltng.config;

import org.skrupeltng.modules.dashboard.Roles;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.dashboard.database.LoginRole;
import org.skrupeltng.modules.dashboard.database.LoginRoleRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("userDetailsService")
public class UserDetailServiceImpl implements UserDetailsService {

	private final LoginRepository loginRepository;
	private final LoginRoleRepository loginRoleRepository;

	public UserDetailServiceImpl(LoginRepository loginRepository, LoginRoleRepository loginRoleRepository) {
		this.loginRepository = loginRepository;
		this.loginRoleRepository = loginRoleRepository;
	}

	@Override
	public LoginDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Login> loginOpt = loginRepository.findByUsername(username);

		if (loginOpt.isPresent()) {
			Login login = loginOpt.get();

			List<? extends GrantedAuthority> authorities = login.getRoles().stream().map(r -> new SimpleGrantedAuthority(r.getRoleName())).toList();
			boolean isAdmin = login.getRoles().stream().filter(r -> r.getRoleName().equals(Roles.ADMIN)).count() == 1;

			return new LoginDetails(username, login.getPassword(), authorities, login.getId(), isAdmin, login.isActive());
		}

		throw new UsernameNotFoundException(username);
	}

	public boolean isLoggedIn() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication != null && authentication.getPrincipal() instanceof LoginDetails;
	}

	private LoginDetails getLoginDetails() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return (LoginDetails) auth.getPrincipal();
	}

	public long getLoginId() {
		LoginDetails user = getLoginDetails();
		return user.getId();
	}

	public boolean isAdmin() {
		LoginDetails user = getLoginDetails();
		return user.isAdmin();
	}

	public boolean isGuest() {
		Optional<LoginRole> role = loginRoleRepository.findByLoginIdAndRoleName(getLoginId(), Roles.GUEST);
		return role.isPresent();
	}

	public boolean isAdminImpersonatingOtherUser() {
		return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
			.anyMatch(a -> a.getAuthority().equals("ROLE_PREVIOUS_ADMINISTRATOR"));
	}
}
