package org.skrupeltng.modules;

import jakarta.servlet.http.HttpServletRequest;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.config.PermissionEvaluatorImpl;
import org.skrupeltng.config.UserDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class AbstractController {

	@Autowired
	protected UserDetailServiceImpl userDetailService;

	@Autowired
	protected MessageSource messageSource;

	@Autowired
	protected ConfigProperties configProperties;

	@Autowired
	protected PermissionEvaluatorImpl permissionEvaluator;

	@Autowired
	protected HttpServletRequest servletRequest;

	protected void addFullpageSortUrlsForSortfields(Model model, Collection<SortField> sortFields, String currentSortingString) {
		Map<String, String> sortOptionUrls = new HashMap<>(sortFields.size());

		for (SortField sortField : sortFields) {
			boolean ascending = currentSortingString == null || !currentSortingString.contains(sortField.name()) || !currentSortingString.contains("-true");
			addFullpageSortUrl(sortOptionUrls, sortField, ascending);
		}

		model.addAttribute("sortOptionUrls", sortOptionUrls);
	}

	protected void addFullpageSortUrl(Map<String, String> sortOptionUrls, SortField sortField, boolean ascending) {
		String url = getCompleteRequestUrl(Set.of("sorting")) + "&sorting=" + sortField.name() + "-" + ascending;
		sortOptionUrls.put(sortField.name(), url);
	}

	protected String getCompleteRequestUrl(Set<String> excludedParams) {
		StringBuilder url = new StringBuilder(servletRequest.getServletPath());

		List<String> queryParams = new ArrayList<>();

		for (Entry<String, String[]> entry : servletRequest.getParameterMap().entrySet()) {
			String paramName = entry.getKey();
			String[] paramValues = entry.getValue();

			if (excludedParams.contains(paramName)) {
				continue;
			}

			queryParams.add(paramName + "=" + paramValues[0]);
		}

		url.append("?");
		url.append(String.join("&", queryParams));

		return url.toString();
	}

	protected boolean turnDoneForPlanet(long planetId) {
		return !permissionEvaluator.checkTurnNotDonePlanet(planetId);
	}

	protected boolean turnDoneForShip(long shipId) {
		return !permissionEvaluator.checkTurnNotDoneShip(shipId);
	}

	protected boolean turnDoneForStarbase(long starbaseId) {
		return !permissionEvaluator.checkTurnNotDoneStarbase(starbaseId);
	}

	protected boolean turnDoneForFleet(long fleetId) {
		return !permissionEvaluator.checkTurnNotDoneFleet(fleetId);
	}

	protected void addToSortItems(List<SortFieldItem> sortFields, SortField sortField) {
		sortFields.add(new SortFieldItem(sortField, true));
		sortFields.add(new SortFieldItem(sortField, false));
	}
}
