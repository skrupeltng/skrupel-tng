package org.skrupeltng.modules;

import jakarta.annotation.PostConstruct;

import org.springframework.stereotype.Service;

@Service("frontendResourcesService")
public class FrontendResourcesService {

	private long startupTime;

	@PostConstruct
	public void init() {
		startupTime = System.currentTimeMillis();
	}

	public long getStartupTime() {
		return startupTime;
	}
}
