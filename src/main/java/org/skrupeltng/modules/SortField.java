package org.skrupeltng.modules;

public enum SortField {

	EXISTING_GAMES_NAME("g.name", "name"),

	EXISTING_GAMES_GAME_MODE("g.win_condition", "win_condition"),

	EXISTING_GAMES_CREATED("g.created", "created"),

	EXISTING_GAMES_STATUS("g.started", "status"),

	EXISTING_GAMES_PLAYERCOUNT("g.player_count", "player_count"),

	EXISTING_GAMES_ROUND("g.round", "round"),

	USERS_ID("l.id", "id"),

	USERS_USERNAME("l.username", "username"),

	USERS_EMAIL("l.email", "email"),

	USERS_CREATED_DATE("l.created", "created"),

	USERS_GAMES_CREATED("\"gamesCreated\"", "games_created"),

	USERS_GAMES_PLAYED("\"gamesPlayed\"", "games_played"),

	USERS_GAMES_WON("\"gamesWon\"", "games_won"),

	USERS_GAMES_LOST("\"gamesLost\"", "games_lost"),

	ADMINGAMES_ID("g.id", "id"),

	ADMINGAMES_NAME("g.name", "name"),

	ADMINGAMES_CREATED("g.created", "created"),

	ADMINGAMES_CREATOR("c.username", "creator"),

	ADMINGAMES_ROUND_DATE("g.round_date", "last_round"),

	ADMINGAMES_ROUND("g.round", "round"),

	ADMINGAMES_GAME_MODE("g.win_condition", "win_condition"),

	ADMINGAMES_FINISHED("g.finished", "finished"),

	COLONY_OVERVIEW_NAME("p.name", "name"),

	COLONY_OVERVIEW_COLONISTS("p.colonists", "colonists"),

	COLONY_OVERVIEW_MONEY("p.money", "money"),

	COLONY_OVERVIEW_SUPPLIES("p.supplies", "supplies"),

	COLONY_OVERVIEW_FUEL("p.fuel", "fuel"),

	COLONY_OVERVIEW_MINERAL1("p.mineral1", "mineral1"),

	COLONY_OVERVIEW_MINERAL2("p.mineral2", "mineral2"),

	COLONY_OVERVIEW_MINERAL3("p.mineral3", "mineral3"),

	COLONY_OVERVIEW_MINES("p.mines", "mines"),

	COLONY_OVERVIEW_FACTORIES("p.factories", "factories"),

	COLONY_OVERVIEW_DEFENSE("p.planetary_defense", "planetary_defense"),

	COLONY_ORBITAL_SYSTEMS("\"builtOrbitalSystemCount\"", "orbital_systems"),

	SHIP_OVERVIEW_NAME("s.name", "name"),

	SHIP_OVERVIEW_TEMPLATE_NAME("st.id", "class"),

	SHIP_OVERVIEW_FLEET("f.name", "fleet"),

	SHIP_OVERVIEW_FUEL_PERCENTAGE("\"fuelPercentage\"", "fuel"),

	SHIP_OVERVIEW_CARGO_PERCENTAGE("\"cargoPercentage\"", "storage"),

	SHIP_OVERVIEW_CREW_PERCENTAGE("\"crewPercentage\"", "crew"),

	SHIP_OVERVIEW_DAMAGE("s.damage", "damage"),

	STARBASE_OVERVIEW_NAME("s.name", "name"),

	STARBASE_OVERVIEW_HAS_SHIP_IN_PRODUCTION("\"hasShipInProduction\"", "has_ship_in_production"),

	STARBASE_OVERVIEW_HULL_LEVEL("s.hull_level", "hull_level"),

	STARBASE_OVERVIEW_PROPULSION_LEVEL("s.propulsion_level", "propulsion_level"),

	STARBASE_OVERVIEW_ENERGY_LEVEL("s.energy_level", "energy_level"),

	STARBASE_OVERVIEW_PROJECTILE_LEVEL("s.projectile_level", "projectile_level"),

	FLEET_OVERVIEW_NAME("f.name", "name"),

	FLEET_OVERVIEW_SHIP_COUNT("\"shipCount\"", "ships"),

	FLEET_OVERVIEW_FUEL_PERCENTAGE("\"fuelPercentage\"", "fuel"),

	FLEET_OVERVIEW_PROJECTILE_PERCENTAGE("\"projectilePercentage\"", "projectiles"),

	PLAYER_MESSAGES_DATE("m.date", ""),

	PLAYER_MESSAGES_FROM("fromUser.username", ""),

	PLAYER_MESSAGES_SUBJECT("m.subject", "");

	private final String databaseField;
	private final String i18nKey;

	SortField(String databaseField, String i18nKey) {
		this.databaseField = databaseField;
		this.i18nKey = i18nKey;
	}

	public String getDatabaseField() {
		return databaseField;
	}

	@java.lang.SuppressWarnings("unused")
	public String getI18nKey() {
		return i18nKey;
	}
}
