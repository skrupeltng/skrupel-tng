package org.skrupeltng.modules.mail.service;

public interface MailConstants {

	String EMAIL_ADDRESS_VALIDATION_PATTERN = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

	String account_activation = "account_activation";
	String player_joined_notification = "player_joined_notification";
	String game_full_notification = "game_full_notification";
	String match_making_game_created_notification = "match_making_game_created_notification";
	String round_notification = "round_notification";
	String game_started_notification = "game_started_notification";
	String game_invite_notification = "game_invite_notification";
	String turn_not_finished_notification = "turn_not_finished_notification";
	String last_to_finish_turn_notification = "last_to_finish_turn_notification";
	String password_recovery = "password_recovery";
	String password_recovery_fail = "password_recovery_fail";
	String round_error = "round_error";
	String feedback = "feedback";
}
