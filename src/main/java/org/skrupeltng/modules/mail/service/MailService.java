package org.skrupeltng.modules.mail.service;

import jakarta.mail.Address;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.skrupeltng.modules.dashboard.database.InstallationDetailsRepository;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.IContext;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service("mailService")
public class MailService {

	public static final String LOGIN = "login";
	public static final String BASE_URL = "baseUrl";
	private final Logger log = LoggerFactory.getLogger(getClass());

	private final JavaMailSender javaMailSender;
	private final TemplateEngine templateEngine;
	private final MessageSource messageSource;
	private final PlayerRepository playerRepository;
	private final InstallationDetailsRepository installationDetailsRepository;

	public MailService(Optional<JavaMailSender> javaMailSender,
					   TemplateEngine templateEngine,
					   MessageSource messageSource,
					   PlayerRepository playerRepository,
					   InstallationDetailsRepository installationDetailsRepository) {
		this.javaMailSender = javaMailSender.orElse(null);
		this.templateEngine = templateEngine;
		this.messageSource = messageSource;
		this.playerRepository = playerRepository;
		this.installationDetailsRepository = installationDetailsRepository;
	}

	public boolean mailsEnabled() {
		return javaMailSender != null;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void sendAccountActivationMail(Login login) {
		Locale locale = LocaleContextHolder.getLocale();

		Context context = new Context(locale);
		context.setVariable(LOGIN, login);

		String baseUrl = createBaseUrl();
		context.setVariable(BASE_URL, baseUrl);

		String subject = messageSource.getMessage("email_subject_account_activation", null, locale);

		sendMail(login.getEmail(), subject, MailConstants.account_activation, context);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void sendPlayerJoinedGameNotification(Login newPlayer, Game game) {
		Login login = game.getCreator();
		Locale locale = Locale.forLanguageTag(login.getLanguage());

		Context context = createStandardGameLinkContext(game, locale, login);
		context.setVariable("newPlayer", newPlayer);

		String subject = messageSource.getMessage("email_subject_player_joined_notification", null, locale);

		sendMail(login.getEmail(), subject, MailConstants.player_joined_notification, context);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void sendGameFullNotification(Game game) {
		Login login = game.getCreator();
		Locale locale = Locale.forLanguageTag(login.getLanguage());

		Context context = createStandardGameLinkContext(game, locale, login);

		Object[] args = {game.getName()};
		String subject = messageSource.getMessage("email_subject_game_full_notification", args, locale);

		sendMail(login.getEmail(), subject, MailConstants.game_full_notification, context);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void sendGameStartedNotificationEmails(long gameId, Long triggeringLoginId) {
		sendGameNotificationEmails(gameId, triggeringLoginId, "email_subject_game_started_notification", MailConstants.game_started_notification);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void sendRoundNotificationEmails(long gameId, Long triggeringLoginId) {
		sendGameNotificationEmails(gameId, triggeringLoginId, "email_subject_round_notification", MailConstants.round_notification);
	}

	private void sendGameNotificationEmails(long gameId, Long triggeringLoginId, String subject, String template) {
		List<Player> players = playerRepository.findByGameId(gameId);

		for (Player player : players) {
			if (gameNotificationMailCanBeSend(triggeringLoginId, player)) {
				sendGameNotificationMail(player.getId(), subject, template);
			}
		}
	}

	protected boolean gameNotificationMailCanBeSend(Long triggeringLoginId, Player player) {
		Login login = player.getLogin();

		return login.isRoundNotificationsEnabled() && !player.isHasLost() &&
			(triggeringLoginId == null || login.getId() != triggeringLoginId && StringUtils.isNotBlank(login.getEmail()));
	}

	private void sendGameNotificationMail(long playerId, String subject, String template) {
		Player player = playerRepository.getReferenceById(playerId);
		Login login = player.getLogin();
		Game game = player.getGame();
		Locale locale = Locale.forLanguageTag(login.getLanguage());

		Context context = new Context(locale);
		context.setVariable(LOGIN, login);
		context.setVariable("game", game);

		String baseUrl = createBaseUrl();
		context.setVariable(BASE_URL, baseUrl);

		Object[] args = {game.getName()};
		String subjectText = messageSource.getMessage(subject, args, locale);

		sendMail(login.getEmail(), subjectText, template, context);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void sendGameInviteNotificationMail(Player player) {
		Login login = player.getLogin();

		if (!login.isInvitedToGameNotificationsEnabled()) {
			return;
		}

		Game game = player.getGame();
		Locale locale = Locale.forLanguageTag(login.getLanguage());

		Context context = createStandardGameLinkContext(game, locale, login);

		Object[] args = {game.getName()};
		String subjectText = messageSource.getMessage("email_subject_game_invite_notification", args, locale);

		sendMail(login.getEmail(), subjectText, MailConstants.game_invite_notification, context);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void sendTurnNotFinishedNotificationMail(Player player) {
		Login login = player.getLogin();

		if (!login.isNotFinishTurnNotificationsEnabled()) {
			return;
		}

		Game game = player.getGame();
		Locale locale = Locale.forLanguageTag(login.getLanguage());

		Context context = createStandardGameLinkContext(game, locale, login);

		Object[] args = {game.getName()};
		String subjectText = messageSource.getMessage("email_subject_turn_not_finished_notification", args, locale);

		sendMail(login.getEmail(), subjectText, MailConstants.turn_not_finished_notification, context);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void sendLastToFinishTurnNotificationMail(Player player) {
		Login login = player.getLogin();

		if (!login.isLastToFinishTurnNotificationsEnabled()) {
			return;
		}

		Game game = player.getGame();
		Locale locale = Locale.forLanguageTag(login.getLanguage());

		Context context = createStandardGameLinkContext(game, locale, login);

		Object[] args = {game.getName()};
		String subjectText = messageSource.getMessage("email_subject_turn_not_finished_notification", args, locale);

		sendMail(login.getEmail(), subjectText, MailConstants.last_to_finish_turn_notification, context);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void sendPasswordRecoveryMail(Login login) {
		Locale locale = Locale.forLanguageTag(login.getLanguage());

		Context context = new Context(locale);
		context.setVariable(LOGIN, login);

		String baseUrl = createBaseUrl();
		context.setVariable(BASE_URL, baseUrl);

		String subject = messageSource.getMessage("email_subject_password_recovery", null, locale);

		sendMail(login.getEmail(), subject, MailConstants.password_recovery, context);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void sendPasswordRecoveryFailMail(String email) {
		Locale locale = LocaleContextHolder.getLocale();

		Context context = new Context(locale);
		context.setVariable("email", email);

		String baseUrl = createBaseUrl();
		context.setVariable(BASE_URL, baseUrl);

		String subject = messageSource.getMessage("email_subject_password_recovery_fail", null, locale);

		sendMail(email, subject, MailConstants.password_recovery_fail, context);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void sendRoundErrorEmail(long gameId, Exception exception) {
		String contactEmail = installationDetailsRepository.getContactEmail();

		if (StringUtils.isNotBlank(contactEmail)) {
			Locale locale = Locale.ENGLISH;
			String stackTrace = ExceptionUtils.getStackTrace(exception);

			Context context = new Context(locale);
			context.setVariable("gameId", gameId);
			context.setVariable("stackTrace", stackTrace);

			sendMail(contactEmail, "Error while calculating round for game " + gameId, MailConstants.round_error, context);
		} else {
			log.warn("Unable to send round error email because no contactEmail set!");
		}
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void sendFeedbackEmail(String feedbackText, Login login) {
		String contactEmail = installationDetailsRepository.getContactEmail();

		if (StringUtils.isNotBlank(contactEmail)) {
			Locale locale = Locale.ENGLISH;

			Context context = new Context(locale);
			// Sanitize user input
			feedbackText = feedbackText.replaceAll("[\n\r]", "_");
			context.setVariable("feedbackText", feedbackText);

			try {
				String subject = "Skrupel TNG - Feedback message from user " + login.getUsername();

				MimeMessage mail = createMimeMessage(contactEmail, subject, MailConstants.feedback, context);

				if (StringUtils.isNotBlank(login.getEmail())) {
					mail.setReplyTo(new Address[]{new InternetAddress(login.getEmail())});
				}

				sendMail(mail);
			} catch (Exception e) {
				log.error("Error while creating mail: ", e);
			}
		} else {
			log.warn("Unable to send feedback email because no contactEmail set!");
		}
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void sendMatchMakingGameCreatedMail(Game game, List<Player> humanPlayers) {
		for (Player player : humanPlayers) {
			Login login = player.getLogin();

			if (!login.isMatchMakingGameCreatedNotificationsEnabled()) {
				continue;
			}

			Locale locale = Locale.forLanguageTag(login.getLanguage());

			Context context = createStandardGameLinkContext(game, locale, login);

			Object[] args = {game.getName()};
			String subject = messageSource.getMessage("email_subject_match_making_game_created", args, locale);

			sendMail(login.getEmail(), subject, MailConstants.match_making_game_created_notification, context);
		}
	}

	private Context createStandardGameLinkContext(Game game, Locale locale, Login login) {
		Context context = new Context(locale);
		context.setVariable(LOGIN, login);
		context.setVariable("game", game);

		String baseUrl = createBaseUrl();
		context.setVariable(BASE_URL, baseUrl);
		context.setVariable("link", "%s/game?id=%d".formatted(baseUrl, game.getId()));
		return context;
	}

	private String createBaseUrl() {
		return installationDetailsRepository.getDomainUrl();
	}

	private void sendMail(String to, String subject, String template, IContext context) {
		if (!mailsEnabled()) {
			return;
		}

		try {
			MimeMessage mail = createMimeMessage(to, subject, template, context);
			sendMail(mail);
		} catch (MessagingException e) {
			log.error("Error while sending email: ", e);
		}
	}

	private void sendMail(MimeMessage mail) {
		if (!mailsEnabled()) {
			return;
		}

		try {
			javaMailSender.send(mail);
		} catch (Exception e) {
			log.error("Error while sending email: ", e);
		}
	}

	private MimeMessage createMimeMessage(String to, String subject, String template, IContext context) throws MessagingException {
		String mailContent = templateEngine.process("emails/" + template, context);

		MimeMessage mail = javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mail, true);
		helper.setTo(to);
		helper.setSubject(subject);
		helper.setText(mailContent, true);
		return mail;
	}
}
