package org.skrupeltng.modules;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public interface PagingRequest {

	Integer page();

	Integer pageSize();

	String sorting();

	default Pageable toPageable() {
		Integer page = page();
		if (page == null) {
			page = 0;
		}

		Integer pageSize = pageSize();
		if (pageSize == null) {
			pageSize = 20;
		}

		if (StringUtils.isNotBlank(sorting())) {
			String[] parts = sorting().split("-");

			if (parts.length == 2) {
				String sortField = parts[0];
				boolean sortDirection = "true".equals(parts[1]);

				String sortFieldString = SortField.valueOf(sortField).getDatabaseField();
				Sort sort = Sort.by(sortDirection ? Sort.Direction.ASC : Sort.Direction.DESC, sortFieldString);

				return PageRequest.of(page, pageSize, sort);
			}
		}

		return PageRequest.of(page, pageSize);
	}
}
