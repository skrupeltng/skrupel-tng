package org.skrupeltng.modules.ai.medium;

import java.util.List;
import java.util.stream.Collectors;

import org.skrupeltng.modules.ai.easy.AIEasyPlanets;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntry;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_MEDIUM")
public class AIMediumPlanets extends AIEasyPlanets {

	@Override
	protected Planet processPlanet(Planet planet, List<Planet> planets, long playerId) {
		planet = super.processPlanet(planet, planets, playerId);
		sellSupplies(planet, planets);
		return buildDefense(planet);
	}

	protected void sellSupplies(Planet planet, List<Planet> planets) {
		if (planet.getStarbase() == null) {
			boolean hasFullyUpgradedStarbase = planets.stream().anyMatch(p -> p.getStarbase() != null && p.getStarbase().fullyUpgraded());
			planet.setAutoSellSupplies(!hasFullyUpgradedStarbase && planet.getFactories() == planet.retrieveMaxFactories() &&
					planet.getMines() == planet.retrieveMaxMines());
		} else if (planet.getSupplies() > 100) {
			int amount = planet.getSupplies() - 50;
			planet.spendSupplies(amount);
			planet.spendMoney(-amount);
		}
	}

	protected Planet buildDefense(Planet planet) {
		int defenseToBeBuild = planet.retrieveMaxPlanetaryDefense() - planet.getPlanetaryDefense();

		if (defenseToBeBuild > 0) {
			planet = planetService.buildPlanetaryDefenseWithoutPermissionCheck(planet.getId(), defenseToBeBuild);
		}

		return planet;
	}

	@Override
	protected void buildStarbase(List<Starbase> starbases, Planet planet, long playerId) {
		List<ShipRouteEntry> routeEntries = shipRouteEntryRepository.findByPlayerId(playerId);

		List<Long> shipIds = routeEntries.stream().filter(s -> planet.getDistance(s.getPlanet()) < 200).map(s -> s.getShip().getId())
				.collect(Collectors.toList());

		if (!shipIds.isEmpty()) {
			shipRepository.clearCurrentRouteEntry(shipIds);
			shipRouteEntryRepository.deleteByShipIds(shipIds);
		}

		super.buildStarbase(starbases, planet, playerId);
	}
}
