package org.skrupeltng.modules.ai.medium.ships;

import org.skrupeltng.modules.ai.easy.ships.AIEasyScouts;
import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipTransportService;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Qualifier("AI_MEDIUM")
public class AIMediumScouts extends AIEasyScouts {

	public AIMediumScouts(ShipRepository shipRepository, ShipTransportService shipTransportService, GameRepository gameRepository, VisibleObjects visibleObjects) {
		super(shipRepository, shipTransportService, gameRepository, visibleObjects);
	}

	@Override
	protected int getNecessaryFuel(Ship ship) {
		return Math.min(ship.getShipTemplate().getFuelCapacity() / 2, 50);
	}

	@Override
	protected void processScout(long gameId, long playerId, Ship ship, List<Planet> targetPlanets) {
		ship = fillTank(ship, ship.getPlanet());

		if (ship.getFuel() < getNecessaryFuel(ship) && ship.getPlanet() == null &&
			ship.getDistance(new CoordinateImpl(ship.getDestinationX(), ship.getDestinationY(), 0)) > 70) {
			List<Planet> nearPlanets = targetPlanets.stream().sorted(new CoordinateDistanceComparator(ship)).limit(5).toList();
			int nearPlanetCount = nearPlanets.size();

			if (nearPlanetCount > 0) {
				Planet nextTarget = nearPlanets.get(MasterDataService.RANDOM.nextInt(nearPlanetCount));
				ship.setDestinationX(nextTarget.getX());
				ship.setDestinationY(nextTarget.getY());
				ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
				shipRepository.save(ship);
			}
		} else {
			super.processScout(gameId, playerId, ship, targetPlanets);
		}
	}
}
