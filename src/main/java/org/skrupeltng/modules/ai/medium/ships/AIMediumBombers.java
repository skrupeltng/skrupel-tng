package org.skrupeltng.modules.ai.medium.ships;

import java.util.List;
import java.util.Optional;

import org.skrupeltng.modules.ai.AIConstants;
import org.skrupeltng.modules.ai.easy.ships.AIEasyBombers;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipTransportService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_MEDIUM")
public class AIMediumBombers extends AIEasyBombers {

	private final ShipService shipService;

	public AIMediumBombers(ShipRepository shipRepository, PlanetRepository planetRepository, ShipTransportService shipTransportService, PoliticsService politicsService, ShipService shipService) {
		super(shipRepository, planetRepository, shipTransportService, politicsService);
		this.shipService = shipService;
	}

	@Override
	protected Optional<Planet> setTargetPlanet(long playerId, Ship ship, List<Planet> targetPlanets, List<Planet> visiblePlanets) {
		final long shipId = ship.getId();
		shipService.buildAllPossibleProjectilesAutomaticallyWithoutPermissionChecks(ship);
		ship = shipRepository.getReferenceById(shipId);

		if (ship.getDestinationShip() != null && ship.getDestinationShip().getPlayer().getId() == playerId) {
			return Optional.empty();
		}

		Planet planet = ship.getPlanet();

		if (planet != null && planet.getPlayer() != null && planet.getPlayer().getId() == playerId && planet.getStarbase() != null) {
			List<Ship> ships = shipRepository.findByGameIdAndPlanetId(planet.getGame().getId(), planet.getId());

			long otherBombers = ships.stream().filter(s -> s.getId() != shipId && s.getName().contains(AIConstants.AI_BOMBER_NAME)).count();

			if (otherBombers == 0L) {
				ship.setLog(AIConstants.AI_BOMBER_NAME);
				ship.resetTravel();
				return Optional.empty();
			}

			int minOtherBomberCount = getMinOtherBomberCount(ship);

			if (otherBombers < minOtherBomberCount) {
				ship.resetTravel();

				if (ship.getLog() == null) {
					Optional<Ship> mainShipOpt = ships.stream().filter(s -> AIConstants.AI_BOMBER_NAME.equals(s.getLog())).findAny();

					if (mainShipOpt.isPresent()) {
						Ship mainShip = mainShipOpt.get();
						ship.setDestinationShip(mainShip);
						ship.setDestinationX(mainShip.getX());
						ship.setDestinationY(mainShip.getY());
						ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
					}
				}

				return Optional.empty();
			}
		}

		return super.setTargetPlanet(playerId, ship, targetPlanets, visiblePlanets);
	}

	protected int getMinOtherBomberCount(Ship ship) {
		int round = ship.getPlayer().getGame().getRound();

		int minOtherBomberCount = 2;

		if (round > 40) {
			minOtherBomberCount = 3;
		}
		if (round > 60) {
			minOtherBomberCount = 4;
		}
		if (round > 80) {
			minOtherBomberCount = 5;
		}
		if (round > 100) {
			minOtherBomberCount = 6;
		}

		return minOtherBomberCount;
	}
}
