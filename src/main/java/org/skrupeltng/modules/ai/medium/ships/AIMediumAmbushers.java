package org.skrupeltng.modules.ai.medium.ships;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ai.easy.ships.AIEasyAmbushers;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipTransportService;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_MEDIUM")
public class AIMediumAmbushers extends AIEasyAmbushers {

	private ShipService shipService;

	public AIMediumAmbushers(ShipRepository shipRepository, ShipTransportService shipTransportService, VisibleObjects visibleObjects, GameRepository gameRepository, PlayerRepository playerRepository, PoliticsService politicsService, ConfigProperties configProperties, ShipService shipService) {
		super(shipRepository, shipTransportService, visibleObjects, gameRepository, playerRepository, politicsService, configProperties);
		this.shipService = shipService;
	}

	@Override
	protected void processAmbushers(long playerId, long gameId, Ship ship) {
		final long shipId = ship.getId();
		shipService.buildAllPossibleProjectilesAutomaticallyWithoutPermissionChecks(ship);

		ship = shipRepository.getReferenceById(shipId);

		super.processAmbushers(playerId, gameId, ship);
	}

	@Override
	protected boolean setTarget(long playerId, Ship ship, long gameId) {
		if (gameRepository.getWinCondition(gameId) == WinCondition.CONQUEST) {
			int center = gameRepository.getGalaxySize(gameId) / 2;

			if (ship.getDistance(new CoordinateImpl(center, center, 0)) > 100) {
				ship.setDestinationX(center);
				ship.setDestinationY(center);
				ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
				return true;
			}
		}

		return super.setTarget(playerId, ship, gameId);
	}
}
