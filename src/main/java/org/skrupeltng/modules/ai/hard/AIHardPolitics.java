package org.skrupeltng.modules.ai.hard;

import org.skrupeltng.modules.ai.medium.AIMediumPolitics;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_HARD")
public class AIHardPolitics extends AIMediumPolitics {

	// @Override
	// public void processPolitics(long playerId) {
	// super.processPolitics(playerId);
	// }
}
