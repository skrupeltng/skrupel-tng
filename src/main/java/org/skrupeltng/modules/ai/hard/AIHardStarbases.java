package org.skrupeltng.modules.ai.hard;

import org.skrupeltng.modules.ai.medium.AIMediumStarbases;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_HARD")
public class AIHardStarbases extends AIMediumStarbases {

	@Override
	protected void buildBomber(Starbase starbase) {
		if (starbase.getPlanet().getFuel() > 100) {
			super.buildBomber(starbase);
		}
	}

	@Override
	protected void buildAmbusher(Starbase starbase) {
		if (starbase.getPlanet().getFuel() > 100) {
			super.buildAmbusher(starbase);
		}
	}
}