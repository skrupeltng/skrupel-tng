package org.skrupeltng.modules.ai.hard;

import org.skrupeltng.modules.ai.AIRoundCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component("AI_HARD")
public class AIHardRoundCalculator implements AIRoundCalculator {

	@Autowired
	@Qualifier("AI_HARD")
	private AIHardShips ships;

	@Autowired
	@Qualifier("AI_HARD")
	private AIHardPlanets planets;

	@Autowired
	@Qualifier("AI_HARD")
	private AIHardStarbases starbases;

	@Autowired
	@Qualifier("AI_HARD")
	private AIHardPolitics politics;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void calculateRound(long playerId) {
		ships.processShips(playerId);
		planets.processPlanets(playerId);
		starbases.processStarbases(playerId);
		politics.processPolitics(playerId);
	}
}
