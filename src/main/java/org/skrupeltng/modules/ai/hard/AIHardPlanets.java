package org.skrupeltng.modules.ai.hard;

import org.skrupeltng.modules.ai.medium.AIMediumPlanets;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_HARD")
public class AIHardPlanets extends AIMediumPlanets {

}