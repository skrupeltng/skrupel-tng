package org.skrupeltng.modules.ai.easy.ships;

import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipTransportService;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@Qualifier("AI_EASY")
public class AIEasyBombers {

	protected final ShipRepository shipRepository;
	protected final PlanetRepository planetRepository;
	protected final ShipTransportService shipTransportService;
	protected final PoliticsService politicsService;

	public AIEasyBombers(ShipRepository shipRepository, PlanetRepository planetRepository, ShipTransportService shipTransportService, PoliticsService politicsService) {
		this.shipRepository = shipRepository;
		this.planetRepository = planetRepository;
		this.shipTransportService = shipTransportService;
		this.politicsService = politicsService;
	}

	protected void processBomber(long playerId, Ship ship, List<Planet> targetPlanets, List<Planet> visiblePlanets) {
		Optional<ShipAbility> viralInvasion = ship.getAbility(ShipAbilityType.VIRAL_INVASION);

		if (viralInvasion.isPresent()) {
			ship.setActiveAbility(viralInvasion.get());
			ship.setTaskValue(ShipAbilityConstants.VIRAL_INVASION_COLONISTS);
			ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
		} else {
			ship.setActiveAbility(null);
			ship.setTaskType(ShipTaskType.PLANET_BOMBARDMENT);
		}

		if (isNearTargetPlanet(ship)) {
			return;
		}

		Optional<Planet> targetPlanet = setTargetPlanet(playerId, ship, targetPlanets, visiblePlanets);

		Optional<ShipAbility> destabilizer = ship.getAbility(ShipAbilityType.DESTABILIZER);

		if (targetPlanet.isPresent() && ship.getPlanet() == targetPlanet.get() && destabilizer.isPresent()) {
			ship.setActiveAbility(destabilizer.get());
			ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
		}

		ship = shipRepository.save(ship);

		Planet planet = ship.getPlanet();

		if (planet != null) {
			ShipTransportRequest request = new ShipTransportRequest(ship);
			request.setFuel(Math.min(ship.getFuel() + planet.getFuel(), ship.getShipTemplate().getFuelCapacity()));

			shipTransportService.transportWithoutPermissionCheck(request, ship, planet);
		}
	}

	protected boolean isNearTargetPlanet(Ship ship) {
		Player player = ship.getPlayer();
		Optional<Planet> planetOpt = planetRepository.findByGameIdAndXAndY(player.getGame().getId(), ship.getDestinationX(), ship.getDestinationY());

		if (planetOpt.isPresent()) {
			Planet planet = planetOpt.get();
			Player planetOwner = planet.getPlayer();

			if (planetOwner != null && politicsService.isAlly(player.getId(), planetOwner.getId())) {
				return false;
			}

			if (ship.getPlanet() != null && planet.getId() == ship.getPlanet().getId()) {
				return planetOwner != null && planet.getColonists() >= 1000;
			}
		}

		return (ship.getDestinationX() > 0 || ship.getDestinationY() > 0) && ship.getDistance(ship.retrieveDestinationCoordinate()) < 70;
	}

	protected Optional<Planet> setTargetPlanet(long playerId, Ship ship, List<Planet> targetPlanets, List<Planet> visiblePlanets) {
		CoordinateDistanceComparator comparator = new CoordinateDistanceComparator(ship);

		Optional<Planet> targetPlanetOpt = getTargetPlanetOpt(playerId, targetPlanets, comparator);

		if (targetPlanetOpt.isEmpty()) {
			targetPlanetOpt = getTargetPlanetOpt(playerId, visiblePlanets, comparator);
		}

		if (targetPlanetOpt.isEmpty()) {
			return Optional.empty();
		}

		Planet targetPlanet = targetPlanetOpt.get();

		ship.setDestinationShip(null);
		ship.setDestinationX(targetPlanet.getX());
		ship.setDestinationY(targetPlanet.getY());
		ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());

		return Optional.of(targetPlanet);
	}

	protected Optional<Planet> getTargetPlanetOpt(long playerId, List<Planet> possiblePlanets, CoordinateDistanceComparator comparator) {
		return possiblePlanets.stream()
			.filter(p -> p.getPlayer() != null && p.getPlayer().getId() != playerId && !politicsService.isAlly(playerId, p.getPlayer().getId()))
			.min(comparator);
	}
}
