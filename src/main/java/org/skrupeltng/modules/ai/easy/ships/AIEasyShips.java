package org.skrupeltng.modules.ai.easy.ships;

import org.skrupeltng.modules.ai.AIConstants;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Qualifier("AI_EASY")
public class AIEasyShips {

	@Autowired
	protected PlanetRepository planetRepository;

	@Autowired
	protected PlayerRepository playerRepository;

	@Autowired
	protected ShipRepository shipRepository;

	@Autowired
	protected ShipRouteEntryRepository shipRouteEntryRepository;

	@Autowired
	protected MasterDataService masterDataService;

	@Autowired
	protected ShipService shipService;

	@Autowired
	protected GameRepository gameRepository;

	@Autowired
	protected VisibleObjects visibleObjects;

	@Autowired
	protected PlayerRelationRepository playerRelationRepository;

	@Autowired
	@Qualifier("AI_EASY")
	private AIEasyFreighters freighters;

	@Autowired
	@Qualifier("AI_EASY")
	private AIEasyScouts scouts;

	@Autowired
	@Qualifier("AI_EASY")
	private AIEasyBombers bombers;

	@Autowired
	@Qualifier("AI_EASY")
	private AIEasyAmbushers ambushers;

	protected AIEasyFreighters getFreighters() {
		return freighters;
	}

	protected AIEasyScouts getScouts() {
		return scouts;
	}

	protected AIEasyBombers getBombers() {
		return bombers;
	}

	protected AIEasyAmbushers getAmbushers() {
		return ambushers;
	}

	public void processShips(long playerId) {
		List<Ship> allPlayerShips = shipRepository.findByPlayerId(playerId);
		List<Ship> movingShips = shipRepository.getMovingShipsByPlayerId(playerId);

		List<PlayerRelation> friendlyRelations = playerRelationRepository.findByPlayerIdAndTypes(playerId,
				List.of(PlayerRelationType.ALLIANCE, PlayerRelationType.NON_AGGRESSION_TREATY, PlayerRelationType.TRADE_AGREEMENT));

		Set<Long> friendlyPlayerIds = new HashSet<>(friendlyRelations.size());

		for (PlayerRelation playerRelation : friendlyRelations) {
			if (playerRelation.getPlayer1().getId() == playerId) {
				friendlyPlayerIds.add(playerRelation.getPlayer2().getId());
			} else {
				friendlyPlayerIds.add(playerRelation.getPlayer1().getId());
			}
		}

		long gameId = playerRepository.getGameIdByPlayerId(playerId);
		Set<CoordinateImpl> visibilityCoordinates = visibleObjects.getVisibilityCoordinates(playerId);
		List<PlanetEntry> visiblePlanetEntries = visibleObjects.getVisiblePlanetsCached(gameId, visibilityCoordinates, playerId);
		List<Planet> visiblePlanets = planetRepository.findByIds(visiblePlanetEntries.stream().map(PlanetEntry::getId).toList());

		List<Planet> targetPlanets = visiblePlanets.stream()
				.filter(p -> (p.getPlayer() == null || (p.getPlayer().getId() != playerId && !friendlyPlayerIds.contains(p.getPlayer().getId()))) &&
						movingShips.stream().noneMatch(s -> s.getDestinationX() == p.getX() && s.getDestinationY() == p.getY()))
				.collect(Collectors.toList());

		List<Planet> ownedPlanets = visiblePlanets.stream()
				.filter(p -> p.getPlayer() != null && p.getPlayer().getId() == playerId)
				.toList();

		List<Planet> highPopulationPlanets = ownedPlanets.stream()
				.filter(p -> p.getColonists() > 20000)
				.toList();

		for (Ship ship : allPlayerShips) {
			processShip(allPlayerShips, targetPlanets, visiblePlanets, ownedPlanets, highPopulationPlanets, ship);
		}
	}

	protected void processShip(List<Ship> allPlayerShips, List<Planet> targetPlanets, List<Planet> visiblePlanets,
			List<Planet> ownedPlanets, List<Planet> highPopulationPlanets, Ship ship) {
		Player player = ship.getPlayer();
		long playerId = player.getId();
		long gameId = player.getGame().getId();
		String name = ship.getName();

		if (name.contains(AIConstants.AI_SCOUT_NAME)) {
			getScouts().processScout(gameId, playerId, ship, targetPlanets);
		} else if (name.contains(AIConstants.AI_FREIGHTER_NAME)) {
			getFreighters().processFreighter(playerId, allPlayerShips, targetPlanets, ownedPlanets, highPopulationPlanets, ship);
		} else if (name.contains(AIConstants.AI_SUBPARTICLECLUSTER_NAME)) {
			processSubParticleCluster(ship);
		} else if (name.contains(AIConstants.AI_QUARKREORGANIZER_NAME)) {
			processQuarkReorganizer(ship);
		} else if (name.contains(AIConstants.AI_BOMBER_NAME)) {
			getBombers().processBomber(playerId, ship, targetPlanets, visiblePlanets);
		} else if (name.contains(AIConstants.AI_AMBUSHER_NAME)) {
			getAmbushers().processAmbushers(playerId, gameId, ship);
		}
	}

	protected void processSubParticleCluster(Ship ship) {
		activateAutoGenerationAbility(ship, ShipAbilityType.SUB_PARTICLE_CLUSTER);
	}

	protected void processQuarkReorganizer(Ship ship) {
		activateAutoGenerationAbility(ship, ShipAbilityType.QUARK_REORGANIZER);
	}

	protected void activateAutoGenerationAbility(Ship ship, ShipAbilityType abilityType) {
		ship.getAbility(abilityType).ifPresent(ability -> {
			ship.setActiveAbility(ability);
			ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
			ship.setTaskValue("true");
			shipRepository.save(ship);
		});
	}
}
