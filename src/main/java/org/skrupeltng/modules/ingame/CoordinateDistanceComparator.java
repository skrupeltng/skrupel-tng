package org.skrupeltng.modules.ingame;

import java.util.Comparator;

public class CoordinateDistanceComparator implements Comparator<Coordinate> {

	private final Coordinate distancePoint;

	public CoordinateDistanceComparator(Coordinate distancePoint) {
		this.distancePoint = distancePoint;
	}

	@Override
	public int compare(Coordinate a, Coordinate b) {
		double dist1 = distancePoint.getDistance(a);
		double dist2 = distancePoint.getDistance(b);
		return Double.compare(dist1, dist2);
	}
}