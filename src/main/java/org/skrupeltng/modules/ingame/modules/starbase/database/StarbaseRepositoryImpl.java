package org.skrupeltng.modules.ingame.modules.starbase.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.ingame.controller.StarbaseListResultDTO;
import org.skrupeltng.modules.ingame.controller.StarbaseOverviewRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

public class StarbaseRepositoryImpl extends RepositoryCustomBase implements StarbaseRepositoryCustom {

	@Override
	public boolean loginOwnsStarbase(long starbaseId, long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	p.login_id \n" +
				"FROM \n" +
				"	starbase s \n" +
				"	INNER JOIN planet pl" +
				"		ON pl.starbase_id = s.id \n" +
				"	INNER JOIN player p \n" +
				"		ON p.id = pl.player_id \n" +
				"WHERE \n" +
				"	s.id = :starbaseId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("starbaseId", starbaseId);

		List<Long> results = jdbcTemplate.queryForList(sql, params, Long.class);
		return results.size() == 1 && results.get(0).longValue() == loginId;
	}

	@Override
	public Page<StarbaseListResultDTO> searchStarbases(StarbaseOverviewRequest request, long loginId) {
		long gameId = request.getGameId();
		String name = request.getName();
		Boolean hasShipInProduction = request.getHasShipInProduction();
		Boolean fullyUpgraded = request.getFullyUpgraded();

		Pageable page = request.toPageable();

		String sql = "" +
				"SELECT \n" +
				"	s.id, \n" +
				"	s.name, \n" +
				"	(j.id IS NOT NULL) as \"hasShipInProduction\", \n" +
				"	s.hull_level as \"hullLevel\", \n" +
				"	s.propulsion_level as \"propulsionLevel\", \n" +
				"	s.energy_level as \"energyLevel\", \n" +
				"	s.projectile_level as \"projectileLevel\", \n" +
				"	count(*) OVER() AS \"totalElements\" \n" +
				"FROM \n" +
				"	starbase s \n" +
				"	INNER JOIN planet p \n" +
				"		ON p.starbase_id = s.id AND p.game_id = :gameId  \n" +
				"	INNER JOIN player pl \n" +
				"		ON pl.id = p.player_id AND pl.login_id = :loginId \n" +
				"	LEFT OUTER JOIN starbase_ship_construction_job j \n" +
				"		ON j.starbase_id = s.id \n";

		Map<String, Object> params = new HashMap<>();
		params.put("gameId", gameId);
		params.put("loginId", loginId);

		List<String> wheres = new ArrayList<>();

		if (StringUtils.isNotBlank(name)) {
			wheres.add("s.name ILIKE :name");
			params.put("name", "%" + name + "%");
		}

		if (hasShipInProduction != null) {
			if (hasShipInProduction) {
				wheres.add("j.id IS NOT NULL");
			} else {
				wheres.add("j.id IS NULL");
			}
		}

		if (fullyUpgraded != null) {
			String condition = "s.hull_level = 10 AND s.propulsion_level = 10 AND s.energy_level = 10 AND s.projectile_level = 10";

			if (fullyUpgraded) {
				wheres.add(condition);
			} else {
				wheres.add("NOT (" + condition + ")");
			}
		}

		sql += where(wheres);

		sql += createOrderBy(page.getSort(), "s.name ASC");

		RowMapper<StarbaseListResultDTO> rowMapper = new BeanPropertyRowMapper<>(StarbaseListResultDTO.class);

		return search(sql, params, page, rowMapper);
	}
}