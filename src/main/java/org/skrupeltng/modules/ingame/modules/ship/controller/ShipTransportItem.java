package org.skrupeltng.modules.ingame.modules.ship.controller;

import org.skrupeltng.modules.ingame.modules.ship.service.ShipTransportResource;

public class ShipTransportItem {

	private static final int STANDARD_FACTOR = 100;

	private final int leftValue;
	private final int rightValue;
	private final int leftMin;
	private final int leftMax;
	private final int rightMin;
	private final int rightMax;
	private final ShipTransportResource resource;

	public ShipTransportItem(int leftValue, int capacityLeft, int rightValue, int capacityRight, ShipTransportResource resource) {
		this.leftValue = leftValue;
		this.rightValue = rightValue;
		this.resource = resource;

		if (capacityLeft != Integer.MAX_VALUE) {
			capacityLeft *= STANDARD_FACTOR;
		}

		if (capacityRight != Integer.MAX_VALUE) {
			capacityRight *= STANDARD_FACTOR;
		}

		int factor = getFactor();
		leftValue *= factor;
		rightValue *= factor;

		int freeCapacityLeft = capacityLeft - leftValue;
		int freeCapacityRight = capacityRight - rightValue;

		leftMin = Math.max(0, leftValue - freeCapacityRight) / factor;
		leftMax = (leftValue + Math.min(freeCapacityLeft, rightValue)) / factor;

		rightMin = Math.max(0, rightValue - freeCapacityLeft) / factor;
		rightMax = (rightValue + Math.min(freeCapacityRight, leftValue)) / factor;
	}

	private int getFactor() {
		if (resource == ShipTransportResource.colonists || resource == ShipTransportResource.money) {
			return 1;
		}

		if (resource == ShipTransportResource.lightgroundunits) {
			return 30;
		}

		if (resource == ShipTransportResource.heavygroundunits) {
			return 150;
		}

		return STANDARD_FACTOR;
	}

	public int getLeftValue() {
		return leftValue;
	}

	public int getRightValue() {
		return rightValue;
	}

	public int getLeftMin() {
		return leftMin;
	}

	public int getLeftMax() {
		return leftMax;
	}

	public int getRightMin() {
		return rightMin;
	}

	public int getRightMax() {
		return rightMax;
	}

	public ShipTransportResource getResource() {
		return resource;
	}
}
