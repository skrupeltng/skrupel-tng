package org.skrupeltng.modules.ingame.modules.fleet.service;

import java.util.List;
import java.util.stream.Collectors;

import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTravelData;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class FleetLeader {

	@Autowired
	private FleetRepository fleetRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private VisibleObjects visibleObjects;

	@Transactional(propagation = Propagation.REQUIRED)
	public List<ShipTravelData> setLeaderWithoutPermissionCheck(long fleetId, long shipId) {
		Fleet fleet = fleetRepository.getReferenceById(fleetId);
		Ship ship = shipRepository.getReferenceById(shipId);

		if (!fleet.getShips().contains(ship)) {
			throw new IllegalArgumentException("Ship " + shipId + " cannot be made leader of fleet " + fleetId + " because it is not part of that fleet!");
		}

		changeLeader(fleet, ship);

		fleet = fleetRepository.getReferenceById(fleetId);
		List<Ship> ships = fleet.getShips();

		return ships.stream().map(s -> new ShipTravelData(s)).collect(Collectors.toList());
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(propagation = Propagation.REQUIRED)
	public List<ShipTravelData> setLeader(long fleetId, long shipId) {
		return setLeaderWithoutPermissionCheck(fleetId, shipId);
	}

	public void changeLeader(Fleet fleet, Ship newLeader) {
		fleet.setLeader(newLeader);
		fleet = fleetRepository.save(fleet);

		updateFleetDestinations(fleet, newLeader);
	}

	public void updateFleetDestinations(Fleet fleet, Ship newLeader) {
		if (newLeader != null && fleet.getShips() != null && fleet.getShips().size() > 1) {
			if (fleet.getShips().contains(newLeader.getDestinationShip())) {
				newLeader.resetTravel();
				shipRepository.save(newLeader);
			}

			for (Ship ship : fleet.getShips()) {
				if (ship != newLeader) {
					if (ship.getTravelSpeed() == 0) {
						ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
					}

					ship.setDestinationShip(newLeader);
					ship.setDestinationX(newLeader.getX());
					ship.setDestinationY(newLeader.getY());
					shipRepository.save(ship);
				}
			}
		}

		visibleObjects.clearCaches();
	}
}
