package org.skrupeltng.modules.ingame.modules.ship.database;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.skrupeltng.modules.dashboard.modules.storymode.service.missions.silverstarag.SilverStarAGMission1;
import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.GoodsContainer;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipCloakingSummary;
import org.skrupeltng.modules.ingame.service.round.combat.ShipDamage;
import org.skrupeltng.modules.masterdata.database.*;
import org.skrupeltng.modules.masterdata.service.MasterDataConstants;
import org.skrupeltng.modules.masterdata.service.MasterDataService;

import jakarta.persistence.*;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Entity
@Table(name = "ship")
public class Ship implements Serializable, Coordinate, GoodsContainer {

	private static final int MAX_SHIP_EXPERIENCE = 5;

	private static final int MAX_INFO_HEIGHT = 65;

	@Serial
	private static final long serialVersionUID = -3929826340145908777L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String name;

	private int x;

	private int y;

	@ManyToOne(fetch = FetchType.LAZY)
	private Player player;

	@ManyToOne(fetch = FetchType.LAZY)
	private Planet planet;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ship_template_id")
	private ShipTemplate shipTemplate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "propulsion_system_template_name")
	private PropulsionSystemTemplate propulsionSystemTemplate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "energy_weapon_template_name")
	private WeaponTemplate energyWeaponTemplate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "projectile_weapon_template_name")
	private WeaponTemplate projectileWeaponTemplate;

	private int aggressiveness;

	@Enumerated(EnumType.STRING)
	private ShipTactics tactics = ShipTactics.STANDARD;

	private int damage;

	private int shield;

	private int crew;

	private int fuel;

	private int colonists;

	private int money;

	private int mineral1;

	private int mineral2;

	private int mineral3;

	private int supplies;

	@Column(name = "light_ground_units")
	private int lightGroundUnits;

	@Column(name = "heavy_ground_units")
	private int heavyGroundUnits;

	private int projectiles;

	@Column(name = "auto_build_projectiles")
	private boolean autoBuildProjectiles;

	private int experience;

	@Column(name = "destination_x")
	private int destinationX;

	@Column(name = "destination_y")
	private int destinationY;

	@Column(name = "last_x")
	private int lastX;

	@Column(name = "last_y")
	private int lastY;

	@Column(name = "travel_speed")
	private int travelSpeed;

	@Column(name = "pre_plasma_storm_travel_speed")
	private int prePlasmaStormTravelSpeed;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "destination_ship_id")
	private Ship destinationShip;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "active_ability_id")
	private ShipAbility activeAbility;

	@Enumerated(EnumType.STRING)
	@Column(name = "task_type")
	private ShipTaskType taskType = ShipTaskType.NONE;

	@Column(name = "task_value")
	private String taskValue;

	private boolean cloaked;

	@Column(name = "scan_radius")
	private int scanRadius = 47;

	@Column(name = "creation_x")
	private int creationX;

	@Column(name = "creation_y")
	private int creationY;

	private String log;

	@Column(name = "route_min_fuel")
	private int routeMinFuel;

	@Column(name = "route_travel_speed")
	private int routeTravelSpeed;

	@Enumerated(EnumType.STRING)
	@Column(name = "route_primary_resource")
	private Resource routePrimaryResource;

	@Column(name = "route_disabled")
	private boolean routeDisabled;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "current_route_entry_id")
	private ShipRouteEntry currentRouteEntry;

	@Column(name = "hunter_training_months")
	private int hunterTrainingMonths;

	@Enumerated(EnumType.STRING)
	@Column(name = "ship_module")
	private ShipModule shipModule;

	@Column(name = "distance_travelled")
	private double distanceTravelled;

	@ManyToOne(fetch = FetchType.LAZY)
	private Fleet fleet;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "ship")
	private List<ShipRouteEntry> route;

	private transient boolean wasInCircle;
	private transient int flightFlag;
	private transient int flightQueue;

	private transient boolean scannedByPlayer;

	private transient Player playerBeforeCapture;

	private transient ShipCloakingSummary cloakingSummary;

	@Override
	public String toString() {
		return "Ship [id=" + id + ", name=" + name + ", x=" + x + ", y=" + y + ", shipTemplate=" + shipTemplate + "]";
	}

	public Ship() {

	}

	public Ship(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	@Override
	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	public ShipTemplate getShipTemplate() {
		return shipTemplate;
	}

	public void setShipTemplate(ShipTemplate shipTemplate) {
		this.shipTemplate = shipTemplate;
	}

	public PropulsionSystemTemplate getPropulsionSystemTemplate() {
		return propulsionSystemTemplate;
	}

	public void setPropulsionSystemTemplate(PropulsionSystemTemplate propulsionSystemTemplate) {
		this.propulsionSystemTemplate = propulsionSystemTemplate;
	}

	public WeaponTemplate getEnergyWeaponTemplate() {
		return energyWeaponTemplate;
	}

	public void setEnergyWeaponTemplate(WeaponTemplate energyWeaponTemplate) {
		this.energyWeaponTemplate = energyWeaponTemplate;
	}

	public WeaponTemplate getProjectileWeaponTemplate() {
		return projectileWeaponTemplate;
	}

	public void setProjectileWeaponTemplate(WeaponTemplate projectileWeaponTemplate) {
		this.projectileWeaponTemplate = projectileWeaponTemplate;
	}

	public int getAggressiveness() {
		return aggressiveness;
	}

	public void setAggressiveness(int aggressiveness) {
		this.aggressiveness = aggressiveness;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int getShield() {
		return shield;
	}

	public void setShield(int shield) {
		this.shield = shield;
	}

	public int getCrew() {
		return crew;
	}

	public void setCrew(int crew) {
		this.crew = crew;
	}

	@Override
	public int getFuel() {
		return fuel;
	}

	public void setFuel(int fuel) {
		this.fuel = fuel;
	}

	@Override
	public int getColonists() {
		return colonists;
	}

	public void setColonists(int colonists) {
		this.colonists = colonists;
	}

	@Override
	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	@Override
	public int getMineral1() {
		return mineral1;
	}

	public void setMineral1(int mineral1) {
		this.mineral1 = mineral1;
	}

	@Override
	public int getMineral2() {
		return mineral2;
	}

	public void setMineral2(int mineral2) {
		this.mineral2 = mineral2;
	}

	@Override
	public int getMineral3() {
		return mineral3;
	}

	public void setMineral3(int mineral3) {
		this.mineral3 = mineral3;
	}

	@Override
	public int getSupplies() {
		return supplies;
	}

	public void setSupplies(int supplies) {
		this.supplies = supplies;
	}

	@Override
	public int getLightGroundUnits() {
		return lightGroundUnits;
	}

	public void setLightGroundUnits(int lightGroundUnits) {
		this.lightGroundUnits = lightGroundUnits;
	}

	@Override
	public int getHeavyGroundUnits() {
		return heavyGroundUnits;
	}

	public void setHeavyGroundUnits(int heavyGroundUnits) {
		this.heavyGroundUnits = heavyGroundUnits;
	}

	public int getProjectiles() {
		return projectiles;
	}

	public void setProjectiles(int projectiles) {
		if (projectiles > retrieveMaxProjectiles()) {
			throw new IllegalArgumentException("Cannot build more than " + retrieveMaxProjectiles() + " projectiles on ship " + id + "!");
		}

		this.projectiles = projectiles;
	}

	public void addProjectiles(int newProjectiles) {
		this.projectiles += newProjectiles;
	}

	public boolean isAutoBuildProjectiles() {
		return autoBuildProjectiles;
	}

	public void setAutoBuildProjectiles(boolean autoBuildProjectiles) {
		this.autoBuildProjectiles = autoBuildProjectiles;
	}

	public int getExperience() {
		return experience;
	}

	public int getExperienceForCombat() {
		if (shipModule == ShipModule.TACTICAL_AI && experience < MAX_SHIP_EXPERIENCE) {
			return experience + 1;
		}

		return experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}

	public int getDestinationX() {
		return destinationX;
	}

	public void setDestinationX(int destinationX) {
		this.destinationX = destinationX;
	}

	public int getDestinationY() {
		return destinationY;
	}

	public void setDestinationY(int destinationY) {
		this.destinationY = destinationY;
	}

	public int getTravelSpeed() {
		return travelSpeed;
	}

	public void setTravelSpeed(int travelSpeed) {
		if (travelSpeed < 1 || getPropulsionSystemTemplate().getTechLevel() == 1 && travelSpeed > 1) {
			travelSpeed = 1;
		}
		if (travelSpeed > 9) {
			travelSpeed = 9;
		}
		this.travelSpeed = travelSpeed;
	}

	public int getPrePlasmaStormTravelSpeed() {
		return prePlasmaStormTravelSpeed;
	}

	public void setPrePlasmaStormTravelSpeed(int prePlasmaStormTravelSpeed) {
		this.prePlasmaStormTravelSpeed = prePlasmaStormTravelSpeed;
	}

	public Ship getDestinationShip() {
		return destinationShip;
	}

	public void setDestinationShip(Ship destinationShip) {
		this.destinationShip = destinationShip;
	}

	public int getLastX() {
		return lastX;
	}

	public void setLastX(int lastX) {
		this.lastX = lastX;
	}

	public int getLastY() {
		return lastY;
	}

	public void setLastY(int lastY) {
		this.lastY = lastY;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public ShipAbility getActiveAbility() {
		return activeAbility;
	}

	public void setActiveAbility(ShipAbility activeAbility) {
		this.activeAbility = activeAbility;
	}

	public ShipTaskType getTaskType() {
		return taskType;
	}

	public void setTaskType(ShipTaskType taskType) {
		this.taskType = taskType;
	}

	public String getTaskValue() {
		return taskValue;
	}

	public void setTaskValue(String taskValue) {
		this.taskValue = taskValue;
	}

	public boolean isCloaked() {
		return cloaked;
	}

	public void setCloaked(boolean cloaked) {
		this.cloaked = cloaked;
	}

	@Override
	public int getScanRadius() {
		return scanRadius;
	}

	public void setScanRadius(int scanRadius) {
		this.scanRadius = scanRadius;
	}

	public int getCreationX() {
		return creationX;
	}

	public void setCreationX(int creationX) {
		this.creationX = creationX;
	}

	public int getCreationY() {
		return creationY;
	}

	public void setCreationY(int creationY) {
		this.creationY = creationY;
	}

	public ShipTactics getTactics() {
		return tactics;
	}

	public void setTactics(ShipTactics tactics) {
		this.tactics = tactics;
	}

	public int getRouteMinFuel() {
		return routeMinFuel;
	}

	public void setRouteMinFuel(int routeMinFuel) {
		this.routeMinFuel = routeMinFuel;
	}

	public int getRouteTravelSpeed() {
		return routeTravelSpeed;
	}

	public void setRouteTravelSpeed(int routeTravelSpeed) {
		this.routeTravelSpeed = routeTravelSpeed;
	}

	public Resource getRoutePrimaryResource() {
		return routePrimaryResource;
	}

	public void setRoutePrimaryResource(Resource routePrimaryResource) {
		this.routePrimaryResource = routePrimaryResource;
	}

	public boolean isRouteDisabled() {
		return routeDisabled;
	}

	public void setRouteDisabled(boolean routeDisabled) {
		this.routeDisabled = routeDisabled;
	}

	public ShipRouteEntry getCurrentRouteEntry() {
		return currentRouteEntry;
	}

	public void setCurrentRouteEntry(ShipRouteEntry currentRouteEntry) {
		this.currentRouteEntry = currentRouteEntry;
	}

	public int getHunterTrainingMonths() {
		return hunterTrainingMonths;
	}

	public void setHunterTrainingMonths(int hunterTrainingMonths) {
		this.hunterTrainingMonths = hunterTrainingMonths;
	}

	public ShipModule getShipModule() {
		return shipModule;
	}

	public void setShipModule(ShipModule shipModule) {
		this.shipModule = shipModule;
	}

	public double getDistanceTravelled() {
		return distanceTravelled;
	}

	public void setDistanceTravelled(double distanceTravelled) {
		this.distanceTravelled = distanceTravelled;
	}

	public Fleet getFleet() {
		return fleet;
	}

	public void setFleet(Fleet fleet) {
		this.fleet = fleet;
	}

	public List<ShipRouteEntry> getRoute() {
		return route;
	}

	public void setRoute(List<ShipRouteEntry> route) {
		this.route = route;
	}

	public boolean isWasInCircle() {
		return wasInCircle;
	}

	public void setWasInCircle(boolean wasInCircle) {
		this.wasInCircle = wasInCircle;
	}

	public int getFlightFlag() {
		return flightFlag;
	}

	public void setFlightFlag(int flightFlag) {
		this.flightFlag = flightFlag;
	}

	public int getFlightQueue() {
		return flightQueue;
	}

	public void setFlightQueue(int flightQueue) {
		this.flightQueue = flightQueue;
	}

	public boolean isScannedByPlayer() {
		return scannedByPlayer;
	}

	public void setScannedByPlayer(boolean scannedByPlayer) {
		this.scannedByPlayer = scannedByPlayer;
	}

	public Player getPlayerBeforeCapture() {
		return playerBeforeCapture;
	}

	public void setPlayerBeforeCapture(Player playerBeforeCapture) {
		this.playerBeforeCapture = playerBeforeCapture;
	}

	public ShipCloakingSummary getCloakingSummary() {
		return cloakingSummary;
	}

	public void setCloakingSummary(ShipCloakingSummary cloakingSummary) {
		this.cloakingSummary = cloakingSummary;
	}

	public String createFullImagePath() {
		return shipTemplate.createFullImagePath();
	}

	public void receiveDamage(int damage, boolean ignoreShields, int mass) {
		receiveDamage(damage, 0, ignoreShields, mass);
	}

	public void receiveDamage(int damage, float damageToCrew, boolean ignoreShields, int mass) {
		if (damage <= 0) {
			return;
		}

		if (shield > 0 && !ignoreShields) {
			float factor = (ShipDamage.DAMAGE_MASS_FACTOR / mass) + 1;
			shield -= damage * factor;

			if (shield < 0) {
				shield = 0;
			}
		} else {
			float factor = (ShipDamage.DAMAGE_MASS_FACTOR / mass) + 1;
			this.damage += damage * (factor * factor) + 2;

			if (this.damage > 100) {
				this.damage = 100;
			} else if (damageToCrew > 0) {
				float crewDamage = damageToCrew * (factor * factor) + 2;
				crew -= Math.floor(shipTemplate.getCrew() * crewDamage / 100);

				if (crew < 0) {
					crew = 0;
				}
			}
		}
	}

	public void spendProjectile() {
		projectiles--;
	}

	public int retrieveShieldInfoHeight() {
		return (int) ((shield / 100f) * MAX_INFO_HEIGHT);
	}

	public int retrieveDamageInfoHeight() {
		return (int) (((100f - damage) / 100) * MAX_INFO_HEIGHT);
	}

	public int retrieveFuelInfoHeight() {
		float fuelCapacity = shipTemplate.getFuelCapacity();
		return (int) ((fuel / fuelCapacity) * MAX_INFO_HEIGHT);
	}

	public int retrieveCargoInfoHeight() {
		float storageSpace = retrieveStorageSpace();
		return (int) ((retrieveTotalGoods() / storageSpace) * MAX_INFO_HEIGHT);
	}

	public boolean hasActiveAbility(ShipAbilityType type) {
		return activeAbility != null && activeAbility.getType() == type;
	}

	public Optional<ShipAbility> getAbility(ShipAbilityType type) {
		return shipTemplate.getAbility(type);
	}

	public Optional<Float> getAbilityValue(ShipAbilityType type, String name) {
		return shipTemplate.getAbilityValue(type, name);
	}

	public String retrieveScannerRadiusStyle() {
		int size = scanRadius * 2;
		return "width: " + size + "px; height: " + size + "px; bottom: " + scanRadius + "px; right: " + scanRadius + "px; border-radius: " + scanRadius + "px;";
	}

	public String retrieveScannerRadiusStyle(int scanRadius) {
		int size = scanRadius * 2;
		return "width: " + size + "px; height: " + size + "px; bottom: " + scanRadius + "px; right: " + scanRadius + "px; border-radius: " + scanRadius + "px;";
	}

	public void resetTask() {
		taskType = ShipTaskType.NONE;
		taskValue = null;
		activeAbility = null;
	}

	public void resetTravel() {
		destinationShip = null;
		destinationX = -1;
		destinationY = -1;
		travelSpeed = 0;
	}

	public String retrieveColor(Long currentPlayerId) {
		if (signatureMaskActive(currentPlayerId)) {
			return taskValue;
		}

		return player.getColor();
	}

	public PlayerRelationType retrieveRelationType(Long currentPlayerId, Map<Long, PlayerRelationType> relationsMap, Map<String, Player> colorToPlayerMap) {
		if (signatureMaskActive(currentPlayerId)) {
			long maskedPlayerId = colorToPlayerMap.get(taskValue).getId();
			return relationsMap.get(maskedPlayerId);
		}

		return relationsMap.get(player.getId());
	}

	public Player retrievePlayer(Long currentPlayerId, Map<String, Player> colorToPlayerMap) {
		if (signatureMaskActive(currentPlayerId)) {
			return colorToPlayerMap.get(taskValue);
		}

		return player;
	}

	private boolean signatureMaskActive(Long currentPlayerId) {
		return currentPlayerId != null && currentPlayerId != player.getId() && hasActiveAbility(ShipAbilityType.SIGNATURE_MASK);
	}

	public int retrieveMassCategoryForLongRangeSensors() {
		if (propulsionSystemTemplate.getTechLevel() == 5) {
			return MasterDataService.RANDOM.nextInt(11);
		}

		int category = Math.round(shipTemplate.getMass() / 100f);

		if (category < 2) {
			return 2;
		}

		if (category > 8) {
			return 8;
		}

		return category;
	}

	public String retrieveGalaxyMapSizeStyle() {
		int cat = retrieveMassCategoryForLongRangeSensors();
		float size = cat + 3f;
		float halfSize = size / 2f;

		return "width: " + size + "px; height: " + size + "px; left: -" + halfSize + "px; top: -" + halfSize + "px; border-radius: " + size + "px;";
	}

	public int retrieveStorageSpace() {
		int storageSpace = shipTemplate.getStorageSpace();

		if (propulsionSystemTemplate.getTechLevel() == 10) {
			return storageSpace / 2;
		}

		return storageSpace;
	}

	public boolean destroyed() {
		return damage >= 100;
	}

	public int retrieveLastX() {
		if (lastX > 0 || lastY > 0) {
			return (int) getHalfWay().getX();
		}

		return 0;
	}

	public int retrieveLastY() {
		if (lastX > 0 || lastY > 0) {
			return (int) getHalfWay().getY();
		}

		return 0;
	}

	private Vector2D getHalfWay() {
		Vector2D current = new Vector2D(x, y);
		Vector2D last = new Vector2D(lastX, lastY);

		if (current.equals(last)) {
			return current;
		}

		double dist = current.distance(last);
		Vector2D dir = last.subtract(current);

		Vector2D newLast = current.add(dir.normalize().scalarMultiply(dist / 2));
		return newLast;
	}

	public int retrieveMaxProjectiles() {
		int possibleByMoney = projectiles + (money / 35);
		int possibleByMineral1 = projectiles + (mineral1 / 2);
		int possibleByMineral2 = projectiles + mineral2;
		int possibleByWeaponCount = MasterDataConstants.PROJECTILES_PER_WEAPON * shipTemplate.getProjectileWeaponsCount();

		if (shipModule == ShipModule.PROJECTILE_EXTENSION) {
			possibleByWeaponCount = Math.round(possibleByWeaponCount * 1.5f);
		}

		return Math.min(possibleByMoney, Math.min(possibleByMineral1, Math.min(possibleByMineral2, possibleByWeaponCount)));
	}

	public void updateRoute(ShipRouteEntry entry) {
		this.currentRouteEntry = entry;
		Planet destinationPlanet = entry.getPlanet();
		this.destinationX = destinationPlanet.getX();
		this.destinationY = destinationPlanet.getY();
		this.travelSpeed = routeTravelSpeed;
	}

	public boolean hasWeapons() {
		return shipTemplate.getEnergyWeaponsCount() > 0 || shipTemplate.getProjectileWeaponsCount() > 0 || shipTemplate.getHangarCapacity() > 0;
	}

	public boolean canTowShip(Ship s) {
		int massThatCanBeTowed = shipTemplate.getPropulsionSystemsCount() * propulsionSystemTemplate.getTechLevel() * 20;
		boolean samePos = s.getX() == x && s.getY() == y;
		int otherMass = s.getShipTemplate().getMass();
		return samePos && otherMass <= massThatCanBeTowed;
	}

	public int retrieveFuelPercentage() {
		return (int) Math.round(100.0 * ((float) fuel / shipTemplate.getFuelCapacity()));
	}

	public int retrieveProjectilePercentage() {
		if (shipTemplate.getProjectileWeaponsCount() > 0) {
			return (int) Math.round(100.0 * ((float) projectiles / (shipTemplate.getProjectileWeaponsCount() * MasterDataConstants.PROJECTILES_PER_WEAPON)));
		}

		return 0;
	}

	public void increaseExperienceBySpaceCombat(Ship enemyShip) {
		int techLevel = shipTemplate.getTechLevel();
		int techLevel2 = enemyShip.getShipTemplate().getTechLevel();
		int experienceFactor = techLevel >= techLevel2 ? 10 : 5;
		float techDiff = (((techLevel - techLevel2) * experienceFactor) + 50) / 100f;

		if (MasterDataService.RANDOM.nextFloat() >= techDiff) {
			experience++;
		}
	}

	public String retrieveExperienceIconClass() {
		return switch (experience) {
			case 1 -> "fas fa-angle-up";
			case 2 -> "fas fa-angle-double-up";
			case 3 -> "far fa-star";
			case 4 -> "fas fa-star";
			case 5 -> "fas fa-medal";
			default -> "";
		};

	}

	public Coordinate retrieveDestinationCoordinate() {
		return new CoordinateImpl(destinationX, destinationY, 0);
	}

	public void updateDestinationBasedOnDistance(int dist) {
		if (dist >= 0) {
			Vector2D pos = new Vector2D(x, y);

			if (destinationShip != null) {
				setDestinationX(destinationShip.getX());
				setDestinationY(destinationShip.getY());
			}

			Vector2D target = new Vector2D(destinationX, destinationY);

			if (!pos.equals(target)) {
				Vector2D direction = target.subtract(pos).normalize();
				Vector2D newTarget = pos.add(direction.scalarMultiply(dist));

				setDestinationX((int) newTarget.getX());
				setDestinationY((int) newTarget.getY());
			}
		}
	}

	private boolean notTutorialMission() {
		return player.getGame().notTutorialMission();
	}

	public boolean showNavigationTab() {
		return notTutorialMission() || player.getGame().getStoryModeMissionStep() >= SilverStarAGMission1.STEP_7_SEND_SHIP;
	}

	public boolean showRouteTab() {
		return notTutorialMission() || player.getGame().getStoryModeMissionStep() >= SilverStarAGMission1.STEP_11_SETUP_ROUTE;
	}

	public boolean showScannerTab() {
		return notTutorialMission() || player.getGame().getStoryModeMissionStep() >= SilverStarAGMission1.STEP_12_INVASION_START;
	}

	public boolean showRemainingTabs() {
		return notTutorialMission();
	}

	public boolean planetOwnedByPlayer() {
		return planet != null && planet.getPlayer() != null && planet.getPlayer().getId() == player.getId();
	}
}
