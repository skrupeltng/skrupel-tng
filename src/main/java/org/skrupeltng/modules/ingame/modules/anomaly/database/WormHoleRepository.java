package org.skrupeltng.modules.ingame.modules.anomaly.database;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface WormHoleRepository extends JpaRepository<WormHole, Long>, WormHoleRepositoryCustom {

	@Query("SELECT " +
			"	a " +
			"FROM " +
			"	WormHole a " +
			"WHERE " +
			"	a.connection IS NULL " +
			"	AND a.type = org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleType.JUMP_PORTAL " +
			"	AND a.ship.id = ?1")
	Optional<WormHole> findUnfinishedJumpPortal(long shipId);

	@Modifying
	@Query("DELETE FROM WormHole a WHERE a.ship.id = ?1")
	void deleteByShipId(long shipId);

	@Modifying
	@Query("DELETE FROM WormHole a WHERE a.ship.id IN (?1)")
	void deleteByShipIds(Collection<Long> shipIds);

	@Query("SELECT a FROM WormHole a WHERE a.game.id = ?1")
	List<WormHole> findByGameId(long gameId);

	@Modifying
	@Query("DELETE FROM WormHole a WHERE a.game.id = ?1")
	void deleteByGameId(long id);
}