package org.skrupeltng.modules.ingame.modules.overview.database;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ShockWaveDestructionLogEntryLogEntryRepository extends JpaRepository<ShockWaveDestructionLogEntry, Long> {

	@Modifying
	@Query("DELETE FROM ShockWaveDestructionLogEntry i WHERE i.newsEntry.id IN (SELECT n.id FROM NewsEntry n WHERE n.player.id = ?1)")
	void deleteByPlayerId(long playerId);

	@Modifying
	@Query("UPDATE ShockWaveDestructionLogEntry i SET i.shipId = null WHERE i.shipId = ?1")
	void clearShipId(long shipId);

	@Modifying
	@Query("UPDATE ShockWaveDestructionLogEntry i SET i.shipId = null WHERE i.shipId IN (?1)")
	void clearShipIds(Collection<Long> shipIds);
}
