package org.skrupeltng.modules.ingame.modules.planet.database;

import java.io.Serial;
import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import org.skrupeltng.modules.ingame.database.player.Player;

@Entity
@Table(name = "player_planet_scan_log")
public class PlayerPlanetScanLog implements Serializable {

	@Serial
	private static final long serialVersionUID = 3965983809933870765L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Player player;

	@ManyToOne(fetch = FetchType.LAZY)
	private Planet planet;

	@Column(name = "last_player_color")
	private String lastPlayerColor;

	@Column(name = "had_starbase")
	private boolean hadStarbase;

	@Column(name = "last_scan_round")
	private int lastScanRound;

	@Column(name = "scanned_temperature")
	private int scannedTemperature;

	@Column(name = "scanned_natives_count")
	private int scannedNativesCount;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "scanned_native_species_id")
	private NativeSpecies scannedNativeSpecies;

	@Column(name = "scanned_colonists")
	private int scannedColonists;

	@Column(name = "scanned_light_ground_units")
	private int scannedLightGroundUnits;

	@Column(name = "scanned_heavy_ground_units")
	private int scannedHeavyGroundUnits;

	@Column(name = "scanned_mines")
	private int scannedMines;

	@Column(name = "scanned_factories")
	private int scannedFactories;

	@Column(name = "scanned_planetary_defense")
	private int scannedPlanetaryDefense;

	@Column(name = "scanned_untapped_fuel")
	private int scannedUntappedFuel;

	@Column(name = "scanned_untapped_mineral1")
	private int scannedUntappedMineral1;

	@Column(name = "scanned_untapped_mineral2")
	private int scannedUntappedMineral2;

	@Column(name = "scanned_untapped_mineral3")
	private int scannedUntappedMineral3;

	@Column(name = "scanned_fuel")
	private int scannedFuel;

	@Column(name = "scanned_mineral1")
	private int scannedMineral1;

	@Column(name = "scanned_mineral2")
	private int scannedMineral2;

	@Column(name = "scanned_mineral3")
	private int scannedMineral3;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	public String getLastPlayerColor() {
		return lastPlayerColor;
	}

	public void setLastPlayerColor(String lastPlayerColor) {
		this.lastPlayerColor = lastPlayerColor;
	}

	public boolean isHadStarbase() {
		return hadStarbase;
	}

	public void setHadStarbase(boolean hadStarbase) {
		this.hadStarbase = hadStarbase;
	}

	public int getLastScanRound() {
		return lastScanRound;
	}

	public void setLastScanRound(int lastScanRound) {
		this.lastScanRound = lastScanRound;
	}

	public int getScannedTemperature() {
		return scannedTemperature;
	}

	public void setScannedTemperature(int scannedTemperature) {
		this.scannedTemperature = scannedTemperature;
	}

	public int getScannedNativesCount() {
		return scannedNativesCount;
	}

	public void setScannedNativesCount(int scannedNativesCount) {
		this.scannedNativesCount = scannedNativesCount;
	}

	public NativeSpecies getScannedNativeSpecies() {
		return scannedNativeSpecies;
	}

	public void setScannedNativeSpecies(NativeSpecies scannedNativeSpecies) {
		this.scannedNativeSpecies = scannedNativeSpecies;
	}

	public int getScannedColonists() {
		return scannedColonists;
	}

	public void setScannedColonists(int scannedColonists) {
		this.scannedColonists = scannedColonists;
	}

	public int getScannedLightGroundUnits() {
		return scannedLightGroundUnits;
	}

	public void setScannedLightGroundUnits(int scannedLightGroundUnits) {
		this.scannedLightGroundUnits = scannedLightGroundUnits;
	}

	public int getScannedHeavyGroundUnits() {
		return scannedHeavyGroundUnits;
	}

	public void setScannedHeavyGroundUnits(int scannedHeavyGroundUnits) {
		this.scannedHeavyGroundUnits = scannedHeavyGroundUnits;
	}

	public int getScannedMines() {
		return scannedMines;
	}

	public void setScannedMines(int scannedMines) {
		this.scannedMines = scannedMines;
	}

	public int getScannedFactories() {
		return scannedFactories;
	}

	public void setScannedFactories(int scannedFactories) {
		this.scannedFactories = scannedFactories;
	}

	public int getScannedPlanetaryDefense() {
		return scannedPlanetaryDefense;
	}

	public void setScannedPlanetaryDefense(int scannedPlanetaryDefense) {
		this.scannedPlanetaryDefense = scannedPlanetaryDefense;
	}

	public int getScannedUntappedFuel() {
		return scannedUntappedFuel;
	}

	public void setScannedUntappedFuel(int scannedUntappedFuel) {
		this.scannedUntappedFuel = scannedUntappedFuel;
	}

	public int getScannedUntappedMineral1() {
		return scannedUntappedMineral1;
	}

	public void setScannedUntappedMineral1(int scannedUntappedMineral1) {
		this.scannedUntappedMineral1 = scannedUntappedMineral1;
	}

	public int getScannedUntappedMineral2() {
		return scannedUntappedMineral2;
	}

	public void setScannedUntappedMineral2(int scannedUntappedMineral2) {
		this.scannedUntappedMineral2 = scannedUntappedMineral2;
	}

	public int getScannedUntappedMineral3() {
		return scannedUntappedMineral3;
	}

	public void setScannedUntappedMineral3(int scannedUntappedMineral3) {
		this.scannedUntappedMineral3 = scannedUntappedMineral3;
	}

	public int getScannedFuel() {
		return scannedFuel;
	}

	public void setScannedFuel(int scannedFuel) {
		this.scannedFuel = scannedFuel;
	}

	public int getScannedMineral1() {
		return scannedMineral1;
	}

	public void setScannedMineral1(int scannedMineral1) {
		this.scannedMineral1 = scannedMineral1;
	}

	public int getScannedMineral2() {
		return scannedMineral2;
	}

	public void setScannedMineral2(int scannedMineral2) {
		this.scannedMineral2 = scannedMineral2;
	}

	public int getScannedMineral3() {
		return scannedMineral3;
	}

	public void setScannedMineral3(int scannedMineral3) {
		this.scannedMineral3 = scannedMineral3;
	}

	@Override
	public String toString() {
		return "PlayerPlanetScanLog{" +
			"id=" + id +
			'}';
	}
}
