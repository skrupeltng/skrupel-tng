package org.skrupeltng.modules.ingame.modules.fleet.service;

import org.skrupeltng.modules.ingame.modules.fleet.controller.FleetResourceData;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class FleetResourceService {

	private final FleetRepository fleetRepository;
	private final ShipService shipService;
	private final FleetFuel fleetFuel;
	private final MessageSource messageSource;

	public FleetResourceService(FleetRepository fleetRepository, ShipService shipService, FleetFuel fleetFuel, MessageSource messageSource) {
		this.fleetRepository = fleetRepository;
		this.shipService = shipService;
		this.fleetFuel = fleetFuel;
		this.messageSource = messageSource;
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet')")
	public FleetResourceData getFleetResourceData(long fleetId) {
		Fleet fleet = fleetRepository.getReferenceById(fleetId);
		FleetResources fleetResources = new FleetResources(fleet, messageSource);
		return fleetResources.getFleetResourceData();
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet')")
	public List<FleetFuelGroup> getFuelGroups(long fleetId) {
		return fleetFuel.getFuelGroups(fleetId);
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(propagation = Propagation.REQUIRED)
	public FleetFuelGroup fillTanks(long fleetId, float percentage, int x, int y) {
		if (percentage < 0f || percentage > 1f) {
			throw new IllegalArgumentException("Invalid fuel percentage value for fleet!");
		}

		fleetFuel.fillTanks(fleetId, percentage, x, y);
		return fleetFuel.getGroupAtCoordinates(fleetId, x, y).orElseThrow();
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(propagation = Propagation.REQUIRED)
	public FleetFuelGroup equalizeTanks(long fleetId, int x, int y) {
		fleetFuel.equalizeTanks(fleetId, x, y);
		return fleetFuel.getGroupAtCoordinates(fleetId, x, y).orElseThrow();
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(propagation = Propagation.REQUIRED)
	public Set<Long> buildProjectiles(long fleetId) {
		Fleet fleet = fleetRepository.getReferenceById(fleetId);
		List<Ship> ships = fleet.getShips();

		Set<Long> planetIds = new HashSet<>();

		for (Ship ship : ships) {
			Planet planet = shipService.buildAllPossibleProjectilesAutomatically(ship.getId());

			if (planet != null && planet.getPlayer() == fleet.getPlayer()) {
				planetIds.add(planet.getId());
			}
		}

		return planetIds;
	}
}
