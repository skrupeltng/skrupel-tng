package org.skrupeltng.modules.ingame.modules.starbase.controller;

import java.io.Serializable;

public class StarbaseShipConstructionRequest implements Serializable {

	private static final long serialVersionUID = 5059411943616371765L;

	private String name;
	private String shipTemplateId;
	private String propulsionSystemId;
	private String energyWeaponId;
	private String projectileWeaponId;
	private String shipModule;
	private Long fleetId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShipTemplateId() {
		return shipTemplateId;
	}

	public void setShipTemplateId(String shipTemplateId) {
		this.shipTemplateId = shipTemplateId;
	}

	public String getPropulsionSystemId() {
		return propulsionSystemId;
	}

	public void setPropulsionSystemId(String propulsionSystemId) {
		this.propulsionSystemId = propulsionSystemId;
	}

	public String getEnergyWeaponId() {
		return energyWeaponId;
	}

	public void setEnergyWeaponId(String energyWeaponId) {
		this.energyWeaponId = energyWeaponId;
	}

	public String getProjectileWeaponId() {
		return projectileWeaponId;
	}

	public void setProjectileWeaponId(String projectileWeaponId) {
		this.projectileWeaponId = projectileWeaponId;
	}

	public String getShipModule() {
		return shipModule;
	}

	public void setShipModule(String shipModule) {
		this.shipModule = shipModule;
	}

	public Long getFleetId() {
		return fleetId;
	}

	public void setFleetId(Long fleetId) {
		this.fleetId = fleetId;
	}
}