package org.skrupeltng.modules.ingame.modules.fleet.database;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.io.Serial;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

@Entity
@Table(name = "fleet")
public class Fleet implements Serializable {

	@Serial
	private static final long serialVersionUID = 9100070696671938761L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String name;

	@ManyToOne(fetch = FetchType.LAZY)
	private Player player;

	@ManyToOne(fetch = FetchType.LAZY)
	private Ship leader;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "fleet")
	private List<Ship> ships;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Ship getLeader() {
		return leader;
	}

	public void setLeader(Ship leader) {
		this.leader = leader;
	}

	public List<Ship> getShips() {
		return ships;
	}

	public void setShips(List<Ship> ships) {
		this.ships = ships;
	}

	public List<Ship> retrieveSortedShips() {
		if (ships == null) {
			return Collections.emptyList();
		}

		ships.sort(SortFleetShipsComparator.INSTANCE);
		return ships;
	}
}
