package org.skrupeltng.modules.ingame.modules.ship.service;

import org.jetbrains.annotations.VisibleForTesting;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.GoodsContainer;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportItem;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportTarget;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class ShipTransporter {

	private final ShipTransportRequest request;
	private final Ship ship;
	private final GoodsContainer target;
	private final ShipTransportTarget transportTarget;

	private boolean isForeignPlanet;
	private Planet targetPlanet;

	public ShipTransporter(ShipTransportRequest request, Ship ship, GoodsContainer target) {
		this.request = request;
		this.ship = ship;
		this.target = target;

		if (target instanceof Ship targetShip) {
			transportTarget = new ShipTransportTarget(ship, targetShip);
		} else if (target instanceof Planet planet) {
			transportTarget = new ShipTransportTarget(ship, planet);
		} else {
			throw new IllegalArgumentException("Invalid target: %s".formatted(target.toString()));
		}

		if (target instanceof Planet planet) {
			targetPlanet = planet;
			isForeignPlanet = planet.getPlayer() == null || planet.getPlayer().getId() != ship.getPlayer().getId();

			if (isForeignPlanet) {
				targetPlanet.setNewPlayer(ship.getPlayer());
			}
		}
	}

	public void performTransport() {
		validateInput();

		for (ShipTransportItem item : transportTarget.getItems()) {
			performTransport(item);
		}
	}

	private void validateInput() {
		for (ShipTransportItem item : transportTarget.getItems()) {
			int value = getRequestValueOfItem(item);

			validateItem(item, value);
		}
	}

	@VisibleForTesting
	protected static void validateItem(ShipTransportItem item, int value) {
		ShipTransportResource fieldName = item.getResource();

		if (value < 0) {
			throw new IllegalArgumentException("Negative value found for %s".formatted(fieldName));
		}
		if (value < item.getLeftMin()) {
			throw new IllegalArgumentException("Value %d lower than min value %d for ship for %s".formatted(value, item.getLeftMin(), fieldName));
		}
		if (value > item.getLeftMax()) {
			throw new IllegalArgumentException("Value %d higher than max value %d for ship for %s".formatted(value, item.getLeftMax(), fieldName));
		}
	}

	private int getRequestValueOfItem(ShipTransportItem item) {
		return switch (item.getResource()) {
			case colonists -> request.getColonists();
			case fuel -> request.getFuel();
			case money -> request.getMoney();
			case supplies -> request.getSupplies();
			case mineral1 -> request.getMineral1();
			case mineral2 -> request.getMineral2();
			case mineral3 -> request.getMineral3();
			case lightgroundunits -> request.getLightGroundUnits();
			case heavygroundunits -> request.getHeavyGroundUnits();
		};
	}

	private void performTransport(ShipTransportItem item) {
		final Consumer<Integer> shipConsumer;
		final Supplier<Integer> requestSupplier;

		final Consumer<Integer> targetConsumer;

		switch (item.getResource()) {
			case colonists -> {
				shipConsumer = ship::setColonists;
				requestSupplier = request::getColonists;

				if (isForeignPlanet) {
					targetConsumer = targetPlanet::setNewColonists;
				} else {
					targetConsumer = target::setColonists;
				}
			}
			case fuel -> {
				shipConsumer = ship::setFuel;
				requestSupplier = request::getFuel;
				targetConsumer = target::setFuel;
			}
			case money -> {
				shipConsumer = ship::setMoney;
				requestSupplier = request::getMoney;
				targetConsumer = target::setMoney;
			}
			case supplies -> {
				shipConsumer = ship::setSupplies;
				requestSupplier = request::getSupplies;
				targetConsumer = target::setSupplies;
			}
			case mineral1 -> {
				shipConsumer = ship::setMineral1;
				requestSupplier = request::getMineral1;
				targetConsumer = target::setMineral1;
			}
			case mineral2 -> {
				shipConsumer = ship::setMineral2;
				requestSupplier = request::getMineral2;
				targetConsumer = target::setMineral2;
			}
			case mineral3 -> {
				shipConsumer = ship::setMineral3;
				requestSupplier = request::getMineral3;
				targetConsumer = target::setMineral3;
			}
			case lightgroundunits -> {
				shipConsumer = ship::setLightGroundUnits;
				requestSupplier = request::getLightGroundUnits;

				if (isForeignPlanet) {
					targetConsumer = targetPlanet::setNewLightGroundUnits;
				} else {
					targetConsumer = target::setLightGroundUnits;
				}
			}
			case heavygroundunits -> {
				shipConsumer = ship::setHeavyGroundUnits;
				requestSupplier = request::getHeavyGroundUnits;

				if (isForeignPlanet) {
					targetConsumer = targetPlanet::setNewHeavyGroundUnits;
				} else {
					targetConsumer = target::setHeavyGroundUnits;
				}
			}
			default -> throw new IllegalArgumentException("Unknown resource type: %s".formatted(item.getResource()));
		}

		Integer value = requestSupplier.get();

		shipConsumer.accept(value);
		targetConsumer.accept(item.getLeftValue() + item.getRightValue() - value);
	}
}
