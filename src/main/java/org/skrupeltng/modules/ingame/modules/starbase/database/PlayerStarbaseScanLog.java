package org.skrupeltng.modules.ingame.modules.starbase.database;

import org.skrupeltng.modules.ingame.database.player.Player;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "player_starbase_scan_log")
public class PlayerStarbaseScanLog {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Player player;

	@ManyToOne(fetch = FetchType.LAZY)
	private Starbase starbase;

	@Column(name = "last_scan_round")
	private int lastScanRound;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Starbase getStarbase() {
		return starbase;
	}

	public void setStarbase(Starbase starbase) {
		this.starbase = starbase;
	}

	public int getLastScanRound() {
		return lastScanRound;
	}

	public void setLastScanRound(int lastScanRound) {
		this.lastScanRound = lastScanRound;
	}
}
