package org.skrupeltng.modules.ingame.modules.fleet.service;

import org.skrupeltng.modules.ingame.modules.fleet.controller.FleetResourceData;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.service.MasterDataConstants;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class FleetResources {

	private final Fleet fleet;
	private final MessageSource messageSource;

	private float totalShipTemplateFuel = 0;
	private float totalShipFuel = 0;
	private float totalProjectiles = 0;
	private float totalProjectilesOnShips = 0;
	private float buildableProjectiles = 0;
	private float totalAggressiveness = 0;
	private int lastAggressiveness = -1;
	private boolean hasDifferentAggressivenesses = false;

	private final Map<Planet, Set<Ship>> planetsMap = new HashMap<>();
	private final List<Ship> shipsWithoutPlanet = new ArrayList<>();
	private final Set<String> availableSpecialAbilities = new HashSet<>();

	private final FleetResourceData data = new FleetResourceData();

	public FleetResources(Fleet fleet, MessageSource messageSource) {
		this.fleet = fleet;
		this.messageSource = messageSource;
	}

	public FleetResourceData getFleetResourceData() {
		data.setShipCount(fleet.getShips().size());

		initCollections();

		Locale locale = LocaleContextHolder.getLocale();
		String abilities = availableSpecialAbilities.stream()
			.map(a -> messageSource.getMessage("ship_ability_" + a, null, locale))
			.sorted()
			.collect(Collectors.joining(", "));
		data.setAvailableSpecialAbilities(abilities);

		aggregateShipsOnPlanets();
		aggregateShipsWithoutPlanets();

		if (totalShipTemplateFuel > 0) {
			data.setCurrentTankPercentage(roundPercentage(totalShipFuel / totalShipTemplateFuel));
		}

		if (totalProjectiles > 0) {
			data.setCurrentProjectilePercentage(roundPercentage(totalProjectilesOnShips / totalProjectiles));
			data.setPossibleProjectilePercentage(roundPercentage((totalProjectilesOnShips + buildableProjectiles) / totalProjectiles));
		}

		data.setAverageAggressiveness((int) (totalAggressiveness / fleet.getShips().size()));
		data.setHasDifferentAggressivenesses(hasDifferentAggressivenesses);

		return data;
	}

	private void initCollections() {
		int weaponCount = 0;

		for (Ship ship : fleet.getShips()) {
			ShipTemplate shipTemplate = ship.getShipTemplate();
			weaponCount += shipTemplate.getEnergyWeaponsCount();
			weaponCount += shipTemplate.getProjectileWeaponsCount();
			weaponCount += shipTemplate.getHangarCapacity();

			totalAggressiveness += ship.getAggressiveness();

			if (lastAggressiveness == -1) {
				lastAggressiveness = ship.getAggressiveness();
			} else if (lastAggressiveness != ship.getAggressiveness()) {
				hasDifferentAggressivenesses = true;
			}

			availableSpecialAbilities.addAll(shipTemplate.getShipAbilities().stream().map(sa -> sa.getType().name()).collect(Collectors.toSet()));

			if (ship.getPlanet() != null) {
				planetsMap.computeIfAbsent(ship.getPlanet(), k -> new HashSet<>()).add(ship);
			} else {
				shipsWithoutPlanet.add(ship);
			}
		}

		data.setWeaponCount(weaponCount);
	}

	private void aggregateShipsOnPlanets() {
		long playerId = fleet.getPlayer().getId();

		for (Planet planet : planetsMap.keySet()) {
			Set<Ship> shipsList = planetsMap.get(planet);

			int planetFuel = planet.retrieveTransportableFuel(playerId);
			int planetMoney = planet.retrieveTransportableMoney(playerId);
			int planetMineral1 = planet.retrieveTransportableMineral1(playerId);
			int planetMineral2 = planet.retrieveTransportableMineral2(playerId);

			for (Ship ship : shipsList) {
				ShipTemplate shipTemplate = ship.getShipTemplate();

				totalShipTemplateFuel += shipTemplate.getFuelCapacity();
				totalShipFuel += ship.getFuel();

				totalProjectiles += shipTemplate.getProjectileWeaponsCount() * MasterDataConstants.PROJECTILES_PER_WEAPON;
				totalProjectilesOnShips += ship.getProjectiles();

				int newFuel = Math.min(shipTemplate.getFuelCapacity() - ship.getFuel(), planetFuel);
				planetFuel -= newFuel;

				int currentProjectiles = ship.getProjectiles();
				int maxProjectiles = ship.getShipTemplate().getProjectileWeaponsCount() * MasterDataConstants.PROJECTILES_PER_WEAPON;

				int money = planetMoney + ship.getMoney();
				int mineral1 = planetMineral1 + ship.getMineral1();
				int mineral2 = planetMineral2 + ship.getMineral2();

				int newProjectiles = maxProjectiles - currentProjectiles;
				newProjectiles = Math.min(newProjectiles, Math.min(money / 35, Math.min(mineral1 / 2, mineral2)));

				buildableProjectiles += newProjectiles;
				planetMoney -= newProjectiles * 35;
				planetMineral1 -= newProjectiles * 2;
				planetMineral2 -= newProjectiles;
			}
		}
	}

	private void aggregateShipsWithoutPlanets() {
		for (Ship ship : shipsWithoutPlanet) {
			ShipTemplate shipTemplate = ship.getShipTemplate();

			totalShipTemplateFuel += shipTemplate.getFuelCapacity();
			totalShipFuel += ship.getFuel();

			totalProjectiles += shipTemplate.getProjectileWeaponsCount() * MasterDataConstants.PROJECTILES_PER_WEAPON;
			totalProjectilesOnShips += ship.getProjectiles();
		}
	}

	public static int roundPercentage(float percentage) {
		return (int) Math.floor(percentage * 100f);
	}
}
