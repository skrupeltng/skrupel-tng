package org.skrupeltng.modules.ingame.modules.overview.database;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import org.skrupeltng.modules.masterdata.database.ShipTemplate;

@Entity
@Table(name = "shock_wave_destruction_log_entry")
public class ShockWaveDestructionLogEntry implements Serializable {

	private static final long serialVersionUID = -8981104559269335028L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "news_entry_id")
	private NewsEntry newsEntry;

	@Column(name = "ship_name")
	private String shipName;

	@Column(name = "ship_id")
	private Long shipId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ship_template_id")
	private ShipTemplate shipTemplate;

	private int damage;

	private boolean destroyed;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public NewsEntry getNewsEntry() {
		return newsEntry;
	}

	public void setNewsEntry(NewsEntry newsEntry) {
		this.newsEntry = newsEntry;
	}

	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public Long getShipId() {
		return shipId;
	}

	public void setShipId(Long shipId) {
		this.shipId = shipId;
	}

	public ShipTemplate getShipTemplate() {
		return shipTemplate;
	}

	public void setShipTemplate(ShipTemplate shipTemplate) {
		this.shipTemplate = shipTemplate;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public boolean isDestroyed() {
		return destroyed;
	}

	public void setDestroyed(boolean destroyed) {
		this.destroyed = destroyed;
	}
}
