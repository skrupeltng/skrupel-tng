package org.skrupeltng.modules.ingame.modules.ship.service;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.controller.ShipListResultDTO;
import org.skrupeltng.modules.ingame.controller.ShipOverviewRequest;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipCloakingSummary;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTacticsChangeRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntry;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTactics;
import org.skrupeltng.modules.ingame.modules.starbase.database.AquiredShipTemplate;
import org.skrupeltng.modules.ingame.modules.starbase.database.AquiredShipTemplateRepository;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.service.MasterDataConstants;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class ShipService {

	private final ShipRepository shipRepository;
	private final PlanetRepository planetRepository;
	private final AquiredShipTemplateRepository aquiredShipTemplateRepository;
	private final MasterDataService masterDataService;
	private final VisibleObjects visibleObjects;
	private final ConfigProperties configProperties;
	private final MessageSource messageSource;

	public ShipService(ShipRepository shipRepository, PlanetRepository planetRepository, AquiredShipTemplateRepository aquiredShipTemplateRepository, MasterDataService masterDataService, VisibleObjects visibleObjects, ConfigProperties configProperties, MessageSource messageSource) {
		this.shipRepository = shipRepository;
		this.planetRepository = planetRepository;
		this.aquiredShipTemplateRepository = aquiredShipTemplateRepository;
		this.masterDataService = masterDataService;
		this.visibleObjects = visibleObjects;
		this.configProperties = configProperties;
		this.messageSource = messageSource;
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public Ship getShip(long shipId) {
		return shipRepository.getReferenceById(shipId);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public Ship getShipWithRoutesFetched(long shipId) {
		Ship ship = getShip(shipId);

		List<ShipRouteEntry> route = ship.getRoute();

		if (route != null) {
			for (ShipRouteEntry entry : route) {
				//noinspection ResultOfMethodCallIgnored
				entry.getPlanet();
			}

			route.sort(Comparator.comparing(ShipRouteEntry::getOrderId));
		}

		return ship;
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public List<Ship> getShipsOfLogin(long gameId, long loginId, Integer x, Integer y) {
		if (x == null || y == null) {
			return shipRepository.findShipsByGameIdAndLoginId(gameId, loginId);
		}

		if (configProperties.isDisablePermissionChecks()) {
			return shipRepository.findShipsByGameIdAndCoordinates(gameId, x, y);
		}

		return shipRepository.findShipsByGameIdAndLoginIdAndCoordinates(gameId, loginId, x, y);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public Page<ShipListResultDTO> getShipsOfLogin(long gameId, ShipOverviewRequest request, long loginId) {
		return shipRepository.searchShips(request, loginId);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public String getDestinationName(long shipId) {
		Ship ship = shipRepository.getReferenceById(shipId);

		Ship destinationShip = ship.getDestinationShip();

		if (destinationShip != null) {
			long playerId = ship.getPlayer().getId();

			if (destinationShip.getPlayer().getId() == playerId ||
				shipRepository.getShipIdsInScannerRangeOfPlayer(playerId).contains(destinationShip.getId())) {
				return destinationShip.getName();
			}

			return messageSource.getMessage("alien_ship", null, "Alien ship", LocaleContextHolder.getLocale());
		}

		int x = ship.getDestinationX();
		int y = ship.getDestinationY();

		if (x < 0 || y < 0) {
			return null;
		}

		long gameId = ship.getPlayer().getGame().getId();

		Optional<Planet> planetOpt = planetRepository.findByGameIdAndXAndY(gameId, x, y);

		if (planetOpt.isPresent()) {
			return planetOpt.get().getName();
		}

		return messageSource.getMessage("empty_space", null, LocaleContextHolder.getLocale());
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public List<Ship> getScannedShips(long shipId) {
		Ship ship = shipRepository.getReferenceById(shipId);
		Player player = ship.getPlayer();
		long playerId = player.getId();
		long gameId = player.getGame().getId();
		int scannerDist = ship.getScanRadius();

		List<Ship> ships = shipRepository.findByGameId(gameId);
		return ships.stream().filter(s -> s.getPlayer().getId() != playerId && s.getDistance(ship) <= scannerDist).toList();
	}

	public Ship getScannedShip(long playerId, long scannedShipId) {
		if (shipRepository.getShipIdsInScannerRangeOfPlayer(playerId).contains(scannedShipId)) {
			return shipRepository.getReferenceById(scannedShipId);
		}

		throw new IllegalArgumentException("Ship " + scannedShipId + " is not in scanner reach of any ship of player " + playerId + "!");
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public String getLogbook(long shipId) {
		Ship ship = shipRepository.getReferenceById(shipId);
		return ship.getLog();
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void changeLogbook(long shipId, String logbook) {
		Ship ship = shipRepository.getReferenceById(shipId);
		ship.setLog(logbook);
		shipRepository.save(ship);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void changeTactics(long shipId, ShipTacticsChangeRequest request) {
		if (request.getAggressiveness() < 0 || request.getAggressiveness() > 100) {
			throw new IllegalArgumentException("Invalid aggressiveness value!");
		}

		Ship ship = shipRepository.getReferenceById(shipId);
		ship.setAggressiveness(request.getAggressiveness());

		ShipTactics tactics = ShipTactics.valueOf(request.getTactics());
		ship.setTactics(tactics);

		shipRepository.save(ship);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void buildProjectiles(long shipId, int quantity) {
		buildProjectilesWithoutPermissionCheck(shipId, quantity);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void buildProjectilesWithoutPermissionCheck(long shipId, int quantity) {
		if (quantity <= 0) {
			throw new IllegalArgumentException("Quantity must be higher than zero!");
		}

		Ship ship = shipRepository.getReferenceById(shipId);
		ship.setProjectiles(ship.getProjectiles() + quantity);
		ship.setMoney(ship.getMoney() - (35 * quantity));
		ship.setMineral1(ship.getMineral1() - (2 * quantity));
		ship.setMineral2(ship.getMineral2() - quantity);
		shipRepository.save(ship);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	public int getBuildableProjectiles(long shipId) {
		Ship ship = shipRepository.getReferenceById(shipId);
		return getBuildableProjectiles(ship);
	}

	private int getBuildableProjectiles(Ship ship) {
		int currentProjectiles = ship.getProjectiles();
		int maxProjectiles = ship.getShipTemplate().getProjectileWeaponsCount() * MasterDataConstants.PROJECTILES_PER_WEAPON;

		if (currentProjectiles == maxProjectiles) {
			return 0;
		}

		Planet planet = ship.getPlanet();

		if (planet == null) {
			return 0;
		}

		long playerId = ship.getPlayer().getId();

		int totalMoney = planet.retrieveTransportableMoney(playerId) + ship.getMoney();
		int totalMineral1 = planet.retrieveTransportableMineral1(playerId) + ship.getMineral1();
		int totalMineral2 = planet.retrieveTransportableMineral2(playerId) + ship.getMineral2();

		int newProjectiles = maxProjectiles - currentProjectiles;
		newProjectiles = Math.min(newProjectiles, Math.min(totalMoney / 35, Math.min(totalMineral1 / 2, totalMineral2)));
		return newProjectiles;
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(propagation = Propagation.REQUIRED)
	public Planet buildAllPossibleProjectilesAutomatically(long shipId) {
		Ship ship = shipRepository.getReferenceById(shipId);
		return buildAllPossibleProjectilesAutomaticallyWithoutPermissionChecks(ship);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Planet buildAllPossibleProjectilesAutomaticallyWithoutPermissionChecks(Ship ship) {
		Planet planet = ship.getPlanet();

		if (planet == null) {
			return null;
		}

		int newProjectiles = getBuildableProjectiles(ship);
		ship.addProjectiles(newProjectiles);

		int moneyCosts = newProjectiles * 35;
		int mineral1Costs = newProjectiles * 2;
		int mineral2Costs = newProjectiles;

		if (ship.getMoney() >= moneyCosts) {
			ship.setMoney(ship.getMoney() - moneyCosts);
		} else {
			moneyCosts -= ship.getMoney();
			ship.setMoney(0);
			planet.spendMoney(moneyCosts);
		}

		if (ship.getMineral1() >= mineral1Costs) {
			ship.setMineral1(ship.getMineral1() - mineral1Costs);
		} else {
			mineral1Costs -= ship.getMineral1();
			ship.setMineral1(0);
			planet.spendMineral1(mineral1Costs);
		}

		if (ship.getMineral2() >= mineral2Costs) {
			ship.setMineral2(ship.getMineral2() - mineral2Costs);
		} else {
			mineral2Costs -= ship.getMineral2();
			ship.setMineral2(0);
			planet.spendMineral2(mineral2Costs);
		}

		shipRepository.save(ship);
		return planetRepository.save(planet);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void toggleAutoBuildProjectiles(long shipId) {
		Ship ship = shipRepository.getReferenceById(shipId);
		ship.setAutoBuildProjectiles(!ship.isAutoBuildProjectiles());
		shipRepository.save(ship);
	}


	@Transactional(propagation = Propagation.REQUIRED)
	public void checkStructureScanner(Player player, Ship otherShip) {
		ShipTemplate shipTemplate = otherShip.getShipTemplate();
		Game game = player.getGame();

		Optional<AquiredShipTemplate> resultOpt = aquiredShipTemplateRepository.findByGameIdAndShipTemplateId(game.getId(), shipTemplate.getId());

		if (resultOpt.isEmpty() && shipTemplate.getFaction() != player.getFaction()) {
			AquiredShipTemplate aquiredShipTemplate = new AquiredShipTemplate();
			aquiredShipTemplate.setGame(game);
			aquiredShipTemplate.setShipTemplate(shipTemplate);
			aquiredShipTemplateRepository.save(aquiredShipTemplate);
		}
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public List<ShipTemplate> getBuiltShipTemplates(long gameId, long loginId) {
		ShipTemplateNameI18NComparator comparator = new ShipTemplateNameI18NComparator(masterDataService);
		return shipRepository.getBuiltShipTemplates(gameId, loginId).stream().sorted(comparator).toList();
	}

	public ShipCloakingSummary getCloakingSummary(Ship ship) {
		Player currentPlayer = ship.getPlayer();
		List<Player> players = currentPlayer.getGame().getPlayers();

		List<String> visibleToPlayersList = new ArrayList<>(players.size() - 1);

		for (Player player : players) {
			if (player == currentPlayer) {
				continue;
			}

			if (cloakedShipIsVisibleToPlayer(player, ship)) {
				visibleToPlayersList.add(player.retrieveDisplayNameWithColor(messageSource));
			}
		}

		return new ShipCloakingSummary(visibleToPlayersList);
	}

	private boolean cloakedShipIsVisibleToPlayer(Player player, Ship cloakedShip) {
		if (shipRepository.getShipIdsInScannerRangeOfPlayer(player.getId()).contains(cloakedShip.getId())) {
			return true;
		}

		Set<CoordinateImpl> visibilityCoordinates = visibleObjects.getVisibilityCoordinates(player.getId());
		return visibilityCoordinates.stream().anyMatch(p -> p.getDistance(cloakedShip) <= p.getScanRadius());
	}

	public List<Ship> getOwnShipsOnPosition(long gameId, long playerId, int x, int y, int distance) {
		List<Long> shipIds = shipRepository.getShipIdsInRadius(gameId, x, y, distance);

		return shipRepository.findByIds(shipIds).stream()
			.filter(s -> s.getPlayer().getId() == playerId)
			.toList();
	}
}
