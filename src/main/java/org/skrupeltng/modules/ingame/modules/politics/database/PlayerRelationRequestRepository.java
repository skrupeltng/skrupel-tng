package org.skrupeltng.modules.ingame.modules.politics.database;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface PlayerRelationRequestRepository extends JpaRepository<PlayerRelationRequest, Long> {

	@Query("SELECT " +
			"	pr " +
			"FROM " +
			"	PlayerRelationRequest pr " +
			"	INNER JOIN pr.requestingPlayer p1 " +
			"	INNER JOIN pr.otherPlayer p2 " +
			"WHERE " +
			"	(p1.id = ?1 OR p2.id = ?1) " +
			"	AND (p1.id = ?2 OR p2.id = ?2)")
	Optional<PlayerRelationRequest> findByPlayerIds(long playerId1, long playerId2);

	@Modifying
	@Query("DELETE FROM PlayerRelationRequest r WHERE (r.requestingPlayer.id = ?1 OR r.otherPlayer.id = ?1)")
	void deleteByPlayerId(long playerId);

	@Query("SELECT r FROM PlayerRelationRequest r WHERE r.otherPlayer.id = ?1")
	List<PlayerRelationRequest> findIncomingRequests(long playerId);

	@Query("SELECT r.otherPlayer.id FROM PlayerRelationRequest r WHERE r.requestingPlayer.id = ?1")
	Set<Long> findOutgoingRequests(long playerId);
}