package org.skrupeltng.modules.ingame.modules.starbase.controller;

import java.io.Serializable;

public class ShipTemplateRequirementsDTO implements Serializable {

	private static final long serialVersionUID = 2900081984846629858L;

	private int propulsionSystems;
	private int energyWeapons;
	private int projectileWeapons;

	public int getPropulsionSystems() {
		return propulsionSystems;
	}

	public void setPropulsionSystems(int propulsionSystems) {
		this.propulsionSystems = propulsionSystems;
	}

	public int getEnergyWeapons() {
		return energyWeapons;
	}

	public void setEnergyWeapons(int energyWeapons) {
		this.energyWeapons = energyWeapons;
	}

	public int getProjectileWeapons() {
		return projectileWeapons;
	}

	public void setProjectileWeapons(int projectileWeapons) {
		this.projectileWeapons = projectileWeapons;
	}
}