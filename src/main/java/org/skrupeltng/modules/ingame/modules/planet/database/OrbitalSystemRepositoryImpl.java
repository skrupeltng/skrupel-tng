package org.skrupeltng.modules.ingame.modules.planet.database;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.skrupeltng.modules.RepositoryCustomBase;

public class OrbitalSystemRepositoryImpl extends RepositoryCustomBase implements OrbitalSystemRepositoryCustom {

	@Override
	public boolean loginOwnsOrbitalSystem(long orbitalSystemId, long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	p.login_id \n" +
				"FROM \n" +
				"	orbital_system o \n" +
				"	INNER JOIN planet pl \n" +
				"		ON pl.id = o.planet_id \n" +
				"	INNER JOIN player p \n" +
				"		ON p.id = pl.player_id \n" +
				"WHERE \n" +
				"	o.id = :orbitalSystemId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("orbitalSystemId", orbitalSystemId);

		List<Long> results = jdbcTemplate.queryForList(sql, params, Long.class);
		return results.size() == 1 && results.get(0).longValue() == loginId;
	}
}