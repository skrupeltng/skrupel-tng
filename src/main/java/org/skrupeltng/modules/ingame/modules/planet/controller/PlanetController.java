package org.skrupeltng.modules.ingame.modules.planet.controller;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.service.PlanetService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.ingame.service.round.PoliticsRoundCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/ingame/planet")
public class PlanetController extends AbstractController {

	private static final Logger log = LoggerFactory.getLogger(PlanetController.class);

	private static final String PLANET = "planet";

	private final PlanetService planetService;
	private final PoliticsRoundCalculator politicsRoundCalculator;

	public PlanetController(PlanetService planetService, PoliticsRoundCalculator politicsRoundCalculator) {
		this.planetService = planetService;
		this.politicsRoundCalculator = politicsRoundCalculator;
	}

	@GetMapping("")
	public String getPlanet(@RequestParam long planetId, @RequestParam(required = false) String subPage, Model model) {
		try {
			Planet planet = planetService.getPlanet(planetId);
			model.addAttribute(PLANET, planet);

			boolean turnDone = turnDoneForPlanet(planetId);
			model.addAttribute("turnDone", turnDone && !configProperties.isDisablePermissionChecks());

			Map<Long, Float> tradeBonusData = politicsRoundCalculator.getTradeBonusData(planet.getGame().getId());
			model.addAttribute("tradeBonusData", tradeBonusData);

			if (!turnDone) {
				if (StringUtils.isBlank(subPage)) {
					subPage = "buildings";
				}

				model.addAttribute("subPage", subPage);
				String subPageFragment = null;

				switch (subPage) {
					case "buildings" -> subPageFragment = getBuildings(planetId, model);
					case "orbital-systems" -> subPageFragment = getOrbitalSystems(planetId, model);
					case "ships-in-orbit" -> subPageFragment = getShipsInOrbit(planetId, model);
					case "native-species" -> subPageFragment = getNativeSpecies(planetId, model);
					case "starbase-construction" -> subPageFragment = getStarbaseConstruction(planetId, model);
					case "logbook" -> subPageFragment = getLogbook(planetId, model);
					default -> log.error("Unexpected value for sub page parameter!");
				}

				model.addAttribute("subPageFragment", subPageFragment);
			}

			return "ingame/planet/planet::content";
		} catch (AccessDeniedException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found");
		}
	}

	@GetMapping("/buildings")
	public String getBuildings(@RequestParam long planetId, Model model) {
		Planet planet = planetService.getPlanet(planetId);
		model.addAttribute(PLANET, planet);
		model.addAttribute("maxMines", planet.retrieveMaxMines());
		model.addAttribute("maxFactories", planet.retrieveMaxFactories());
		model.addAttribute("maxPlanetaryDefense", planet.retrieveMaxPlanetaryDefense());

		int fuelForNextRound = 0;
		int mineral1ForNextRound = 0;
		int mineral2ForNextRound = 0;
		int mineral3ForNextRound = 0;

		if (planet.getMines() > 0 && planet.retrieveTotalUntappedResources() > 0) {
			fuelForNextRound = planet.retrieveFuelToBeMined();
			mineral1ForNextRound = planet.retrieveMineral1ToBeMined();
			mineral2ForNextRound = planet.retrieveMineral2ToBeMined();
			mineral3ForNextRound = planet.retrieveMineral3ToBeMined();
		}

		model.addAttribute("fuelForNextRound", fuelForNextRound);
		model.addAttribute("mineral1ForNextRound", mineral1ForNextRound);
		model.addAttribute("mineral2ForNextRound", mineral2ForNextRound);
		model.addAttribute("mineral3ForNextRound", mineral3ForNextRound);

		return "ingame/planet/buildings::content";
	}

	@PostMapping("/mines")
	@ResponseBody
	public void buildMines(@RequestParam long planetId, @RequestParam int quantity) {
		planetService.buildMines(planetId, quantity);
	}

	@PostMapping("/factories")
	@ResponseBody
	public void buildFactories(@RequestParam long planetId, @RequestParam int quantity) {
		planetService.buildFactories(planetId, quantity);
	}

	@PostMapping("/sell-supplies")
	@ResponseBody
	public void sellSupplies(@RequestParam long planetId, @RequestParam int quantity) {
		planetService.sellSupplies(planetId, quantity);
	}

	@PostMapping("/planetary-defense")
	@ResponseBody
	public void buildPlanetaryDefense(@RequestParam long planetId, @RequestParam int quantity) {
		planetService.buildPlanetaryDefense(planetId, quantity);
	}

	@PostMapping("/automated-mines")
	@ResponseBody
	public void toggleAutoBuildMines(@RequestParam long planetId) {
		planetService.toggleAutoBuildMines(planetId);
	}

	@PostMapping("/automated-factories")
	@ResponseBody
	public void toggleAutoBuildFactories(@RequestParam long planetId) {
		planetService.toggleAutoBuildFactories(planetId);
	}

	@PostMapping("/automated-sell-supplies")
	@ResponseBody
	public void toggleAutoSellSupplies(@RequestParam long planetId) {
		planetService.toggleAutoSellSupplies(planetId);
	}

	@PostMapping("/automated-planetary-defense")
	@ResponseBody
	public void toggleAutoBuildPlanetaryDefense(@RequestParam long planetId) {
		planetService.toggleAutoBuildPlanetaryDefense(planetId);
	}

	@GetMapping("/starbase-construction")
	public String getStarbaseConstruction(@RequestParam long planetId, Model model) {
		Planet planet = planetService.getPlanet(planetId);

		if (planet.getStarbaseUnderConstructionType() == null) {
			model.addAttribute(PLANET, planet);
			model.addAttribute("starbaseTypes", StarbaseType.values());

			return "ingame/planet/starbase-construction::content";
		}

		return "ingame/planet/starbase-construction::under-construction";
	}

	@GetMapping("/starbase-construction-name-select")
	public String constructStarbase(@RequestParam String type, Model model) {
		model.addAttribute("type", type);
		return "ingame/planet/starbase-construction::name-select";
	}

	@PostMapping("/starbase")
	public String constructStarbase(@RequestParam long planetId, @RequestParam String name, @RequestParam String type) {
		planetService.constructStarbase(planetId, StarbaseType.valueOf(type), name);
		return "ingame/planet/starbase-construction::order-successfull";
	}

	@GetMapping("/logbook")
	public String getLogbook(@RequestParam long planetId, Model model) {
		model.addAttribute("logbook", planetService.getLogbook(planetId));
		return "ingame/logbook::content";
	}

	@PostMapping("/logbook")
	public String changeLogbook(@RequestParam long planetId, @RequestParam String logbook) {
		planetService.changeLogbook(planetId, logbook);
		return "ingame/logbook::success";
	}

	@GetMapping("/native-species")
	public String getNativeSpecies(@RequestParam long planetId, Model model) {
		Planet planet = planetService.getPlanet(planetId);
		model.addAttribute(PLANET, planet);
		model.addAttribute("hideName", false);

		NativeSpeciesDescription description = new NativeSpeciesDescription(messageSource);
		model.addAttribute("description", description);

		return "ingame/planet/native-species::content";
	}

	@GetMapping("/orbital-systems")
	public String getOrbitalSystems(@RequestParam long planetId, Model model) {
		List<OrbitalSystem> orbitalSystems = planetService.getOrbitalSystemsByPlanet(planetId);
		model.addAttribute("orbitalSystems", orbitalSystems);
		return "ingame/planet/orbitalsystems::content";
	}

	@GetMapping("/ships-in-orbit")
	public String getShipsInOrbit(@RequestParam long planetId, Model model) {
		List<Ship> ships = planetService.getShipsInOrbit(planetId);

		if (ships.isEmpty()) {
			return "ingame/planet/ships-in-orbit::empty";
		}

		long loginId = userDetailService.getLoginId();

		List<Ship> ownShips = new ArrayList<>(ships.size());
		List<Ship> foreignShips = new ArrayList<>(ships.size());

		for (Ship ship : ships) {
			if (loginId == ship.getPlayer().getLogin().getId()) {
				ownShips.add(ship);
			} else {
				foreignShips.add(ship);
			}
		}

		model.addAttribute("ownShips", ownShips);
		model.addAttribute("foreignShips", foreignShips);

		return "ingame/planet/ships-in-orbit::content";
	}
}
