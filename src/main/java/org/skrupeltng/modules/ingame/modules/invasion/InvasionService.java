package org.skrupeltng.modules.ingame.modules.invasion;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.InvasionDifficulty;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.masterdata.database.FactionRepository;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplateRepository;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.database.ShipTemplateRepository;
import org.skrupeltng.modules.masterdata.database.WeaponTemplate;
import org.skrupeltng.modules.masterdata.database.WeaponTemplateRepository;
import org.skrupeltng.modules.masterdata.service.MasterDataConstants;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class InvasionService {

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private FactionRepository factionRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private ShipTemplateRepository shipTemplateRepository;

	@Autowired
	private PropulsionSystemTemplateRepository propulsionSystemTemplateRepository;

	@Autowired
	private WeaponTemplateRepository weaponTemplateRepository;

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Autowired
	private ConfigProperties configProperties;

	@Autowired
	private NewsService newsService;

	@Transactional(propagation = Propagation.REQUIRED)
	public void setupInvasionGame(Game game) {
		List<Player> players = game.getPlayers();

		if (!game.isUseFixedTeams()) {
			PlayerRelationType relationType = game.isInvasionCooperative() ? PlayerRelationType.ALLIANCE : PlayerRelationType.WAR;

			Set<Set<Long>> visitedPairs = new HashSet<>();

			for (Player p1 : players) {
				for (Player p2 : players) {
					if (p1.getId() != p2.getId() && visitedPairs.add(Set.of(p1.getId(), p2.getId()))) {
						PlayerRelation relation = new PlayerRelation();
						relation.setPlayer1(p1);
						relation.setPlayer2(p2);
						relation.setType(relationType);
						playerRelationRepository.save(relation);
					}
				}
			}
		}

		Login login = loginRepository.findByUsername(AILevel.AI_EASY.name()).orElseThrow();
		Player player = new Player(game, login);
		player.setBackgroundAIAgent(true);
		player.setAiLevel(AILevel.AI_EASY);
		player.setFaction(factionRepository.getReferenceById("orion"));
		player.setTurnFinished(true);
		player = playerRepository.save(player);
		game.getPlayers().add(player);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@java.lang.SuppressWarnings("java:S5413")
	public void processInvasionSpawn(long gameId) {
		Game game = gameRepository.getReferenceById(gameId);
		InvasionDifficulty invasionDifficulty = game.getInvasionDifficulty();

		if (invasionDifficulty == null || game.getWinCondition() != WinCondition.INVASION) {
			return;
		}

		Map<Integer, InvasionWaveConfig> waveConfigs = InvasionConfigData.getWaveConfigs(invasionDifficulty);
		InvasionWaveConfig config = waveConfigs.get(game.getRound() + 1);

		if (config == null) {
			return;
		}

		int playerCount = (int) game.getPlayers().stream().filter(p -> !p.isBackgroundAIAgent()).count();
		int waveCount = configProperties.getInvasionScaleFactor() * playerCount;

		List<Point> originalSpawnPoints = getSpawnPoints(game);
		List<Point> spawnPoints = new ArrayList<>(originalSpawnPoints);

		List<String> sectors = new ArrayList<>(waveCount);

		for (int i = 0; i < waveCount; i++) {
			Point spawnPoint = spawnPoints.remove(MasterDataService.RANDOM.nextInt(spawnPoints.size()));
			String sector = spawnWave(game, config, spawnPoint);
			sectors.add(sector);

			if (spawnPoints.isEmpty()) {
				spawnPoints = new ArrayList<>(originalSpawnPoints);
			}
		}

		if (waveCount == 1) {
			for (Player player : game.getPlayers()) {
				newsService.add(player, NewsEntryConstants.news_entry_invasion_wave_started, "/images/news/admin.jpg", null, null, sectors.get(0));
			}
		} else {
			String lastSector = sectors.remove(sectors.size() - 1);
			String firstSectors = String.join(", ", sectors);

			for (Player player : game.getPlayers()) {
				newsService.add(player, NewsEntryConstants.news_entry_invasion_waves_started, "/images/news/admin.jpg", null, null, firstSectors,
					lastSector);
			}
		}

		game.setInvasionWave(game.getInvasionWave() + 1);
	}

	protected String spawnWave(Game game, InvasionWaveConfig config, Point spawnPoint) {
		int shipCount = nextInt(config.minShipCount(), config.maxShipCount());

		Player backgroundAIAgent = playerRepository.getBackgroundAIAgentPlayer(game.getId());

		Map<Integer, List<ShipTemplate>> templatesMap = createShipTemplatesMap();
		Map<Integer, List<PropulsionSystemTemplate>> propulsionSystemTemplatesMap = createPropulsionSystemTemplatesMap();
		Map<Integer, List<WeaponTemplate>> energyWeaponsMap = createWeaponTemplatesMap(false);
		Map<Integer, List<WeaponTemplate>> projectileWeaponsMap = createWeaponTemplatesMap(true);

		String sector = null;

		for (int i = 0; i < shipCount; i++) {
			sector = spawnShip(backgroundAIAgent, spawnPoint, config, templatesMap, propulsionSystemTemplatesMap, energyWeaponsMap, projectileWeaponsMap);
		}

		return sector;
	}

	@java.lang.SuppressWarnings("java:S135")
	protected String spawnShip(Player player, Point spawnPoint, InvasionWaveConfig config, Map<Integer, List<ShipTemplate>> templatesMap,
							   Map<Integer, List<PropulsionSystemTemplate>> propulsionSystemTemplatesMap, Map<Integer, List<WeaponTemplate>> energyWeaponsMap,
							   Map<Integer, List<WeaponTemplate>> projectileWeaponsMap) {
		ShipTemplate shipTemplate = getElement(templatesMap, config.minTechLevel(), config.maxTechLevel());

		Ship ship = new Ship();
		ship.setPlayer(player);
		ship.setShipTemplate(shipTemplate);
		ship.setName("Invader");
		ship.setCrew(shipTemplate.getCrew());

		int fuel = (int) (shipTemplate.getFuelCapacity() * 0.2f);
		fuel = Math.min(30, fuel);
		if (fuel > shipTemplate.getFuelCapacity()) {
			fuel = shipTemplate.getFuelCapacity();
		}
		ship.setFuel(fuel);

		ship.setPropulsionSystemTemplate(getElement(propulsionSystemTemplatesMap, config.minWarpSpeed(), config.maxWarpSpeed()));

		if (shipTemplate.getEnergyWeaponsCount() > 0) {
			WeaponTemplate energyWeapon = getElement(energyWeaponsMap, config.minWeaponLevel(), config.maxWeaponLevel());
			ship.setEnergyWeaponTemplate(energyWeapon);
		}

		if (shipTemplate.getProjectileWeaponsCount() > 0) {
			WeaponTemplate projectileWeapon = getElement(projectileWeaponsMap, config.minWeaponLevel(), config.maxWeaponLevel());
			ship.setProjectileWeaponTemplate(projectileWeapon);
			ship.addProjectiles(shipTemplate.getProjectileWeaponsCount() * MasterDataConstants.PROJECTILES_PER_WEAPON);
		}

		Game game = player.getGame();
		long gameId = game.getId();
		int galaxySize = game.getGalaxySize();

		for (int i = 0; i < configProperties.getIterationLimit(); i++) {
			int x = nextCoordinate(spawnPoint.getX());
			int y = nextCoordinate(spawnPoint.getY());

			List<Long> shipIdsInRadius = shipRepository.getShipIdsInRadius(gameId, x, y, 10);

			if (!shipIdsInRadius.isEmpty()) {
				continue;
			}

			List<Long> planetIdsInRadius = planetRepository.getPlanetIdsInRadius(gameId, x, y, 10, true);

			if (!planetIdsInRadius.isEmpty()) {
				continue;
			}

			if (x > 0 && x < galaxySize && y > 0 && y < galaxySize) {
				ship.setX(x);
				ship.setY(y);
				break;
			}
		}

		ship = shipRepository.save(ship);

		return ship.getSectorString();

	}

	protected List<Point> getSpawnPoints(Game game) {
		int galaxySize = game.getGalaxySize();
		boolean onlyCorners = game.getInvasionDifficulty().isOnlyCorners();

		List<Point> spawnPoints = new ArrayList<>(onlyCorners ? 4 : 8);
		spawnPoints.add(new Point(0, 0));
		spawnPoints.add(new Point(galaxySize, 0));
		spawnPoints.add(new Point(0, galaxySize));
		spawnPoints.add(new Point(galaxySize, galaxySize));

		if (!onlyCorners) {
			int half = galaxySize / 2;
			spawnPoints.add(new Point(half, 0));
			spawnPoints.add(new Point(galaxySize, half));
			spawnPoints.add(new Point(half, galaxySize));
			spawnPoints.add(new Point(0, half));
		}

		return spawnPoints;
	}

	protected Map<Integer, List<ShipTemplate>> createShipTemplatesMap() {
		List<ShipTemplate> templates = shipTemplateRepository.findAll();
		Map<Integer, List<ShipTemplate>> templatesMap = new HashMap<>(10);

		for (ShipTemplate template : templates) {
			if (!template.getId().startsWith("kuatoh") && (template.getEnergyWeaponsCount() > 0 || template.getProjectileWeaponsCount() > 0)) {
				List<ShipTemplate> list = templatesMap.computeIfAbsent(template.getTechLevel(), t -> new ArrayList<>());
				list.add(template);
			}
		}

		return templatesMap;
	}

	protected Map<Integer, List<PropulsionSystemTemplate>> createPropulsionSystemTemplatesMap() {
		List<PropulsionSystemTemplate> templates = propulsionSystemTemplateRepository.findAll();

		Map<Integer, List<PropulsionSystemTemplate>> templatesMap = new HashMap<>(templates.size());

		for (PropulsionSystemTemplate template : templates) {
			List<PropulsionSystemTemplate> list = templatesMap.computeIfAbsent(template.getWarpSpeed(), t -> new ArrayList<>());
			list.add(template);
		}

		return templatesMap;
	}

	protected Map<Integer, List<WeaponTemplate>> createWeaponTemplatesMap(boolean usesProjectiles) {
		List<WeaponTemplate> templates = weaponTemplateRepository.findByUsesProjectiles(usesProjectiles);

		Map<Integer, List<WeaponTemplate>> templatesMap = new HashMap<>(10);

		for (WeaponTemplate template : templates) {
			List<WeaponTemplate> list = templatesMap.computeIfAbsent(template.getTechLevel(), t -> new ArrayList<>());
			list.add(template);
		}

		return templatesMap;
	}

	protected <T> T getElement(Map<Integer, List<T>> map, int min, int max) {
		List<T> relevantElements = new ArrayList<>();

		for (int i = min; i <= max; i++) {
			List<T> list = map.get(i);

			if (list != null) {
				relevantElements.addAll(list);
			}
		}

		return relevantElements.get(MasterDataService.RANDOM.nextInt(relevantElements.size()));
	}

	protected int nextInt(int min, int max) {
		int diff = max - min;

		if (diff == 0) {
			return min;
		}

		return min + MasterDataService.RANDOM.nextInt(diff);
	}

	protected int nextCoordinate(double start) {
		int addedValue = MasterDataService.RANDOM.nextInt(80);
		addedValue *= MasterDataService.RANDOM.nextBoolean() ? -1 : 1;
		return (int) start + addedValue;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processInvasionShips(long gameId) {
		List<Planet> ownedPlanets = planetRepository.findOwnedPlanetsByGameId(gameId);

		if (ownedPlanets.isEmpty()) {
			return;
		}

		Game game = gameRepository.getReferenceById(gameId);
		InvasionDifficulty difficulty = game.getInvasionDifficulty();

		List<Ship> invaderShips = shipRepository.getShipsOfBackgroundAIAgents(gameId, true);
		List<Ship> defenderShips = shipRepository.getShipsOfBackgroundAIAgents(gameId, false);
		List<Player> players = playerRepository.findByGameId(gameId);

		for (Ship ship : invaderShips) {
			processInvasionShip(ownedPlanets, difficulty, defenderShips, players, ship);
		}
	}

	private void processInvasionShip(List<Planet> ownedPlanets, InvasionDifficulty difficulty, List<Ship> defenderShips, List<Player> players, Ship ship) {
		boolean targetSet = false;

		if (difficulty != InvasionDifficulty.EASY && !defenderShips.isEmpty()) {
			defenderShips.sort(new CoordinateDistanceComparator(ship));
			Ship nearestDefenderShip = defenderShips.get(0);

			if (nearestDefenderShip.getDistance(ship) < 125) {
				sendShipToNearShip(ship, defenderShips);
				targetSet = true;
			}
		}

		if (!targetSet && (ship.getPlanet() == null || ship.getPlanet().getPlayer() == null)) {
			sendShipToNearPlanet(ship, ownedPlanets);
		}

		if (difficulty == InvasionDifficulty.HARD) {
			checkAbilityUsage(ship, players);
		}

		if (ship.getTaskType() == null || ship.getTaskType() == ShipTaskType.NONE) {
			ship.setTaskType(ShipTaskType.PLANET_BOMBARDMENT);
		}

		shipRepository.save(ship);
	}

	protected void sendShipToNearShip(Ship ship, List<Ship> defenderShips) {
		int max = Math.min(defenderShips.size(), 5);
		Ship defenderShip = defenderShips.get(MasterDataService.RANDOM.nextInt(max));

		ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
		ship.setDestinationX(defenderShip.getX());
		ship.setDestinationY(defenderShip.getY());
		ship.setDestinationShip(defenderShip);
	}

	protected void sendShipToNearPlanet(Ship ship, List<Planet> ownedPlanets) {
		ownedPlanets.sort(new CoordinateDistanceComparator(ship));

		int max = Math.min(ownedPlanets.size(), 5);
		Planet planet = ownedPlanets.get(MasterDataService.RANDOM.nextInt(max));

		ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
		ship.setDestinationX(planet.getX());
		ship.setDestinationY(planet.getY());
		ship.setDestinationShip(null);
	}

	protected void checkAbilityUsage(Ship ship, List<Player> players) {
		List<ShipAbility> abilities = ship.getShipTemplate().getShipAbilities();

		for (ShipAbility ability : abilities) {
			checkAbilityUsage(ship, players, ability);
		}
	}

	private void checkAbilityUsage(Ship ship, List<Player> players, ShipAbility ability) {
		switch (ability.getType()) {
			case CLOAKING_PERFECT, CLOAKING_RELIABLE, CLOAKING_UNRELIABLE -> {
				ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
				ship.setActiveAbility(ability);
			}
			case DESTABILIZER -> {
				if (ship.getPlanet() != null && ship.getPlanet().getPlayer() != null) {
					ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
					ship.setActiveAbility(ability);
				}
			}
			case OVERDRIVE -> {
				if (ship.getDistance(new CoordinateImpl(ship.getDestinationX(), ship.getDestinationY(), 0)) <= 125) {
					ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
					ship.setActiveAbility(ability);
				}
			}
			case SIGNATURE_MASK -> {
				if (ship.getActiveAbility() == null) {
					ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
					ship.setActiveAbility(ability);
					ship.setTaskValue(getRandomPlayerColor(players));
				}
			}
			case SUB_SPACE_DISTORTION -> {
				if (ship.getDestinationShip() != null && ship.getDistance(ship.getDestinationShip()) <= configProperties.getSubSpaceDistortionRange()) {
					ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
					ship.setActiveAbility(ability);
				}
			}
			case VIRAL_INVASION -> {
				ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
				ship.setActiveAbility(ability);
				ship.setTaskValue(ShipAbilityConstants.VIRAL_INVASION_COLONISTS);
			}
			default -> {
				// not needed
			}
		}
	}

	protected String getRandomPlayerColor(List<Player> players) {
		return players.get(MasterDataService.RANDOM.nextInt(players.size())).getColor();
	}
}
