package org.skrupeltng.modules.ingame.modules.anomaly.database;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import org.skrupeltng.modules.ingame.database.Game;

@Entity
@Table(name = "plasma_storm")
public class PlasmaStorm implements Serializable {

	private static final long serialVersionUID = 5776202041310330057L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private int x;

	private int y;

	@ManyToOne(fetch = FetchType.LAZY)
	private Game game;

	@Column(name = "rounds_left")
	private int roundsLeft;

	@Column(name = "storm_id")
	private long stormId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public int getRoundsLeft() {
		return roundsLeft;
	}

	public void setRoundsLeft(int roundsLeft) {
		this.roundsLeft = roundsLeft;
	}

	public long getStormId() {
		return stormId;
	}

	public void setStormId(long stormId) {
		this.stormId = stormId;
	}

	@Override
	public String toString() {
		return "PlasmaStorm [id=" + id + ", x=" + x + ", y=" + y + ", stormId=" + stormId + "]";
	}
}
