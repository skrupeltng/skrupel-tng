package org.skrupeltng.modules.ingame.modules.overview.database;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import org.skrupeltng.modules.ingame.modules.planet.database.Planet;

@Entity
@Table(name = "small_meteor_log_entry")
public class SmallMeteorLogEntry implements Serializable {

	private static final long serialVersionUID = -2687047846098329903L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "news_entry_id")
	private NewsEntry newsEntry;

	@ManyToOne(fetch = FetchType.LAZY)
	private Planet planet;

	private int fuel;
	private int mineral1;
	private int mineral2;
	private int mineral3;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public NewsEntry getNewsEntry() {
		return newsEntry;
	}

	public void setNewsEntry(NewsEntry newsEntry) {
		this.newsEntry = newsEntry;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	public int getFuel() {
		return fuel;
	}

	public void setFuel(int fuel) {
		this.fuel = fuel;
	}

	public int getMineral1() {
		return mineral1;
	}

	public void setMineral1(int mineral1) {
		this.mineral1 = mineral1;
	}

	public int getMineral2() {
		return mineral2;
	}

	public void setMineral2(int mineral2) {
		this.mineral2 = mineral2;
	}

	public int getMineral3() {
		return mineral3;
	}

	public void setMineral3(int mineral3) {
		this.mineral3 = mineral3;
	}
}
