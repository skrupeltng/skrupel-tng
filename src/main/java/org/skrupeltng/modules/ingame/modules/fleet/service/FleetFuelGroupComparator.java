package org.skrupeltng.modules.ingame.modules.fleet.service;

import java.util.Comparator;

public class FleetFuelGroupComparator implements Comparator<FleetFuelGroup> {

	public static final FleetFuelGroupComparator INSTANCE = new FleetFuelGroupComparator();

	private FleetFuelGroupComparator() {

	}

	@Override
	public int compare(FleetFuelGroup o1, FleetFuelGroup o2) {
		if (hasLeader(o1)) {
			return -1;
		}
		if (hasLeader(o2)) {
			return 1;
		}

		return -Integer.compare(o1.getShips().size(), o2.getShips().size());
	}

	private boolean hasLeader(FleetFuelGroup group) {
		return group.getShips().stream().anyMatch(s -> s.getFleet().getLeader().getId() == s.getId());
	}
}
