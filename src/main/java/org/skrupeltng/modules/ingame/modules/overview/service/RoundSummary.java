package org.skrupeltng.modules.ingame.modules.overview.service;

import java.time.Instant;

public class RoundSummary {

	private int round;
	private Instant date;
	private String winCondition;
	private int newColonies;
	private int newShips;
	private int newStarbases;
	private int destroyedShips;
	private int lostShips;

	private int newColoniesPlayer;
	private int newShipsPlayer;
	private int newStarbasesPlayer;
	private int destroyedShipsPlayer;
	private int lostShipsPlayer;

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public Instant getDate() {
		return date;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	public String getWinCondition() {
		return winCondition;
	}

	public void setWinCondition(String winCondition) {
		this.winCondition = winCondition;
	}

	public int getNewColonies() {
		return newColonies;
	}

	public void setNewColonies(int newColonies) {
		this.newColonies = newColonies;
	}

	public int getNewShips() {
		return newShips;
	}

	public void setNewShips(int newShips) {
		this.newShips = newShips;
	}

	public int getNewStarbases() {
		return newStarbases;
	}

	public void setNewStarbases(int newStarbases) {
		this.newStarbases = newStarbases;
	}

	public int getDestroyedShips() {
		return destroyedShips;
	}

	public void setDestroyedShips(int destroyedShips) {
		this.destroyedShips = destroyedShips;
	}

	public int getLostShips() {
		return lostShips;
	}

	public void setLostShips(int lostShips) {
		this.lostShips = lostShips;
	}

	public int getNewColoniesPlayer() {
		return newColoniesPlayer;
	}

	public void setNewColoniesPlayer(int newColoniesPlayer) {
		this.newColoniesPlayer = newColoniesPlayer;
	}

	public int getNewShipsPlayer() {
		return newShipsPlayer;
	}

	public void setNewShipsPlayer(int newShipsPlayer) {
		this.newShipsPlayer = newShipsPlayer;
	}

	public int getNewStarbasesPlayer() {
		return newStarbasesPlayer;
	}

	public void setNewStarbasesPlayer(int newStarbasesPlayer) {
		this.newStarbasesPlayer = newStarbasesPlayer;
	}

	public int getDestroyedShipsPlayer() {
		return destroyedShipsPlayer;
	}

	public void setDestroyedShipsPlayer(int destroyedShipsPlayer) {
		this.destroyedShipsPlayer = destroyedShipsPlayer;
	}

	public int getLostShipsPlayer() {
		return lostShipsPlayer;
	}

	public void setLostShipsPlayer(int lostShipsPlayer) {
		this.lostShipsPlayer = lostShipsPlayer;
	}
}
