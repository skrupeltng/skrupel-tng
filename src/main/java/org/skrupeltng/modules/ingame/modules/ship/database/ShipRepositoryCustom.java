package org.skrupeltng.modules.ingame.modules.ship.database;

import java.util.List;
import java.util.Set;

import org.skrupeltng.modules.ingame.controller.ShipListResultDTO;
import org.skrupeltng.modules.ingame.controller.ShipOverviewRequest;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.springframework.data.domain.Page;

public interface ShipRepositoryCustom {

	boolean loginOwnsShip(long shipId, long loginId);

	long getEnemyShipCountByPlanetDestination(int x, int y, long gameId, long playerId);

	List<Long> getShipIdsInRadius(long gameId, int x, int y, int distance);

	void resetShipScanRadius(long gameId);

	void updateScanRadius(long gameId, int scanRadius, ShipAbilityType abilityType);

	void cloakAntigravPropulsion(long gameId);

	long getSupportingShipCount(long playerId, int x, int y, long shipId, int minWeaponCount, int minTechLevel);

	long getCommunicationCenterShipCount(long playerId, int x, int y, long excludedShipId);

	int getHighestSupportingTechLevel(long shipId, long playerId, int x, int y);

	void processShipsInPlasmaStorms(long gameId);

	Page<ShipListResultDTO> searchShips(ShipOverviewRequest request, long loginId);

	List<Long> getShipsOnAlliedPlanets(long gameId, PlayerRelationType type);

	void refuelBackgroundAIAgentShips(long gameId);

	void scanPlanets(long gameId);

	void scanStarbases(long gameId);

	Set<Long> getShipIdsInScannerRangeOfPlayer(long playerId);

	void clearScannedShipIdsCache();
}
