package org.skrupeltng.modules.ingame.modules.ship.controller;

import java.util.List;

public class ShipCloakingSummary {

	private final List<String> visibleToPlayers;

	public ShipCloakingSummary(List<String> visibleToPlayers) {
		this.visibleToPlayers = visibleToPlayers;
	}

	public List<String> getVisibleToPlayers() {
		return visibleToPlayers;
	}
}