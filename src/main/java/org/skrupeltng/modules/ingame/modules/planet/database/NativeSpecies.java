package org.skrupeltng.modules.ingame.modules.planet.database;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "native_species")
public class NativeSpecies implements Serializable {

	private static final long serialVersionUID = -3108158303559122911L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String name;

	@Enumerated(EnumType.STRING)
	private NativeSpeciesType type;

	private float tax;

	@Enumerated(EnumType.STRING)
	private NativeSpeciesEffect effect;

	@Column(name = "effect_value")
	private float effectValue;

	public NativeSpecies() {

	}

	public NativeSpecies(NativeSpeciesEffect effect, float effectValue) {
		this.effect = effect;
		this.effectValue = effectValue;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public NativeSpeciesType getType() {
		return type;
	}

	public void setType(NativeSpeciesType type) {
		this.type = type;
	}

	public float getTax() {
		return tax;
	}

	public void setTax(float tax) {
		this.tax = tax;
	}

	public NativeSpeciesEffect getEffect() {
		return effect;
	}

	public void setEffect(NativeSpeciesEffect effect) {
		this.effect = effect;
	}

	public float getEffectValue() {
		return effectValue;
	}

	public void setEffectValue(float effectValue) {
		this.effectValue = effectValue;
	}
}
