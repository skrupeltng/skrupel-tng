package org.skrupeltng.modules.ingame.modules.ship.controller;

import java.io.Serializable;

public class ShipOptionsChangeRequest implements Serializable {

	private static final long serialVersionUID = 661216952025066326L;

	private String name;
	private Long newOwnerId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getNewOwnerId() {
		return newOwnerId;
	}

	public void setNewOwnerId(Long newOwnerId) {
		this.newOwnerId = newOwnerId;
	}
}