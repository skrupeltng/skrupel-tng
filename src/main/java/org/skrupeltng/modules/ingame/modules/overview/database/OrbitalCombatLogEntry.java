package org.skrupeltng.modules.ingame.modules.overview.database;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.database.WeaponTemplate;

@Entity
@Table(name = "orbital_combat_log_entry")
public class OrbitalCombatLogEntry implements Serializable {

	private static final long serialVersionUID = 1429359257763822647L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "news_entry_id")
	private NewsEntry newsEntry;

	@ManyToOne(fetch = FetchType.LAZY)
	private Planet planet;

	@Column(name = "for_planet_player")
	private boolean forPlanetPlayer;

	@Column(name = "initial_orbital_defense_percentage")
	private int initialOrbitalDefensePercentage;

	@Column(name = "remaining_orbital_defense_percentage")
	private int remainingOrbitalDefensePercentage;

	private boolean destroyed;

	@Column(name = "ship_id")
	private Long shipId;

	@Column(name = "ship_name")
	private String shipName;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ship_template_id")
	private ShipTemplate shipTemplate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "propulsion_system_template_name")
	private PropulsionSystemTemplate propulsionSystemTemplate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "energy_weapon_template_name")
	private WeaponTemplate energyWeaponTemplate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "projectile_weapon_template_name")
	private WeaponTemplate projectileWeaponTemplate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public NewsEntry getNewsEntry() {
		return newsEntry;
	}

	public void setNewsEntry(NewsEntry newsEntry) {
		this.newsEntry = newsEntry;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	public boolean isForPlanetPlayer() {
		return forPlanetPlayer;
	}

	public void setForPlanetPlayer(boolean forPlanetPlayer) {
		this.forPlanetPlayer = forPlanetPlayer;
	}

	public int getInitialOrbitalDefensePercentage() {
		return initialOrbitalDefensePercentage;
	}

	public void setInitialOrbitalDefensePercentage(int initialOrbitalDefensePercentage) {
		this.initialOrbitalDefensePercentage = initialOrbitalDefensePercentage;
	}

	public int getRemainingOrbitalDefensePercentage() {
		return remainingOrbitalDefensePercentage;
	}

	public void setRemainingOrbitalDefensePercentage(int remainingOrbitalDefensePercentage) {
		this.remainingOrbitalDefensePercentage = remainingOrbitalDefensePercentage;
	}

	public boolean isDestroyed() {
		return destroyed;
	}

	public void setDestroyed(boolean destroyed) {
		this.destroyed = destroyed;
	}

	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public Long getShipId() {
		return shipId;
	}

	public void setShipId(Long shipId) {
		this.shipId = shipId;
	}

	public ShipTemplate getShipTemplate() {
		return shipTemplate;
	}

	public void setShipTemplate(ShipTemplate shipTemplate) {
		this.shipTemplate = shipTemplate;
	}

	public PropulsionSystemTemplate getPropulsionSystemTemplate() {
		return propulsionSystemTemplate;
	}

	public void setPropulsionSystemTemplate(PropulsionSystemTemplate propulsionSystemTemplate) {
		this.propulsionSystemTemplate = propulsionSystemTemplate;
	}

	public WeaponTemplate getEnergyWeaponTemplate() {
		return energyWeaponTemplate;
	}

	public void setEnergyWeaponTemplate(WeaponTemplate energyWeaponTemplate) {
		this.energyWeaponTemplate = energyWeaponTemplate;
	}

	public WeaponTemplate getProjectileWeaponTemplate() {
		return projectileWeaponTemplate;
	}

	public void setProjectileWeaponTemplate(WeaponTemplate projectileWeaponTemplate) {
		this.projectileWeaponTemplate = projectileWeaponTemplate;
	}
}
