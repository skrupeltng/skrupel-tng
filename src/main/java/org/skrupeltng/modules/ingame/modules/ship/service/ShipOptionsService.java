package org.skrupeltng.modules.ingame.modules.ship.service;

import org.apache.commons.lang3.StringUtils;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.service.AchievementService;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetService;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipOptionsChangeRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ShipOptionsService {

	private final ShipRepository shipRepository;
	private final PlayerRepository playerRepository;
	private final PlayerRelationRepository playerRelationRepository;
	private final VisibleObjects visibleObjects;
	private final ShipRouteEntryRepository shipRouteEntryRepository;
	private final AchievementService achievementService;
	private final FleetService fleetService;

	public ShipOptionsService(ShipRepository shipRepository, PlayerRepository playerRepository, PlayerRelationRepository playerRelationRepository, VisibleObjects visibleObjects, ShipRouteEntryRepository shipRouteEntryRepository, AchievementService achievementService, FleetService fleetService) {
		this.shipRepository = shipRepository;
		this.playerRepository = playerRepository;
		this.playerRelationRepository = playerRelationRepository;
		this.visibleObjects = visibleObjects;
		this.shipRouteEntryRepository = shipRouteEntryRepository;
		this.achievementService = achievementService;
		this.fleetService = fleetService;
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void changeOptions(long shipId, ShipOptionsChangeRequest request) {
		Ship ship = shipRepository.getReferenceById(shipId);

		PolicyFactory policy = new HtmlPolicyBuilder().toFactory();
		String name = policy.sanitize(request.getName());

		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("Ship name cannot be empty!");
		}

		ship.setName(name);
		ship = shipRepository.save(ship);

		Long newOwnerId = request.getNewOwnerId();

		if (newOwnerId != null) {
			giveShipToAlly(ship, newOwnerId);
		}
	}

	protected void giveShipToAlly(Ship ship, long playerId) {
		final Player originalOwner = ship.getPlayer();
		Game game = originalOwner.getGame();
		Player player = playerRepository.getReferenceById(playerId);

		if (player.getGame().getId() != game.getId()) {
			throw new IllegalArgumentException("Player " + playerId + " is not part of game " + game.getId() + "!");
		}

		List<PlayerRelation> relations = playerRelationRepository.findByPlayerIds(ship.getPlayer().getId(), playerId);

		if (relations.isEmpty() || relations.get(0).getType() != PlayerRelationType.ALLIANCE) {
			throw new IllegalArgumentException("Player " + playerId + " is not in an Alliance with player " + ship.getPlayer().getId() + "!");
		}

		ship.setPlayer(player);
		ship.resetTask();
		ship.resetTravel();

		ship = shipRepository.save(ship);

		fleetService.clearFleetOfShip(ship.getId());
		shipRepository.clearCurrentRouteEntry(ship.getId());
		shipRouteEntryRepository.deleteByShipId(ship.getId());

		achievementService.unlockAchievement(originalOwner, AchievementType.SHIP_GIVEN_TO_ALLY);

		visibleObjects.clearCaches();
	}
}
