package org.skrupeltng.modules.ingame.modules.overview.service;

import java.util.List;
import java.util.Optional;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntryArgument;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntryArgumentRepository;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NewsService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private NewsEntryRepository newsEntryRepository;

	@Autowired
	private NewsEntryArgumentRepository newsEntryArgumentRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Transactional(propagation = Propagation.REQUIRED)
	public NewsEntry add(Player player, String template, String image, Long clickTargetId, NewsEntryClickTargetType clickTargetType, Object... arguments) {
		NewsEntryCreationRequest request = new NewsEntryCreationRequest(player, template, image, clickTargetId, clickTargetType, arguments);
		return add(request);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public NewsEntry addWithNewTransaction(long playerId, String template, String image, Long clickTargetId, NewsEntryClickTargetType clickTargetType,
			Object... arguments) {
		Player player = playerRepository.getReferenceById(playerId);
		NewsEntryCreationRequest request = new NewsEntryCreationRequest(player, template, image, clickTargetId, clickTargetType, arguments);
		return add(request);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public NewsEntry add(NewsEntryCreationRequest request) {
		NewsEntry e = new NewsEntry();
		e.setPlayer(request.getPlayer());
		e.setGame(request.getPlayer().getGame());
		e.setImage(request.getImage());
		e.setTemplate(request.getTemplate());
		e.setClickTargetId(request.getClickTargetId());
		e.setClickTargetType(request.getClickTargetType());

		e = newsEntryRepository.save(e);

		if (request.getArguments() != null) {
			for (Object argument : request.getArguments()) {
				NewsEntryArgument arg = new NewsEntryArgument();
				arg.setNewsEntry(e);
				arg.setValue(argument.toString());
				newsEntryArgumentRepository.save(arg);
			}
		}

		return e;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void cleanNewsEntries(long gameId) {
		log.debug("Clearing news entries...");
		newsEntryRepository.deleteByGameId(gameId);
		log.debug("News entries cleared.");
	}

	public RoundSummary getRoundSummary(long gameId, long playerId) {
		return newsEntryRepository.getRoundSummary(gameId, playerId);
	}

	public List<NewsEntry> findNews(long gameId, long loginId) {
		return newsEntryRepository.findByGameIdAndLoginId(gameId, loginId);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public boolean toggleDeleteNews(long newsEntryId) {
		Optional<NewsEntry> result = newsEntryRepository.findById(newsEntryId);

		if (result.isPresent()) {
			NewsEntry e = result.get();
			e.setDelete(!e.isDelete());
			e = newsEntryRepository.save(e);

			return e.isDelete();
		}

		return false;
	}
}
