package org.skrupeltng.modules.ingame.modules.overview.database.spacecombat;

import java.io.Serializable;
import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntry;

@Entity
@Table(name = "space_combat_log_entry")
public class SpaceCombatLogEntry implements Serializable {

	private static final long serialVersionUID = 7434825234372671818L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "news_entry_id")
	private NewsEntry newsEntry;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "spaceCombatLogEntry")
	private List<SpaceCombatLogEntryItem> items;

	private transient SpaceCombatLogEntryItem ownItem;
	private transient SpaceCombatLogEntryItem enemyItem;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public NewsEntry getNewsEntry() {
		return newsEntry;
	}

	public void setNewsEntry(NewsEntry newsEntry) {
		this.newsEntry = newsEntry;
	}

	public SpaceCombatLogEntryItem getOwnItem() {
		return ownItem;
	}

	public void setOwnItem(SpaceCombatLogEntryItem ownItem) {
		this.ownItem = ownItem;
	}

	public SpaceCombatLogEntryItem getEnemyItem() {
		return enemyItem;
	}

	public void setEnemyItem(SpaceCombatLogEntryItem enemyItem) {
		this.enemyItem = enemyItem;
	}

	public List<SpaceCombatLogEntryItem> getItems() {
		return items;
	}

	public void setItems(List<SpaceCombatLogEntryItem> items) {
		this.items = items;
	}

	public SpaceCombatLogEntryItem retrieveOwnItem() {
		return items.stream().filter(i -> !i.isEnemy()).findFirst().orElseThrow();
	}

	public SpaceCombatLogEntryItem retrieveEnemyItem() {
		return items.stream().filter(i -> i.isEnemy()).findFirst().orElseThrow();
	}

	@Override
	public String toString() {
		return "SpaceCombatLogEntry [id=" + id + ", items=" + items + "]";
	}
}
