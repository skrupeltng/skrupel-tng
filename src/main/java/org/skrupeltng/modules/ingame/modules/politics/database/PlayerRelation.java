package org.skrupeltng.modules.ingame.modules.politics.database;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;

import java.io.Serial;
import java.io.Serializable;

@Entity
@Table(name = "player_relation")
public class PlayerRelation implements Serializable {

	@Serial
	private static final long serialVersionUID = 5504952865753231859L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "player1_id")
	private Player player1;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "player2_id")
	private Player player2;

	@Enumerated(EnumType.STRING)
	private PlayerRelationType type;

	@JoinColumn(name = "rounds_left")
	private int roundsLeft = -1;

	public PlayerRelation() {

	}

	public PlayerRelation(Player player1, Player player2) {
		this.player1 = player1;
		this.player2 = player2;
	}

	public PlayerRelation(Player player1, Player player2, PlayerRelationType type) {
		this.player1 = player1;
		this.player2 = player2;
		this.type = type;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Player getPlayer1() {
		return player1;
	}

	public void setPlayer1(Player player1) {
		this.player1 = player1;
	}

	public Player getPlayer2() {
		return player2;
	}

	public void setPlayer2(Player player2) {
		this.player2 = player2;
	}

	public PlayerRelationType getType() {
		return type;
	}

	public void setType(PlayerRelationType type) {
		this.type = type;
	}

	public int getRoundsLeft() {
		return roundsLeft;
	}

	public void setRoundsLeft(int roundsLeft) {
		this.roundsLeft = roundsLeft;
	}

	public String retrieveOtherPlayerName(long currentLoginId) {
		if (player2.getLogin().getId() == currentLoginId) {
			return player1.getLogin().getUsername();
		}

		return player2.getLogin().getUsername();
	}
}
