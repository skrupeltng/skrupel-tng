package org.skrupeltng.modules.ingame.modules.overview.controller;

import org.skrupeltng.modules.dashboard.database.Team;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.invasion.InvasionConfigData;
import org.skrupeltng.modules.ingame.modules.invasion.InvasionWaveConfig;
import org.skrupeltng.modules.ingame.service.round.wincondition.MineralRaceProgress;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.DoubleFunction;
import java.util.function.ToDoubleFunction;

public class GameModeOverviewDataFetcher {

    private final Model model;
    private final Game game;
    private final Set<Long> encounteredPlayers;
    private final List<Player> deathFoes;
    private final MessageSource messageSource;

    public GameModeOverviewDataFetcher(Model model, Game game, Set<Long> encounteredPlayers, List<Player> deathFoes, MessageSource messageSource) {
        this.model = model;
        this.game = game;
        this.encounteredPlayers = encounteredPlayers;
        this.deathFoes = deathFoes;
        this.messageSource = messageSource;
    }

    public void addWinConditionData() {
        WinCondition winCondition = game.getWinCondition();

        if (winCondition == WinCondition.DEATH_FOE) {
            addDeathFoeData();
        } else if (winCondition == WinCondition.INVASION) {
            addInvasionData();
        } else if (winCondition == WinCondition.MINERAL_RACE) {
            addMineralRaceData();
        } else if (winCondition == WinCondition.CONQUEST) {
            addConquestData();
        }
    }


    private void addDeathFoeData() {
        int actualDeathFoeCount = deathFoes.size();

        List<Player> filteredDeathFoes = deathFoes;

        if (game.isEnableWysiwyg()) {
            filteredDeathFoes = deathFoes.stream()
                    .filter(p -> encounteredPlayers.contains(p.getId()))
                    .toList();
        }

        if (game.isUseFixedTeams()) {
            if (filteredDeathFoes.isEmpty()) {
                model.addAttribute("deathFoesNotSeenYet", true);
            } else {
                model.addAttribute("deathFoes", filteredDeathFoes);

                if (actualDeathFoeCount != filteredDeathFoes.size()) {
                    model.addAttribute("notMetDeathFoes", actualDeathFoeCount - filteredDeathFoes.size());
                }
            }
        } else {
            if (filteredDeathFoes.isEmpty()) {
                model.addAttribute("deathFoeNotSeenYet", true);
            } else {
                model.addAttribute("deathFoe", filteredDeathFoes.get(0));
            }
        }
    }

    private void addInvasionData() {
        Map<Integer, InvasionWaveConfig> waveConfigs = InvasionConfigData.getWaveConfigs(game.getInvasionDifficulty());

        if (waveConfigs.size() <= game.getInvasionWave()) {
            model.addAttribute("invasionRoundsLeft", false);
        } else {
            model.addAttribute("invasionRoundsLeft", true);
            model.addAttribute("invasionWaveCount", waveConfigs.size());
            Integer nextWaveRoundStart = waveConfigs.keySet().stream().sorted().toList().get(game.getInvasionWave());
            model.addAttribute("nextInvasionWaveRounds", nextWaveRoundStart - game.getRound());
        }
    }

    private void addMineralRaceData() {
        MineralRaceProgress mineralRaceProgress = new MineralRaceProgress(game, 0, game.getPlayers());
        float mineralRaceQuantity = game.getMineralRaceQuantity();
        ToDoubleFunction<Player> resourceFunction = p -> mineralRaceProgress.getPlayerToIntFunction().applyAsInt(p) / mineralRaceQuantity;

        NumberFormat percentInstance = NumberFormat.getPercentInstance(LocaleContextHolder.getLocale());
        percentInstance.setMinimumFractionDigits(1);
        percentInstance.setMaximumFractionDigits(1);
        DoubleFunction<String> displayStringFunction = percentInstance::format;

        List<GameObjectiveProgress> mineralRaceProgressEntries = createObjectiveProgressEntries(resourceFunction, displayStringFunction);
        model.addAttribute("mineralRaceProgressEntries", mineralRaceProgressEntries);
    }

    private void addConquestData() {
        ToDoubleFunction<Player> resourceFunction = Player::getVictoryPoints;
        DoubleFunction<String> displayStringFunction = d -> String.format("%d / %d", (int) d, game.getConquestRounds());

        List<GameObjectiveProgress> conquestProgressEntries = createObjectiveProgressEntries(resourceFunction, displayStringFunction);
        model.addAttribute("conquestProgressEntries", conquestProgressEntries);
    }

    private List<GameObjectiveProgress> createObjectiveProgressEntries(ToDoubleFunction<Player> valueFunction, DoubleFunction<String> displayStringFunction) {
        List<GameObjectiveProgress> entries = new ArrayList<>();

        if (game.isUseFixedTeams()) {
            for (Team team : game.getTeams()) {
                Optional<Double> teamSumOpt = team.getPlayers().stream()
                        .map(valueFunction::applyAsDouble)
                        .reduce(Double::sum);

                double value = teamSumOpt.orElse(0.0);
                String displayString = displayStringFunction.apply(value);
                String teamPrefix = messageSource.getMessage("team", null, "Team", LocaleContextHolder.getLocale());
                entries.add(new GameObjectiveProgress("%s %s".formatted(teamPrefix, team.getName()), displayString, value));
            }
        } else {
            for (Player player : game.getPlayers()) {
                double value = valueFunction.applyAsDouble(player);
                String displayString = displayStringFunction.apply(value);

                String playerName = player.getLogin().getUsername();
                playerName = messageSource.getMessage(playerName, null, playerName, LocaleContextHolder.getLocale());

                entries.add(new GameObjectiveProgress(playerName, displayString, value));
            }
        }

        entries.sort(Comparator.comparing(GameObjectiveProgress::value).reversed());
        return entries;
    }
}
