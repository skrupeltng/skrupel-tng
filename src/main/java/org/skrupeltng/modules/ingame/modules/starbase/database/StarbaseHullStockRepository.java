package org.skrupeltng.modules.ingame.modules.starbase.database;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface StarbaseHullStockRepository extends JpaRepository<StarbaseHullStock, Long> {

	Optional<StarbaseHullStock> findByShipTemplateIdAndStarbaseId(String templateId, long starbaseId);

	@Modifying
	@Query("DELETE FROM StarbaseHullStock s WHERE s.starbase.id = ?1")
	void deleteByStarbaseId(long starbaseId);
}