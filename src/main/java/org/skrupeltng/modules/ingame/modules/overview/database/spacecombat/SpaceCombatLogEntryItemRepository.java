package org.skrupeltng.modules.ingame.modules.overview.database.spacecombat;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface SpaceCombatLogEntryItemRepository extends JpaRepository<SpaceCombatLogEntryItem, Long> {

	@Modifying
	@Query("DELETE FROM " +
			"	SpaceCombatLogEntryItem i " +
			"WHERE " +
			"	i.spaceCombatLogEntry.id IN (" +
			"		SELECT " +
			"			e.id " +
			"		FROM " +
			"			SpaceCombatLogEntry e " +
			"		WHERE " +
			"			e.newsEntry.id IN (SELECT n.id FROM NewsEntry n WHERE n.player.id = ?1))")
	void deleteByPlayerId(long playerId);

	@Modifying
	@Query("UPDATE SpaceCombatLogEntryItem i SET i.shipId = null WHERE i.shipId = ?1")
	void clearShipId(long shipId);

	@Modifying
	@Query("UPDATE SpaceCombatLogEntryItem i SET i.shipId = null WHERE i.shipId IN (?1)")
	void clearShipIds(Collection<Long> shipIds);
}