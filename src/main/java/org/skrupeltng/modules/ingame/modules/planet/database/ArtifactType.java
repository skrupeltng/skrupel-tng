package org.skrupeltng.modules.ingame.modules.planet.database;

public enum ArtifactType {

	FUEL, MINERAL1, MINERAL2, MINERAL3, COLONY, WEATHER_CONTROL_STATION
}