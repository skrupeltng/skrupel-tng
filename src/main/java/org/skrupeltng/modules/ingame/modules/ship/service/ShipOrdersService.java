package org.skrupeltng.modules.ingame.modules.ship.service;

import org.jetbrains.annotations.VisibleForTesting;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetLeader;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipCourseSelectionRequest;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTaskChangeRequest;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTaskString;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ShipOrdersService {

	private final ShipRepository shipRepository;
	private final MasterDataService masterDataService;
	private final FleetLeader fleetLeader;
	private final ShipTaskString shipTaskString;
	private final VisibleObjects visibleObjects;
	private final ConfigProperties configProperties;

	public ShipOrdersService(ShipRepository shipRepository, FleetLeader fleetLeader, VisibleObjects visibleObjects, ConfigProperties configProperties, ShipTaskString shipTaskString, MasterDataService masterDataService) {
		this.shipRepository = shipRepository;
		this.fleetLeader = fleetLeader;
		this.visibleObjects = visibleObjects;
		this.configProperties = configProperties;
		this.shipTaskString = shipTaskString;
		this.masterDataService = masterDataService;
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(propagation = Propagation.REQUIRED)
	public String setCourse(long shipId, ShipCourseSelectionRequest request) {
		Ship ship = shipRepository.getReferenceById(shipId);
		Player player = ship.getPlayer();
		Game game = player.getGame();

		int x = request.getX();
		int y = request.getY();
		int speed = request.getSpeed();
		Long destinationShipId = request.getDestinationShipId();

		validateCourse(request, ship, game);

		ship.setDestinationX(x);
		ship.setDestinationY(y);
		ship.setTravelSpeed(speed);

		if (ship.getRoute().size() >= 2) {
			ship.setRouteDisabled(true);
		}

		if (destinationShipId != null) {
			Set<CoordinateImpl> visibilityCoordinates = visibleObjects.getVisibilityCoordinates(player.getId());
			Set<Long> visibleShips = visibleObjects.getVisibleShipsCached(game.getId(), visibilityCoordinates, player.getId(), false).stream().map(Ship::getId)
				.collect(Collectors.toSet());

			Ship destinationShip = shipRepository.getReferenceById(destinationShipId);

			if (visibleShips.contains(destinationShip.getId())) {
				ship.setDestinationShip(destinationShip);
			} else {
				throw new IllegalArgumentException("The destination ship is not visible to you!");
			}
		} else {
			ship.setDestinationShip(null);
		}

		ship = shipRepository.save(ship);

		Fleet fleet = ship.getFleet();

		if (fleet != null && fleet.getLeader() != null && fleet.getLeader().getId() == ship.getId()) {
			fleetLeader.setLeader(fleet.getId(), shipId);
		}

		return shipTaskString.retrieveTaskString(ship);
	}

	@VisibleForTesting
	protected void validateCourse(ShipCourseSelectionRequest request, Ship ship, Game game) {
		int x = request.getX();
		int y = request.getY();
		int speed = request.getSpeed();

		if (x < 0 || y < 0 || x > game.getGalaxySize() || y > game.getGalaxySize()) {
			throw new IllegalArgumentException("Course target out of galaxy bounds!");
		}

		if (speed < 0 || speed > 9) {
			throw new IllegalArgumentException("Invalid travel speed!");
		}

		if (ship.getPropulsionSystemTemplate().getTechLevel() == 1 && speed > 1) {
			throw new IllegalArgumentException("Sunsail can only travel with Warp 1!");
		}
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteCourse(long shipId) {
		Ship ship = shipRepository.getReferenceById(shipId);
		ship.resetTravel();
		shipRepository.save(ship);

		visibleObjects.clearCaches();
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void changeTask(long shipId, ShipTaskChangeRequest request) {
		Ship ship = shipRepository.getReferenceById(shipId);

		String taskTypeString = request.getTaskType();
		Long escortTargetId = request.getEscortTargetId();

		ShipTaskType taskType = ShipTaskType.valueOf(taskTypeString);

		validateTask(request, ship, taskType);

		if (ship.getDestinationShip() != null && ship.getDestinationShip().getPlayer().getId() == ship.getPlayer().getId() && escortTargetId == null) {
			ship.resetTravel();
		}

		ship.setActiveAbility(null);
		ship.setTaskType(taskType);
		ship.setTaskValue(request.getTaskValue());

		if (taskType == ShipTaskType.ACTIVE_ABILITY) {
			activateAbility(request, ship);
		}

		int escortTargetSpeed = request.getEscortTargetSpeed();

		if (escortTargetId != null && escortTargetSpeed > 0 && escortTargetSpeed <= 10) {
			escort(ship, escortTargetId, escortTargetSpeed);
		}

		shipRepository.save(ship);
	}

	@VisibleForTesting
	protected void activateAbility(ShipTaskChangeRequest request, Ship ship) {
		String activeAbilityTypeString = request.getActiveAbilityType();

		ShipAbilityType abilityType = ShipAbilityType.valueOf(activeAbilityTypeString);
		Optional<ShipAbility> abilityOpt = ship.getAbility(abilityType);

		if (abilityOpt.isPresent()) {
			ShipAbility activeAbility = abilityOpt.get();
			ship.setActiveAbility(activeAbility);
		} else {
			throw new IllegalArgumentException("Ship %d does not have ShipAbility %s!".formatted(ship.getId(), abilityType));
		}
	}

	@VisibleForTesting
	protected void escort(Ship ship, Long escortTargetId, int escortTargetSpeed) {
		Optional<Ship> targetOpt = shipRepository.findById(escortTargetId);

		if (targetOpt.isPresent()) {
			Ship target = targetOpt.get();
			ship.setDestinationX(target.getX());
			ship.setDestinationY(target.getY());
			ship.setDestinationShip(target);
			ship.setTravelSpeed(escortTargetSpeed);
		} else {
			throw new IllegalArgumentException("Escort target ship with id %d not found!".formatted(escortTargetId));
		}
	}

	@VisibleForTesting
	protected void validateTask(ShipTaskChangeRequest request, Ship ship, ShipTaskType taskType) {
		ShipTemplate shipTemplate = ship.getShipTemplate();

		switch (taskType) {
			case AUTO_DESTRUCTION:
				if (ship.getShipModule() != ShipModule.AUTO_DESTRUCTION) {
					throw new IllegalArgumentException("Missing ship module!");
				}
				if (!configProperties.isAutoDestructShipModuleEnabled()) {
					throw new IllegalArgumentException("Auto Destruction disabled!");
				}
				break;
			case CAPTURE_SHIP:
				if (!ship.hasWeapons()) {
					throw new IllegalArgumentException("Ship has no weapons!");
				}
				break;
			case CLEAR_MINE_FIELD:
				if (shipTemplate.getHangarCapacity() == 0) {
					throw new IllegalArgumentException("Ship has no hangars!");
				}
				break;
			case CREATE_MINE_FIELD:
				validateMineFieldCreation(request, ship);
				break;
			case HIRE_CREW:
				int crewToBeHired = Integer.parseInt(request.getTaskValue());

				if (shipTemplate.getCrew() - ship.getCrew() < crewToBeHired) {
					throw new IllegalArgumentException("Crew hire mismatch!");
				}
				break;
			case HUNTER_TRAINING:
				validateHunterTraining(ship);
				break;
			case PLANET_BOMBARDMENT:
				if (!ship.hasWeapons()) {
					throw new IllegalArgumentException("Weapons missing!");
				}
				break;
			case REPAIR:
				if (ship.getDamage() == 0) {
					throw new IllegalArgumentException("Ship is not damaged!");
				}
				break;
			case TRACTOR_BEAM:
				validateTractorBeam(request, ship);
				break;
			default:
				// Other ShipTaskTypes don't require any validation here
				break;
		}
	}

	@VisibleForTesting
	protected void validateMineFieldCreation(ShipTaskChangeRequest request, Ship ship) {
		if (ship.getProjectileWeaponTemplate() == null) {
			throw new IllegalArgumentException("Ship has no projectile weapons!");
		}

		if (ship.getProjectiles() < Integer.parseInt(request.getTaskValue())) {
			throw new IllegalArgumentException("Not enough projectiles!");
		}
	}

	@VisibleForTesting
	protected void validateHunterTraining(Ship ship) {
		ShipTemplate shipTemplate = ship.getShipTemplate();
		Planet planet = ship.getPlanet();

		if (planet == null) {
			throw new IllegalArgumentException("Cannot train hunter because ship not on planet!");
		}

		if (planet.getPlayer() == null) {
			throw new IllegalArgumentException("Cannot train hunter because planet uninhabited!");
		}

		if (planet.getPlayer().getId() != ship.getPlayer().getId()) {
			throw new IllegalArgumentException("Cannot train hunter because planet not owned!");
		}

		if (!planet.hasOrbitalSystem(OrbitalSystemType.HUNTER_ACADEMY)) {
			throw new IllegalArgumentException("Cannot train hunter because hunter academy missing!");
		}

		if (shipTemplate.getMass() >= 100) {
			throw new IllegalArgumentException("Cannot train hunter because ship too heavy!");
		}

		Starbase starbase = planet.getStarbase();

		if (starbase == null) {
			throw new IllegalArgumentException("Cannot train hunter because starbase missing!");
		}

		if (starbase.getType() != StarbaseType.BATTLE_STATION) {
			throw new IllegalArgumentException("Cannot train hunter because starbase is no battle station!");
		}

		if (ship.getExperience() == 5) {
			throw new IllegalArgumentException("Cannot train hunter because already fully trained!");
		}

		if (ship.getTravelSpeed() != 0) {
			throw new IllegalArgumentException("Cannot train hunter because ship travels!");
		}
	}

	@VisibleForTesting
	protected void validateTractorBeam(ShipTaskChangeRequest request, Ship ship) {
		if (!masterDataService.hasTractorBeam(ship.getPropulsionSystemTemplate().getTechLevel())) {
			throw new IllegalArgumentException("Ship has no tractor beam!");
		}

		long tractorShipId = Long.parseLong(request.getTaskValue());
		Ship towedShip = shipRepository.getReferenceById(tractorShipId);

		if (!ship.canTowShip(towedShip)) {
			throw new IllegalArgumentException("This ship cannot tow that!");
		}
	}
}
