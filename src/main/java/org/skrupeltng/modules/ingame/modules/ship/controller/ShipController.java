package org.skrupeltng.modules.ingame.modules.ship.controller;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetService;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.politics.service.PlayerEncounterService;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntry;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteResourceAction;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTactics;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipOptionsService;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipOrdersService;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipRouteService;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipTransportService;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.ingame.service.round.combat.space.CombatSimulator;
import org.skrupeltng.modules.ingame.service.round.combat.space.SpaceCombatSimulationResultDTO;
import org.skrupeltng.modules.masterdata.database.Resource;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/ingame/ship")
public class ShipController extends AbstractShipController {

	private static final Logger log = LoggerFactory.getLogger(ShipController.class);

	private static final String FLEETS = "fleets";

	private final ShipOrdersService shipOrdersService;
	private final ShipOptionsService shipOptionsService;
	private final ShipTransportService shipTransportService;
	private final ShipRouteService shipRouteService;
	private final FleetService fleetService;
	private final PoliticsService politicsService;
	private final PlayerEncounterService playerEncounterService;
	private final CombatSimulator combatSimulator;

	public ShipController(ShipOrdersService shipOrdersService, ShipOptionsService shipOptionsService, ShipTransportService shipTransportService, ShipRouteService shipRouteService, FleetService fleetService, PoliticsService politicsService, PlayerEncounterService playerEncounterService, CombatSimulator combatSimulator) {
		this.shipOrdersService = shipOrdersService;
		this.shipOptionsService = shipOptionsService;
		this.shipTransportService = shipTransportService;
		this.shipRouteService = shipRouteService;
		this.fleetService = fleetService;
		this.politicsService = politicsService;
		this.playerEncounterService = playerEncounterService;
		this.combatSimulator = combatSimulator;
	}

	@GetMapping("")
	public String getShip(@RequestParam long shipId, @RequestParam(required = false) String subPage, Model model) {
		try {
			Ship ship = shipService.getShip(shipId);
			model.addAttribute("ship", ship);

			boolean turnDone = turnDoneForShip(shipId);
			model.addAttribute("turnDone", turnDone && !configProperties.isDisablePermissionChecks());

			model.addAttribute(FLEETS, fleetService.getPossibleFleetsForShip(shipId));

			addCloakingInfo(ship);

			if (!turnDone) {
				if (StringUtils.isBlank(subPage)) {
					if (ship.showNavigationTab()) {
						subPage = "orders";
					} else {
						subPage = "transporter";
					}
				}

				model.addAttribute("subPage", subPage);
				String subPageFragment = null;

				switch (subPage) {
					case "orders" -> subPageFragment = getOrders(shipId, model);
					case "scanner" -> subPageFragment = getScannerOverview(shipId, model);
					case "transporter" -> subPageFragment = getTransporter(shipId, model);
					case "route" -> subPageFragment = getRoute(shipId, model);
					case "tactics" -> subPageFragment = getTactics(shipId, model);
					case "weapon-systems" -> subPageFragment = getWeaponSystems(shipId, model);
					case "storage" -> subPageFragment = getStorage(shipId, model);
					case "propulsion-system" -> subPageFragment = getPropulsionSystem(shipId, model);
					case "options" -> subPageFragment = getOptions(shipId, model);
					default -> log.error("Unexpected value for sub page parameter!");
				}

				model.addAttribute("subPageFragment", subPageFragment);
			}

			return "ingame/ship/ship::content";
		} catch (AccessDeniedException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found");
		}
	}

	private void addCloakingInfo(Ship ship) {
		if (ship.isCloaked()) {
			ShipCloakingSummary cloakingSummary = shipService.getCloakingSummary(ship);
			ship.setCloakingSummary(cloakingSummary);
		}
	}

	@GetMapping("/orders")
	public String getOrders(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);

		Player player = ship.getPlayer();
		Game game = player.getGame();
		model.addAttribute("mineFieldsEnabled", game.isEnableMineFields());

		long loginId = userDetailService.getLoginId();
		List<Ship> allShips = shipService.getShipsOfLogin(game.getId(), loginId, null, null);
		allShips.remove(ship);
		model.addAttribute("allShips", allShips);

		boolean escorting = ship.getDestinationShip() != null && allShips.contains(ship.getDestinationShip());

		if (escorting) {
			model.addAttribute("escortTarget", ship.getDestinationShip());
		}

		ShipAbility activeAbility = ship.getActiveAbility();

		if (activeAbility != null) {
			model.addAttribute("activeAbility", activeAbility.getType());
		}

		List<Ship> towableShips = allShips.stream().filter(ship::canTowShip).toList();
		model.addAttribute("towableShips", towableShips);
		model.addAttribute("showTractorBeam", !towableShips.isEmpty() && masterDataService.hasTractorBeam(ship.getPropulsionSystemTemplate().getTechLevel()));

		if (ship.getAbility(ShipAbilityType.SIGNATURE_MASK).isPresent()) {
			addSignatureMaskData(model, player, game);
		}

		if (ship.getAbility(ShipAbilityType.OVERDRIVE).isPresent()) {
			addOverdriveData(model, ship);
		}

		model.addAttribute("hasAutoDestruction", ship.getShipModule() == ShipModule.AUTO_DESTRUCTION);

		Planet planet = ship.getPlanet();
		ShipTemplate shipTemplate = ship.getShipTemplate();

		if (planet != null && planet.hasOrbitalSystem(OrbitalSystemType.HUNTER_ACADEMY) && shipTemplate.getMass() < 100) {
			Starbase starbase = planet.getStarbase();

			if (starbase != null && starbase.getType() == StarbaseType.BATTLE_STATION && ship.getExperience() < 5 && ship.getTravelSpeed() == 0) {
				model.addAttribute("canTrain", true);
			}
		}

		Map<Long, ShipAbilityDescription> descriptions = shipTemplate.getShipAbilities().stream().map(shipAbilityDescriptionMapper::mapShipAbilityDescription)
			.collect(Collectors.toMap(ShipAbilityDescription::getAbilityId, s -> s));
		model.addAttribute("abilityDescriptions", descriptions);

		return getNavigation(ship, model, "ingame/ship/orders::content");
	}

	private void addOverdriveData(Model model, Ship ship) {
		int min = ship.getAbilityValue(ShipAbilityType.OVERDRIVE, ShipAbilityConstants.OVERDRIVE_MIN).orElseThrow().intValue();
		int max = ship.getAbilityValue(ShipAbilityType.OVERDRIVE, ShipAbilityConstants.OVERDRIVE_MAX).orElseThrow().intValue();

		List<Integer> overDriveSteps = new ArrayList<>();

		for (int step = min; step <= max; step += 10) {
			overDriveSteps.add(step);
		}

		model.addAttribute("overDriveSteps", overDriveSteps);
	}

	private void addSignatureMaskData(Model model, Player player, Game game) {
		if (game.isEnableWysiwyg()) {
			List<Player> encounteredPlayer = playerEncounterService.getEncounteredPlayer(player.getId());
			encounteredPlayer.add(0, player);
			model.addAttribute("players", encounteredPlayer);
		} else {
			model.addAttribute("players", game.getPlayers());
		}
	}

	@PostMapping("/navigation")
	@ResponseBody
	public String setCourse(@RequestParam long shipId, @RequestBody ShipCourseSelectionRequest request) {
		return shipOrdersService.setCourse(shipId, request);
	}

	@GetMapping("/navigation")
	public String getShipNavigationContent(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		return getNavigation(ship, model, "ingame/ship/orders::navigation");
	}

	@PostMapping("/navigation/follow-leader")
	@ResponseBody
	public String followLeader(@RequestParam long shipId) {
		return fleetService.followLeader(shipId);
	}

	@DeleteMapping("/navigation")
	@ResponseBody
	public void deleteCourse(@RequestParam long shipId) {
		shipOrdersService.deleteCourse(shipId);
	}

	@GetMapping("/transporter")
	public String getTransporter(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		Planet planet = shipTransportService.getNearestPlanetInExtendedTransporterRange(ship);

		Player player = ship.getPlayer();
		long shipPlayerId = player.getId();
		int distance = getTransportRange(ship);

		List<Ship> ships = shipService.getOwnShipsOnPosition(player.getGame().getId(), shipPlayerId, ship.getX(), ship.getY(), distance).stream()
			.filter(s -> s.getId() != ship.getId())
			.toList();

		if (planet != null || !ships.isEmpty()) {
			List<ShipTransportTarget> shipTargets = ships.stream()
				.map(s -> new ShipTransportTarget(ship, s))
				.toList();

			final ShipTransportTarget selectedTarget;

			if (planet != null) {
				selectedTarget = new ShipTransportTarget(ship, planet);
				model.addAttribute("planetTarget", selectedTarget);
			} else {
				selectedTarget = shipTargets.get(0);
				model.addAttribute("targetShip", ships.get(0));
			}

			model.addAttribute("shipTargets", shipTargets);

			addTransporterModelData(model, ship, selectedTarget);

			return "ingame/ship/transporter::content";
		}

		return "ingame/ship/transporter::cannot-transport";
	}

	private int getTransportRange(Ship ship) {
		Optional<Float> abilityValue = ship.getAbilityValue(ShipAbilityType.EXTENDED_TRANSPORTER, ShipAbilityConstants.EXTENDED_TRANSPORTER_RANGE);

		if (abilityValue.isPresent()) {
			return abilityValue.get().intValue();
		}

		return 0;
	}

	private void addTransporterModelData(Model model, Ship ship, ShipTransportTarget selectedTarget) {
		model.addAttribute("ship", ship);
		model.addAttribute("playerId", ship.getPlayer().getId());
		model.addAttribute("selectedTarget", selectedTarget);
		model.addAttribute("isInfantryTransporter", ship.getAbility(ShipAbilityType.INFANTRY_TRANSPORT).isPresent());
	}

	@GetMapping("/transporter/target")
	public String getTransportTargetView(@RequestParam long shipId, @RequestParam long id, @RequestParam boolean isPlanet, Model model) {
		Ship ship = shipService.getShip(shipId);

		Player player = ship.getPlayer();
		long shipPlayerId = player.getId();
		final ShipTransportTarget selectedTarget;

		if (isPlanet) {
			Planet planet = shipTransportService.getNearestPlanetInExtendedTransporterRange(ship);

			if (planet == null) {
				throw new IllegalArgumentException("Planet not found!");
			}

			selectedTarget = new ShipTransportTarget(ship, planet);
		} else {
			int distance = getTransportRange(ship);
			List<Ship> ships = shipService.getOwnShipsOnPosition(player.getGame().getId(), shipPlayerId, ship.getX(), ship.getY(), distance);

			Optional<Ship> shipOpt = ships.stream().filter(s -> s.getId() == id).findFirst();

			if (shipOpt.isEmpty()) {
				throw new IllegalArgumentException("Ship not found!");
			}

			selectedTarget = new ShipTransportTarget(ship, shipOpt.get());
		}

		addTransporterModelData(model, ship, selectedTarget);

		return "ingame/ship/transporter::sliders";
	}

	@GetMapping("/transporter/capacity-panel/to-planet")
	public String getTransportToPlanetCapacityPanel(@RequestParam("shipId") long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);

		return "ingame/ship/transporter::capacity-panel-to-planet";
	}

	@GetMapping("/transporter/capacity-panel/to-ship")
	public String getTransportToShipCapacityPanel(@RequestParam("shipId") long shipId, @RequestParam("targetShipId") long targetShipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);

		Ship targetShip = shipService.getShip(targetShipId);
		model.addAttribute("targetShip", targetShip);

		return "ingame/ship/transporter::capacity-panel-to-ship";
	}

	@PostMapping("/transporter")
	public String transport(@RequestParam long shipId, @RequestBody ShipTransportRequest request) {
		shipTransportService.transport(shipId, request);
		return "ingame/ship/transporter::transport-successfull";
	}

	@GetMapping("/scanner")
	public String getScannerOverview(@RequestParam long shipId, Model model) {
		List<Ship> ships = shipService.getScannedShips(shipId);

		model.addAttribute("scanner", true);
		model.addAttribute("ships", ships);

		return "ingame/ship/scanner::content";
	}

	@GetMapping("/scanner/ship")
	public String getScannedShipDetails(@RequestParam long shipId, @RequestParam long scannedShipId, Model model) {
		long playerId = shipService.getShip(shipId).getPlayer().getId();

		Ship ship = shipService.getScannedShip(playerId, scannedShipId);
		model.addAttribute("ship", ship);

		SpaceCombatSimulationResultDTO combatSimulationResult = combatSimulator.getShipCombatSuccessProbability(shipId, scannedShipId);
		model.addAttribute("combatSimulationResult", combatSimulationResult);

		return "ingame/ship/scanner::ship";
	}

	@GetMapping("/logbook")
	public String getLogbook(@RequestParam long shipId, Model model) {
		model.addAttribute("logbook", shipService.getLogbook(shipId));
		return "ingame/logbook::content";
	}

	@PostMapping("/logbook")
	public String changeLogbook(@RequestParam long shipId, @RequestParam String logbook) {
		shipService.changeLogbook(shipId, logbook);
		return "ingame/logbook::success";
	}

	@GetMapping("/ship-template")
	public String shipTemplateDetails(@RequestParam String shipTemplateId, Model model) {
		ShipTemplate shipTemplate = masterDataService.getShipTemplate(shipTemplateId);
		model.addAttribute("template", shipTemplate);

		List<ShipAbilityDescription> descriptions = shipTemplate.getShipAbilities().stream().map(shipAbilityDescriptionMapper::mapShipAbilityDescription).toList();
		model.addAttribute("abilityDescriptions", descriptions);
		model.addAttribute("modal", true);

		return "ship-template-details::content";
	}

	@PostMapping("/task")
	public String changeTask(@RequestBody ShipTaskChangeRequest request, Model model) {
		shipOrdersService.changeTask(request.getShipId(), request);
		return getOrders(request.getShipId(), model);
	}

	@GetMapping("/travel-data")
	@ResponseBody
	public ShipTravelData getShipTravelData(@RequestParam long shipId) {
		Ship ship = shipService.getShip(shipId);
		return new ShipTravelData(ship);
	}

	@GetMapping("/route-data")
	public String getShipRouteData(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShipWithRoutesFetched(shipId);
		model.addAttribute("ship", ship);
		return "ingame/galaxymap/galaxy-map::ship-routes";
	}

	@GetMapping("/tactics")
	public String getTactics(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);

		model.addAttribute("ship", ship);
		model.addAttribute("tacticsList", ShipTactics.values());

		boolean enableTactialCombat = ship.getPlayer().getGame().isEnableTactialCombat();
		model.addAttribute("enableTactialCombat", enableTactialCombat);

		Locale locale = LocaleContextHolder.getLocale();
		String helpText = messageSource.getMessage("help_ship_aggressiveness", null, locale);

		if (enableTactialCombat) {
			helpText += "<br/><br/>";
			helpText += messageSource.getMessage("help_ship_tactics", null, locale);
		}

		model.addAttribute("helpText", helpText);

		return "ingame/ship/tactics::content";
	}

	@PostMapping("/tactics")
	public String changeTactics(@RequestBody ShipTacticsChangeRequest request) {
		shipService.changeTactics(request.getShipId(), request);
		return "ingame/ship/tactics::success";
	}

	@GetMapping("/storage")
	public String getStorage(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);
		return "ingame/ship/storage::content";
	}

	@GetMapping("/weapon-systems")
	public String getWeaponSystems(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);

		boolean canAutoBuildProjectiles = shipService.getBuildableProjectiles(shipId) > 0;
		model.addAttribute("canAutoBuildProjectiles", canAutoBuildProjectiles);

		return "ingame/ship/weapon-systems::content";
	}

	@GetMapping("/propulsion-system")
	public String getPropulsionSystem(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);
		return "ingame/ship/propulsion-system::content";
	}

	@PostMapping("/projectiles")
	@ResponseBody
	public void buildProjectiles(@RequestParam long shipId, @RequestParam int quantity) {
		shipService.buildProjectiles(shipId, quantity);
	}

	@PostMapping("/automated-projectiles")
	@ResponseBody
	public void toggleAutoBuildProjectiles(@RequestParam long shipId) {
		shipService.toggleAutoBuildProjectiles(shipId);
	}

	@PostMapping("/projectiles-auto")
	@ResponseBody
	public void buildAllPossibleProjectilesAutomatically(@RequestParam long shipId) {
		shipService.buildAllPossibleProjectilesAutomatically(shipId);
	}

	@GetMapping("/stats-details")
	public String getStatsDetails(@RequestParam long shipId, @RequestParam Boolean forSelection, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);
		model.addAttribute("forSelection", forSelection);
		model.addAttribute(FLEETS, fleetService.getPossibleFleetsForShip(shipId));
		model.addAttribute("turnDone", turnDoneForShip(shipId) && !configProperties.isDisablePermissionChecks());

		addCloakingInfo(ship);

		return "ingame/ship/ship::stats-details";
	}

	@GetMapping("/fleet-dropdown-list")
	public String getFleetDropdownList(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);
		model.addAttribute(FLEETS, fleetService.getPossibleFleetsForShip(shipId));

		return "ingame/ship/ship::fleet-dropdown-list";
	}

	@GetMapping("/route")
	public String getRoute(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);

		ship.setTravelSpeed(ship.getRouteTravelSpeed());

		model.addAttribute("ship", ship);
		List<Planet> planets = new ArrayList<>(ship.getPlayer().getPlanets());
		planets.sort(Comparator.comparing(Planet::getName));
		model.addAttribute("planets", planets);

		List<ShipRouteEntry> entries = shipRouteService.getRoute(shipId);
		model.addAttribute("entries", entries);

		model.addAttribute("actions", ShipRouteResourceAction.values());
		model.addAttribute("resources", Resource.values());

		return "ingame/ship/route::content";
	}

	@PutMapping("/route-entry")
	public String addRouteEntry(@RequestParam long shipId, @RequestParam long planetId, @RequestParam String action, Model model) {
		shipRouteService.addRouteEntry(shipId, planetId, ShipRouteResourceAction.valueOf(action));
		return getRoute(shipId, model);
	}

	@PostMapping("/route-entry-action")
	@ResponseBody
	public void changeRouteEntryAction(@RequestParam long entryId, @RequestParam String type, @RequestParam String action) {
		shipRouteService.changeRouteEntryAction(entryId, Resource.valueOf(type), ShipRouteResourceAction.valueOf(action));
	}

	@PostMapping("/route-entry-planet")
	@ResponseBody
	public void changeRouteEntryPlanet(@RequestParam long entryId, @RequestParam long planetId) {
		shipRouteService.changeRouteEntryPlanet(entryId, planetId);
	}

	@PostMapping("/route-entry-wait-for-full-storage")
	@ResponseBody
	public void toggleWaitForFullStorage(@RequestParam long entryId) {
		shipRouteService.toggleWaitForFullStorage(entryId);
	}

	@PostMapping("/move-route-entry-up")
	public String moveRouteEntryUp(@RequestParam long entryId, Model model) {
		long shipId = shipRouteService.moveRouteEntryUp(entryId);
		return getRoute(shipId, model);
	}

	@PostMapping("/move-route-entry-down")
	public String moveRouteEntryDown(@RequestParam long entryId, Model model) {
		long shipId = shipRouteService.moveRouteEntryDown(entryId);
		return getRoute(shipId, model);
	}

	@DeleteMapping("/route-entry")
	public String deleteRouteEntry(@RequestParam long entryId, Model model) {
		long shipId = shipRouteService.deleteRouteEntry(entryId);
		return getRoute(shipId, model);
	}

	@PostMapping("/route-travel-speed")
	@ResponseBody
	public void setRouteTravelSpeed(@RequestParam long shipId, @RequestParam int travelSpeed) {
		shipRouteService.setRouteTravelSpeed(shipId, travelSpeed);
	}

	@PostMapping("/route-disabled")
	@ResponseBody
	public String setRouteDisabled(@RequestParam long shipId, @RequestParam boolean disabled) {
		return shipRouteService.setRouteDisabled(shipId, disabled);
	}

	@PostMapping("/route-primary-resource")
	@ResponseBody
	public void setRoutePrimaryResource(@RequestParam long shipId, @RequestParam String primaryResource) {
		shipRouteService.setRoutePrimaryResource(shipId, Resource.valueOf(primaryResource));
	}

	@PostMapping("/route-min-fuel")
	@ResponseBody
	public void setRouteMinFuel(@RequestParam long shipId, @RequestParam int minFuel) {
		shipRouteService.setRouteMinFuel(shipId, minFuel);
	}

	@GetMapping("/options")
	public String getOptions(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);
		model.addAttribute("logbook", shipService.getLogbook(shipId));

		if (ship.getPlayer().getGame().getStoryModeCampaign() == null) {
			List<Player> allies = politicsService.getPlayersWithRelation(ship.getPlayer().getId(), PlayerRelationType.ALLIANCE);
			model.addAttribute("allies", allies);
		} else {
			model.addAttribute("allies", Collections.emptyList());
		}

		return "ingame/ship/settings::content";
	}

	@PostMapping("/options")
	public String changeOptions(@RequestParam long shipId, @RequestBody ShipOptionsChangeRequest request) {
		shipOptionsService.changeOptions(shipId, request);
		return "ingame/ship/settings::name-changed";
	}
}
