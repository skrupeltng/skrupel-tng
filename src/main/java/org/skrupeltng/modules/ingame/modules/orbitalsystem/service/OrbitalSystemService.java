package org.skrupeltng.modules.ingame.modules.orbitalsystem.service;

import org.jetbrains.annotations.VisibleForTesting;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.orbitalsystem.controller.GroundUnitProductionData;
import org.skrupeltng.modules.ingame.modules.orbitalsystem.controller.GroundUnitProductionRequest;
import org.skrupeltng.modules.ingame.modules.orbitalsystem.controller.MineralTransformationData;
import org.skrupeltng.modules.ingame.modules.orbitalsystem.controller.TransformMineralsRequest;
import org.skrupeltng.modules.ingame.modules.planet.database.*;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OrbitalSystemService {

	private final OrbitalSystemRepository orbitalSystemRepository;
	private final PlanetRepository planetRepository;
	private final ShipRepository shipRepository;
	private final ConfigProperties configProperties;

	public OrbitalSystemService(OrbitalSystemRepository orbitalSystemRepository, PlanetRepository planetRepository, ShipRepository shipRepository, ConfigProperties configProperties) {
		this.orbitalSystemRepository = orbitalSystemRepository;
		this.planetRepository = planetRepository;
		this.shipRepository = shipRepository;
		this.configProperties = configProperties;
	}

	@PreAuthorize("hasPermission(#orbitalSystemId, 'orbitalSystem') and hasPermission(#orbitalSystemId, 'turnNotDoneOrbitalSystem')")
	public OrbitalSystem get(long orbitalSystemId) {
		return orbitalSystemRepository.getReferenceById(orbitalSystemId);
	}

	@PreAuthorize("hasPermission(#orbitalSystemId, 'orbitalSystem') and hasPermission(#orbitalSystemId, 'turnNotDoneOrbitalSystem')")
	@Transactional(propagation = Propagation.REQUIRED)
	public Planet construct(long orbitalSystemId, OrbitalSystemType type) {
		OrbitalSystem orbitalSystem = orbitalSystemRepository.getReferenceById(orbitalSystemId);

		return constructWithoutPermissionsCheck(type, orbitalSystem);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Planet constructWithoutPermissionsCheck(OrbitalSystemType type, OrbitalSystem orbitalSystem) {
		if (orbitalSystem.getType() != null) {
			throw new IllegalArgumentException("OrbitalSystem " + orbitalSystem.getId() + " already has a type!");
		}

		Planet planet = orbitalSystem.getPlanet();

		if (!type.canBeBuild(planet)) {
			throw new IllegalArgumentException("Planet " + planet.getId() + " does not have enough resources to build orbital system " + type + "!");
		}

		if (planet.getOrbitalSystems().stream().anyMatch(o -> o.getType() == type)) {
			throw new IllegalArgumentException("Planet " + planet.getId() + " already has an OrbitalSystem with type '" + type + "'!");
		}

		if (!planet.getPlayer().getFaction().orbitalSystemAllowed(type)) {
			throw new IllegalArgumentException("OrbitalSystem not allowed for this faction!");
		}

		orbitalSystem.setType(type);
		orbitalSystem = orbitalSystemRepository.save(orbitalSystem);

		planet = orbitalSystem.getPlanet();
		planet.spendMoney(type.getMoney());
		planet.spendSupplies(type.getSupplies());
		planet.spendFuel(type.getFuel());
		planet.spendMineral1(type.getMineral1());
		planet.spendMineral2(type.getMineral2());
		planet.spendMineral3(type.getMineral3());

		planet = planetRepository.save(planet);

		if (type == OrbitalSystemType.ORBITAL_EXTENSION) {
			int size = planet.getOrbitalSystems().size();

			if (size < 6) {
				orbitalSystem = new OrbitalSystem(planet);
				orbitalSystemRepository.save(orbitalSystem);
			}

			if (size < 5) {
				orbitalSystem = new OrbitalSystem(planet);
				orbitalSystemRepository.save(orbitalSystem);
			}
		}

		return planet;
	}

	@PreAuthorize("hasPermission(#orbitalSystemId, 'orbitalSystem') and hasPermission(#orbitalSystemId, 'turnNotDoneOrbitalSystem')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void tearDownOrbitalSystem(long orbitalSystemId) {
		OrbitalSystem orbitalSystem = orbitalSystemRepository.getReferenceById(orbitalSystemId);

		verifyThatOrbitalSystemCanBeTornDown(orbitalSystem);

		Planet planet = orbitalSystem.getPlanet();

		planet.spendMoney(orbitalSystem.getTearDownMoneyCosts());
		planet.spendSupplies(orbitalSystem.getTearDownSupplyCosts());
		planet.spendFuel(orbitalSystem.getTearDownFuelCosts());
		planet.spendMineral1(orbitalSystem.getTearDownMineral1Costs());
		planet.spendMineral2(orbitalSystem.getTearDownMineral2Costs());
		planet.spendMineral3(orbitalSystem.getTearDownMineral3Costs());

		planetRepository.save(planet);

		orbitalSystem.setType(null);
		orbitalSystemRepository.save(orbitalSystem);
	}

	@VisibleForTesting
	protected void verifyThatOrbitalSystemCanBeTornDown(OrbitalSystem orbitalSystem) {
		if (!orbitalSystem.canBeTornDown()) {
			throw new IllegalArgumentException("Cannot be torn down!");
		}
	}

	@PreAuthorize("hasPermission(#orbitalSystemId, 'orbitalSystem') and hasPermission(#orbitalSystemId, 'turnNotDoneOrbitalSystem')")
	public List<Ship> getScannedShips(long orbitalSystemId) {
		OrbitalSystem orbitalSystem = orbitalSystemRepository.getReferenceById(orbitalSystemId);
		Planet planet = orbitalSystem.getPlanet();
		Player player = planet.getPlayer();
		long playerId = player.getId();
		long gameId = player.getGame().getId();
		int scannerDist = planet.getScanRadius();

		if (planet.hasOrbitalSystem(OrbitalSystemType.PSY_CORPS)) {
			scannerDist = 90;
		}

		if (planet.getStarbase() != null && planet.getStarbase().getType().getTypeId() > 1) {
			scannerDist = 116;
		}

		int dist = scannerDist;

		List<Ship> ships = shipRepository.findByGameId(gameId);
		return ships.stream().filter(s -> s.getPlayer().getId() != playerId && s.getDistance(planet) <= dist).toList();
	}

	@PreAuthorize("hasPermission(#orbitalSystemId, 'orbitalSystem') and hasPermission(#orbitalSystemId, 'turnNotDoneOrbitalSystem')")
	public Ship getScannedShip(long orbitalSystemId, long scannedShipId) {
		Ship ship = shipRepository.getReferenceById(scannedShipId);

		if (getScannedShips(orbitalSystemId).contains(ship)) {
			return ship;
		}

		throw new IllegalArgumentException("Ship " + scannedShipId + " is not in scanner reach of OrbitalSystem " + orbitalSystemId + "!");
	}

	@PreAuthorize("hasPermission(#orbitalSystemId, 'orbitalSystem') and hasPermission(#orbitalSystemId, 'turnNotDoneOrbitalSystem')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void produceGroundUnits(long orbitalSystemId, GroundUnitProductionRequest request) {
		OrbitalSystem orbitalSystem = orbitalSystemRepository.getReferenceById(orbitalSystemId);
		Planet planet = orbitalSystem.getPlanet();

		GroundUnitProductionData data = new GroundUnitProductionData(orbitalSystem.getType(), planet);
		int quantity = request.getQuantity();

		if (quantity <= 0) {
			throw new IllegalArgumentException("Quantity must be higher than zero!");
		}

		if (quantity > data.retrieveNewlyProducable()) {
			throw new IllegalArgumentException("OrbitalSystem " + orbitalSystemId + " cannot produce ground units: " + request);
		}

		planet.setColonists(planet.getColonists() - (data.getColonistCosts() * quantity));
		planet.spendMoney(data.getMoneyCosts() * quantity);
		planet.spendSupplies(data.getSupplyCosts() * quantity);
		planet.spendFuel(data.getFuelCosts() * quantity);
		planet.spendMineral1(data.getMineral1Costs() * quantity);
		planet.spendMineral2(data.getMineral2Costs() * quantity);
		planet.spendMineral3(data.getMineral3Costs() * quantity);

		if (request.isLight()) {
			planet.setLightGroundUnitsProduction(planet.getLightGroundUnitsProduction() + quantity);
		} else {
			planet.setHeavyGroundUnitsProduction(planet.getHeavyGroundUnitsProduction() + quantity);
		}

		planetRepository.save(planet);
	}

	@PreAuthorize("hasPermission(#orbitalSystemId, 'orbitalSystem') and hasPermission(#orbitalSystemId, 'turnNotDoneOrbitalSystem')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void transformMinerals(long orbitalSystemId, TransformMineralsRequest request) {
		OrbitalSystem orbitalSystem = orbitalSystemRepository.getReferenceById(orbitalSystemId);
		Planet planet = orbitalSystem.getPlanet();

		int moneyRate = configProperties.getMineralTransformerMoneyRate();
		int sourceRate = configProperties.getMineralTransformerSourceRate();

		transformMineralsOnPlanet(request, planet, moneyRate, sourceRate);

		planetRepository.save(planet);
	}

	protected void transformMineralsOnPlanet(TransformMineralsRequest request, Planet planet, int moneyRate, int sourceRate) {
		int requestMoneyValue = request.getMoneyValue();
		int requestSourceValue = request.getSourceValue();

		MineralTransformationData transformationData = new MineralTransformationData(requestMoneyValue, moneyRate, requestSourceValue, sourceRate);
		int targetValue = transformationData.getMaxTarget();

		if (targetValue <= 0) {
			throw new IllegalArgumentException("No minerals can be transformed with this request!");
		}

		planet.spendMoney(requestMoneyValue);

		switch (request.getSourceMineral()) {
			case "mineral1" -> planet.spendMineral1(requestSourceValue);
			case "mineral2" -> planet.spendMineral2(requestSourceValue);
			case "mineral3" -> planet.spendMineral3(requestSourceValue);
			default -> throw new IllegalStateException("Unexpected value: " + request.getSourceMineral());
		}

		switch (request.getTargetMineral()) {
			case "mineral1" -> planet.spendMineral1(-targetValue);
			case "mineral2" -> planet.spendMineral2(-targetValue);
			case "mineral3" -> planet.spendMineral3(-targetValue);
			default -> throw new IllegalStateException("Unexpected value: " + request.getSourceMineral());
		}
	}

	@PreAuthorize("hasPermission(#orbitalSystemId, 'orbitalSystem')")
	public long getPlanetId(long orbitalSystemId) {
		return orbitalSystemRepository.getPlanetId(orbitalSystemId);
	}
}
