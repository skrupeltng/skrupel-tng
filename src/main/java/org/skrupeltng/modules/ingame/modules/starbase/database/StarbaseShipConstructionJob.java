package org.skrupeltng.modules.ingame.modules.starbase.database;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.database.WeaponTemplate;

@Entity
@Table(name = "starbase_ship_construction_job")
public class StarbaseShipConstructionJob implements Serializable {

	private static final long serialVersionUID = -1727871799782125060L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "ship_name")
	private String shipName;

	@ManyToOne(fetch = FetchType.EAGER)
	private Starbase starbase;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ship_template_id")
	private ShipTemplate shipTemplate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "propulsion_system_template_name")
	private PropulsionSystemTemplate propulsionSystemTemplate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "energy_weapon_template_name")
	private WeaponTemplate energyWeaponTemplate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "projectile_weapon_template_name")
	private WeaponTemplate projectileWeaponTemplate;

	@Enumerated(EnumType.STRING)
	@Column(name = "ship_module")
	private ShipModule shipModule;

	@Column(name = "fleet_id")
	private Long fleetId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public Starbase getStarbase() {
		return starbase;
	}

	public void setStarbase(Starbase starbase) {
		this.starbase = starbase;
	}

	public ShipTemplate getShipTemplate() {
		return shipTemplate;
	}

	public void setShipTemplate(ShipTemplate shipTemplate) {
		this.shipTemplate = shipTemplate;
	}

	public PropulsionSystemTemplate getPropulsionSystemTemplate() {
		return propulsionSystemTemplate;
	}

	public void setPropulsionSystemTemplate(PropulsionSystemTemplate propulsionSystemTemplate) {
		this.propulsionSystemTemplate = propulsionSystemTemplate;
	}

	public WeaponTemplate getEnergyWeaponTemplate() {
		return energyWeaponTemplate;
	}

	public void setEnergyWeaponTemplate(WeaponTemplate energyWeaponTemplate) {
		this.energyWeaponTemplate = energyWeaponTemplate;
	}

	public WeaponTemplate getProjectileWeaponTemplate() {
		return projectileWeaponTemplate;
	}

	public void setProjectileWeaponTemplate(WeaponTemplate projectileWeaponTemplate) {
		this.projectileWeaponTemplate = projectileWeaponTemplate;
	}

	public ShipModule getShipModule() {
		return shipModule;
	}

	public void setShipModule(ShipModule shipModule) {
		this.shipModule = shipModule;
	}

	public Long getFleetId() {
		return fleetId;
	}

	public void setFleetId(Long fleetId) {
		this.fleetId = fleetId;
	}
}
