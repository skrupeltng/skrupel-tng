package org.skrupeltng.modules.ingame.modules.orbitalsystem.controller;

import java.io.Serializable;

public class GroundUnitProductionRequest implements Serializable {

	private static final long serialVersionUID = -2040363244390471061L;

	private long orbitalSystemId;
	private int quantity;
	private boolean light;

	public long getOrbitalSystemId() {
		return orbitalSystemId;
	}

	public void setOrbitalSystemId(long orbitalSystemId) {
		this.orbitalSystemId = orbitalSystemId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public boolean isLight() {
		return light;
	}

	public void setLight(boolean light) {
		this.light = light;
	}

	@Override
	public String toString() {
		return "GroundUnitProductionRequest [orbitalSystemId=" + orbitalSystemId + ", quantity=" + quantity + ", light=" + light + "]";
	}
}