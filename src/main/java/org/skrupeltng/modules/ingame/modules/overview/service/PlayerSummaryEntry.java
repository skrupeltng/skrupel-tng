package org.skrupeltng.modules.ingame.modules.overview.service;

public class PlayerSummaryEntry {

	private long playerId;
	private String name;
	private String teamName;
	private String factionId;
	private String color;
	private boolean isAi;
	private int starbaseCount;
	private int planetCount;
	private int shipCount;
	private int starbaseRank;
	private int planetRank;
	private int shipRank;
	private int rank;
	private boolean turnFinished;
	private boolean hasLost;

	public long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getFactionId() {
		return factionId;
	}

	public void setFactionId(String factionId) {
		this.factionId = factionId;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public boolean isAi() {
		return isAi;
	}

	public void setAi(boolean isAi) {
		this.isAi = isAi;
	}

	public int getStarbaseCount() {
		return starbaseCount;
	}

	public void setStarbaseCount(int starbaseCount) {
		this.starbaseCount = starbaseCount;
	}

	public int getPlanetCount() {
		return planetCount;
	}

	public void setPlanetCount(int planetCount) {
		this.planetCount = planetCount;
	}

	public int getShipCount() {
		return shipCount;
	}

	public void setShipCount(int shipCount) {
		this.shipCount = shipCount;
	}

	public int getStarbaseRank() {
		return starbaseRank;
	}

	public void setStarbaseRank(int starbaseRank) {
		this.starbaseRank = starbaseRank;
	}

	public int getPlanetRank() {
		return planetRank;
	}

	public void setPlanetRank(int planetRank) {
		this.planetRank = planetRank;
	}

	public int getShipRank() {
		return shipRank;
	}

	public void setShipRank(int shipRank) {
		this.shipRank = shipRank;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public boolean isTurnFinished() {
		return turnFinished;
	}

	public void setTurnFinished(boolean turnFinished) {
		this.turnFinished = turnFinished;
	}

	public int retrieveTotalCount() {
		return planetCount + shipCount + starbaseCount;
	}

	public boolean isHasLost() {
		return hasLost;
	}

	public void setHasLost(boolean hasLost) {
		this.hasLost = hasLost;
	}

	@Override
	public String toString() {
		return "PlayerSummaryEntry [name=" + name + ", factionId=" + factionId + ", starbaseCount=" + starbaseCount + ", planetCount=" + planetCount +
			", shipCount=" + shipCount + "]";
	}
}
