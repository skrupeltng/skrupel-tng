package org.skrupeltng.modules.ingame.modules.overview.database;

import java.util.HashMap;
import java.util.Map;

import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.ingame.modules.overview.service.RoundSummary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

public class NewsEntryRepositoryImpl extends RepositoryCustomBase implements NewsEntryRepositoryCustom {

	@Override
	public void deleteByGameId(long gameId) {
		String newsEntrySubSelect = "(SELECT e.id FROM news_entry e WHERE e.game_id = :gameId AND delete = true)";

		String sql = "" +
			"DELETE FROM " +
			"	space_combat_log_entry_item " +
			"WHERE " +
			"	space_combat_log_entry_id IN (" +
			"		SELECT " +
			"			l.id " +
			"		FROM " +
			"			space_combat_log_entry l " +
			"		WHERE " +
			"			l.news_entry_id IN " + newsEntrySubSelect + ") ";

		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);

		jdbcTemplate.update(sql, params);

		sql = "" +
			"DELETE FROM " +
			"	space_combat_log_entry " +
			"WHERE " +
			"	news_entry_id IN " + newsEntrySubSelect;

		jdbcTemplate.update(sql, params);

		sql = "" +
			"DELETE FROM " +
			"	orbital_combat_log_entry " +
			"WHERE " +
			"	news_entry_id IN " + newsEntrySubSelect;

		jdbcTemplate.update(sql, params);

		sql = "" +
			"DELETE FROM " +
			"	small_meteor_log_entry " +
			"WHERE " +
			"	news_entry_id IN " + newsEntrySubSelect;

		jdbcTemplate.update(sql, params);

		sql = "" +
			"DELETE FROM " +
			"	shock_wave_destruction_log_entry " +
			"WHERE " +
			"	news_entry_id IN " + newsEntrySubSelect;

		jdbcTemplate.update(sql, params);

		sql = "" +
			"DELETE FROM " +
			"	news_entry_argument " +
			"WHERE " +
			"	news_entry_id IN " + newsEntrySubSelect;

		jdbcTemplate.update(sql, params);

		sql = "" +
			"DELETE FROM " +
			"	news_entry " +
			"WHERE " +
			"	game_id = :gameId " +
			"	AND delete = true";

		jdbcTemplate.update(sql, params);
	}

	@Override
	public RoundSummary getRoundSummary(long gameId, long playerId) {
		String sql = """
			SELECT 
				g.id,
				g.round, 
				g.round_date as "date", 
				g.win_condition as "winCondition",
				
				SUM(COALESCE(p.last_round_new_colonies, 0)) as "newColonies", 
				SUM(COALESCE(p.last_round_new_ships, 0)) as "newShips", 
				SUM(COALESCE(p.last_round_new_starbases, 0)) "newStarbases", 
				SUM(COALESCE(p.last_round_destroyed_ships, 0)) as "destroyedShips", 
				SUM(COALESCE(p.last_round_lost_ships, 0)) as "lostShips", 
				
				cp.last_round_new_colonies as "newColoniesPlayer", 
				cp.last_round_new_ships as "newShipsPlayer", 
				cp.last_round_new_starbases "newStarbasesPlayer", 
				cp.last_round_destroyed_ships as "destroyedShipsPlayer", 
				cp.last_round_lost_ships as "lostShipsPlayer" 
			FROM 
				game g
				LEFT OUTER JOIN player p
					ON p.game_id = g.id AND p.id != :playerId,
				player cp
			WHERE 
				g.id = :gameId
				AND cp.id = :playerId
			GROUP BY
				g.id,
				g.round, 
				g.round_date,
				g.win_condition, 
				cp.last_round_new_colonies,
				cp.last_round_new_ships,
				cp.last_round_new_starbases,
				cp.last_round_destroyed_ships,
				cp.last_round_lost_ships 
			""";

		Map<String, Object> params = new HashMap<>(2);
		params.put("gameId", gameId);
		params.put("playerId", playerId);

		RowMapper<RoundSummary> rowMapper = new BeanPropertyRowMapper<>(RoundSummary.class);
		return jdbcTemplate.queryForObject(sql, params, rowMapper);
	}
}
