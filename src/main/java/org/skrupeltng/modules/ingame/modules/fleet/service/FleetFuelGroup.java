package org.skrupeltng.modules.ingame.modules.fleet.service;

import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

import java.util.ArrayList;
import java.util.List;

public class FleetFuelGroup {

	private final CoordinateImpl coordinate;
	private final List<Ship> ships = new ArrayList<>();

	private int currentFuelPercentage;
	private int currentFuelQuantity;
	private int maxFuelPercentage;
	private boolean allShipsHaveEqualFuel;

	public FleetFuelGroup(CoordinateImpl coordinate) {
		this.coordinate = coordinate;
	}

	public CoordinateImpl getCoordinate() {
		return coordinate;
	}

	public List<Ship> getShips() {
		return ships;
	}

	public void addShip(Ship ship) {
		ships.add(ship);
	}

	public int getCurrentFuelPercentage() {
		return currentFuelPercentage;
	}

	public void setCurrentFuelPercentage(int currentFuelPercentage) {
		this.currentFuelPercentage = currentFuelPercentage;
	}

	public int getCurrentFuelQuantity() {
		return currentFuelQuantity;
	}

	public void setCurrentFuelQuantity(int currentFuelQuantity) {
		this.currentFuelQuantity = currentFuelQuantity;
	}

	public int getMaxFuelPercentage() {
		return maxFuelPercentage;
	}

	public void setMaxFuelPercentage(int maxFuelPercentage) {
		this.maxFuelPercentage = maxFuelPercentage;
	}

	public boolean isAllShipsHaveEqualFuel() {
		return allShipsHaveEqualFuel;
	}

	public void setAllShipsHaveEqualFuel(boolean allShipsHaveEqualFuel) {
		this.allShipsHaveEqualFuel = allShipsHaveEqualFuel;
	}
}
