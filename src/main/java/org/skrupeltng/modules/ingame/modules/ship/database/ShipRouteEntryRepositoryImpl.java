package org.skrupeltng.modules.ingame.modules.ship.database;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.skrupeltng.modules.RepositoryCustomBase;

public class ShipRouteEntryRepositoryImpl extends RepositoryCustomBase implements ShipRouteEntryRepositoryCustom {

	@Override
	public boolean loginOwnsRoute(long shipRouteEntryId, long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	p.login_id \n" +
				"FROM \n" +
				"	ship_route_entry e \n" +
				"	INNER JOIN ship s \n" +
				"		ON s.id = e.ship_id \n" +
				"	INNER JOIN player p \n" +
				"		ON p.id = s.player_id \n" +
				"WHERE \n" +
				"	e.id = :shipRouteEntryId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("shipRouteEntryId", shipRouteEntryId);

		List<Long> results = jdbcTemplate.queryForList(sql, params, Long.class);
		return results.size() == 1 && results.get(0).longValue() == loginId;
	}
}