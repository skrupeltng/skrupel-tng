package org.skrupeltng.modules.ingame.modules.starbase.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface StarbaseRepository extends JpaRepository<Starbase, Long>, StarbaseRepositoryCustom {

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Starbase s " +
			"	INNER JOIN s.planet pl " +
			"	INNER JOIN pl.player p " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND p.login.id = ?2 " +
			"ORDER BY " +
			"	s.hullLevel + s.propulsionLevel + s.energyLevel + s.projectileLevel DESC")
	List<Starbase> findByGameIdAndLoginId(long gameId, long loginId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Starbase s " +
			"	INNER JOIN FETCH s.planet pl " +
			"WHERE " +
			"	pl.player.id = ?1")
	List<Starbase> findByPlayerId(long playerId);

	@Modifying
	@Query("UPDATE Starbase s SET s.log = null WHERE s.id IN (SELECT sb.id FROM Planet p INNER JOIN p.starbase sb WHERE p.player.id = ?1)")
	void clearLogBooksOfPlayer(long playerId);

	@Query("SELECT s.name FROM Starbase s WHERE s.id = ?1")
	String getName(long starbaseId);
}