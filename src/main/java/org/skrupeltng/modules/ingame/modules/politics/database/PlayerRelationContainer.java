package org.skrupeltng.modules.ingame.modules.politics.database;

import static org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationAction.CANCEL_ALLIANCE;
import static org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationAction.CANCEL_NON_AGGRESSION_TREATY;
import static org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationAction.CANCEL_TRADE_AGREEMENT;
import static org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationAction.DELCARE_WAR;
import static org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationAction.OFFER_ALLIANCE;
import static org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationAction.OFFER_NON_AGGRESSION_TREATY;
import static org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationAction.OFFER_PEACE;
import static org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationAction.OFFER_TRADE_AGREEMENT;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public class PlayerRelationContainer {

	private Long requestId;
	private long playerId;
	private String playerName;
	private String playerColor;
	private String relationType;
	private int roundsLeft;

	private boolean fixedRelations;

	private boolean toBeAccepted;
	private String requestedRelationType;

	private MessageSource messageSource;

	public Long getRequestId() {
		return requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getPlayerColor() {
		return playerColor;
	}

	public void setPlayerColor(String playerColor) {
		this.playerColor = playerColor;
	}

	public String getRelationType() {
		return relationType;
	}

	public void setRelationType(String relationType) {
		this.relationType = relationType;
	}

	public int getRoundsLeft() {
		return roundsLeft;
	}

	public void setRoundsLeft(int roundsLeft) {
		this.roundsLeft = roundsLeft;
	}

	public boolean isFixedRelations() {
		return fixedRelations;
	}

	public void setFixedRelations(boolean fixedRelations) {
		this.fixedRelations = fixedRelations;
	}

	public String getRequestedRelationType() {
		return requestedRelationType;
	}

	public void setRequestedRelationType(String requestedRelationType) {
		this.requestedRelationType = requestedRelationType;
	}

	public boolean isToBeAccepted() {
		return toBeAccepted;
	}

	public void setToBeAccepted(boolean toBeAccepted) {
		this.toBeAccepted = toBeAccepted;
	}

	public List<PlayerRelationAction> retrieveActions() {
		if (requestedRelationType != null || roundsLeft > 0 || fixedRelations) {
			return Collections.emptyList();
		}

		if (relationType == null) {
			return List.of(DELCARE_WAR, OFFER_TRADE_AGREEMENT, OFFER_NON_AGGRESSION_TREATY, OFFER_ALLIANCE);
		}

		if (relationType.equals(PlayerRelationType.WAR.name())) {
			return List.of(OFFER_PEACE);
		}

		if (relationType.equals(PlayerRelationType.ALLIANCE.name())) {
			return List.of(CANCEL_ALLIANCE);
		}

		if (relationType.equals(PlayerRelationType.NON_AGGRESSION_TREATY.name())) {
			return List.of(OFFER_ALLIANCE, CANCEL_NON_AGGRESSION_TREATY);
		}

		return List.of(OFFER_NON_AGGRESSION_TREATY, OFFER_ALLIANCE, CANCEL_TRADE_AGREEMENT);
	}

	public void offerMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String retrieveDescription() {
		Locale locale = LocaleContextHolder.getLocale();

		if (requestId != null) {
			if (requestedRelationType == null) {
				return messageSource.getMessage("peace_requested", null, locale);
			}

			String description = messageSource.getMessage("player_relation_type_" + requestedRelationType, null, locale);
			description += " " + messageSource.getMessage("requested", null, locale);
			return description;

		}

		if (relationType != null) {
			String relationTypeString = messageSource.getMessage("player_relation_type_" + relationType, null, locale);

			if (roundsLeft == 1) {
				relationTypeString += " " + messageSource.getMessage("player_relation_ends_in_one_month", null, locale);
			} else if (roundsLeft > 0) {
				Object[] args = new Object[] { roundsLeft };
				relationTypeString += " " + messageSource.getMessage("player_relation_ends", args, null, locale);
			}

			return relationTypeString;
		}

		return "-";
	}

	public String retrieveTypeIcon() {
		if (relationType != null) {
			PlayerRelationType type = PlayerRelationType.valueOf(relationType);
			return type.getIcon();
		}

		return null;
	}

	public String retrievePlayerName() {
		return messageSource.getMessage(playerName, null, playerName, LocaleContextHolder.getLocale());
	}

	@Override
	public String toString() {
		return "PlayerRelationContainer [requestId=" + requestId + ", playerId=" + playerId + ", playerName=" + playerName + ", relationType=" + relationType +
				", toBeAccepted=" + toBeAccepted + ", requestedRelationType=" + requestedRelationType + "]";
	}
}