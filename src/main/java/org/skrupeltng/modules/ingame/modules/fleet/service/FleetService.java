package org.skrupeltng.modules.ingame.modules.fleet.service;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.HelperUtils;
import org.skrupeltng.modules.ingame.controller.FleetListResultDTO;
import org.skrupeltng.modules.ingame.controller.FleetOverviewRequest;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.controlgroup.database.ControlGroupRepository;
import org.skrupeltng.modules.ingame.modules.controlgroup.database.ControlGroupType;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTaskString;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class FleetService {

	private final FleetRepository fleetRepository;
	private final PlayerRepository playerRepository;
	private final ShipRepository shipRepository;
	private final ControlGroupRepository controlGroupRepository;
	private final FleetLeader fleetLeader;
	private final VisibleObjects visibleObjects;
	private final ShipTaskString shipTaskString;

	public FleetService(FleetRepository fleetRepository, PlayerRepository playerRepository, ShipRepository shipRepository, ControlGroupRepository controlGroupRepository, FleetLeader fleetLeader, VisibleObjects visibleObjects, ShipTaskString shipTaskString) {
		this.fleetRepository = fleetRepository;
		this.playerRepository = playerRepository;
		this.shipRepository = shipRepository;
		this.controlGroupRepository = controlGroupRepository;
		this.fleetLeader = fleetLeader;
		this.visibleObjects = visibleObjects;
		this.shipTaskString = shipTaskString;
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public Page<FleetListResultDTO> getFleetsOfLogin(long gameId, FleetOverviewRequest request, long loginId) {
		Page<FleetListResultDTO> fleets = fleetRepository.searchFleets(request, loginId);

		for (FleetListResultDTO dto : fleets) {
			String[] tasks = dto.getTaskData().split(";");
			Set<String> set = new HashSet<>(List.of(tasks));

			if (set.size() == 1) {
				String[] parts = set.iterator().next().split(",");

				if (parts.length > 0) {
					dto.setTaskType(parts[0]);

					if (parts.length == 2) {
						dto.setActiveAbilityType(parts[1]);
					}
				}
			} else {
				dto.setTaskData(null);
			}
		}

		return fleets;
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet')")
	public Fleet getById(long fleetId) {
		return fleetRepository.getReferenceById(fleetId);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(propagation = Propagation.REQUIRED)
	public long createFleet(long gameId, String name, long loginId) {
		Optional<Player> playerOpt = playerRepository.findByGameIdAndLoginId(gameId, loginId);

		if (playerOpt.isEmpty()) {
			throw new IllegalArgumentException("Login " + loginId + " is not part of game " + gameId + "!");
		}

		Fleet fleet = new Fleet();
		fleet.setName(name);
		fleet.setPlayer(playerOpt.get());

		fleet = fleetRepository.save(fleet);

		return fleet.getId();
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteFleet(long fleetId) {
		controlGroupRepository.deleteByTypeAndEntityId(ControlGroupType.FLEET, fleetId);
		fleetRepository.clearFleetsFromShipsByFleetId(fleetId);
		fleetRepository.deleteById(fleetId);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void addShipToFleet(long shipId, long fleetId) {
		Ship ship = shipRepository.getReferenceById(shipId);
		Fleet fleet = fleetRepository.getReferenceById(fleetId);
		addShipToFleetWithoutPermissionChecks(ship, fleet);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void addShipToFleetWithoutPermissionChecks(Ship ship, Fleet fleet) {
		Fleet oldFleet = ship.getFleet();

		if (oldFleet != null) {
			if (oldFleet.getShips().size() == 1) {
				oldFleet.setLeader(null);
				fleetRepository.save(oldFleet);
			} else if (oldFleet.getLeader().getId() == ship.getId()) {
				long shipId = ship.getId();

				oldFleet.getShips().stream()
					.filter(s -> s.getId() != shipId)
					.max(Comparator.comparing(s -> s.getShipTemplate().getTechLevel()))
					.ifPresent(newLeader -> fleetLeader.changeLeader(oldFleet, newLeader));
			}
		}

		ship.setFleet(fleet);

		ship = shipRepository.save(ship);

		if (fleet.getShips() == null) {
			fleet.setShips(new ArrayList<>());
		}

		fleet.getShips().add(ship);

		if (fleet.getLeader() == null) {
			fleetLeader.changeLeader(fleet, ship);
		} else {
			fleetLeader.updateFleetDestinations(fleet, fleet.getLeader());
		}
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void clearFleetOfShip(long shipId) {
		clearFleetOfShipWithoutPermissionCheck(shipId);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void clearFleetOfShipWithoutPermissionCheck(long shipId) {
		Ship ship = shipRepository.getReferenceById(shipId);
		Fleet fleet = ship.getFleet();

		if (fleet != null && fleet.getLeader() == ship) {
			if (fleet.getShips().size() > 1) {
				Optional<Ship> result = fleet.getShips().stream().filter(s -> s != ship).findFirst();

				if (result.isPresent()) {
					fleetLeader.changeLeader(fleet, result.get());
					ship.resetTravel();
				} else {
					fleetLeader.changeLeader(fleet, null);
				}
			} else {
				fleetLeader.changeLeader(fleet, null);
			}
		} else {
			ship.resetTravel();
		}

		ship.setFleet(null);

		shipRepository.save(ship);

		visibleObjects.clearCaches();
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public List<Fleet> getPossibleFleetsForShip(long shipId) {
		return fleetRepository.getPossibleFleetsForShip(shipId);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public List<Fleet> getAllFleetsForPlayer(long gameId, long loginId) {
		return fleetRepository.findByGameIdAndLoginId(gameId, loginId);
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void changeTactics(long fleetId, int aggressiveness) {
		if (aggressiveness < 0 || aggressiveness > 100) {
			throw new IllegalArgumentException("Invalid aggressiveness value!");
		}

		List<Ship> ships = fleetRepository.getReferenceById(fleetId).getShips();

		for (Ship ship : ships) {
			ship.setAggressiveness(aggressiveness);
		}

		shipRepository.saveAll(ships);
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void changeName(long fleetId, String name) {
		if (StringUtils.isBlank(name)) {
			throw new IllegalArgumentException("Fleet name cannot be blank!");
		}

		Fleet fleet = fleetRepository.getReferenceById(fleetId);
		fleet.setName(name);
		fleetRepository.save(fleet);
	}

	public List<Fleet> getFleets(long fleetId) {
		return fleetRepository.findOtherFleets(fleetId);
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet') and hasPermission(#otherFleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void mergeFleets(long fleetId, long otherFleetId) {
		Fleet otherFleet = fleetRepository.getReferenceById(otherFleetId);

		List<Ship> ships = new ArrayList<>(otherFleet.getShips());

		for (Ship ship : ships) {
			clearFleetOfShip(ship.getId());
			addShipToFleet(ship.getId(), fleetId);
		}

		deleteFleet(otherFleetId);
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void setRouteTravelSpeed(long fleetId, int travelSpeed) {
		if (travelSpeed < 0 || travelSpeed > 9) {
			throw new IllegalArgumentException("Invalid travel speed value: " + travelSpeed);
		}

		final Fleet fleet = fleetRepository.getReferenceById(fleetId);
		for (final Ship ship : fleet.getShips()) {
			ship.setTravelSpeed(travelSpeed);
			ship.setRouteTravelSpeed(travelSpeed);
			shipRepository.save(ship);
		}
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void toggleForceTravelSpeed(long fleetId, boolean force) {
		final Fleet fleet = fleetRepository.getReferenceById(fleetId);
		Ship leader = fleet.getLeader();

		if (leader == null) {
			return;
		}

		List<Ship> ships = fleet.getShips();

		if (HelperUtils.isEmpty(ships) || ships.size() == 1) {
			return;
		}

		for (Ship ship : ships) {
			if (ship.getId() != leader.getId()) {
				if (force) {
					ship.setTravelSpeed(leader.getTravelSpeed());
				} else {
					PropulsionSystemTemplate propulsionSystemTemplate = ship.getPropulsionSystemTemplate();
					ship.setTravelSpeed(propulsionSystemTemplate.getWarpSpeed());
				}

				shipRepository.save(ship);
			}
		}
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	@Transactional(propagation = Propagation.REQUIRED)
	public String followLeader(long shipId) {
		Ship ship = shipRepository.getReferenceById(shipId);
		Fleet fleet = ship.getFleet();

		if (fleet == null || fleet.getLeader() == ship) {
			return "-";
		}

		Ship leader = fleet.getLeader();
		ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
		ship.setDestinationShip(leader);
		ship.setDestinationX(leader.getX());
		ship.setDestinationY(leader.getY());

		if (ship.getRoute().size() >= 2) {
			ship.setRouteDisabled(true);
		}

		ship = shipRepository.save(ship);

		return shipTaskString.retrieveTaskString(ship);
	}
}
