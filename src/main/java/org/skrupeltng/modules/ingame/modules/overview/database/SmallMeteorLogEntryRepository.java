package org.skrupeltng.modules.ingame.modules.overview.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface SmallMeteorLogEntryRepository extends JpaRepository<SmallMeteorLogEntry, Long> {

	@Modifying
	@Query("DELETE FROM SmallMeteorLogEntry i WHERE i.newsEntry.id IN (SELECT n.id FROM NewsEntry n WHERE n.player.id = ?1)")
	void deleteByPlayerId(long playerId);
}
