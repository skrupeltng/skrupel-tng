package org.skrupeltng.modules.ingame.modules.orbitalsystem.controller;

import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.orbitalsystem.service.OrbitalSystemService;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/ingame/orbital-system")
public class OrbitalSystemController extends AbstractController {

	private final OrbitalSystemService orbitalSystemService;

	public OrbitalSystemController(OrbitalSystemService orbitalSystemService) {
		this.orbitalSystemService = orbitalSystemService;
	}

	@GetMapping("")
	public String getOrbitalSystem(@RequestParam long orbitalSystemId, Model model) {
		OrbitalSystem orbitalSystem = orbitalSystemService.get(orbitalSystemId);
		model.addAttribute("orbitalSystem", orbitalSystem);
		Planet planet = orbitalSystem.getPlanet();

		OrbitalSystemType type = orbitalSystem.getType();

		if (type == null) {
			Player player = planet.getPlayer();
			Faction faction = player.getFaction();
			boolean enableEspionage = player.getGame().isEnableEspionage();
			OrbitalSystemType[] types = OrbitalSystemType.values();

			List<OrbitalSystemType> typeList = new ArrayList<>(types.length);
			Set<OrbitalSystemType> existingTypes = planet.getOrbitalSystems().stream().map(OrbitalSystem::getType).collect(Collectors.toSet());

			for (OrbitalSystemType t : types) {
				if (canBuildOrbitalSystem(planet, faction, enableEspionage, existingTypes, t)) {
					typeList.add(t);
				}
			}

			model.addAttribute("types", typeList);

			return "ingame/orbitalsystem/orbitalsystem-construction::content";
		}

		if (type == OrbitalSystemType.ECHO_CENTER) {
			List<Ship> ships = orbitalSystemService.getScannedShips(orbitalSystemId);
			model.addAttribute("ships", ships);
			model.addAttribute("orbitalSystemId", orbitalSystemId);
			return "ingame/ship/scanner::content";
		}

		if (type == OrbitalSystemType.MILITARY_BASE || type == OrbitalSystemType.ARMS_COMPLEX) {
			boolean light = type == OrbitalSystemType.MILITARY_BASE;
			model.addAttribute("light", light);

			GroundUnitProductionData data = new GroundUnitProductionData(type, planet);
			model.addAttribute("data", data);

			return "ingame/orbitalsystem/orbitalsystem::groundunits";
		}

		if (type == OrbitalSystemType.MINERAL_TRANSFORMER) {
			model.addAttribute("planet", planet);
			model.addAttribute("moneyRate", configProperties.getMineralTransformerMoneyRate());
			model.addAttribute("sourceRate", configProperties.getMineralTransformerSourceRate());

			Map<String, MineralTransformationData> data = new HashMap<>(6);

			int money = planet.getMoney();
			int mineral1 = planet.getMineral1();
			int mineral2 = planet.getMineral2();
			int mineral3 = planet.getMineral3();

			String mineral1Name = "mineral1";
			String mineral2Name = "mineral2";
			String mineral3Name = "mineral3";

			addMineralTransformationData(data, money, mineral1, mineral1Name, mineral2Name);
			addMineralTransformationData(data, money, mineral1, mineral1Name, mineral3Name);
			addMineralTransformationData(data, money, mineral2, mineral2Name, mineral1Name);
			addMineralTransformationData(data, money, mineral2, mineral2Name, mineral3Name);
			addMineralTransformationData(data, money, mineral3, mineral3Name, mineral1Name);
			addMineralTransformationData(data, money, mineral3, mineral3Name, mineral2Name);

			model.addAttribute("data", data);

			model.addAttribute("noTransformationPossible", data.values().stream().noneMatch(m -> m.getMaxTarget() > 0));

			return "ingame/orbitalsystem/orbitalsystem::mineral-transformer";
		}

		return "ingame/orbitalsystem/orbitalsystem::simple";
	}

	private boolean canBuildOrbitalSystem(Planet planet, Faction faction, boolean enableEspionage, Set<OrbitalSystemType> existingTypes, OrbitalSystemType t) {
		if (t == OrbitalSystemType.ORBITAL_EXTENSION && planet.getOrbitalSystemCount() >= 5) {
			return false;
		}

		return !existingTypes.contains(t) && faction.orbitalSystemAllowed(t) && (enableEspionage || t != OrbitalSystemType.SPY_CENTER);
	}

	private void addMineralTransformationData(Map<String, MineralTransformationData> data, int money, int source, String sourceName, String targetName) {
		int moneyRate = configProperties.getMineralTransformerMoneyRate();
		int sourceRate = configProperties.getMineralTransformerSourceRate();

		MineralTransformationData entry = new MineralTransformationData(money, moneyRate, source, sourceRate);
		data.put(sourceName + "_" + targetName, entry);
	}

	@PostMapping("")
	@ResponseBody
	public void construct(@RequestParam long orbitalSystemId, @RequestParam String type) {
		orbitalSystemService.construct(orbitalSystemId, OrbitalSystemType.valueOf(type));
	}

	@DeleteMapping("")
	@ResponseBody
	public void tearDown(@RequestParam long orbitalSystemId) {
		orbitalSystemService.tearDownOrbitalSystem(orbitalSystemId);
	}

	@GetMapping("/scanner/ship")
	public String getScannedShipDetails(@RequestParam long orbitalSystemId, @RequestParam long scannedShipId, Model model) {
		Ship ship = orbitalSystemService.getScannedShip(orbitalSystemId, scannedShipId);
		model.addAttribute("ship", ship);
		return "ingame/ship/scanner::ship";
	}

	@PostMapping("/ground-units")
	@ResponseBody
	public void produceGroundUnits(@RequestBody GroundUnitProductionRequest request) {
		orbitalSystemService.produceGroundUnits(request.getOrbitalSystemId(), request);
	}

	@PostMapping("/transform-minerals")
	@ResponseBody
	public void transformMinerals(@RequestBody TransformMineralsRequest request) {
		orbitalSystemService.transformMinerals(request.getOrbitalSystemId(), request);
	}

	@GetMapping("/{orbitalSystemId}/planetId")
	@ResponseBody
	public long getOrbitalSystemPlanetId(@PathVariable("orbitalSystemId") long orbitalSystemId) {
		return orbitalSystemService.getPlanetId(orbitalSystemId);
	}
}
