package org.skrupeltng.modules.ingame.modules.fleet.controller;

import java.io.Serializable;

public class FleetPercentageData implements Serializable {

	private static final long serialVersionUID = 1820930935306039519L;

	private float currentPercentage;
	private float possiblePercentage;

	public float getCurrentPercentage() {
		return currentPercentage;
	}

	public void setCurrentPercentage(float currentPercentage) {
		this.currentPercentage = currentPercentage;
	}

	public float getPossiblePercentage() {
		return possiblePercentage;
	}

	public void setPossiblePercentage(float possiblePercentage) {
		this.possiblePercentage = possiblePercentage;
	}
}