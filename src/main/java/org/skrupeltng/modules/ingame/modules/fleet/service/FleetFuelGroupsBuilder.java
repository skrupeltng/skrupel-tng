package org.skrupeltng.modules.ingame.modules.fleet.service;

import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.modules.fleet.database.SortFleetShipsComparator;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

public class FleetFuelGroupsBuilder {

	private final List<Ship> ships;

	public FleetFuelGroupsBuilder(List<Ship> ships) {
		this.ships = ships;
	}

	public List<FleetFuelGroup> createGroups() {
		Map<CoordinateImpl, FleetFuelGroup> groupsMap = new HashMap<>(ships.size());

		for (Ship ship : ships) {
			CoordinateImpl coordinate = new CoordinateImpl(ship);
			FleetFuelGroup group = groupsMap.computeIfAbsent(coordinate, FleetFuelGroup::new);
			group.addShip(ship);
		}

		groupsMap.values().forEach(this::computeValues);

		return groupsMap.values().stream().sorted(FleetFuelGroupComparator.INSTANCE).toList();
	}

	private void computeValues(FleetFuelGroup fleetFuelGroup) {
		int currentFuelTotal = 0;
		int fuelCapacityTotal = 0;
		int availableFuelTotal = 0;

		boolean planetAdded = false;

		TreeSet<Integer> shipFuelPercentages = new TreeSet<>();

		for (Ship ship : fleetFuelGroup.getShips()) {
			shipFuelPercentages.add(FleetResources.roundPercentage(ship.getFuel() / (float) ship.getShipTemplate().getFuelCapacity()));

			currentFuelTotal += ship.getFuel();
			availableFuelTotal += ship.getFuel();
			fuelCapacityTotal += ship.getShipTemplate().getFuelCapacity();

			if (!planetAdded) {
				planetAdded = true;
				Planet planet = ship.getPlanet();

				if (planet != null) {
					availableFuelTotal += planet.getFuel();
				}
			}
		}

		fleetFuelGroup.getShips().sort(SortFleetShipsComparator.INSTANCE);

		if (fuelCapacityTotal > 0) {
			fleetFuelGroup.setAllShipsHaveEqualFuel(Math.abs(shipFuelPercentages.first() - shipFuelPercentages.last()) < 2);
			fleetFuelGroup.setCurrentFuelQuantity(currentFuelTotal);
			fleetFuelGroup.setCurrentFuelPercentage(FleetResources.roundPercentage(currentFuelTotal / (float) fuelCapacityTotal));
			fleetFuelGroup.setMaxFuelPercentage(FleetResources.roundPercentage(availableFuelTotal / (float) fuelCapacityTotal));

			if (fleetFuelGroup.getMaxFuelPercentage() > 100) {
				fleetFuelGroup.setMaxFuelPercentage(100);
			}
		}
	}
}
