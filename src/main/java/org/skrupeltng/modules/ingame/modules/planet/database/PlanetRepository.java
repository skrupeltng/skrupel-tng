package org.skrupeltng.modules.ingame.modules.planet.database;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface PlanetRepository extends JpaRepository<Planet, Long>, PlanetRepositoryCustom {

	@Query("SELECT p FROM Planet p WHERE p.game.id = ?1 AND p.player IS NULL AND p.newColonists > 0 AND p.newPlayer IS NOT NULL")
	List<Planet> findNewColonies(long gameId);

	@Query("SELECT p FROM Planet p WHERE p.game.id = ?1 AND p.player IS NOT NULL")
	List<Planet> findOwnedPlanetsByGameId(long gameId);

	Optional<Planet> findByGameIdAndXAndY(long gameId, int x, int y);

	List<Planet> findByGameId(long gameId);

	List<Planet> findByPlayerId(long playerId);

	@Query("SELECT COUNT(pl) FROM Planet pl WHERE pl.player.id = ?1")
	long getPlanetCountByPlayerId(long playerId);

	@Query("SELECT " +
			"	new org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetCountResult(p.id, COUNT(pl)) " +
			"FROM " +
			"	Planet pl " +
			"	INNER JOIN pl.player p " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"GROUP " +
			"	BY p.id")
	List<PlayerPlanetCountResult> getPlanetCountsByGameId(long gameId);

	@Query("SELECT pl FROM Planet pl WHERE pl.id IN (?1)")
	List<Planet> findByIds(List<Long> planetIds);

	@Modifying
	@Query("UPDATE Planet p SET p.player = NULL, p.colonists = 0, p.lightGroundUnits = 0, p.heavyGroundUnits = 0, log = null WHERE p.player.id = ?1")
	void clearPlayer(long playerId);

	@Modifying
	@Query("UPDATE Planet p SET p.newPlayer = NULL, p.newColonists = 0, p.newLightGroundUnits = 0, p.newHeavyGroundUnits = 0 WHERE p.newPlayer.id = ?1")
	void clearNewPlayer(long playerId);

	@Query("SELECT " +
			"	pl " +
			"FROM " +
			"	Planet pl " +
			"	INNER JOIN pl.player p " +
			"WHERE " +
			"	pl.game.id = ?1 " +
			"	AND (pl.colonists > 0 OR pl.lightGroundUnits > 0 OR pl.heavyGroundUnits > 0) " +
			"	AND (pl.newColonists > 0 OR pl.newLightGroundUnits > 0 OR pl.newHeavyGroundUnits > 0) " +
			"ORDER BY " +
			"	pl.id ASC")
	List<Planet> findPlanetsForGroundCombat(long gameId);

	@Query("SELECT p.name FROM Planet p WHERE p.id = ?1")
	String getName(long planetId);
}
