package org.skrupeltng.modules.ingame.modules.planet.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PlanetTypeRepository extends JpaRepository<PlanetType, String> {

	@Query("SELECT p.id FROM PlanetType p WHERE p.imagePrefix = ?1")
	String getIdByImagePrefix(String imagePrefix);
}