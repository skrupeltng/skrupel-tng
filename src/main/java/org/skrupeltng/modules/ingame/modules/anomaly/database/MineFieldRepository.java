package org.skrupeltng.modules.ingame.modules.anomaly.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface MineFieldRepository extends JpaRepository<MineField, Long>, MineFieldRepositoryCustom {

	List<MineField> findByGameId(long gameId);

	@Modifying
	@Query("DELETE FROM MineField m WHERE m.player.id = ?1")
	void deleteByPlayerId(long playerId);
}