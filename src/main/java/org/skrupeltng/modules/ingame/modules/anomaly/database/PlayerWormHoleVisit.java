package org.skrupeltng.modules.ingame.modules.anomaly.database;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import org.skrupeltng.modules.ingame.database.player.Player;

@Entity
@Table(name = "player_worm_hole_visit")
public class PlayerWormHoleVisit implements Serializable {

	private static final long serialVersionUID = 472041909944232519L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Player player;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "worm_hole_id")
	private WormHole wormHole;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public WormHole getWormHole() {
		return wormHole;
	}

	public void setWormHole(WormHole wormHole) {
		this.wormHole = wormHole;
	}
}
