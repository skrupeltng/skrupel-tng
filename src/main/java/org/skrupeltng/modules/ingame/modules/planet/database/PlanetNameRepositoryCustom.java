package org.skrupeltng.modules.ingame.modules.planet.database;

import java.util.List;

public interface PlanetNameRepositoryCustom {

	List<String> getAllPlanetNames();
}