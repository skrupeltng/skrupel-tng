package org.skrupeltng.modules.ingame.modules.planet.database;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.skrupeltng.modules.RepositoryCustomBase;
import org.springframework.cache.annotation.Cacheable;

public class PlanetNameRepositoryImpl extends RepositoryCustomBase implements PlanetNameRepositoryCustom {

	@Override
	@Cacheable("planetNames")
	public List<String> getAllPlanetNames() {
		String sql = "SELECT name FROM planet_name";

		Map<String, Object> params = new HashMap<>(0);
		List<String> planetNames = jdbcTemplate.queryForList(sql, params, String.class);
		return planetNames;
	}
}