package org.skrupeltng.modules.ingame.modules.overview.database.spacecombat;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface SpaceCombatLogEntryRepository extends JpaRepository<SpaceCombatLogEntry, Long> {

	@Modifying
	@Query("DELETE FROM SpaceCombatLogEntry i WHERE i.newsEntry.id IN (SELECT n.id FROM NewsEntry n WHERE n.player.id = ?1)")
	void deleteByPlayerId(long playerId);
}
