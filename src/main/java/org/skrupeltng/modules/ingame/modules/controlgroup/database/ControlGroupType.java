package org.skrupeltng.modules.ingame.modules.controlgroup.database;

public enum ControlGroupType {

	PLANET, STARBASE, SHIP, FLEET, ORBITALSYSTEM
}