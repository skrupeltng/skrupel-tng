package org.skrupeltng.modules.ingame.modules.ship.database;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ShipRepository extends JpaRepository<Ship, Long>, ShipRepositoryCustom {

	List<Ship> findByPlayerId(long playerId);

	@Modifying
	@Query("UPDATE Ship s SET s.lastX = 0, s.lastY = 0 WHERE s.travelSpeed = 0 AND s.player.id IN (SELECT p.id FROM Player p WHERE p.game.id = ?1)")
	void clearLastCoordinates(long gameId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p " +
			"	INNER JOIN s.planet pl " +
			"WHERE" +
			"	p.game.id = ?1 " +
			"	AND pl.id = ?2")
	List<Ship> findByGameIdAndPlanetId(long gameId, long planetId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN FETCH s.shipTemplate st " +
			"	INNER JOIN s.player p " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND p.login.id = ?2 " +
			"ORDER BY " +
			"	st.techLevel ASC, " +
			"	s.name ASC")
	List<Ship> findShipsByGameIdAndLoginId(long gameId, long loginId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN FETCH s.shipTemplate st " +
			"	INNER JOIN s.player p " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND p.login.id = ?2 " +
			"	AND s.x = ?3 " +
			"	AND s.y = ?4 " +
			"ORDER BY " +
			"	st.techLevel ASC, " +
			"	s.name ASC")
	List<Ship> findShipsByGameIdAndLoginIdAndCoordinates(long gameId, long loginId, int x, int y);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN FETCH s.shipTemplate st " +
			"	INNER JOIN s.player p " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND s.x = ?2 " +
			"	AND s.y = ?3 " +
			"ORDER BY " +
			"	st.techLevel ASC, " +
			"	s.name ASC")
	List<Ship> findShipsByGameIdAndCoordinates(long gameId, int x, int y);

	@Query("SELECT " +
			"	DISTINCT(s) " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN FETCH s.player p " +
			"	INNER JOIN FETCH p.login l " +
			"	INNER JOIN FETCH s.shipTemplate st " +
			"	INNER JOIN FETCH s.propulsionSystemTemplate pst " +
			"	LEFT OUTER JOIN FETCH s.route r " +
			"	LEFT OUTER JOIN FETCH r.planet rp " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"ORDER BY " +
			"	st.techLevel ASC, " +
			"	s.name ASC")
	List<Ship> findForIngameView(long gameId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN FETCH s.player p " +
			"	INNER JOIN FETCH p.login l " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"ORDER BY " +
			"	s.id DESC")
	List<Ship> findByGameId(long gameId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN FETCH s.player p " +
			"	INNER JOIN FETCH p.login l " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"ORDER BY " +
			"	s.aggressiveness DESC," +
			"	s.id ASC")
	List<Ship> findForSpaceCombat(long gameId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN FETCH s.player p " +
			"	INNER JOIN FETCH s.shipTemplate st " +
			"	LEFT OUTER JOIN FETCH s.destinationShip ds " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND s.travelSpeed > 0")
	List<Ship> getMovingShips(long gameId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"WHERE " +
			"	s.player.id = ?1 " +
			"	AND s.travelSpeed > 0")
	List<Ship> getMovingShipsByPlayerId(long playerId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN FETCH s.player sp " +
			"	INNER JOIN FETCH s.planet pl " +
			"	INNER JOIN FETCH pl.player p " +
			"	INNER JOIN FETCH s.shipTemplate st " +
			"	LEFT OUTER JOIN FETCH s.energyWeaponTemplate ewt " +
			"	LEFT OUTER JOIN FETCH s.projectileWeaponTemplate pwt " +
			"WHERE " +
			"	p.game.id = ?1" +
			"	AND p.id != sp.id " +
			"ORDER BY" +
			"	s.aggressiveness DESC")
	List<Ship> getShipsOnEnemyPlanet(long gameId);

	@Modifying
	@Query("UPDATE Ship s SET s.destinationShip = null WHERE s.destinationShip.id IN (?1)")
	void clearDestinationShip(Collection<Long> shipIds);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN FETCH s.player p " +
			"	INNER JOIN FETCH s.shipTemplate st " +
			"	LEFT OUTER JOIN FETCH s.energyWeaponTemplate ewt " +
			"	LEFT OUTER JOIN FETCH s.projectileWeaponTemplate pwt " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND p.id != ?2 " +
			"	AND s.x = ?3 " +
			"	AND s.y = ?4 " +
			"ORDER BY" +
			"	s.aggressiveness DESC," +
			"	s.id ASC")
	List<Ship> findEnemyShipsOnSamePosition(long gameId, long playerId, int x, int y);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player pl " +
			"WHERE " +
			"	pl.game.id = ?1 " +
			"	AND s.x = ?2 " +
			"	AND s.y = ?3 " +
			"	AND s.name = ?4")
	List<Ship> findByGameIdAndXAndYAndName(long id, int x, int y, String name);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND s.taskType = ?2")
	List<Ship> getShipsWithTask(long gameId, ShipTaskType type);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p" +
			"	INNER JOIN s.activeAbility a " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND a.type = ?2")
	List<Ship> getShipsWithActiveAbility(long gameId, ShipAbilityType type);

	@Query("SELECT " +
			"	DISTINCT(pl) " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p" +
			"	INNER JOIN s.activeAbility a " +
			"	INNER JOIN s.planet pl " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND a.type = ?2")
	List<Planet> getPlanetsWithActiveShipAbility(long gameId, ShipAbilityType type);

	@Query("SELECT s FROM Ship s WHERE s.id IN (?1) ORDER BY s.id ASC")
	List<Ship> findByIds(List<Long> ids);

	@Query("SELECT st.mass FROM Ship s INNER JOIN s.shipTemplate st WHERE s.id = ?1")
	int getMass(long shipId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p " +
			"		WITH p.game.id = ?1 " +
			"WHERE " +
			"	s.propulsionSystemTemplate.name = ?2")
	List<Ship> findByPropulsionAndGameId(long gameId, String propulsionSystemTemplateName);

	@Modifying
	@Query("UPDATE Ship s SET s.planet = null WHERE s.planet.id = ?1")
	void clearPlanet(long planetId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p " +
			"		WITH p.game.id = ?1 " +
			"WHERE " +
			"	s.destinationX = ?2 " +
			"	AND s.destinationY = ?3")
	List<Ship> findByDestination(long gameId, int x, int y);

	@Modifying
	@Query("UPDATE Ship s SET s.shield = 100 WHERE s.player.id IN (SELECT p.id FROM Player p WHERE p.game.id = ?1)")
	void restoreShields(long gameId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p " +
			"		WITH p.game.id = ?1 " +
			"WHERE " +
			"	s.autoBuildProjectiles = true")
	List<Ship> getShipWithAutomaticProjectileConstruction(long gameId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player pl " +
			"	INNER JOIN FETCH s.currentRouteEntry c " +
			"	LEFT OUTER JOIN FETCH s.planet p " +
			"WHERE " +
			"	pl.game.id = ?1 " +
			"	AND s.routeDisabled = false")
	List<Ship> getShipsWithRoutes(long gameId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player pl " +
			"	INNER JOIN FETCH s.currentRouteEntry c " +
			"	INNER JOIN FETCH s.planet p " +
			"WHERE " +
			"	pl.game.id = ?1 " +
			"	AND s.routeDisabled = false")
	List<Ship> getShipsWithPlanetOnRoute(long gameId);

	@Modifying
	@Query("UPDATE Ship s SET s.currentRouteEntry = null WHERE s.id = ?1")
	void clearCurrentRouteEntry(long shipId);

	@Modifying
	@Query("UPDATE Ship s SET s.currentRouteEntry = null WHERE s.id IN (?1)")
	void clearCurrentRouteEntry(Collection<Long> shipIds);

	@Modifying
	@Query("UPDATE " +
			"	Ship s " +
			"SET " +
			"	s.currentRouteEntry = null " +
			"WHERE " +
			"	s.currentRouteEntry.id IN (" +
			"		SELECT " +
			"			r.id " +
			"		FROM " +
			"			ShipRouteEntry r " +
			"		WHERE " +
			"			r.ship.id = s.id " +
			"			AND r.planet.id = ?1 " +
			"	)")
	void clearCurrentRouteEntryByRoutePlanetId(long planetId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p " +
			"	INNER JOIN s.shipTemplate st " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND s.planet IS NULL " +
			"	AND st.techLevel > 3 " +
			"	AND st.energyWeaponsCount = 0 " +
			"	AND st.projectileWeaponsCount = 0 " +
			"	AND st.hangarCapacity = 0 " +
			"	AND (" +
			"		s.money > 0 " +
			"		OR s.supplies > 0 " +
			"		OR s.mineral1 > 0 " +
			"		OR s.mineral2 > 0 " +
			"		OR s.mineral3 > 0 " +
			"	)" +
			"ORDER BY " +
			"	s.id ASC")
	List<Ship> getShipsVulnerableToPirates(long gameId);

	@Query("SELECT " +
			"	s.id " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND s.planet IS NULL " +
			"GROUP BY " +
			"	s.id " +
			"ORDER BY " +
			"	s.id ASC")
	List<Long> findShipsInFreeSpace(long gameId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.shipTemplate st " +
			"WHERE " +
			"	s.planet.id = ?1 " +
			"ORDER BY " +
			"	st.techLevel ASC, " +
			"	s.name ASC")
	List<Ship> findByPlanetId(long planetId);

	@Query("SELECT " +
			"	st " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p " +
			"		WITH p.game.id = ?1 AND p.login.id = ?2 " +
			"	INNER JOIN s.shipTemplate st " +
			"ORDER BY " +
			"	st.techLevel ASC, " +
			"	s.name ASC")
	Set<ShipTemplate> getBuiltShipTemplates(long gameId, long loginId);

	@Query("SELECT " +
			"	s1 " +
			"FROM " +
			"	Ship s1 " +
			"	INNER JOIN s1.player p1, " +
			"	Ship s2 " +
			"	INNER JOIN s2.player p2 " +
			"WHERE " +
			"	p1.game.id = ?1 " +
			"	AND p2.game.id = ?1 " +
			"	AND p1.id != p2.id " +
			"	AND s1.planet IS NULL " +
			"	AND s1.x = s2.x " +
			"	AND s1.y = s2.y")
	Set<Ship> getShipsWithOtherForeignShips(long gameId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN FETCH s.player p " +
			"	INNER JOIN FETCH p.login l " +
			"	INNER JOIN FETCH s.shipTemplate st " +
			"	INNER JOIN FETCH s.propulsionSystemTemplate pst " +
			"WHERE " +
			"	s.planet.id IN (?1) " +
			"ORDER BY " +
			"	st.techLevel ASC, " +
			"	s.name ASC")
	List<Ship> findByPlanetIds(Set<Long> planetIds);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND p.backgroundAIAgent = ?2")
	List<Ship> getShipsOfBackgroundAIAgents(long gameId, boolean isBackgroundAIAgent);

	@Query("SELECT s.name FROM Ship s WHERE s.id = ?1")
	String getName(long shipId);

	@Modifying
	@Query("DELETE FROM Ship s WHERE s.id IN (?1)")
	void deleteByIds(Collection<Long> shipIds);

	@Modifying
	@Query("UPDATE Ship s SET s.cloaked = false WHERE s.player.id IN (SELECT p.id FROM Player p WHERE p.game.id = ?1)")
	void resetCloacking(long gameId);

	@Query("SELECT s FROM Ship s WHERE s.player.id = ?1 AND s.x = ?2 AND s.y = ?3 ORDER BY s.id ASC")
	List<Ship> findByPlayerIdAndCoordinates(long playerId, int x, int y);
}
