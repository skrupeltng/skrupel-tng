package org.skrupeltng.modules.ingame.modules.planet.controller;

import java.util.Locale;

import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpecies;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpeciesEffect;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public class NativeSpeciesDescription {

	private final MessageSource messageSource;

	public NativeSpeciesDescription(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String createNativeSpeciesDescription(Planet planet) {
		NativeSpecies nativeSpecies = planet.getNativeSpecies();
		return createNativeSpeciesDescriptionByNativeSpecies(nativeSpecies);
	}

	public String createNativeSpeciesDescriptionByNativeSpecies(NativeSpecies nativeSpecies) {
		NativeSpeciesEffect effect = nativeSpecies.getEffect();
		float effectValue = nativeSpecies.getEffectValue();

		return createNativeSpeciesDescriptionByValues(effect, effectValue);
	}

	public String createNativeSpeciesDescriptionByValues(NativeSpeciesEffect effect, float effectValue) {
		String key = "native_species_effect_" + effect.name();

		if (effectValue < 0f) {
			effectValue *= -1;
			key += "_negative";
		}

		Object[] args = new Object[] { effectValue };
		Locale locale = LocaleContextHolder.getLocale();

		String effectDescription = messageSource.getMessage(key, args, locale);
		return effectDescription;
	}
}