package org.skrupeltng.modules.ingame.modules.ship.service;

import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTaskString;
import org.skrupeltng.modules.ingame.modules.ship.database.*;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.masterdata.database.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ShipRouteService {

	private final ShipRepository shipRepository;
	private final PlanetRepository planetRepository;
	private final ShipRouteEntryRepository shipRouteEntryRepository;
	private final ShipTaskString shipTaskString;
	private final VisibleObjects visibleObjects;

	public ShipRouteService(ShipRepository shipRepository, PlanetRepository planetRepository, ShipRouteEntryRepository shipRouteEntryRepository, ShipTaskString shipTaskString, VisibleObjects visibleObjects) {
		this.shipRepository = shipRepository;
		this.planetRepository = planetRepository;
		this.shipRouteEntryRepository = shipRouteEntryRepository;
		this.shipTaskString = shipTaskString;
		this.visibleObjects = visibleObjects;
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	public List<ShipRouteEntry> getRoute(long shipId) {
		return shipRouteEntryRepository.findByShipId(shipId);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void addRouteEntry(long shipId, long planetId, ShipRouteResourceAction action) {
		Ship ship = shipRepository.getReferenceById(shipId);
		Planet planet = planetRepository.getReferenceById(planetId);

		if (planet.getPlayer() != ship.getPlayer()) {
			throw new IllegalArgumentException("Planet " + planetId + " does not belong to player of ship " + shipId + "!");
		}

		ShipRouteEntry routeEntry = new ShipRouteEntry();
		routeEntry.setShip(ship);
		routeEntry.setPlanet(planet);
		routeEntry.setOrderId(ship.getRoute().size());
		routeEntry.setFuelAction(action);
		routeEntry.setMoneyAction(action);
		routeEntry.setSupplyAction(action);
		routeEntry.setMineral1Action(action);
		routeEntry.setMineral2Action(action);
		routeEntry.setMineral3Action(action);

		routeEntry = shipRouteEntryRepository.save(routeEntry);

		ship = routeEntry.getShip();

		if (ship.getCurrentRouteEntry() == null) {
			List<ShipRouteEntry> routeEntries = shipRouteEntryRepository.findByShipId(shipId);

			if (routeEntries.size() > 1) {
				ship.setRouteTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());

				if (ship.getRoutePrimaryResource() == null) {
					ship.setRoutePrimaryResource(Resource.SUPPLIES);
				}

				if (ship.getRouteMinFuel() <= 0) {
					ship.setRouteMinFuel(30);
				}

				ship.updateRoute(routeEntries.get(0));
				shipRepository.save(ship);
			}
		}

		visibleObjects.clearCaches();
	}

	@PreAuthorize("hasPermission(#entryId, 'route') and hasPermission(#entryId, 'turnNotDoneRoute')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void changeRouteEntryPlanet(long entryId, long planetId) {
		ShipRouteEntry entry = shipRouteEntryRepository.getReferenceById(entryId);

		Planet planet = planetRepository.getReferenceById(planetId);

		if (entry.getShip().getPlayer() != planet.getPlayer()) {
			throw new IllegalArgumentException("Planet " + planetId + " does not belong to player of ship " + entry.getShip().getId() + "!");
		}

		entry.setPlanet(planet);

		entry = shipRouteEntryRepository.save(entry);

		Ship ship = entry.getShip();

		if (ship.getCurrentRouteEntry() == entry && !ship.isRouteDisabled()) {
			ship.setDestinationX(planet.getX());
			ship.setDestinationY(planet.getY());
			ship.setTravelSpeed(ship.getRouteTravelSpeed());
			shipRepository.save(ship);
		}
	}

	@PreAuthorize("hasPermission(#entryId, 'route') and hasPermission(#entryId, 'turnNotDoneRoute')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void changeRouteEntryAction(long entryId, Resource resource, ShipRouteResourceAction action) {
		ShipRouteEntry entry = shipRouteEntryRepository.getReferenceById(entryId);

		switch (resource) {
			case FUEL -> entry.setFuelAction(action);
			case MINERAL1 -> entry.setMineral1Action(action);
			case MINERAL2 -> entry.setMineral2Action(action);
			case MINERAL3 -> entry.setMineral3Action(action);
			case SUPPLIES -> entry.setSupplyAction(action);
			case MONEY -> entry.setMoneyAction(action);
			default -> throw new IllegalStateException("Unexpected value: " + resource);
		}

		shipRouteEntryRepository.save(entry);
	}

	@PreAuthorize("hasPermission(#entryId, 'route') and hasPermission(#entryId, 'turnNotDoneRoute')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void toggleWaitForFullStorage(long entryId) {
		ShipRouteEntry entry = shipRouteEntryRepository.getReferenceById(entryId);
		entry.setWaitForFullStorage(!entry.isWaitForFullStorage());
		shipRouteEntryRepository.save(entry);
	}

	@PreAuthorize("hasPermission(#entryId, 'route') and hasPermission(#entryId, 'turnNotDoneRoute')")
	@Transactional(propagation = Propagation.REQUIRED)
	public long moveRouteEntryUp(long entryId) {
		ShipRouteEntry entry = shipRouteEntryRepository.getReferenceById(entryId);
		Ship ship = entry.getShip();
		List<ShipRouteEntry> routeEntries = shipRouteEntryRepository.findByShipId(ship.getId());

		int index = routeEntries.indexOf(entry);

		if (index > 0) {
			swapRouteEntries(index, index - 1, routeEntries);
		}

		return ship.getId();
	}

	@PreAuthorize("hasPermission(#entryId, 'route') and hasPermission(#entryId, 'turnNotDoneRoute')")
	@Transactional(propagation = Propagation.REQUIRED)
	public long moveRouteEntryDown(long entryId) {
		ShipRouteEntry entry = shipRouteEntryRepository.getReferenceById(entryId);
		Ship ship = entry.getShip();
		List<ShipRouteEntry> routeEntries = shipRouteEntryRepository.findByShipId(ship.getId());

		int index = routeEntries.indexOf(entry);

		if (index < routeEntries.size() - 1) {
			swapRouteEntries(index, index + 1, routeEntries);
		}

		return ship.getId();
	}

	private void swapRouteEntries(int index1, int index2, List<ShipRouteEntry> routeEntries) {
		ShipRouteEntry a = routeEntries.get(index1);
		ShipRouteEntry b = routeEntries.get(index2);
		routeEntries.set(index1, b);
		routeEntries.set(index2, a);

		for (int i = 0; i < routeEntries.size(); i++) {
			ShipRouteEntry entry = routeEntries.get(i);
			entry.setOrderId(i);
			shipRouteEntryRepository.save(entry);
		}

		visibleObjects.clearCaches();
	}

	@PreAuthorize("hasPermission(#entryId, 'route') and hasPermission(#entryId, 'turnNotDoneRoute')")
	@Transactional(propagation = Propagation.REQUIRED)
	public long deleteRouteEntry(long entryId) {
		ShipRouteEntry entry = shipRouteEntryRepository.getReferenceById(entryId);
		Ship ship = entry.getShip();
		List<ShipRouteEntry> routeEntries = shipRouteEntryRepository.findByShipId(ship.getId());

		if (routeEntries.size() > 2) {
			if (ship.getCurrentRouteEntry() == entry) {
				int index = routeEntries.indexOf(entry) + 1;

				if (index >= routeEntries.size() - 1) {
					index = 0;
				}

				ShipRouteEntry next = routeEntries.get(index);
				ship.setCurrentRouteEntry(next);

				if (!ship.isRouteDisabled()) {
					ship.setDestinationX(next.getPlanet().getX());
					ship.setDestinationY(next.getPlanet().getY());
					ship.setTravelSpeed(ship.getRouteTravelSpeed());
				}

				shipRepository.save(ship);
			}
		} else {
			ship.resetTravel();
			ship.setCurrentRouteEntry(null);

			shipRepository.save(ship);
		}

		shipRouteEntryRepository.delete(entry);

		visibleObjects.clearCaches();

		return ship.getId();
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void setRouteTravelSpeed(long shipId, int travelSpeed) {
		if (travelSpeed < 0 || travelSpeed > 9) {
			throw new IllegalArgumentException("Invalid travel speed value: " + travelSpeed);
		}

		Ship ship = shipRepository.getReferenceById(shipId);
		ship.setTravelSpeed(travelSpeed);
		ship.setRouteTravelSpeed(travelSpeed);
		shipRepository.save(ship);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(propagation = Propagation.REQUIRED)
	public String setRouteDisabled(long shipId, boolean disabled) {
		Ship ship = shipRepository.getReferenceById(shipId);
		ship.setRouteDisabled(disabled);
		ship.resetTravel();

		if (!disabled && ship.getCurrentRouteEntry() != null) {
			Planet planet = ship.getCurrentRouteEntry().getPlanet();
			ship.setDestinationX(planet.getX());
			ship.setDestinationY(planet.getY());
			ship.setTravelSpeed(ship.getRouteTravelSpeed());
		}

		ship = shipRepository.save(ship);

		return shipTaskString.retrieveTaskString(ship);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void setRouteMinFuel(long shipId, int minFuel) {
		Ship ship = shipRepository.getReferenceById(shipId);

		if (minFuel < 0 || minFuel > ship.getShipTemplate().getFuelCapacity()) {
			throw new IllegalArgumentException("Invalid minFuel value: " + minFuel);
		}

		ship.setRouteMinFuel(minFuel);
		shipRepository.save(ship);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void setRoutePrimaryResource(long shipId, Resource primaryResource) {
		Ship ship = shipRepository.getReferenceById(shipId);
		ship.setRoutePrimaryResource(primaryResource);
		shipRepository.save(ship);
	}
}
