package org.skrupeltng.modules.ingame.modules.ship.service;

@java.lang.SuppressWarnings("java:S115")
public enum ShipTransportResource {

	colonists("fa fa-male", "colonists"),

	fuel("fas fa-circle", "fuel"),

	money("fa fa-coins", "money"),

	supplies("fa fa-cube", "supplies"),

	mineral1("fas fa-certificate", "mineral1"),

	mineral2("fas fa-square-full", "mineral2"),

	mineral3("fas fa-play fa-rotate-90", "mineral3"),

	lightgroundunits("fa fa-male skr-light-ground-units", "light_ground_units"),

	heavygroundunits("fa fa-male skr-heavy-ground-unit", "heavy_ground_units");

	private final String icon;
	private final String iconTitle;

	ShipTransportResource(String icon, String iconTitle) {
		this.icon = icon;
		this.iconTitle = iconTitle;
	}

	public String getIcon() {
		return icon;
	}

	public String getIconTitle() {
		return iconTitle;
	}
}
