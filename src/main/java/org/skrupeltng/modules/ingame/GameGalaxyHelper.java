package org.skrupeltng.modules.ingame;

import org.springframework.stereotype.Component;

@Component("galaxyHelper")
public class GameGalaxyHelper {

	public String getCoordinatesStyle(int x, int y) {
		return getCoordinatesStyle(x, y, 0);
	}

	public String getCoordinatesStyle(int x, int y, int addition) {
		x += addition;
		y += addition;

		return "left: " + x + "px; top: " + y + "px;";
	}

	public String getCoordinatesPercentageStyle(int x, int y, float galaxySize) {
		float xp = (x / galaxySize) * 100f;
		float yp = (y / galaxySize) * 100f;

		return "left: " + xp + "%; top: " + yp + "%;";
	}

	public String getGalaxySizeStyle(int size) {
		return "width: " + size + "px; height: " + size + "px;";
	}

	public String getVisibilityCoordinatesMinimapStyle(int galaxySize) {
		int wrapperSmall = 286;
		int wrapperBig = 530;

		float galaxySizeWithPadding = galaxySize + 250f;
		float factorSmall = wrapperSmall / galaxySizeWithPadding;
		float factorBig = wrapperBig / galaxySizeWithPadding;

		int paddingSmall = (int)(factorSmall * 125);
		int paddingBig = (int)(factorBig * 125);

		int mapSmall = wrapperSmall - (paddingSmall * 2);
		int mapBig = wrapperBig - (paddingBig * 2);

		int visSmall = (int)(250 * factorSmall);
		int visBig = (int)(250 * factorBig);

		return "@media only screen and (max-width: 991px) {\n" +
				"	#skr-game-mini-map-wrapper {\n" +
				"		width: " + wrapperSmall + "px;\n" +
				"		height: " + wrapperSmall + "px;\n" +
				"	}\n" +
				"	#skr-game-mini-map {\n" +
				"		width: " + mapSmall + "px;\n" +
				"		height: " + mapSmall + "px;\n" +
				"		left: " + paddingSmall + "px;\n" +
				"		top: " + paddingSmall + "px;\n" +
				"	}\n" +
				"}\n" +
				"\n" +
				"@media only screen and (min-width: 992px) {\n" +
				"	#skr-game-mini-map-wrapper {\n" +
				"		width: " + wrapperBig + "px;\n" +
				"		height: " + wrapperBig + "px;\n" +
				"	}\n" +
				"	#skr-game-mini-map {\n" +
				"		width: " + mapBig + "px;\n" +
				"		height: " + mapBig + "px;\n" +
				"		left: " + paddingBig + "px;\n" +
				"		top: " + paddingBig + "px;\n" +
				"	}\n" +
				"}\n" +
				"\n" +
				"@media only screen and (max-width: 991px) {\n" +
				"	#skr-game-mini-map .skr-ingame-visibility-background {\n" +
				"		width: " + visSmall + "px;\n" +
				"		height: " + visSmall + "px;\n" +
				"	}\n" +
				"}\n" +
				"\n" +
				"@media only screen and (min-width: 992px) {\n" +
				"	#skr-game-mini-map .skr-ingame-visibility-background {\n" +
				"		width: " + visBig + "px;\n" +
				"		height: " + visBig + "px;\n" +
				"	}\n" +
				"}";
	}
}