package org.skrupeltng.modules.ingame.service.round.ship;

import java.util.Optional;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GravityWaveGenerator {

	@Autowired
	private PlanetRepository planetRepository;

	protected Optional<Double> getDistanceToIntersection(int centerX, int centerY, Ship ship, int effectRadius) {
		Vector2D p1 = new Vector2D(ship.getX(), ship.getY());

		if (new Vector2D(centerX, centerY).distance(p1) < effectRadius) {
			return Optional.of(0.0);
		}

		Vector2D p2 = new Vector2D(ship.getDestinationX(), ship.getDestinationY());
		Vector2D dp = p2.subtract(p1);

		double a = dp.getNormSq();
		double b = 2 * (dp.getX() * (p1.getX() - centerX) + dp.getY() * (p1.getY() - centerY));
		double c = (p1.getX() - centerX) * (p1.getX() - centerX) + (p1.getY() - centerY) * (p1.getY() - centerY) - effectRadius * effectRadius;
		double det = b * b - 4 * a * c;

		if (a <= 0.00001 || det <= 0) {
			return Optional.empty();
		}

		double sqrtDet = Math.sqrt(det);
		double twoA = 2 * a;
		double t1 = (-b + sqrtDet) / twoA;
		Vector2D intersection1 = getInterceptionPoint(p1, dp, t1);

		double t2 = (-b - sqrtDet) / twoA;
		Vector2D intersection2 = getInterceptionPoint(p1, dp, t2);

		double intersectionDist1 = intersection1.distance(p1);
		double intersectionDist2 = intersection2.distance(p1);

		double intersectionDist = intersectionDist1 < intersectionDist2 ? intersectionDist1 : intersectionDist2;
		return Optional.of(intersectionDist);
	}

	private Vector2D getInterceptionPoint(Vector2D p1, Vector2D dp, double t) {
		return new Vector2D(p1.getX() + t * dp.getX(), p1.getY() + t * dp.getY());
	}

	public Optional<Double> preventShipsFromJumpingIntoGravityWaveZone(int centerX, int centerY, Ship ship, int effectRadius) {
		Optional<Double> distanceToIntersectionOpt = getDistanceToIntersection(centerX, centerY, ship, effectRadius);

		if (distanceToIntersectionOpt.isPresent()) {
			int dist = distanceToIntersectionOpt.get().intValue();
			ship.updateDestinationBasedOnDistance(dist);

			ship.setX(ship.getDestinationX());
			ship.setY(ship.getDestinationY());
			ship.resetTask();
			ship.resetTravel();
			ship.setPlanet(planetRepository.findByGameIdAndXAndY(ship.getPlayer().getGame().getId(), ship.getX(), ship.getY()).orElse(null));
		}

		return distanceToIntersectionOpt;
	}
}