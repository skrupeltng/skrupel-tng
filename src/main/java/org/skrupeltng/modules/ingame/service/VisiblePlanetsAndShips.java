package org.skrupeltng.modules.ingame.service;

import java.util.List;

import org.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.skrupeltng.modules.ingame.controller.ShipCluster;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

public class VisiblePlanetsAndShips {

	private List<PlanetEntry> planets;
	private List<Ship> singleShips;
	private List<ShipCluster> shipClusters;
	private List<Ship> allShips;

	public VisiblePlanetsAndShips(List<PlanetEntry> planets, List<Ship> singleShips, List<ShipCluster> shipClusters, List<Ship> allShips) {
		this.planets = planets;
		this.singleShips = singleShips;
		this.shipClusters = shipClusters;
		this.allShips = allShips;
	}

	public List<PlanetEntry> getPlanets() {
		return planets;
	}

	public void setPlanets(List<PlanetEntry> planets) {
		this.planets = planets;
	}

	public List<Ship> getSingleShips() {
		return singleShips;
	}

	public void setSingleShips(List<Ship> singleShips) {
		this.singleShips = singleShips;
	}

	public List<ShipCluster> getShipClusters() {
		return shipClusters;
	}

	public void setShipClusters(List<ShipCluster> shipClusters) {
		this.shipClusters = shipClusters;
	}

	public List<Ship> getAllShips() {
		return allShips;
	}

	public void setAllShips(List<Ship> allShips) {
		this.allShips = allShips;
	}
}