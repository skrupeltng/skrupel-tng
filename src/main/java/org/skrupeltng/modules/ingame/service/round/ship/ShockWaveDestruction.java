package org.skrupeltng.modules.ingame.service.round.ship;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.service.AchievementService;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.ShockWaveDestructionLogEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.ShockWaveDestructionLogEntryLogEntryRepository;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.service.round.DestroyedShipsCounter;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

@Component
public class ShockWaveDestruction {

	private final ShockWaveDestructionLogEntryLogEntryRepository shockWaveDestructionLogEntryLogEntryRepository;
	private final ShipRepository shipRepository;
	private final NewsService newsService;
	private final AchievementService achievementService;
	private final PoliticsService politicsService;
	private final ConfigProperties configProperties;

	public ShockWaveDestruction(ShockWaveDestructionLogEntryLogEntryRepository shockWaveDestructionLogEntryLogEntryRepository, ShipRepository shipRepository, NewsService newsService, AchievementService achievementService, PoliticsService politicsService, ConfigProperties configProperties) {
		this.shockWaveDestructionLogEntryLogEntryRepository = shockWaveDestructionLogEntryLogEntryRepository;
		this.shipRepository = shipRepository;
		this.newsService = newsService;
		this.achievementService = achievementService;
		this.politicsService = politicsService;
		this.configProperties = configProperties;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Collection<Long> damageShipsWithShockWave(long gameId, Ship ship, boolean subSpaceDistortion, DestroyedShipsCounter destroyedShipsCounter) {
		int range = subSpaceDistortion ? configProperties.getSubSpaceDistortionRange() : configProperties.getAutoDestructRange();
		int damageFactor = subSpaceDistortion
			? 50 * ship.getAbilityValue(ShipAbilityType.SUB_SPACE_DISTORTION, ShipAbilityConstants.SUB_SPACE_DISTORTION_LEVEL).orElseThrow().intValue()
			: 250;

		List<Long> shipIds = shipRepository.getShipIdsInRadius(gameId, ship.getX(), ship.getY(), range);
		shipIds.remove(ship.getId());

		Player shockWavePlayer = ship.getPlayer();

		List<Ship> ships = shipRepository.findByIds(shipIds);

		ShockWaveDestructionCalculator calculator = new ShockWaveDestructionCalculator(ships, damageFactor);
		calculator.calculate();

		Map<Player, List<ShockWaveDestructionLogEntry>> logEntriesMap = calculator.getLogEntries();
		int destroyedEnemyShips = 0;

		for (Entry<Player, List<ShockWaveDestructionLogEntry>> entry : logEntriesMap.entrySet()) {
			destroyedEnemyShips = getDestroyedEnemyShips(subSpaceDistortion, shockWavePlayer, destroyedEnemyShips, entry);
		}

		if (destroyedEnemyShips >= 7) {
			achievementService.unlockAchievement(shockWavePlayer, AchievementType.DESTROY_SEVEN_SHIPS_BY_SUBSPACE_DISTORTION);
		}

		Set<Long> destroyedShipIds = new HashSet<>(ships.size());

		for (Ship s : ships) {
			if (s.destroyed()) {
				destroyedShipsCounter.addDestroyedShip(s.getPlayer().getId());
				destroyedShipIds.add(s.getId());
			} else {
				shipRepository.save(s);
			}
		}

		return destroyedShipIds;
	}

	private int getDestroyedEnemyShips(boolean subSpaceDistortion, Player shockWavePlayer, int destroyedEnemyShips, Entry<Player, List<ShockWaveDestructionLogEntry>> entry) {
		Player victimPlayer = entry.getKey();
		List<ShockWaveDestructionLogEntry> logEntries = entry.getValue();

		int damaged = 0;
		int destroyed = 0;

		boolean isAlly = politicsService.isAlly(victimPlayer.getId(), shockWavePlayer.getId());

		for (ShockWaveDestructionLogEntry logEntry : logEntries) {
			if (logEntry.isDestroyed()) {
				destroyed++;

				if (!isAlly) {
					destroyedEnemyShips++;
				}
			} else {
				damaged++;
			}
		}

		ShockWaveDestructionLogEntry firstLogEntry = logEntries.get(0);

		collectLogs(subSpaceDistortion, victimPlayer, logEntries, damaged, destroyed, firstLogEntry);
		return destroyedEnemyShips;
	}

	private void collectLogs(boolean subSpaceDistortion, Player victimPlayer, List<ShockWaveDestructionLogEntry> logEntries, int damaged, int destroyed, ShockWaveDestructionLogEntry firstLogEntry) {
		String newsTemplate;
		Object[] args;
		boolean saveLogEntries = false;

		if (damaged == 1 && destroyed == 1) {
			saveLogEntries = true;
			newsTemplate = getSingleDamagedAndSingleDestroyed(subSpaceDistortion);
			args = new Object[]{};
		} else if (damaged == 1 && destroyed == 0) {
			newsTemplate = getSingleDamagedAndNoneDestroyed(subSpaceDistortion);
			args = new Object[]{firstLogEntry.getShipName(), firstLogEntry.getDamage()};
		} else if (damaged == 0 && destroyed == 1) {
			newsTemplate = getNoneDamagedAndSingleDestroyed(subSpaceDistortion);
			args = new Object[]{firstLogEntry.getShipName()};
		} else if (damaged > 1 && destroyed == 0) {
			saveLogEntries = true;
			newsTemplate = getMultipleDamagedAndNoneDestroyed(subSpaceDistortion);
			args = new Object[]{damaged};
		} else if (damaged == 0 && destroyed > 1) {
			saveLogEntries = true;
			newsTemplate = getNoneDamagedAndMultipleDestroyed(subSpaceDistortion);
			args = new Object[]{destroyed};
		} else if (damaged == 1 && destroyed > 1) {
			saveLogEntries = true;
			newsTemplate = getSingleDamagedAndMultipleDestroyed(subSpaceDistortion);
			args = new Object[]{destroyed};
		} else if (damaged > 1 && destroyed == 1) {
			saveLogEntries = true;
			newsTemplate = getMultipleDamagedAndSingleDestroyed(subSpaceDistortion);
			args = new Object[]{damaged};
		} else {
			saveLogEntries = true;
			newsTemplate = getMultipleDamagedAndMultipleDestroyed(subSpaceDistortion);
			args = new Object[]{damaged, destroyed};
		}

		saveLogs(victimPlayer, logEntries, firstLogEntry, newsTemplate, args, saveLogEntries);
	}

	private void saveLogs(Player victimPlayer, List<ShockWaveDestructionLogEntry> logEntries, ShockWaveDestructionLogEntry firstLogEntry, String newsTemplate, Object[] args, boolean saveLogEntries) {
		String image = firstLogEntry.getShipTemplate().createFullImagePath();
		NewsEntry newsEntry = newsService.add(victimPlayer, newsTemplate, image, null, null, args);

		if (saveLogEntries) {
			for (ShockWaveDestructionLogEntry logEntry : logEntries) {
				logEntry.setNewsEntry(newsEntry);
			}

			shockWaveDestructionLogEntryLogEntryRepository.saveAll(logEntries);
		}
	}

	private String getMultipleDamagedAndMultipleDestroyed(boolean subSpaceDistortion) {
		return subSpaceDistortion ? NewsEntryConstants.news_entry_damaged_and_destroyed_by_subspace_distortion_multiple
			: NewsEntryConstants.news_entry_damaged_and_destroyed_by_auto_destruction_multiple;
	}

	private String getMultipleDamagedAndSingleDestroyed(boolean subSpaceDistortion) {
		return subSpaceDistortion ? NewsEntryConstants.news_entry_multiple_damaged_and_one_destroyed_by_subspace_distortion
			: NewsEntryConstants.news_entry_multiple_damaged_and_one_destroyed_by_auto_destruction;
	}

	private String getSingleDamagedAndMultipleDestroyed(boolean subSpaceDistortion) {
		return subSpaceDistortion ? NewsEntryConstants.news_entry_one_damaged_and_multiple_destroyed_by_subspace_distortion
			: NewsEntryConstants.news_entry_one_damaged_and_multiple_destroyed_by_auto_destruction;
	}

	private String getNoneDamagedAndMultipleDestroyed(boolean subSpaceDistortion) {
		return subSpaceDistortion ? NewsEntryConstants.news_entry_destroyed_by_subspace_distortion_multiple
			: NewsEntryConstants.news_entry_destroyed_by_auto_destruction_multiple;
	}

	private String getMultipleDamagedAndNoneDestroyed(boolean subSpaceDistortion) {
		return subSpaceDistortion ? NewsEntryConstants.news_entry_damaged_by_subspace_distortion_multiple
			: NewsEntryConstants.news_entry_damaged_by_auto_destruction_multiple;
	}

	private String getNoneDamagedAndSingleDestroyed(boolean subSpaceDistortion) {
		return subSpaceDistortion ? NewsEntryConstants.news_entry_destroyed_by_subspace_distortion
			: NewsEntryConstants.news_entry_destroyed_by_auto_destruction;
	}

	private String getSingleDamagedAndNoneDestroyed(boolean subSpaceDistortion) {
		return subSpaceDistortion ? NewsEntryConstants.news_entry_damaged_by_subspace_distortion : NewsEntryConstants.news_entry_damaged_by_auto_destruction;
	}

	private String getSingleDamagedAndSingleDestroyed(boolean subSpaceDistortion) {
		return subSpaceDistortion ? NewsEntryConstants.news_entry_damaged_and_destroyed_by_subspace_distortion
			: NewsEntryConstants.news_entry_damaged_and_destroyed_by_auto_destruction;
	}
}
