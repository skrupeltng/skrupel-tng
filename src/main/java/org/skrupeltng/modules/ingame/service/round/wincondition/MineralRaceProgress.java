package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.skrupeltng.modules.dashboard.database.Team;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.ToIntFunction;

public class MineralRaceProgress {

	private final Game game;
	private final int targetQuantity;
	private final Collection<Player> players;

	public MineralRaceProgress(Game game, int targetQuantity, Collection<Player> players) {
		this.game = game;
		this.targetQuantity = targetQuantity;
		this.players = players;
	}

	public List<Player> calculatePlayersWithTargetProgress() {
		ToIntFunction<Player> function = getPlayerToIntFunction();
		return check(function);
	}

	public ToIntFunction<Player> getPlayerToIntFunction() {
		return switch (game.getMineralRaceStorageType()) {
			case HOME_PLANET -> this::getHomePlanetQuantity;
			case STARBASE_PLANET -> this::getStarbasesQuantity;
			case PLANET -> this::getPlanetsQuantity;
			case FLEET -> this::getShipsQuantity;
			case FLEET_IN_EMPTY_SPACE -> this::getShipsInSpaceQuantity;
		};
	}

	private List<Player> check(ToIntFunction<Player> function) {
		List<Player> winners = new ArrayList<>(players.size());

		if (game.isUseFixedTeams()) {
			for (Team team : game.getTeams()) {
				int teamQuantity = 0;

				for (Player player : team.getPlayers()) {
					teamQuantity += function.applyAsInt(player);
				}

				if (teamQuantity >= targetQuantity) {
					winners.addAll(team.getPlayers());
				}
			}
		} else {
			for (Player player : players) {
				int quantity = function.applyAsInt(player);

				if (quantity >= targetQuantity) {
					winners.add(player);
				}
			}
		}

		return winners;
	}

	private int getHomePlanetQuantity(Player player) {
		Planet homePlanet = player.getHomePlanet();

		if (homePlanet != null) {
			return homePlanet.retrieveResourceQuantity(game.getMineralRaceMineralName());
		}

		return 0;
	}

	private int getStarbasesQuantity(Player player) {
		Set<Planet> planets = player.getPlanets();
		int sum = 0;

		for (Planet planet : planets) {
			if (planet.getStarbase() != null) {
				sum += planet.retrieveResourceQuantity(game.getMineralRaceMineralName());
			}
		}

		return sum;
	}

	private int getPlanetsQuantity(Player player) {
		Set<Planet> planets = player.getPlanets();
		int sum = 0;

		for (Planet planet : planets) {
			sum += planet.retrieveResourceQuantity(game.getMineralRaceMineralName());
		}

		return sum;
	}

	private int getShipsQuantity(Player player) {
		Set<Ship> ships = player.getShips();
		int sum = 0;

		for (Ship ship : ships) {
			sum += ship.retrieveResourceQuantity(game.getMineralRaceMineralName());
		}

		return sum;
	}

	private int getShipsInSpaceQuantity(Player player) {
		Set<Ship> ships = player.getShips();
		int sum = 0;

		for (Ship ship : ships) {
			if (ship.getPlanet() == null) {
				sum += ship.retrieveResourceQuantity(game.getMineralRaceMineralName());
			}
		}

		return sum;
	}
}
