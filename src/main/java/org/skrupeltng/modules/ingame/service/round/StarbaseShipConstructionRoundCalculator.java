package org.skrupeltng.modules.ingame.service.round;

import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetService;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseHullStock;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseHullStockRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbasePropulsionStock;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbasePropulsionStockRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseShipConstructionJob;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseShipConstructionJobRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseWeaponStock;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseWeaponStockRepository;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.database.WeaponTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Component
public class StarbaseShipConstructionRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private StarbaseShipConstructionJobRepository starbaseShipConstructionJobRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private StarbaseHullStockRepository starbaseHullStockRepository;

	@Autowired
	private StarbasePropulsionStockRepository starbasePropulsionStockRepository;

	@Autowired
	private StarbaseWeaponStockRepository starbaseWeaponStockRepository;

	@Autowired
	private FleetRepository fleetRepository;

	@Autowired
	private FleetService fleetService;

	@Autowired
	private StatsUpdater statsUpdater;

	@Autowired
	private NewsService newsService;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processShipConstructionJobs(long gameId) {
		log.debug("Processing ship construction jobs...");

		List<StarbaseShipConstructionJob> jobs = starbaseShipConstructionJobRepository.findByGameId(gameId);

		for (StarbaseShipConstructionJob job : jobs) {
			Planet planet = job.getStarbase().getPlanet();
			buildShip(job, planet);

			if (planet.hasOrbitalSystem(OrbitalSystemType.HUNTER_CONSTRUCTION_YARD)) {
				buildAdditionalHunter(planet, job);
			}
		}

		starbaseShipConstructionJobRepository.deleteAll(jobs);

		log.debug("Ship construction jobs processed.");
	}

	private void buildShip(StarbaseShipConstructionJob job, Planet planet) {
		Ship ship = new Ship();
		ship.setShield(100);
		ship.setCrew(job.getShipTemplate().getCrew());
		ship.setDestinationX(-1);
		ship.setDestinationY(-1);
		ship.setName(job.getShipName());
		ship.setPlayer(planet.getPlayer());
		ship.setShipTemplate(job.getShipTemplate());
		ship.setPropulsionSystemTemplate(job.getPropulsionSystemTemplate());
		ship.setEnergyWeaponTemplate(job.getEnergyWeaponTemplate());
		ship.setProjectileWeaponTemplate(job.getProjectileWeaponTemplate());
		ship.setX(planet.getX());
		ship.setY(planet.getY());
		ship.setCreationX(planet.getX());
		ship.setCreationY(planet.getY());
		ship.setPlanet(planet);
		ship.setShipModule(job.getShipModule());

		ship = shipRepository.save(ship);

		Long fleetId = job.getFleetId();

		if (fleetId != null) {
			Optional<Fleet> fleetOpt = fleetRepository.findById(fleetId);

			if (fleetOpt.isPresent()) {
				fleetService.addShipToFleetWithoutPermissionChecks(ship, fleetOpt.get());
			}
		}

		playerRepository.increaseNewShips(planet.getPlayer().getId());

		newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_constructed, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName());
	}

	private void buildAdditionalHunter(Planet planet, StarbaseShipConstructionJob job) {
		ShipTemplate shipTemplate = job.getShipTemplate();

		if (shipTemplate.getMass() >= 100) {
			return;
		}

		if (shipTemplate.getShipAbilities().stream().anyMatch(a -> a.getType() != ShipAbilityType.LUCKY_SHOT && a.getType() != ShipAbilityType.OVERDRIVE)) {
			return;
		}

		Starbase starbase = planet.getStarbase();

		StarbaseHullStock hullStock = null;
		Optional<StarbaseHullStock> hullStockOpt = starbase.getHullStocks().stream().filter(s -> s.getShipTemplate() == shipTemplate).findAny();
		if (!hullStockOpt.isPresent()) {
			return;
		}

		hullStock = hullStockOpt.get();

		StarbaseWeaponStock energyWeaponStock = null;
		WeaponTemplate energyWeaponTemplate = job.getEnergyWeaponTemplate();

		if (energyWeaponTemplate != null) {
			Optional<StarbaseWeaponStock> energyWeaponStockOpt = starbase.getWeaponStocks().stream().filter(s -> s.getWeaponTemplate() == energyWeaponTemplate)
					.findAny();

			if (!energyWeaponStockOpt.isPresent()) {
				return;
			}

			energyWeaponStock = energyWeaponStockOpt.get();

			if (energyWeaponStock.getStock() < shipTemplate.getEnergyWeaponsCount()) {
				return;
			}
		}

		StarbaseWeaponStock projectileWeaponStock = null;
		WeaponTemplate projectileWeaponTemplate = job.getProjectileWeaponTemplate();

		if (projectileWeaponTemplate != null) {
			Optional<StarbaseWeaponStock> projectileWeaponStockOpt = starbase.getWeaponStocks().stream()
					.filter(s -> s.getWeaponTemplate() == projectileWeaponTemplate)
					.findAny();

			if (!projectileWeaponStockOpt.isPresent()) {
				return;
			}

			projectileWeaponStock = projectileWeaponStockOpt.get();

			if (projectileWeaponStock.getStock() < shipTemplate.getProjectileWeaponsCount()) {
				return;
			}
		}

		PropulsionSystemTemplate propulsionSystemTemplate = job.getPropulsionSystemTemplate();
		Optional<StarbasePropulsionStock> propulsionStockOpt = starbase.getPropulsionStocks().stream()
				.filter(s -> s.getPropulsionSystemTemplate() == propulsionSystemTemplate).findAny();

		if (!propulsionStockOpt.isPresent()) {
			return;
		}

		StarbasePropulsionStock propulsionStock = propulsionStockOpt.get();

		if (propulsionStock.getStock() < shipTemplate.getPropulsionSystemsCount()) {
			return;
		}

		StarbaseShipConstructionJob newJob = new StarbaseShipConstructionJob();
		newJob.setStarbase(starbase);
		newJob.setShipTemplate(shipTemplate);
		newJob.setEnergyWeaponTemplate(energyWeaponTemplate);
		newJob.setProjectileWeaponTemplate(projectileWeaponTemplate);
		newJob.setPropulsionSystemTemplate(propulsionSystemTemplate);
		newJob.setShipName(job.getShipName() + "(2)");

		hullStock.setStock(hullStock.getStock() - 1);

		if (hullStock.getStock() == 0) {
			starbaseHullStockRepository.delete(hullStock);
		} else {
			starbaseHullStockRepository.save(hullStock);
		}

		if (energyWeaponStock != null) {
			energyWeaponStock.setStock(energyWeaponStock.getStock() - shipTemplate.getEnergyWeaponsCount());

			if (energyWeaponStock.getStock() == 0) {
				starbaseWeaponStockRepository.delete(energyWeaponStock);
			} else {
				starbaseWeaponStockRepository.save(energyWeaponStock);
			}
		}

		if (projectileWeaponStock != null) {
			projectileWeaponStock.setStock(projectileWeaponStock.getStock() - shipTemplate.getProjectileWeaponsCount());

			if (projectileWeaponStock.getStock() == 0) {
				starbaseWeaponStockRepository.delete(projectileWeaponStock);
			} else {
				starbaseWeaponStockRepository.save(projectileWeaponStock);
			}
		}

		propulsionStock.setStock(propulsionStock.getStock() - shipTemplate.getPropulsionSystemsCount());

		if (propulsionStock.getStock() == 0) {
			starbasePropulsionStockRepository.delete(propulsionStock);
		} else {
			starbasePropulsionStockRepository.save(propulsionStock);
		}

		statsUpdater.incrementStats(starbase.getPlanet().getPlayer(), LoginStatsFaction::getShipsBuilt, LoginStatsFaction::setShipsBuilt,
				AchievementType.SHIPS_BUILT_1, AchievementType.SHIPS_BUILT_20, AchievementType.SHIPS_BUILT_100);

		buildShip(newJob, planet);
	}
}
