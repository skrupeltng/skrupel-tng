package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.ingame.database.player.Player;

import java.util.Collections;
import java.util.List;

public record WinnersCalculationResult(List<Player> winners, List<AchievementType> unlockedAchievements) {

	public WinnersCalculationResult() {
		this(Collections.emptyList());
	}

	public WinnersCalculationResult(List<Player> winners) {
		this(winners, Collections.emptyList());
	}
}
