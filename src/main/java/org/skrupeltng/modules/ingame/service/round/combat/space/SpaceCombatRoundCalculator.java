package org.skrupeltng.modules.ingame.service.round.combat.space;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetService;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.spacecombat.SpaceCombatLogEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.spacecombat.SpaceCombatLogEntryItem;
import org.skrupeltng.modules.ingame.modules.overview.database.spacecombat.SpaceCombatLogEntryItemRepository;
import org.skrupeltng.modules.ingame.modules.overview.database.spacecombat.SpaceCombatLogEntryRepository;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipRemoval;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.service.round.DestroyedShipsCounter;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Component
public class SpaceCombatRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private ShipRouteEntryRepository shipRouteEntryRepository;

	@Autowired
	private ShipService shipService;

	@Autowired
	private ShipRemoval shipRemoval;

	@Autowired
	private PoliticsService politicsService;

	@Autowired
	private NewsService newsService;

	@Autowired
	private FleetService fleetService;

	@Autowired
	private StatsUpdater statsUpdater;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private SpaceCombatLogEntryRepository spaceCombatLogEntryRepository;

	@Autowired
	private SpaceCombatLogEntryItemRepository spaceCombatLogEntryItemRepository;

	@Autowired
	private ConfigProperties configProperties;

	@Autowired
	private PlayerRepository playerRepository;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processCombat(long gameId) {
		log.debug("Processing space combat...");

		List<Ship> ships = shipRepository.findForSpaceCombat(gameId);

		DestroyedShipsCounter destroyedShipsCounter = new DestroyedShipsCounter(playerRepository);
		SpaceCombatData data = new SpaceCombatData(destroyedShipsCounter);

		for (Ship ship : ships) {
			long playerId = ship.getPlayer().getId();
			List<Ship> enemyShips = shipRepository.findEnemyShipsOnSamePosition(gameId, playerId, ship.getX(), ship.getY());

			processCombatEncounters(enemyShips, ship, data);
		}

		Collection<SpaceCombatEncounterNewsEntryData> newsEntryData = createEncounterSummaryNewsEntries(data);

		for (SpaceCombatEncounterNewsEntryData entry : newsEntryData) {
			NewsEntry newsEntry = newsService.add(entry.getPlayer(), entry.getNewsEntryTemplate(), entry.getImage(), null, null, entry.getArguments());
			List<SpaceCombatLogEntry> shipItems = entry.getLogEntries();

			for (SpaceCombatLogEntry item : shipItems) {
				item.setNewsEntry(newsEntry);
				item = spaceCombatLogEntryRepository.save(item);

				SpaceCombatLogEntryItem ownItem = item.getOwnItem();
				ownItem.setSpaceCombatLogEntry(item);
				spaceCombatLogEntryItemRepository.save(ownItem);

				SpaceCombatLogEntryItem enemyItem = item.getEnemyItem();
				enemyItem.setSpaceCombatLogEntry(item);
				spaceCombatLogEntryItemRepository.save(enemyItem);
			}
		}

		data.getCapturedShipIds().removeAll(data.getDestroyedShipIds());

		if (!data.getDestroyedShipIds().isEmpty()) {
			destroyedShipsCounter.updateCounters();
			shipRemoval.deleteShipsByIds(data.getDestroyedShipIds());
		}

		for (Long shipId : data.getCapturedShipIds()) {
			shipRepository.clearCurrentRouteEntry(shipId);
			shipRouteEntryRepository.deleteByShipId(shipId);
		}

		shipRepository.restoreShields(gameId);

		log.debug("Space combat processed.");
	}

	protected Collection<SpaceCombatEncounterNewsEntryData> createEncounterSummaryNewsEntries(SpaceCombatData data) {
		Map<String, SpaceCombatEncounterNewsEntryData> newsEntryData = new HashMap<>();
		Set<Coordinate> encounterCoordinates = data.getEncounters().keySet();

		for (Coordinate coordinate : encounterCoordinates) {
			List<SpaceCombatEncounterResultData> encounters = data.getEncounters().get(coordinate);

			for (SpaceCombatEncounterResultData encounter : encounters) {
				if (encounter.isShip1Evaded() || encounter.isShip2Evaded()) {
					continue;
				}

				Ship ship1 = encounter.getShip1();
				Ship ship2 = encounter.getShip2();

				boolean ship1Destroyed = encounter.isShip1Destroyed();
				boolean ship2Destroyed = encounter.isShip2Destroyed();
				boolean ship1Captured = encounter.isShip1Captured();
				boolean ship2Captured = encounter.isShip2Captured();

				processShipPairEncounter(newsEntryData, ship1, ship2, ship1Destroyed, ship2Destroyed, ship1Captured, ship2Captured);
				processShipPairEncounter(newsEntryData, ship2, ship1, ship2Destroyed, ship1Destroyed, ship2Captured, ship1Captured);
			}
		}

		Collection<SpaceCombatEncounterNewsEntryData> newsEntries = newsEntryData.values();

		for (SpaceCombatEncounterNewsEntryData entry : newsEntries) {
			entry.finishData();
		}

		return newsEntries;
	}

	private void processShipPairEncounter(Map<String, SpaceCombatEncounterNewsEntryData> newsEntryData, Ship ship, Ship enemyShip, boolean ownDestroyed,
										  boolean enemyDestroyed, boolean ownCaptured, boolean enemyCaptured) {
		int x = ship.getX();
		int y = ship.getY();
		Player player = ship.getPlayer();

		if (ownCaptured) {
			x = ship.getLastX();
			y = ship.getLastY();
			player = ship.getPlayerBeforeCapture();

			if (player == null) {
				player = ship.getPlayer();
			}
		}

		String key = x + "_" + y + "_" + player.getId();
		SpaceCombatEncounterNewsEntryData entry = newsEntryData.get(key);

		if (entry == null) {
			entry = new SpaceCombatEncounterNewsEntryData(player, x, y);
			newsEntryData.put(key, entry);
		}

		entry.addShipPair(ship, enemyShip, ownDestroyed, enemyDestroyed, ownCaptured, enemyCaptured);
	}

	private void processCombatEncounters(List<Ship> enemyShips, Ship ship, SpaceCombatData data) {
		for (Ship enemyShip : enemyShips) {
			if (data.shipIsDestroyed(ship)) {
				break;
			}

			if (data.shipIsDestroyed(enemyShip)) {
				continue;
			}

			if (ship.getX() != enemyShip.getX() || ship.getY() != enemyShip.getY()) {
				continue;
			}

			checkStructurScanner(ship, enemyShip);
			checkStructurScanner(enemyShip, ship);

			if (politicsService.isAlly(ship.getPlayer().getId(), enemyShip.getPlayer().getId())) {
				continue;
			}

			Optional<SpaceCombatEncounterResultData> encounterOpt = data.addShipPair(ship, enemyShip);

			if (encounterOpt.isPresent()) {
				SpaceCombatEncounterResultData encounter = encounterOpt.get();
				SpaceCombatCalculator calculator = new SpaceCombatCalculator(encounter, getSupport(ship), getSupport(enemyShip));
				calculator.calculate();

				if (encounter.isShip1Evaded()) {
					evade(ship, enemyShip, true);
				}
				if (encounter.isShip2Evaded()) {
					evade(enemyShip, ship, true);
				}

				if (encounter.isShip1Evaded() || encounter.isShip2Evaded()) {
					continue;
				}

				if (encounter.isShip1HitLuckyShot()) {
					handleLuckyShot(ship, enemyShip, data);
				} else if (encounter.isShip2HitLuckyShot()) {
					handleLuckyShot(enemyShip, ship, data);
				} else {
					handleShipDamage(ship, enemyShip, encounter, data);
					handleShipDamage(enemyShip, ship, encounter, data);
				}
			}
		}
	}

	private void checkStructurScanner(Ship ship, Ship enemyShip) {
		if (ship.getAbility(ShipAbilityType.STRUCTUR_SCANNER).isPresent()) {
			shipService.checkStructureScanner(ship.getPlayer(), enemyShip);
		}
	}

	public int getSupport(Ship ship) {
		long playerId = ship.getPlayer().getId();
		int x = ship.getX();
		int y = ship.getY();

		ShipTemplate template = ship.getShipTemplate();
		int minWeaponCount = Math.max(Math.max(template.getEnergyWeaponsCount(), template.getProjectileWeaponsCount()), template.getHangarCapacity());

		int minTechLevel = template.getTechLevel() - 2;

		if (minTechLevel < 1) {
			minTechLevel = 1;
		}

		long advantage = shipRepository.getSupportingShipCount(playerId, x, y, ship.getId(), minWeaponCount / 2, minTechLevel);

		if (advantage > minWeaponCount) {
			advantage = minWeaponCount;
		}

		advantage += ship.getExperienceForCombat();

		long comCenterCount = shipRepository.getCommunicationCenterShipCount(playerId, x, y, ship.getId());

		if (comCenterCount > 0) {
			advantage++;
		}

		return (int) advantage;
	}

	private void handleLuckyShot(Ship ship1, Ship ship2, SpaceCombatData data) {
		String sector = ship1.getSectorString();
		newsService.add(ship1.getPlayer(), NewsEntryConstants.news_entry_lucky_shot_success, ship1.createFullImagePath(), ship1.getId(),
			NewsEntryClickTargetType.ship, ship1.getName(), sector, ship2.getName());
		newsService.add(ship2.getPlayer(), NewsEntryConstants.news_entry_lucky_shot_victim, ship2.createFullImagePath(), null, null,
			ship2.getName(), sector, ship1.getName());
		data.addToDestroyedShips(ship2);

		statsUpdater.incrementStats(ship1.getPlayer(), LoginStatsFaction::getShipsDestroyed, LoginStatsFaction::setShipsDestroyed,
			AchievementType.SHIPS_DESTROYED_1, AchievementType.SHIPS_DESTROYED_10);
		statsUpdater.incrementStats(ship2.getPlayer(), LoginStatsFaction::getShipsLost, LoginStatsFaction::setShipsLost);
	}

	private void handleShipDamage(Ship ship, Ship enemyShip, SpaceCombatEncounterResultData encounter, SpaceCombatData data) {
		processExperience(ship, enemyShip);

		if (ship.getCrew() <= 0 && enemyShip.getCrew() <= 0) {
			data.addToDestroyedShips(ship);

			statsUpdater.incrementStats(enemyShip.getPlayer(), LoginStatsFaction::getShipsDestroyed, LoginStatsFaction::setShipsDestroyed,
				AchievementType.SHIPS_DESTROYED_1, AchievementType.SHIPS_DESTROYED_10);
			statsUpdater.incrementStats(ship.getPlayer(), LoginStatsFaction::getShipsLost, LoginStatsFaction::setShipsLost);
		} else if (ship.destroyed()) {
			data.addToDestroyedShips(ship);

			statsUpdater.incrementStats(enemyShip.getPlayer(), LoginStatsFaction::getShipsDestroyed, LoginStatsFaction::setShipsDestroyed);
			statsUpdater.incrementStats(ship.getPlayer(), LoginStatsFaction::getShipsLost, LoginStatsFaction::setShipsLost);
		} else {
			if (!enemyShip.destroyed() && ship.getCrew() <= 0) {
				statsUpdater.incrementStats(enemyShip.getPlayer(), LoginStatsFaction::getShipsCaptured, LoginStatsFaction::setShipsCaptured,
					AchievementType.SHIPS_CAPTURED);
				statsUpdater.incrementStats(ship.getPlayer(), LoginStatsFaction::getShipsLost, LoginStatsFaction::setShipsLost);

				fleetService.clearFleetOfShipWithoutPermissionCheck(ship.getId());

				ship.setPlayerBeforeCapture(ship.getPlayer());
				ship.setPlayer(enemyShip.getPlayer());
				ship.setExperience(0);
				ship.resetTask();
				ship.resetTravel();

				data.addToCapturedShips(ship);

				evade(ship, enemyShip, false);
			} else if (ship.getCrew() > 0 && encounter.isEvadeAfterFirstCombatRound()) {
				encounter.setShip1Evaded(true);
				evade(ship, enemyShip, true);
			}

			shipRepository.save(ship);
		}
	}

	private void evade(Ship ship, Ship enemyShip, boolean addNews) {
		Game game = ship.getPlayer().getGame();

		CoordinateImpl next = Coordinate.getRandomNearCoordinates(ship.getX(), ship.getY(), configProperties.getEvadeDistance(), game.getGalaxySize());
		ship.setLastX(ship.getX());
		ship.setLastY(ship.getY());
		ship.setX(next.getX());
		ship.setY(next.getY());
		ship.setPlanet(planetRepository.findByGameIdAndXAndY(game.getId(), next.getX(), next.getY()).orElse(null));

		if (addNews) {
			String sector = ship.getSectorString();

			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_evaded_successfull, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName(), enemyShip.getName(), sector);
		}
	}

	private void processExperience(Ship ship, Ship enemyShip) {
		if (ship.getExperienceForCombat() < configProperties.getMaxExperience()) {
			ship.increaseExperienceBySpaceCombat(enemyShip);
		}
	}
}
