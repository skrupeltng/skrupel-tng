package org.skrupeltng.modules.ingame.service;

import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.dashboard.modules.notification.service.NotificationConstants;
import org.skrupeltng.modules.dashboard.modules.notification.service.NotificationService;
import org.skrupeltng.modules.dashboard.modules.storymode.database.StoryModeCampaign;
import org.skrupeltng.modules.dashboard.modules.storymode.service.StoryModeMissionProvider;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.controller.MineFieldEntry;
import org.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.skrupeltng.modules.ingame.controller.RoundFinishedDataDTO;
import org.skrupeltng.modules.ingame.controller.ShipCluster;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.PlayerGameTurnInfoItem;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.PlasmaStorm;
import org.skrupeltng.modules.ingame.modules.anomaly.database.PlasmaStormRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFold;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHole;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.service.round.RoundCalculationService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Service
public class IngameService {

	private final GameRepository gameRepository;
	private final LoginRepository loginRepository;
	private final PlayerRepository playerRepository;
	private final PlasmaStormRepository plasmaStormRepository;
	private final RoundCalculationService roundCalculationService;
	private final NotificationService notificationService;
	private final VisibleObjects visibleObjects;
	private final Map<String, StoryModeMissionProvider> missionProviders;

	public IngameService(GameRepository gameRepository,
						 LoginRepository loginRepository,
						 PlayerRepository playerRepository,
						 PlasmaStormRepository plasmaStormRepository,
						 RoundCalculationService roundCalculationService,
						 NotificationService notificationService,
						 VisibleObjects visibleObjects,
						 Map<String, StoryModeMissionProvider> missionProviders) {
		this.gameRepository = gameRepository;
		this.loginRepository = loginRepository;
		this.playerRepository = playerRepository;
		this.plasmaStormRepository = plasmaStormRepository;
		this.roundCalculationService = roundCalculationService;
		this.notificationService = notificationService;
		this.visibleObjects = visibleObjects;
		this.missionProviders = missionProviders;
	}

	public Optional<Game> getGame(long id) {
		return gameRepository.findById(id);
	}

	public long getPlayerId(long loginId, long gameId) {
		return playerRepository.getIdByLoginIdAndGameId(loginId, gameId);
	}

	public Player getPlayer(long loginId, long gameId) {
		return playerRepository.findByLoginIdAndGameId(loginId, gameId);
	}

	public List<PlayerGameTurnInfoItem> getTurnInfosByLogin(long loginId, long excludedGameId) {
		return gameRepository.getTurnInfos(loginId, excludedGameId);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public List<PlanetEntry> getPlanets(long gameId, long loginId, Set<CoordinateImpl> visibilityCoordinates, boolean getAll) {
		long playerId = getAll ? 0L : getPlayerId(loginId, gameId);

		return visibleObjects.getVisiblePlanets(gameId, visibilityCoordinates, playerId, getAll);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public Set<CoordinateImpl> getVisibilityCoordinates(long gameId, long loginId) {
		long playerId = getPlayerId(loginId, gameId);
		return visibleObjects.getVisibilityCoordinates(playerId);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(propagation = Propagation.REQUIRED)
	public boolean finishTurn(long gameId, long loginId) {
		Optional<Player> playerResult = playerRepository.findByGameIdAndLoginId(gameId, loginId);

		if (playerResult.isPresent()) {
			Player player = playerResult.get();
			player.setTurnFinished(true);
			player.setLastFinishedRound(player.getGame().getRound());
			player = playerRepository.save(player);

			return allPlayersHaveFinishedTheirTurns(player.getGame());
		}

		throw new IllegalArgumentException("Login %d does not take part in game %d!".formatted(loginId, gameId));
	}

	private boolean allPlayersHaveFinishedTheirTurns(Game game) {
		return game.getPlayers().stream().noneMatch(p -> !p.isTurnFinished() && !p.isHasLost());
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public void calculateRound(long gameId, long triggeringLoginId) {
		Optional<Game> gameOpt = getGame(gameId);

		if (gameOpt.isEmpty()) {
			throw new IllegalArgumentException("Game %d not found!".formatted(gameId));
		}

		if (allPlayersHaveFinishedTheirTurns(gameOpt.get())) {
			roundCalculationService.calculateRound(gameId, triggeringLoginId);

			Game game = gameOpt.get();
			List<Player> players = game.getPlayers();
			String gameName = gameRepository.getName(gameId);
			String link = "/ingame/game?id=" + gameId;

			for (Player player : players) {
				if (player.getLogin().getId() != triggeringLoginId && !player.isHasLost()) {
					notificationService.addNotification(player.getLogin(), link, NotificationConstants.notification_new_round, gameName);
				}
			}
		} else {
			throw new IllegalArgumentException("Not all players of game " + gameId + " have finished their turns!");
		}
	}

	public RoundFinishedDataDTO getRoundFinishedData(long gameId) {
		RoundFinishedDataDTO response = new RoundFinishedDataDTO();

		Game game = getGame(gameId).orElseThrow();
		StoryModeCampaign storyModeCampaign = game.getStoryModeCampaign();

		if (storyModeCampaign != null && game.isFinished()) {
			Player player = game.getPlayers().stream().filter(p -> p.getAiLevel() == null).findFirst().orElseThrow();
			response.setStoryModeMissionSuccess(!player.isHasLost());
			response.setStoryModeMissionFailed(player.isHasLost());

			if (!player.isHasLost() && game.getStoryModeMission() == storyModeCampaign.getMissionCount()) {
				StoryModeMissionProvider missionProvider = missionProviders.get(storyModeCampaign.getFaction().getId());
				Optional<String> nextFactionOpt = missionProvider.getNextCampaignFaction();
				response.setMoreCampaignsAvailable(nextFactionOpt.isPresent());
			} else {
				response.setMoreCampaignsAvailable(true);
			}
		}

		return response;
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public VisiblePlanetsAndShips getVisiblePlanetsAndShips(long gameId, long loginId, Set<CoordinateImpl> visibilityCoordinates, boolean getAll) {
		return getVisiblePlanetsAndShipsInternal(gameId, loginId, visibilityCoordinates, getAll);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public VisiblePlanetsAndShips getVisiblePlanetsAndShipsForFinishedGameMiniMap(long gameId) {
		return getVisiblePlanetsAndShipsInternal(gameId, 0L, null, true);
	}

	private VisiblePlanetsAndShips getVisiblePlanetsAndShipsInternal(long gameId, long loginId, Set<CoordinateImpl> visibilityCoordinates,
																	 boolean getAll) {
		List<PlanetEntry> planets = getPlanets(gameId, loginId, visibilityCoordinates, getAll);

		List<Ship> ships = getShips(gameId, loginId, visibilityCoordinates, getAll);

		Map<String, ShipCluster> shipClusterMap = createShipClusterMap(ships);

		Collection<ShipCluster> shipClusterValues = shipClusterMap.values();
		List<ShipCluster> shipClusters = shipClusterValues.stream().filter(c -> c.getShips().size() > 1).toList();

		List<Ship> singleShips = shipClusterValues.stream().filter(c -> c.getShips().size() == 1).flatMap(c -> c.getShips().stream()).toList();

		return new VisiblePlanetsAndShips(planets, singleShips, shipClusters, ships);
	}

	protected Map<String, ShipCluster> createShipClusterMap(List<Ship> ships) {
		Map<String, ShipCluster> shipClusters = new HashMap<>(ships.size());

		for (Ship ship : ships) {
			if (ship.getPlanet() == null) {
				String coord = ship.getX() + "_" + ship.getY();
				ShipCluster shipCluster = shipClusters.get(coord);
				int scanRadius = ship.getScanRadius();

				if (shipCluster == null) {
					shipCluster = new ShipCluster(ship.getX(), ship.getY(), scanRadius);
					shipClusters.put(coord, shipCluster);
				} else if (shipCluster.getScanRadius() < scanRadius) {
					shipCluster.setScanRadius(scanRadius);
				}

				shipCluster.getShips().add(ship);
			}
		}

		return shipClusters;
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Ship> getShips(long gameId, long loginId, Set<CoordinateImpl> visibilityCoordinates, boolean getAll) {
		long playerId = getAll ? 0L : getPlayerId(loginId, gameId);
		return visibleObjects.getVisibleShips(gameId, visibilityCoordinates, playerId, getAll);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<WormHole> getWormHoles(long gameId, Set<CoordinateImpl> visibilityCoordinates, long playerId) {
		return visibleObjects.getVisibleWormHoles(gameId, visibilityCoordinates, playerId);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<SpaceFold> getSpaceFolds(long gameId, Set<CoordinateImpl> visibilityCoordinates) {
		return visibleObjects.getVisibleSpaceFolds(gameId, visibilityCoordinates);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<PlasmaStorm> getPlasmaStorms(long gameId) {
		return plasmaStormRepository.findByGameId(gameId);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<MineFieldEntry> getMineFields(long gameId, Set<CoordinateImpl> visibilityCoordinates, long currentPlayerId) {
		return visibleObjects.getVisibleMineFields(gameId, visibilityCoordinates, currentPlayerId);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void overviewViewed(long gameId, long loginId) {
		Optional<Player> playerResult = playerRepository.findByGameIdAndLoginId(gameId, loginId);

		if (playerResult.isPresent()) {
			Player player = playerResult.get();
			player.setOverviewViewed(true);
			playerRepository.save(player);
		} else {
			throw new IllegalArgumentException("Login " + loginId + " does not take part in game " + gameId + "!");
		}
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void newsViewed(long gameId, long loginId) {
		Optional<Player> playerResult = playerRepository.findByGameIdAndLoginId(gameId, loginId);

		if (playerResult.isPresent()) {
			Player player = playerResult.get();
			player.setNewsViewed(true);
			playerRepository.save(player);
		} else {
			throw new IllegalArgumentException("Login " + loginId + " does not take part in game " + gameId + "!");
		}
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public boolean turnFinished(long gameId, long loginId) {
		Optional<Player> player = playerRepository.findByGameIdAndLoginId(gameId, loginId);
		return player.map(Player::isTurnFinished).orElse(false);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void changeIngameZoomLevel(long loginId, float ingameZoomLevel) {
		loginRepository.changeIngameZoomLevel(loginId, ingameZoomLevel);
	}

	public boolean wysiwygEnabled(long gameId) {
		return gameRepository.wysiwygEnabled(gameId);
	}
}
