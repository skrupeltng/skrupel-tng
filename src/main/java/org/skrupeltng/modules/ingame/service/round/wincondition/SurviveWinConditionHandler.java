package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.skrupeltng.modules.dashboard.database.Team;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("SURVIVE")
public class SurviveWinConditionHandler implements WinConditionHandler {

	@Override
	public WinnersCalculationResult calculate(Game game) {
		List<Player> notLostPlayers = game.getPlayers().stream().filter(p -> !p.isHasLost()).toList();

		if (notLostPlayers.size() == 1) {
			return new WinnersCalculationResult(notLostPlayers);
		}

		if (game.isUseFixedTeams()) {
			long remainingTeams = notLostPlayers.stream()
				.map(Player::getTeam)
				.map(Team::getId)
				.distinct()
				.count();

			if (remainingTeams == 1L) {
				return new WinnersCalculationResult(notLostPlayers);
			}
		}

		return new WinnersCalculationResult();
	}
}
