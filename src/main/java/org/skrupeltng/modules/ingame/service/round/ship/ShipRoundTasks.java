package org.skrupeltng.modules.ingame.service.round.ship;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ImageConstants;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.MineField;
import org.skrupeltng.modules.ingame.modules.anomaly.database.MineFieldRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsEntryCreationRequest;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpeciesEffect;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipRemoval;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipTransportService;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.ingame.service.round.DestroyedShipsCounter;
import org.skrupeltng.modules.ingame.service.round.RandomSplitter;
import org.skrupeltng.modules.ingame.service.round.combat.ShipDamage;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.database.WeaponTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Component
public class ShipRoundTasks {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private NewsService newsService;

	@Autowired
	private ShipTransportService shipTransportService;

	@Autowired
	private ShipRemoval shipRemoval;

	@Autowired
	private ShipService shipService;

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private MineFieldRepository mineFieldRepository;

	@Autowired
	private ConfigProperties configProperties;

	@Autowired
	private ShockWaveDestruction shockWaveDestruction;

	@Autowired
	private ShockWaveDestructionFinish shockWaveDestructionFinish;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void precheckTractorBeam(long gameId) {
		log.debug("Prechecking tractor beam...");

		List<Ship> ships = shipRepository.getShipsWithTask(gameId, ShipTaskType.TRACTOR_BEAM);

		for (Ship ship : ships) {
			if (ship.getTaskValue() != null) {
				long tractorTargetId = Long.parseLong(ship.getTaskValue());
				Ship tractorTarget = shipRepository.getReferenceById(tractorTargetId);

				if (tractorTarget.getX() != ship.getX() || tractorTarget.getY() != ship.getY()) {
					ship.resetTask();
					shipRepository.save(ship);
				} else {
					if (ship.getTravelSpeed() > 7) {
						ship.setTravelSpeed(7);
						shipRepository.save(ship);
					}

					tractorTarget.resetTask();
					tractorTarget.resetTravel();

					shipRepository.save(tractorTarget);
				}
			} else {
				ship.resetTask();
				shipRepository.save(ship);
			}
		}

		log.debug("Prechecking tractor beam finsihed.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processAutorefuel(long gameId) {
		log.debug("Processing auto refuel...");

		List<Ship> ships = shipRepository.getShipsWithTask(gameId, ShipTaskType.AUTOREFUEL);

		for (Ship ship : ships) {
			Planet planet = ship.getPlanet();

			if (planet != null) {
				int fuel = ship.getFuel();
				int fuelCapacity = ship.getShipTemplate().getFuelCapacity();
				int planetFuel = planet.getFuel();

				if (fuelCapacity > fuel && planetFuel > 0) {
					int freeFuelCapacity = fuelCapacity - fuel;

					if (planet.hasBunker(ship.getPlayer().getId())) {
						planetFuel -= 100;

						if (planetFuel <= 0) {
							continue;
						}
					}

					int fuelToBeAdded = Math.min(freeFuelCapacity, planetFuel);

					ship.setFuel(fuel + fuelToBeAdded);
					planet.spendFuel(fuelToBeAdded);
					shipRepository.save(ship);
					planetRepository.save(planet);
				}
			}
		}

		log.debug("Processing auto refuel finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processShipRecycle(long gameId) {
		log.debug("Processing ship recycle...");

		List<Ship> ships = shipRepository.getShipsWithTask(gameId, ShipTaskType.SHIP_RECYCLE);

		for (Ship ship : ships) {
			Planet planet = ship.getPlanet();
			Player player = ship.getPlayer();

			if (planet != null && planet.getPlayer() == player) {
				Starbase starbase = planet.getStarbase();

				if (starbase != null || planet.hasOrbitalSystem(OrbitalSystemType.RECYCLING_FACILITY)) {
					ShipTransportRequest request = new ShipTransportRequest();
					Pair<Ship, Planet> result = shipTransportService.transportWithoutPermissionCheck(request, ship, planet);
					ship = result.getFirst();
					planet = result.getSecond();

					ShipTemplate template = ship.getShipTemplate();
					int mineral1 = Math.round(template.getCostMineral1() * 0.85f);
					int mineral2 = Math.round(template.getCostMineral2() * 0.85f);
					int mineral3 = Math.round(template.getCostMineral3() * 0.85f);

					planet.spendMineral1(-mineral1);
					planet.spendMineral2(-mineral2);
					planet.spendMineral3(-mineral3);

					String image = ship.createFullImagePath();
					String name = ship.getName();

					planet = planetRepository.save(planet);
					shipRemoval.deleteShip(ship, null, false);

					newsService.add(player, NewsEntryConstants.news_entry_ship_recycled, image, planet.getId(), NewsEntryClickTargetType.planet, name, mineral1,
						mineral2, mineral3);
				}
			}
		}

		log.debug("Processing ship recycle finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processShipRepair(long gameId) {
		log.debug("Processing ship repair...");

		List<Ship> ships = shipRepository.getShipsWithTask(gameId, ShipTaskType.REPAIR);

		for (Ship ship : ships) {
			if (ship.getDamage() == 0) {
				continue;
			}

			Planet planet = ship.getPlanet();
			Player player = ship.getPlayer();

			if (planet != null && planet.getPlayer() == player) {
				Starbase starbase = planet.getStarbase();
				int repairAmount = 0;

				if (starbase != null) {
					StarbaseType type = starbase.getType();

					if (type == StarbaseType.STAR_BASE || type == StarbaseType.WAR_BASE) {
						repairAmount = 11;
					} else if (type == StarbaseType.SHIP_YARD) {
						repairAmount = 19;
					}
				}

				long playerId = player.getId();

				List<Ship> localShips = shipRepository.findByGameIdAndPlanetId(gameId, planet.getId());
				Optional<Float> shipRepairAmount = localShips.stream()
					.filter(s -> s.getPlayer().getId() == playerId && s.getAbility(ShipAbilityType.REPAIR).isPresent())
					.map(s -> s.getAbilityValue(ShipAbilityType.REPAIR, ShipAbilityConstants.REPAIR_PERCENTAGE))
					.filter(Optional::isPresent).map(Optional::get)
					.max(Comparator.naturalOrder());

				if (shipRepairAmount.isPresent()) {
					repairAmount = Math.max(repairAmount, shipRepairAmount.get().intValue());
				}

				if (repairAmount > 0) {
					int newDamage = ship.getDamage() - repairAmount;

					if (newDamage < 0) {
						newDamage = 0;
					}

					ship.setDamage(newDamage);

					ship = shipRepository.save(ship);

					if (newDamage == 0) {
						newsService.add(player, NewsEntryConstants.news_entry_ship_repair_complete, ship.createFullImagePath(), ship.getId(),
							NewsEntryClickTargetType.ship, ship.getName());
					}
				}
			}
		}

		log.debug("Processing ship repair finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processHireCrew(long gameId) {
		log.debug("Processing hire crew...");

		List<Ship> ships = shipRepository.getShipsWithTask(gameId, ShipTaskType.HIRE_CREW);

		for (Ship ship : ships) {
			int currentCrew = ship.getCrew();
			int maxCrew = ship.getShipTemplate().getCrew();
			Planet planet = ship.getPlanet();
			Player player = ship.getPlayer();

			if (planet != null && planet.getPlayer() == player && planet.getStarbase() != null) {
				int toBeHired = Integer.parseInt(ship.getTaskValue());
				int crewDiff = maxCrew - currentCrew;

				if (crewDiff < toBeHired) {
					toBeHired = crewDiff;
				}

				int colonists = planet.getColonists();

				if (colonists < toBeHired) {
					ship.setTaskValue("" + (toBeHired - colonists));
					toBeHired = colonists;
				}

				if (toBeHired > 0) {
					planet.setColonists(colonists - toBeHired);
					planetRepository.save(planet);

					ship.setCrew(currentCrew + toBeHired);

					if (ship.getCrew() == maxCrew) {
						ship.resetTask();

						newsService.add(player, NewsEntryConstants.news_entry_crew_hired_complete, ship.createFullImagePath(), ship.getId(),
							NewsEntryClickTargetType.ship, ship.getName(), toBeHired);
					} else {
						newsService.add(player, NewsEntryConstants.news_entry_crew_hired, ship.createFullImagePath(), ship.getId(),
							NewsEntryClickTargetType.ship, ship.getName(), toBeHired);
					}

					shipRepository.save(ship);
				}
			}
		}

		log.debug("Processing hire crew finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processPlanetBombardment(long gameId) {
		log.debug("Processing planet bombardment...");

		List<Ship> ships = shipRepository.getShipsWithTask(gameId, ShipTaskType.PLANET_BOMBARDMENT);
		Map<Long, PlanetaryBombardmentData> data = new HashMap<>();

		for (Ship ship : ships) {
			Planet planet = ship.getPlanet();

			if (planet == null) {
				continue;
			}

			Player shipPlayer = ship.getPlayer();
			Player planetPlayer = planet.getPlayer();

			if (shipPlayer == planetPlayer) {
				continue;
			}

			if (planetPlayer != null) {
				List<PlayerRelation> relationOpt = playerRelationRepository.findByPlayerIds(shipPlayer.getId(), planetPlayer.getId());

				if (!relationOpt.isEmpty() && relationOpt.get(0).getType() == PlayerRelationType.ALLIANCE) {
					continue;
				}

				ShipTemplate template = ship.getShipTemplate();
				int hangarDamage = template.getHangarCapacity() * 35;

				WeaponTemplate energyWeaponTemplate = ship.getEnergyWeaponTemplate();
				int energyDamage = 0;

				if (energyWeaponTemplate != null) {
					energyDamage = ShipDamage.ENERGY_WEAPON_DAMAGE_DATA[energyWeaponTemplate.getDamageIndex()] * template.getEnergyWeaponsCount();
				}

				WeaponTemplate projectileWeaponTemplate = ship.getProjectileWeaponTemplate();
				int projectileDamage = 0;

				if (projectileWeaponTemplate != null) {
					projectileDamage = ShipDamage.PROJECTILE_WEAPON_DAMAGE_DATA[projectileWeaponTemplate.getDamageIndex()] *
						template.getProjectileWeaponsCount();
				}

				int attackPower = Math.round((hangarDamage + energyDamage + projectileDamage) / 4f);

				List<Integer> attacks = RandomSplitter.getRandomValues(attackPower, 4);

				int colonistPercentage = attacks.get(0);

				Optional<Float> survivalRateEffectOpt = planet.retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.INCREASE_SURVIVAL_RATE);
				int maxColonistPercentage = 100;

				if (survivalRateEffectOpt.isPresent()) {
					maxColonistPercentage = maxColonistPercentage - survivalRateEffectOpt.get().intValue();
				}

				if (colonistPercentage > maxColonistPercentage) {
					colonistPercentage = maxColonistPercentage;
				}

				int minesPercentage = attacks.get(1);
				if (minesPercentage > 100) {
					minesPercentage = 100;
				}

				int factoriesPercentage = attacks.get(2);
				if (factoriesPercentage > 100) {
					factoriesPercentage = 100;
				}

				int defensePercentage = attacks.get(3);
				if (defensePercentage > 100) {
					defensePercentage = 100;
				}

				int colonistVictims = Math.round(planet.getColonists() / 100f * colonistPercentage);

				if (planet.hasBunker(ship.getPlayer().getId())) {
					int survivingColonists = Math.min(1000, planet.getColonists());
					colonistVictims = planet.getColonists() - survivingColonists;
				}

				int destroyedMines = Math.round(planet.getMines() / 100f * minesPercentage);
				int destroyedFactories = Math.round(planet.getFactories() / 100f * factoriesPercentage);
				int destroyedDefense = Math.round(planet.getPlanetaryDefense() / 100f * defensePercentage);

				PlanetaryBombardmentData entry = data.get(planet.getId());

				if (entry == null) {
					entry = new PlanetaryBombardmentData(planet);
					data.put(planet.getId(), entry);
				}

				planet.applyDamage(destroyedMines, destroyedFactories, destroyedDefense, colonistVictims);

				entry.addShip(ship);
				entry.addKilledColonists(colonistVictims);
				entry.addDestroyedMines(destroyedMines);
				entry.addDestroyedFactories(destroyedFactories);
				entry.addDestroyedDefense(destroyedDefense);

				planetRepository.save(planet);
			}
		}

		for (PlanetaryBombardmentData entry : data.values()) {
			entry.finishData();
			List<NewsEntryCreationRequest> requests = entry.getNewsEntryCreationRequests();

			for (NewsEntryCreationRequest request : requests) {
				newsService.add(request);
			}
		}

		log.debug("Processing planet bombardment finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processTractorBeam(long gameId) {
		log.debug("Processing tractor beam...");

		List<Ship> ships = shipRepository.getShipsWithTask(gameId, ShipTaskType.TRACTOR_BEAM);

		for (Ship ship : ships) {
			long tractorTargetId = Long.parseLong(ship.getTaskValue());
			Ship tractorTarget = shipRepository.getReferenceById(tractorTargetId);
			tractorTarget.resetTask();
			tractorTarget.resetTravel();

			tractorTarget.setPlanet(ship.getPlanet());
			tractorTarget.setX(ship.getX());
			tractorTarget.setY(ship.getY());

			shipRepository.save(tractorTarget);
		}

		log.debug("Processing tractor beam finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processAutomaticProjectileConstruction(long gameId) {
		log.debug("Processing automatic projectile construction...");

		List<Ship> ships = shipRepository.getShipWithAutomaticProjectileConstruction(gameId);

		for (Ship ship : ships) {
			int maxProjectiles = ship.retrieveMaxProjectiles();
			int diff = maxProjectiles - ship.getProjectiles();

			if (diff > 0) {
				shipService.buildProjectilesWithoutPermissionCheck(ship.getId(), diff);
			}
		}

		log.debug("Processing automatic projectile construction finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processHunterTraining(long gameId) {
		log.debug("Processing hunter training...");

		List<Ship> ships = shipRepository.getShipsWithTask(gameId, ShipTaskType.HUNTER_TRAINING);

		int moneyCosts = configProperties.getHunterTrainingMoneyCosts();
		int supplyCosts = configProperties.getHunterTrainingSupplyCosts();
		int fuelCosts = configProperties.getHunterTrainingFuelCosts();
		int maxExperience = configProperties.getMaxExperience();

		for (Ship ship : ships) {
			int money = ship.getMoney();
			int supplies = ship.getSupplies();
			int fuel = ship.getFuel();
			int experience = ship.getExperience();

			if (money >= moneyCosts && supplies >= supplyCosts && fuel >= fuelCosts && experience < maxExperience) {
				ship.setHunterTrainingMonths(ship.getHunterTrainingMonths() + 1);

				if (ship.getHunterTrainingMonths() == 5) {
					ship.setHunterTrainingMonths(0);
					ship.setExperience(experience + 1);
				}

				if (ship.getExperience() == maxExperience) {
					ship.setMoney(money - moneyCosts);
					ship.setSupplies(supplies - supplyCosts);
					ship.setFuel(fuel - fuelCosts);
					ship.setTaskType(null);
				}

				shipRepository.save(ship);
			}
		}

		log.debug("Processing hunter training finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processCreateMineField(long gameId) {
		log.debug("Processing create mine field...");

		if (!gameRepository.mineFieldsEnabled(gameId)) {
			return;
		}

		List<Ship> ships = shipRepository.getShipsWithTask(gameId, ShipTaskType.CREATE_MINE_FIELD);

		for (Ship ship : ships) {
			if (ship.getPlanet() == null) {
				int mines = Integer.parseInt(ship.getTaskValue());

				if (mines > ship.getProjectiles()) {
					mines = ship.getProjectiles();
				}

				if (mines > 0) {
					MineField mineField = new MineField();
					mineField.setGame(ship.getPlayer().getGame());
					mineField.setPlayer(ship.getPlayer());
					mineField.setX(ship.getX());
					mineField.setY(ship.getY());
					mineField.setMines(mines);
					mineField.setLevel(ship.getProjectileWeaponTemplate().getDamageIndex());
					mineFieldRepository.save(mineField);

					newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_created, ImageConstants.NEWS_MINE_FIELD, ship.getId(),
						NewsEntryClickTargetType.ship, ship.getName(), mines);
					ship.setProjectiles(ship.getProjectiles() - mines);
				}
			} else {
				newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_not_created, ImageConstants.NEWS_MINE_FIELD, ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName());
			}

			ship.resetTask();
			shipRepository.save(ship);
		}

		log.debug("Processing create mine field finished.");
	}

	public void processAutoDestruction(long gameId) {
		if (!configProperties.isAutoDestructShipModuleEnabled()) {
			return;
		}

		log.debug("Processing auto destruction...");

		List<Ship> ships = shipRepository.getShipsWithTask(gameId, ShipTaskType.AUTO_DESTRUCTION);

		DestroyedShipsCounter destroyedShipsCounter = new DestroyedShipsCounter(playerRepository);

		for (Ship ship : ships) {
			destroyedShipsCounter.addDestroyedShip(ship.getPlayer().getId());

			Collection<Long> destroyedShipIds = shockWaveDestruction.damageShipsWithShockWave(gameId, ship, false, destroyedShipsCounter);

			long shockWavePlayerId = ship.getPlayer().getId();

			for (Long shipId : destroyedShipIds) {
				shockWaveDestructionFinish.deleteDestroyedShip(shipId, shockWavePlayerId);
			}

			newsService.addWithNewTransaction(ship.getPlayer().getId(), NewsEntryConstants.news_entry_ship_auto_destructed, ship.createFullImagePath(), null,
				null, ship.getName());
		}

		destroyedShipsCounter.updateCounters();

		log.debug("Processing auto destruction finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteAutoDestructionShips(long gameId) {
		if (!configProperties.isAutoDestructShipModuleEnabled()) {
			return;
		}

		log.debug("Deleting auto destruction ships...");

		List<Ship> ships = shipRepository.getShipsWithTask(gameId, ShipTaskType.AUTO_DESTRUCTION);

		for (Ship ship : ships) {
			shipRemoval.deleteShip(ship, null, false);
		}

		log.debug("Deleting auto destruction ships finished.");
	}
}
