package org.skrupeltng.modules.ingame.service.round;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class AutoTickService {

	private static final Logger log = LoggerFactory.getLogger(AutoTickService.class);

	private final GameRepository gameRepository;
	private final RoundCalculationService roundCalculationService;

	public AutoTickService(GameRepository gameRepository, RoundCalculationService roundCalculationService) {
		this.gameRepository = gameRepository;
		this.roundCalculationService = roundCalculationService;
	}

	// Every two seconds
	@Scheduled(fixedDelay = 2000)
	public void performAutoTicks() {
		List<Game> games = gameRepository.findAutoTickGames();
		final LocalDateTime now = LocalDateTime.now();

		for (Game game : games) {
			LocalDateTime dt = game.getRoundDate().atZone(ZoneId.systemDefault()).toLocalDateTime();
			LocalDateTime nextAutoTick = dt.plusSeconds(game.getAutoTickSeconds());

			if (nextAutoTick.isBefore(now)) {
				try {
					roundCalculationService.calculateRound(game.getId());
				} catch (Exception e) {
					log.error("Error while calculating auto tick for game " + game.getId() + ": ", e);
				}
			}
		}
	}
}
