package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.skrupeltng.modules.dashboard.database.Team;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component("CONQUEST")
public class ConquestWinConditionHandler implements WinConditionHandler {

	@Override
	public WinnersCalculationResult calculate(Game game) {
		if (game.isUseFixedTeams()) {
			List<Player> winners = new ArrayList<>();

			for (Team team : game.getTeams()) {
				Optional<Integer> teamVictoryPoints = team.getPlayers().stream()
					.map(Player::getVictoryPoints)
					.reduce(Integer::sum);

				if (teamVictoryPoints.isPresent() && teamVictoryPoints.get() >= game.getConquestRounds()) {
					winners.addAll(team.getPlayers());
				}
			}

			return new WinnersCalculationResult(winners);
		}

		List<Player> players = game.getPlayers();

		return new WinnersCalculationResult(players.stream()
			.filter(p -> p.getVictoryPoints() >= game.getConquestRounds())
			.toList());
	}
}
