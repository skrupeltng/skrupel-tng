package org.skrupeltng.modules.ingame.service.round.losecondition;

import java.util.Set;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.springframework.stereotype.Component;

@Component("LOSE_STATIONS")
public class LoseStationsLoseConditionHandler implements LoseConditionHandler {

	@Override
	public boolean hasLost(Player player) {
		Set<Planet> planets = player.getPlanets();

		if (planets.size() == 0) {
			return true;
		}

		return !planets.stream().anyMatch(p -> p.getStarbase() != null);
	}
}