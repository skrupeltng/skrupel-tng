package org.skrupeltng.modules.ingame.service.round.combat.space;

import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTactics;
import org.skrupeltng.modules.ingame.service.round.combat.ShipDamage;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.service.MasterDataService;

public class SpaceCombatCalculator {

	private final SpaceCombatEncounterResultData encounter;
	private final int support1;
	private final int support2;

	public SpaceCombatCalculator(SpaceCombatEncounterResultData encounter, int support1, int support2) {
		this.encounter = encounter;
		this.support1 = support1;
		this.support2 = support2;
	}

	public void calculate() {
		if (bothEvade()) {
			return;
		}

		Ship ship1 = encounter.getShip1();
		Ship ship2 = encounter.getShip2();

		if (checkLuckyShot(ship1)) {
			encounter.setShip1HitLuckyShot(true);
			return;
		}

		if (checkLuckyShot(ship2)) {
			encounter.setShip2HitLuckyShot(true);
			return;
		}

		ShipDamage shipDamage1 = new ShipDamage(ship1, ship2.getTactics(), support1 - support2);
		ShipDamage shipDamage2 = new ShipDamage(ship2, ship1.getTactics(), support2 - support1);

		if (shipDamage1.cannotDealDamage() && shipDamage2.cannotDealDamage()) {
			performCombatWithoutWeapons();
		} else {
			checkShieldDamper(ship1);
			checkShieldDamper(ship2);

			encounter.setEvadeAfterFirstCombatRound(evadeAfterFirstCombatRound());

			processCombat(shipDamage1, shipDamage2);

			if (ship1.destroyed() || ship2.destroyed()) {
				encounter.setEvadeAfterFirstCombatRound(false);
			}
		}

		updateDestroyedFlags(ship1, ship2);
	}

	private void updateDestroyedFlags(Ship ship1, Ship ship2) {
		if (ship1.destroyed()) {
			encounter.setShip1Destroyed(true);
		} else if (ship1.getCrew() == 0 && !ship2.destroyed()) {
			encounter.setShip1Captured(true);
		}

		if (ship2.destroyed()) {
			encounter.setShip2Destroyed(true);
		} else if (ship2.getCrew() == 0 && !ship1.destroyed()) {
			encounter.setShip2Captured(true);
		}
	}

	protected void processCombat(ShipDamage shipDamage1, ShipDamage shipDamage2) {
		Ship ship1 = encounter.getShip1();
		Ship ship2 = encounter.getShip2();

		boolean ignoreShields1 = ship1.hasActiveAbility(ShipAbilityType.SHIELD_DAMPER);
		boolean ignoreShields2 = ship2.hasActiveAbility(ShipAbilityType.SHIELD_DAMPER);

		while (ship1.getDamage() < 100 && ship2.getDamage() < 100 && ship1.getCrew() > 0 && ship2.getCrew() > 0) {
			if (shipDamage1.cannotDealDamage() && shipDamage2.cannotDealDamage()) {
				performCombatWithoutWeapons();
				return;
			}

			for (int round = 1; round <= 10; round++) {
				if (ship1.getDamage() < 100 && ship2.getDamage() < 100 && ship1.getCrew() > 0 && ship2.getCrew() > 0) {
					shipDamage1.damageEnemyShip(ship2, round, ignoreShields1, shipDamage2.getMass());
					shipDamage2.damageEnemyShip(ship1, round, ignoreShields2, shipDamage1.getMass());
				}
			}

			if (encounter.isEvadeAfterFirstCombatRound()) {
				return;
			}
		}
	}

	protected boolean bothEvade() {
		Ship ship1 = encounter.getShip1();
		Ship ship2 = encounter.getShip2();
		Player player = ship1.getPlayer();
		Game game = player.getGame();

		if (game.isEnableTactialCombat() && ship1.getTactics() == ShipTactics.DEFENSIVE && ship2.getTactics() == ShipTactics.DEFENSIVE) {
			encounter.setShip1Evaded(true);
			encounter.setShip2Evaded(true);
			return true;
		}

		return false;
	}

	protected boolean checkLuckyShot(Ship ship) {
		if (ship.getAbility(ShipAbilityType.LUCKY_SHOT).isPresent()) {
			float chance = ship.getAbilityValue(ShipAbilityType.LUCKY_SHOT, ShipAbilityConstants.LUCKY_SHOT_PROBABILTY).orElseThrow().intValue() / 100f;
			chance += ship.getExperienceForCombat();
			float value = MasterDataService.RANDOM.nextFloat();

			return chance > value;
		}

		return false;
	}

	protected void performCombatWithoutWeapons() {
		Ship ship1 = encounter.getShip1();
		Ship ship2 = encounter.getShip2();
		ShipTemplate template1 = ship1.getShipTemplate();
		ShipTemplate template2 = ship2.getShipTemplate();

		int mass1 = template1.getMass();
		int mass2 = template2.getMass();

		if (mass1 == mass2) {
			ship1.setDamage(100);
			ship2.setDamage(100);
		} else if (mass1 > mass2) {
			ship1.setDamage(ship1.getDamage() + Math.round(mass2 / (mass1 / 100f)));
			ship1.setShield(0);
			ship2.setDamage(100);
		} else {
			ship2.setDamage(ship2.getDamage() + Math.round(mass1 / (mass2 / 100f)));
			ship2.setShield(0);
			ship1.setDamage(100);
		}
	}

	protected boolean evadeAfterFirstCombatRound() {
		Ship ship1 = encounter.getShip1();
		Ship ship2 = encounter.getShip2();

		if (ship1.getPlayer().getGame().isEnableTactialCombat()) {
			ShipTactics t1 = ship1.getTactics();
			ShipTactics t2 = ship2.getTactics();

			return (t1 == ShipTactics.DEFENSIVE && t2 == ShipTactics.STANDARD) || (t2 == ShipTactics.DEFENSIVE && t1 == ShipTactics.STANDARD);
		}

		return false;
	}

	protected void checkShieldDamper(Ship ship) {
		ShipAbility activeAbility = ship.getActiveAbility();

		if (activeAbility != null && activeAbility.getType() == ShipAbilityType.SHIELD_DAMPER) {
			int costs = ship.getAbilityValue(ShipAbilityType.SHIELD_DAMPER, ShipAbilityConstants.SHIELD_DAMPER_COSTS).orElseThrow().intValue();

			if (ship.getMineral2() >= costs) {
				ship.setMineral2(ship.getMineral2() - costs);
			} else {
				ship.setActiveAbility(null);
			}
		}
	}
}
