package org.skrupeltng.modules.ingame.service.round.ship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.database.ShockWaveDestructionLogEntry;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.service.round.combat.ShipDamage;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;

public class ShockWaveDestructionCalculator {

	private final List<Ship> ships;
	private final int damageFactor;

	private final Map<Player, List<ShockWaveDestructionLogEntry>> logEntries = new HashMap<>();

	public ShockWaveDestructionCalculator(List<Ship> ships, int damageFactor) {
		this.ships = ships;
		this.damageFactor = damageFactor;
	}

	public void calculate() {
		for (Ship ship : ships) {
			if (ship.hasActiveAbility(ShipAbilityType.SUB_SPACE_DISTORTION) || ship.getTaskType() == ShipTaskType.AUTO_DESTRUCTION) {
				continue;
			}

			int mass = ship.getShipTemplate().getMass();
			int newDamage = (int) Math.round(damageFactor * Math.pow(ShipDamage.DAMAGE_MASS_FACTOR / (mass + 1), 2) + 2);

			ship.setDamage(ship.getDamage() + newDamage);

			Player victimPlayer = ship.getPlayer();

			ShockWaveDestructionLogEntry logEntry = new ShockWaveDestructionLogEntry();
			logEntry.setDamage(newDamage);
			logEntry.setShipName(ship.getName());
			logEntry.setShipId(ship.getId());
			logEntry.setShipTemplate(ship.getShipTemplate());
			logEntry.setDestroyed(ship.destroyed());

			List<ShockWaveDestructionLogEntry> playerLogEntries = logEntries.computeIfAbsent(victimPlayer, k -> new ArrayList<>());

			playerLogEntries.add(logEntry);
		}
	}

	public Map<Player, List<ShockWaveDestructionLogEntry>> getLogEntries() {
		return logEntries;
	}
}
