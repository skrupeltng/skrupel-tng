package org.skrupeltng.modules.ingame.service.round;

import java.util.List;

import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetLeader;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class FleetRoundCalculator {

	@Autowired
	private FleetRepository fleetRepository;

	@Autowired
	private FleetLeader fleetLeader;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateFleetsWithoutLeader(long gameId) {
		List<Fleet> fleets = fleetRepository.findFleetsWithoutLeader(gameId);

		for (Fleet fleet : fleets) {
			Ship ship = fleet.getShips().get(0);
			fleetLeader.setLeaderWithoutPermissionCheck(fleet.getId(), ship.getId());
		}
	}
}
