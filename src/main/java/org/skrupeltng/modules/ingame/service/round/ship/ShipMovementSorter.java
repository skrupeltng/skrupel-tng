package org.skrupeltng.modules.ingame.service.round.ship;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ShipMovementSorter {

	@Autowired
	private ConfigProperties configProperties;

	public List<Ship> sortMovingShips(List<Ship> ships) {
		if (ships.size() < 2) {
			return ships;
		}

		Set<Ship> workingSet = createWorkingSet(ships);

		if (workingSet.size() < 2) {
			return ships;
		}

		searchCircles(workingSet);
		breakCircles(workingSet);
		breakBranches(workingSet);

		ships.sort(Comparator.comparing(Ship::getFlightQueue));

		return ships;
	}

	protected Set<Ship> createWorkingSet(List<Ship> ships) {
		Set<Ship> workingSet = new TreeSet<>(Comparator.comparing(Ship::getId));

		for (Ship ship : ships) {
			if (ship.getDestinationShip() != null) {
				workingSet.add(ship);
				workingSet.add(ship.getDestinationShip());
			}
		}

		return workingSet;
	}

	@java.lang.SuppressWarnings("java:S1854")
	private void searchCircles(Collection<Ship> ships) {
		for (final Ship ship : ships) {
			if (ship.getFlightQueue() != 0) {
				continue;
			}

			int queueLength = 0;
			Ship currentShip = ship;

			for (int limitCounter = 0; limitCounter < configProperties.getIterationLimit(); limitCounter++) {
				queueLength++;
				currentShip.setFlightFlag(queueLength);
				currentShip = currentShip.getDestinationShip();

				if (currentShip != null) {
					if (currentShip.getFlightFlag() != 0) {
						currentShip = handleCircle(ship, currentShip);
						break;
					} else {
						int flightQueue = currentShip.getFlightQueue();

						if (flightQueue != 0) {
							if (flightQueue < 0) {
								currentShip = handleQueueThatEndsInCircle(ship, queueLength, flightQueue);
							} else {
								handleFinishedQueue(ship, flightQueue + queueLength);
							}

							break;
						}
					}
				} else {
					handleFinishedQueue(ship, queueLength);
					break;
				}
			}
		}
	}

	private Ship handleCircle(Ship ship, Ship currentShip) {
		int temp = -currentShip.getFlightFlag() - 1;
		currentShip = ship;

		for (int limitCounter = 0; limitCounter < configProperties.getIterationLimit(); limitCounter++) {
			currentShip.setFlightQueue(Math.min(++temp, -1));
			currentShip.setFlightFlag(0);
			currentShip = currentShip.getDestinationShip();

			if (currentShip == null || currentShip.getFlightFlag() == 0) {
				break;
			}
		}

		return currentShip;
	}

	private Ship handleQueueThatEndsInCircle(Ship ship, int queueLength, int flightQueue) {
		int temp = flightQueue - queueLength;

		ship.setFlightQueue(temp);
		ship.setFlightFlag(0);
		ship = ship.getDestinationShip();

		return ship;
	}

	private void handleFinishedQueue(Ship ship, int startQueuePosition) {
		for (int i = 0; i < startQueuePosition; i++) {
			ship.setFlightQueue(startQueuePosition - i);
			ship.setFlightFlag(0);
			ship = ship.getDestinationShip();
		}
	}

	private void breakCircles(Collection<Ship> ships) {
		for (int limitCounter = 0; limitCounter < configProperties.getIterationLimit(); limitCounter++) {
			List<Ship> queueShips = ships.stream().filter(s -> s.getFlightQueue() == -1).toList();

			if (queueShips.isEmpty()) {
				return;
			}

			Ship currentShip = queueShips.get(0);
			int queueLength = 0;
			int centerX = 0;
			int centerY = 0;

			for (int limitCounter2 = 0; limitCounter2 < configProperties.getIterationLimit(); limitCounter2++) {
				currentShip.setFlightFlag(1);

				currentShip = currentShip.getDestinationShip();
				centerX += currentShip.getX();
				centerY += currentShip.getY();
				queueLength++;

				if (currentShip.getFlightFlag() != 0) {
					break;
				}
			}

			if (queueLength == 0) {
				return;
			}

			centerX = Math.round((float) centerX / queueLength);
			centerY = Math.round((float) centerY / queueLength);

			Ship slowestShip = findSlowestShip(currentShip, queueLength);

			slowestShip.setDestinationX(centerX);
			slowestShip.setDestinationY(centerY);
			slowestShip.setWasInCircle(true);

			for (int i = queueLength; i > 0; i--) {
				slowestShip = slowestShip.getDestinationShip();
				slowestShip.setFlightQueue(i);
			}
		}
	}

	private Ship findSlowestShip(Ship currentShip, int queueLength) {
		float minTravelSpeed = 9f;
		Ship slowestShip = currentShip;

		for (int i = 0; i < queueLength; i++) {
			currentShip = currentShip.getDestinationShip();
			int travelSpeed = currentShip.getTravelSpeed();

			if (travelSpeed <= minTravelSpeed) {
				slowestShip = currentShip;
				minTravelSpeed = travelSpeed;
			}
		}

		return slowestShip;
	}

	private void breakBranches(Collection<Ship> ships) {
		for (final Ship ship : ships) {
			if (ship.getFlightQueue() >= -1) {
				continue;
			}

			Ship currentShip = ship;

			for (int limitCounter = 0; limitCounter < configProperties.getIterationLimit(); limitCounter++) {
				currentShip = currentShip.getDestinationShip();

				if (currentShip.getFlightQueue() >= 1) {
					break;
				}
			}

			int value = currentShip.getFlightQueue() - ship.getFlightQueue();

			currentShip = ship;

			for (int limitCounter = 0; limitCounter < configProperties.getIterationLimit(); limitCounter++) {
				currentShip.setFlightQueue(--value);
				currentShip = currentShip.getDestinationShip();

				if (currentShip.getFlightQueue() >= 1) {
					break;
				}
			}
		}
	}
}
