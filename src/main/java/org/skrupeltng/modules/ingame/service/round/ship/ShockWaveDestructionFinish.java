package org.skrupeltng.modules.ingame.service.round.ship;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFoldRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipRemoval;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class ShockWaveDestructionFinish {

	private final ShipRepository shipRepository;
	private final SpaceFoldRepository spaceFoldRepository;
	private final PlayerRepository playerRepository;
	private final ShipRemoval shipRemoval;
	private final ConfigProperties configProperties;

	public ShockWaveDestructionFinish(ShipRepository shipRepository, SpaceFoldRepository spaceFoldRepository, PlayerRepository playerRepository, ShipRemoval shipRemoval, ConfigProperties configProperties) {
		this.shipRepository = shipRepository;
		this.spaceFoldRepository = spaceFoldRepository;
		this.playerRepository = playerRepository;
		this.shipRemoval = shipRemoval;
		this.configProperties = configProperties;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void destroySpaceFoldsBySubSpaceDistortion(long gameId, Ship ship, int level) {
		int range = configProperties.getSubSpaceDistortionRange();
		List<Long> spaceFoldIds = spaceFoldRepository.getSpaceFoldIdsInRadius(gameId, ship.getX(), ship.getY(), range);

		for (Long spaceFoldId : spaceFoldIds) {
			int chance = 1 + MasterDataService.RANDOM.nextInt(9);

			if (chance <= level) {
				spaceFoldRepository.deleteById(spaceFoldId);
			}
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteDestroyedShip(long shipId, long shockWavePlayerId) {
		Ship ship = shipRepository.getReferenceById(shipId);
		Player shockWavePlayer = playerRepository.getReferenceById(shockWavePlayerId);

		shipRemoval.deleteShip(ship, shockWavePlayer);
	}
}
