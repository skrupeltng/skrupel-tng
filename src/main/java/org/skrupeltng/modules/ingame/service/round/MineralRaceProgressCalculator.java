package org.skrupeltng.modules.ingame.service.round;

import org.skrupeltng.modules.ImageConstants;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.MineralRacePlayerProgressNotification;
import org.skrupeltng.modules.ingame.database.player.MineralRacePlayerProgressNotificationRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.service.round.wincondition.MineralRaceProgress;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

@Component
public class MineralRaceProgressCalculator {

	private static final List<Float> PROGRESSIONS = List.of(0.95f, 0.9f, 0.75f, 0.5f);

	private final GameRepository gameRepository;
	private final MineralRacePlayerProgressNotificationRepository mineralRacePlayerRepository;
	private final MessageSource messageSource;
	private final NewsService newsService;

	public MineralRaceProgressCalculator(GameRepository gameRepository,
										 MineralRacePlayerProgressNotificationRepository mineralRacePlayerRepository,
										 MessageSource messageSource,
										 NewsService newsService) {
		this.gameRepository = gameRepository;
		this.mineralRacePlayerRepository = mineralRacePlayerRepository;
		this.messageSource = messageSource;
		this.newsService = newsService;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void checkMineralRaceProgress(long gameId) {
		Game game = gameRepository.getReferenceById(gameId);

		if (game.getWinCondition() != WinCondition.MINERAL_RACE) {
			return;
		}

		String mineralName = game.getMineralRaceMineralName().name().toLowerCase();

		final Integer mineralRaceQuantity = game.getMineralRaceQuantity();
		List<Player> allPlayers = game.getPlayers();
		final Set<Player> remainingPlayers = new HashSet<>(allPlayers);

		for (Float progress : PROGRESSIONS) {
			if (remainingPlayers.isEmpty()) {
				break;
			}

			int targetQuantity = (int) Math.ceil(mineralRaceQuantity * progress);
			MineralRaceProgress mineralRaceProgress = new MineralRaceProgress(game, targetQuantity, remainingPlayers);
			List<Player> playersWithProgress = mineralRaceProgress.calculatePlayersWithTargetProgress();

			for (Player player : playersWithProgress) {
				remainingPlayers.remove(player);
				Optional<MineralRacePlayerProgressNotification> opt = mineralRacePlayerRepository.findByPlayerIdAndProgress(player.getId(), progress);

				if (opt.isEmpty()) {
					MineralRacePlayerProgressNotification notification = new MineralRacePlayerProgressNotification();
					notification.setPlayer(player);
					notification.setLastNotifiedProgress(progress);
					mineralRacePlayerRepository.save(notification);

					for (Player p : allPlayers) {
						Locale locale = Locale.forLanguageTag(p.getLogin().getLanguage());
						String playerName = player.retrieveDisplayNameWithColor(messageSource, locale);
						String mineralNameI18N = messageSource.getMessage(mineralName, null, locale);

						int progressDisplay = (int) Math.ceil(100f * progress);
						newsService.add(p,
							NewsEntryConstants.news_entry_mineral_race_progress_notification,
							ImageConstants.NEWS_MESSAGE,
							null,
							null,
							playerName,
							progressDisplay,
							mineralNameI18N);
					}
				}
			}
		}
	}
}
