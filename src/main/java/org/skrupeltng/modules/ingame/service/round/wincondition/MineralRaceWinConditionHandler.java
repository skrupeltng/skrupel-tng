package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("MINERAL_RACE")
public class MineralRaceWinConditionHandler implements WinConditionHandler {

	@Override
	public WinnersCalculationResult calculate(Game game) {
		int targetQuantity = game.getMineralRaceQuantity();
		List<Player> players = game.getPlayers();

		MineralRaceProgress progress = new MineralRaceProgress(game, targetQuantity, players);
		List<Player> winners = progress.calculatePlayersWithTargetProgress();
		return new WinnersCalculationResult(winners);
	}
}
