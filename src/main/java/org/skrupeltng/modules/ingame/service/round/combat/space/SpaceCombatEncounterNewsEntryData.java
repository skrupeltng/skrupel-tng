package org.skrupeltng.modules.ingame.service.round.combat.space;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.database.spacecombat.SpaceCombatLogEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.spacecombat.SpaceCombatLogEntryItem;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

public class SpaceCombatEncounterNewsEntryData {

	private Player player;
	private int x;
	private int y;

	private Set<Long> totalOwnShips = new HashSet<>();
	private Set<Long> lostOwnShips = new HashSet<>();
	private Set<Long> totalEnemyShips = new HashSet<>();
	private Set<Long> lostEnemyShips = new HashSet<>();

	private String image;

	private List<SpaceCombatLogEntry> logEntries = new ArrayList<>();

	private String newsEntryTemplate;
	private Object[] arguments;

	public SpaceCombatEncounterNewsEntryData(Player player, int x, int y) {
		this.player = player;
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void addShipPair(Ship ownShip, Ship enemyShip, boolean ownDestroyed, boolean enemyDestroyed, boolean ownCaptured, boolean enemyCaptured) {
		totalOwnShips.add(ownShip.getId());
		totalEnemyShips.add(enemyShip.getId());

		if (ownDestroyed || ownCaptured) {
			lostOwnShips.add(ownShip.getId());
		}

		if (enemyDestroyed || enemyCaptured) {
			lostEnemyShips.add(enemyShip.getId());
		}

		if (image == null) {
			image = ownShip.createFullImagePath();
		}

		SpaceCombatLogEntry logEntry = new SpaceCombatLogEntry();
		logEntry.setOwnItem(createLogItem(ownShip, false, ownDestroyed, ownCaptured));
		logEntry.setEnemyItem(createLogItem(enemyShip, true, enemyDestroyed, enemyCaptured));
		logEntries.add(logEntry);
	}

	private SpaceCombatLogEntryItem createLogItem(Ship ship, boolean enemy, boolean destroyed, boolean captured) {
		SpaceCombatLogEntryItem item = new SpaceCombatLogEntryItem();
		item.setEnemy(enemy);
		item.setDestroyed(destroyed);
		item.setCaptured(captured);
		item.setShipId(ship.getId());
		item.setShipName(ship.getName());
		item.setShipTemplate(ship.getShipTemplate());
		item.setPropulsionSystemTemplate(ship.getPropulsionSystemTemplate());
		item.setEnergyWeaponTemplate(ship.getEnergyWeaponTemplate());
		item.setProjectileWeaponTemplate(ship.getProjectileWeaponTemplate());
		return item;
	}

	public void finishData() {
		if (totalOwnShips.size() == 1 && totalEnemyShips.size() == 1) {
			String shipName = logEntries.get(0).getOwnItem().getShipName();

			if (lostOwnShips.size() == 0) {
				newsEntryTemplate = NewsEntryConstants.news_entry_space_combat_summary_victory_single;
				arguments = new Object[] { shipName, x, y };
			} else if (lostEnemyShips.size() == 0) {
				newsEntryTemplate = NewsEntryConstants.news_entry_space_combat_summary_defeat_single;
				arguments = new Object[] { shipName, x, y };
			} else {
				newsEntryTemplate = NewsEntryConstants.news_entry_space_combat_summary_mutual_destroyed;
				arguments = new Object[] { shipName, x, y };
			}
		} else if (totalOwnShips.size() == lostOwnShips.size()) {
			if (lostEnemyShips.size() == 0) {
				if (totalEnemyShips.size() == 1) {
					newsEntryTemplate = NewsEntryConstants.news_entry_space_combat_summary_total_defeat_single_enemy;
					arguments = new Object[] { totalOwnShips.size(), x, y };
				} else {
					newsEntryTemplate = NewsEntryConstants.news_entry_space_combat_summary_total_defeat;
					arguments = new Object[] { totalOwnShips.size(), x, y, totalEnemyShips.size() };
				}
			} else if (totalEnemyShips.size() == 1) {
				newsEntryTemplate = NewsEntryConstants.news_entry_space_combat_summary_defeat_single_enemy;
				arguments = new Object[] { totalOwnShips.size(), x, y };
			} else {
				newsEntryTemplate = NewsEntryConstants.news_entry_space_combat_summary_defeat;
				arguments = new Object[] { totalOwnShips.size(), x, y, totalEnemyShips.size(), lostEnemyShips.size() };
			}
		} else if (lostOwnShips.size() == 0) {
			if (totalEnemyShips.size() == 1) {
				newsEntryTemplate = NewsEntryConstants.news_entry_space_combat_summary_total_victory_single_enemy;
				arguments = new Object[] { totalOwnShips.size(), x, y };
			} else {
				newsEntryTemplate = NewsEntryConstants.news_entry_space_combat_summary_total_victory;
				arguments = new Object[] { totalOwnShips.size(), x, y, totalEnemyShips.size() };
			}
		} else if (totalEnemyShips.size() == 1) {
			newsEntryTemplate = NewsEntryConstants.news_entry_space_combat_summary_victory_single_enemy;
			arguments = new Object[] { totalOwnShips.size(), x, y, totalOwnShips.size() - lostOwnShips.size() };
		} else {
			newsEntryTemplate = NewsEntryConstants.news_entry_space_combat_summary_victory;
			arguments = new Object[] { totalOwnShips.size(), x, y, totalEnemyShips.size(), totalOwnShips.size() - lostOwnShips.size() };
		}
	}

	public Player getPlayer() {
		return player;
	}

	public String getNewsEntryTemplate() {
		return newsEntryTemplate;
	}

	public String getImage() {
		return image;
	}

	public Object[] getArguments() {
		return arguments;
	}

	public List<SpaceCombatLogEntry> getLogEntries() {
		return logEntries;
	}
}