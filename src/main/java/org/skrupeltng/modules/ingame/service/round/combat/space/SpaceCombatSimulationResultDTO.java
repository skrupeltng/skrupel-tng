package org.skrupeltng.modules.ingame.service.round.combat.space;

import java.io.Serializable;

public class SpaceCombatSimulationResultDTO implements Serializable {

	private static final long serialVersionUID = -63628292222813029L;

	private float survivalPercentage;
	private float enemySurvivalPercentage;
	private float capturePercentage;
	private float enemyCapturePercentage;

	private String survivalIcon;
	private String enemySurvivalIcon;
	private String captureIcon;
	private String enemyCaptureIcon;

	public float getSurvivalPercentage() {
		return survivalPercentage;
	}

	public void setSurvivalPercentage(float survivalPercentage) {
		this.survivalPercentage = survivalPercentage;
	}

	public float getEnemySurvivalPercentage() {
		return enemySurvivalPercentage;
	}

	public void setEnemySurvivalPercentage(float enemySurvivalPercentage) {
		this.enemySurvivalPercentage = enemySurvivalPercentage;
	}

	public float getCapturePercentage() {
		return capturePercentage;
	}

	public void setCapturePercentage(float capturePercentage) {
		this.capturePercentage = capturePercentage;
	}

	public float getEnemyCapturePercentage() {
		return enemyCapturePercentage;
	}

	public void setEnemyCapturePercentage(float enemyCapturePercentage) {
		this.enemyCapturePercentage = enemyCapturePercentage;
	}

	public String getSurvivalIcon() {
		return survivalIcon;
	}

	public void setSurvivalIcon(String survivalIcon) {
		this.survivalIcon = survivalIcon;
	}

	public String getEnemySurvivalIcon() {
		return enemySurvivalIcon;
	}

	public void setEnemySurvivalIcon(String enemySurvivalIcon) {
		this.enemySurvivalIcon = enemySurvivalIcon;
	}

	public String getCaptureIcon() {
		return captureIcon;
	}

	public void setCaptureIcon(String captureIcon) {
		this.captureIcon = captureIcon;
	}

	public String getEnemyCaptureIcon() {
		return enemyCaptureIcon;
	}

	public void setEnemyCaptureIcon(String enemyCaptureIcon) {
		this.enemyCaptureIcon = enemyCaptureIcon;
	}
}