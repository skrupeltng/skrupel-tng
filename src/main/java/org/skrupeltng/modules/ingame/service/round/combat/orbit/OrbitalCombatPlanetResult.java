package org.skrupeltng.modules.ingame.service.round.combat.orbit;

import java.util.ArrayList;
import java.util.List;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.database.OrbitalCombatLogEntry;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

public class OrbitalCombatPlanetResult {

	private final Planet planet;
	private final List<OrbitalCombatLogEntry> planetPlayerLogEntries = new ArrayList<>();
	private final List<OrbitalCombatLogEntry> shipPlayerLogEntries = new ArrayList<>();
	private final Player shipPlayer;

	public OrbitalCombatPlanetResult(Planet planet, Player shipPlayer) {
		this.planet = planet;
		this.shipPlayer = shipPlayer;
	}

	public Planet getPlanet() {
		return planet;
	}

	public Player getShipPlayer() {
		return shipPlayer;
	}

	public List<OrbitalCombatLogEntry> getPlanetPlayerLogEntries() {
		return planetPlayerLogEntries;
	}

	public List<OrbitalCombatLogEntry> getShipPlayerLogEntries() {
		return shipPlayerLogEntries;
	}

	public void addShip(Ship ship, int initialDefensePercentage, int remainingDefensePercentage) {
		planetPlayerLogEntries.add(createEntry(ship, initialDefensePercentage, remainingDefensePercentage, true));
		shipPlayerLogEntries.add(createEntry(ship, initialDefensePercentage, remainingDefensePercentage, false));
	}

	private OrbitalCombatLogEntry createEntry(Ship ship, int initialDefensePercentage, int remainingDefensePercentage, boolean forPlanetPlayer) {
		OrbitalCombatLogEntry logEntry = new OrbitalCombatLogEntry();
		logEntry.setPlanet(planet);
		logEntry.setForPlanetPlayer(forPlanetPlayer);
		logEntry.setDestroyed(ship.destroyed());
		logEntry.setInitialOrbitalDefensePercentage(initialDefensePercentage);
		logEntry.setRemainingOrbitalDefensePercentage(remainingDefensePercentage);
		logEntry.setShipName(ship.getName());
		logEntry.setShipId(ship.getId());
		logEntry.setShipTemplate(ship.getShipTemplate());
		logEntry.setPropulsionSystemTemplate(ship.getPropulsionSystemTemplate());
		logEntry.setEnergyWeaponTemplate(ship.getEnergyWeaponTemplate());
		logEntry.setProjectileWeaponTemplate(ship.getProjectileWeaponTemplate());
		return logEntry;
	}
}