package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.skrupeltng.modules.ingame.database.Game;
import org.springframework.stereotype.Component;

@Component("DEATH_FOE")
public class DeathFoeWinConditionHandler implements WinConditionHandler {

	@Override
	public WinnersCalculationResult calculate(Game game) {
		return new WinnersCalculationResult(game.getPlayers().stream()
			.filter(p -> p.getDeathFoes().stream().allMatch(d -> d.getDeathFoe().isHasLost()))
			.toList());
	}
}
