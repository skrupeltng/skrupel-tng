package org.skrupeltng.modules.ingame.service.round;

import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;

import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntry;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteResourceAction;
import org.skrupeltng.modules.masterdata.database.Resource;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class RouteRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private ShipRouteEntryRepository shipRouteEntryRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processRouteTravel(long gameId) {
		log.debug("Processing ship routes...");

		List<Ship> ships = shipRepository.getShipsWithRoutes(gameId);

		for (Ship ship : ships) {
			int totalGoods = ship.retrieveTotalGoods();
			int storageSpace = ship.retrieveStorageSpace();

			log.trace("Processing route travel for Ship " + ship + "...");

			Planet planet = ship.getPlanet();

			if (canRefuel(ship, planet)) {
				int additionalFuel = Math.min(ship.getRouteMinFuel() - ship.getFuel(), planet.getFuel());
				ship.setFuel(ship.getFuel() + additionalFuel);
				planet.spendFuel(additionalFuel);
				planetRepository.save(planet);
				ship = shipRepository.save(ship);

				log.trace("Adding fuel: " + additionalFuel);
			}

			if (nextRouteEntryCanBeSet(ship, totalGoods, storageSpace)) {
				List<ShipRouteEntry> route = shipRouteEntryRepository.findByShipId(ship.getId());

				int nextIndex = route.indexOf(ship.getCurrentRouteEntry()) + 1;

				if (nextIndex == route.size()) {
					nextIndex = 0;
				}

				ShipRouteEntry next = route.get(nextIndex);
				ship.updateRoute(next);

				log.trace("Next route entry: " + next);

				ship = shipRepository.save(ship);
			}

			log.trace("Processing route travel for Ship " + ship + " done.");
		}

		log.debug("Processing ship routes finished.");
	}

	protected boolean canRefuel(Ship ship, Planet planet) {
		return planet != null && ship.getRouteMinFuel() > ship.getFuel();
	}

	protected boolean nextRouteEntryCanBeSet(Ship ship, int totalGoods, int storageSpace) {
		ShipRouteEntry current = ship.getCurrentRouteEntry();
		return current.getPlanet() == ship.getPlanet() && (!current.isWaitForFullStorage() || totalGoods == storageSpace);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processPlanetExchange(long gameId) {
		log.debug("Processing ship route planet exchange...");

		List<Ship> ships = shipRepository.getShipsWithPlanetOnRoute(gameId);

		for (Ship ship : ships) {
			log.trace("Processing route exchange for Ship " + ship + "...");

			Planet planet = ship.getPlanet();
			Optional<ShipRouteEntry> entryOpt = ship.getRoute().stream().filter(r -> r.getPlanet() == planet).findFirst();

			if (entryOpt.isPresent()) {
				log.trace("Ship is on route planet: " + planet);

				ShipRouteEntry entry = entryOpt.get();
				loadPrimaryResource(entry);

				manageResource(Resource.FUEL, ship, entry.getFuelAction());
				manageResource(Resource.MONEY, ship, entry.getMoneyAction());
				manageResource(Resource.MINERAL1, ship, entry.getMineral1Action());
				manageResource(Resource.MINERAL2, ship, entry.getMineral2Action());
				manageResource(Resource.MINERAL3, ship, entry.getMineral3Action());
				manageResource(Resource.SUPPLIES, ship, entry.getSupplyAction());
			}

			if (ship.getRouteMinFuel() > ship.getFuel()) {
				int additionalFuel = Math.min(ship.getRouteMinFuel() - ship.getFuel(), planet.getFuel());
				ship.setFuel(ship.getFuel() + additionalFuel);
				planet.spendFuel(additionalFuel);

				log.trace("Adding fuel: " + additionalFuel);
			}

			shipRepository.save(ship);
			planetRepository.save(ship.getPlanet());

			log.trace("Processing route exchange for Ship " + ship + " done.");
		}

		log.debug("Processing ship route planet exchange finished.");
	}

	protected void loadPrimaryResource(ShipRouteEntry entry) {
		Ship ship = entry.getShip();
		Planet planet = ship.getPlanet();
		Resource primaryResource = ship.getRoutePrimaryResource();
		int totalGoods = ship.retrieveTotalGoods();
		int storageSpace = ship.retrieveStorageSpace();

		log.trace("Loading primary resource: " + primaryResource);
		log.trace("totalGoods: " + totalGoods);
		log.trace("storageSpace: " + storageSpace);

		switch (primaryResource) {
			case MINERAL1:
				if (entry.getMineral1Action() != ShipRouteResourceAction.TAKE) {
					return;
				}
				load(Ship::setMineral1, ship, storageSpace, totalGoods, ship.getMineral1(), planet.getMineral1(), planet, Planet::setMineral1);
				break;
			case MINERAL2:
				if (entry.getMineral2Action() != ShipRouteResourceAction.TAKE) {
					return;
				}
				load(Ship::setMineral2, ship, storageSpace, totalGoods, ship.getMineral2(), planet.getMineral2(), planet, Planet::setMineral2);
				break;
			case MINERAL3:
				if (entry.getMineral3Action() != ShipRouteResourceAction.TAKE) {
					return;
				}
				load(Ship::setMineral3, ship, storageSpace, totalGoods, ship.getMineral3(), planet.getMineral3(), planet, Planet::setMineral3);
				break;
			case SUPPLIES:
				if (entry.getSupplyAction() != ShipRouteResourceAction.TAKE) {
					return;
				}
				load(Ship::setSupplies, ship, storageSpace, totalGoods, ship.getSupplies(), planet.getSupplies(), planet, Planet::setSupplies);
				break;
			default:
				break;
		}
	}

	protected void manageResource(Resource resource, Ship ship, ShipRouteResourceAction action) {
		Planet planet = ship.getPlanet();

		if (action == ShipRouteResourceAction.LEAVE) {
			log.trace("Leaving: " + resource);

			switch (resource) {
				case FUEL:
					planet.setFuel(planet.getFuel() + ship.getFuel());
					ship.setFuel(0);
					break;
				case MINERAL1:
					planet.setMineral1(planet.getMineral1() + ship.getMineral1());
					ship.setMineral1(0);
					break;
				case MINERAL2:
					planet.setMineral2(planet.getMineral2() + ship.getMineral2());
					ship.setMineral2(0);
					break;
				case MINERAL3:
					planet.setMineral3(planet.getMineral3() + ship.getMineral3());
					ship.setMineral3(0);
					break;
				case SUPPLIES:
					planet.setSupplies(planet.getSupplies() + ship.getSupplies());
					ship.setSupplies(0);
					break;
				case MONEY:
					planet.setMoney(planet.getMoney() + ship.getMoney());
					ship.setMoney(0);
					break;
			}
		} else if (action == ShipRouteResourceAction.TAKE) {
			int totalGoods = ship.retrieveTotalGoods();
			ShipTemplate shipTemplate = ship.getShipTemplate();
			int fuelCapacity = shipTemplate.getFuelCapacity();
			int storageSpace = ship.retrieveStorageSpace();

			log.trace("taking: " + resource);
			log.trace("totalGoods: " + totalGoods);
			log.trace("fuelCapacity: " + fuelCapacity);
			log.trace("storageSpace: " + storageSpace);

			switch (resource) {
				case FUEL:
					load(Ship::setFuel, ship, fuelCapacity, ship.getFuel(), ship.getFuel(), planet.getFuel(), planet, Planet::setFuel);
					break;
				case MINERAL1:
					load(Ship::setMineral1, ship, storageSpace, totalGoods, ship.getMineral1(), planet.getMineral1(), planet, Planet::setMineral1);
					break;
				case MINERAL2:
					load(Ship::setMineral2, ship, storageSpace, totalGoods, ship.getMineral2(), planet.getMineral2(), planet, Planet::setMineral2);
					break;
				case MINERAL3:
					load(Ship::setMineral3, ship, storageSpace, totalGoods, ship.getMineral3(), planet.getMineral3(), planet, Planet::setMineral3);
					break;
				case SUPPLIES:
					load(Ship::setSupplies, ship, storageSpace, totalGoods, ship.getSupplies(), planet.getSupplies(), planet, Planet::setSupplies);
					break;
				case MONEY:
					load(Ship::setMoney, ship, Integer.MAX_VALUE, ship.getMoney(), ship.getMoney(), planet.getMoney(), planet, Planet::setMoney);
					break;
			}
		}
	}

	protected void load(BiConsumer<Ship, Integer> shipSetter, Ship ship, int max, int shipTotal, int shipRes, int planetRes, Planet planet,
			BiConsumer<Planet, Integer> planetSetter) {
		log.trace("Loading: " + max);
		log.trace("shipTotal: " + shipTotal);
		log.trace("shipRes: " + shipRes);
		log.trace("planetRes: " + planetRes);

		if (shipTotal < max) {
			if (shipTotal + planetRes > max) {
				int diff = max - shipTotal;
				planetRes -= diff;
				shipRes += diff;
			} else {
				shipRes += planetRes;
				planetRes = 0;
			}

			log.trace("new shipRes: " + shipRes);
			log.trace("new planetRes: " + planetRes);
			shipSetter.accept(ship, shipRes);
			planetSetter.accept(planet, planetRes);
		}
	}
}
