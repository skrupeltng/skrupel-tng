package org.skrupeltng.modules.ingame.service.round.combat.space;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

import org.skrupeltng.modules.ingame.service.round.DestroyedShipsCounter;

public class SpaceCombatData {

	private final Set<Set<Long>> processedShipPairs = new HashSet<>();
	private final Map<Coordinate, List<SpaceCombatEncounterResultData>> encounters = new HashMap<>();
	private final Set<Long> destroyedShipIds = new HashSet<>();
	private final Set<Long> capturedShipIds = new HashSet<>();

	private final DestroyedShipsCounter destroyedShipsCounter;

	public SpaceCombatData(DestroyedShipsCounter destroyedShipsCounter) {
		this.destroyedShipsCounter = destroyedShipsCounter;
	}

	public Optional<SpaceCombatEncounterResultData> addShipPair(Ship ship1, Ship ship2) {
		if (processedShipPairs.add(Set.of(ship1.getId(), ship2.getId()))) {
			SpaceCombatEncounterResultData encounter = new SpaceCombatEncounterResultData(ship1, ship2);

			Coordinate coordinate = new CoordinateImpl(ship1.getX(), ship1.getY(), 0);
			List<SpaceCombatEncounterResultData> list = encounters.computeIfAbsent(coordinate, k -> new ArrayList<>());

			list.add(encounter);

			return Optional.of(encounter);
		}

		return Optional.empty();
	}

	public void addToDestroyedShips(Ship ship) {
		destroyedShipIds.add(ship.getId());
		destroyedShipsCounter.addDestroyedShip(ship.getPlayer().getId());
	}

	public void addToCapturedShips(Ship ship) {
		capturedShipIds.add(ship.getId());
	}

	public boolean shipIsDestroyed(Ship ship) {
		return destroyedShipIds.contains(ship.getId());
	}

	public Set<Long> getDestroyedShipIds() {
		return destroyedShipIds;
	}

	public Set<Long> getCapturedShipIds() {
		return capturedShipIds;
	}

	public Map<Coordinate, List<SpaceCombatEncounterResultData>> getEncounters() {
		return encounters;
	}
}
