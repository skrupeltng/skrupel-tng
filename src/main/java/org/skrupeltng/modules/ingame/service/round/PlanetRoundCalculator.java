package org.skrupeltng.modules.ingame.service.round;

import org.jetbrains.annotations.NotNull;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.FogOfWarType;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.controlgroup.database.ControlGroupRepository;
import org.skrupeltng.modules.ingame.modules.controlgroup.database.ControlGroupType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.ArtifactType;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpecies;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpeciesEffect;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpeciesType;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetType;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetTypeRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLog;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.skrupeltng.modules.ingame.modules.planet.service.PlanetService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.PlayerStarbaseScanLog;
import org.skrupeltng.modules.ingame.modules.starbase.database.PlayerStarbaseScanLogRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbasePropulsionStock;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbasePropulsionStockRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplateRepository;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class PlanetRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlayerPlanetScanLogRepository playerPlanetScanLogRepository;

	@Autowired
	private PlayerStarbaseScanLogRepository playerStarbaseScanLogRepository;

	@Autowired
	private OrbitalSystemRepository orbitalSystemRepository;

	@Autowired
	private NewsService newsService;

	@Autowired
	private PlanetService planetService;

	@Autowired
	private PropulsionSystemTemplateRepository propulsionSystemTemplateRepository;

	@Autowired
	private StarbasePropulsionStockRepository starbasePropulsionStockRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private ShipRouteEntryRepository shipRouteEntryRepository;

	@Autowired
	private PlanetTypeRepository planetTypeRepository;

	@Autowired
	private ControlGroupRepository controlGroupRepository;

	@Autowired
	private StatsUpdater statsUpdater;

	@Autowired
	private VisibleObjects visibleObjects;

	@Autowired
	private ConfigProperties configProperties;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processNewColonies(long gameId) {
		log.debug("Processing new colonies...");

		List<Planet> newColonies = planetRepository.findNewColonies(gameId);

		for (Planet planet : newColonies) {
			planet.setPlayer(planet.getNewPlayer());

			planet.setNewPlayer(null);
			planet.setColonists(planet.getNewColonists());
			planet.setNewColonists(0);

			planet.setLightGroundUnits(planet.getNewLightGroundUnits());
			planet.setNewLightGroundUnits(0);

			planet.setHeavyGroundUnits(planet.getNewHeavyGroundUnits());
			planet.setNewHeavyGroundUnits(0);

			planet = planetRepository.save(planet);

			playerRepository.increaseNewColonies(planet.getPlayer().getId());

			newsService.add(planet.getPlayer(), NewsEntryConstants.news_entry_new_colony, planet.createFullImagePath(), planet.getId(),
				NewsEntryClickTargetType.planet, planet.getName());

			statsUpdater.incrementStats(planet.getPlayer(), LoginStatsFaction::getPlanetsColonized, LoginStatsFaction::setPlanetsColonized,
				AchievementType.PLANETS_COLONIZED_1, AchievementType.PLANETS_COLONIZED_20, AchievementType.PLANETS_COLONIZED_100);
		}

		log.debug("New colonies processed.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processOwnedPlanets(long gameId, Map<Long, Float> tradeBonusData) {
		log.debug("Processing owned planets...");

		List<Planet> ownedPlanets = planetRepository.findOwnedPlanetsByGameId(gameId);

		for (Planet planet : ownedPlanets) {
			checkArtifact(planet);
			useWeatherControlStation(planet);
			checkCloakingFieldGenerator(planet);
			produceGroundUnits(planet);
			checkNativeSpeciesAttack(planet);
			checkNativeSpeciesPopulation(planet);

			if (planet.getColonists() < 1000) {
				reduceColonistsAndGroundUnits(planet);
			} else if (planet.getColonists() < 10000000) {
				increaseColonistsAndMoney(planet);
			}

			if (planet.getPlayer() != null) {
				int totalIncome = planet.retrieveAdditionalMoney(tradeBonusData);
				planet.spendMoney(-totalIncome);

				processFactories(planet);
				checkProducingNativeSpecies(planet);
				processMines(planet);

				if (planet.isAutoBuildFactories()) {
					autoBuildFactories(planet);
				}

				if (planet.isAutoBuildMines()) {
					autoBuildMines(planet);
				}

				if (planet.isAutoBuildPlanetaryDefense()) {
					autoBuildPlanetaryDefense(planet);
				}

				if (planet.isAutoSellSupplies()) {
					autoSellSupplies(planet);
				}

				StarbaseType starbaseType = planet.getStarbaseUnderConstructionType();

				if (starbaseType != null) {
					Starbase starbase = new Starbase();
					starbase.setType(starbaseType);
					starbase.setName(planet.getStarbaseUnderConstructionName());
					starbase = starbaseRepository.save(starbase);

					planet.setStarbase(starbase);
					planet.setStarbaseUnderConstructionType(null);
					planet.setStarbaseUnderConstructionName(null);

					starbase.setPlanet(planet);

					updateStarbaseWithNativeSpecies(planet, starbase);

					playerRepository.increaseNewStarbases(planet.getPlayer().getId());
					newsService.add(planet.getPlayer(), NewsEntryConstants.news_entry_starbase_constructed, planet.createFullImagePath(), starbase.getId(),
						NewsEntryClickTargetType.starbase, starbase.getName());
				}

				checkNativeSpeciesWarning(planet);
			}

			planet.reduceExcessMines();
			planet.destroyExcessFactories();
			planet.destroyExcessDefense();

			planetRepository.save(planet);
		}

		log.debug("Owned planets processed.");
	}

	private void checkArtifact(Planet planet) {
		ArtifactType artifactType = planet.getArtifactType();

		if (artifactType != null) {
			switch (artifactType) {
				case COLONY:
					handleColony(planet);
					break;
				case WEATHER_CONTROL_STATION:
					checkWeatherControlStation(planet);
					break;
				case FUEL:
					addResourceArtifact(Planet::setUntappedFuel, Planet::getUntappedFuel, planet, "Lemin");
					break;
				case MINERAL1:
					addResourceArtifact(Planet::setUntappedMineral1, Planet::getUntappedMineral1, planet, "Baxterium");
					break;
				case MINERAL2:
					addResourceArtifact(Planet::setUntappedMineral2, Planet::getUntappedMineral2, planet, "Rennurbin");
					break;
				case MINERAL3:
					addResourceArtifact(Planet::setUntappedMineral3, Planet::getUntappedMineral3, planet, "Vomisaan");
					break;
			}
		}
	}

	private void handleColony(Planet planet) {
		if (MasterDataService.RANDOM.nextBoolean()) {
			int foundColonists = 2000 + MasterDataService.RANDOM.nextInt(8000);
			planet.setColonists(planet.getColonists() + foundColonists);
			planet.setArtifactType(null);

			newsService.add(planet.getPlayer(), NewsEntryConstants.news_entry_artifact_colony_found, planet.createFullImagePath(), planet.getId(),
				NewsEntryClickTargetType.planet, planet.getName(), foundColonists);
		}
	}

	private void checkWeatherControlStation(Planet planet) {
		if (MasterDataService.RANDOM.nextInt(100) <= 33) {
			List<OrbitalSystem> orbitalSystems = planet.getOrbitalSystems();
			Optional<OrbitalSystem> emptyOrbitalSystemOpt = orbitalSystems.stream().filter(o -> o.getType() == null).findAny();

			if (emptyOrbitalSystemOpt.isPresent()) {
				OrbitalSystem orbitalSystem = emptyOrbitalSystemOpt.get();
				orbitalSystem.setType(OrbitalSystemType.WEATHER_CONTROL_STATION);
				orbitalSystems.remove(orbitalSystem);
				orbitalSystem = orbitalSystemRepository.save(orbitalSystem);
				orbitalSystems.add(orbitalSystem);

				planet.setArtifactType(null);

				newsService.add(planet.getPlayer(), NewsEntryConstants.news_entry_artifact_weather_control_station_found,
					planet.createFullImagePath(), planet.getId(), NewsEntryClickTargetType.planet, planet.getName());
			}
		}
	}

	private void addResourceArtifact(BiConsumer<Planet, Integer> setter, Function<Planet, Integer> getter, Planet planet, String resourceName) {
		if (MasterDataService.RANDOM.nextInt(100) <= 18) {
			planet.setArtifactType(null);
			int resource = 500 + MasterDataService.RANDOM.nextInt(1000) + getter.apply(planet);
			setter.accept(planet, resource);

			newsService.add(planet.getPlayer(), NewsEntryConstants.news_entry_artifact_resource_found, planet.createFullImagePath(), planet.getId(),
				NewsEntryClickTargetType.planet, planet.getName(), resourceName);
		}
	}

	private void useWeatherControlStation(Planet planet) {
		if (planet.hasOrbitalSystem(OrbitalSystemType.WEATHER_CONTROL_STATION)) {
			int preferredTemperature = planet.getPlayer().getFaction().getPreferredTemperature();

			if (planet.getTemperature() != preferredTemperature) {
				if (preferredTemperature == 0) {
					String type = planet.getType();
					PlanetType planetType = planetTypeRepository.getReferenceById(type);
					int diff = planetType.getMaxTemperature() - planetType.getMinTemperature();
					int temperature = planetType.getMinTemperature() + MasterDataService.RANDOM.nextInt(diff);
					planet.setTemperature(temperature);
				} else {
					planet.setTemperature(preferredTemperature);
				}
			}
		}
	}

	private void checkCloakingFieldGenerator(Planet planet) {
		if (planet.hasOrbitalSystem(OrbitalSystemType.CLOAKING_FIELD_GENERATOR)) {
			List<Ship> ships = planet.getShips();

			for (Ship ship : ships) {
				ship.setCloaked(true);
			}

			shipRepository.saveAll(ships);
		}
	}

	private void produceGroundUnits(Planet planet) {
		if (planet.getLightGroundUnitsProduction() > 0) {
			planet.setLightGroundUnits(planet.getLightGroundUnits() + planet.getLightGroundUnitsProduction());
			planet.setLightGroundUnitsProduction(0);
		}

		if (planet.getHeavyGroundUnitsProduction() > 0) {
			planet.setHeavyGroundUnits(planet.getHeavyGroundUnits() + planet.getHeavyGroundUnitsProduction());
			planet.setHeavyGroundUnitsProduction(0);
		}
	}

	private void checkNativeSpeciesPopulation(Planet planet) {
		NativeSpecies nativeSpecies = planet.getNativeSpecies();
		int nativeSpeciesCount = planet.getNativeSpeciesCount();

		if (nativeSpecies != null && nativeSpeciesCount > 0) {
			int reservationSize = configProperties.getReservationSize();

			boolean hasReservation = planet.hasOrbitalSystem(OrbitalSystemType.RESERVATION);
			int diff = Math.round(nativeSpeciesCount * 0.01745f);

			if ((!hasReservation || nativeSpeciesCount > reservationSize) && MasterDataService.RANDOM.nextBoolean()) {
				diff *= -1;
			}

			planet.setNativeSpeciesCount(nativeSpeciesCount + diff);
			nativeSpeciesCount = planet.getNativeSpeciesCount();

			if (nativeSpeciesCount > 0) {
				Faction faction = planet.getPlayer().getFaction();
				NativeSpeciesType type = nativeSpecies.getType();
				float assimilationRate = faction.getAssimilationRate();
				NativeSpeciesType assimilationType = faction.getAssimilationType();

				if ((assimilationType == null || assimilationType == type) && assimilationRate > 0f) {
					int assimilation = Math.round(planet.getColonists() / 100f * assimilationRate);

					if (assimilation > 0 && (!hasReservation || nativeSpeciesCount > reservationSize)) {
						planet.setColonists(planet.getColonists() + assimilation);
						planet.setNativeSpeciesCount(nativeSpeciesCount - assimilation);

						if (planet.getNativeSpeciesCount() <= 0) {
							planet.setNativeSpecies(null);
							planet.setNativeSpeciesCount(0);
						}
					}
				}
			}
		} else {
			planet.setNativeSpecies(null);
			planet.setNativeSpeciesCount(0);
		}
	}

	private void checkNativeSpeciesAttack(Planet planet) {
		Optional<Float> opt = planet.retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.ATTACK);

		if (opt.isPresent()) {
			int nativeSpeciesCount = planet.getNativeSpeciesCount();
			int attackerCountBase = 50 + MasterDataService.RANDOM.nextInt(Math.min(4950, nativeSpeciesCount));
			float attackStrength = opt.get() / 100f;
			int attackerCount = Math.round(attackerCountBase * attackStrength);

			int lightGroundUnits = planet.getLightGroundUnits();
			int heavyGroundUnits = planet.getHeavyGroundUnits();
			float defenseRate = planet.getPlayer().getFaction().getGroundCombatDefenseRate();
			int colonists = planet.getColonists();
			int defenderCount = (int) ((colonists + (lightGroundUnits * 16) + (heavyGroundUnits * 60)) * defenseRate);

			if (defenderCount >= attackerCount) {
				colonists = Math.round(colonists - (attackerCount * (float) colonists / defenderCount));
				lightGroundUnits = Math.round(lightGroundUnits - (attackerCount * (float) lightGroundUnits / defenderCount));
				heavyGroundUnits = Math.round(heavyGroundUnits - (attackerCount * (float) heavyGroundUnits / defenderCount));
				nativeSpeciesCount -= attackerCountBase;
			} else {
				nativeSpeciesCount -= Math.round(defenderCount / attackStrength);
				colonists = 0;
				lightGroundUnits = 0;
				heavyGroundUnits = 0;
			}

			planet.setColonists(colonists);
			planet.setLightGroundUnits(Math.max(lightGroundUnits, 0));
			planet.setHeavyGroundUnits(Math.max(heavyGroundUnits, 0));
			planet.setNativeSpeciesCount(Math.max(nativeSpeciesCount, 0));

			if (planet.getNativeSpeciesCount() == 0) {
				planet.setNativeSpecies(null);
			}
		}
	}

	private void checkNativeSpeciesWarning(Planet planet) {
		if (planet.retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.WARN_ABOUT_ENEMY_SHIPS).isPresent()) {
			Player player = planet.getPlayer();
			long count = shipRepository.getEnemyShipCountByPlanetDestination(planet.getX(), planet.getY(), player.getGame().getId(), player.getId());

			if (count > 0L) {
				newsService.add(player, NewsEntryConstants.news_entry_native_species_warning, planet.createFullImagePath(), planet.getId(),
					NewsEntryClickTargetType.planet, planet.getName());
			}
		}
	}

	private void updateStarbaseWithNativeSpecies(Planet planet, Starbase starbase) {
		Optional<Float> opt = planet.retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.STARBASE_ALL_TECHLEVEL_BONUS);

		if (opt.isPresent()) {
			int level = opt.get().intValue();
			starbase.setHullLevel(level);
			starbase.setPropulsionLevel(level);
			starbase.setEnergyLevel(level);
			starbase.setProjectileLevel(level);
			return;
		}

		opt = planet.retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.STARBASE_ENERGY_BONUS);

		if (opt.isPresent()) {
			int level = opt.get().intValue();
			starbase.setEnergyLevel(level);
			return;
		}

		opt = planet.retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.STARBASE_PROPULSION_BONUS);

		if (opt.isPresent()) {
			int level = opt.get().intValue();
			starbase.setPropulsionLevel(level);
			return;
		}

		opt = planet.retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.STARBASE_RANDOM_BONUS);

		if (opt.isPresent()) {
			int tech = MasterDataService.RANDOM.nextInt(4);
			int level = 1 + MasterDataService.RANDOM.nextInt(10);

			switch (tech) {
				case 0:
					starbase.setHullLevel(level);
					break;
				case 1:
					starbase.setPropulsionLevel(level);
					break;
				case 2:
					starbase.setEnergyLevel(level);
					break;
				case 3:
					starbase.setProjectileLevel(level);
					break;
			}

			return;
		}

		opt = planet.retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.UNLIMITED_SUNSAILS);

		if (opt.isPresent()) {
			PropulsionSystemTemplate template = propulsionSystemTemplateRepository.getReferenceById("sun_sail");
			StarbasePropulsionStock stock = new StarbasePropulsionStock();
			stock.setPropulsionSystemTemplate(template);
			stock.setStarbase(starbase);
			stock.setStock(1000);
			starbasePropulsionStockRepository.save(stock);
		}
	}

	private void reduceColonistsAndGroundUnits(Planet planet) {
		planet.setColonists(planet.getColonists() - 50 - MasterDataService.RANDOM.nextInt(150));

		if (planet.getColonists() <= 0) {
			planet.setColonists(0);
			boolean ownerLosesPlanet = false;

			int allGroundUnits = planet.getLightGroundUnits() + planet.getHeavyGroundUnits();

			if (allGroundUnits == 0 || planet.getSupplies() == 0) {
				ownerLosesPlanet = true;
			} else {
				int suppliesConsumingGroundUnits = Math.round(allGroundUnits * 0.15f);

				if (suppliesConsumingGroundUnits < 15) {
					suppliesConsumingGroundUnits = 15;
				}

				if (suppliesConsumingGroundUnits > planet.getSupplies()) {
					int groundUnitsWithoutSupplies = suppliesConsumingGroundUnits - planet.getSupplies();
					planet.setSupplies(0);

					int groundUnitsToBeRemoved = Math.round(groundUnitsWithoutSupplies * 0.15f);
					allGroundUnits -= groundUnitsToBeRemoved;

					if (allGroundUnits <= 0) {
						ownerLosesPlanet = true;
					} else {
						if (groundUnitsToBeRemoved <= planet.getHeavyGroundUnits()) {
							planet.setHeavyGroundUnits(planet.getHeavyGroundUnits() - groundUnitsToBeRemoved);
						} else {
							groundUnitsToBeRemoved -= planet.getHeavyGroundUnits();
							planet.setHeavyGroundUnits(0);
							planet.setLightGroundUnits(planet.getLightGroundUnits() - groundUnitsToBeRemoved);
						}
					}
				} else {
					planet.spendSupplies(suppliesConsumingGroundUnits);
				}
			}

			if (ownerLosesPlanet) {
				long playerId = planet.getPlayer().getId();

				statsUpdater.incrementStats(planet.getPlayer(), LoginStatsFaction::getPlanetsLost, LoginStatsFaction::setPlanetsLost);

				if (planet.getStarbase() != null) {
					statsUpdater.incrementStats(planet.getPlayer(), LoginStatsFaction::getStarbasesLost, LoginStatsFaction::setStarbasesLost);
				}

				planet.setSupplies(0);
				planet.setAutoBuildMines(false);
				planet.setAutoBuildFactories(false);
				planet.setAutoBuildPlanetaryDefense(false);
				planet.setPlayer(null);
				planet.setLog(null);

				controlGroupRepository.deleteByTypeAndEntityId(ControlGroupType.PLANET, planet.getId());

				if (planet.getStarbase() != null) {
					controlGroupRepository.deleteByTypeAndEntityId(ControlGroupType.STARBASE, planet.getStarbase().getId());
				}

				shipRepository.clearCurrentRouteEntryByRoutePlanetId(planet.getId());
				shipRouteEntryRepository.deleteByPlanetIdAndPlayerId(planet.getId(), playerId);
			}
		}
	}

	private void increaseColonistsAndMoney(Planet planet) {
		float growthRate = planet.retrievePopulationGrowthRate();
		planet.setColonists(planet.getColonists() + Math.round(planet.getColonists() * growthRate));
	}

	protected void increaseColonistsWithBoundaryCheck(Planet planet, int additionalColonists) {
		int oldValue = planet.getColonists();

		planet.setColonists(planet.getColonists() + additionalColonists);

		if (oldValue > 0 && planet.getColonists() < 0) {
			planet.setColonists(Integer.MAX_VALUE);
		}
	}

	protected void processMines(Planet planet) {
		int mines = planet.getMines();

		if (mines > 0) {
			int totalResources = planet.retrieveTotalUntappedResources();

			if (totalResources > 0) {
				planet.mineFuel(planet.retrieveFuelToBeMined());
				planet.mineMineral1(planet.retrieveMineral1ToBeMined());
				planet.mineMineral2(planet.retrieveMineral2ToBeMined());
				planet.mineMineral3(planet.retrieveMineral3ToBeMined());

				if (planet.retrieveTotalUntappedResources() <= 0) {
					statsUpdater.incrementStats(planet.getPlayer(), LoginStatsFaction::getPlanetsMinedOut, LoginStatsFaction::setPlanetsMinedOut,
						AchievementType.PLANETS_MINED_OUT);
				}
			}
		}
	}

	private void processFactories(Planet planet) {
		int newSupplies = planet.retrieveAdditionalSupplies();

		if (newSupplies > 0) {
			Optional<Float> opt = planet.retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.CONSUME_SUPPLIES);

			if (opt.isPresent()) {
				newSupplies -= Math.round(newSupplies / 100f * (30f + MasterDataService.RANDOM.nextInt(50)));
			}

			planet.spendSupplies(-newSupplies);
		}
	}

	private void checkProducingNativeSpecies(Planet planet) {
		Optional<Float> opt = planet.retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.CREATE_FUEL);

		if (opt.isPresent()) {
			int fuel = (int) Math.ceil(opt.get() * (planet.getNativeSpeciesCount() / 10000f));
			planet.spendFuel(-fuel);
			return;
		}
	}

	private void autoBuildFactories(Planet planet) {
		int max = planet.retrieveMaxFactories();
		int add = max - planet.getFactories();

		if (add > 0) {
			planetService.buildFactoriesWithoutPermissionCheck(planet.getId(), add);
		}
	}

	private void autoBuildMines(Planet planet) {
		int max = planet.retrieveMaxMines();
		int add = max - planet.getMines();

		if (add > 0) {
			planetService.buildMinesWithoutPermissionCheck(planet.getId(), add);
		}
	}

	private void autoBuildPlanetaryDefense(Planet planet) {
		int max = planet.retrieveMaxPlanetaryDefense();
		int add = max - planet.getPlanetaryDefense();

		if (add > 0) {
			planetService.buildPlanetaryDefenseWithoutPermissionCheck(planet.getId(), add);
		}
	}

	private void autoSellSupplies(Planet planet) {
		if (planet.getSupplies() > 0) {
			planet.spendMoney(-planet.getSupplies());
			planet.setSupplies(0);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void logVisitedPlanets(long gameId) {
		log.debug("Logging visited planets...");

		Game game = gameRepository.getReferenceById(gameId);
		FogOfWarType fogOfWarType = game.getFogOfWarType();

		int radius = getRadius(game, fogOfWarType);

		List<Player> players = game.getPlayers();
		Set<Planet> planets = game.getPlanets();

		Map<String, PlayerPlanetScanLog> playerScanLogsMap = playerPlanetScanLogRepository.findByGameId(gameId).stream()
			.collect(Collectors.toMap(l -> l.getPlanet().getId() + "_" + l.getPlayer().getId(), l -> l));

		Map<String, PlayerStarbaseScanLog> starbaseScanLogMap = playerStarbaseScanLogRepository.findByGameId(gameId).stream()
			.collect(Collectors.toMap(l -> l.getStarbase().getId() + "_" + l.getPlayer().getId(), l -> l));

		List<PlayerPlanetScanLog> planetLogsToBeSaved = new ArrayList<>();
		List<PlayerStarbaseScanLog> starbaseLogsToBeSaved = new ArrayList<>();

		for (Player player : players) {
			long playerId = player.getId();
			Set<CoordinateImpl> visibilityCoordinates = visibleObjects.getVisibilityCoordinates(playerId);

			for (Planet planet : planets) {
				for (CoordinateImpl coords : visibilityCoordinates) {
					double distance = coords.getDistance(planet);

					if (distance <= radius) {
						PlayerPlanetScanLog planetScanLog = getPlayerPlanetScanLog(playerScanLogsMap, player, playerId, planet);

						Starbase starbase = planet.getStarbase();

						handleStarbase(starbaseScanLogMap, starbaseLogsToBeSaved, player, playerId, starbase);

						checkVisibilityRadius(planet, distance, planetScanLog, starbase);

						planetLogsToBeSaved.add(planetScanLog);
						break;
					}
				}
			}
		}

		if (!planetLogsToBeSaved.isEmpty()) {
			playerPlanetScanLogRepository.saveAll(planetLogsToBeSaved);
		}

		if (!starbaseLogsToBeSaved.isEmpty()) {
			playerStarbaseScanLogRepository.saveAll(starbaseLogsToBeSaved);
		}

		log.debug("Logging visited planets finished.");
	}

	private int getRadius(Game game, FogOfWarType fogOfWarType) {
		int radius = game.getGalaxySize() * 2;

		if (fogOfWarType == FogOfWarType.LONG_RANGE_SENSORS) {
			radius = 250;
		} else if (fogOfWarType == FogOfWarType.VISITED) {
			radius = 150;
		}

		return radius;
	}

	private void handleStarbase(Map<String, PlayerStarbaseScanLog> starbaseScanLogMap, List<PlayerStarbaseScanLog> starbaseLogsToBeSaved, Player player, long playerId, Starbase starbase) {
		if (starbase != null) {
			PlayerStarbaseScanLog starbaseScanLog = starbaseScanLogMap.get(starbase.getId() + "_" + playerId);

			if (starbaseScanLog == null) {
				starbaseScanLog = new PlayerStarbaseScanLog();
				starbaseScanLog.setPlayer(player);
				starbaseScanLog.setStarbase(starbase);
				starbaseLogsToBeSaved.add(starbaseScanLog);
			}
		}
	}

	@NotNull
	private PlayerPlanetScanLog getPlayerPlanetScanLog(Map<String, PlayerPlanetScanLog> playerScanLogsMap, Player player, long playerId, Planet planet) {
		PlayerPlanetScanLog planetScanLog = playerScanLogsMap.get(planet.getId() + "_" + playerId);

		if (planetScanLog == null) {
			planetScanLog = new PlayerPlanetScanLog();
			planetScanLog.setPlayer(player);
			planetScanLog.setPlanet(planet);
		}

		return planetScanLog;
	}

	private void checkVisibilityRadius(Planet planet, double distance, PlayerPlanetScanLog planetScanLog, Starbase starbase) {
		if (distance <= configProperties.getVisibilityRadius()) {
			planetScanLog.setHadStarbase(starbase != null);
			Player planetOwner = planet.getPlayer();
			planetScanLog.setLastPlayerColor(planetOwner != null ? planetOwner.getColor() : null);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateScanRadius(long gameId) {
		log.debug("Updating planet scan radius...");

		planetRepository.resetScanRadius(gameId);
		planetRepository.updatePsyCorpsScanRadius(gameId);
		planetRepository.updateExtendedScanRadius(gameId);

		log.debug("Updating planet scan radius finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void scanPlanetsWithShips(long gameId) {
		log.debug("Scanning planets with ships...");

		shipRepository.scanPlanets(gameId);
		shipRepository.scanStarbases(gameId);

		log.debug("Scanning planets with ships finished.");
	}
}
