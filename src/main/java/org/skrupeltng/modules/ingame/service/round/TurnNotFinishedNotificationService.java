package org.skrupeltng.modules.ingame.service.round;

import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.mail.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class TurnNotFinishedNotificationService {

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private MailService mailService;

	// Every ten seconds
	@Scheduled(fixedDelay = 10000)
	public void checkTurnNotFinishedPlayers() {
		List<Game> games = gameRepository.findGamesWithTurnNotFinishedNotification();
		final LocalDateTime now = LocalDateTime.now();

		for (Game game : games) {
			LocalDateTime dt = game.getRoundDate().atZone(ZoneId.systemDefault()).toLocalDateTime();
			LocalDateTime notificationDate = dt.plusSeconds(game.getTurnNotFinishedNotificationSeconds());

			if (notificationDate.isBefore(now)) {
				try {
					Set<Player> humanPlayers = game.getPlayers().stream().filter(p -> p.getAiLevel() == null).collect(Collectors.toSet());

					if (humanPlayers.size() > 1) {
						for (Player player : humanPlayers) {
							if (!player.isTurnFinished()) {
								mailService.sendTurnNotFinishedNotificationMail(player);
							}
						}
					}

				} finally {
					gameRepository.setTurnNotFinishedNotificationsSent(game.getId(), true);
				}
			}
		}
	}

	// Every ten seconds
	@Scheduled(fixedDelay = 10000)
	public void checkLastToNotFinishTurnPlayers() {
		List<Game> games = gameRepository.findGamesWithLastToFinishTurnNotification();
		final LocalDateTime now = LocalDateTime.now();

		for (Game game : games) {
			LocalDateTime dt = game.getRoundDate().atZone(ZoneId.systemDefault()).toLocalDateTime();
			LocalDateTime notificationDate = dt.plusSeconds(game.getTurnNotFinishedNotificationSeconds());

			if (notificationDate.isBefore(now)) {
				boolean lastPlayerFound = false;

				try {
					Set<Player> humanPlayers = game.getPlayers().stream().filter(p -> p.getAiLevel() == null).collect(Collectors.toSet());

					if (humanPlayers.size() > 1) {
						Set<Player> notFinishedPlayers = game.getPlayers().stream().filter(p -> !p.isTurnFinished()).collect(Collectors.toSet());

						if (notFinishedPlayers.size() == 1) {
							lastPlayerFound = true;
							Player player = notFinishedPlayers.iterator().next();
							mailService.sendLastToFinishTurnNotificationMail(player);
						}
					}
				} finally {
					if (lastPlayerFound) {
						gameRepository.setLastToFinishTurnNotificationsSent(game.getId(), true);
						gameRepository.setTurnNotFinishedNotificationsSent(game.getId(), true);
					}
				}
			}
		}
	}
}
