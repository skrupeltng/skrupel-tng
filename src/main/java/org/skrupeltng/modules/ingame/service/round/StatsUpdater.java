package org.skrupeltng.modules.ingame.service.round;

import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.dashboard.database.LoginStatsFactionRepository;
import org.skrupeltng.modules.dashboard.service.AchievementService;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StatsUpdater {

	@Autowired
	private LoginStatsFactionRepository loginStatsFactionRepository;

	@Autowired
	private AchievementService achievementService;

	public void incrementStats(Player player, Function<LoginStatsFaction, Integer> getter, BiConsumer<LoginStatsFaction, Integer> setter,
			AchievementType... achievementTypes) {
		Login login = player.getLogin();
		String factionId = player.getFaction().getId();

		Optional<LoginStatsFaction> opt = loginStatsFactionRepository.findByLoginIdAndFactionId(login.getId(), factionId);
		LoginStatsFaction stats = opt.orElseGet(() -> new LoginStatsFaction(login, factionId));

		for (AchievementType type : achievementTypes) {
			long oldValue = loginStatsFactionRepository.getSum(player.getLogin().getId(), type.getFieldName());

			if (type.getStatsValue() <= oldValue + 1) {
				achievementService.unlockAchievement(player, type);
			}
		}

		int nextValue = getter.apply(stats) + 1;
		setter.accept(stats, nextValue);

		loginStatsFactionRepository.save(stats);
	}
}