package org.skrupeltng.modules.ingame.service.round;

import java.util.List;
import java.util.Optional;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.anomaly.database.PlayerWormHoleVisit;
import org.skrupeltng.modules.ingame.modules.anomaly.database.PlayerWormHoleVisitRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHole;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class WormHoleRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private WormHoleRepository wormHoleRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private PlayerWormHoleVisitRepository playerWormHoleVisitRepository;

	@Autowired
	private NewsService newsService;

	@Autowired
	private ConfigProperties configProperties;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processWormholes(long gameId) {
		log.debug("Processing wormholes...");

		List<WormHole> wormholes = wormHoleRepository.findByGameId(gameId);

		for (WormHole wormHole : wormholes) {
			WormHoleType type = wormHole.getType();
			int reach = type == WormHoleType.STABLE_WORMHOLE || type == WormHoleType.UNSTABLE_WORMHOLE ? WormHole.WORMHOLE_REACH : WormHole.JUMP_PORTAL_REACH;

			List<Long> shipIds = shipRepository.getShipIdsInRadius(gameId, wormHole.getX(), wormHole.getY(), reach);

			if (shipIds.isEmpty()) {
				continue;
			}

			List<Ship> ships = shipRepository.findByIds(shipIds);
			int galaxySize = wormHole.getGame().getGalaxySize();

			for (Ship ship : ships) {
				WormHole connection = wormHole.getConnection();
				boolean shipDestroysConnection = ship.hasActiveAbility(ShipAbilityType.HERMETIC_MATRIX);

				if (connection != null) {
					if (shipDestroysConnection) {
						connection.setConnection(null);
						connection = wormHoleRepository.save(connection);

						wormHole.setConnection(null);
						wormHole = wormHoleRepository.save(wormHole);

						playerWormHoleVisitRepository.deleteByWormHoleId(connection.getId());
						playerWormHoleVisitRepository.deleteByWormHoleId(wormHole.getId());
					}

					handleWormholeWithConnection(reach, galaxySize, ship, connection, shipDestroysConnection);
				} else {
					handleWormholeWithoutConnection(gameId, galaxySize, ship, wormHole);
				}

				logVisit(wormHole, ship);
			}

			shipRepository.clearDestinationShip(shipIds);
		}

		log.debug("Wormholes processed.");
	}

	private void handleWormholeWithConnection(int reach, int galaxySize, Ship ship, WormHole connection, boolean shipDestroysConnection) {
		Player player = ship.getPlayer();
		String name = ship.getName();
		WormHoleType type = connection.getType();
		String image = ship.createFullImagePath();

		CoordinateImpl nearCoordinates = Coordinate.getRandomNearCoordinates(connection.getX(), connection.getY(), reach + 3, galaxySize);

		ship.setX(nearCoordinates.getX());
		ship.setY(nearCoordinates.getY());
		ship.setLastX(ship.getX());
		ship.setLastY(ship.getY());
		ship.resetTravel();

		ship = shipRepository.save(ship);

		if (shipDestroysConnection) {
			if (type == WormHoleType.JUMP_PORTAL) {
				newsService.add(player, NewsEntryConstants.news_entry_jump_portal_passed_connection_cut, image, ship.getId(), NewsEntryClickTargetType.ship,
						name);
			} else {
				newsService.add(player, NewsEntryConstants.news_entry_stable_wormhole_passed_connection_cut, image, ship.getId(), NewsEntryClickTargetType.ship,
						name);
			}
		} else {
			if (type == WormHoleType.JUMP_PORTAL) {
				newsService.add(player, NewsEntryConstants.news_entry_jump_portal_passed, image, ship.getId(), NewsEntryClickTargetType.ship, name);
			} else {
				newsService.add(player, NewsEntryConstants.news_entry_stable_wormhole_passed, image, ship.getId(), NewsEntryClickTargetType.ship, name);
			}
		}
	}

	private void handleWormholeWithoutConnection(long gameId, int galaxySize, Ship ship, WormHole wormHole) {
		Player player = ship.getPlayer();
		String name = ship.getName();
		WormHoleType type = wormHole.getType();
		String image = ship.createFullImagePath();

		int x = 50;
		int y = 50;

		for (int i = 0; i < configProperties.getIterationLimit(); i++) {
			x = 50 + MasterDataService.RANDOM.nextInt(galaxySize - 50);
			y = 50 + MasterDataService.RANDOM.nextInt(galaxySize - 50);

			List<Long> planets = planetRepository.getPlanetIdsInRadius(gameId, x, y, 20, false);

			if (planets.isEmpty()) {
				break;
			}
		}

		ship.setX(x);
		ship.setY(y);
		ship.setLastX(x);
		ship.setLastY(y);
		ship.resetTravel();

		ship = shipRepository.save(ship);

		if (type == WormHoleType.JUMP_PORTAL) {
			newsService.add(player, NewsEntryConstants.news_entry_unfinished_jump_portal_passed, image, ship.getId(), NewsEntryClickTargetType.ship, name);
		} else {
			newsService.add(player, NewsEntryConstants.news_entry_unstable_wormhole_passed, image, ship.getId(), NewsEntryClickTargetType.ship, name);
		}
	}

	private void logVisit(WormHole wormHole, Ship ship) {
		Player player = ship.getPlayer();
		Optional<PlayerWormHoleVisit> visitOpt = playerWormHoleVisitRepository.findByPlayerIdAndWormHoleId(player.getId(), wormHole.getId());

		if (visitOpt.isEmpty()) {
			if (wormHole.getConnection() != null) {
				visitOpt = playerWormHoleVisitRepository.findByPlayerIdAndWormHoleId(player.getId(), wormHole.getConnection().getId());

				if (visitOpt.isPresent()) {
					return;
				}
			}

			PlayerWormHoleVisit visit = new PlayerWormHoleVisit();
			visit.setPlayer(player);
			visit.setWormHole(wormHole);
			playerWormHoleVisitRepository.save(visit);
		}
	}
}
