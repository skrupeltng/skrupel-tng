package org.skrupeltng.modules.ingame.controller;

import java.util.Set;

import org.skrupeltng.modules.ingame.database.player.Player;

public class SendPlayerMessageHelper {

	private final Set<Long> encounteredPlayers;

	public SendPlayerMessageHelper(Set<Long> encounteredPlayers) {
		this.encounteredPlayers = encounteredPlayers;
	}

	public boolean canSendMessage(Player player) {
		return player.getAiLevel() == null && (encounteredPlayers == null || encounteredPlayers.contains(player.getId()));
	}

	public boolean canSendMessage(Long playerId, String playerAiLevel) {
		return playerAiLevel == null && (encounteredPlayers == null || encounteredPlayers.contains(playerId));
	}
}