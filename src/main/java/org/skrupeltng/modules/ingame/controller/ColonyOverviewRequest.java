package org.skrupeltng.modules.ingame.controller;

public class ColonyOverviewRequest extends AbstractPageRequest {

	private static final long serialVersionUID = 1071094212625152494L;

	private long gameId;
	private String name;
	private Boolean starbase;
	private Boolean hasRoute;
	private Boolean hasNativeSpecies;
	private Boolean autoBuildsMines;
	private Boolean autoBuildsFactories;
	private Boolean autoSellsSupplies;
	private Boolean autoBuildsDefense;

	public long getGameId() {
		return gameId;
	}

	public void setGameId(long gameId) {
		this.gameId = gameId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getStarbase() {
		return starbase;
	}

	public void setStarbase(Boolean starbase) {
		this.starbase = starbase;
	}

	public Boolean getHasRoute() {
		return hasRoute;
	}

	public void setHasRoute(Boolean hasRoute) {
		this.hasRoute = hasRoute;
	}

	public Boolean getHasNativeSpecies() {
		return hasNativeSpecies;
	}

	public void setHasNativeSpecies(Boolean hasNativeSpecies) {
		this.hasNativeSpecies = hasNativeSpecies;
	}

	public Boolean getAutoBuildsMines() {
		return autoBuildsMines;
	}

	public void setAutoBuildsMines(Boolean autoBuildsMines) {
		this.autoBuildsMines = autoBuildsMines;
	}

	public Boolean getAutoBuildsFactories() {
		return autoBuildsFactories;
	}

	public void setAutoBuildsFactories(Boolean autoBuildsFactories) {
		this.autoBuildsFactories = autoBuildsFactories;
	}

	public Boolean getAutoSellsSupplies() {
		return autoSellsSupplies;
	}

	public void setAutoSellsSupplies(Boolean autoSellsSupplies) {
		this.autoSellsSupplies = autoSellsSupplies;
	}

	public Boolean getAutoBuildsDefense() {
		return autoBuildsDefense;
	}

	public void setAutoBuildsDefense(Boolean autoBuildsDefense) {
		this.autoBuildsDefense = autoBuildsDefense;
	}
}