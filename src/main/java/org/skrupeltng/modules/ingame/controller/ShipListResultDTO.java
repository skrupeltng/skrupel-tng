package org.skrupeltng.modules.ingame.controller;

import java.io.Serializable;

import org.skrupeltng.modules.PageableResult;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;

public class ShipListResultDTO implements Serializable, PageableResult {

	private static final long serialVersionUID = 2248946169260784545L;

	private long id;
	private String name;
	private int fuelPercentage;
	private int cargoPercentage;
	private int crewPercentage;
	private int damage;
	private String templateName;
	private String fleet;

	private ShipTaskType taskType;
	private boolean hasRoute;
	private ShipAbilityType activeAbilityType;

	private int totalElements;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getFuelPercentage() {
		return fuelPercentage;
	}

	public void setFuelPercentage(int fuelPercentage) {
		this.fuelPercentage = fuelPercentage;
	}

	public int getCargoPercentage() {
		return cargoPercentage;
	}

	public void setCargoPercentage(int cargoPercentage) {
		this.cargoPercentage = cargoPercentage;
	}

	public int getCrewPercentage() {
		return crewPercentage;
	}

	public void setCrewPercentage(int crewPercentage) {
		this.crewPercentage = crewPercentage;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getFleet() {
		return fleet;
	}

	public void setFleet(String fleet) {
		this.fleet = fleet;
	}

	public ShipTaskType getTaskType() {
		return taskType;
	}

	public void setTaskType(ShipTaskType taskType) {
		this.taskType = taskType;
	}

	public boolean isHasRoute() {
		return hasRoute;
	}

	public void setHasRoute(boolean hasRoute) {
		this.hasRoute = hasRoute;
	}

	public ShipAbilityType getActiveAbilityType() {
		return activeAbilityType;
	}

	public void setActiveAbilityType(ShipAbilityType activeAbilityType) {
		this.activeAbilityType = activeAbilityType;
	}

	@Override
	public int getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}
}