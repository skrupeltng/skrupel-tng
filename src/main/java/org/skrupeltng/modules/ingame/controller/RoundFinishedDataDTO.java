package org.skrupeltng.modules.ingame.controller;

import java.io.Serializable;

public class RoundFinishedDataDTO implements Serializable {

	private static final long serialVersionUID = 932745659086622282L;

	private boolean storyModeMissionSuccess;
	private boolean moreCampaignsAvailable;
	private boolean storyModeMissionFailed;

	public boolean isStoryModeMissionSuccess() {
		return storyModeMissionSuccess;
	}

	public void setStoryModeMissionSuccess(boolean storyModeMissionSuccess) {
		this.storyModeMissionSuccess = storyModeMissionSuccess;
	}

	public boolean isMoreCampaignsAvailable() {
		return moreCampaignsAvailable;
	}

	public void setMoreCampaignsAvailable(boolean moreCampaignsAvailable) {
		this.moreCampaignsAvailable = moreCampaignsAvailable;
	}

	public boolean isStoryModeMissionFailed() {
		return storyModeMissionFailed;
	}

	public void setStoryModeMissionFailed(boolean storyModeMissionFailed) {
		this.storyModeMissionFailed = storyModeMissionFailed;
	}
}