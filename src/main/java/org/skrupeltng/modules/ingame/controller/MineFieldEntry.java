package org.skrupeltng.modules.ingame.controller;

import org.skrupeltng.modules.ingame.Coordinate;

public class MineFieldEntry implements Coordinate {

	private long id;
	private int x;
	private int y;
	private boolean own;
	private boolean friendly;

	@Override
	public int getX() {
		return x;
	}

	@Override
	public int getY() {
		return y;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isOwn() {
		return own;
	}

	public void setOwn(boolean own) {
		this.own = own;
	}

	public boolean isFriendly() {
		return friendly;
	}

	public void setFriendly(boolean friendly) {
		this.friendly = friendly;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public int getScanRadius() {
		return 0;
	}

	public String retrieveGalaxyMapSizeStyle() {
		return "left: " + (x - 87) + "px; top: " + (y - 87) + "px;";
	}
}