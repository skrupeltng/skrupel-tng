package org.skrupeltng.modules.ingame.controller;

import java.io.Serializable;

import org.skrupeltng.modules.PageableResult;

public class StarbaseListResultDTO implements Serializable, PageableResult {

	private static final long serialVersionUID = 9076140976069273366L;

	private long id;
	private String name;
	private boolean hasShipInProduction;
	private int hullLevel;
	private int propulsionLevel;
	private int energyLevel;
	private int projectileLevel;

	private int totalElements;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isHasShipInProduction() {
		return hasShipInProduction;
	}

	public void setHasShipInProduction(boolean hasShipInProduction) {
		this.hasShipInProduction = hasShipInProduction;
	}

	public int getHullLevel() {
		return hullLevel;
	}

	public void setHullLevel(int hullLevel) {
		this.hullLevel = hullLevel;
	}

	public int getPropulsionLevel() {
		return propulsionLevel;
	}

	public void setPropulsionLevel(int propulsionLevel) {
		this.propulsionLevel = propulsionLevel;
	}

	public int getEnergyLevel() {
		return energyLevel;
	}

	public void setEnergyLevel(int energyLevel) {
		this.energyLevel = energyLevel;
	}

	public int getProjectileLevel() {
		return projectileLevel;
	}

	public void setProjectileLevel(int projectileLevel) {
		this.projectileLevel = projectileLevel;
	}

	@Override
	public int getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}
}