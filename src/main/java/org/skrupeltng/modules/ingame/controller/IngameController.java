package org.skrupeltng.modules.ingame.controller;

import jakarta.annotation.PostConstruct;
import org.apache.commons.lang3.tuple.Pair;
import org.skrupeltng.exceptions.ResourceNotFoundException;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.SortField;
import org.skrupeltng.modules.SortFieldItem;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.PlayerGameTurnInfoItem;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.modules.anomaly.database.PlasmaStorm;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFold;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHole;
import org.skrupeltng.modules.ingame.modules.controlgroup.database.ControlGroup;
import org.skrupeltng.modules.ingame.modules.controlgroup.service.ControlGroupService;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetService;
import org.skrupeltng.modules.ingame.modules.overview.controller.PlayerMessageDTO;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntry;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.overview.service.OverviewService;
import org.skrupeltng.modules.ingame.modules.overview.service.PlayerMessageService;
import org.skrupeltng.modules.ingame.modules.overview.service.RoundSummary;
import org.skrupeltng.modules.ingame.modules.planet.controller.NativeSpeciesDescription;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.service.PlanetService;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationContainer;
import org.skrupeltng.modules.ingame.modules.politics.service.PlayerEncounterService;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipCloakingSummary;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseShipConstructionJob;
import org.skrupeltng.modules.ingame.modules.starbase.service.StarbaseService;
import org.skrupeltng.modules.ingame.service.IngameService;
import org.skrupeltng.modules.ingame.service.VisiblePlanetsAndShips;
import org.skrupeltng.modules.ingame.service.round.PoliticsRoundCalculator;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/ingame")
public class IngameController extends AbstractController {

	@Autowired
	private IngameService ingameService;

	@Autowired
	private PlanetService planetService;

	@Autowired
	private StarbaseService starbaseService;

	@Autowired
	private ShipService shipService;

	@Autowired
	private FleetService fleetService;

	@Autowired
	private NewsService newsService;

	@Autowired
	private OverviewService overviewService;

	@Autowired
	private PoliticsService politicsService;

	@Autowired
	private PlayerMessageService playerMessageService;

	@Autowired
	private ControlGroupService controlGroupService;

	@Autowired
	private PoliticsRoundCalculator politicsRoundCalculator;

	@Autowired
	private PlayerEncounterService playerEncounterService;

	private final List<SortFieldItem> coloniesOverviewSortItems = new ArrayList<>();
	private final List<SortFieldItem> starbasesOverviewSortItems = new ArrayList<>();
	private final List<SortFieldItem> shipsOverviewSortItems = new ArrayList<>();
	private final List<SortFieldItem> fleetsOverviewSortItems = new ArrayList<>();

	@PostConstruct
	public void init() {
		addToColoniesOverviewSortItems(SortField.COLONY_OVERVIEW_NAME);
		addToColoniesOverviewSortItems(SortField.COLONY_OVERVIEW_COLONISTS);
		addToColoniesOverviewSortItems(SortField.COLONY_OVERVIEW_MONEY);
		addToColoniesOverviewSortItems(SortField.COLONY_OVERVIEW_SUPPLIES);
		addToColoniesOverviewSortItems(SortField.COLONY_OVERVIEW_FUEL);
		addToColoniesOverviewSortItems(SortField.COLONY_OVERVIEW_MINERAL1);
		addToColoniesOverviewSortItems(SortField.COLONY_OVERVIEW_MINERAL2);
		addToColoniesOverviewSortItems(SortField.COLONY_OVERVIEW_MINERAL3);
		addToColoniesOverviewSortItems(SortField.COLONY_OVERVIEW_MINES);
		addToColoniesOverviewSortItems(SortField.COLONY_OVERVIEW_FACTORIES);
		addToColoniesOverviewSortItems(SortField.COLONY_OVERVIEW_DEFENSE);
		addToColoniesOverviewSortItems(SortField.COLONY_ORBITAL_SYSTEMS);

		addToStarbasesOverviewSortItems(SortField.STARBASE_OVERVIEW_NAME);
		addToStarbasesOverviewSortItems(SortField.STARBASE_OVERVIEW_HAS_SHIP_IN_PRODUCTION);
		addToStarbasesOverviewSortItems(SortField.STARBASE_OVERVIEW_HULL_LEVEL);
		addToStarbasesOverviewSortItems(SortField.STARBASE_OVERVIEW_PROPULSION_LEVEL);
		addToStarbasesOverviewSortItems(SortField.STARBASE_OVERVIEW_ENERGY_LEVEL);
		addToStarbasesOverviewSortItems(SortField.STARBASE_OVERVIEW_PROPULSION_LEVEL);

		addToShipsOverviewSortItems(SortField.SHIP_OVERVIEW_NAME);
		addToShipsOverviewSortItems(SortField.SHIP_OVERVIEW_TEMPLATE_NAME);
		addToShipsOverviewSortItems(SortField.SHIP_OVERVIEW_FLEET);
		addToShipsOverviewSortItems(SortField.SHIP_OVERVIEW_FUEL_PERCENTAGE);
		addToShipsOverviewSortItems(SortField.SHIP_OVERVIEW_CARGO_PERCENTAGE);
		addToShipsOverviewSortItems(SortField.SHIP_OVERVIEW_CREW_PERCENTAGE);
		addToShipsOverviewSortItems(SortField.SHIP_OVERVIEW_DAMAGE);

		addToFleetsOverviewSortItems(SortField.FLEET_OVERVIEW_NAME);
		addToFleetsOverviewSortItems(SortField.FLEET_OVERVIEW_SHIP_COUNT);
		addToFleetsOverviewSortItems(SortField.FLEET_OVERVIEW_FUEL_PERCENTAGE);
		addToFleetsOverviewSortItems(SortField.FLEET_OVERVIEW_PROJECTILE_PERCENTAGE);
	}

	private void addToColoniesOverviewSortItems(SortField sortField) {
		addToSortItems(coloniesOverviewSortItems, sortField);
	}

	private void addToStarbasesOverviewSortItems(SortField sortField) {
		addToSortItems(starbasesOverviewSortItems, sortField);
	}

	private void addToShipsOverviewSortItems(SortField sortField) {
		addToSortItems(shipsOverviewSortItems, sortField);
	}

	private void addToFleetsOverviewSortItems(SortField sortField) {
		addToSortItems(fleetsOverviewSortItems, sortField);
	}

	@GetMapping("/game")
	public String game(@RequestParam long id, Model model) {
		Optional<Game> optional = ingameService.getGame(id);

		if (optional.isEmpty()) {
			throw new ResourceNotFoundException();
		}

		Game game = optional.get();

		long loginId = userDetailService.getLoginId();
		Player player = ingameService.getPlayer(loginId, id);

		if (player.isHasLost() || game.isFinished()) {
			return "redirect:/game?id=" + id;
		}

		Planet homePlanet = player.getHomePlanet();

		if (homePlanet != null) {
			int initX = homePlanet.getX();
			int initY = homePlanet.getY();

			if (homePlanet.getPlayer() != player) {
				Optional<Planet> planetOpt = player.getPlanets().stream().max(Comparator.comparing(Planet::getColonists));

				if (planetOpt.isPresent()) {
					Planet planet = planetOpt.get();
					initX = planet.getX();
					initY = planet.getY();
				}
			}

			model.addAttribute("initX", initX);
			model.addAttribute("initY", initY);

			if (game.getRound() == 1) {
				model.addAttribute("initPlanetId", homePlanet.getId());
			}
		}

		long playerId = player.getId();

		model.addAttribute("game", game);
		model.addAttribute("playerId", playerId);
		model.addAttribute("turnFinished", player.isTurnFinished());
		model.addAttribute("random", MasterDataService.RANDOM);
		model.addAttribute("playerColor", player.getColor());
		model.addAttribute("showOverviewModal", showOverviewModal(player));
		model.addAttribute("ingameZoomLevel", player.getLogin().getIngameZoomLevel());

		Set<Long> encounteredPlayers = null;

		if (game.isEnableWysiwyg()) {
			encounteredPlayers = playerEncounterService.getEncounteredPlayerIds(playerId);
		}

		model.addAttribute("sendPlayerMessageHelper", new SendPlayerMessageHelper(encounteredPlayers));

		List<PlayerGameTurnInfoItem> turnInfos = ingameService.getTurnInfosByLogin(loginId, id);
		model.addAttribute("turnInfos", turnInfos);

		long unfinishedTurns = turnInfos.stream().filter(g -> !g.isTurnFinished()).count();
		model.addAttribute("unfinishedTurns", unfinishedTurns);

		Set<CoordinateImpl> visibilityCoordinates = ingameService.getVisibilityCoordinates(id, loginId);
		model.addAttribute("visibilityCoordinates", visibilityCoordinates);

		List<WormHole> wormHoles = ingameService.getWormHoles(id, visibilityCoordinates, playerId);
		model.addAttribute("wormHoles", wormHoles);

		List<SpaceFold> spaceFolds = ingameService.getSpaceFolds(id, visibilityCoordinates);
		model.addAttribute("spaceFolds", spaceFolds);

		List<PlasmaStorm> plasmaStorms = ingameService.getPlasmaStorms(id);
		model.addAttribute("plasmaStorms", plasmaStorms);

		List<MineFieldEntry> mineFields = ingameService.getMineFields(id, visibilityCoordinates, playerId);
		model.addAttribute("mineFields", mineFields);

		Map<Long, PlayerRelationType> relationsMap = getRelationsMap(playerId);
		model.addAttribute("relations", relationsMap);

		Map<String, Player> colorToPlayerMap = game.getPlayers().stream().collect(Collectors.toMap(Player::getColor, p -> p));
		model.addAttribute("colorToPlayerMap", colorToPlayerMap);

		List<ControlGroup> controlGroups = getControlGroupList(id, loginId, player);
		model.addAttribute("controlGroups", controlGroups);

		int galaxySize = game.getGalaxySize();
		int sectorCount = (int)Math.ceil(galaxySize / 250f);
		List<Sector> sectors = new ArrayList<>(sectorCount * sectorCount);

		for (int lineNumber = 1; lineNumber <= sectorCount; lineNumber++) {
			for (int letterIndex = 1; letterIndex <= sectorCount; letterIndex++) {
				char letter = (char)(letterIndex + 64);

				String label = letter + "" + lineNumber;
				int x = letterIndex * 125 + ((letterIndex - 1) * 125) - 125;
				int y = lineNumber * 125 + (lineNumber - 1) * 125 - 125;

				sectors.add(new Sector(label, x, y, galaxySize));
			}
		}

		model.addAttribute("sectors", sectors);

		Map<Long, Float> tradeBonusData = politicsRoundCalculator.getTradeBonusData(id);
		model.addAttribute("tradeBonusData", tradeBonusData);
		model.addAttribute("nativeSpeciesDescription", new NativeSpeciesDescription(messageSource));

		VisiblePlanetsAndShips visiblePlanetsAndShips = ingameService.getVisiblePlanetsAndShips(id, loginId, visibilityCoordinates, false);
		model.addAttribute("planets", visiblePlanetsAndShips.getPlanets());
		model.addAttribute("ships", visiblePlanetsAndShips.getAllShips());
		model.addAttribute("singleShips", visiblePlanetsAndShips.getSingleShips());
		model.addAttribute("shipClusters", visiblePlanetsAndShips.getShipClusters());

		return "ingame/ingame";
	}

	private boolean showOverviewModal(Player player) {
		if (player.isOverviewViewed()) {
			return false;
		}

		long gameId = player.getGame().getId();
		RoundSummary roundSummary = newsService.getRoundSummary(gameId, player.getId());

		if (roundSummary.getDestroyedShips() > 0 || roundSummary.getLostShips() > 0 || roundSummary.getNewColonies() > 0 || roundSummary.getNewShips() > 0 ||
				roundSummary.getNewStarbases() > 0) {
			return true;
		}

		List<NewsEntry> news = newsService.findNews(gameId, player.getLogin().getId());

		if (!news.isEmpty()) {
			return true;
		}

		Page<PlayerMessageDTO> playerMessages = playerMessageService.getMessages(gameId);

		if (playerMessages.stream().anyMatch(m -> !m.isRead())) {
			return true;
		}

		List<PlayerRelationContainer> ownRelations = overviewService.getContainers(player.getId(), gameId);
		return ownRelations.stream().anyMatch(PlayerRelationContainer::isToBeAccepted);
	}

	private List<ControlGroup> getControlGroupList(long id, long loginId, Player player) {
		Map<Integer, ControlGroup> controlGroupsMap = controlGroupService.getControlGroups(id, loginId).stream()
				.collect(Collectors.toMap(ControlGroup::getHotkey, cg -> cg));

		List<ControlGroup> controlGroups = new ArrayList<>(10);

		for (int i = 1; i <= 10; i++) {
			if (i == 10) {
				i = 0;
			}
			ControlGroup controlGroup = controlGroupsMap.get(i);

			if (controlGroup == null) {
				controlGroups.add(new ControlGroup(player, i));
			} else {
				controlGroups.add(controlGroup);
			}

			if (i == 0) {
				break;
			}
		}

		return controlGroups;
	}

	private Map<Long, PlayerRelationType> getRelationsMap(long playerId) {
		Map<Long, PlayerRelationType> relationsMap = new HashMap<>();

		for (PlayerRelationType type : PlayerRelationType.values()) {
			List<Player> players = politicsService.getPlayersWithRelation(playerId, type);

			for (Player p : players) {
				relationsMap.put(p.getId(), type);
			}
		}

		return relationsMap;
	}

	@PostMapping("/control-group")
	public String setControlGroup(@RequestParam("gameId") long gameId, @RequestBody ControlGroupRequestDTO request, Model model) {
		long loginId = userDetailService.getLoginId();
		controlGroupService.setControlGroup(gameId, loginId, request);
		Player player = ingameService.getPlayer(loginId, gameId);

		List<ControlGroup> controlGroups = getControlGroupList(gameId, loginId, player);
		model.addAttribute("controlGroups", controlGroups);

		return "ingame/ingame::control-groups-panel";
	}

	@GetMapping("/control-groups")
	public String getControlGroups(@RequestParam("gameId") long gameId, Model model) {
		long loginId = userDetailService.getLoginId();
		Player player = ingameService.getPlayer(loginId, gameId);

		List<ControlGroup> controlGroups = getControlGroupList(gameId, loginId, player);
		model.addAttribute("controlGroups", controlGroups);

		return "ingame/ingame::control-groups-panel";
	}

	@PostMapping("/overview-viewed")
	@ResponseBody
	public void overviewViewed(@RequestParam(required = true) long gameId) {
		if (userDetailService.isAdminImpersonatingOtherUser()) {
			return;
		}

		ingameService.overviewViewed(gameId, userDetailService.getLoginId());
	}

	@PostMapping("/news-viewed")
	@ResponseBody
	public void newsViewed(@RequestParam(required = true) long gameId) {
		if (userDetailService.isAdminImpersonatingOtherUser()) {
			return;
		}

		ingameService.newsViewed(gameId, userDetailService.getLoginId());
	}

	@PostMapping("/turn")
	@ResponseBody
	public boolean finishTurn(@RequestParam(required = true) long gameId) {
		return ingameService.finishTurn(gameId, userDetailService.getLoginId());
	}

	@GetMapping("/turn-finished")
	public String turnFinished() {
		return "ingame/turn-finished::content";
	}

	@GetMapping("turn-not-finished")
	@ResponseBody
	public boolean isTurnNotFinished(@RequestParam long gameId) {
		return !ingameService.turnFinished(gameId, userDetailService.getLoginId());
	}

	@PostMapping("/round")
	@ResponseBody
	public void finishRound(@RequestParam(required = true) long gameId) {
		ingameService.calculateRound(gameId, userDetailService.getLoginId());
	}

	@GetMapping("/round")
	@ResponseBody
	public RoundFinishedDataDTO getRoundFinishedData(@RequestParam(required = true) long gameId) {
		return ingameService.getRoundFinishedData(gameId);
	}

	@GetMapping("/ship-selection")
	public String showShips(@RequestParam long gameId, @RequestParam int x, @RequestParam int y, Model model) {
		List<Ship> ships = shipService.getShipsOfLogin(gameId, userDetailService.getLoginId(), x, y);

		for (Ship ship : ships) {
			if (ship.isCloaked()) {
				ShipCloakingSummary cloakingSummary = shipService.getCloakingSummary(ship);
				ship.setCloakingSummary(cloakingSummary);
			}
		}

		model.addAttribute("ships", ships);
		model.addAttribute("x", x);
		model.addAttribute("y", y);
		model.addAttribute("fleets", fleetService.getPossibleFleetsForShip(ships.get(0).getId()));
		model.addAttribute("turnDone", turnDoneForShip(ships.get(0).getId()) && !configProperties.isDisablePermissionChecks());
		return "ingame/ship-selection";
	}

	@GetMapping("/colony-overview")
	public String showColonies(@RequestParam long gameId, Model model) {
		ColonyOverviewRequest request = new ColonyOverviewRequest();
		request.setGameId(gameId);
		request.setPageSize(15);
		request.setSortField(SortField.COLONY_OVERVIEW_COLONISTS.name());
		request.setSortAscending(false);

		Page<PlanetListResultDTO> planets = planetService.getPlanetsOfLogin(gameId, request, userDetailService.getLoginId());
		model.addAttribute("planets", planets);
		model.addAttribute("coloniesOverviewSortFields", coloniesOverviewSortItems);
		return "ingame/colony-overview::content";
	}

	@PostMapping("/colony-overview")
	public String updateColoniesOverview(@RequestBody ColonyOverviewRequest request, Model model) {
		Page<PlanetListResultDTO> planets = planetService.getPlanetsOfLogin(request.getGameId(), request, userDetailService.getLoginId());
		model.addAttribute("planets", planets);
		return "ingame/colony-overview::table-content";
	}

	@GetMapping("/starbase-overview")
	public String showStarbases(@RequestParam long gameId, Model model) {
		StarbaseOverviewRequest request = new StarbaseOverviewRequest();
		request.setGameId(gameId);
		request.setPageSize(15);
		request.setSortField(SortField.SHIP_OVERVIEW_NAME.name());
		request.setSortAscending(true);

		Page<StarbaseListResultDTO> starbases = starbaseService.getStarbasesOfLogin(request.getGameId(), request, userDetailService.getLoginId());
		model.addAttribute("starbases", starbases);
		model.addAttribute("starbasesOverviewSortFields", starbasesOverviewSortItems);
		return "ingame/starbase-overview::content";
	}

	@PostMapping("/starbase-overview")
	public String updateStarbasesOverview(@RequestBody StarbaseOverviewRequest request, Model model) {
		Page<StarbaseListResultDTO> starbases = starbaseService.getStarbasesOfLogin(request.getGameId(), request, userDetailService.getLoginId());
		model.addAttribute("starbases", starbases);
		return "ingame/starbase-overview::table-content";
	}

	@GetMapping("/ship-overview")
	public String showShips(@RequestParam long gameId, Model model) {
		ShipOverviewRequest request = new ShipOverviewRequest();
		request.setGameId(gameId);
		request.setPageSize(15);
		request.setSortField(SortField.SHIP_OVERVIEW_NAME.name());
		request.setSortAscending(true);

		long loginId = userDetailService.getLoginId();

		Page<ShipListResultDTO> ships = shipService.getShipsOfLogin(gameId, request, loginId);
		model.addAttribute("ships", ships);
		model.addAttribute("fleets", fleetService.getAllFleetsForPlayer(gameId, loginId));
		model.addAttribute("shipsOverviewSortFields", shipsOverviewSortItems);
		addBuiltShipTemplates(model, gameId);
		return "ingame/ship-overview::content";
	}

	@PostMapping("/ship-overview")
	public String updateShipsOverview(@RequestBody ShipOverviewRequest request, Model model) {
		long loginId = userDetailService.getLoginId();

		Page<ShipListResultDTO> ships = shipService.getShipsOfLogin(request.getGameId(), request, loginId);
		model.addAttribute("ships", ships);
		model.addAttribute("fleets", fleetService.getAllFleetsForPlayer(request.getGameId(), loginId));
		addBuiltShipTemplates(model, request.getGameId());
		return "ingame/ship-overview::table-content";
	}

	private void addBuiltShipTemplates(Model model, long gameId) {
		long loginId = userDetailService.getLoginId();
		List<ShipTemplate> shipTemplates = shipService.getBuiltShipTemplates(gameId, loginId);
		model.addAttribute("shipTemplates", shipTemplates);
	}

	@GetMapping("/fleet-overview")
	public String showFleets(@RequestParam long gameId, Model model) {
		FleetOverviewRequest request = new FleetOverviewRequest();
		request.setGameId(gameId);
		request.setPageSize(15);
		request.setSortField(SortField.FLEET_OVERVIEW_NAME.name());
		request.setSortAscending(true);

		Page<FleetListResultDTO> fleets = fleetService.getFleetsOfLogin(request.getGameId(), request, userDetailService.getLoginId());
		model.addAttribute("fleets", fleets);
		model.addAttribute("fleetsOverviewSortFields", fleetsOverviewSortItems);

		long loginId = userDetailService.getLoginId();
		Player player = ingameService.getPlayer(loginId, gameId);
		model.addAttribute("turnDone", player.isTurnFinished());

		return "ingame/fleet-overview::content";
	}

	@PostMapping("/fleet-overview")
	public String updateFleetsOverview(@RequestBody FleetOverviewRequest request, Model model) {
		Page<FleetListResultDTO> fleets = fleetService.getFleetsOfLogin(request.getGameId(), request, userDetailService.getLoginId());
		model.addAttribute("fleets", fleets);

		long loginId = userDetailService.getLoginId();
		Player player = ingameService.getPlayer(loginId, request.getGameId());
		model.addAttribute("turnDone", player.isTurnFinished());

		return "ingame/fleet-overview::table-content";
	}

	@GetMapping("/planet-mouse-over")
	public String getPlanetMouseOver(@RequestParam long planetId, Model model) {
		Planet planet = planetService.getPlanet(planetId);
		model.addAttribute("planet", planet);

		if (configProperties.isDisablePermissionChecks()) {
			model.addAttribute("playerId", 0L);
		} else {
			long playerId = planet.getPlayer().getId();
			model.addAttribute("playerId", playerId);
			Map<Long, PlayerRelationType> relationsMap = getRelationsMap(playerId);
			model.addAttribute("relations", relationsMap);
		}

		Map<Long, Float> tradeBonusData = politicsRoundCalculator.getTradeBonusData(planet.getGame().getId());
		model.addAttribute("tradeBonusData", tradeBonusData);
		model.addAttribute("nativeSpeciesDescription", new NativeSpeciesDescription(messageSource));

		return "ingame/galaxymap/planet-mouseover::content";
	}

	@GetMapping("/planet-mouse-over/scanned")
	public String getScannedPlanetMouseOver(@RequestParam long planetId, Model model) {
		Pair<PlanetEntry, Long> result = planetService.getPlanetEntryForScannedMouseOver(planetId, userDetailService.getLoginId());

		PlanetEntry planet = result.getLeft();
		Long playerId = result.getRight();

		model.addAttribute("planet", planet);

		if (configProperties.isDisablePermissionChecks()) {
			model.addAttribute("playerId", 0L);
		} else {
			model.addAttribute("playerId", playerId);
			Map<Long, PlayerRelationType> relationsMap = getRelationsMap(playerId);
			model.addAttribute("relations", relationsMap);
		}

		return "ingame/galaxymap/planet-mouseover::content";
	}

	@GetMapping("/ship-mouse-over")
	public String getShipMouseOver(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);

		if (configProperties.isDisablePermissionChecks()) {
			model.addAttribute("playerId", 0L);
		} else {
			model.addAttribute("playerId", ship.getPlayer().getId());
		}

		return "ingame/galaxymap/ship-mouseover::content";
	}

	@GetMapping("/ships-mouse-over-item")
	public String getShipsMouseOverItem(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);

		if (configProperties.isDisablePermissionChecks()) {
			model.addAttribute("playerId", 0L);
		} else {
			model.addAttribute("playerId", ship.getPlayer().getId());
		}

		return "ingame/galaxymap/ships-mouseover::ship-item";
	}

	@GetMapping("/starbase-mouse-over")
	public String getStarbaseMouseOver(@RequestParam long starbaseId, Model model) {
		Starbase starbase = starbaseService.getStarbase(starbaseId);
		model.addAttribute("starbaseName", starbase.getName());
		model.addAttribute("starbaseLog", starbase.getLog());
		model.addAttribute("starbaseHullLevel", starbase.getHullLevel());
		model.addAttribute("starbasePropulsionLevel", starbase.getPropulsionLevel());
		model.addAttribute("starbaseEnergyLevel", starbase.getEnergyLevel());
		model.addAttribute("starbaseProjectileLevel", starbase.getProjectileLevel());

		Optional<StarbaseShipConstructionJob> jobOpt = starbaseService.shipConstructionJobExists(starbaseId);

		if (jobOpt.isPresent()) {
			model.addAttribute("starbaseShipConstructionName", jobOpt.get().getShipName());
		}

		return "ingame/galaxymap/starbase-mouseover::content";
	}

	@PostMapping("/zoom-level")
	@ResponseBody
	public void changeIngameZoomLevel(@RequestBody float ingameZoomLevel) {
		ingameService.changeIngameZoomLevel(userDetailService.getLoginId(), ingameZoomLevel);
	}
}
