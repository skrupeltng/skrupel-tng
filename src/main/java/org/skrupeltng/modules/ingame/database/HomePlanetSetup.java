package org.skrupeltng.modules.ingame.database;

public enum HomePlanetSetup {

	NONE, STAR_BASE, WAR_BASE
}