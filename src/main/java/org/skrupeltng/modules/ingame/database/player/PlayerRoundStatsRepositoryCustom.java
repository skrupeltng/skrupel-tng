package org.skrupeltng.modules.ingame.database.player;

public interface PlayerRoundStatsRepositoryCustom {

	void updateStats(long gameId, int round);
}