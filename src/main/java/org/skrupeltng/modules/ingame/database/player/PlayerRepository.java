package org.skrupeltng.modules.ingame.database.player;

import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface PlayerRepository extends JpaRepository<Player, Long>, PlayerRepositoryCustom {

	Optional<Player> findByGameIdAndLoginId(long gameId, long loginId);

	@Query("SELECT p FROM Player p WHERE p.backgroundAIAgent = false AND p.aiLevel IS NOT NULL AND p.game.id = ?1 ORDER BY p.id ASC")
	List<Player> findAIPlayers(long gameId);

	@Query("SELECT p.id FROM Player p WHERE p.login.id = ?1 AND p.game.id = ?2")
	long getIdByLoginIdAndGameId(long loginId, long gameId);

	@Query("SELECT p FROM Player p WHERE p.login.id = ?1 AND p.game.id = ?2")
	Player findByLoginIdAndGameId(long loginId, long gameId);

	@Query("SELECT p FROM Player p WHERE p.backgroundAIAgent = false AND p.game.id = ?1 AND p.hasLost = false")
	List<Player> getNotLostPlayers(long gameId);

	@Query("SELECT p FROM Player p WHERE p.backgroundAIAgent = false AND p.game.id = ?1 AND p.hasLost = true")
	List<Player> getLostPlayers(long gameId);

	@Query("SELECT p FROM Player p WHERE p.backgroundAIAgent = false AND p.game.id = ?1 ORDER BY p.id")
	List<Player> findByGameId(long gameId);

	@Query("SELECT COUNT(p.id) FROM Player p WHERE p.backgroundAIAgent = false AND p.game.id = ?1")
	long getPlayerCount(long id);

	@Query("SELECT p.homePlanet FROM Player p WHERE p.id = ?1")
	Planet getHomePlanet(long playerId);

	@Query("SELECT p.game.id FROM Player p WHERE p.id = ?1")
	long getGameIdByPlayerId(long playerId);

	@Query("SELECT p.color FROM Player p WHERE p.id = ?1")
	String getPlayerColor(long playerId);

	@Query("SELECT p FROM Player p WHERE p.backgroundAIAgent = true AND p.game.id = ?1")
	Player getBackgroundAIAgentPlayer(long gameId);

	@Modifying
	@Query("UPDATE Player p SET p.lastRoundNewColonies = p.lastRoundNewColonies + 1 WHERE p.id = ?1")
	void increaseNewColonies(long playerId);

	@Modifying
	@Query("UPDATE Player p SET p.lastRoundNewShips = p.lastRoundNewShips + 1 WHERE p.id = ?1")
	void increaseNewShips(long playerId);

	@Modifying
	@Query("UPDATE Player p SET p.lastRoundNewStarbases = p.lastRoundNewStarbases + 1 WHERE p.id = ?1")
	void increaseNewStarbases(long playerId);

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Modifying
	@Query("UPDATE Player p SET p.lastRoundDestroyedShips = p.lastRoundDestroyedShips + ?2 WHERE p.id = ?1")
	void increaseDestroyedShips(long playerId, int amount);

	@Modifying
	@Query("UPDATE Player p SET p.lastRoundLostShips = p.lastRoundLostShips + 1 WHERE p.id = ?1")
	void increaseLostShips(long playerId);

	@Modifying
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Query("""
		  	UPDATE 
				Player p 
			SET 
				p.lastRoundNewColonies = 0, 
				p.lastRoundNewShips = 0, 
				p.lastRoundNewStarbases = 0,
				p.lastRoundDestroyedShips = 0, 
				p.lastRoundLostShips = 0 
			WHERE 
				p.game.id = ?1
		""")
	void resetLastRoundCounts(long gameId);

	@Modifying
	@Query("UPDATE Player p SET p.team = null WHERE p.game.id = ?1")
	void clearTeamsFromPlayers(long gameId);
}
