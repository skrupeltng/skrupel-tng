package org.skrupeltng.modules.ingame.database;

import org.skrupeltng.modules.dashboard.PlanetDensity;
import org.skrupeltng.modules.dashboard.ResourceDensity;
import org.skrupeltng.modules.dashboard.ResourceDensityHomePlanet;
import org.skrupeltng.modules.dashboard.database.GalaxyConfig;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.Team;
import org.skrupeltng.modules.dashboard.modules.storymode.database.StoryModeCampaign;
import org.skrupeltng.modules.dashboard.modules.storymode.service.missions.silverstarag.SilverStarAGMission1;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.masterdata.database.Resource;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import java.io.Serial;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "game")
public class Game implements Serializable {

	@Serial
	private static final long serialVersionUID = 7630810870074432707L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private final Instant created;

	@ManyToOne(fetch = FetchType.LAZY)
	private Login creator;

	private String name;

	@Enumerated(EnumType.STRING)
	@Column(name = "win_condition")
	private WinCondition winCondition;

	@Column(name = "use_fixed_teams")
	private boolean useFixedTeams;

	@Enumerated(EnumType.STRING)
	@Column(name = "mineral_race_mineral_name")
	private Resource mineralRaceMineralName;

	@Column(name = "mineral_race_quantity")
	private Integer mineralRaceQuantity;

	@Enumerated(EnumType.STRING)
	@Column(name = "mineral_race_storage_type")
	private MineralRaceStorageType mineralRaceStorageType;

	@Column(name = "invasion_cooperative")
	private boolean invasionCooperative;

	@Enumerated(EnumType.STRING)
	@Column(name = "invasion_difficulty")
	private InvasionDifficulty invasionDifficulty;

	@Column(name = "invasion_wave")
	private int invasionWave;

	@Enumerated(EnumType.STRING)
	@Column(name = "conquest_type")
	private ConquestType conquestType;

	@Column(name = "conquest_rounds")
	private int conquestRounds;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "conquest_planet_id")
	private Planet conquestPlanet;

	private boolean started;

	private boolean finished;

	private int round;

	@Column(name = "is_private")
	private boolean isPrivate;

	@Column(name = "round_date")
	private Instant roundDate;

	@Column(name = "auto_tick_seconds")
	private int autoTickSeconds;

	@Column(name = "turn_not_finished_notification_seconds")
	private int turnNotFinishedNotificationSeconds;

	@Column(name = "last_to_finish_turn_notification_seconds")
	private int lastToFinishTurnNotificationSeconds;

	@Column(name = "turn_not_finished_notifications_sent")
	private boolean turnNotFinishedNotificationsSent;

	@Column(name = "last_to_finish_turn_notifications_sent")
	private boolean lastToFinishTurnNotificationsSent;

	@Column(name = "enable_espionage")
	private boolean enableEspionage;

	@Column(name = "enable_mine_fields")
	private boolean enableMineFields;

	@Column(name = "enable_tactial_combat")
	private boolean enableTactialCombat;

	@Column(name = "enable_wysiwyg")
	private boolean enableWysiwyg;

	@Enumerated(EnumType.STRING)
	@Column(name = "start_position_setup")
	private StartPositionSetup startPositionSetup;

	@Enumerated(EnumType.STRING)
	@Column(name = "home_planet_setup")
	private HomePlanetSetup homePlanetSetup;

	@Column(name = "randomize_home_planet_names")
	private boolean randomizeHomePlanetNames;

	@Enumerated(EnumType.STRING)
	@Column(name = "lose_condition")
	private LoseCondition loseCondition;

	@Column(name = "player_count")
	private int playerCount;

	@Column(name = "galaxy_size")
	private int galaxySize;

	@Column(name = "init_money")
	private int initMoney;

	@Enumerated(EnumType.STRING)
	@Column(name = "planet_density")
	private PlanetDensity planetDensity;

	@Enumerated(EnumType.STRING)
	@Column(name = "resource_density")
	private ResourceDensity resourceDensity;

	@Enumerated(EnumType.STRING)
	@Column(name = "resource_density_home_planet")
	private ResourceDensityHomePlanet resourceDensityHomePlanet;

	@Column(name = "inhabited_planets_percentage")
	private float inhabitedPlanetsPercentage;

	@Column(name = "unstable_wormhole_count")
	private int unstableWormholeCount;

	@Enumerated(EnumType.STRING)
	@Column(name = "stable_wormhole_config")
	private StableWormholeConfig stableWormholeConfig = StableWormholeConfig.NONE;

	@Column(name = "max_concurrent_plasma_storm_count")
	private int maxConcurrentPlasmaStormCount;

	@Column(name = "plasma_storm_probability")
	private float plasmaStormProbability;

	@Column(name = "plasma_storm_rounds")
	private int plasmaStormRounds;

	@Enumerated(EnumType.STRING)
	@Column(name = "fog_of_war_type")
	private FogOfWarType fogOfWarType;

	@Column(name = "pirate_attack_probability_center")
	private float pirateAttackProbabilityCenter;

	@Column(name = "pirate_attack_probability_outskirts")
	private float pirateAttackProbabilityOutskirts;

	@Column(name = "pirate_min_loot")
	private float pirateMinLoot;

	@Column(name = "pirate_max_loot")
	private float pirateMaxLoot;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "galaxy_config_id")
	private GalaxyConfig galaxyConfig;

	@Enumerated(EnumType.STRING)
	@Column(name = "finished_turn_visibility_type")
	private FinishedTurnVisibilityType finishedTurnVisibilityType;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "story_mode_campaign_id")
	private StoryModeCampaign storyModeCampaign;

	@Column(name = "story_mode_mission")
	private int storyModeMission;

	@Column(name = "story_mode_mission_step")
	private int storyModeMissionStep;

	@Column(name = "story_mode_mission_data")
	private String storyModeMissionData;

	@Column(name = "story_mode_mission_step_seen")
	private boolean storyModeMissionStepSeen;

	@Column(name = "match_making_game")
	private boolean matchMakingGame;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "game")
	private List<Player> players;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "game")
	private Set<Planet> planets;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "game")
	private List<Team> teams;

	public Game() {
		created = Instant.now();
		roundDate = created;
		round = 1;
	}

	public Game(long id) {
		this();
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Login getCreator() {
		return creator;
	}

	public void setCreator(Login creator) {
		this.creator = creator;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public WinCondition getWinCondition() {
		return winCondition;
	}

	public void setWinCondition(WinCondition winCondition) {
		this.winCondition = winCondition;
	}

	public boolean isUseFixedTeams() {
		return useFixedTeams;
	}

	public void setUseFixedTeams(boolean useFixedTeams) {
		this.useFixedTeams = useFixedTeams;
	}

	public Resource getMineralRaceMineralName() {
		return mineralRaceMineralName;
	}

	public void setMineralRaceMineralName(Resource mineralRaceMineralName) {
		this.mineralRaceMineralName = mineralRaceMineralName;
	}

	public Integer getMineralRaceQuantity() {
		return mineralRaceQuantity;
	}

	public void setMineralRaceQuantity(Integer mineralRaceQuantity) {
		this.mineralRaceQuantity = mineralRaceQuantity;
	}

	public MineralRaceStorageType getMineralRaceStorageType() {
		return mineralRaceStorageType;
	}

	public void setMineralRaceStorageType(MineralRaceStorageType mineralRaceStorageType) {
		this.mineralRaceStorageType = mineralRaceStorageType;
	}

	public boolean isInvasionCooperative() {
		return invasionCooperative;
	}

	public void setInvasionCooperative(boolean invasionCooperative) {
		this.invasionCooperative = invasionCooperative;
	}

	public InvasionDifficulty getInvasionDifficulty() {
		return invasionDifficulty;
	}

	public void setInvasionDifficulty(InvasionDifficulty invasionDifficulty) {
		this.invasionDifficulty = invasionDifficulty;
	}

	public int getInvasionWave() {
		return invasionWave;
	}

	public void setInvasionWave(int invasionWave) {
		this.invasionWave = invasionWave;
	}

	public ConquestType getConquestType() {
		return conquestType;
	}

	public void setConquestType(ConquestType conquestType) {
		this.conquestType = conquestType;
	}

	public int getConquestRounds() {
		return conquestRounds;
	}

	public void setConquestRounds(int conquestRounds) {
		this.conquestRounds = conquestRounds;
	}

	public Planet getConquestPlanet() {
		return conquestPlanet;
	}

	public void setConquestPlanet(Planet conquestPlanet) {
		this.conquestPlanet = conquestPlanet;
	}

	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public boolean isPrivate() {
		return isPrivate;
	}

	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	public Instant getRoundDate() {
		return roundDate;
	}

	public void setRoundDate(Instant roundDate) {
		this.roundDate = roundDate;
	}

	public int getAutoTickSeconds() {
		return autoTickSeconds;
	}

	public void setAutoTickSeconds(int autoTickSeconds) {
		this.autoTickSeconds = autoTickSeconds;
	}

	public int getTurnNotFinishedNotificationSeconds() {
		return turnNotFinishedNotificationSeconds;
	}

	public void setTurnNotFinishedNotificationSeconds(int turnNotFinishedNotificationSeconds) {
		this.turnNotFinishedNotificationSeconds = turnNotFinishedNotificationSeconds;
	}

	public int getLastToFinishTurnNotificationSeconds() {
		return lastToFinishTurnNotificationSeconds;
	}

	public void setLastToFinishTurnNotificationSeconds(int lastToFinishTurnNotificationSeconds) {
		this.lastToFinishTurnNotificationSeconds = lastToFinishTurnNotificationSeconds;
	}

	public boolean isTurnNotFinishedNotificationsSent() {
		return turnNotFinishedNotificationsSent;
	}

	public void setTurnNotFinishedNotificationsSent(boolean turnNotFinishedNotificationsSent) {
		this.turnNotFinishedNotificationsSent = turnNotFinishedNotificationsSent;
	}

	public boolean isLastToFinishTurnNotificationsSent() {
		return lastToFinishTurnNotificationsSent;
	}

	public void setLastToFinishTurnNotificationsSent(boolean lastToFinishTurnNotificationsSent) {
		this.lastToFinishTurnNotificationsSent = lastToFinishTurnNotificationsSent;
	}

	public boolean isEnableEspionage() {
		return enableEspionage;
	}

	public void setEnableEspionage(boolean enableEspionage) {
		this.enableEspionage = enableEspionage;
	}

	public boolean isEnableMineFields() {
		return enableMineFields;
	}

	public void setEnableMineFields(boolean enableMineFields) {
		this.enableMineFields = enableMineFields;
	}

	public boolean isEnableTactialCombat() {
		return enableTactialCombat;
	}

	public void setEnableTactialCombat(boolean enableTactialCombat) {
		this.enableTactialCombat = enableTactialCombat;
	}

	public boolean isEnableWysiwyg() {
		return enableWysiwyg;
	}

	public void setEnableWysiwyg(boolean enableWysiwyg) {
		this.enableWysiwyg = enableWysiwyg;
	}

	public StartPositionSetup getStartPositionSetup() {
		return startPositionSetup;
	}

	public void setStartPositionSetup(StartPositionSetup startPositionSetup) {
		this.startPositionSetup = startPositionSetup;
	}

	public HomePlanetSetup getHomePlanetSetup() {
		return homePlanetSetup;
	}

	public void setHomePlanetSetup(HomePlanetSetup homePlanetSetup) {
		this.homePlanetSetup = homePlanetSetup;
	}

	public LoseCondition getLoseCondition() {
		return loseCondition;
	}

	public void setLoseCondition(LoseCondition loseCondition) {
		this.loseCondition = loseCondition;
	}

	public int getPlayerCount() {
		return playerCount;
	}

	public void setPlayerCount(int playerCount) {
		this.playerCount = playerCount;
	}

	public int getGalaxySize() {
		return galaxySize;
	}

	public void setGalaxySize(int galaxySize) {
		this.galaxySize = galaxySize;
	}

	public int getInitMoney() {
		return initMoney;
	}

	public void setInitMoney(int initMoney) {
		this.initMoney = initMoney;
	}

	public PlanetDensity getPlanetDensity() {
		return planetDensity;
	}

	public void setPlanetDensity(PlanetDensity planetDensity) {
		this.planetDensity = planetDensity;
	}

	public ResourceDensity getResourceDensity() {
		return resourceDensity;
	}

	public void setResourceDensity(ResourceDensity resourceDensity) {
		this.resourceDensity = resourceDensity;
	}

	public ResourceDensityHomePlanet getResourceDensityHomePlanet() {
		return resourceDensityHomePlanet;
	}

	public void setResourceDensityHomePlanet(ResourceDensityHomePlanet resourceDensityHomePlanet) {
		this.resourceDensityHomePlanet = resourceDensityHomePlanet;
	}

	public float getInhabitedPlanetsPercentage() {
		return inhabitedPlanetsPercentage;
	}

	public void setInhabitedPlanetsPercentage(float inhabitedPlanetsPercentage) {
		this.inhabitedPlanetsPercentage = inhabitedPlanetsPercentage;
	}

	public int getUnstableWormholeCount() {
		return unstableWormholeCount;
	}

	public void setUnstableWormholeCount(int unstableWormholeCount) {
		this.unstableWormholeCount = unstableWormholeCount;
	}

	public StableWormholeConfig getStableWormholeConfig() {
		return stableWormholeConfig;
	}

	public void setStableWormholeConfig(StableWormholeConfig stableWormholeConfig) {
		this.stableWormholeConfig = stableWormholeConfig;
	}

	public int getMaxConcurrentPlasmaStormCount() {
		return maxConcurrentPlasmaStormCount;
	}

	public void setMaxConcurrentPlasmaStormCount(int maxConcurrentPlasmaStormCount) {
		this.maxConcurrentPlasmaStormCount = maxConcurrentPlasmaStormCount;
	}

	public float getPlasmaStormProbability() {
		return plasmaStormProbability;
	}

	public void setPlasmaStormProbability(float plasmaStormProbability) {
		this.plasmaStormProbability = plasmaStormProbability;
	}

	public int getPlasmaStormRounds() {
		return plasmaStormRounds;
	}

	public void setPlasmaStormRounds(int plasmaStormRounds) {
		this.plasmaStormRounds = plasmaStormRounds;
	}

	public FogOfWarType getFogOfWarType() {
		return fogOfWarType;
	}

	public void setFogOfWarType(FogOfWarType fogOfWarType) {
		this.fogOfWarType = fogOfWarType;
	}

	public float getPirateAttackProbabilityCenter() {
		return pirateAttackProbabilityCenter;
	}

	public void setPirateAttackProbabilityCenter(float pirateAttackProbabilityCenter) {
		this.pirateAttackProbabilityCenter = pirateAttackProbabilityCenter;
	}

	public float getPirateAttackProbabilityOutskirts() {
		return pirateAttackProbabilityOutskirts;
	}

	public void setPirateAttackProbabilityOutskirts(float pirateAttackProbabilityOutskirts) {
		this.pirateAttackProbabilityOutskirts = pirateAttackProbabilityOutskirts;
	}

	public float getPirateMinLoot() {
		return pirateMinLoot;
	}

	public void setPirateMinLoot(float pirateMinLoot) {
		this.pirateMinLoot = pirateMinLoot;
	}

	public float getPirateMaxLoot() {
		return pirateMaxLoot;
	}

	public void setPirateMaxLoot(float pirateMaxLoot) {
		this.pirateMaxLoot = pirateMaxLoot;
	}

	public boolean isRandomizeHomePlanetNames() {
		return randomizeHomePlanetNames;
	}

	public void setRandomizeHomePlanetNames(boolean randomizeHomePlanetNames) {
		this.randomizeHomePlanetNames = randomizeHomePlanetNames;
	}

	public GalaxyConfig getGalaxyConfig() {
		return galaxyConfig;
	}

	public void setGalaxyConfig(GalaxyConfig galaxyConfig) {
		this.galaxyConfig = galaxyConfig;
	}

	public FinishedTurnVisibilityType getFinishedTurnVisibilityType() {
		return finishedTurnVisibilityType;
	}

	public void setFinishedTurnVisibilityType(FinishedTurnVisibilityType finishedTurnVisibilityType) {
		this.finishedTurnVisibilityType = finishedTurnVisibilityType;
	}

	public StoryModeCampaign getStoryModeCampaign() {
		return storyModeCampaign;
	}

	public void setStoryModeCampaign(StoryModeCampaign storyModeCampaign) {
		this.storyModeCampaign = storyModeCampaign;
	}

	public int getStoryModeMission() {
		return storyModeMission;
	}

	public void setStoryModeMission(int storyModeMission) {
		this.storyModeMission = storyModeMission;
	}

	public int getStoryModeMissionStep() {
		return storyModeMissionStep;
	}

	public void setStoryModeMissionStep(int storyModeMissionStep) {
		this.storyModeMissionStep = storyModeMissionStep;
	}

	public String getStoryModeMissionData() {
		return storyModeMissionData;
	}

	public void setStoryModeMissionData(String storyModeMissionData) {
		this.storyModeMissionData = storyModeMissionData;
	}

	public boolean isStoryModeMissionStepSeen() {
		return storyModeMissionStepSeen;
	}

	public void setStoryModeMissionStepSeen(boolean storyModeMissionStepSeen) {
		this.storyModeMissionStepSeen = storyModeMissionStepSeen;
	}

	public boolean isMatchMakingGame() {
		return matchMakingGame;
	}

	public void setMatchMakingGame(boolean matchMakingGame) {
		this.matchMakingGame = matchMakingGame;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public Set<Planet> getPlanets() {
		return planets;
	}

	public void setPlanets(Set<Planet> planets) {
		this.planets = planets;
	}

	public List<Team> getTeams() {
		return teams;
	}

	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}

	public Instant getCreated() {
		return created;
	}

	public boolean notTutorialMission() {
		return storyModeCampaign == null || !storyModeCampaign.getFaction().getId().equals("silverstarag") || storyModeMission != 1;
	}

	public boolean starbaseSelectable() {
		return notTutorialMission() || storyModeMissionStep >= SilverStarAGMission1.STEP_4_UPGRADE_STARBASE;
	}
}
