package org.skrupeltng.modules.dashboard;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import org.skrupeltng.modules.ingame.database.ConquestType;
import org.skrupeltng.modules.ingame.database.FinishedTurnVisibilityType;
import org.skrupeltng.modules.ingame.database.FogOfWarType;
import org.skrupeltng.modules.ingame.database.HomePlanetSetup;
import org.skrupeltng.modules.ingame.database.InvasionDifficulty;
import org.skrupeltng.modules.ingame.database.LoseCondition;
import org.skrupeltng.modules.ingame.database.MineralRaceStorageType;
import org.skrupeltng.modules.ingame.database.StableWormholeConfig;
import org.skrupeltng.modules.ingame.database.StartPositionSetup;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.masterdata.database.Resource;

public class NewGameRequest {

	@NotNull
	@Min(1)
	@Max(64)
	private int playerCount;

	@NotNull
	@Min(250)
	@Max(16384)
	private int galaxySize;

	@NotNull
	private String planetDensity;

	@NotNull
	private String resourceDensity;

	@NotNull
	private String galaxyConfigId;

	@NotNull
	@NotEmpty
	private String name;

	@NotNull
	private AutoTickItem autoTickItem;

	@NotNull
	private TurnNotificationItem turnNotFinishedItem;

	@NotNull
	private TurnNotificationItem lastToFinishTurnItem;

	@NotNull
	private String winCondition;

	private boolean useFixedTeams;

	private String mineralRaceMineralName;

	@Min(1000)
	@Max(100000)
	private Integer mineralRaceQuantity;

	private String mineralRaceStorageType;

	private boolean invasionCooperative;

	private InvasionDifficulty invasionDifficulty;

	private ConquestType conquestType;

	private int conquestRounds;

	private boolean enableEspionage;

	private boolean enableMineFields;

	private boolean enableTactialCombat;

	private boolean enableWysiwyg;

	@NotNull
	private String startPositionSetup;

	@NotNull
	private String homePlanetSetup;

	private boolean randomizeHomePlanetNames;

	@NotNull
	private String loseCondition;

	@NotNull
	@Min(1)
	@Max(9999999)
	private int initMoney;

	@NotNull
	private String resourceDensityHomePlanet;

	@NotNull
	@Min(0)
	@Max(1)
	private float inhabitedPlanetsPercentage;

	@NotNull
	@Min(0)
	@Max(50)
	private int unstableWormholeCount;

	@NotNull
	private String stableWormholeConfig;

	@NotNull
	@Min(0)
	@Max(10)
	private int maxConcurrentPlasmaStormCount;

	@NotNull
	@Min(0)
	@Max(100)
	private int plasmaStormProbability;

	@NotNull
	@Min(3)
	@Max(20)
	private int plasmaStormRounds;

	@NotNull
	private String fogOfWarType;

	@NotNull
	@Min(0)
	@Max(100)
	private int pirateAttackProbabilityCenter;

	@NotNull
	@Min(0)
	@Max(100)
	private int pirateAttackProbabilityOutskirts;

	@NotNull
	@Min(0)
	@Max(100)
	private int pirateMinLoot;

	@NotNull
	@Min(0)
	@Max(100)
	private int pirateMaxLoot;

	private boolean isPrivate;

	private FinishedTurnVisibilityType finishedTurnVisibilityType;

	public int getPlayerCount() {
		return playerCount;
	}

	public void setPlayerCount(int playerCount) {
		this.playerCount = playerCount;
	}

	public int getGalaxySize() {
		return galaxySize;
	}

	public void setGalaxySize(int galaxySize) {
		this.galaxySize = galaxySize;
	}

	public String getPlanetDensity() {
		return planetDensity;
	}

	public void setPlanetDensity(String planetDensity) {
		this.planetDensity = planetDensity;
	}

	public String getResourceDensity() {
		return resourceDensity;
	}

	public void setResourceDensity(String resourceDensity) {
		this.resourceDensity = resourceDensity;
	}

	public String getGalaxyConfigId() {
		return galaxyConfigId;
	}

	public void setGalaxyConfigId(String galaxyConfigId) {
		this.galaxyConfigId = galaxyConfigId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AutoTickItem getAutoTickItem() {
		return autoTickItem;
	}

	public void setAutoTickItem(AutoTickItem autoTickItem) {
		this.autoTickItem = autoTickItem;
	}

	public TurnNotificationItem getTurnNotFinishedItem() {
		return turnNotFinishedItem;
	}

	public void setTurnNotFinishedItem(TurnNotificationItem turnNotFinishedItem) {
		this.turnNotFinishedItem = turnNotFinishedItem;
	}

	public TurnNotificationItem getLastToFinishTurnItem() {
		return lastToFinishTurnItem;
	}

	public void setLastToFinishTurnItem(TurnNotificationItem lastToFinishTurnItem) {
		this.lastToFinishTurnItem = lastToFinishTurnItem;
	}

	public String getWinCondition() {
		return winCondition;
	}

	public void setWinCondition(String winCondition) {
		this.winCondition = winCondition;
	}

	public boolean isUseFixedTeams() {
		return useFixedTeams;
	}

	public void setUseFixedTeams(boolean useFixedTeams) {
		this.useFixedTeams = useFixedTeams;
	}

	public String getMineralRaceMineralName() {
		return mineralRaceMineralName;
	}

	public void setMineralRaceMineralName(String mineralRaceMineralName) {
		this.mineralRaceMineralName = mineralRaceMineralName;
	}

	public Integer getMineralRaceQuantity() {
		return mineralRaceQuantity;
	}

	public void setMineralRaceQuantity(Integer mineralRaceQuantity) {
		this.mineralRaceQuantity = mineralRaceQuantity;
	}

	public String getMineralRaceStorageType() {
		return mineralRaceStorageType;
	}

	public void setMineralRaceStorageType(String mineralRaceStorageType) {
		this.mineralRaceStorageType = mineralRaceStorageType;
	}

	public boolean isInvasionCooperative() {
		return invasionCooperative;
	}

	public void setInvasionCooperative(boolean invasionCooperative) {
		this.invasionCooperative = invasionCooperative;
	}

	public InvasionDifficulty getInvasionDifficulty() {
		return invasionDifficulty;
	}

	public void setInvasionDifficulty(InvasionDifficulty invasionDifficulty) {
		this.invasionDifficulty = invasionDifficulty;
	}

	public ConquestType getConquestType() {
		return conquestType;
	}

	public void setConquestType(ConquestType conquestType) {
		this.conquestType = conquestType;
	}

	public int getConquestRounds() {
		return conquestRounds;
	}

	public void setConquestRounds(int conquestRounds) {
		this.conquestRounds = conquestRounds;
	}

	public boolean isEnableEspionage() {
		return enableEspionage;
	}

	public void setEnableEspionage(boolean enableEspionage) {
		this.enableEspionage = enableEspionage;
	}

	public boolean isEnableMineFields() {
		return enableMineFields;
	}

	public void setEnableMineFields(boolean enableMineFields) {
		this.enableMineFields = enableMineFields;
	}

	public boolean isEnableTactialCombat() {
		return enableTactialCombat;
	}

	public void setEnableTactialCombat(boolean enableTactialCombat) {
		this.enableTactialCombat = enableTactialCombat;
	}

	public boolean isEnableWysiwyg() {
		return enableWysiwyg;
	}

	public void setEnableWysiwyg(boolean enableWysiwyg) {
		this.enableWysiwyg = enableWysiwyg;
	}

	public String getStartPositionSetup() {
		return startPositionSetup;
	}

	public void setStartPositionSetup(String startPositionSetup) {
		this.startPositionSetup = startPositionSetup;
	}

	public String getHomePlanetSetup() {
		return homePlanetSetup;
	}

	public void setHomePlanetSetup(String homePlanetSetup) {
		this.homePlanetSetup = homePlanetSetup;
	}

	public boolean isRandomizeHomePlanetNames() {
		return randomizeHomePlanetNames;
	}

	public void setRandomizeHomePlanetNames(boolean randomizeHomePlanetNames) {
		this.randomizeHomePlanetNames = randomizeHomePlanetNames;
	}

	public String getLoseCondition() {
		return loseCondition;
	}

	public void setLoseCondition(String loseCondition) {
		this.loseCondition = loseCondition;
	}

	public int getInitMoney() {
		return initMoney;
	}

	public void setInitMoney(int initMoney) {
		this.initMoney = initMoney;
	}

	public String getResourceDensityHomePlanet() {
		return resourceDensityHomePlanet;
	}

	public void setResourceDensityHomePlanet(String resourceDensityHomePlanet) {
		this.resourceDensityHomePlanet = resourceDensityHomePlanet;
	}

	public float getInhabitedPlanetsPercentage() {
		return inhabitedPlanetsPercentage;
	}

	public void setInhabitedPlanetsPercentage(float inhabitedPlanetsPercentage) {
		this.inhabitedPlanetsPercentage = inhabitedPlanetsPercentage;
	}

	public int getUnstableWormholeCount() {
		return unstableWormholeCount;
	}

	public void setUnstableWormholeCount(int unstableWormholeCount) {
		this.unstableWormholeCount = unstableWormholeCount;
	}

	public String getStableWormholeConfig() {
		return stableWormholeConfig;
	}

	public void setStableWormholeConfig(String stableWormholeConfig) {
		this.stableWormholeConfig = stableWormholeConfig;
	}

	public int getMaxConcurrentPlasmaStormCount() {
		return maxConcurrentPlasmaStormCount;
	}

	public void setMaxConcurrentPlasmaStormCount(int maxConcurrentPlasmaStormCount) {
		this.maxConcurrentPlasmaStormCount = maxConcurrentPlasmaStormCount;
	}

	public int getPlasmaStormProbability() {
		return plasmaStormProbability;
	}

	public void setPlasmaStormProbability(int plasmaStormProbability) {
		this.plasmaStormProbability = plasmaStormProbability;
	}

	public int getPlasmaStormRounds() {
		return plasmaStormRounds;
	}

	public void setPlasmaStormRounds(int plasmaStormRounds) {
		this.plasmaStormRounds = plasmaStormRounds;
	}

	public String getFogOfWarType() {
		return fogOfWarType;
	}

	public void setFogOfWarType(String fogOfWarType) {
		this.fogOfWarType = fogOfWarType;
	}

	public int getPirateAttackProbabilityCenter() {
		return pirateAttackProbabilityCenter;
	}

	public void setPirateAttackProbabilityCenter(int pirateAttackProbabilityCenter) {
		this.pirateAttackProbabilityCenter = pirateAttackProbabilityCenter;
	}

	public int getPirateAttackProbabilityOutskirts() {
		return pirateAttackProbabilityOutskirts;
	}

	public void setPirateAttackProbabilityOutskirts(int pirateAttackProbabilityOutskirts) {
		this.pirateAttackProbabilityOutskirts = pirateAttackProbabilityOutskirts;
	}

	public int getPirateMinLoot() {
		return pirateMinLoot;
	}

	public void setPirateMinLoot(int pirateMinLoot) {
		this.pirateMinLoot = pirateMinLoot;
	}

	public int getPirateMaxLoot() {
		return pirateMaxLoot;
	}

	public void setPirateMaxLoot(int pirateMaxLoot) {
		this.pirateMaxLoot = pirateMaxLoot;
	}

	public boolean isPrivate() {
		return isPrivate;
	}

	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	public FinishedTurnVisibilityType getFinishedTurnVisibilityType() {
		return finishedTurnVisibilityType;
	}

	public void setFinishedTurnVisibilityType(FinishedTurnVisibilityType finishedTurnVisibilityType) {
		this.finishedTurnVisibilityType = finishedTurnVisibilityType;
	}

	public static NewGameRequest createDefaultRequest() {
		NewGameRequest request = new NewGameRequest();
		request.setEnableEspionage(false);
		request.setEnableMineFields(false);
		request.setEnableTactialCombat(true);
		request.setEnableWysiwyg(false);
		request.setGalaxyConfigId("gala_0");
		request.setGalaxySize(1000);
		request.setHomePlanetSetup(HomePlanetSetup.STAR_BASE.name());
		request.setResourceDensityHomePlanet(ResourceDensityHomePlanet.HIGH.name());
		request.setInitMoney(7500);
		request.setLoseCondition(LoseCondition.LOSE_HOME_PLANET.name());
		request.setPlanetDensity(PlanetDensity.VERY_HIGH.name());
		request.setPlayerCount(2);
		request.setResourceDensity(ResourceDensity.HIGH.name());
		request.setStartPositionSetup(StartPositionSetup.CIRCLE_AROUND_CENTER.name());
		request.setHomePlanetSetup(HomePlanetSetup.STAR_BASE.name());
		request.setWinCondition(WinCondition.SURVIVE.name());
		request.setFogOfWarType(FogOfWarType.LONG_RANGE_SENSORS.name());
		request.setInhabitedPlanetsPercentage(0.25f);
		request.setPlasmaStormRounds(3);
		request.setPirateMinLoot(1);
		request.setPirateMaxLoot(2);
		request.setTurnNotFinishedItem(TurnNotificationItem.EIGHT_HOURS);
		request.setLastToFinishTurnItem(TurnNotificationItem.FOUR_HOURS);
		request.setFinishedTurnVisibilityType(FinishedTurnVisibilityType.NONE);
		request.setInvasionCooperative(true);
		request.setConquestType(ConquestType.REGION);
		request.setConquestRounds(12);
		request.setStableWormholeConfig(StableWormholeConfig.NONE.name());
		request.setAutoTickItem(AutoTickItem.NEVER);
		request.setMineralRaceQuantity(10000);
		request.setMineralRaceMineralName(Resource.FUEL.name());
		request.setMineralRaceStorageType(MineralRaceStorageType.HOME_PLANET.name());
		request.setInvasionDifficulty(InvasionDifficulty.EASY);
		return request;
	}
}
