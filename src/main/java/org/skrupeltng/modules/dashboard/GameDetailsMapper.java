package org.skrupeltng.modules.dashboard;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;
import org.skrupeltng.modules.ingame.database.Game;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GameDetailsMapper {

	@Mapping(source = "creator.username", target = "creator")
	@Mapping(source = "creator.id", target = "creatorId")
	@Mapping(source = "autoTickSeconds", target = "autoTickItem")
	@Mapping(source = "turnNotFinishedNotificationSeconds", target = "turnNotFinishedItem")
	@Mapping(source = "lastToFinishTurnNotificationSeconds", target = "lastToFinishTurnItem")
	GameDetails newGameToGameDetails(Game request);

	default AutoTickItem newAutoTickItem(int seconds) {
		return switch (seconds) {
			case 21600 -> AutoTickItem.SIX_HOURS;
			case 43200 -> AutoTickItem.TWELVE_HOURS;
			case 64800 -> AutoTickItem.EIGHTTEEN_HOURS;
			case 86400 -> AutoTickItem.TWENTYFOUR_HOURS;
			case 172800 -> AutoTickItem.FORTYEIGHT_HOURS;
			default -> AutoTickItem.NEVER;
		};
	}

	default TurnNotificationItem newTurnNotificationItem(int seconds) {
		return switch (seconds) {
			case 7200 -> TurnNotificationItem.TWO_HOURS;
			case 14400 -> TurnNotificationItem.FOUR_HOURS;
			case 28800 -> TurnNotificationItem.EIGHT_HOURS;
			case 57600 -> TurnNotificationItem.SIXTEEN_HOURS;
			case 86400 -> TurnNotificationItem.TWENTYFOUR_HOURS;
			default -> TurnNotificationItem.NEVER;
		};
	}

	@Mapping(source = "creator", target = "creator", ignore = true)
	@Mapping(source = "id", target = "id", ignore = true)
	@Mapping(source = "started", target = "started", ignore = true)
	@Mapping(source = "finished", target = "finished", ignore = true)
	@Mapping(source = "autoTickItem.seconds", target = "autoTickSeconds")
	@Mapping(source = "turnNotFinishedItem.seconds", target = "turnNotFinishedNotificationSeconds")
	@Mapping(source = "lastToFinishTurnItem.seconds", target = "lastToFinishTurnNotificationSeconds")
	void updateGame(GameDetails details, @MappingTarget Game game);

	@Mapping(source = "autoTickItem.seconds", target = "autoTickSeconds")
	@Mapping(source = "turnNotFinishedItem.seconds", target = "turnNotFinishedNotificationSeconds")
	@Mapping(source = "lastToFinishTurnItem.seconds", target = "lastToFinishTurnNotificationSeconds")
	Game newGameRequestToGame(NewGameRequest request);
}
