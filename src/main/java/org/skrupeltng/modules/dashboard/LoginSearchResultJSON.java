package org.skrupeltng.modules.dashboard;

import java.io.Serializable;
import java.util.Objects;

public class LoginSearchResultJSON implements Serializable, Comparable<LoginSearchResultJSON> {

	private static final long serialVersionUID = 1343093528299157442L;

	private long id;
	private String playerName;

	public LoginSearchResultJSON() {

	}

	public LoginSearchResultJSON(long id, String playerName) {
		this.id = id;
		this.playerName = playerName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, playerName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof LoginSearchResultJSON)) {
			return false;
		}
		LoginSearchResultJSON other = (LoginSearchResultJSON)obj;
		return id == other.id && Objects.equals(playerName, other.playerName);
	}

	@Override
	public int compareTo(LoginSearchResultJSON o) {
		return Long.compare(id, o.id);
	}
}