package org.skrupeltng.modules.dashboard.controller;

import java.io.Serializable;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class PasswordRecoveryRequest implements Serializable {

	private static final long serialVersionUID = -3594187811631219905L;

	@NotNull
	@NotBlank
	@Email
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
