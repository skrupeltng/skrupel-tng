package org.skrupeltng.modules.dashboard.controller;

import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerDeathFoe;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class WinnerTextGenerator {

	private final MessageSource messageSource;
	private final WinCondition winCondition;
	private final List<Player> winners;
	private final List<Player> lostAllies;

	private final Locale locale;

	private final List<Object> args = new ArrayList<>();

	private String gameFinishedTextPrefix = "game_finished_text_";

	public WinnerTextGenerator(WinCondition winCondition, MessageSource messageSource, List<Player> winners, List<Player> lostAllies) {
		this.winCondition = winCondition;
		this.messageSource = messageSource;
		this.winners = winners;
		this.lostAllies = lostAllies;

		locale = LocaleContextHolder.getLocale();
	}

	public String createWinnerText() {
		if (winners.isEmpty()) {
			if (winCondition == WinCondition.SURVIVE) {
				return messageSource.getMessage("no_one_survived", null, locale);
			}
			if (winCondition == WinCondition.DEATH_FOE) {
				return messageSource.getMessage("mutually_destroyed_death_foes", null, locale);
			}
			if (winCondition == WinCondition.INVASION) {
				return messageSource.getMessage("game_finished_text_none_INVASION", null, locale);
			}
		}

		String lostAlliesText = createLostAlliesText();

		calculatePrefixAndArgs();

		Object[] arguments = args.toArray();
		String winnerText = messageSource.getMessage(gameFinishedTextPrefix + winCondition.name(), arguments, locale);

		if (lostAlliesText != null) {
			winnerText += " " + lostAlliesText;
		}

		return winnerText;
	}

	private void calculatePrefixAndArgs() {
		if (winners.size() == 1) {
			gameFinishedTextPrefix += "single_";

			args.add(getPlayerNameWithColor(winners.get(0)));

			if (winCondition == WinCondition.DEATH_FOE) {
				List<Player> playersDeathFoes = winners.get(0).getDeathFoes().stream().map(PlayerDeathFoe::getDeathFoe).toList();

				args.add(getPlayerNameWithColor(playersDeathFoes.get(0)));
			}
		} else {
			gameFinishedTextPrefix += "multiple_";
			args.add(joinPlayerNames(winners));
		}
	}

	protected String createLostAlliesText() {
		if (lostAllies.size() == 1) {
			return messageSource.getMessage("lost_ally", new Object[]{getPlayerNameWithColor(lostAllies.get(0))}, locale);
		} else if (lostAllies.size() > 1) {
			return messageSource.getMessage("lost_allies", new Object[]{joinPlayerNames(lostAllies)}, locale);
		}

		return null;
	}

	protected String joinPlayerNames(List<Player> players) {
		if (players.size() >= 2) {
			players = new ArrayList<>(players);

			Player last = players.remove(players.size() - 1);
			String playerNames = players.stream().sorted().map(this::getPlayerNameWithColor).collect(Collectors.joining(", "));
			String and = messageSource.getMessage("and", null, locale);
			String playerNameWithColor = getPlayerNameWithColor(last);
			return "%s %s %s".formatted(playerNames, and, playerNameWithColor);
		}

		return getPlayerNameWithColor(players.get(0));
	}

	protected String getPlayerNameWithColor(Player player) {
		return player.retrieveDisplayNameWithColor(messageSource);
	}
}
