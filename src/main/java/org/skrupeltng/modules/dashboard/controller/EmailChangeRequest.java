package org.skrupeltng.modules.dashboard.controller;

import java.io.Serial;
import java.io.Serializable;

import org.skrupeltng.modules.dashboard.database.Login;

public class EmailChangeRequest implements Serializable {

	@Serial
	private static final long serialVersionUID = -7468737030492274304L;

	private String email;
	private boolean roundNotificationsEnabled;
	private boolean joinNotificationsEnabled;
	private boolean gameFullNotificationsEnabled;
	private boolean invitedToGameNotificationsEnabled;
	private boolean notFinishTurnNotificationsEnabled;
	private boolean lastToFinishTurnNotificationsEnabled;
	private boolean matchMakingGameCreatedNotificationsEnabled;

	public EmailChangeRequest() {

	}

	public EmailChangeRequest(Login login) {
		email = login.getEmail();
		roundNotificationsEnabled = login.isRoundNotificationsEnabled();
		joinNotificationsEnabled = login.isJoinNotificationsEnabled();
		gameFullNotificationsEnabled = login.isGameFullNotificationsEnabled();
		invitedToGameNotificationsEnabled = login.isInvitedToGameNotificationsEnabled();
		notFinishTurnNotificationsEnabled = login.isNotFinishTurnNotificationsEnabled();
		lastToFinishTurnNotificationsEnabled = login.isLastToFinishTurnNotificationsEnabled();
		matchMakingGameCreatedNotificationsEnabled = login.isMatchMakingGameCreatedNotificationsEnabled();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isRoundNotificationsEnabled() {
		return roundNotificationsEnabled;
	}

	public void setRoundNotificationsEnabled(boolean roundNotificationsEnabled) {
		this.roundNotificationsEnabled = roundNotificationsEnabled;
	}

	public boolean isJoinNotificationsEnabled() {
		return joinNotificationsEnabled;
	}

	public void setJoinNotificationsEnabled(boolean joinNotificationsEnabled) {
		this.joinNotificationsEnabled = joinNotificationsEnabled;
	}

	public boolean isGameFullNotificationsEnabled() {
		return gameFullNotificationsEnabled;
	}

	public void setGameFullNotificationsEnabled(boolean gameFullNotificationsEnabled) {
		this.gameFullNotificationsEnabled = gameFullNotificationsEnabled;
	}

	public boolean isInvitedToGameNotificationsEnabled() {
		return invitedToGameNotificationsEnabled;
	}

	public void setInvitedToGameNotificationsEnabled(boolean invitedToGameNotificationsEnabled) {
		this.invitedToGameNotificationsEnabled = invitedToGameNotificationsEnabled;
	}

	public boolean isNotFinishTurnNotificationsEnabled() {
		return notFinishTurnNotificationsEnabled;
	}

	public void setNotFinishTurnNotificationsEnabled(boolean notFinishTurnNotificationsEnabled) {
		this.notFinishTurnNotificationsEnabled = notFinishTurnNotificationsEnabled;
	}

	public boolean isLastToFinishTurnNotificationsEnabled() {
		return lastToFinishTurnNotificationsEnabled;
	}

	public void setLastToFinishTurnNotificationsEnabled(boolean lastToFinishTurnNotificationsEnabled) {
		this.lastToFinishTurnNotificationsEnabled = lastToFinishTurnNotificationsEnabled;
	}

	public boolean isMatchMakingGameCreatedNotificationsEnabled() {
		return matchMakingGameCreatedNotificationsEnabled;
	}

	public void setMatchMakingGameCreatedNotificationsEnabled(boolean matchMakingGameCreatedNotificationsEnabled) {
		this.matchMakingGameCreatedNotificationsEnabled = matchMakingGameCreatedNotificationsEnabled;
	}
}
