package org.skrupeltng.modules.dashboard.controller;

import java.io.Serializable;

import org.skrupeltng.modules.ingame.modules.ship.database.ShipTactics;
import org.skrupeltng.modules.masterdata.database.ShipModule;

public class SpaceCombatCalculatorRequest implements Serializable {

	private static final long serialVersionUID = -7700441354468409690L;

	private String hull1;
	private String energy1;
	private String projectile1;
	private int damage1;
	private int projectile1cnt;
	private int support1;
	private int crew1;
	private ShipTactics tactics1;
	private ShipModule module1;
	private boolean captureMode1;

	private String hull2;
	private String energy2;
	private String projectile2;
	private int damage2;
	private int projectile2cnt;
	private int support2;
	private int crew2;
	private ShipTactics tactics2;
	private ShipModule module2;
	private boolean captureMode2;

	public String getHull1() {
		return hull1;
	}

	public void setHull1(String hull1) {
		this.hull1 = hull1;
	}

	public String getEnergy1() {
		return energy1;
	}

	public void setEnergy1(String energy1) {
		this.energy1 = energy1;
	}

	public String getProjectile1() {
		return projectile1;
	}

	public void setProjectile1(String projectile1) {
		this.projectile1 = projectile1;
	}

	public int getDamage1() {
		return damage1;
	}

	public void setDamage1(int damage1) {
		this.damage1 = damage1;
	}

	public int getProjectile1cnt() {
		return projectile1cnt;
	}

	public void setProjectile1cnt(int projectile1cnt) {
		this.projectile1cnt = projectile1cnt;
	}

	public int getSupport1() {
		return support1;
	}

	public void setSupport1(int support1) {
		this.support1 = support1;
	}

	public int getCrew1() {
		return crew1;
	}

	public void setCrew1(int crew1) {
		this.crew1 = crew1;
	}

	public ShipTactics getTactics1() {
		return tactics1;
	}

	public void setTactics1(ShipTactics tactics1) {
		this.tactics1 = tactics1;
	}

	public ShipModule getModule1() {
		return module1;
	}

	public void setModule1(ShipModule module1) {
		this.module1 = module1;
	}

	public boolean isCaptureMode1() {
		return captureMode1;
	}

	public void setCaptureMode1(boolean captureMode1) {
		this.captureMode1 = captureMode1;
	}

	public String getHull2() {
		return hull2;
	}

	public void setHull2(String hull2) {
		this.hull2 = hull2;
	}

	public String getEnergy2() {
		return energy2;
	}

	public void setEnergy2(String energy2) {
		this.energy2 = energy2;
	}

	public String getProjectile2() {
		return projectile2;
	}

	public void setProjectile2(String projectile2) {
		this.projectile2 = projectile2;
	}

	public int getDamage2() {
		return damage2;
	}

	public void setDamage2(int damage2) {
		this.damage2 = damage2;
	}

	public int getProjectile2cnt() {
		return projectile2cnt;
	}

	public void setProjectile2cnt(int projectile2cnt) {
		this.projectile2cnt = projectile2cnt;
	}

	public int getSupport2() {
		return support2;
	}

	public void setSupport2(int support2) {
		this.support2 = support2;
	}

	public int getCrew2() {
		return crew2;
	}

	public void setCrew2(int crew2) {
		this.crew2 = crew2;
	}

	public ShipTactics getTactics2() {
		return tactics2;
	}

	public void setTactics2(ShipTactics tactics2) {
		this.tactics2 = tactics2;
	}

	public ShipModule getModule2() {
		return module2;
	}

	public void setModule2(ShipModule module2) {
		this.module2 = module2;
	}

	public boolean isCaptureMode2() {
		return captureMode2;
	}

	public void setCaptureMode2(boolean captureMode2) {
		this.captureMode2 = captureMode2;
	}

	@Override
	public String toString() {
		return "SpaceCombatCalculatorRequest [hull1=" + hull1 + ", energy1=" + energy1 + ", projectile1=" + projectile1 + ", damage1=" + damage1 +
				", projectile1cnt=" + projectile1cnt + ", support1=" + support1 + ", crew1=" + crew1 + ", tactics1=" + tactics1 + ", module1=" + module1 +
				", captureMode1=" + captureMode1 + ", hull2=" + hull2 + ", energy2=" + energy2 + ", projectile2=" + projectile2 + ", damage2=" + damage2 +
				", projectile2cnt=" + projectile2cnt + ", support2=" + support2 + ", crew2=" + crew2 + ", tactics2=" + tactics2 + ", module2=" + module2 +
				", captureMode2=" + captureMode2 + "]";
	}
}