package org.skrupeltng.modules.dashboard.controller;

import org.skrupeltng.modules.dashboard.service.TeamService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/game/team")
public class TeamController {

	private final TeamService teamService;

	public TeamController(TeamService teamService) {
		this.teamService = teamService;
	}

	@PutMapping("/")
	public boolean addTeam(@RequestBody ModifyTeamRequest request) {
		return teamService.addTeam(request.gameId(), request.name());
	}

	@PostMapping("/")
	public boolean renameTeam(@RequestBody ModifyTeamRequest request) {
		return teamService.renameTeam(request.gameId(), request.teamId(), request.name());
	}

	@PostMapping("/switch")
	public void switchTeam(@RequestParam("gameId") long gameId,
						   @RequestParam("playerId") long playerId,
						   @RequestParam("teamId") long teamId) {
		teamService.switchTeam(gameId, playerId, teamId);
	}

	@DeleteMapping("/")
	public void removeTeam(@RequestParam("gameId") long gameId, @RequestParam("teamId") long teamId) {
		teamService.removeTeam(gameId, teamId);
	}
}
