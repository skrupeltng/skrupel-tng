package org.skrupeltng.modules.dashboard.controller;

import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ChangePasswordValidator implements Validator {

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private UserDetailServiceImpl userService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public boolean supports(Class<?> clazz) {
		return EmailChangeRequest.class.equals(clazz) || PasswordChangeRequest.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target instanceof PasswordChangeRequest) {
			PasswordChangeRequest request = (PasswordChangeRequest)target;

			if (request.getNewPassword() != null && !request.getNewPassword().equals(request.getNewPasswordRepeated())) {
				errors.rejectValue("newPasswordRepeated", "passwords_must_match");
			}

			Login login = loginRepository.getReferenceById(userService.getLoginId());

			if (!passwordEncoder.matches(request.getOldPassword(), login.getPassword())) {
				errors.rejectValue("oldPassword", "old_password_invalid");
			}
		}
	}
}
