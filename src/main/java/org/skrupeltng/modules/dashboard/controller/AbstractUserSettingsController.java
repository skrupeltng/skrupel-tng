package org.skrupeltng.modules.dashboard.controller;

import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.dashboard.database.Login;
import org.springframework.ui.Model;

public class AbstractUserSettingsController extends AbstractController {

	protected void addUserDetailsData(Model model, Login login, boolean addEmailModel) {
		model.addAttribute("isAdmin", userDetailService.isAdmin());

		if (addEmailModel) {
			model.addAttribute("emailSettingsRequest", new EmailChangeRequest(login));
		}
	}
}