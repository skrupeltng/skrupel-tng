package org.skrupeltng.modules.dashboard.controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.service.LoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
public class PasswordRecoveryController extends AbstractController {

	private static final Logger log = LoggerFactory.getLogger(PasswordRecoveryController.class);

	private static final String DASHBOARD_PASSWORD_RECOVERY_NOT_FOUND = "dashboard/password-recovery-not-found";

	private final LoginService loginService;
	private final ResetPasswordValidator resetPasswordValidator;
	private final HttpServletRequest httpServletRequest;

	public PasswordRecoveryController(LoginService loginService, ResetPasswordValidator resetPasswordValidator, HttpServletRequest httpServletRequest) {
		this.loginService = loginService;
		this.resetPasswordValidator = resetPasswordValidator;
		this.httpServletRequest = httpServletRequest;
	}

	@InitBinder
	protected void initBinder(final WebDataBinder binder) {
		binder.addValidators(resetPasswordValidator);
	}

	@GetMapping("/reset-password")
	public String getResetPassword(Model model) {
		model.addAttribute("request", new PasswordRecoveryRequest());
		return "dashboard/reset-password";
	}

	@PostMapping("/reset-password")
	public String initResetPassword(@Valid @ModelAttribute("request") PasswordRecoveryRequest request, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return "dashboard/reset-password";
		}

		loginService.sendPasswordRecoveryEmail(request.getEmail());

		return "dashboard/password-recovery-email-sent";
	}

	@GetMapping("/reset-password/token/{token}")
	public String showResetPasswordForm(@PathVariable("token") String token, Model model) {
		Optional<Login> result = loginService.getLoginByPasswordRecoveryToken(token);

		if (result.isEmpty()) {
			return DASHBOARD_PASSWORD_RECOVERY_NOT_FOUND;
		}

		model.addAttribute("request", new ResetPasswordRequest());
		model.addAttribute("token", token);
		return "dashboard/password-recovery-form";
	}

	@PostMapping("/reset-password/token/{token}")
	public String resetPassword(@Valid @ModelAttribute("request") ResetPasswordRequest request, BindingResult bindingResult,
								@PathVariable("token") String token, Model model) {
		Optional<Login> result = loginService.getLoginByPasswordRecoveryToken(token);

		if (result.isEmpty()) {
			return DASHBOARD_PASSWORD_RECOVERY_NOT_FOUND;
		}

		if (bindingResult.hasErrors()) {
			model.addAttribute("token", token);
			return "dashboard/password-recovery-form";
		}

		result = loginService.resetPassword(token, request);

		if (result.isEmpty()) {
			return DASHBOARD_PASSWORD_RECOVERY_NOT_FOUND;
		}

		SecurityContextHolder.clearContext();

		try {
			httpServletRequest.login(result.get().getUsername(), request.getPassword());
			return "redirect:/password-recovery-success";
		} catch (ServletException e) {
			log.error("Unable to auto-login new user: ", e);
			model.addAttribute("message", "Unable to finish registration process: %s".formatted(e.getMessage()));
			return "error";
		}
	}

	@GetMapping("/password-recovery-success")
	public String showPasswordRecoverySuccess() {
		return "dashboard/password-recovery-success";
	}
}
