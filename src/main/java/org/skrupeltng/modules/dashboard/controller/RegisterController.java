package org.skrupeltng.modules.dashboard.controller;

import org.skrupeltng.config.LoginDetails;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.dashboard.RegisterRequest;
import org.skrupeltng.modules.dashboard.service.LoginService;
import org.skrupeltng.modules.mail.service.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import java.util.Optional;

@Controller
public class RegisterController extends AbstractController {

	private static final Logger log = LoggerFactory.getLogger(RegisterController.class);

	private final LoginService loginService;
	private final RegistrationValidator registrationValidator;
	private final MailService mailService;
	private final HttpServletRequest httpServletRequest;

	public RegisterController(LoginService loginService,
							  RegistrationValidator registrationValidator,
							  MailService mailService,
							  HttpServletRequest httpServletRequest) {
		this.loginService = loginService;
		this.registrationValidator = registrationValidator;
		this.mailService = mailService;
		this.httpServletRequest = httpServletRequest;
	}

	@InitBinder
	protected void initBinder(final WebDataBinder binder) {
		binder.addValidators(registrationValidator);
	}

	@GetMapping("/register")
	public String register(Model model) {
		if (loginService.noPlayerExist()) {
			return "redirect:init-setup";
		}

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (auth != null) {
			Object principal = auth.getPrincipal();

			if (principal instanceof LoginDetails loggedInUser && !loginService.isGuestAccount(loggedInUser.getId())) {
				return "redirect:/my-games";
			}
		}

		RegisterRequest request = new RegisterRequest();
		request.setEmailNotificationsEnabled(true);
		model.addAttribute("request", request);

		return "dashboard/register";
	}

	@PostMapping("/register")
	public String register(@Valid @ModelAttribute("request") RegisterRequest request, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return "dashboard/register";
		}

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Optional<Long> guestUserIdOpt = Optional.empty();

		if (auth != null) {
			Object principal = auth.getPrincipal();

			if (principal instanceof LoginDetails loggedInUser) {
				if (!loginService.isGuestAccount(loggedInUser.getId())) {
					return "redirect:/my-games";
				}

				guestUserIdOpt = Optional.of(loggedInUser.getId());
			}
		}

		loginService.registerNewLogin(request, guestUserIdOpt);

		if (mailService.mailsEnabled() && configProperties.isEnableAccountValidationByEmail()) {
			return "redirect:/register-successfull";
		}

		SecurityContextHolder.clearContext();

		try {
			httpServletRequest.login(request.getUsername(), request.getPassword());
			return "redirect:/registration-finished";
		} catch (ServletException e) {
			log.error("Unable to auto-login new user: ", e);
			model.addAttribute("message", "Unable to finish registration process: %s" .formatted(e.getMessage()));
			return "error";
		}
	}
}
