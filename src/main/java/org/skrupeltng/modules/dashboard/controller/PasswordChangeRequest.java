package org.skrupeltng.modules.dashboard.controller;

import java.io.Serializable;

import org.skrupeltng.modules.dashboard.database.Login;

public class PasswordChangeRequest implements Serializable {

	private static final long serialVersionUID = -4439326045183463264L;

	private String username;

	private String oldPassword;

	@ValidPassword
	private String newPassword;

	private String newPasswordRepeated;

	public PasswordChangeRequest() {

	}

	public PasswordChangeRequest(Login login) {
		username = login.getUsername();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getNewPasswordRepeated() {
		return newPasswordRepeated;
	}

	public void setNewPasswordRepeated(String newPasswordRepeated) {
		this.newPasswordRepeated = newPasswordRepeated;
	}
}