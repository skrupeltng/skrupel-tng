package org.skrupeltng.modules.dashboard.controller;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.LengthRule;
import org.passay.MessageResolver;
import org.passay.PasswordData;
import org.passay.PasswordValidator;
import org.passay.RuleResult;
import org.passay.WhitespaceRule;
import org.passay.spring.SpringMessageResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {

	private final MessageResolver resolver;
	private final MessageSource messageSource;
	private final PasswordValidator validator;

	@Autowired
	public PasswordConstraintValidator(MessageSource messageSource) {
		this.messageSource = messageSource;
		resolver = new SpringMessageResolver(messageSource);

		validator = new PasswordValidator(resolver, List.of(
				new LengthRule(8, 256),
				new CharacterRule(EnglishCharacterData.UpperCase),
				new CharacterRule(EnglishCharacterData.LowerCase),
				new CharacterRule(EnglishCharacterData.Digit),
				new WhitespaceRule()));
	}

	@Override
	public void initialize(ValidPassword arg0) {

	}

	@Override
	public boolean isValid(String password, ConstraintValidatorContext context) {
		RuleResult result = validator.validate(new PasswordData(password));

		if (result.isValid()) {
			return true;
		}

		context.disableDefaultConstraintViolation();

		String completeMessage = messageSource.getMessage("password_does_not_meet_requirements", null, LocaleContextHolder.getLocale());
		completeMessage += String.join(", ", validator.getMessages(result));

		context.buildConstraintViolationWithTemplate(completeMessage).addConstraintViolation();

		return false;
	}
}
