package org.skrupeltng.modules.dashboard;

public interface Roles {

	String PLAYER = "ROLE_PLAYER";
	String GUEST = "ROLE_GUEST";
	String ADMIN = "ROLE_ADMIN";
	String AI = "ROLE_AI";
}