package org.skrupeltng.modules.dashboard.service;

import org.skrupeltng.modules.ingame.database.player.*;
import org.skrupeltng.modules.ingame.modules.anomaly.database.MineFieldRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.PlayerWormHoleVisitRepository;
import org.skrupeltng.modules.ingame.modules.controlgroup.database.ControlGroupRepository;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.overview.database.*;
import org.skrupeltng.modules.ingame.modules.overview.database.spacecombat.SpaceCombatLogEntryItemRepository;
import org.skrupeltng.modules.ingame.modules.overview.database.spacecombat.SpaceCombatLogEntryRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerEncounterRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequestRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipRemoval;
import org.skrupeltng.modules.ingame.modules.starbase.database.PlayerStarbaseScanLogRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class PlayerRemoval {

	@Autowired
	private ShipRemoval shipRemoval;

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Autowired
	private PlayerRelationRequestRepository playerRelationRequestRepository;

	@Autowired
	private PlayerPlanetScanLogRepository playerPlanetScanLogRepository;

	@Autowired
	private PlayerStarbaseScanLogRepository playerStarbaseScanLogRepository;

	@Autowired
	private PlayerDeathFoeRepository playerDeathFoeRepository;

	@Autowired
	private PlayerRoundStatsRepository playerRoundStatsRepository;

	@Autowired
	private NewsEntryRepository newsEntryRepository;

	@Autowired
	private SpaceCombatLogEntryRepository spaceCombatLogEntryRepository;

	@Autowired
	private SpaceCombatLogEntryItemRepository spaceCombatLogEntryItemRepository;

	@Autowired
	private OrbitalCombatLogEntryRepository orbitalCombatLogEntryRepository;

	@Autowired
	private SmallMeteorLogEntryRepository smallMeteorLogEntryRepository;

	@Autowired
	private ShockWaveDestructionLogEntryLogEntryRepository shockWaveDestructionLogEntryLogEntryRepository;

	@Autowired
	private NewsEntryArgumentRepository newsEntryArgumentRepository;

	@Autowired
	private FleetRepository fleetRepository;

	@Autowired
	private MineFieldRepository mineFieldRepository;

	@Autowired
	private ControlGroupRepository controlGroupRepository;

	@Autowired
	private PlayerEncounterRepository playerEncounterRepository;

	@Autowired
	private MineralRacePlayerProgressNotificationRepository mineralRacePlayerProgressNotificationRepository;

	@Autowired
	private PlayerWormHoleVisitRepository playerWormHoleVisitRepository;

	@Transactional(propagation = Propagation.REQUIRED)
	public void deletePlayer(Player player) {
		shipRemoval.deleteShips(player.getShips());

		long playerId = player.getId();

		starbaseRepository.clearLogBooksOfPlayer(playerId);
		planetRepository.clearPlayer(playerId);
		planetRepository.clearNewPlayer(playerId);

		playerEncounterRepository.deleteByPlayerId(playerId);
		playerRelationRepository.deleteByPlayerId(playerId);
		playerRelationRequestRepository.deleteByPlayerId(playerId);
		playerPlanetScanLogRepository.deleteByPlayerId(playerId);
		playerStarbaseScanLogRepository.deleteByPlayerId(playerId);
		playerRoundStatsRepository.deleteByPlayerId(playerId);
		spaceCombatLogEntryItemRepository.deleteByPlayerId(playerId);
		spaceCombatLogEntryRepository.deleteByPlayerId(playerId);
		orbitalCombatLogEntryRepository.deleteByPlayerId(playerId);
		smallMeteorLogEntryRepository.deleteByPlayerId(playerId);
		shockWaveDestructionLogEntryLogEntryRepository.deleteByPlayerId(playerId);
		newsEntryArgumentRepository.deleteByPlayerId(playerId);
		newsEntryRepository.deleteByPlayerId(playerId);
		playerDeathFoeRepository.deleteByPlayerId(playerId);
		fleetRepository.deleteByPlayerId(playerId);
		mineFieldRepository.deleteByPlayerId(playerId);
		controlGroupRepository.deleteByPlayerId(playerId);
		mineralRacePlayerProgressNotificationRepository.deleteByPlayerId(playerId);
		playerWormHoleVisitRepository.deleteByPlayerId(playerId);

		playerRepository.delete(player);
	}
}
