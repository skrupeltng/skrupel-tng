package org.skrupeltng.modules.dashboard.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.random.MersenneTwister;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.dashboard.ResourceDensityHomePlanet;
import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.HomePlanetSetup;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetType;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetTypeRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StartPositionHelper {

	private static final int START_PLANET_RANGE_RADIUS = 250;
	private static final int PLANET_DENSITY_COUNT_FACTOR = 60;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private PlanetTypeRepository planetTypeRepository;

	@Autowired
	private OrbitalSystemRepository orbitalSystemRepository;

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private ConfigProperties configProperties;

	public void assignStartPositionsCircleAroundCenter(Game game, List<Player> players, List<Planet> planets, String galaxyConfigId) {
		switch (galaxyConfigId) {
			case "gala_1":
			case "gala_9":
				setStartingPositionsForCircleAroundCenter(game, players, planets, game.getGalaxySize() / 4);
				break;
			case "gala_2":
			case "gala_4":
				setStartingPositionsForCircleAroundCenter(game, players, planets, game.getGalaxySize() / 5);
				break;
			default:
				setStartingPositionsForCircleAroundCenter(game, players, planets, game.getGalaxySize() / 2);
				break;
		}
	}

	private void setStartingPositionsForCircleAroundCenter(Game game, List<Player> players, List<Planet> planets, int circleRadius) {
		int galaxyHalfSize = game.getGalaxySize() / 2;
		double randomAngle = MasterDataService.RANDOM.nextDouble() * Math.PI;
		Vector2D positionOnCircle = rotateVectorAroundAngle(new Vector2D(circleRadius, 0d), randomAngle);

		double angleBetweenPlayers = Math.PI * 2d / players.size();

		List<Player> shuffletPlayers = new ArrayList<>(players);
		Collections.shuffle(shuffletPlayers);

		for (Player player : shuffletPlayers) {
			Coordinate coordOnCircle = new CoordinateImpl((int)positionOnCircle.getX() + galaxyHalfSize, (int)positionOnCircle.getY() + galaxyHalfSize, 0);
			Planet planet = planets.stream().min(new CoordinateDistanceComparator(coordOnCircle)).orElseThrow();
			addOwnedPlanet(player, planet);

			positionOnCircle = rotateVectorAroundAngle(positionOnCircle, angleBetweenPlayers);
		}
	}

	private Vector2D rotateVectorAroundAngle(Vector2D vector, double angle) {
		double x = vector.getX();
		double y = vector.getY();
		return new Vector2D(Math.cos(angle) * x - Math.sin(angle) * y, Math.sin(angle) * x + Math.cos(angle) * y);
	}

	protected void assignStartPositionsEqualDistance(Game game, List<Player> players, List<Planet> planets) {
		final int initMinAllowedPlayerDistance;

		if (game.getPlayerCount() > 1) {
			initMinAllowedPlayerDistance = game.getGalaxySize() / (game.getPlayerCount() - 1);
		} else {
			initMinAllowedPlayerDistance = 0;
		}

		Set<Planet> playerPlanets = new HashSet<>(players.size());

		List<Planet> startPositionPlanets = getStartPositionPlanetsForEqualDistance(planets, players.size(),
				PLANET_DENSITY_COUNT_FACTOR / game.getPlanetDensity().getFactor(), game);

		for (Player player : players) {
			if (playerPlanets.isEmpty()) {
				Planet planet = startPositionPlanets.get(MasterDataService.RANDOM.nextInt(startPositionPlanets.size()));
				addOwnedPlanet(player, planet);
				playerPlanets.add(planet);
			} else {
				searchForPlanetForEqualDistanceAssignment(initMinAllowedPlayerDistance, playerPlanets, startPositionPlanets, player);
			}
		}
	}

	private void searchForPlanetForEqualDistanceAssignment(final int initMinAllowedPlayerDistance, Set<Planet> playerPlanets, List<Planet> startPositionPlanets, Player player) {
		int tries = 0;
		int minAllowedPlayerDistance = initMinAllowedPlayerDistance;

		for (int i = 0; i < configProperties.getIterationLimit(); i++) {
			Planet planet = startPositionPlanets.get(MasterDataService.RANDOM.nextInt(startPositionPlanets.size()));

			if (playerPlanets.contains(planet)) {
				continue;
			}

			final int allowedDistance = minAllowedPlayerDistance;
			boolean hasNearPlayerPlanets = playerPlanets.stream().anyMatch(p -> planet.getDistance(p) < allowedDistance);

			if (!hasNearPlayerPlanets) {
				addOwnedPlanet(player, planet);
				return;
			}

			tries++;

			if (tries > 4) {
				minAllowedPlayerDistance--;
				tries = 0;
			}
		}
	}

	protected List<Planet> getStartPositionPlanetsForEqualDistance(List<Planet> allPlanets, int playerCount, int minPlanetCount, Game game) {
		List<Planet> startingPlanets = new ArrayList<>(allPlanets.size());

		int margin = configProperties.getInvasionStartingPlanetMargin();
		boolean invasion = game.getWinCondition() == WinCondition.INVASION;
		int galaxySizeMaxMargin = game.getGalaxySize() - margin;

		for (Planet planet : allPlanets) {
			if (invasion) {
				int x = planet.getX();
				int y = planet.getY();

				if (x < margin || x > galaxySizeMaxMargin || y < margin || y > galaxySizeMaxMargin) {
					continue;
				}
			}

			long nearPlanets = allPlanets.stream().filter(p -> p != planet && planet.getDistance(p) <= START_PLANET_RANGE_RADIUS).count();

			if (nearPlanets >= minPlanetCount) {
				startingPlanets.add(planet);
			}
		}

		if (startingPlanets.size() < playerCount) {
			return allPlanets;
		}

		return startingPlanets;
	}

	protected void assignStartPositionsRandom(List<Player> players, List<Planet> planets) {
		Set<Long> assignedPlanetIds = new HashSet<>(players.size());

		for (Player player : players) {
			while (true) {
				Planet planet = planets.get(MasterDataService.RANDOM.nextInt(planets.size()));

				if (assignedPlanetIds.add(planet.getId())) {
					addOwnedPlanet(player, planet);
					break;
				}
			}
		}
	}

	protected void addOwnedPlanet(Player player, Planet planet) {
		Game game = player.getGame();
		MersenneTwister random = MasterDataService.RANDOM;

		planet.setPlayer(player);
		Faction faction = player.getFaction();

		if (!game.isRandomizeHomePlanetNames()) {
			planet.setName(faction.getHomePlanetName());
		}

		final String preferredPlanetType = faction.getPreferredPlanetType();
		if (preferredPlanetType == null || "".equals(preferredPlanetType)) {
			final List<PlanetType> planetTypes = planetTypeRepository.findAll();
			final PlanetType planetType = planetTypes.get(random.nextInt(planetTypes.size()));
			planet.setType(planetType.getId());
		} else {
			planet.setType(faction.getPreferredPlanetType());
		}

		if (faction.getPreferredTemperature() == 0) {
			final PlanetType planetType = planetTypeRepository.getReferenceById(planet.getType());
			int temperatureRange = planetType.getMaxTemperature() - planetType.getMinTemperature();
			int temperature = planetType.getMinTemperature() + random.nextInt(temperatureRange);
			planet.setTemperature(temperature);
		} else {
			planet.setTemperature(faction.getPreferredTemperature());
		}

		PlanetType planetType = planetTypeRepository.getReferenceById(planet.getType());
		String image = planetType.getImagePrefix() + "_" + (1 + random.nextInt(planetType.getImageCount()));
		planet.setImage(image);

		int colonists = random.nextInt(1000) + (random.nextInt(1000) * 5) + 50000;
		planet.setColonists(colonists);
		planet.setMoney(game.getInitMoney());
		planet.setSupplies(5);
		planet.setMines(5);
		planet.setFactories(5);
		planet.setPlanetaryDefense(5);
		planet.setFuel(50 + random.nextInt(20));
		planet.setMineral1(50 + random.nextInt(20));
		planet.setMineral2(50 + random.nextInt(20));
		planet.setMineral3(50 + random.nextInt(20));
		planet.setArtifactType(null);

		ResourceDensityHomePlanet density = game.getResourceDensityHomePlanet();

		int minResources = density.getMin();
		int resourceDiff = density.getMax() - minResources;
		planet.setUntappedFuel(minResources + random.nextInt(resourceDiff));
		planet.setUntappedMineral1(minResources + random.nextInt(resourceDiff));
		planet.setUntappedMineral2(minResources + random.nextInt(resourceDiff));
		planet.setUntappedMineral3(minResources + random.nextInt(resourceDiff));

		planet.setNecessaryMinesForOneFuel(masterDataService.getNecessaryMinesByIndex(3 + random.nextInt(2)));
		planet.setNecessaryMinesForOneMineral1(masterDataService.getNecessaryMinesByIndex(3 + random.nextInt(2)));
		planet.setNecessaryMinesForOneMineral2(masterDataService.getNecessaryMinesByIndex(3 + random.nextInt(2)));
		planet.setNecessaryMinesForOneMineral3(masterDataService.getNecessaryMinesByIndex(3 + random.nextInt(2)));

		planet.setNativeSpecies(null);
		planet.setNativeSpeciesCount(0);

		HomePlanetSetup homePlanetSetup = game.getHomePlanetSetup();

		if (homePlanetSetup == HomePlanetSetup.STAR_BASE) {
			Starbase starbase = new Starbase();
			starbase.setName("Starbase 1");
			starbase.setType(StarbaseType.STAR_BASE);
			starbase = starbaseRepository.save(starbase);
			planet.setStarbase(starbase);
		} else if (homePlanetSetup == HomePlanetSetup.WAR_BASE) {
			Starbase starbase = new Starbase();
			starbase.setName("Warbase 1");
			starbase.setType(StarbaseType.WAR_BASE);
			starbase = starbaseRepository.save(starbase);
			planet.setStarbase(starbase);
		}

		orbitalSystemRepository.deleteByPlanetId(planet.getId());

		List<OrbitalSystem> orbitalSystems = new ArrayList<>(4);
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystemRepository.saveAll(orbitalSystems);

		planet = planetRepository.save(planet);
		player.setHomePlanet(planet);
		playerRepository.save(player);
	}
}
