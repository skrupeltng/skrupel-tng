package org.skrupeltng.modules.dashboard.service;

public class GameFullException extends Exception {

	private static final long serialVersionUID = -5599309918904784031L;

	private final int maxPlayerCount;

	public GameFullException(int maxPlayerCount) {
		this.maxPlayerCount = maxPlayerCount;
	}

	public int getMaxPlayerCount() {
		return maxPlayerCount;
	}
}