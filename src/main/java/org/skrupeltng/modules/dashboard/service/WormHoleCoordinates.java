package org.skrupeltng.modules.dashboard.service;

import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.StableWormholeConfig;
import org.skrupeltng.modules.masterdata.service.MasterDataService;

public class WormHoleCoordinates {

	private final boolean isFirst;
	private final int i;
	private final int minGalaxyMapBorder;

	private final int max;
	private final int half;

	public WormHoleCoordinates(int galaxySize, boolean isFirst, int i, int minGalaxyMapBorder) {
		this.isFirst = isFirst;
		this.i = i;
		this.minGalaxyMapBorder = minGalaxyMapBorder;

		max = galaxySize;
		half = galaxySize / 2;
	}

	public Coordinate getStableWormholeCoordinate(StableWormholeConfig stableWormholeConfig) {
		return switch (stableWormholeConfig) {
			case NONE, ONE_RANDOM, TWO_RANDOM, THREE_RANDOM, FOUR_RANDOM, FIVE_RANDOM -> new CoordinateImpl(createRandomCoordinate(), createRandomCoordinate(), 0);
			case ONE_NORTH_SOUTH, TWO_NORTH_SOUTH -> createNorthSouth();
			case ONE_WEST_EAST, TWO_WEST_EAST -> createWestEast();
			case ONE_NORTH_WEST_EAST_SOUTH -> createNorthWestEastSouth();
			case ONE_NORTHWEST_SOUTHEAST -> createNorthWestSouthEast();
			case ONE_NORTHEAST_SOUTHWEST -> createNorthEastSouthWest();
			case TWO_DIAGONAL -> createTwoDiagonal();
		};
	}

	private Coordinate createNorthSouth() {
		int x = createRandomCoordinate();
		int y;

		if (isFirst) {
			y = getRandomValueBetweenMinAndHalf();
		} else {
			y = getRandomValueBetweenHalfAndMax();
		}

		return new CoordinateImpl(x, y, 0);
	}

	private Coordinate createWestEast() {
		int x;
		int y = createRandomCoordinate();

		if (isFirst) {
			x = getRandomValueBetweenMinAndHalf();
		} else {
			x = getRandomValueBetweenHalfAndMax();
		}

		return new CoordinateImpl(x, y, 0);
	}

	private Coordinate createNorthWestEastSouth() {
		int x;
		int y;

		if (i == 0) {
			x = createRandomCoordinate();

			if (isFirst) {
				y = getRandomValueBetweenMinAndHalf();
			} else {
				y = getRandomValueBetweenHalfAndMax();
			}
		} else {
			y = createRandomCoordinate();

			if (isFirst) {
				x = getRandomValueBetweenMinAndHalf();
			} else {
				x = getRandomValueBetweenHalfAndMax();
			}
		}

		return new CoordinateImpl(x, y, 0);
	}

	private Coordinate createNorthWestSouthEast() {
		int x;
		int y;

		if (isFirst) {
			x = getRandomValueBetweenMinAndHalf();
			y = getRandomValueBetweenMinAndHalf();
		} else {
			x = getRandomValueBetweenHalfAndMax();
			y = getRandomValueBetweenHalfAndMax();
		}

		return new CoordinateImpl(x, y, 0);
	}

	private Coordinate createNorthEastSouthWest() {
		int x;
		int y;

		if (isFirst) {
			x = getRandomValueBetweenHalfAndMax();
			y = getRandomValueBetweenMinAndHalf();
		} else {
			x = getRandomValueBetweenMinAndHalf();
			y = getRandomValueBetweenHalfAndMax();
		}

		return new CoordinateImpl(x, y, 0);
	}

	private Coordinate createTwoDiagonal() {
		int x;
		int y;

		if (i == 0) {
			if (isFirst) {
				x = getRandomValueBetweenMinAndHalf();
				y = getRandomValueBetweenMinAndHalf();
			} else {
				x = getRandomValueBetweenHalfAndMax();
				y = getRandomValueBetweenHalfAndMax();
			}
		} else {
			if (isFirst) {
				x = getRandomValueBetweenHalfAndMax();
				y = getRandomValueBetweenMinAndHalf();
			} else {
				x = getRandomValueBetweenMinAndHalf();
				y = getRandomValueBetweenHalfAndMax();
			}
		}

		return new CoordinateImpl(x, y, 0);
	}

	private int createRandomCoordinate() {
		return createRandomCoordinate(minGalaxyMapBorder, max);
	}

	public static int createRandomCoordinate(int minGalaxyMapBorder, int max) {
		return minGalaxyMapBorder + MasterDataService.RANDOM.nextInt(max - (minGalaxyMapBorder * 2));
	}

	private int getRandomValueBetweenMinAndHalf() {
		return minGalaxyMapBorder + MasterDataService.RANDOM.nextInt(half - minGalaxyMapBorder);
	}

	private int getRandomValueBetweenHalfAndMax() {
		return half + MasterDataService.RANDOM.nextInt(half - minGalaxyMapBorder);
	}
}
