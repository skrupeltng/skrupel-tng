package org.skrupeltng.modules.dashboard.service;

import java.util.List;
import java.util.Set;

import org.skrupeltng.modules.dashboard.database.TeamRepository;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.PlasmaStormRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFoldRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.AquiredShipTemplateRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.service.StarbaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class GameRemoval {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private OrbitalSystemRepository orbitalSystemRepository;

	@Autowired
	private StarbaseService starbaseService;

	@Autowired
	private SpaceFoldRepository spaceFoldRepository;

	@Autowired
	private WormHoleRepository wormHoleRepository;

	@Autowired
	private PlasmaStormRepository plasmaStormRepository;

	@Autowired
	private AquiredShipTemplateRepository aquiredShipTemplateRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private PlayerRemoval playerRemoval;

	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(long gameId) {
		Game game = gameRepository.getReferenceById(gameId);
		delete(game);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(Game game) {
		long gameId = game.getId();
		log.info("Deleting game {}...", gameId);

		plasmaStormRepository.deleteByGameId(gameId);
		aquiredShipTemplateRepository.deleteByGameId(gameId);
		playerRepository.clearTeamsFromPlayers(gameId);

		List<Player> players = game.getPlayers();

		for (Player player : players) {
			playerRemoval.deletePlayer(player);
		}

		wormHoleRepository.deleteByGameId(gameId);
		teamRepository.deleteByGameId(gameId);

		Set<Planet> planets = game.getPlanets();

		for (Planet planet : planets) {
			long planetId = planet.getId();
			spaceFoldRepository.deleteByPlanetId(planetId);
			orbitalSystemRepository.deleteByPlanetId(planetId);

			Starbase starbase = planet.getStarbase();

			planetRepository.delete(planet);

			if (starbase != null) {
				starbaseService.delete(starbase);
			}
		}

		gameRepository.delete(game);

		log.info("Game {} deleted.", gameId);
	}
}
