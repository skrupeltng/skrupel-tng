package org.skrupeltng.modules.dashboard.service;

import java.util.List;

public class GalaxyData {

	private final PlanetContainer[][] containerMap;
	private final List<PlanetContainer> containerList;

	public GalaxyData(PlanetContainer[][] containerMap, List<PlanetContainer> containerList) {
		this.containerMap = containerMap;
		this.containerList = containerList;
	}

	public PlanetContainer[][] getContainerMap() {
		return containerMap;
	}

	public List<PlanetContainer> getContainerList() {
		return containerList;
	}
}