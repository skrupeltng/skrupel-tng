package org.skrupeltng.modules.dashboard.service;

import org.apache.commons.math3.random.MersenneTwister;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.dashboard.ResourceDensity;
import org.skrupeltng.modules.dashboard.database.GalaxyConfig;
import org.skrupeltng.modules.dashboard.database.GalaxyConfigRepository;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.modules.planet.database.*;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Component
public class PlanetGenerator {

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private PlanetNameRepository planetNameRepository;

	@Autowired
	private PlanetTypeRepository planetTypeRepository;

	@Autowired
	private OrbitalSystemRepository orbitalSystemRepository;

	@Autowired
	private GalaxyConfigRepository galaxyConfigRepository;

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private NativeSpeciesRepository nativeSpeciesRepository;

	@Autowired
	private ConfigProperties configProperties;

	@Transactional(propagation = Propagation.REQUIRED)
	public String generatePlanets(Game game) {
		int planetCount = game.getGalaxySize() / game.getPlanetDensity().getFactor();

		if (planetCount < game.getPlayerCount()) {
			planetCount = game.getPlayerCount();
		}

		List<Planet> planets = new ArrayList<>(planetCount);

		final List<String> allPlanetNames = planetNameRepository.getAllPlanetNames();
		List<PlanetType> planetTypes = planetTypeRepository.findAll();

		ResourceDensity resourceDensity = game.getResourceDensity();
		int resourceDensityDiff = resourceDensity.getMax() - resourceDensity.getMin();

		MersenneTwister random = MasterDataService.RANDOM;
		String galaxyStructure = game.getGalaxyConfig().getId();

		if (galaxyStructure.equals(GalaxyConfig.RANDOM_GALAXY_CONFIG)) {
			List<String> galaxyConfigIds = galaxyConfigRepository.getIds();
			galaxyConfigIds.remove(GalaxyConfig.RANDOM_GALAXY_CONFIG);
			galaxyStructure = galaxyConfigIds.get(random.nextInt(galaxyConfigIds.size()));
		}

		GalaxyData galaxyData = getGalaxyStructureData(galaxyStructure);
		List<PlanetContainer> containerList = galaxyData.getContainerList();

		int galaxySize = game.getGalaxySize();
		int containerDimension = galaxySize / configProperties.getGalaxyStructureFileSize();
		final List<NativeSpecies> allNativeSpecies = nativeSpeciesRepository.findAll();

		int minGalaxyMapBorder = configProperties.getMinGalaxyMapBorder();

		List<String> planetNamesLeft = new ArrayList<>(allPlanetNames);
		int planetNameRepetitions = 1;

		for (int i = 0; i < planetCount; i++) {
			Planet planet = new Planet();

			int iterations = 0;
			boolean tooManyIterations = false;

			for (int j = 0; j < configProperties.getIterationLimit(); j++) {
				PlanetContainer container = containerList.get(random.nextInt(containerList.size()));
				int containerX = container.getX();
				int containerY = container.getY();

				planet.setX((containerDimension * containerX) + random.nextInt(containerDimension));
				planet.setY((containerDimension * containerY) + random.nextInt(containerDimension));

				if (planet.getX() < minGalaxyMapBorder) {
					planet.setX(minGalaxyMapBorder);
				}
				if (planet.getY() < minGalaxyMapBorder) {
					planet.setY(minGalaxyMapBorder);
				}
				if (planet.getX() > galaxySize - minGalaxyMapBorder) {
					planet.setX(galaxySize - minGalaxyMapBorder);
				}
				if (planet.getY() > galaxySize - minGalaxyMapBorder) {
					planet.setY(galaxySize - minGalaxyMapBorder);
				}

				boolean tooNear = false;

				if (containerDimension < 40) {
					for (Planet p : planets) {
						if (p.getDistance(planet) < configProperties.getMinPlanetDistance()) {
							tooNear = true;
							break;
						}
					}
				} else {
					List<PlanetContainer> neighbors = container.getNeighbors();

					for (PlanetContainer neighbor : neighbors) {
						for (Planet p : neighbor) {
							if (p.getDistance(planet) < configProperties.getMinPlanetDistance()) {
								tooNear = true;
								break;
							}
						}

						if (tooNear) {
							break;
						}
					}
				}

				if (!tooNear) {
					container.add(planet);
					break;
				}

				iterations++;

				if (iterations > configProperties.getMaxPlanetIterations()) {
					tooManyIterations = true;
					break;
				}
			}

			if (!tooManyIterations) {
				planets.add(planet);
				planet.setGame(game);
				game.getPlanets().add(planet);

				if (planetNamesLeft.isEmpty()) {
					planetNameRepetitions++;
					planetNamesLeft = new ArrayList<>(allPlanetNames);
				}

				String name = planetNamesLeft.remove(random.nextInt(planetNamesLeft.size()));

				if (planetNameRepetitions > 1) {
					name += " (" + planetNameRepetitions + ")";
				}

				planet.setName(name);

				PlanetType planetType = planetTypes.get(random.nextInt(planetTypes.size()));
				planet.setType(planetType.getId());

				int temperatureRange = planetType.getMaxTemperature() - planetType.getMinTemperature();
				int temperature = planetType.getMinTemperature() + random.nextInt(temperatureRange);
				planet.setTemperature(temperature);

				String image = planetType.getImagePrefix() + "_" + (1 + random.nextInt(planetType.getImageCount()));
				planet.setImage(image);

				planet.setFuel(1 + random.nextInt(70));
				planet.setMineral1(1 + random.nextInt(70));
				planet.setMineral2(1 + random.nextInt(70));
				planet.setMineral3(1 + random.nextInt(70));

				planet.setUntappedFuel(resourceDensity.getMin() + random.nextInt(resourceDensityDiff));
				planet.setUntappedMineral1(resourceDensity.getMin() + random.nextInt(resourceDensityDiff));
				planet.setUntappedMineral2(resourceDensity.getMin() + random.nextInt(resourceDensityDiff));
				planet.setUntappedMineral3(resourceDensity.getMin() + random.nextInt(resourceDensityDiff));

				planet.setNecessaryMinesForOneFuel(masterDataService.getNecessaryMinesByIndex(random.nextInt(5)));
				planet.setNecessaryMinesForOneMineral1(masterDataService.getNecessaryMinesByIndex(random.nextInt(5)));
				planet.setNecessaryMinesForOneMineral2(masterDataService.getNecessaryMinesByIndex(random.nextInt(5)));
				planet.setNecessaryMinesForOneMineral3(masterDataService.getNecessaryMinesByIndex(random.nextInt(5)));

				float inhabitedPlanetsPercentage = game.getInhabitedPlanetsPercentage();

				if (inhabitedPlanetsPercentage > 0f && random.nextFloat() <= inhabitedPlanetsPercentage) {
					NativeSpecies nativeSpecies = allNativeSpecies.get(random.nextInt(allNativeSpecies.size()));
					planet.setNativeSpecies(nativeSpecies);
					planet.setNativeSpeciesCount(2000 + random.nextInt(15500));
				}

				int artifactTypeOrdinal = random.nextInt(99);

				if (artifactTypeOrdinal < 6) {
					for (ArtifactType artifactType : ArtifactType.values()) {
						if (artifactType.ordinal() == artifactTypeOrdinal) {
							planet.setArtifactType(artifactType);
							break;
						}
					}
				}
			}
		}

		List<Planet> savedPlanets = planetRepository.saveAll(planets);
		game.setPlanets(new HashSet<>(savedPlanets));

		List<OrbitalSystem> orbitalSystems = new ArrayList<>(savedPlanets.size() * 3);

		for (Planet planet : savedPlanets) {
			int count = 1 + random.nextInt(6);

			for (int i = 1; i <= count; i++) {
				orbitalSystems.add(new OrbitalSystem(planet));
			}
		}

		orbitalSystemRepository.saveAll(orbitalSystems);

		return galaxyStructure;
	}

	private GalaxyData getGalaxyStructureData(String galaxyStructure) {
		int galaxyStructureFileSize = configProperties.getGalaxyStructureFileSize();

		try (InputStream stream = new ClassPathResource("/galaxies/" + galaxyStructure + ".txt").getInputStream();
			 BufferedReader reader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))) {

			PlanetContainer[][] containers = new PlanetContainer[galaxyStructureFileSize][];
			List<PlanetContainer> containerList = new ArrayList<>(galaxyStructureFileSize * galaxyStructureFileSize);

			for (int y = 0; y < galaxyStructureFileSize; y++) {
				String line = reader.readLine();
				char[] charArray = line.toCharArray();

				PlanetContainer[] row = new PlanetContainer[galaxyStructureFileSize];
				containers[y] = row;

				for (int x = 0; x < galaxyStructureFileSize; x++) {
					if (charArray[x] == '1') {
						PlanetContainer container = new PlanetContainer(x, y);
						row[x] = container;
						containerList.add(container);
					}
				}
			}

			containerList.parallelStream().forEach(container -> {
				int x = container.getX();
				int y = container.getY();

				addNeighbors(container, containers, x + 1, y);
				addNeighbors(container, containers, x + 1, y + 1);
				addNeighbors(container, containers, x + 1, y - 1);
				addNeighbors(container, containers, x - 1, y);
				addNeighbors(container, containers, x - 1, y + 1);
				addNeighbors(container, containers, x - 1, y - 1);
				addNeighbors(container, containers, x, y + 1);
				addNeighbors(container, containers, x, y - 1);
			});

			return new GalaxyData(containers, containerList);
		} catch (IOException e) {
			throw new IllegalStateException("Error while creating galaxy structure data.", e);
		}
	}

	private void addNeighbors(PlanetContainer center, PlanetContainer[][] containers, int x, int y) {
		int galaxyStructureFileSize = configProperties.getGalaxyStructureFileSize();
		if (x < 0 || y < 0 || x >= galaxyStructureFileSize || y >= galaxyStructureFileSize) {
			return;
		}

		PlanetContainer[] line = containers[y];

		if (line != null) {
			PlanetContainer container = line[x];

			if (container != null) {
				center.getNeighbors().add(container);
			}
		}
	}
}
