package org.skrupeltng.modules.dashboard.service;

import org.apache.commons.math3.random.MersenneTwister;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.masterdata.service.MasterDataService;

import java.awt.*;
import java.util.List;

public class PlayerColorAssigner {

	private static final List<String> STANDARD_COLORS = List.of("00ff00", "ff8100", "00ffff", "7f1eff", "1e90ff", "c71585", "ff0000", "228b22", "ffff00", "2424d9");

	private final List<Player> players;

	public PlayerColorAssigner(List<Player> players) {
		this.players = players;
	}

	public void assignPlayerColors() {
		if (players.size() <= STANDARD_COLORS.size()) {
			for (int i = 0; i < players.size(); i++) {
				players.get(i).setColor(STANDARD_COLORS.get(i));
			}
		} else {
			float interval = 1f / players.size();
			float hue = 0f;

			for (Player player : players) {
				Color cc = Color.getHSBColor(hue, 1f, 1f);

				if (cc.getRed() < 20 && cc.getBlue() > 230 && cc.getGreen() < 20) {
					MersenneTwister rand = MasterDataService.RANDOM;
					cc = new Color(100 + rand.nextInt(20), 230 - rand.nextInt(20), 100 + rand.nextInt(20));
				}

				String colorString = String.format("%02x%02x%02x", cc.getRed(), cc.getGreen(), cc.getBlue());
				player.setColor(colorString);
				hue += interval;
			}
		}
	}
}
