package org.skrupeltng.modules.dashboard;

public enum ResourceDensityHomePlanet {

	EXTREM(2000, 3000), HIGH(1500, 2500), MEDIUM(1000, 2000), LOW(500, 1000);

	private final int min;
	private final int max;

	private ResourceDensityHomePlanet(int min, int max) {
		this.min = min;
		this.max = max;
	}

	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}
}