package org.skrupeltng.modules.dashboard.modules.storymode.service.missions.silverstarag;

import org.skrupeltng.modules.ai.AIConstants;
import org.skrupeltng.modules.dashboard.modules.storymode.service.missions.StoryMissionTerritoryLevel;
import org.skrupeltng.modules.dashboard.modules.storymode.service.missions.StoryModeMissionHelper;
import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHole;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntryRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipRemoval;
import org.skrupeltng.modules.masterdata.database.FactionId;
import org.skrupeltng.modules.masterdata.service.MasterDataConstants;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Component
public class SilverStarAGMission3 {

	private static final Logger log = LoggerFactory.getLogger(SilverStarAGMission3.class);

	private static final int STEP_1_ORION_DISTANCE_HINT = 1;
	private static final int STEP_2_DESTROY_PLANETS = 2;
	private static final int STEP_3_DEFEND = 3;
	private static final int STEP_4_ACCEPTANCE = 4;
	private static final int STEP_5_ESCAPE = 5;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private ShipRemoval shipRemoval;

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Autowired
	private WormHoleRepository wormHoleRepository;

	@Autowired
	private NewsEntryRepository newsEntryRepository;

	@Autowired
	private StoryModeMissionHelper missionHelper;

	public void finalizeGame(long gameId) {
		List<Player> players = playerRepository.findByGameId(gameId);

		Player humanPlayer = missionHelper.getHumanPlayer(players);
		Player orionPlayer = missionHelper.getAIPlayer(players, FactionId.orion);
		Player nightmarePlayer = missionHelper.getAIPlayer(players, FactionId.nightmare);

		Game game = humanPlayer.getGame();
		int galaxySize = game.getGalaxySize();

		missionHelper.setupPlayer(gameId, humanPlayer, galaxySize / 2 + 100, 200, 500, StoryMissionTerritoryLevel.HIGH, "silverstarag_10", "psion_flow_net",
			20);
		missionHelper.setupPlayer(gameId, orionPlayer, 150, galaxySize / 2 + 100, 400, StoryMissionTerritoryLevel.HIGH, "orion_3", "solarisplasmotan", 15);

		int bottomRight = (galaxySize / 4) * 3;
		List<Planet> enemyPlanets = missionHelper.setupPlayer(gameId, nightmarePlayer, bottomRight, bottomRight, 300, StoryMissionTerritoryLevel.LOW,
			"nightmare_13", "solarisplasmotan", 8);

		spawnScout(humanPlayer, enemyPlanets);
		WormHole wormHole = new WormHole();
		wormHole.setGame(game);
		wormHole.setType(WormHoleType.UNSTABLE_WORMHOLE);
		wormHole.setX(game.getGalaxySize() - 30);
		wormHole.setY(game.getGalaxySize() - 30);
		wormHoleRepository.save(wormHole);
	}

	private void spawnScout(Player humanPlayer, List<Planet> enemyPlanets) {
		enemyPlanets.sort(new CoordinateDistanceComparator(humanPlayer.getHomePlanet()));
		Planet nearestEnemyPlanet = enemyPlanets.get(0);
		int x = nearestEnemyPlanet.getX() - 70;
		int y = nearestEnemyPlanet.getY() - 70;

		Ship scout = missionHelper.spawnShip("silverstarag_8", "psion_flow_net", "desintegrator", "mun_catapult", x, y, humanPlayer, "Infiltrator", null);
		scout.setFuel((scout.getShipTemplate().getFuelCapacity() / 4) * 3);
		scout.setMineral2(scout.getShipTemplate().getStorageSpace());

		shipRepository.save(scout);
	}

	public void checkRoundStart(Game game) {
		List<Player> players = game.getPlayers();

		if (game.getStoryModeMissionStep() < STEP_3_DEFEND) {
			inhibitHumanPlayerEnteringOrionSpace(players);
		} else {
			nightmareShipsExodus(game, players);
		}
	}

	private void inhibitHumanPlayerEnteringOrionSpace(List<Player> players) {
		Player humanPlayer = missionHelper.getHumanPlayer(players);
		Player orionPlayer = missionHelper.getAIPlayer(players, FactionId.orion);

		Set<Ship> ships = humanPlayer.getShips();
		Set<Planet> planets = orionPlayer.getPlanets();

		for (Ship ship : ships) {
			Coordinate destinationCoord = new CoordinateImpl(ship.getDestinationX(), ship.getDestinationY(), 0);

			for (Planet planet : planets) {
				if (planet.getDistance(destinationCoord) < 60) {
					ship.resetTravel();
					shipRepository.save(ship);
					break;
				}
			}
		}
	}

	private void nightmareShipsExodus(Game game, List<Player> players) {
		Player nightmarePlayer = missionHelper.getAIPlayer(players, FactionId.nightmare);
		WormHole wormHole = getWormhole(game);

		Set<Ship> ships = nightmarePlayer.getShips();

		for (Ship ship : ships) {
			if (ship.getDistance(wormHole) < WormHole.WORMHOLE_REACH) {
				shipRemoval.deleteShip(ship, null, false);
			} else {
				ship.resetTask();
				ship.setDestinationX(wormHole.getX());
				ship.setDestinationY(wormHole.getY());
				ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
				shipRepository.save(ship);
			}
		}
	}

	public Optional<Integer> checkRoundEnd(Game game) {
		switch (game.getStoryModeMissionStep()) {
			case STEP_1_ORION_DISTANCE_HINT:
				missionHelper.setNextStep(game, STEP_2_DESTROY_PLANETS);
				break;
			case STEP_2_DESTROY_PLANETS:
				checkStep1AlmostAllPlanetsDestroyed(game);
				break;
			case STEP_3_DEFEND:
				checkStep3Defended(game);
				break;
			case STEP_4_ACCEPTANCE:
				checkStep4Acceptance(game);
				break;
			case STEP_5_ESCAPE:
				return checkStep5Escaped(game);
			default:
				log.error("Unknown story mission step {}", game.getStoryModeMissionStep());
		}

		return Optional.empty();
	}

	private void checkStep1AlmostAllPlanetsDestroyed(Game game) {
		Player nightmarePlayer = missionHelper.getAIPlayer(game.getPlayers(), FactionId.nightmare);

		if (nightmarePlayer.getPlanets().size() < 2) {
			missionHelper.setNextStep(game, STEP_3_DEFEND);

			Player humanPlayer = missionHelper.getHumanPlayer(game.getPlayers());
			playerRelationRepository.deleteByPlayerId(humanPlayer.getId());
		}
	}

	private void checkStep3Defended(Game game) {
		updateOrionAttacks(game);
		Player humanPlayer = missionHelper.getHumanPlayer(game.getPlayers());

		List<NewsEntry> news = newsEntryRepository.findByGameIdAndLoginId(game.getId(), humanPlayer.getLogin().getId());

		boolean initNextStep = news.stream()
			.anyMatch(n -> n.getTemplate().equals(NewsEntryConstants.news_entry_planet_got_bombed_single) ||
				n.getTemplate().equals(NewsEntryConstants.news_entry_planet_got_bombed_multiple));

		if (initNextStep) {
			missionHelper.setNextStep(game, STEP_4_ACCEPTANCE);
			game.setStoryModeMissionData("1");
		}
	}

	private void checkStep4Acceptance(Game game) {
		updateOrionAttacks(game);
		missionHelper.setNextStep(game, STEP_5_ESCAPE);
	}

	private Optional<Integer> checkStep5Escaped(Game game) {
		Player humanPlayer = missionHelper.getHumanPlayer(game.getPlayers());
		WormHole wormHole = getWormhole(game);

		if (humanPlayer.getShips().stream()
			.anyMatch(s -> s.getDistance(wormHole) <= 15 && s.getColonists() >= 100 && s.getMoney() >= 100)) {
			missionHelper.finishMission(game, true);
			return Optional.of(3);
		} else if (humanPlayer.getShips().isEmpty() && humanPlayer.getPlanets().isEmpty()) {
			missionHelper.finishMission(game, false);
		} else {
			updateOrionAttacks(game);
		}

		return Optional.empty();
	}

	private WormHole getWormhole(Game game) {
		return wormHoleRepository.findByGameId(game.getId()).get(0);
	}

	private void updateOrionAttacks(Game game) {
		Player orionPlayer = missionHelper.getAIPlayer(game.getPlayers(), FactionId.orion);

		long invadingShips = orionPlayer.getShips().stream().filter(s -> s.getShipTemplate().getId().equals("orion_11")).count();

		if (invadingShips < 6L) {
			int nextCountPerPlanet = 1;

			if (game.getStoryModeMissionStep() >= STEP_4_ACCEPTANCE) {
				nextCountPerPlanet = Integer.parseInt(game.getStoryModeMissionData());
				game.setStoryModeMissionData((nextCountPerPlanet + 1) + "");
			}

			Set<Planet> planets = orionPlayer.getPlanets();

			for (Planet planet : planets) {
				int x = planet.getX();
				int y = planet.getY();

				for (int i = 0; i < nextCountPerPlanet; i++) {
					String name = MasterDataService.RANDOM.nextBoolean() ? AIConstants.AI_BOMBER_NAME : AIConstants.AI_AMBUSHER_NAME;

					Ship ship = missionHelper.spawnShip("orion_11", "transwarp", "particle_vortex", "nova_bombs", x, y, orionPlayer, name, planet);
					ship.setFuel(ship.getShipTemplate().getFuelCapacity());
					ship.addProjectiles(MasterDataConstants.PROJECTILES_PER_WEAPON * ship.getShipTemplate().getProjectileWeaponsCount());
					shipRepository.save(ship);
				}
			}
		}
	}

	public void verifyMissionCreation(long gameId) {
		List<Player> players = playerRepository.findByGameId(gameId);

		Player orionPlayer = missionHelper.getAIPlayer(players, FactionId.orion);
		Objects.requireNonNull(orionPlayer.getHomePlanet());

		Player nightmarePlayer = missionHelper.getAIPlayer(players, FactionId.nightmare);
		Objects.requireNonNull(nightmarePlayer.getHomePlanet());

		Player humanPlayer = missionHelper.getHumanPlayer(players);
		Objects.requireNonNull(humanPlayer.getHomePlanet());
	}
}
