package org.skrupeltng.modules.dashboard.modules.matchmaking.service;

import org.skrupeltng.config.UserDetailServiceImpl;
import org.springframework.stereotype.Service;

@Service("matchMakingInfoService")
public class MatchMakingInfoService {

	private final UserDetailServiceImpl userDetailService;
	private final MatchMakingService matchMakingService;

	public MatchMakingInfoService(UserDetailServiceImpl userDetailService,
								  MatchMakingService matchMakingService) {
		this.userDetailService = userDetailService;
		this.matchMakingService = matchMakingService;
	}

	public boolean matchMakingEnabled() {
		return matchMakingService.getOrCreateMatchMakingData(userDetailService.getLoginId()).isActive();
	}
}
