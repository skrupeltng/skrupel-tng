package org.skrupeltng.modules.dashboard.modules.matchmaking.service;

public record MatchMakingValueItem(String value,
								   String displayValue) {

	public MatchMakingValueItem(String value) {
		this(value, value);
	}
}
