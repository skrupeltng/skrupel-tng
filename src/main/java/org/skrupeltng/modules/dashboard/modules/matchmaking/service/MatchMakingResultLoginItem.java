package org.skrupeltng.modules.dashboard.modules.matchmaking.service;

public record MatchMakingResultLoginItem(long loginId, String factionId) {
}
