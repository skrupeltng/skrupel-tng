package org.skrupeltng.modules.dashboard.modules.storymode.service.missions.silverstarag;

import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.dashboard.modules.storymode.service.StoryModeMissionProvider;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.masterdata.database.FactionId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component("silverstarag")
public class SilverStarAGMissionProvider extends StoryModeMissionProvider {

	private static final Logger log = LoggerFactory.getLogger(SilverStarAGMissionProvider.class);

	private final SilverStarAGMission1 mission1;
	private final SilverStarAGMission2 mission2;
	private final SilverStarAGMission3 mission3;

	public SilverStarAGMissionProvider(SilverStarAGMission1 mission1, SilverStarAGMission2 mission2, SilverStarAGMission3 mission3) {
		this.mission1 = mission1;
		this.mission2 = mission2;
		this.mission3 = mission3;
	}

	@Override
	public NewGameRequest getNewGameRequest(int missionNumber) {
		return switch (missionNumber) {
			case 1 -> getNewGameRequestMission1();
			case 2 -> getNewGameRequestMission2();
			case 3 -> getNewGameRequestMission3();
			default -> null;
		};
	}

	private NewGameRequest getNewGameRequestMission1() {
		NewGameRequest newGameRequest = NewGameRequest.createDefaultRequest();
		newGameRequest.setName("Mission 1");
		newGameRequest.setPlayerCount(2);
		newGameRequest.setGalaxyConfigId("gala_6");
		newGameRequest.setInhabitedPlanetsPercentage(0f);
		newGameRequest.setGalaxySize(1250);
		return newGameRequest;
	}

	private NewGameRequest getNewGameRequestMission2() {
		NewGameRequest newGameRequest = NewGameRequest.createDefaultRequest();
		newGameRequest.setName("Mission 2");
		newGameRequest.setPlayerCount(3);
		newGameRequest.setGalaxyConfigId("gala_9");
		newGameRequest.setGalaxySize(750);
		return newGameRequest;
	}

	private NewGameRequest getNewGameRequestMission3() {
		NewGameRequest newGameRequest = NewGameRequest.createDefaultRequest();
		newGameRequest.setName("Mission 3");
		newGameRequest.setPlayerCount(3);
		newGameRequest.setGalaxyConfigId("gala_0");
		newGameRequest.setGalaxySize(1000);
		return newGameRequest;
	}

	@Override
	public FactionId getFactionId() {
		return FactionId.silverstarag;
	}

	@Override
	public List<FactionId> getEnemyAIFactions(int missionNumber) {
		return List.of(FactionId.nightmare);
	}

	@Override
	public List<FactionId> getAlliedAIFactions(int missionNumber) {
		switch (missionNumber) {
			case 1:
				return Collections.emptyList();
			case 2, 3:
				return List.of(FactionId.orion);
			default:
				log.error("Unable to retrieve allied AI factions for unknown mission number {}", missionNumber);
		}

		return Collections.emptyList();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void finalizeGame(long gameId, int missionNumber) {
		switch (missionNumber) {
			case 1 -> mission1.finalizeGame(gameId);
			case 2 -> mission2.finalizeGame(gameId);
			case 3 -> mission3.finalizeGame(gameId);
			default -> log.error("Unable to finalize unknown mission number {} for game {}", missionNumber, gameId);
		}
	}

	@Override
	public void checkRoundStart(Game game) {
		if (game.getStoryModeMission() == 3) {
			mission3.checkRoundStart(game);
		}
	}

	@Override
	public Optional<Integer> checkRoundEnd(Game game) {
		return switch (game.getStoryModeMission()) {
			case 1 -> mission1.checkRoundEnd(game);
			case 2 -> mission2.checkRoundEnd(game);
			case 3 -> mission3.checkRoundEnd(game);
			default -> Optional.empty();
		};
	}

	@Override
	public Optional<String> getNextCampaignFaction() {
		// The next campaign will be orion
		return Optional.empty();
	}

	@Override
	protected int getMissionCount() {
		return 3;
	}

	@Override
	protected int getSortOrder() {
		return 0;
	}

	@Override
	public boolean hasDisabledWormholes(long gameId) {
		return true;
	}

	@Override
	public void verifyMissionCreation(int missionNumber, long gameId) {
		switch (missionNumber) {
			case 1 -> mission1.verifyMissionCreation(gameId);
			case 2 -> mission2.verifyMissionCreation(gameId);
			case 3 -> mission3.verifyMissionCreation(gameId);
			default -> log.error("Unable to verify unknown mission number {} for game {}", missionNumber, gameId);
		}
	}
}
