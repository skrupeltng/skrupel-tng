package org.skrupeltng.modules.dashboard.modules.matchmaking.controller.dto;

import java.util.List;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class MatchMakingDataDTO {

	@NotNull
	private String factionId;
	@NotNull
	@Size(min = 1)
	private List<MatchMakingCriteriaItemDTO> playerCountItems;
	@NotNull
	@Size(min = 1)
	private List<MatchMakingCriteriaItemDTO> coopItems;
	@NotNull
	@Size(min = 1)
	private List<MatchMakingCriteriaItemDTO> gameModeItems;
	@NotNull
	@Size(min = 1)
	private List<MatchMakingCriteriaItemDTO> galaxySizeItems;
	@NotNull
	private List<MatchMakingCriteriaItemDTO> botCountItems;
	private boolean active;

	public MatchMakingDataDTO() {

	}

	public MatchMakingDataDTO(String factionId,
							  List<MatchMakingCriteriaItemDTO> playerCountItems,
							  List<MatchMakingCriteriaItemDTO> coopItems,
							  List<MatchMakingCriteriaItemDTO> gameModeItems,
							  List<MatchMakingCriteriaItemDTO> galaxySizeItems,
							  List<MatchMakingCriteriaItemDTO> botCountItems,
							  boolean active) {
		this.factionId = factionId;
		this.playerCountItems = playerCountItems;
		this.coopItems = coopItems;
		this.gameModeItems = gameModeItems;
		this.galaxySizeItems = galaxySizeItems;
		this.botCountItems = botCountItems;
		this.active = active;
	}

	public String getFactionId() {
		return factionId;
	}

	public void setFactionId(String factionId) {
		this.factionId = factionId;
	}

	public List<MatchMakingCriteriaItemDTO> getPlayerCountItems() {
		return playerCountItems;
	}

	public void setPlayerCountItems(List<MatchMakingCriteriaItemDTO> playerCountItems) {
		this.playerCountItems = playerCountItems;
	}

	public List<MatchMakingCriteriaItemDTO> getCoopItems() {
		return coopItems;
	}

	public void setCoopItems(List<MatchMakingCriteriaItemDTO> coopItems) {
		this.coopItems = coopItems;
	}

	public List<MatchMakingCriteriaItemDTO> getGameModeItems() {
		return gameModeItems;
	}

	public void setGameModeItems(List<MatchMakingCriteriaItemDTO> gameModeItems) {
		this.gameModeItems = gameModeItems;
	}

	public List<MatchMakingCriteriaItemDTO> getGalaxySizeItems() {
		return galaxySizeItems;
	}

	public void setGalaxySizeItems(List<MatchMakingCriteriaItemDTO> galaxySizeItems) {
		this.galaxySizeItems = galaxySizeItems;
	}

	public List<MatchMakingCriteriaItemDTO> getBotCountItems() {
		return botCountItems;
	}

	public void setBotCountItems(List<MatchMakingCriteriaItemDTO> botCountItems) {
		this.botCountItems = botCountItems;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "MatchMakingDataDTO{" +
			"factionId='" + factionId + '\'' +
			", playerCountItems=" + playerCountItems +
			", coopItems=" + coopItems +
			", gameModeItems=" + gameModeItems +
			", galaxySizeItems=" + galaxySizeItems +
			", botCountItems=" + botCountItems +
			", active=" + active +
			'}';
	}
}
