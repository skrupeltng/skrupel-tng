package org.skrupeltng.modules.dashboard.modules.matchmaking.service;

import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingEntry;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public record MatchMakingFilterResult(List<MatchMakingEntry> entriesWithMatches,
									  Set<String> matchedValues) {

	public MatchMakingFilterResult() {
		this(Collections.emptyList(), Collections.emptySet());
	}
}
