package org.skrupeltng.modules.dashboard.modules.admin.controller;

import java.io.Serializable;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class DataPrivacyStatementsData implements Serializable {

	private static final long serialVersionUID = 3473398472468765587L;

	@NotNull
	@NotBlank
	private String textEnglish;

	@NotNull
	@NotBlank
	private String textGerman;

	public String getTextEnglish() {
		return textEnglish;
	}

	public void setTextEnglish(String textEnglish) {
		this.textEnglish = textEnglish;
	}

	public String getTextGerman() {
		return textGerman;
	}

	public void setTextGerman(String textGerman) {
		this.textGerman = textGerman;
	}
}
