package org.skrupeltng.modules.dashboard.modules.storymode.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface StoryModeCampaignRepository extends JpaRepository<StoryModeCampaign, Long> {

	@Query("SELECT c FROM StoryModeCampaign c WHERE c.login.id = ?1 ORDER BY c.sortOrder ASC")
	List<StoryModeCampaign> findByLoginId(long loginId);

	StoryModeCampaign findByLoginIdAndFactionId(long loginId, String factionId);

	@Modifying
	@Query("UPDATE Game g SET g.storyModeCampaign = null WHERE g.creator.id = ?1")
	void clearStoryModeCampaignsFromGames(long loginId);

	@Modifying
	@Query("DELETE FROM StoryModeCampaign c WHERE c.login.id = ?1")
	void deleteByLoginId(long loginId);
}