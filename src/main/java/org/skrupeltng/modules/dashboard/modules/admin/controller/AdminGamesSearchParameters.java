package org.skrupeltng.modules.dashboard.modules.admin.controller;

import org.skrupeltng.modules.PagingRequest;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.springframework.web.bind.annotation.RequestParam;

public record AdminGamesSearchParameters(@RequestParam(required = false) String name,
										 @RequestParam(required = false) WinCondition winCondition,
										 @RequestParam(required = false) String creatorName,
										 @RequestParam(required = false) Boolean finished,
										 @RequestParam(required = false) Integer page,
										 @RequestParam(required = false) Integer pageSize,
										 @RequestParam(required = false) String sorting) implements PagingRequest {

}
