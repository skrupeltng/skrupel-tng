package org.skrupeltng.modules.dashboard.modules.admin.controller;

public class DiscordLinkChangeRequestDTO {

	private String discordLink;

	public DiscordLinkChangeRequestDTO() {

	}

	public DiscordLinkChangeRequestDTO(String discordLink) {
		this.discordLink = discordLink;
	}

	public String getDiscordLink() {
		return discordLink;
	}

	public void setDiscordLink(String discordLink) {
		this.discordLink = discordLink;
	}
}