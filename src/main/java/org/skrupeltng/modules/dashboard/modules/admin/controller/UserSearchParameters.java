package org.skrupeltng.modules.dashboard.modules.admin.controller;

import org.skrupeltng.modules.PagingRequest;
import org.springframework.web.bind.annotation.RequestParam;

public record UserSearchParameters(@RequestParam(required = false) String username,
								   @RequestParam(required = false) Boolean createdGames,
								   @RequestParam(required = false) Boolean playedGames,
								   @RequestParam(required = false) Boolean matchMakingEnabled,
								   @RequestParam(required = false) Integer page,
								   @RequestParam(required = false) Integer pageSize,
								   @RequestParam(required = false) String sorting) implements PagingRequest {

}
