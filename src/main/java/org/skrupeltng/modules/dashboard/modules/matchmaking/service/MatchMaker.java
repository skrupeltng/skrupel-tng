package org.skrupeltng.modules.dashboard.modules.matchmaking.service;

import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingCriteriaItem;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingCriteriaType;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingEntry;

import java.util.*;
import java.util.stream.Collectors;

public class MatchMaker {

	private final EnumMap<MatchMakingCriteriaType, Set<String>> userValuesMap = new EnumMap<>(MatchMakingCriteriaType.class);

	private final List<MatchMakingEntry> entries;
	private final List<Integer> allowedPlayerCounts;

	public MatchMaker(MatchMakingEntry userEntry, List<MatchMakingEntry> entries, List<Integer> allowedPlayerCounts) {
		this.entries = entries;
		this.allowedPlayerCounts = allowedPlayerCounts;

		for (MatchMakingCriteriaItem item : userEntry.getItems()) {
			Set<String> values = userValuesMap.computeIfAbsent(item.getType(), t -> new HashSet<>());
			values.add(item.getValue());
		}
	}

	public MatchMakingResult findMatches() {
		List<MatchMakingEntry> remainingEntries = filterByPlayerCount();

		if (remainingEntries == null) {
			return new MatchMakingResult();
		}

		MatchMakingEntryFilter filter = new MatchMakingEntryFilter(userValuesMap);

		for (MatchMakingCriteriaType type : MatchMakingCriteriaType.values()) {
			if (type == MatchMakingCriteriaType.PLAYER_COUNT) {
				continue;
			}

			MatchMakingFilterResult result = filter.filter(remainingEntries, type);
			remainingEntries = result.entriesWithMatches();

			if (remainingEntries.isEmpty()) {
				return new MatchMakingResult();
			}
		}

		return createMatchMakingResult(filter, remainingEntries);
	}

	private List<MatchMakingEntry> filterByPlayerCount() {
		Map<MatchMakingEntry, Set<Integer>> entryToPlayerCountsMap = createEntryToPlayerCountsMap();

		List<MatchMakingEntry> resultEntries = null;

		Set<Integer> userPlayerCounts = userValuesMap.get(MatchMakingCriteriaType.PLAYER_COUNT).stream()
			.map(Integer::parseInt)
			.collect(Collectors.toSet());

		for (Integer playerCount : allowedPlayerCounts) {
			if (!userPlayerCounts.contains(playerCount)) {
				continue;
			}

			List<MatchMakingEntry> possibleEntries = new ArrayList<>(playerCount);

			for (Map.Entry<MatchMakingEntry, Set<Integer>> item : entryToPlayerCountsMap.entrySet()) {
				MatchMakingEntry entry = item.getKey();
				Set<Integer> playerCounts = item.getValue();

				if (playerCounts.contains(playerCount)) {
					possibleEntries.add(entry);
				}
			}

			if (possibleEntries.size() + 1 >= playerCount) {
				resultEntries = possibleEntries;
			}
		}

		return resultEntries;
	}

	private Map<MatchMakingEntry, Set<Integer>> createEntryToPlayerCountsMap() {
		Map<MatchMakingEntry, Set<Integer>> entryToPlayerCountsMap = new HashMap<>();

		for (MatchMakingEntry entry : entries) {
			Set<Integer> list = entry.createItemsMap().get(MatchMakingCriteriaType.PLAYER_COUNT).stream()
				.map(v -> Integer.parseInt(v.getValue()))
				.collect(Collectors.toSet());

			entryToPlayerCountsMap.put(entry, list);
		}

		return entryToPlayerCountsMap;
	}

	private MatchMakingResult createMatchMakingResult(MatchMakingEntryFilter filter, List<MatchMakingEntry> remainingEntries) {
		EnumMap<MatchMakingCriteriaType, Set<String>> matchesValuesMap = new EnumMap<>(MatchMakingCriteriaType.class);

		for (MatchMakingCriteriaType type : MatchMakingCriteriaType.values()) {
			MatchMakingFilterResult result = filter.filter(remainingEntries, type);
			matchesValuesMap.put(type, result.matchedValues());
		}

		List<MatchMakingResultLoginItem> loginItems = remainingEntries.stream()
			.map(e -> new MatchMakingResultLoginItem(e.getLogin().getId(), e.getFactionId()))
			.toList();

		EnumMap<MatchMakingCriteriaType, String> criteria = new EnumMap<>(MatchMakingCriteriaType.class);

		for (Map.Entry<MatchMakingCriteriaType, Set<String>> entry : matchesValuesMap.entrySet()) {
			List<String> values = new ArrayList<>(entry.getValue());
			Collections.shuffle(values);
			String value = values.get(0);
			criteria.put(entry.getKey(), value);
		}

		return new MatchMakingResult(criteria, loginItems);
	}
}
