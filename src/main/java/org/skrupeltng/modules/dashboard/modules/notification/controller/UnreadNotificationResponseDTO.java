package org.skrupeltng.modules.dashboard.modules.notification.controller;

import java.io.Serializable;

public class UnreadNotificationResponseDTO implements Serializable {

	private static final long serialVersionUID = -6283704921892048533L;

	private long unreadCount;

	private String message;
	private String link;
	private Long desktopNotificationId;

	public long getUnreadCount() {
		return unreadCount;
	}

	public void setUnreadCount(long unreadCount) {
		this.unreadCount = unreadCount;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Long getDesktopNotificationId() {
		return desktopNotificationId;
	}

	public void setDesktopNotificationId(Long desktopNotificationId) {
		this.desktopNotificationId = desktopNotificationId;
	}
}