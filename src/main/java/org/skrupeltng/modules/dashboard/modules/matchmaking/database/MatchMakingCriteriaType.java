package org.skrupeltng.modules.dashboard.modules.matchmaking.database;

public enum MatchMakingCriteriaType {

	PLAYER_COUNT,
	COOP,
	GAME_MODE,
	GALAXY_SIZE,
	BOT_COUNT
}
