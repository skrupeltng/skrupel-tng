package org.skrupeltng.modules.dashboard.modules.matchmaking.controller;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.skrupeltng.modules.dashboard.modules.matchmaking.controller.dto.MatchMakingCriteriaItemDTO;
import org.skrupeltng.modules.dashboard.modules.matchmaking.controller.dto.MatchMakingDataDTO;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class MatchMakingValidator implements Validator {

	@Override
	public boolean supports(@NotNull Class<?> clazz) {
		return MatchMakingDataDTO.class.equals(clazz);
	}

	@Override
	public void validate(@NotNull Object target, @NotNull Errors errors) {
		MatchMakingDataDTO request = (MatchMakingDataDTO) target;

		checkItems(request.getPlayerCountItems(), errors, "playerCountItems");
		checkItems(request.getGameModeItems(), errors, "gameModeItems");
		checkItems(request.getCoopItems(), errors, "coopItems");
		checkItems(request.getGalaxySizeItems(), errors, "galaxySizeItems");
		checkItems(request.getBotCountItems(), errors, "botCountItems");
	}

	private void checkItems(List<MatchMakingCriteriaItemDTO> items, Errors errors, String fieldName) {
		if (items.stream().noneMatch(MatchMakingCriteriaItemDTO::isSelected)) {
			errors.rejectValue(fieldName, "at_least_on_item_must_be_selected");
		}
	}
}
