package org.skrupeltng.modules.dashboard.modules.matchmaking.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingCriteriaItem;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingCriteriaType;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingEntry;

public class MatchMakingEntryFilter {

	private final Map<MatchMakingCriteriaType, Set<String>> userValuesMap;

	public MatchMakingEntryFilter(Map<MatchMakingCriteriaType, Set<String>> userValuesMap) {
		this.userValuesMap = userValuesMap;
	}

	public MatchMakingFilterResult filter(List<MatchMakingEntry> entries, MatchMakingCriteriaType type) {
		Set<String> userValues = userValuesMap.get(type);

		if (userValues == null) {
			return new MatchMakingFilterResult();
		}

		Set<String> matchedValues = new HashSet<>(userValues);

		List<MatchMakingEntry> entriesWithMatches = new ArrayList<>(entries.size());

		for (MatchMakingEntry entry : entries) {
			Set<String> entryValues = entry.getItems().stream()
				.filter(i -> i.getType() == type && matchedValues.contains(i.getValue()))
				.map(MatchMakingCriteriaItem::getValue)
				.collect(Collectors.toSet());

			if (!entryValues.isEmpty()) {
				matchedValues.retainAll(entryValues);
				entriesWithMatches.add(entry);
			}
		}

		return new MatchMakingFilterResult(entriesWithMatches, matchedValues);
	}
}
