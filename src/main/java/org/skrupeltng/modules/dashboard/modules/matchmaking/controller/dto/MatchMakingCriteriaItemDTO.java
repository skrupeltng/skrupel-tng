package org.skrupeltng.modules.dashboard.modules.matchmaking.controller.dto;

import jakarta.validation.constraints.NotNull;

public class MatchMakingCriteriaItemDTO {

	@NotNull
	private String value;
	private String displayValue;
	private boolean selected;

	public MatchMakingCriteriaItemDTO() {

	}

	public MatchMakingCriteriaItemDTO(String value, String displayValue, boolean selected) {
		this.value = value;
		this.displayValue = displayValue;
		this.selected = selected;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	@Override
	public String toString() {
		return "MatchMakingCriteriaItemDTO{" +
			"value='" + value + '\'' +
			", displayValue='" + displayValue + '\'' +
			", selected=" + selected +
			'}';
	}
}
