package org.skrupeltng.modules.dashboard.modules.storymode.service.missions;

public enum StoryMissionTerritoryLevel {

	MAXED(70000, 1800, 2000, 100, 400, 10, 10, 10),

	VERY_HIGH(60000, 1500, 1000, 70, 250, 9, 9, 8),

	HIGH(55000, 1300, 800, 60, 200, 8, 8, 6),

	MEDIUM(50000, 1200, 600, 30, 120, 7, 7, 4),

	LOW(45000, 1100, 400, 15, 80, 6, 7, 3),

	VERY_LOW(40000, 1000, 200, 5, 50, 4, 6, 2);

	private final int mainColonists;
	private final int colonists;
	private final int money;
	private final int supplies;
	private final int minerals;
	private final int hullTechLevel;
	private final int propulsionTechLevel;
	private final int weaponsTechLevel;

	private StoryMissionTerritoryLevel(int mainColonists, int colonists, int money, int supplies, int minerals, int hullTechLevel, int propulsionTechLevel,
			int weaponsTechLevel) {
		this.mainColonists = mainColonists;
		this.colonists = colonists;
		this.money = money;
		this.supplies = supplies;
		this.minerals = minerals;
		this.hullTechLevel = hullTechLevel;
		this.propulsionTechLevel = propulsionTechLevel;
		this.weaponsTechLevel = weaponsTechLevel;
	}

	public int getMainColonists() {
		return mainColonists;
	}

	public int getColonists() {
		return colonists;
	}

	public int getMoney() {
		return money;
	}

	public int getSupplies() {
		return supplies;
	}

	public int getMinerals() {
		return minerals;
	}

	public int getHullTechLevel() {
		return hullTechLevel;
	}

	public int getPropulsionTechLevel() {
		return propulsionTechLevel;
	}

	public int getWeaponsTechLevel() {
		return weaponsTechLevel;
	}
}