package org.skrupeltng.modules.dashboard.modules.storymode.service;

public class StoryModeMissionItem {

	private int missionNumber;
	private boolean unlocked;
	private boolean completed;
	private boolean highestUnlockedMission;
	private long gameId;

	public int getMissionNumber() {
		return missionNumber;
	}

	public void setMissionNumber(int missionNumber) {
		this.missionNumber = missionNumber;
	}

	public boolean isUnlocked() {
		return unlocked;
	}

	public void setUnlocked(boolean unlocked) {
		this.unlocked = unlocked;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public boolean isHighestUnlockedMission() {
		return highestUnlockedMission;
	}

	public void setHighestUnlockedMission(boolean highestUnlockedMission) {
		this.highestUnlockedMission = highestUnlockedMission;
	}

	public long getGameId() {
		return gameId;
	}

	public void setGameId(long gameId) {
		this.gameId = gameId;
	}
}