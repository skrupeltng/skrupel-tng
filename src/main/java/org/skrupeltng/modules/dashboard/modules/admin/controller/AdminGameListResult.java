package org.skrupeltng.modules.dashboard.modules.admin.controller;

import org.skrupeltng.modules.PageableResult;

import java.time.Instant;

public class AdminGameListResult implements PageableResult {

	private long id;
	private String name;
	private Instant created;
	private String creator;
	private Instant roundDate;
	private String players;
	private int round;
	private boolean finished;
	private String winCondition;

	private int totalElements;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Instant getCreated() {
		return created;
	}

	public void setCreated(Instant created) {
		this.created = created;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Instant getRoundDate() {
		return roundDate;
	}

	public void setRoundDate(Instant roundDate) {
		this.roundDate = roundDate;
	}

	public String getPlayers() {
		return players;
	}

	public void setPlayers(String players) {
		this.players = players;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public String getWinCondition() {
		return winCondition;
	}

	public void setWinCondition(String winCondition) {
		this.winCondition = winCondition;
	}

	@Override
	public int getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}
}
