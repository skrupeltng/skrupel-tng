package org.skrupeltng.modules.dashboard.modules.matchmaking.service;

import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingCriteriaType;
import org.skrupeltng.modules.ingame.database.WinCondition;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

@Component
public class AllowedMatchMakingValues {

	private final MessageSource messageSource;

	public AllowedMatchMakingValues(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public List<MatchMakingValueItem> getAllowedValues(MatchMakingCriteriaType type) {
		Locale locale = LocaleContextHolder.getLocale();

		return switch (type) {
			case PLAYER_COUNT -> toValueItems("2", "4", "6");
			case BOT_COUNT -> toValueItems("0", "2", "4", "6");
			case COOP -> List.of(
				new MatchMakingValueItem("true", messageSource.getMessage("yes", null, locale)),
				new MatchMakingValueItem("false", messageSource.getMessage("no", null, locale))
			);
			case GAME_MODE -> Stream.of(WinCondition.values())
				.map(w -> new MatchMakingValueItem(w.name(), messageSource.getMessage("win_condition_" + w.name(), null, locale)))
				.toList();
			case GALAXY_SIZE -> toValueItems("500", "1000", "2000", "4000");
		};
	}

	private List<MatchMakingValueItem> toValueItems(String... values) {
		return Stream.of(values).map(MatchMakingValueItem::new).toList();
	}
}
