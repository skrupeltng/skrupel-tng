package org.skrupeltng.modules.dashboard.modules.matchmaking.database;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import org.skrupeltng.modules.dashboard.database.Login;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "match_making_entry")
public class MatchMakingEntry implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private final Instant created;

	@ManyToOne(fetch = FetchType.LAZY)
	private Login login;

	@Column
	private boolean active;

	@Column(name = "faction_id")
	private String factionId;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "matchMakingEntry")
	private List<MatchMakingCriteriaItem> items;

	public MatchMakingEntry() {
		created = Instant.now();
	}

	public Map<MatchMakingCriteriaType, List<MatchMakingCriteriaItem>> createItemsMap() {
		EnumMap<MatchMakingCriteriaType, List<MatchMakingCriteriaItem>> criteriaItemMap = new EnumMap<>(MatchMakingCriteriaType.class);

		for (MatchMakingCriteriaItem item : items) {
			criteriaItemMap.computeIfAbsent(item.getType(), type -> new ArrayList<>()).add(item);
		}

		return criteriaItemMap;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Instant getCreated() {
		return created;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getFactionId() {
		return factionId;
	}

	public void setFactionId(String factionId) {
		this.factionId = factionId;
	}

	public List<MatchMakingCriteriaItem> getItems() {
		return items;
	}

	public void setItems(List<MatchMakingCriteriaItem> items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "MatchMakingEntry{" +
			"id=" + id +
			", active=" + active +
			", factionId='" + factionId + '\'' +
			'}';
	}
}
