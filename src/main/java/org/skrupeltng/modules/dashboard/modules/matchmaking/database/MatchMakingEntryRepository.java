package org.skrupeltng.modules.dashboard.modules.matchmaking.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface MatchMakingEntryRepository extends JpaRepository<MatchMakingEntry, Long>, MatchMakingEntryRepositoryCustom {

	@Query("""
            SELECT
            	e
            FROM
            	MatchMakingEntry e
            	INNER JOIN FETCH e.items i
            WHERE
            	e.id IN (?1)""")
    List<MatchMakingEntry> getByIdsForMatchMaking(List<Long> ids);

	@Query("""
            SELECT
            	e
            FROM
            	MatchMakingEntry e
            	INNER JOIN FETCH e.items i
            WHERE
            	e.login.id = ?1""")
    Optional<MatchMakingEntry> getByLoginId(long loginId);

	@Modifying
	@Query("UPDATE MatchMakingEntry e SET e.active = false WHERE e.login.id IN (?1)")
	void disableMatchMakingForLoginIds(List<Long> loginIds);

	@Modifying
	@Query("DELETE FROM MatchMakingEntry e WHERE e.login.id = ?1")
	void deleteByLoginId(long loginId);
}
