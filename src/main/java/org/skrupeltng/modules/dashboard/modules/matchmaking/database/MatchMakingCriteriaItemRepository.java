package org.skrupeltng.modules.dashboard.modules.matchmaking.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface MatchMakingCriteriaItemRepository extends JpaRepository<MatchMakingCriteriaItem, Long> {

	@Modifying
	@Query("DELETE FROM MatchMakingCriteriaItem i WHERE i.matchMakingEntry.id IN (SELECT m.id FROM MatchMakingEntry m WHERE m.login.id = ?1)")
	void deleteByLoginId(long loginId);
}
