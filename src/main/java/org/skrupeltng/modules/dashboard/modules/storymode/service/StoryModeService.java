package org.skrupeltng.modules.dashboard.modules.storymode.service;

import org.skrupeltng.modules.dashboard.modules.storymode.controller.StartMissionRequest;
import org.skrupeltng.modules.dashboard.modules.storymode.database.StoryModeCampaign;
import org.skrupeltng.modules.dashboard.modules.storymode.database.StoryModeCampaignRepository;
import org.skrupeltng.modules.dashboard.service.GameRemoval;
import org.skrupeltng.modules.dashboard.service.GameStartingHelper;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.service.round.PlanetRoundCalculator;
import org.skrupeltng.modules.ingame.service.round.RoundPreparer;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StoryModeService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private StoryModeCampaignRepository storyModeCampaignRepository;

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private GameStartingHelper gameStartingHelper;

	@Autowired
	private PlanetRoundCalculator planetRoundCalculator;

	@Autowired
	private RoundPreparer roundPreparer;

	@Autowired
	private GameRemoval gameRemoval;

	@Autowired
	private Map<String, StoryModeMissionProvider> missionProviders;

	public List<StoryModeCampaignItem> getUnlockedCampaigns(long loginId) {
		List<StoryModeCampaign> campaigns = storyModeCampaignRepository.findByLoginId(loginId);

		Map<String, Game> storyGames = gameRepository.getStoryModeMissions(loginId).stream()
				.collect(Collectors.toMap(g -> g.getStoryModeCampaign().getFaction().getId() + "_" + g.getStoryModeMission(), g -> g, (a, b) -> a));

		List<StoryModeCampaignItem> campaignData = new ArrayList<>();

		boolean highestUnlockedFound = false;

		for (StoryModeCampaign campaign : campaigns) {
			StoryModeCampaignItem campaignEntry = new StoryModeCampaignItem();
			campaignData.add(campaignEntry);

			Faction faction = campaign.getFaction();
			campaignEntry.setFaction(faction);

			StoryModeMissionProvider missionProvider = missionProviders.get(faction.getId());
			campaignEntry.setMissions(missionProvider.getMissionData(campaign));

			for (StoryModeMissionItem mission : campaignEntry.getMissions()) {
				Game game = storyGames.get(faction.getId() + "_" + mission.getMissionNumber());

				if (game != null) {
					mission.setGameId(game.getId());
				}

				if (mission.getMissionNumber() == campaign.getHighestUnlockedMission()) {
					mission.setHighestUnlockedMission(true);

					if (!highestUnlockedFound && !mission.isCompleted()) {
						highestUnlockedFound = true;
						campaignEntry.setHasHighestUnlockedMission(true);
					}
				}
			}
		}

		return campaignData;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public long startMission(StartMissionRequest request, long loginId) {
		for (int i = 0; i < 10; i++) {
			long gameId = 0L;

			try {
				StoryModeMissionProvider missionProvider = missionProviders.get(request.getFactionId());
				gameId = missionProvider.createGame(loginId, request.getMissionNumber());

				gameStartingHelper.startGame(gameId, loginId);

				missionProvider.finalizeGame(gameId, request.getMissionNumber());
				planetRoundCalculator.logVisitedPlanets(gameId);
				planetRoundCalculator.scanPlanetsWithShips(gameId);

				roundPreparer.computeAIRounds(gameId);
				roundPreparer.setupTurnValues(gameId, true);

				missionProvider.verifyMissionCreation(request.getMissionNumber(), gameId);

				return gameId;
			} catch (Exception e) {
				logger.error("Error while creating mission {} of {}", request.getMissionNumber(), request.getFactionId(), e);

				if (gameId > 0L) {
					gameRemoval.delete(gameId);
				}
			}
		}

		return -1L;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@PreAuthorize("hasPermission(#gameId, 'game')")
	public long restartMission(long gameId, long loginId) {
		Game game = gameRepository.getReferenceById(gameId);

		StoryModeCampaign storyModeCampaign = game.getStoryModeCampaign();
		int missionNumber = game.getStoryModeMission();

		if (storyModeCampaign == null) {
			throw new IllegalArgumentException("Game " + gameId + " is not a story mission!");
		}

		gameRemoval.delete(game);

		StartMissionRequest request = new StartMissionRequest();
		request.setFactionId(storyModeCampaign.getFaction().getId());
		request.setMissionNumber(missionNumber);

		return startMission(request, loginId);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@PreAuthorize("hasPermission(#gameId, 'game')")
	public void markMissionStepAsSeen(long gameId) {
		Game game = gameRepository.getReferenceById(gameId);
		game.setStoryModeMissionStepSeen(true);
		gameRepository.save(game);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public long loadNextMission(long gameId, long loginId) {
		Game game = gameRepository.getReferenceById(gameId);

		StoryModeCampaign storyModeCampaign = game.getStoryModeCampaign();
		int missionNumber = game.getStoryModeMission();

		if (storyModeCampaign == null) {
			throw new IllegalArgumentException("Game " + gameId + " is not a story mission!");
		}

		gameRemoval.delete(game);

		StartMissionRequest request = new StartMissionRequest();

		if (storyModeCampaign.getMissionCount() == missionNumber) {
			StoryModeMissionProvider missionProvider = missionProviders.get(storyModeCampaign.getFaction().getId());
			Optional<String> nextFactionOpt = missionProvider.getNextCampaignFaction();

			if (nextFactionOpt.isEmpty()) {
				throw new IllegalArgumentException("There are no more campaigns after " + storyModeCampaign.getFaction().getId() + "!");
			}

			request.setFactionId(nextFactionOpt.get());
			request.setMissionNumber(1);
		} else {
			request.setFactionId(storyModeCampaign.getFaction().getId());
			request.setMissionNumber(missionNumber + 1);
		}

		return startMission(request, loginId);
	}
}
