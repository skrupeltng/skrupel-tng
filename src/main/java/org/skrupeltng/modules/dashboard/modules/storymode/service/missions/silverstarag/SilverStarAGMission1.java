package org.skrupeltng.modules.dashboard.modules.storymode.service.missions.silverstarag;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.dashboard.modules.storymode.service.missions.StoryModeMissionHelper;
import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHole;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleType;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.masterdata.database.FactionId;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class SilverStarAGMission1 {

	private static final Logger log = LoggerFactory.getLogger(SilverStarAGMission1.class);

	private static final String SMALL_FREIGHTER = "silverstarag_4";

	public static final int STEP_1_BUILD_FACTORIES = 1;
	public static final int STEP_2_BUILD_MINES = 2;
	public static final int STEP_3_BUILD_ORBITAL_SYSTEM = 3;
	public static final int STEP_4_UPGRADE_STARBASE = 4;
	public static final int STEP_5_BUILD_SHIP = 5;
	public static final int STEP_6_FILL_SHIP = 6;
	public static final int STEP_7_SEND_SHIP = 7;
	public static final int STEP_8_COLONIZE_FIRST_PLANET = 8;
	public static final int STEP_9_SETUP_COLONY = 9;
	public static final int STEP_10_COLONIZE_MORE_PLANETS = 10;
	public static final int STEP_11_SETUP_ROUTE = 11;
	public static final int STEP_12_INVASION_START = 12;
	public static final int STEP_13_FIRST_SHIP_DESTROYED = 13;

	private final PlanetRepository planetRepository;
	private final PlayerRepository playerRepository;
	private final ShipRepository shipRepository;
	private final StarbaseRepository starbaseRepository;
	private final WormHoleRepository wormHoleRepository;
	private final StoryModeMissionHelper missionHelper;
	private final ConfigProperties configProperties;

	public SilverStarAGMission1(PlanetRepository planetRepository, PlayerRepository playerRepository, ShipRepository shipRepository, StarbaseRepository starbaseRepository, WormHoleRepository wormHoleRepository, StoryModeMissionHelper missionHelper, ConfigProperties configProperties) {
		this.planetRepository = planetRepository;
		this.playerRepository = playerRepository;
		this.shipRepository = shipRepository;
		this.starbaseRepository = starbaseRepository;
		this.wormHoleRepository = wormHoleRepository;
		this.missionHelper = missionHelper;
		this.configProperties = configProperties;
	}

	public void finalizeGame(long gameId) {
		List<Player> players = playerRepository.findByGameId(gameId);
		Player aiPlayer = missionHelper.getAIPlayer(players);
		missionHelper.clearHomePlanet(aiPlayer);

		Player humanPlayer = missionHelper.getHumanPlayer(players);
		missionHelper.clearHomePlanet(humanPlayer);

		Game game = humanPlayer.getGame();

		Planet humanHomePlanet = game.getPlanets().stream().min(Comparator.comparing(Planet::getY)).orElseThrow();
		missionHelper.inhabitPlanet(7500, 2000, humanPlayer, 53017, humanHomePlanet);
		humanHomePlanet.setName("Vucurn");
		humanHomePlanet.setColonists(53017);
		humanHomePlanet.setMines(203);
		humanHomePlanet.setFactories(118);
		humanHomePlanet.setPlanetaryDefense(20);
		humanHomePlanet.setSupplies(5);
		humanHomePlanet.setFuel(312);
		humanHomePlanet.setMineral1(325);
		humanHomePlanet.setMineral2(367);
		humanHomePlanet.setMineral3(319);
		humanHomePlanet.setAutoBuildFactories(false);
		humanHomePlanet.setAutoBuildMines(false);
		humanHomePlanet.setAutoBuildPlanetaryDefense(false);
		humanHomePlanet = planetRepository.save(humanHomePlanet);

		humanPlayer.setHomePlanet(humanHomePlanet);

		playerRepository.save(humanPlayer);

		Starbase starbase = new Starbase();
		starbase.setPlanet(humanHomePlanet);
		starbase.setType(StarbaseType.STAR_BASE);
		starbase.setName("Starbase 1");

		starbase = starbaseRepository.save(starbase);

		humanHomePlanet.setStarbase(starbase);

		WormHole wormHole = new WormHole();
		wormHole.setGame(game);
		wormHole.setType(WormHoleType.UNSTABLE_WORMHOLE);
		wormHole.setX(game.getGalaxySize() / 2);
		wormHole.setY(game.getGalaxySize() / 2);
		wormHoleRepository.save(wormHole);
	}

	public Optional<Integer> checkRoundEnd(Game game) {
		switch (game.getStoryModeMissionStep()) {
			case STEP_1_BUILD_FACTORIES -> checkStep1FactoriesBuilt(game);
			case STEP_2_BUILD_MINES -> checkStep2MinesBuilt(game);
			case STEP_3_BUILD_ORBITAL_SYSTEM -> checkStep3OrbitalSystemBuilt(game);
			case STEP_4_UPGRADE_STARBASE -> checkStep4StarbaseUpgraded(game);
			case STEP_5_BUILD_SHIP -> checkStep5ShipBuilt(game);
			case STEP_6_FILL_SHIP -> checkStep6ShipFilled(game);
			case STEP_7_SEND_SHIP -> checkStep7ShipSent(game);
			case STEP_8_COLONIZE_FIRST_PLANET -> checkStep8PlanetColonized(game);
			case STEP_9_SETUP_COLONY -> checkStep9FirstColonySetup(game);
			case STEP_10_COLONIZE_MORE_PLANETS -> checkStep10MorePlanetsColonized(game);
			case STEP_11_SETUP_ROUTE -> checkStep11RouteSetup(game);
			case STEP_12_INVASION_START -> checkStep12ShipInvestigated(game);
			case STEP_13_FIRST_SHIP_DESTROYED -> checkStep13ShipEnteredWormhole(game);
			default -> log.error("Unknown story mission step {}", game.getStoryModeMissionStep());
		}

		return Optional.empty();
	}

	private void checkStep1FactoriesBuilt(Game game) {
		Player player = missionHelper.getHumanPlayer(game.getPlayers());
		Planet homePlanet = player.getHomePlanet();

		if (homePlanet.getFactories() >= 123) {
			missionHelper.setNextStep(game, STEP_2_BUILD_MINES);
		}
	}

	private void checkStep2MinesBuilt(Game game) {
		Player player = missionHelper.getHumanPlayer(game.getPlayers());
		Planet homePlanet = player.getHomePlanet();

		if (homePlanet.getMines() >= 20) {
			missionHelper.setNextStep(game, STEP_3_BUILD_ORBITAL_SYSTEM);
		}
	}

	private void checkStep3OrbitalSystemBuilt(Game game) {
		Player player = missionHelper.getHumanPlayer(game.getPlayers());
		Planet homePlanet = player.getHomePlanet();

		if (homePlanet.hasOrbitalSystem(OrbitalSystemType.MEGA_FACTORY)) {
			missionHelper.setNextStep(game, STEP_4_UPGRADE_STARBASE);
		}
	}

	private void checkStep4StarbaseUpgraded(Game game) {
		Player player = missionHelper.getHumanPlayer(game.getPlayers());
		Starbase starbase = player.getHomePlanet().getStarbase();

		if (starbase.getHullLevel() >= 3 && starbase.getPropulsionLevel() >= 7) {
			missionHelper.setNextStep(game, STEP_5_BUILD_SHIP);
		}
	}

	private void checkStep5ShipBuilt(Game game) {
		Player player = missionHelper.getHumanPlayer(game.getPlayers());
		Set<Ship> ships = player.getShips();

		if (ships.stream().anyMatch(s -> s.getShipTemplate().getId().equals(SMALL_FREIGHTER))) {
			missionHelper.setNextStep(game, STEP_6_FILL_SHIP);
		}
	}

	private void checkStep6ShipFilled(Game game) {
		Player player = missionHelper.getHumanPlayer(game.getPlayers());
		Set<Ship> ships = player.getShips();

		Optional<Ship> freighterOpt = ships.stream().filter(s -> s.getShipTemplate().getId().equals(SMALL_FREIGHTER)).findFirst();

		if (freighterOpt.isPresent()) {
			Ship freighter = freighterOpt.get();

			if (freighterIsFilled(freighter, 30)) {
				missionHelper.setNextStep(game, STEP_7_SEND_SHIP);
			}
		}
	}

	private void checkStep7ShipSent(Game game) {
		Player player = missionHelper.getHumanPlayer(game.getPlayers());
		Set<Ship> ships = player.getShips();

		Optional<Ship> freighterOpt = ships.stream().filter(s -> s.getShipTemplate().getId().equals(SMALL_FREIGHTER)).findFirst();

		if (freighterOpt.isPresent()) {
			Ship freighter = freighterOpt.get();

			if (freighterIsFilled(freighter, 0) && freighter.getPlanet() != null && freighter.getPlanet().getId() != player.getHomePlanet().getId()) {
				missionHelper.setNextStep(game, STEP_8_COLONIZE_FIRST_PLANET);
			}
		}
	}

	private boolean freighterIsFilled(Ship freighter, int minFuel) {
		return freighter.getFuel() >= minFuel && freighter.getColonists() >= 1000 && freighter.getMoney() >= 150 && freighter.getSupplies() >= 20;
	}

	private void checkStep8PlanetColonized(Game game) {
		Player player = missionHelper.getHumanPlayer(game.getPlayers());

		if (player.getPlanets().size() > 1 &&
			player.getPlanets().stream().anyMatch(p -> p.getId() != player.getHomePlanet().getId() && p.getSupplies() > 0 && p.getMoney() > 0)) {
			missionHelper.setNextStep(game, STEP_9_SETUP_COLONY);
		}
	}

	private void checkStep9FirstColonySetup(Game game) {
		Player player = missionHelper.getHumanPlayer(game.getPlayers());

		if (player.getPlanets().size() > 1 &&
			player.getPlanets().stream().anyMatch(p -> p.getId() != player.getHomePlanet().getId() && p.getFactories() > 0)) {
			missionHelper.setNextStep(game, STEP_10_COLONIZE_MORE_PLANETS);
		}
	}

	private void checkStep10MorePlanetsColonized(Game game) {
		Player player = missionHelper.getHumanPlayer(game.getPlayers());

		if (player.getPlanets().size() > 3) {
			missionHelper.setNextStep(game, STEP_11_SETUP_ROUTE);
		}
	}

	private void checkStep11RouteSetup(Game game) {
		Player player = missionHelper.getHumanPlayer(game.getPlayers());

		if (player.getShips().stream().anyMatch(s -> s.getRoute().size() > 1)) {
			missionHelper.setNextStep(game, STEP_12_INVASION_START);

			Player aiPlayer = missionHelper.getAIPlayer(game.getPlayers());

			PlanetEntry farthestPlanet = getFarthestVisiblePlanetFromHomePlanet(player);

			if (farthestPlanet == null) {
				return;
			}

			Vector2D planet2D = new Vector2D(farthestPlanet.getX(), farthestPlanet.getY());
			Vector2D homePlanet2D = new Vector2D(player.getHomePlanet().getX(), player.getHomePlanet().getY());
			Vector2D direction = homePlanet2D.subtract(planet2D).normalize();

			Vector2D shipPos = planet2D.add(direction.scalarMultiply(15));
			int x = (int) shipPos.getX();
			int y = (int) shipPos.getY();

			missionHelper.spawnShip("nightmare_10", "transwarp", "particle_vortex", null, x, y, aiPlayer, "Destroyer", null);
		}
	}

	private PlanetEntry getFarthestVisiblePlanetFromHomePlanet(Player player) {
		List<PlanetEntry> visiblePlanets = missionHelper.getVisiblePlanets(player);

		PlanetEntry farthestPlanet = null;
		double maxDist = 0f;

		Coordinate center = player.getHomePlanet();

		for (PlanetEntry planet : visiblePlanets) {
			double dist = planet.getDistance(center);

			if (dist > maxDist) {
				maxDist = dist;
				farthestPlanet = planet;
			}
		}

		return farthestPlanet;
	}

	private void checkStep12ShipInvestigated(Game game) {
		Player aiPlayer = missionHelper.getAIPlayer(game.getPlayers());
		Set<Ship> ships = aiPlayer.getShips();

		Player humanPlayer = missionHelper.getHumanPlayer(game.getPlayers());

		if (ships.size() == 1) {
			Ship ship = ships.iterator().next();

			for (Ship humanPlayerShip : humanPlayer.getShips()) {
				if (ship.getDistance(humanPlayerShip) <= configProperties.getSubSpaceDistortionRange()) {
					ship.getAbility(ShipAbilityType.SUB_SPACE_DISTORTION).ifPresent(ability -> {
						ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
						ship.setActiveAbility(ability);
					});
					break;
				}
			}
		} else {
			missionHelper.setNextStep(game, STEP_13_FIRST_SHIP_DESTROYED);
			spawnInvaderShips(aiPlayer, humanPlayer);
		}
	}

	private void spawnInvaderShips(Player aiPlayer, Player humanPlayer) {
		List<Ship> humanPlayerShips = new ArrayList<>(humanPlayer.getShips());
		List<Planet> humanPlayerPlanets = new ArrayList<>(humanPlayer.getPlanets());

		PlanetEntry farthestPlanet = getFarthestVisiblePlanetFromHomePlanet(humanPlayer);

		if (farthestPlanet == null) {
			return;
		}

		int x = farthestPlanet.getX();
		int y = farthestPlanet.getY();

		addInvaderShipWithDestination(x, y, aiPlayer, humanPlayerShips, humanPlayerPlanets);
		addInvaderShipWithDestination(x, y, aiPlayer, humanPlayerShips, humanPlayerPlanets);
		addInvaderShipWithDestination(x, y, aiPlayer, humanPlayerShips, humanPlayerPlanets);
		addInvaderShipWithDestination(x, y, aiPlayer, humanPlayerShips, humanPlayerPlanets);
	}

	private void addInvaderShipWithDestination(int x, int y, Player aiPlayer, List<Ship> targetShips, List<Planet> targetPlanets) {
		x += missionHelper.getRandomCoordinateValue(15);
		y += missionHelper.getRandomCoordinateValue(15);

		Ship ship = missionHelper.spawnShip("nightmare_10", "transwarp", "particle_vortex", null, x, y, aiPlayer, "Destroyer", null);
		ship.setFuel(ship.getShipTemplate().getFuelCapacity());
		ship.setTaskType(ShipTaskType.PLANET_BOMBARDMENT);

		updateInvaderShipDestination(targetShips, targetPlanets, ship);

		shipRepository.save(ship);
	}

	private void updateInvaderShipDestination(List<Ship> targetShips, List<Planet> targetPlanets, Ship ship) {
		if (!targetShips.isEmpty()) {
			Ship targetShip = targetShips.get(MasterDataService.RANDOM.nextInt(targetShips.size()));
			ship.setDestinationShip(targetShip);
			ship.setDestinationX(targetShip.getX());
			ship.setDestinationY(targetShip.getY());
			ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
		} else if (!targetPlanets.isEmpty()) {
			Planet targetPlanet = targetPlanets.get(MasterDataService.RANDOM.nextInt(targetPlanets.size()));
			ship.setDestinationX(targetPlanet.getX());
			ship.setDestinationY(targetPlanet.getY());
			ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
		}
	}

	private void checkStep13ShipEnteredWormhole(Game game) {
		Player humanPlayer = missionHelper.getHumanPlayer(game.getPlayers());

		WormHole wormHole = wormHoleRepository.findByGameId(game.getId()).get(0);

		if (humanPlayer.getShips().stream()
			.anyMatch(s -> s.getDistance(wormHole) <= 15 && s.getColonists() >= 100 && s.getMoney() >= 100)) {
			game = missionHelper.finishMission(game, true);
			missionHelper.updateCompletedAndUnlockedMissionInCampaign(game, 1);
		} else if (humanPlayer.getShips().isEmpty() && humanPlayer.getPlanets().isEmpty()) {
			missionHelper.finishMission(game, false);
		} else {
			continueAttacks(game, humanPlayer);
		}
	}

	private void continueAttacks(Game game, Player humanPlayer) {
		Player aiPlayer = missionHelper.getAIPlayer(game.getPlayers());

		if (aiPlayer.getShips().isEmpty()) {
			spawnInvaderShips(aiPlayer, humanPlayer);
		} else {
			List<Ship> humanPlayerShips = new ArrayList<>(humanPlayer.getShips());
			List<Planet> humanPlayerPlanets = new ArrayList<>(humanPlayer.getPlanets());

			for (Ship ship : aiPlayer.getShips()) {
				if (ship.getDestinationShip() == null) {
					Optional<Planet> planetOpt = planetRepository.findByGameIdAndXAndY(game.getId(), ship.getDestinationX(), ship.getDestinationY());

					if (planetOpt.isEmpty() || planetOpt.get().getPlayer() == null) {
						updateInvaderShipDestination(humanPlayerShips, humanPlayerPlanets, ship);
					}
				}
			}
		}
	}

	public void verifyMissionCreation(long gameId) {
		List<Player> players = playerRepository.findByGameId(gameId);

		Player nightmarePlayer = missionHelper.getAIPlayer(players, FactionId.nightmare);
		if (nightmarePlayer.getHomePlanet() != null) {
			throw new IllegalStateException("Nightmare AI player should have no homeplanet!");
		}

		Player humanPlayer = missionHelper.getHumanPlayer(players);
		Objects.requireNonNull(humanPlayer.getHomePlanet());
	}
}
