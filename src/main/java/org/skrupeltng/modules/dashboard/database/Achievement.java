package org.skrupeltng.modules.dashboard.database;

import java.io.Serializable;
import java.time.Instant;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "achievement")
public class Achievement implements Serializable {

	private static final long serialVersionUID = -2316878593537235327L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "earned_date")
	private Instant earnedDate;

	@ManyToOne(fetch = FetchType.LAZY)
	private Login login;

	@Enumerated(EnumType.STRING)
	private AchievementType type;

	public Achievement() {

	}

	public Achievement(Login login, AchievementType type) {
		this.login = login;
		this.type = type;
		this.earnedDate = Instant.now();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Instant getEarnedDate() {
		return earnedDate;
	}

	public void setEarnedDate(Instant earnedDate) {
		this.earnedDate = earnedDate;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public AchievementType getType() {
		return type;
	}

	public void setType(AchievementType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Achievement [id=" + id + ", earnedDate=" + earnedDate + ", login=" + login + ", type=" + type + "]";
	}
}
