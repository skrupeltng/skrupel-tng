package org.skrupeltng.modules.dashboard.database;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "data_privacy_statement")
public class DataPrivacyStatement implements Serializable {

	private static final long serialVersionUID = -5420690690758035048L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String language;

	private String text;

	public DataPrivacyStatement() {

	}

	public DataPrivacyStatement(String language) {
		this.language = language;
	}

	public DataPrivacyStatement(String language, String text) {
		this.language = language;
		this.text = text;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
