package org.skrupeltng.modules.dashboard.database;

import jakarta.annotation.PostConstruct;
import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.Roles;
import org.skrupeltng.modules.dashboard.modules.admin.controller.UserListResultDTO;
import org.skrupeltng.modules.dashboard.modules.admin.controller.UserSearchParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginRepositoryImpl extends RepositoryCustomBase implements LoginRepositoryCustom {

	private final List<String> aiUsernames = new ArrayList<>();

	@PostConstruct
	public void init() {
		for (AILevel aiLevel : AILevel.values()) {
			aiUsernames.add(aiLevel.name());
		}
	}

	@Override
	public Page<UserListResultDTO> searchUsers(UserSearchParameters parameters) {
		String username = parameters.username();
		Boolean createdGames = parameters.createdGames();
		Boolean playedGames = parameters.playedGames();
		Boolean matchMakingEnabled = parameters.matchMakingEnabled();

		String sql = """
			SELECT
				l.id,
				l.username,
				l.email,
				l.created,
				COALESCE(SUM(COALESCE(lsf.games_created)), 0) as "gamesCreated",
				COALESCE(SUM(COALESCE(lsf.games_played)), 0) as "gamesPlayed",
				COALESCE(SUM(COALESCE(lsf.games_won)), 0) as "gamesWon",
				COALESCE(SUM(COALESCE(lsf.games_lost)), 0) as "gamesLost",
				count(*) OVER() AS "totalElements"
			FROM
				login l
				LEFT OUTER JOIN login_stats_faction lsf
					ON lsf.login_id = l.id
				LEFT OUTER JOIN login_role lr
					ON lr.login_id = l.id AND lr.role_name = '%s'
				LEFT OUTER JOIN match_making_entry mme
					ON mme.login_id = l.id
			""".formatted(Roles.ADMIN);

		Map<String, Object> params = new HashMap<>();
		List<String> wheres = new ArrayList<>();
		List<String> havings = new ArrayList<>();

		wheres.add("lr.id IS NULL");

		wheres.add("l.username NOT IN (:aiUsernames)");
		params.put("aiUsernames", aiUsernames);

		if (StringUtils.isNotBlank(username)) {
			wheres.add("l.username ILIKE :username");
			params.put("username", "%" + username + "%");
		}

		if (matchMakingEnabled != null) {
			if (matchMakingEnabled) {
				wheres.add("mme.active = true");
			} else {
				wheres.add("(mme IS NULL OR mme.active = false)");
			}
		}

		if (createdGames != null) {
			if (createdGames) {
				havings.add("COALESCE(SUM(COALESCE(lsf.games_created, 0)), 0) > 0");
			} else {
				havings.add("COALESCE(SUM(COALESCE(lsf.games_created, 0)), 0) = 0");
			}
		}

		if (playedGames != null) {
			if (playedGames) {
				havings.add("COALESCE(SUM(COALESCE(lsf.games_played, 0)), 0) > 0");
			} else {
				havings.add("COALESCE(SUM(COALESCE(lsf.games_played, 0)), 0) = 0");
			}
		}

		sql += where(wheres);

		sql += """
			 GROUP BY
			l.id,
			l.username,
			l.email,
			l.created
			""";

		sql += having(havings);

		Pageable pageRequest = parameters.toPageable();
		Sort sort = pageRequest.getSort();
		String orderBy = createOrderBy(sort, "l.id DESC");

		sql += orderBy;

		RowMapper<UserListResultDTO> rowMapper = new BeanPropertyRowMapper<>(UserListResultDTO.class);

		return search(sql, params, pageRequest, rowMapper);
	}
}
