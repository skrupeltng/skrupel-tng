package org.skrupeltng.modules.dashboard.database;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "installation_details")
public class InstallationDetails implements Serializable {

	private static final long serialVersionUID = -4805793030150222119L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "legal_text")
	private String legalText;

	@Column(name = "domain_url")
	private String domainUrl;

	@Column(name = "contact_email")
	private String contactEmail;

	@Column(name = "guest_account_code")
	private String guestAccountCode;

	@Column(name = "discord_link")
	private String discordLink;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLegalText() {
		return legalText;
	}

	public void setLegalText(String legalText) {
		this.legalText = legalText;
	}

	public String getDomainUrl() {
		return domainUrl;
	}

	public void setDomainUrl(String domainUrl) {
		this.domainUrl = domainUrl;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getGuestAccountCode() {
		return guestAccountCode;
	}

	public void setGuestAccountCode(String guestAccountCode) {
		this.guestAccountCode = guestAccountCode;
	}

	public String getDiscordLink() {
		return discordLink;
	}

	public void setDiscordLink(String discordLink) {
		this.discordLink = discordLink;
	}
}
