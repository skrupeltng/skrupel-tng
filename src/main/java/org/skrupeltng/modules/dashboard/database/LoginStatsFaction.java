package org.skrupeltng.modules.dashboard.database;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "login_stats_faction")
public class LoginStatsFaction implements Serializable {

	private static final long serialVersionUID = 279028126563945957L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Login login;

	@Column(name = "faction_id")
	private String factionId;

	@Column(name = "games_created")
	private int gamesCreated;

	@Column(name = "games_played")
	private int gamesPlayed;

	@Column(name = "games_won")
	private int gamesWon;

	@Column(name = "games_lost")
	private int gamesLost;

	@Column(name = "planets_colonized")
	private int planetsColonized;

	@Column(name = "planets_conquered")
	private int planetsConquered;

	@Column(name = "planets_mined_out")
	private int planetsMinedOut;

	@Column(name = "planets_lost")
	private int planetsLost;

	@Column(name = "ships_built")
	private int shipsBuilt;

	@Column(name = "ships_destroyed")
	private int shipsDestroyed;

	@Column(name = "ships_captured")
	private int shipsCaptured;

	@Column(name = "ships_lost")
	private int shipsLost;

	@Column(name = "starbases_created")
	private int starbasesCreated;

	@Column(name = "starbases_conquered")
	private int starbasesConquered;

	@Column(name = "starbases_lost")
	private int starbasesLost;

	@Column(name = "starbases_maxed")
	private int starbasesMaxed;

	public LoginStatsFaction() {

	}

	public LoginStatsFaction(Login login, String factionId) {
		this.login = login;
		this.factionId = factionId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public String getFactionId() {
		return factionId;
	}

	public void setFactionId(String factionId) {
		this.factionId = factionId;
	}

	public int getGamesCreated() {
		return gamesCreated;
	}

	public void setGamesCreated(int gamesCreated) {
		this.gamesCreated = gamesCreated;
	}

	public int getGamesPlayed() {
		return gamesPlayed;
	}

	public void setGamesPlayed(int gamesPlayed) {
		this.gamesPlayed = gamesPlayed;
	}

	public int getGamesWon() {
		return gamesWon;
	}

	public void setGamesWon(int gamesWon) {
		this.gamesWon = gamesWon;
	}

	public int getGamesLost() {
		return gamesLost;
	}

	public void setGamesLost(int gamesLost) {
		this.gamesLost = gamesLost;
	}

	public int getPlanetsColonized() {
		return planetsColonized;
	}

	public void setPlanetsColonized(int planetsColonized) {
		this.planetsColonized = planetsColonized;
	}

	public int getPlanetsConquered() {
		return planetsConquered;
	}

	public void setPlanetsConquered(int planetsConquered) {
		this.planetsConquered = planetsConquered;
	}

	public int getPlanetsMinedOut() {
		return planetsMinedOut;
	}

	public void setPlanetsMinedOut(int planetsMinedOut) {
		this.planetsMinedOut = planetsMinedOut;
	}

	public int getPlanetsLost() {
		return planetsLost;
	}

	public void setPlanetsLost(int planetsLost) {
		this.planetsLost = planetsLost;
	}

	public int getShipsBuilt() {
		return shipsBuilt;
	}

	public void setShipsBuilt(int shipsBuilt) {
		this.shipsBuilt = shipsBuilt;
	}

	public int getShipsDestroyed() {
		return shipsDestroyed;
	}

	public void setShipsDestroyed(int shipsDestroyed) {
		this.shipsDestroyed = shipsDestroyed;
	}

	public int getShipsCaptured() {
		return shipsCaptured;
	}

	public void setShipsCaptured(int shipsCaptured) {
		this.shipsCaptured = shipsCaptured;
	}

	public int getShipsLost() {
		return shipsLost;
	}

	public void setShipsLost(int shipsLost) {
		this.shipsLost = shipsLost;
	}

	public int getStarbasesCreated() {
		return starbasesCreated;
	}

	public void setStarbasesCreated(int starbasesCreated) {
		this.starbasesCreated = starbasesCreated;
	}

	public int getStarbasesConquered() {
		return starbasesConquered;
	}

	public void setStarbasesConquered(int starbasesConquered) {
		this.starbasesConquered = starbasesConquered;
	}

	public int getStarbasesLost() {
		return starbasesLost;
	}

	public void setStarbasesLost(int starbasesLost) {
		this.starbasesLost = starbasesLost;
	}

	public int getStarbasesMaxed() {
		return starbasesMaxed;
	}

	public void setStarbasesMaxed(int starbasesMaxed) {
		this.starbasesMaxed = starbasesMaxed;
	}
}
