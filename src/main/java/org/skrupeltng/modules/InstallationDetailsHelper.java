package org.skrupeltng.modules;

import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.dashboard.database.InstallationDetailsRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component("installationDetailsHelper")
public class InstallationDetailsHelper {

	private final InstallationDetailsRepository installationDetailsRepository;
	private final HttpServletRequest httpServletRequest;

	public InstallationDetailsHelper(InstallationDetailsRepository installationDetailsRepository, HttpServletRequest httpServletRequest) {
		this.installationDetailsRepository = installationDetailsRepository;
		this.httpServletRequest = httpServletRequest;
	}

	@Cacheable("hasLegalText")
	public boolean hasLegalText() {
		String legalText = installationDetailsRepository.getLegalText();
		return StringUtils.isNotBlank(legalText);
	}

	@Cacheable("domainUrl")
	public String getDomainUrl() {
		return installationDetailsRepository.getDomainUrl();
	}

	public String getCanonicalUrl() {
		return installationDetailsRepository.getDomainUrl() + httpServletRequest.getRequestURI();
	}

	@Cacheable("contactEmail")
	public String getContactEmail() {
		return installationDetailsRepository.getContactEmail();
	}

	@Cacheable("discordLink")
	public String getDiscordLink() {
		return installationDetailsRepository.getDiscordLink();
	}

	@Cacheable("discordLinkPresent")
	public boolean discordLinkIsPresent() {
		return StringUtils.isNotBlank(getDiscordLink());
	}

	@CacheEvict(value = "hasLegalText", allEntries = true)
	public void clearHasLegalTextCache() {
		// clears the hasLegalText cache
	}

	@CacheEvict(value = "domainUrl", allEntries = true)
	public void clearDomainUrlCache() {
		// clears the domainUrl cache
	}

	@CacheEvict(value = "contactEmail", allEntries = true)
	public void clearContactEmailCache() {
		// clears the contactEmail cache
	}

	@CacheEvict(value = {"discordLink", "discordLinkPresent"}, allEntries = true)
	public void clearDiscordLinkCache() {
		// clears the discordLink and discordLinkPresent caches
	}
}
