package org.skrupeltng.modules.masterdata;

public interface StarbaseProducable {

	int getTechLevel();

	int getCostMoney();

	int getCostMineral1();

	int getCostMineral2();

	int getCostMineral3();
}