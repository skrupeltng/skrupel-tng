package org.skrupeltng.modules.masterdata.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface FactionRepository extends JpaRepository<Faction, String> {

	@Query("SELECT f.id FROM Faction f ORDER BY f.id ASC")
	List<String> getAllFactionIds();

	@Query("SELECT f FROM Faction f WHERE f.id != '" + Faction.RANDOM_FACTION + "' ORDER BY f.id ASC")
	List<Faction> getAllFactionsWithoutRandom();
}