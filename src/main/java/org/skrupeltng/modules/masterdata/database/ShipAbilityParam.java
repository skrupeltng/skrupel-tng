package org.skrupeltng.modules.masterdata.database;

public class ShipAbilityParam {

	private final int startIndex;
	private final int length;
	private final float valueMultiplier;
	private final String name;
	private final int checkValue;
	private final int checkValueMin;

	public ShipAbilityParam(int startIndex, int length) {
		this(startIndex, length, 1f, null);
	}

	public ShipAbilityParam(int startIndex, int length, String name) {
		this(startIndex, length, 1f, name);
	}

	public ShipAbilityParam(int startIndex, int length, int checkValue) {
		this(startIndex, length, 1f, null, checkValue, 0);
	}

	public ShipAbilityParam(int startIndex, int length, int checkValue, int checkValueMin) {
		this(startIndex, length, 1f, null, checkValue, checkValueMin);
	}

	public ShipAbilityParam(int startIndex, int length, float valueMultiplier, String name) {
		this(startIndex, length, valueMultiplier, name, 0, 0);
	}

	public ShipAbilityParam(int startIndex, int length, float valueMultiplier, String name, int checkValue, int checkValueMin) {
		this.startIndex = startIndex;
		this.length = length;
		this.valueMultiplier = valueMultiplier;
		this.name = name;
		this.checkValue = checkValue;
		this.checkValueMin = checkValueMin;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public int getLength() {
		return length;
	}

	public int getEndIndex() {
		return startIndex + length;
	}

	public float getValueMultiplier() {
		return valueMultiplier;
	}

	public String getName() {
		return name;
	}

	public int getCheckValue() {
		return checkValue;
	}

	public int getCheckValueMin() {
		return checkValueMin;
	}
}