package org.skrupeltng.modules.masterdata.database;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.ingame.modules.starbase.controller.ShipTemplateRequirementsDTO;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

public class ShipTemplateRepositoryImpl extends RepositoryCustomBase implements ShipTemplateRepositoryCustom {

	@Override
	public ShipTemplateRequirementsDTO getRequirements(String shipTemplateId) {
		String sql = "" +
				"SELECT " +
				"	t.propulsion_systems_count as \"propulsionSystems\", " +
				"	t.energy_weapons_count as \"energyWeapons\", " +
				"	t.projectile_weapons_count as \"projectileWeapons\" " +
				"FROM " +
				"	ship_template t " +
				"WHERE " +
				"	t.id = :shipTemplateId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("shipTemplateId", shipTemplateId);

		RowMapper<ShipTemplateRequirementsDTO> rowMapper = new BeanPropertyRowMapper<>(ShipTemplateRequirementsDTO.class);

		List<ShipTemplateRequirementsDTO> results = jdbcTemplate.query(sql, params, rowMapper);

		if (results.size() == 1) {
			return results.get(0);
		}

		return null;
	}
}