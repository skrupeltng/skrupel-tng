package org.skrupeltng.modules.masterdata.database;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import org.skrupeltng.modules.masterdata.StarbaseProducable;

@Entity
@Table(name = "weapon_template")
public class WeaponTemplate implements Serializable, StarbaseProducable {

	private static final long serialVersionUID = 7181804630298196368L;

	@Id
	private String name;

	@Column(name = "damage_index")
	private int damageIndex;

	@Column(name = "tech_level")
	private int techLevel;

	@Column(name = "uses_projectiles")
	private boolean usesProjectiles;

	@Column(name = "cost_money")
	private int costMoney;

	@Column(name = "cost_mineral1")
	private int costMineral1;

	@Column(name = "cost_mineral2")
	private int costMineral2;

	@Column(name = "cost_mineral3")
	private int costMineral3;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDamageIndex() {
		return damageIndex;
	}

	public void setDamageIndex(int damageIndex) {
		this.damageIndex = damageIndex;
	}

	@Override
	public int getTechLevel() {
		return techLevel;
	}

	public void setTechLevel(int techLevel) {
		this.techLevel = techLevel;
	}

	public boolean isUsesProjectiles() {
		return usesProjectiles;
	}

	public void setUsesProjectiles(boolean usesProjectiles) {
		this.usesProjectiles = usesProjectiles;
	}

	@Override
	public int getCostMoney() {
		return costMoney;
	}

	public void setCostMoney(int costMoney) {
		this.costMoney = costMoney;
	}

	@Override
	public int getCostMineral1() {
		return costMineral1;
	}

	public void setCostMineral1(int costMineral1) {
		this.costMineral1 = costMineral1;
	}

	@Override
	public int getCostMineral2() {
		return costMineral2;
	}

	public void setCostMineral2(int costMineral2) {
		this.costMineral2 = costMineral2;
	}

	@Override
	public int getCostMineral3() {
		return costMineral3;
	}

	public void setCostMineral3(int costMineral3) {
		this.costMineral3 = costMineral3;
	}
}
