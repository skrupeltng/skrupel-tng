package org.skrupeltng.modules.masterdata.database;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface I18NRepository extends JpaRepository<I18N, Long> {

	Optional<I18N> findByKeyAndLanguage(String key, String language);

	@Query("SELECT i.value FROM I18N i WHERE i.key = ?1 AND i.language = ?2")
	String getValue(String key, String language);
}