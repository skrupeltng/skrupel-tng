package org.skrupeltng.modules.masterdata.database;

import java.util.Comparator;

import org.skrupeltng.modules.masterdata.StarbaseProducable;

public class ProducableTechLevelComparator implements Comparator<StarbaseProducable> {

	@Override
	public int compare(StarbaseProducable a, StarbaseProducable b) {
		return Integer.compare(a.getTechLevel(), b.getTechLevel());
	}
}