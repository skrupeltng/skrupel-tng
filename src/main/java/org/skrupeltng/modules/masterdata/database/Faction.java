package org.skrupeltng.modules.masterdata.database;

import java.io.Serializable;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpeciesType;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;

@Entity
@Table(name = "faction")
public class Faction implements Serializable {

	private static final long serialVersionUID = 8555363870878082042L;

	public static final String RANDOM_FACTION = "_RANDOM_FACTION";

	@Id
	private String id;

	@Column(name = "preferred_planet_type")
	private String preferredPlanetType;

	@Column(name = "preferred_temperature")
	private int preferredTemperature;

	@Column(name = "mine_production_rate")
	private float mineProductionRate;

	@Column(name = "factory_production_rate")
	private float factoryProductionRate;

	@Column(name = "tax_rate")
	private float taxRate;

	@Column(name = "ground_combat_attack_rate")
	private float groundCombatAttackRate;

	@Column(name = "ground_combat_defense_rate")
	private float groundCombatDefenseRate;

	@Column(name = "home_planet_name")
	private String homePlanetName;

	@Column(name = "assimilation_rate")
	private float assimilationRate;

	@Column(name = "assimilation_type")
	private NativeSpeciesType assimilationType;

	@Column(name = "unlocked_orbital_systems")
	private String unlockedOrbitalSystems;

	@Column(name = "forbidden_orbital_systems")
	private String forbiddenOrbitalSystems;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "faction")
	private List<ShipTemplate> ships;

	public Faction() {

	}

	public Faction(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPreferredPlanetType() {
		return preferredPlanetType;
	}

	public void setPreferredPlanetType(String preferredPlanetType) {
		this.preferredPlanetType = preferredPlanetType;
	}

	public int getPreferredTemperature() {
		return preferredTemperature;
	}

	public void setPreferredTemperature(int preferredTemperature) {
		this.preferredTemperature = preferredTemperature;
	}

	public float getMineProductionRate() {
		return mineProductionRate;
	}

	public void setMineProductionRate(float mineProductionRate) {
		this.mineProductionRate = mineProductionRate;
	}

	public float getFactoryProductionRate() {
		return factoryProductionRate;
	}

	public void setFactoryProductionRate(float factoryProductionRate) {
		this.factoryProductionRate = factoryProductionRate;
	}

	public float getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(float taxRate) {
		this.taxRate = taxRate;
	}

	public float getGroundCombatAttackRate() {
		return groundCombatAttackRate;
	}

	public void setGroundCombatAttackRate(float groundCombatAttackRate) {
		this.groundCombatAttackRate = groundCombatAttackRate;
	}

	public float getGroundCombatDefenseRate() {
		return groundCombatDefenseRate;
	}

	public void setGroundCombatDefenseRate(float groundCombatDefenseRate) {
		this.groundCombatDefenseRate = groundCombatDefenseRate;
	}

	public List<ShipTemplate> getShips() {
		return ships;
	}

	public void setShips(List<ShipTemplate> ships) {
		this.ships = ships;
	}

	public String getHomePlanetName() {
		return homePlanetName;
	}

	public void setHomePlanetName(String homePlanetName) {
		this.homePlanetName = homePlanetName;
	}

	public float getAssimilationRate() {
		return assimilationRate;
	}

	public void setAssimilationRate(float assimilationRate) {
		this.assimilationRate = assimilationRate;
	}

	public NativeSpeciesType getAssimilationType() {
		return assimilationType;
	}

	public void setAssimilationType(NativeSpeciesType assimilationType) {
		this.assimilationType = assimilationType;
	}

	public String getUnlockedOrbitalSystems() {
		return unlockedOrbitalSystems;
	}

	public void setUnlockedOrbitalSystems(String unlockedOrbitalSystems) {
		this.unlockedOrbitalSystems = unlockedOrbitalSystems;
	}

	public String getForbiddenOrbitalSystems() {
		return forbiddenOrbitalSystems;
	}

	public void setForbiddenOrbitalSystems(String forbiddenOrbitalSystems) {
		this.forbiddenOrbitalSystems = forbiddenOrbitalSystems;
	}

	public boolean orbitalSystemAllowed(OrbitalSystemType type) {
		if (unlockedOrbitalSystems != null && unlockedOrbitalSystems.contains(type.name())) {
			return true;
		}

		if (forbiddenOrbitalSystems != null && forbiddenOrbitalSystems.contains(type.name())) {
			return false;
		}

		return type.isAllowedByDefault();
	}
}
