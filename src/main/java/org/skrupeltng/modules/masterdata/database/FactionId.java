package org.skrupeltng.modules.masterdata.database;

@java.lang.SuppressWarnings("java:S115")
public enum FactionId {

	orion, kuatoh, trodan, nightmare, silverstarag, kopaytirish, oltin, vantu
}