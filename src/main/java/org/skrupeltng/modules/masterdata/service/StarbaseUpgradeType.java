package org.skrupeltng.modules.masterdata.service;

public enum StarbaseUpgradeType {

	HULL, PROPULSION, ENERGY, PROJECTILE
}