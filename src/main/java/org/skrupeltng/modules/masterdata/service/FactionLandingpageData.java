package org.skrupeltng.modules.masterdata.service;

import org.skrupeltng.modules.ingame.modules.ship.controller.ShipAbilityDescription;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;

public class FactionLandingpageData {

	private Faction faction;
	private String name;
	private String description;
	private ShipTemplate template1;
	private ShipTemplate template2;
	private String templateName1;
	private String templateName2;
	private ShipAbilityDescription ability1;
	private ShipAbilityDescription ability2;

	public Faction getFaction() {
		return faction;
	}

	public void setFaction(Faction faction) {
		this.faction = faction;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ShipTemplate getTemplate1() {
		return template1;
	}

	public void setTemplate1(ShipTemplate template1) {
		this.template1 = template1;
	}

	public ShipTemplate getTemplate2() {
		return template2;
	}

	public void setTemplate2(ShipTemplate template2) {
		this.template2 = template2;
	}

	public String getTemplateName1() {
		return templateName1;
	}

	public void setTemplateName1(String templateName1) {
		this.templateName1 = templateName1;
	}

	public String getTemplateName2() {
		return templateName2;
	}

	public void setTemplateName2(String templateName2) {
		this.templateName2 = templateName2;
	}

	public ShipAbilityDescription getAbility1() {
		return ability1;
	}

	public void setAbility1(ShipAbilityDescription ability1) {
		this.ability1 = ability1;
	}

	public ShipAbilityDescription getAbility2() {
		return ability2;
	}

	public void setAbility2(ShipAbilityDescription ability2) {
		this.ability2 = ability2;
	}
}