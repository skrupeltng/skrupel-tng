cd /vagrant/
cd docker/php
git clone https://github.com/kantoks/skrupel.git
cp inc.conf.php skrupel/.
cd skrupel/daten
git clone https://github.com/skrupel/skrupel-rasse-borg.git borg
git clone https://github.com/skrupel/skrupel-rasse-dominion.git dominion
git clone https://github.com/skrupel/skrupel-rasse-eldar.git eldar
git clone https://github.com/skrupel/skrupel-rasse-empire.git empire
git clone https://github.com/skrupel/skrupel-rasse-erdallianz.git erdallianz
git clone https://github.com/skrupel/skrupel-rasse-foederation.git foederation
git clone https://github.com/skrupel/skrupel-rasse-isa.git isa
git clone https://github.com/skrupel/skrupel-rasse-klingon.git klingon
git clone https://github.com/skrupel/skrupel-rasse-kuatoh.git kuatoh
git clone https://github.com/skrupel/skrupel-rasse-rebels.git rebels
git clone https://github.com/skrupel/skrupel-rasse-replikator.git replikator
git clone https://github.com/skrupel/skrupel-rasse-romulan.git romulan
git clone https://github.com/skrupel/skrupel-rasse-schatten.git schatten
git clone https://github.com/skrupel/skrupel-rasse-silverstarag.git silverstarag
git clone https://github.com/skrupel/skrupel-rasse-ucom.git ucom
git clone https://github.com/skrupel/skrupel-rasse-zylonen.git zylonen
cd ../../
docker build -t php .
cd ../mysql
docker build -t mysql .
