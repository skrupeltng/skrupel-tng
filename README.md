[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=skrupeltng_skrupel-tng&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=skrupeltng_skrupel-tng)

# Skrupel TNG
Skrupel-TNG is a free sci-fi browser game based on the PHP-based browser game [Skrupel](https://github.com/kantoks/skrupel) by Bernd Kantoks.

It is meant to be free for everyone without any ads. Anyone is welcome to deploy it on their own server. The current production release can
 be played [here](https://skrupeltng.de/). Feel free to join and try it out!

## Technology stack
* Java 19
* Spring Boot 3
* PostgreSQL 14
* Thymeleaf 3
* Bootstrap 5
* TypeScript 5

## Build
Run the following command to get an executable jar file:
```bash
./gradlew build -x test
```

### Upgrade Notifications
When upgrading to **version 6.0 or higher** you will need to execute the following SQL command on your database before deploying the new version:
   ```sql
   update databasechangelog set md5sum = null;
   ```

When upgrading to **version 2024.3 or higher** you will need to remove the following lines of your application.yaml file before deploying the new version:
   ```yaml
   spring.jpa.properties.hibernate.temp.use_jdbc_metadata_defaults: false
   spring.jpa.database-platform: org.hibernate.dialect.PostgreSQL10Dialect
   ```

## Deployment
First steps could be as follows:

1. Create a file called `application.yml` file with the following content:

   ```yaml
   spring.datasource:
     url: jdbc:postgresql://<DATABASE_HOST>/<DATABASE_NAME>
     username: <DATABASE_USERNAME>
     password: <DATABASE_PASSWORD>
   spring.thymeleaf.cache: true
   server.port: 8080
   spring.liquibase.contexts: prod
   skr.enable_account_validation_by_email: false
   spring.mail:
    host: <EMAIL_HOST>
    port: <EMAIL_PORT>
    username: <EMAIL_USERNAME>
    password: <EMAIL_PASSWORD>
    properties.mail.smtp:
        auth: true
        starttls.enable: true
   ```
   Of course, you need to replace the `DATABASE_*` and `EMAIL_*` fields with your own data.
   The E-mail configuration is optional but recommended as this will enable features like password recovery and E-mail round notifications.
   If you want to force new users to verify their E-mail addresses you can set the `skr.enable_account_validation_by_email` to `true`.

2. Put the jar file in the same folder as the `application.yml` file.
3. Run the following shell command: `java -jar <JAR_FILE_NAME>`

   The application should start up and be reachable at port 8080.

It is recommended however to install a Spring Boot application as a service. Take a look at the [Spring Boot Documentation](https://docs.spring.io/spring-boot/docs/current/reference/html/deployment-install.html#deployment-systemd-service).

## Development
A general guide for development is [here](https://gitlab.com/robertbleyl/skrupel-tng/-/wikis/Skrupel-TNG-Development).
If you are interested in contributing a playable faction you can check out the [faction guide](https://gitlab.com/robertbleyl/skrupel-tng/-/wikis/Skrupel-TNG-Faction-Guide).
